const { MongoClient } = require("mongodb");
// Replace the uri string with your MongoDB deployment's connection string
const HOME = process.env.HOME;
const config = require(HOME+"/.parse/config.json");
const uri = config.databaseURL;
const client = new MongoClient(uri, { useUnifiedTopology: true } );

async function run() {
  try {
    console.log("Try");
    await client.connect();
    const db=await client.db('parse');
    const user=await db.collection("_User");
    const cursor=await user.find();
    console.log(await cursor.count());
    const items = [];
    const names = [];
    await cursor.forEach(function(item){names.push(item.username);items.push(item);})
    await names.forEach(function(item){console.log(item);});
  } finally {
    console.log("Finally");
    // Ensures that the client will close when you finish/error
    await client.close();
  }
}
run().catch(console.dir);
