const { MongoClient } = require("mongodb");
// Replace the uri string with your MongoDB deployment's connection string
const HOME = process.env.HOME;
const config = require(HOME+"/.parse/config.json");
const uri = config.databaseURL;

const client = new MongoClient(uri, { useUnifiedTopology: true } );
async function run() {
  try {
    console.log("Try");
    await client.connect();
    const database=await client.db('parse');
    const User = await database.collection('_User');
    await User.createIndex({username:1});
    find = User.find().sort({username:1});
    if(await find.hasNext()){
      next = await find.next();
      group=[next.username, next._id];
    };
    const dups=[];
    while(await find.hasNext()){
      next=await find.next();
      if(group[0] == next.username) {
        group.push(next._id);
      } else {
        if(group.length>2)
          dups.push(group);
        group=[ next.username, next._id ];
      };
    };
    while(dups.length) {
      group=dups.pop();
      console.log(group);
      while(group.length>2){
        User.deleteOne({_id: group.pop()});
        console.log(await User.find({_username: group[0]}).toArray());
      };
    };
    console.log("Tried");
  } finally {
    console.log("Finally");
    // Ensures that the client will close when you finish/error
    await client.close();
  }
}
run().catch(console.dir);
