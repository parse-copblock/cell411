const { MongoClient } = require("mongodb");
// Replace the uri string with your MongoDB deployment's connection string
const HOME = process.env.HOME;
const config = require(HOME+"/.parse/config.json");
const uri = config.databaseURL;
const client = new MongoClient(uri, { useUnifiedTopology: true } );

async function disp(db,c)
{
  console.log("table: "+c.name);
  const is = await db.collection(c.name).listIndexes().toArray();
  const ns = is.map(i => i.name);
  ns.forEach( function(n) {
    console.log("  "+n);
    if( n === "_id_" )
      return;
    db.collection(c.name).dropIndex(n);
  });
};
async function run() {
  try {
    console.log("Try");
    await client.connect();
    const db=await client.db('parse');
    const cs=await db.listCollections().toArray();
    for(i=0;i<cs.length;i++){
      await disp(db,cs[i]);
    };
  } finally {
    console.log("Finally");
    // Ensures that the client will close when you finish/error
    await client.close();
  }
}
run().catch(console.dir);
