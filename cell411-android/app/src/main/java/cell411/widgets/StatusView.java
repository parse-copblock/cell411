package cell411.widgets;/*
 *
 * WOWZA MEDIA SYSTEMS, LLC ("Wowza") CONFIDENTIAL
 * Copyright (c) 2005-2016 Wowza Media Systems, LLC, All Rights Reserved.
 *
 * NOTICE: All information contained herein is, and remains the property of Wowza Media Systems, LLC.
 * The intellectual and technical concepts contained herein are proprietary to Wowza Media Systems, LLC
 * and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret
 * or copyright law. Dissemination of this information or reproduction of this material is strictly forbidden
 * unless prior written permission is obtained from Wowza Media Systems, LLC. Access to the source code
 * contained herein is hereby forbidden to anyone except current Wowza Media Systems, LLC employees, managers
 * or contractors who have executed Confidentiality and Non-disclosure agreements explicitly covering such access.
 *
 * The copyright notice above does not evidence any actual or intended publication or disclosure of this
 * source code, which includes information that is confidential and/or proprietary, and is a trade secret, of
 * Wowza Media Systems, LLC. ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC PERFORMANCE, OR PUBLIC DISPLAY
 * OF OR THROUGH USE OF THIS SOURCE CODE WITHOUT THE EXPRESS WRITTEN CONSENT OF WOWZA MEDIA SYSTEMS, LLC IS
 * STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE LAWS AND INTERNATIONAL TREATIES. THE RECEIPT OR POSSESSION
 * OF THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS TO REPRODUCE, DISCLOSE OR
 * DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL ANYTHING THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.
 *
 */

import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.safearx.cell411.R;
import com.wowza.gocoder.sdk.api.errors.WZError;
import com.wowza.gocoder.sdk.api.status.WZState;
import com.wowza.gocoder.sdk.api.status.WZStatus;

import java.util.Stack;

public class StatusView extends RelativeLayout {
  final private static String TAG = "StatusView";
  private static Object lock = new Object();
  private TextView mTxtStatus;
  private Button mBtnDismiss;
  private AlphaAnimation showAnimation, hideAnimation;
  private volatile boolean isAnimating;
  private Stack<StatusMessage> mStatusStack;

  public StatusView(Context context) {
    super(context);
    init(context);
  }

  public StatusView(Context context, AttributeSet attrs) {
    super(context, attrs);
    init(context);
  }

  private void init(Context context) {
    mStatusStack = new Stack<>();
    LayoutInflater inflater =
      (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    inflater.inflate(R.layout.status_view, this, true);
    mTxtStatus = (TextView) findViewById(R.id.txtStatus);
    mTxtStatus.setTextColor(Color.WHITE);
    mBtnDismiss = (Button) findViewById(R.id.btnDismiss);
    mBtnDismiss.setVisibility(GONE);
    mBtnDismiss.setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(View view) {
        setVisibility(INVISIBLE);
        mBtnDismiss.setVisibility(GONE);
      }
    });
    showAnimation = new AlphaAnimation(0.0f, 1.0f);
    showAnimation.setDuration(500);
    showAnimation.setAnimationListener(new Animation.AnimationListener() {
      @Override
      public void onAnimationStart(Animation animation) {
        isAnimating = true;
        bringToFront();
        setClickable(true);
      }

      @Override
      public void onAnimationEnd(Animation animation) {
        bringToFront();
        setClickable(true);
        setVisibility(VISIBLE);
        setAlpha(1f);
        isAnimating = false;
        showNext();
      }

      @Override
      public void onAnimationRepeat(Animation animation) {
      }
    });
    hideAnimation = new AlphaAnimation(1.0f, 0.0f);
    hideAnimation.setDuration(1500);
    hideAnimation.setAnimationListener(new Animation.AnimationListener() {
      @Override
      public void onAnimationStart(Animation animation) {
        isAnimating = true;
      }

      @Override
      public void onAnimationEnd(Animation animation) {
        setClickable(false);
        setVisibility(View.INVISIBLE);
        setAlpha(1f);
        isAnimating = false;
      }

      @Override
      public void onAnimationRepeat(Animation animation) {
      }
    });
    isAnimating = false;
    setClickable(false);
  }

  public void setStatus(WZStatus status) {
    if(status.getLastError() == null) {
      setState(status.getState());
    } else {
      setError(status.getLastError());
    }
  }

  public void setState(int state) {
    synchronized(lock) {
      switch(state) {
        case WZState.IDLE:
        case WZState.RUNNING:
          mStatusStack.add(new StatusMessage(state));
          break;
        case WZState.STARTING:
          mStatusStack.add(
            new StatusMessage(state, getResources().getString(R.string.status_connecting)));
          break;
        case WZState.READY:
          mStatusStack.add(
            new StatusMessage(state, getResources().getString(R.string.status_connected)));
          break;
        case WZState.STOPPING:
          mStatusStack.add(
            new StatusMessage(state, getResources().getString(R.string.status_disconnecting)));
          break;
        default:
          return;
      }
    }
    showNext();
  }

  public void setError(WZError error) {
    clear();
    mTxtStatus.setText(error.getErrorDescription());
    mBtnDismiss.setVisibility(VISIBLE);
    setAlpha(1f);
    setVisibility(VISIBLE);
    bringToFront();
  }

  public void setMessage(String message) {
    synchronized(lock) {
      mStatusStack.push(new StatusMessage(message));
    }
  }

  public void setErrorMessage(String message) {
    setError(new WZError(message));
  }

  public void clear() {
    synchronized(lock) {
      mStatusStack.clear();
    }
  }

  private void showNext() {
    if(isAnimating) {
      return;
    }
    synchronized(lock) {
      if(!mStatusStack.empty()) {
        StatusMessage status = mStatusStack.pop();
        if(status.messageText == null) {
          startAnimation(hideAnimation);
        } else {
          mTxtStatus.setText(status.messageText);
          setAlpha(1f);
          if(getVisibility() != VISIBLE) {
            startAnimation(showAnimation);
          }
        }
      }
    }
  }

  private static class StatusMessage extends WZStatus {
    public String messageText;

    StatusMessage(int state) {
      super(state);
      this.messageText = null;
    }

    StatusMessage(int state, String messageText) {
      super(state);
      this.messageText = messageText;
    }

    StatusMessage(String messageText) {
      super();
      this.messageText = messageText;
    }
  }
}
