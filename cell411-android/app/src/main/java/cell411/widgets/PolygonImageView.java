package cell411.widgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.ImageView;

import com.safearx.cell411.R;

public class PolygonImageView extends ImageView implements OnTouchListener {
  String TAG;
  private OpaqueAreaClickListener mOpaqueAreaClickListener;

  public PolygonImageView(Context context, AttributeSet attrs) {
    super(context, attrs);
    TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.PolygonImageView, 0, 0);
    TAG = a.getString(R.styleable.PolygonImageView_tag);
    Log.i("tag", TAG);
    a.recycle();
    this.setOnTouchListener(this);
  }

  public void setOnOpaqueAreaClickListener(OpaqueAreaClickListener opAreaClickListener) {
    mOpaqueAreaClickListener = opAreaClickListener;
  }

  @Override
  public void onWindowFocusChanged(boolean hasWindowFocus) {
    super.onWindowFocusChanged(hasWindowFocus);
    this.setDrawingCacheEnabled(true);
  }

  @Override
  public boolean onTouch(View v, MotionEvent event) {
    if(event.getAction() == MotionEvent.ACTION_DOWN) {
      Bitmap bmp = v.getDrawingCache();
      int color = 0, x = (int) event.getX(), y = (int) event.getY();
      try {
        color = bmp.getPixel(x, y);
        if(color != Color.TRANSPARENT) {
          Log.i("Clicked in Custom Class", "YES");
          mOpaqueAreaClickListener.onOpaqueAreaClicked(TAG);
          return true;
        } else {
          return false;
        }
      } catch(Exception e) {
        e.printStackTrace();
      }
    }
    return false;
  }
}