package cell411.widgets;

public interface OpaqueAreaClickListener {
  public void onOpaqueAreaClicked(String TAG);
}
