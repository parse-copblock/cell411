package cell411.widgets;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Movie;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout.LayoutParams;

import com.safearx.cell411.R;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;

import cell411.utils.LogEvent;

public class SpinnerForBTConnection extends View {
  private static final boolean DECODE_STREAM = true;
  private Movie mMovie;
  private long mMovieStart;
  private int width = 100;
  private int height = 120;

  public SpinnerForBTConnection(Context context) {
    super(context);
    if(!isInEditMode()) {
      initialize(context);
    }
  }

  public SpinnerForBTConnection(Context context, AttributeSet attrs) {
    super(context, attrs);
    if(!isInEditMode()) {
      initialize(context);
    }
  }

  public SpinnerForBTConnection(Context context, AttributeSet attrs, int defStyle) {
    super(context, attrs, defStyle);
    if(!isInEditMode()) {
      initialize(context);
    }
  }

  private static byte[] streamToBytes(InputStream is) {
    ByteArrayOutputStream os = new ByteArrayOutputStream(1024);
    byte[] buffer = new byte[1024];
    int len;
    try {
      while((len = is.read(buffer)) >= 0) {
        os.write(buffer, 0, len);
      }
    } catch(java.io.IOException e) {
    }
    return os.toByteArray();
  }

  @SuppressWarnings("deprecation")
  @Override
  protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
    if((getLayoutParams().width == LayoutParams.MATCH_PARENT) ||
         (getLayoutParams().width == LayoutParams.FILL_PARENT)) {
      width = MeasureSpec.getSize(widthMeasureSpec);
    } else {
      width = getLayoutParams().width;
    }
    if((getLayoutParams().height == LayoutParams.MATCH_PARENT) ||
         (getLayoutParams().height == LayoutParams.FILL_PARENT)) {
      height = MeasureSpec.getSize(heightMeasureSpec);
    } else {
      height = getLayoutParams().height;
    }
    setMeasuredDimension(width | MeasureSpec.EXACTLY, height | MeasureSpec.EXACTLY);
  }

  @SuppressLint("DrawAllocation")
  @Override
  protected void onDraw(Canvas canvas) {
    super.onDraw(canvas);
    setLayerType(View.LAYER_TYPE_SOFTWARE, null);
    long now = android.os.SystemClock.uptimeMillis();
    if(mMovieStart == 0) { // first time
      mMovieStart = now;
    }
    if(mMovie != null) {
      int dur = mMovie.duration();
      if(dur == 0) {
        dur = 5000;
      }
      int relTime = (int) ((now - mMovieStart) % dur);
      mMovie.setTime(relTime);
      //LogEvent.Log("Spinner", "relTime: " + relTime);
      Bitmap movieBitmap =
        Bitmap.createBitmap(mMovie.width(), mMovie.height(), Bitmap.Config.ARGB_8888);
      Canvas movieCanvas = new Canvas(movieBitmap);
      mMovie.draw(movieCanvas, 0, 0);
      Rect src = new Rect(0, 0, mMovie.width(), mMovie.height());
      Rect dst = new Rect(0, 0, this.getWidth(), this.getHeight());
      canvas.drawBitmap(movieBitmap, src, dst, null);
      invalidate();
    } else {
      LogEvent.Log("Spinner", "Movie is null");
    }
  }

  private void initialize(Context context) {
    InputStream is;
    is = context.getResources().openRawResource(R.raw.spinner_for_bt);
    if(DECODE_STREAM) {
      mMovie = Movie.decodeStream(is);
    } else {
      byte[] array = streamToBytes(is);
      mMovie = Movie.decodeByteArray(array, 0, array.length);
    }
  }
}