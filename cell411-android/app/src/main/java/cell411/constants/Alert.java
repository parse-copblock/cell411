package cell411.constants;

/**
 * Created by Sachin on 27-04-2016.
 */
public class Alert {
  // alert values to be used while sending push notifications and storing on Parse table
  public static final String BROKEN_CAR = "Vehicle Broken";
  public static final String BULLIED = "Bullied";
  public static final String CRIMINAL = "Crime";
  public static final String GENERAL = "General";
  public static final String PULLED_OVER = "Vehicle Pulled";
  public static final String DANGER = "Danger";
  public static final String PHOTO = "Photo";
  public static final String FIRE = "Fire";
  public static final String MEDICAL = "Medical";
  public static final String POLICE_INTERACTION = "Cop Blocking";
  public static final String POLICE_ARREST = "Arrested";
  public static final String PANIC = "Panic";
  public static final String FALLEN = "Fallen";
  // To be used when an alert is not recognized
  public static final String UNRECOGNIZED = "Unrecognized";
  // Cell 411 specific alert
  public static final String VIDEO = "Video";
  // iER specific alert
  public static final String HIJACK = "Hijack";
  public static final String PRE_AUTHORISATION = "Pre Authorisation";
  // RO 112 specific alert
  public static final String PHYSICAL_ABUSE = "Physical Abuse";
  public static final String TRAPPED = "Trapped";
  public static final String CAR_ACCIDENT = "Car Accident";
  public static final String NATURAL_DISASTER = "Natural Disaster";
}