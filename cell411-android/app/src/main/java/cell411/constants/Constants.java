package cell411.constants;

/**
 * Created by Sachin on 15-01-2016.
 */
public class Constants {
  public static final int SUCCESS_RESULT = 0;
  public static final int FAILURE_RESULT = 1;
  public static final String PACKAGE_NAME = "com.wiselysoft.connected";
  public static final String RECEIVER = PACKAGE_NAME + ".RECEIVER";
  public static final String RESULT_DATA_ERROR = PACKAGE_NAME + ".RESULT_DATA_KEY";
  public static final String LOCATION_DATA_EXTRA = PACKAGE_NAME + ".LOCATION_DATA_EXTRA";
  public static final String LOCATION_DATA_LAT = PACKAGE_NAME + ".LAT";
  public static final String LOCATION_DATA_LNG = PACKAGE_NAME + ".LNG";
  public static final String RESULT_DATA_COUNTRY = PACKAGE_NAME + ".RESULT_DATA_COUNTRY";
  public static final String RESULT_DATA_CITY = PACKAGE_NAME + ".RESULT_DATA_CITY";
  public static final String RESULT_DATA_DETAIL_ADDRESS =
    PACKAGE_NAME + ".RESULT_DATA_DETAIL_ADDRESS";
  public static final String LOCATION_DATA_ID = PACKAGE_NAME + ".ID";
  public static final String LOCATION_DATA_REQUEST_ID = PACKAGE_NAME + ".REQUEST_ID";
  public static final String LOCATION_DATA_COUNTRY = PACKAGE_NAME + ".COUNTRY";
}