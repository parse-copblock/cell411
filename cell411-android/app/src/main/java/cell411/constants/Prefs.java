package cell411.constants;

/**
 * Created by Sachin on 07-06-2016.
 */
public class Prefs {
  public static final String IS_LOGGED_IN = "isLoggedIn";
  public static final String INCLUDE_SECURITY_GUARDS = "includeSecurityGuards";
  public static final String MESSAGE_VERSION_SHOWN = "message_version_shown";
  public static final String MESSAGE_VERSION_SHOWN_IGNORE = "message_version_shown_ignore";
  public static final String MESSAGE_VERSION_SHOWN_TIMESTAMP = "message_version_shown_timestamp";
  public static final String LAST_RECOMMENDED_VERSION_SHOWN_ALERT =
    "recommended_version_shown_alert";
  public static final String LAST_RECOMMENDED_VERSION_SHOWN_ALERT_TIMESTAMP =
    "recommended_version_shown_alert_timestamp";
  public static final String LAST_MAJOR_VERSION_SHOWN_ALERT = "major_version_shown_alert";
  public static final String LAST_MAJOR_VERSION_SHOWN_ALERT_TIMESTAMP =
    "major_version_shown_alert_timestamp";
  public static final String LAST_CURRENT_VERSION_SHOWN_ALERT = "current_version_shown_alert";
  public static final String LAST_CURRENT_VERSION_SHOWN_ALERT_TIMESTAMP =
    "current_version_shown_alert_timestamp";
}