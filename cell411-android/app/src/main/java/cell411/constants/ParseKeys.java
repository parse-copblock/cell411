package cell411.constants;

/**
 * Created by Sachin on 07-06-2016.
 */
public class ParseKeys {
  public static final String FIRST_NAME = "firstName";
  public static final String LAST_NAME = "lastName";
  public static final String MOBILE_NUMBER = "mobileNumber";
  public static final String PATROL_MODE = "PatrolMode";
  public static final String NEW_PUBLIC_CELL_ALERT = "newPublicCellAlert";
  public static final String BLOOD_TYPE = "bloodType";
  public static final String CREATED_BY = "createdBy";
  public static final String NAME = "name";
  public static final String USER = "user";
  public static final String PRIVILEGE = "privilege";
  public static final String EMAIL = "email";
  public static final String CLIENT_FIRM_ID = "clientFirmId";
  public static final String RIDE_REQUEST_ALERT = "rideRequestAlert";
  //  public static final String PHONE_VERIFIED = "phoneVerified";
  public static final String LOCATION = "location";
  public static final String CLASS_CELL = "Cell";
  //public static final String DEFAULT_CELL_FRIENDS = "Friends";
  //public static final String DEFAULT_CELL_FAMILY = "Family";
  //public static final String DEFAULT_CELL_NEIGHBOURHOOD_WATCH = "Neighborhood Watch";
  //public static final String DEFAULT_CELL_CO_WORKERS = "Coworkers";
  //public static final String DEFAULT_CELL_SCHOOL_FRIENDS = "School Friends";
  public static final String PRIVILEGE_BANNED = "BANNED";
  public static final String PRIVILEGE_SUSPENDED = "SUSPENDED";
}
