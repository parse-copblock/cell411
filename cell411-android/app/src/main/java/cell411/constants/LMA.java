package cell411.constants;

/**
 * Created by Sachin on 06-09-2016.
 */
public class LMA {
  //public static String BASE_URL_ALERT = "http://lma.nanodynamix.co.za/api/pde/id/13";
  //public static String BASE_URL_REGISTRATION = "http://lma.nanodynamix.co.za/api/pde/id/14";
  public static String BASE_URL_ALERT = "http://api.affinityhealth.co.za/api/ier/panic";
  public static String BASE_URL_PHOTO_ALERT = "http://api.affinityhealth.co.za/api/ier/photoAlert";
  public static String BASE_URL_REGISTRATION = "http://api.affinityhealth.co.za/api/ier/register";
  public static String BASE_URL_UPDATE_PROFILE =
    "http://api.affinityhealth.co.za/api/ier/updateProfile/";
  public static String PARAM_KEY = "NDX-LMA-KEY";
  public static String KEY_VALUE_ALERT = "606c526eb4d3383fd312a61945e30022fcb4766d";
  public static String KEY_VALUE_REGISTRATION = "e69105423fe55f41f003a1655ba4db26ac7e1f4e";
  public static String PARAM_UNIQUE_ID = "Unique_ID";
  public static String PARAM_USER_ID = "User_ID";
  public static String PARAM_FIRST_NAME = "First_Name";
  public static String PARAM_SUR_NAME = "Surname";
  public static String PARAM_CONTACT_MOBILE = "Contact_Mobile";
  public static String PARAM_CONTACT_EMAIL = "Contact_Email";
  public static String PARAM_ALERT_TYPE = "Alert_Type";
  public static String PARAM_GEO_LOCATION = "Geo_Location";
  public static String PARAM_NOTE = "note";
  public static String PARAM_IMAGE_URL = "image_url";
  public static String PARAM_EMERGENCY_CONTACT = "emergency_contact";
  public static String PARAM_EMERGENCY_NUMBER = "emergency_number";
  public static String PARAM_BLOOD_GROUP = "blood_group";
  public static String PARAM_ALLERGIES = "allergies";
  public static String PARAM_CONDITIONS = "conditions";
}