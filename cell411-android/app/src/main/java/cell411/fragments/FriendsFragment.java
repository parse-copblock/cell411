package cell411.fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.parse.ParseException;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseRelation;
import com.parse.ParseUser;
import com.parse.callback.FindCallback;
import com.parse.callback.SaveCallback;
import com.safearx.cell411.R;

import java.util.ArrayList;
import java.util.List;

import cell411.Singleton;
import cell411.activity.MainActivity;
import cell411.activity.ProfileImageActivity;
import cell411.activity.UserActivity;
import cell411.models.Cell;
import cell411.models.User;
import cell411.utils.LogEvent;
import cell411.widgets.CircularImageView;
import cell411.widgets.Spinner;

/**
 * Created by Sachin on 18-04-2016.
 */
public class FriendsFragment extends Fragment {
  private static FriendListAdapter adapterFriends;
  private static ArrayList<User> friendList;
  private static RelativeLayout rlNoFriends;
  private final String TAG = "FriendsFragment";
  private Spinner spinner;
  private RecyclerView recyclerView;

  public static void refreshFriendsList() {
    if(adapterFriends != null) {
      adapterFriends.arrayList.clear();
      if(Singleton.INSTANCE.getFriends() != null) {
        friendList = (ArrayList<User>) Singleton.INSTANCE.getFriends().clone();
      }
      adapterFriends.arrayList.addAll(friendList);
      adapterFriends.notifyDataSetChanged();
    }
    if(friendList != null && rlNoFriends != null && friendList.size() == 0) {
      rlNoFriends.setVisibility(View.VISIBLE);
    } else if(rlNoFriends != null) {
      rlNoFriends.setVisibility(View.GONE);
    }
  }

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
                           Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.fragment_friends, container, false);
    spinner = (Spinner) view.findViewById(R.id.spinner);
    recyclerView = (RecyclerView) view.findViewById(R.id.rv_friends);
    // use this setting to improve performance if you know that changes
    // in content do not change the layout size of the RecyclerView
    recyclerView.setHasFixedSize(true);
    // use a linear layout manager
    recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
    rlNoFriends = (RelativeLayout) view.findViewById(R.id.rl_no_friends);
    if(Singleton.INSTANCE.getFriends() != null) {
      friendList = (ArrayList<User>) Singleton.INSTANCE.getFriends().clone();
    }
    if(friendList == null) {
      spinner.setVisibility(View.VISIBLE);
      ParseUser user = ParseUser.getCurrentUser();
      ParseRelation relation = user.getRelation("friends");
      ParseQuery query = relation.getQuery();
      query.findInBackground(new FindCallback<ParseObject>() {
        @Override
        public void done(List list, ParseException e) {
          spinner.setVisibility(View.GONE);
          if(e == null) {
            friendList = new ArrayList<>();
            if(list != null) {
              for(int i = 0; i < list.size(); i++) {
                ParseUser user = (ParseUser) list.get(i);
                String email = null;
                if(user.getEmail() != null && !user.getEmail().isEmpty()) {
                  email = user.getEmail();
                } else {
                  email = user.getUsername();
                }
                friendList.add(
                  new User(email, (String) user.get("firstName"), (String) user.get("lastName"),
                    user));
              }
            }
            Singleton.INSTANCE.setFriends((ArrayList<User>) friendList.clone());
            if(MainActivity.weakRef.get() != null && !MainActivity.weakRef.get().isFinishing()) {
              adapterFriends = new FriendListAdapter(friendList);
              recyclerView.setAdapter(adapterFriends);
            }
            if(friendList.size() == 0) {
              rlNoFriends.setVisibility(View.VISIBLE);
            }
          } else {
            if(MainActivity.weakRef.get() != null && !MainActivity.weakRef.get().isFinishing()) {
              MainActivity.INSTANCE.getApplicationContext();
              Singleton.sendToast(e);
            }
          }
        }
      });
    } else {
      if(friendList.size() == 0) {
        rlNoFriends.setVisibility(View.VISIBLE);
      }
      adapterFriends = new FriendListAdapter(friendList);
      recyclerView.setAdapter(adapterFriends);
    }
        /*fab = (FloatingActionButton) view.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showAddFriendDialog();
            }
        });*/
    return view;
  }

  @Override
  public void onResume() {
    super.onResume();
    refreshFriendsList();
  }

  public class FriendListAdapter extends RecyclerView.Adapter<FriendListAdapter.ViewHolder> {
    private final View.OnClickListener mOnClickListener = new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        int position = recyclerView.getChildAdapterPosition(v);
        if(((User) friendList.get(position)).user.getInt("isDeleted") == 1) {
          return;
        }
        ParseGeoPoint parseGeoPoint =
          (ParseGeoPoint) ((User) friendList.get(position)).user.get("location");
        Intent intentUser = new Intent(getActivity(), UserActivity.class);
        intentUser.putExtra("userId", friendList.get(position).user.getObjectId());
        intentUser.putExtra("imageName", "" + friendList.get(position).user.get("imageName"));
        intentUser.putExtra("firstName", friendList.get(position).firstName);
        intentUser.putExtra("lastName", friendList.get(position).lastName);
        intentUser.putExtra("username", friendList.get(position).user.getUsername());
        intentUser.putExtra("email", friendList.get(position).user.getEmail());
        if(parseGeoPoint != null) {
          intentUser.putExtra("lat", parseGeoPoint.getLatitude());
          intentUser.putExtra("lng", parseGeoPoint.getLongitude());
        }
        startActivity(intentUser);
      }
    };
    public ArrayList<User> arrayList;
    private final View.OnLongClickListener mOnLongClickListener = new View.OnLongClickListener() {
      @Override
      public boolean onLongClick(View v) {
        // Delete this friend
        showDeleteFriendDialog(
          ((User) adapterFriends.arrayList.get(recyclerView.getChildAdapterPosition(v))).user,
          recyclerView.getChildAdapterPosition(v));
        return false;
      }
    };

    // Provide a suitable constructor (depends on the kind of dataset)
    public FriendListAdapter(ArrayList<User> arrayList) {
      this.arrayList = arrayList;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
      // create a new view
      View v =
        LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_friend, parent, false);
      // set the view's size, margins, paddings and layout parameters
      ViewHolder vh = new ViewHolder(v, viewType);
      v.setOnClickListener(mOnClickListener);
      v.setOnLongClickListener(mOnLongClickListener);
      return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int position) {
      // - get element from your dataset at this position
      // - replace the contents of the view with that element
      final User user = (User) arrayList.get(position);
      viewHolder.txtUserName.setText(user.firstName + " " + user.lastName);
      if(user.user.getInt("isDeleted") == 1) {
        viewHolder.txtUserName.setTextColor(
          getResources().getColor(R.color.text_disabled_hint_icon));
        viewHolder.imgUser.setImageResource(R.drawable.ic_placeholder_user);
        viewHolder.imgUser.setOnClickListener(null);
      } else {
        viewHolder.txtUserName.setTextColor(getResources().getColor(R.color.text_primary));
        Singleton.INSTANCE.setImage(viewHolder.imgUser,
          user.user.getObjectId() + user.user.get("imageName"), user.email);
        viewHolder.imgUser.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View view) {
            String email = user.user.getEmail();
            if(email == null || email.isEmpty()) {
              email = user.user.getUsername();
            }
            Intent profileImageIntent = new Intent(getActivity(), ProfileImageActivity.class);
            profileImageIntent.putExtra("userId", user.user.getObjectId());
            profileImageIntent.putExtra("imageName", "" + user.user.get("imageName"));
            profileImageIntent.putExtra("email", email);
            startActivity(profileImageIntent);
          }
        });
      }
    }

    // Return the size of your data set (invoked by the layout manager)
    @Override
    public int getItemCount() {
      return arrayList.size();
    }

    private void showDeleteFriendDialog(final ParseUser user, final int position) {
      AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
      alert.setMessage(getString(R.string.dialog_msg_unfriend,
        user.get("firstName") + " " + user.get("lastName")));
      alert.setNegativeButton(R.string.dialog_btn_cancel, new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialogInterface, int i) {
        }
      });
      alert.setPositiveButton(R.string.dialog_btn_ok, new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialogInterface, int i) {
          ParseUser currentUser = ParseUser.getCurrentUser();
          ParseRelation<ParseUser> relation = currentUser.getRelation("friends");
          relation.remove(user);
          currentUser.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
              if(e == null) {
                User user2 = adapterFriends.arrayList.get(position);
                Singleton.INSTANCE.removeFriendFromList(user2);
                adapterFriends.arrayList.remove(position);
                adapterFriends.notifyDataSetChanged();
                if(Singleton.INSTANCE.getCells() == null) {
                  ParseQuery<ParseObject> cellQuery = ParseQuery.getQuery("Cell");
                  cellQuery.whereEqualTo("createdBy", ParseUser.getCurrentUser());
                  cellQuery.whereNotEqualTo("type", 5);
                  cellQuery.include("members");
                  cellQuery.findInBackground(new FindCallback<ParseObject>() {
                    @Override
                    public void done(List<ParseObject> parseObjects, ParseException e) {
                      if(e == null) {
                        ArrayList<Cell> cellList = new ArrayList<>();
                        if(parseObjects != null) {
                          for(int i = 0; i < parseObjects.size(); i++) {
                            ParseObject cell = parseObjects.get(i);
                            Cell cl = new Cell((String) cell.get("name"), cell);
                            if(cell.getList("members") != null) {
                              LogEvent.Log("Cell",
                                "There are some members in" + " " + cell.get("name") + " " +
                                  "newPrivateCell");
                              List<ParseUser> members = cell.getList("members");
                              // Check if deleted friend exists in this newPrivateCell, then delete it
                              boolean friendFoundInCell = false;
                              for(int j = 0; j < members.size(); j++) {
                                if(members.get(j).getObjectId().equals(user.getObjectId())) {
                                  members.remove(j);
                                  --j;
                                  friendFoundInCell = true;
                                }
                              }
                              if(friendFoundInCell) {
                                cell.put("members", members);
                                cell.saveEventually();
                              }
                              cl.members.addAll(members);
                            } else {
                              LogEvent.Log("Cell", "There are no members in " + cell.get("name") +
                                                     " newPrivateCell");
                            }
                            cellList.add(cl);
                          }
                          Singleton.INSTANCE.setCells((ArrayList<Cell>) cellList.clone());
                        }
                      } else {
                        if(MainActivity.weakRef.get() != null &&
                             !MainActivity.weakRef.get().isFinishing()) {
                          MainActivity.INSTANCE.getApplicationContext();
                          Singleton.sendToast(e);
                        }
                      }
                    }
                  });
                } else {
                  ArrayList<Cell> cellList =
                    (ArrayList<Cell>) Singleton.INSTANCE.getCells().clone();
                  // iterate through cells
                  for(int i = 0; i < cellList.size(); i++) {
                    Cell cell = cellList.get(i);
                    if(cell.members != null && cell.members.size() != 0) {
                      // Check if deleted friend exists in this newPrivateCell, then delete it
                      boolean friendFoundInCell = false;
                      for(int j = 0; j < cell.members.size(); j++) {
                        if(cell.members.get(j).getObjectId().equals(user.getObjectId())) {
                          cell.members.remove(j);
                          --j;
                          friendFoundInCell = true;
                        }
                      }
                      if(friendFoundInCell) {
                        cell.cell.put("members", cell.members);
                        cell.cell.saveEventually();
                      }
                    }
                  }
                  Singleton.INSTANCE.setCells((ArrayList<Cell>) cellList.clone());
                }
              } else {
                if(MainActivity.weakRef.get() != null &&
                     !MainActivity.weakRef.get().isFinishing()) {
                  MainActivity.INSTANCE.getApplicationContext();
                  Singleton.sendToast(e);
                }
              }
            }
          });
        }
      });
      AlertDialog dialog = alert.create();
      dialog.show();
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public class ViewHolder extends RecyclerView.ViewHolder {
      // each data item is just a string in this case
      private TextView txtUserName;
      private CircularImageView imgUser;

      public ViewHolder(View view, int type) {
        super(view);
        txtUserName = (TextView) view.findViewById(R.id.txt_user_name);
        imgUser = (CircularImageView) view.findViewById(R.id.img_user);
      }
    }
  }
}