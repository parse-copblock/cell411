package cell411.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.safearx.cell411.R;

/**
 * Created by Sachin on 15-04-2016.
 */
public class GalleryFragment extends Fragment {
  private int imageId;
  private String title;
  private String desc;
  private int index;

  public static GalleryFragment newInstance(int imageId, String title, String desc, int index) {
    GalleryFragment fragment = new GalleryFragment();
    Bundle args = new Bundle();
    args.putInt("imageId", imageId);
    args.putString("title", title);
    args.putString("desc", desc);
    args.putInt("index", index);
    fragment.setArguments(args);
    return fragment;
  }

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    imageId = getArguments().getInt("imageId");
    title = getArguments().getString("title");
    desc = getArguments().getString("desc");
    index = getArguments().getInt("index");
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
                           Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.fragment_gallery, container, false);
    ImageView imgGallery = (ImageView) view.findViewById(R.id.img_gallery);
    TextView txtTitle = (TextView) view.findViewById(R.id.lbl_title);
    TextView txtDesc = (TextView) view.findViewById(R.id.lbl_description);
    if(index > 0) { // Cell 411 or gallery page other than 1
      view.findViewById(R.id.avatar).setVisibility(View.GONE);
      view.findViewById(R.id.view_avatag_bg).setVisibility(View.GONE);
    } else {
      view.findViewById(R.id.avatar).setVisibility(View.VISIBLE);
      view.findViewById(R.id.view_avatag_bg).setVisibility(View.VISIBLE);
    }
    imgGallery.setImageResource(imageId);
    txtTitle.setText(title);
    txtDesc.setText(desc);
    return view;
  }
}