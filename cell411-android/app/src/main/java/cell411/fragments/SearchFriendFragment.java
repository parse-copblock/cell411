package cell411.fragments;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ProgressBar;
import android.widget.SearchView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.parse.ParseException;
import com.parse.ParseGeoPoint;
import com.parse.ParseQuery;
import com.parse.ParseRelation;
import com.parse.ParseUser;
import com.safearx.cell411.R;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CancellationException;

import cell411.Singleton;
import cell411.activity.MainActivity;
import cell411.activity.ProfileImageActivity;
import cell411.activity.UserActivity;
import cell411.methods.AddFriendModules;
import cell411.models.Footer;
import cell411.models.User;
import cell411.utils.LogEvent;
import cell411.widgets.CircularImageView;
import cell411.widgets.Spinner;

/**
 * Created by Sachin on 18-04-2016.
 */
public class SearchFriendFragment extends Fragment
  implements SearchView.OnQueryTextListener, SearchView.OnCloseListener {
  private final int LIMIT_PER_PAGE = 10;
  private String TAG = "SearchFriendFragment";
  private Spinner spinner;
  private RecyclerView recyclerView;
  private LinearLayoutManager linearLayoutManager;
  private UserListAdapter adapterUsers;
  private ArrayList<Object> usersList;
  private ParseQuery<ParseUser> mainQuery;
  private ParseQuery<ParseUser> queryExcludeUsers;
  private String searchString = null;
  private int page = 0;
  private boolean noMoreData = false;
  private boolean apiCallInProgress = false;
  private int pastVisibleItems, visibleItemCount, totalItemCount;
  private Handler handler = new Handler();
  private boolean loading = true;
  private ApiCall apiCall;

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
                           Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.fragment_search_friend, container, false);
    spinner = (Spinner) view.findViewById(R.id.spinner);
    recyclerView = (RecyclerView) view.findViewById(R.id.rv_users);
    // use this setting to improve performance if you know that changes
    // in content do not change the layout size of the RecyclerView
    recyclerView.setHasFixedSize(true);
    // use a linear layout manager
    linearLayoutManager = new LinearLayoutManager(getActivity());
    recyclerView.setLayoutManager(linearLayoutManager);
    SearchView searchView = (SearchView) view.findViewById(R.id.searchview_user);
    int searchPlateId =
      searchView.getContext().getResources().getIdentifier("android:id/search_plate", null, null);
    View v = searchView.findViewById(searchPlateId);
    //v.setBackgroundColor(Color.parseColor("#33c3e4ff"));
    int id =
      searchView.getContext().getResources().getIdentifier("android:id/search_src_text", null,
        null);
    //TextView textView = (TextView) searchView.findViewById(id);
    //textView.setTextColor(Color.BLACK);
    //textView.setTextColor(Color.parseColor(getString(R.color.text_primary)));
    searchView.setIconifiedByDefault(false);
    searchView.setOnQueryTextListener(this);
    searchView.setOnCloseListener(this);
    ParseRelation relationFriends = ParseUser.getCurrentUser().getRelation("friends");
    ParseQuery queryFriends = relationFriends.getQuery();
    ParseRelation relationSpammedBy = ParseUser.getCurrentUser().getRelation("spammedBy");
    ParseQuery querySpammedBy = relationSpammedBy.getQuery();
    List<ParseQuery<ParseUser>> queries = new ArrayList<>();
    queries.add(queryFriends);
    queries.add(querySpammedBy);
    queryExcludeUsers = ParseQuery.or(queries);
    return view;
  }

  @Override
  public boolean onQueryTextSubmit(String s) {
    hideSoftKeyboard();
    //searchString = s;
    //refreshListAndSearchUser();
    return false;
  }

  @Override
  public boolean onQueryTextChange(String newText) {
    if(newText.isEmpty()) {
      searchString = null;
      refreshListAndSearchUser();
    } else {
      searchString = newText;
      refreshListAndSearchUser();
    }
    return false;
  }

  @Override
  public boolean onClose() {
    hideSoftKeyboard();
    return false;
  }

  /**
   * Used to hide the keyboard
   */
  public void hideSoftKeyboard() {
    if(getActivity().getCurrentFocus() != null) {
      InputMethodManager inputMethodManager =
        (InputMethodManager) getActivity().getSystemService(MainActivity.INPUT_METHOD_SERVICE);
      inputMethodManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(),
        0);
    }
  }

  private void refreshListAndSearchUser() {
    page = 0;
    noMoreData = false;
    loading = true;
    if(adapterUsers != null) {
      adapterUsers.arrayList.clear();
      adapterUsers.notifyDataSetChanged();
      usersList.clear();
    }
    retrieveUsers();
  }

  private void retrieveUsers() {
    if(usersList == null) {
      usersList = new ArrayList<>();
    }
    if(searchString == null || searchString.trim().isEmpty()) {
      LogEvent.Log(TAG, "if::searchString == null && adapterUsers != null");
      if(mainQuery != null) {
        mainQuery.cancel();
        LogEvent.Log(TAG, "mainQuery.cancel() called");
      }
      if(adapterUsers != null) {
        adapterUsers.arrayList.clear();
        adapterUsers.notifyDataSetChanged();
        usersList.clear();
      }
      spinner.setVisibility(View.GONE);
      hideSoftKeyboard();
      return;
    }
    if(usersList.size() == 0) {
      spinner.setVisibility(View.VISIBLE);
    }
    apiCallInProgress = true;
    LogEvent.Log(TAG, "searchString: " + searchString);
    searchString = searchString.trim();
    LogEvent.Log(TAG, "searchString after trim: " + searchString);
    String[] searchStringArr = searchString.split(" ");
    String searchRejex = "^(" + searchStringArr[0];
    for(int i = 1; i < searchStringArr.length; i++) {
      searchRejex += "|" + searchStringArr[i];
    }
    searchRejex += ")";
    LogEvent.Log(TAG, "searchRejex: " + searchRejex);
    ParseQuery<ParseUser> userQueryByFirstName = ParseUser.getQuery();
    userQueryByFirstName.whereMatches("firstName", searchRejex, "i");
    ParseQuery<ParseUser> userQueryByLastName = ParseUser.getQuery();
    userQueryByLastName.whereMatches("lastName", searchRejex, "i");
    ParseQuery<ParseUser> userQueryByUsername = null;
    ParseQuery<ParseUser> userQueryByEmail = null;
    if(searchString.contains("@") && searchStringArr.length == 1) {
      userQueryByUsername = ParseUser.getQuery();
      userQueryByUsername.whereEqualTo("username", searchString.toLowerCase());
      userQueryByEmail = ParseUser.getQuery();
      userQueryByEmail.whereEqualTo("email", searchString.toLowerCase());
    }
    ParseQuery<ParseUser> userQueryByPhone = null;
    String searchStringWithoutSpace = searchString.replaceAll(" ", "");
    String searchStringWithoutSpecialCharacter = searchString.replaceAll("[\\D]+", "");
    if(searchStringWithoutSpace.length() == searchStringWithoutSpecialCharacter.length()) {
      userQueryByPhone = ParseUser.getQuery();
      userQueryByPhone.whereEqualTo("mobileNumber", searchStringWithoutSpace);
    }
    List<ParseQuery<ParseUser>> queries = new ArrayList<>();
    queries.add(userQueryByFirstName);
    queries.add(userQueryByLastName);
    if(userQueryByUsername != null) {
      queries.add(userQueryByUsername);
    }
    if(userQueryByEmail != null) {
      queries.add(userQueryByEmail);
    }
    if(userQueryByPhone != null) {
      queries.add(userQueryByPhone);
    }
        /*if (mainQuery != null) {
            mainQuery.cancel();
            LogEvent.Log(TAG, "mainQuery.cancel() called");
        }*/
    mainQuery = ParseQuery.or(queries);
    mainQuery.whereDoesNotMatchKeyInQuery("objectId", "objectId", queryExcludeUsers);
    mainQuery.whereNotEqualTo("objectId", ParseUser.getCurrentUser().getObjectId());
    mainQuery.whereNear("location", ParseUser.getCurrentUser().getParseGeoPoint("location"));
    mainQuery.whereNotEqualTo("isDeleted", 1);
    mainQuery.setLimit(LIMIT_PER_PAGE);
    mainQuery.setSkip(LIMIT_PER_PAGE * page);
    if(apiCall != null && !apiCall.isCancelled()) {
      LogEvent.Log(TAG, "apiCall.cancel(true) called");
      apiCall.cancel(true);
    }
    apiCall = new ApiCall();
    apiCall.execute();
  }

  // The Search will give wrong result only when the previous call completes later than the
  // current call where no results were found for the query
  private boolean isResultBelongToCurrentQuery(List<ParseUser> parseUsers) {
    if(searchString == null || searchString.isEmpty()) {
      // There is nothing in the searchString that means the result does belong to an old query
      return false;
    }
    if(parseUsers == null || parseUsers.size() == 0) {
      return true;
    }
    for(int index = 0; index < parseUsers.size(); index++) {
      ParseUser parseUser = parseUsers.get(index);
      String firstName = parseUser.getString("firstName");
      String lastName = parseUser.getString("lastName");
      String username = parseUser.getString("username");
      String email = parseUser.getString("email");
      String mobileNumber = parseUser.getString("mobileNumber");
      LogEvent.Log(TAG, "index: " + index);
      LogEvent.Log(TAG, "current searchString: " + searchString);
      String[] searchStringArr = searchString.split(" ");
      String searchRejex = searchStringArr[0];
      for(int i = 1; i < searchStringArr.length; i++) {
        searchRejex += "|" + searchStringArr[i];
      }
      LogEvent.Log(TAG, "current searchRejex: " + searchRejex);
      if(searchString.contains("@") && searchStringArr.length == 1) {
        if(!username.equalsIgnoreCase(searchString) && !email.equalsIgnoreCase(searchString)) {
          LogEvent.Log(TAG, "Email does not match");
          return false;
        } else {
          LogEvent.Log(TAG, "Email match found");
          continue;
        }
      } else {
        LogEvent.Log(TAG, "No Email match");
      }
      String searchStringWithoutSpace = searchString.replaceAll(" ", "");
      String searchStringWithoutSpecialCharacter = searchString.replaceAll("[\\D]+", "");
      LogEvent.Log(TAG, "current searchStringWithoutSpace: " + searchStringWithoutSpace);
      LogEvent.Log(TAG,
        "current searchStringWithoutSpecialCharacter: " + searchStringWithoutSpecialCharacter);
      if(searchStringWithoutSpace.equals(searchStringWithoutSpecialCharacter)) {
        if(!mobileNumber.equalsIgnoreCase(searchStringWithoutSpace)) {
          LogEvent.Log(TAG, "Phone does not match");
          return false;
        } else {
          LogEvent.Log(TAG, "Phone match found");
          continue;
        }
      } else {
        LogEvent.Log(TAG, "No Phone match");
      }
      if(!firstName.toLowerCase().matches(searchRejex.toLowerCase()) &&
           !lastName.toLowerCase().matches(searchRejex.toLowerCase())) {
        LogEvent.Log(TAG, "firstName: " + firstName);
        LogEvent.Log(TAG, "lastName: " + lastName);
        LogEvent.Log(TAG, "Name does not match");
        return false;
      } else {
        LogEvent.Log(TAG, "firstName: " + firstName);
        LogEvent.Log(TAG, "lastName: " + lastName);
        LogEvent.Log(TAG, "Name match found");
        continue;
      }
    }
    return true;
  }

  private void setListScrollListener() {
    // register scroll listener to check when user reaches
    // bottom of the list so that we can load more items
    //recyclerView.clearOnScrollListeners();
    recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
      @Override
      public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        if(dy > 0) { //check for scroll down
          visibleItemCount = linearLayoutManager.getChildCount();
          totalItemCount = linearLayoutManager.getItemCount();
          pastVisibleItems = linearLayoutManager.findFirstVisibleItemPosition();
          //if (loading) {
          if((visibleItemCount + pastVisibleItems) >= totalItemCount) {
            loading = false;
            LogEvent.Log(TAG, "Last Item Wow !");
            //Do pagination.. i.e. fetch new data
            handler.postDelayed(new Runnable() {
              @Override
              public void run() {
                if(!noMoreData) {
                  loadMoreItems();
                }
              }
            }, 300);
          } else {
            LogEvent.Log(TAG, "Not Last Item");
          }
          //}
        }
      }
    });
    LogEvent.Log(TAG, "Scroll Listener Added");
  }

  /**
   * This method will load more items when user reaches bottom of the list.
   * It will set the text at the footer to "No more videos to load" when
   * there will be no more videos available.
   */
  private void loadMoreItems() {
    LogEvent.Log(TAG, "loadMoreItems invoked");
    if(!apiCallInProgress) {
      if(!noMoreData) {
        retrieveUsers();
      } else {
        ((Footer) adapterUsers.arrayList.get(adapterUsers.arrayList.size() - 1)).info =
          getString(R.string.no_more_users_to_load);
        ((Footer) adapterUsers.arrayList.get(adapterUsers.arrayList.size() - 1)).spinnerVisibility =
          false;
        adapterUsers.notifyItemChanged(adapterUsers.arrayList.size() - 1);
      }
    }
  }

  private class ApiCall extends AsyncTask<Void, Void, Void> {
    @Override
    protected Void doInBackground(Void... voids) {
      try {
        final List<ParseUser> parseUsers = mainQuery.find();
        LogEvent.Log(TAG, "mainQuery.find() success");
        if(getActivity() == null) {
          return null;
        }
        getActivity().runOnUiThread(new Runnable() {
          @Override
          public void run() {
            spinner.setVisibility(View.GONE);
            apiCallInProgress = false;
            ArrayList<Object> userList = new ArrayList<>();
            if(parseUsers != null) {
              for(int i = 0; i < parseUsers.size(); i++) {
                ParseUser parseUser = parseUsers.get(i);
                String email = null;
                if(parseUser.getEmail() != null && !parseUser.getEmail().isEmpty()) {
                  email = parseUser.getEmail();
                } else {
                  email = parseUser.getUsername();
                }
                userList.add(new User(email, (String) parseUser.get("firstName"),
                  (String) parseUser.get("lastName"), parseUser));
              }
              if(page == 0 && adapterUsers != null) {
                adapterUsers.arrayList.clear();
                adapterUsers.notifyDataSetChanged();
                usersList.clear();
              }
              if(parseUsers.size() < 10) {
                noMoreData = true;
                LogEvent.Log(TAG, "No more data");
              } else {
                page++;
                LogEvent.Log(TAG, "More data available");
              }
            }
            usersList.addAll(userList);
            if(MainActivity.weakRef.get() != null && !MainActivity.weakRef.get().isFinishing() &&
                 getActivity() != null) {
              if(adapterUsers == null) {
                if(userList.size() > 0) {
                  // Add footer to the list that will display a progressbar
                  userList.add(new Footer(getString(R.string.load_more), true));
                } else {
                  // No any cells found
                  LogEvent.Log(TAG, "No any users found, display empty note");
                }
                adapterUsers = new UserListAdapter(userList);
                getActivity().runOnUiThread(new Runnable() {
                  @Override
                  public void run() {
                  }
                });
                recyclerView.setAdapter(adapterUsers);
                setListScrollListener();
              } else {
                if(adapterUsers.arrayList.size() == 0 && userList.size() > 0) {
                  // Add footer to the list that will display a progressbar
                  userList.add(new Footer(getString(R.string.load_more), true));
                  adapterUsers.arrayList.addAll(adapterUsers.arrayList.size(), userList);
                } else if(userList.size() > 0) {
                  adapterUsers.arrayList.addAll(adapterUsers.arrayList.size() - 1, userList);
                } else {
                  // No any cells found
                  LogEvent.Log(TAG, "No any users found, display empty note");
                }
              }
              adapterUsers.notifyDataSetChanged();
              if(noMoreData) {
                //recyclerView.clearOnScrollListeners();
                LogEvent.Log(TAG, "Scroll Listener Removed");
                if(adapterUsers != null && adapterUsers.arrayList.size() > 0) {
                  ((Footer) adapterUsers.arrayList.get(adapterUsers.arrayList.size() - 1)).
                    info = getString(R.string.no_more_users_to_load);
                  ((Footer) adapterUsers.arrayList.get(adapterUsers.arrayList.size() - 1)).
                    spinnerVisibility = false;
                  adapterUsers.notifyItemChanged(adapterUsers.arrayList.size() - 1);
                }
                LogEvent.Log(TAG, "if::noMoreData && userList.size() > 0 clause");
              } else {
                LogEvent.Log(TAG, "else::noMoreData && userList.size() > 0 clause");
              }
            } else {
              LogEvent.Log(TAG, "else clause");
            }
          }
        });
      } catch(final ParseException e) {
        e.printStackTrace();
        if(getActivity() != null && MainActivity.weakRef.get() != null &&
             !MainActivity.weakRef.get().isFinishing()) {
          getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
              MainActivity.INSTANCE.getApplicationContext();
              Singleton.sendToast(e);
            }
          });
        }
      } catch(CancellationException e) {
        e.printStackTrace();
      }
      return null;
    }
  }

  public class UserListAdapter extends RecyclerView.Adapter<UserListAdapter.ViewHolder> {
    private final int VIEW_TYPE_USER = 0;
    private final int VIEW_TYPE_FOOTER = 1;
    private final View.OnClickListener mOnClickListener = new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        int position = recyclerView.getChildAdapterPosition(v);
        if(usersList.get(position) instanceof User) {
          ParseGeoPoint parseGeoPoint =
            (ParseGeoPoint) ((User) usersList.get(position)).user.get("location");
          Intent intentUser = new Intent(getActivity(), UserActivity.class);
          intentUser.putExtra("userId", ((User) usersList.get(position)).user.getObjectId());
          intentUser.putExtra("imageName",
            "" + ((User) usersList.get(position)).user.get("imageName"));
          intentUser.putExtra("firstName", ((User) usersList.get(position)).firstName);
          intentUser.putExtra("lastName", ((User) usersList.get(position)).lastName);
          intentUser.putExtra("username", ((User) usersList.get(position)).user.getUsername());
          intentUser.putExtra("email", ((User) usersList.get(position)).user.getEmail());
          if(parseGeoPoint != null) {
            intentUser.putExtra("lat", parseGeoPoint.getLatitude());
            intentUser.putExtra("lng", parseGeoPoint.getLongitude());
          }
          startActivity(intentUser);
        }
      }
    };
    public ArrayList<Object> arrayList;

    // Provide a suitable constructor (depends on the kind of data set)
    public UserListAdapter(ArrayList<Object> arrayList) {
      this.arrayList = arrayList;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public UserListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
      // create a new view
      View v;
      if(viewType == VIEW_TYPE_USER) {
        v = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_user, parent, false);
      } else {
        v = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_footer, parent, false);
      }
      ViewHolder vh = new ViewHolder(v, viewType);
      if(viewType == VIEW_TYPE_USER) {
        v.setOnClickListener(mOnClickListener);
      }
      return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int position) {
      // - get element from your data set at this position
      // - replace the contents of the view with that element
      if(getItemViewType(position) == VIEW_TYPE_USER) {
        final User user = (User) arrayList.get(position);
        viewHolder.txtUserName.setText(user.firstName + " " + user.lastName);
        String email = user.user.getEmail();
        if(email == null || email.isEmpty()) {
          email = user.user.getUsername();
        }
        Singleton.INSTANCE.setImage(viewHolder.imgUser,
          user.user.getObjectId() + user.user.get("imageName"), email);
        viewHolder.imgUser.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View view) {
            String email = user.user.getEmail();
            if(email == null || email.isEmpty()) {
              email = user.user.getUsername();
            }
            Intent profileImageIntent = new Intent(getActivity(), ProfileImageActivity.class);
            profileImageIntent.putExtra("userId", "" + user.user.getObjectId());
            profileImageIntent.putExtra("imageName", "" + user.user.get("imageName"));
            profileImageIntent.putExtra("email", email);
            startActivity(profileImageIntent);
          }
        });
        if(user.isRequestSent) { // friend request is already sent to this user
          viewHolder.txtBtnAddFriend.setText(R.string.resend);
          viewHolder.txtBtnAddFriend.setBackgroundResource(R.drawable.bg_cell_join);
        } else if(user.friendAddInProgress) { // app is trying to send friend request to this user
          viewHolder.txtBtnAddFriend.setText(R.string.sending);
          viewHolder.txtBtnAddFriend.setBackgroundResource(R.drawable.bg_cell_join_processing);
        } else { // user can send friend request to this user
          viewHolder.txtBtnAddFriend.setText(R.string.add);
          viewHolder.txtBtnAddFriend.setBackgroundResource(R.drawable.bg_cell_join);
        }
        viewHolder.txtBtnAddFriend.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View view) {
            if(!user.friendAddInProgress) {
              sendFriendRequest(position, user.user.getObjectId());
            }
          }
        });
      } else {
        final Footer footer = (Footer) arrayList.get(position);
        viewHolder.txtInfo.setText(footer.info);
        if(footer.spinnerVisibility) {
          viewHolder.pb.setVisibility(View.VISIBLE);
        } else {
          viewHolder.pb.setVisibility(View.GONE);
        }
      }
    }

    // Return the size of your data set (invoked by the layout manager)
    @Override
    public int getItemCount() {
      return arrayList.size();
    }

    @Override
    public int getItemViewType(int position) {
      super.getItemViewType(position);
      if(arrayList.get(position) instanceof User) {
        return VIEW_TYPE_USER;
      } else {
        return VIEW_TYPE_FOOTER;
      }
    }

    private void sendFriendRequest(final int index, final String userObjectId) {
      ((User) arrayList.get(index)).isRequestSent = false;
      ((User) arrayList.get(index)).friendAddInProgress = true;
      notifyDataSetChanged();
      AddFriendModules.addFriend(getActivity(), ((User) arrayList.get(index)).user.getUsername(),
        new AddFriendModules.OnSendFriendRequestListener() {
          @Override
          public void onRequestSent() {
            for(int i = 0; i < arrayList.size(); i++) {
              if(((User) arrayList.get(i)).user.getObjectId().equals(userObjectId)) {
                ((User) arrayList.get(i)).isRequestSent = true;
                ((User) arrayList.get(i)).friendAddInProgress = false;
                notifyDataSetChanged();
                break;
              }
            }
            //((User) arrayList.get(index)).isRequestSent = true;
            //((User) arrayList.get(index)).friendAddInProgress = false;
            //notifyDataSetChanged();
          }

          @Override
          public void onRequestFailed() {
            for(int i = 0; i < arrayList.size(); i++) {
              if(((User) arrayList.get(i)).user.getObjectId().equals(userObjectId)) {
                ((User) arrayList.get(i)).isRequestSent = false;
                ((User) arrayList.get(i)).friendAddInProgress = false;
                notifyDataSetChanged();
                break;
              }
            }
            //((User) arrayList.get(index)).isRequestSent = false;
            //((User) arrayList.get(index)).friendAddInProgress = false;
            //notifyDataSetChanged();
          }
        });
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public class ViewHolder extends RecyclerView.ViewHolder {
      // each data item is just a string in this case
      private TextView txtUserName;
      private CircularImageView imgUser;
      private TextView txtBtnAddFriend;
      private TextView txtInfo;
      private ProgressBar pb;

      public ViewHolder(View view, int type) {
        super(view);
        if(type == VIEW_TYPE_USER) {
          txtUserName = (TextView) view.findViewById(R.id.txt_user_name);
          imgUser = (CircularImageView) view.findViewById(R.id.img_user);
          txtBtnAddFriend = (TextView) view.findViewById(R.id.txt_btn_add_friend);
        } else if(type == VIEW_TYPE_FOOTER) {
          txtInfo = (TextView) view.findViewById(R.id.txt_info);
          pb = (ProgressBar) view.findViewById(R.id.pb);
        }
      }
    }
  }
}
