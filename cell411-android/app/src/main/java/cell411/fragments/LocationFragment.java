package cell411.fragments;

import android.Manifest;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Looper;
import android.provider.Settings;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;

import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.snackbar.Snackbar;
import com.parse.ParseGeoPoint;
import com.parse.ParseUser;
import com.safearx.cell411.BuildConfig;
import com.safearx.cell411.R;

import cell411.Singleton;
import cell411.interfaces.CurrentLocationFromLocationUpdatesCallBack;
import cell411.interfaces.LastLocationCallBack;
import cell411.interfaces.LocationFailToRetrieveCallBack;
import cell411.receivers.GpsReceiver;
import cell411.utils.LogEvent;

/**
 * Created by Sachin on 24-05-2018.
 */
public class LocationFragment extends Fragment {
  /**
   * Constant used in the location settings dialog.
   */
  protected static final int REQUEST_CHECK_SETTINGS = 0x1;
  private static final String TAG = LocationFragment.class.getSimpleName();
  private static final int REQUEST_PERMISSIONS_REQUEST_CODE = 34;
  /**
   * Stores the types of location services the client is interested in using. Used for checking
   * settings to determine if the device has optimal location settings.
   */
  protected LocationSettingsRequest mLocationSettingsRequest;
  /**
   * Provides access to the Fused Location Provider API.
   */
  private FusedLocationProviderClient mFusedLocationClient;
  /**
   * Provides access to the Location Settings API.
   */
  private SettingsClient mSettingsClient;
  /**
   * Stores parameters for requests to the FusedLocationProviderApi.
   */
  private LocationRequest mLocationRequest;
  /**
   * Callback for Location events.
   */
  private LocationCallback mLocationCallback;
  /**
   * Represents a geographical location.
   */
  private Location mLastLocation;
  private View contentView;
  private SharedPreferences prefs;
  private LastLocationCallBack mLastLocationCallBack;
  private CurrentLocationFromLocationUpdatesCallBack mCurrentLocationFromLocationUpdatesCallBack;
  private LocationFailToRetrieveCallBack mLocationFailToRetrieveCallBack;
  private LastLocationCallBack.Feature mFeature;

  @Override
  public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    prefs = Singleton.INSTANCE.getAppPrefs();
    contentView = getActivity().findViewById(android.R.id.content);
    mFusedLocationClient = LocationServices.getFusedLocationProviderClient(getActivity());
    mSettingsClient = LocationServices.getSettingsClient(getActivity());
    // Kick off the process of building the LocationCallback, LocationRequest, and
    // LocationSettingsRequest objects.
    createLocationRequest();
    buildLocationSettingsRequest();
  }

  /**
   * Return the current state of the permissions needed.
   */
  public boolean checkPermissions() {
    int permissionState =
      ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION);
        /*int permissionState = ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.ACCESS_COARSE_LOCATION);*/
    return permissionState == PackageManager.PERMISSION_GRANTED;
  }

  private void startLocationPermissionRequest() {
        /*ActivityCompat.requestPermissions(getActivity(),
                new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
                REQUEST_PERMISSIONS_REQUEST_CODE);*/
    requestPermissions(new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
      REQUEST_PERMISSIONS_REQUEST_CODE);
  }

  public void requestPermissions() {
    boolean shouldProvideRationale =
      ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
        Manifest.permission.ACCESS_FINE_LOCATION);
    // Provide an additional rationale to the user. This would happen if the user denied the
    // request previously, but didn't check the "Don't ask again" checkbox.
    if(shouldProvideRationale) {
      LogEvent.Log(TAG, "Displaying permission rationale to provide additional context.");
      startLocationPermissionRequest();
            /*showSnackbar(R.string.permission_rationale, android.R.string.ok,
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            // Request permission
                            startLocationPermissionRequest();
                        }
                    });*/
    } else {
      LogEvent.Log(TAG, "Requesting permission");
      // Request permission. It's possible this can be auto answered if device policy
      // sets the permission in a given state or the user denied the permission
      // previously and checked "Never ask again".
      startLocationPermissionRequest();
    }
  }

  /**
   * Shows a {@link Snackbar}.
   *
   * @param mainTextStringId The id for the string resource for the Snackbar text.
   * @param actionStringId   The text of the action item.
   * @param listener         The listener associated with the Snackbar action.
   */
  private void showSnackbar(final int mainTextStringId, final int actionStringId,
                            View.OnClickListener listener) {
    Snackbar.make(contentView, getString(mainTextStringId), Snackbar.LENGTH_LONG).setAction(
      getString(actionStringId), listener).show();
  }

  /**
   * Callback received when a permissions request has been completed.
   */
  @Override
  public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                         @NonNull int[] grantResults) {
    LogEvent.Log(TAG, "onRequestPermissionResult");
    if(requestCode == REQUEST_PERMISSIONS_REQUEST_CODE) {
      if(grantResults.length <= 0) {
        // If user interaction was interrupted, the permission request is cancelled and you
        // receive empty arrays.
        LogEvent.Log(TAG, "User interaction was cancelled.");
      } else if(grantResults[0] == PackageManager.PERMISSION_GRANTED) {
        // Permission granted.
        //checkLocationSettings();
        getLastLocation(null, null, null);
      } else {
        // Permission denied.
        if(mLocationFailToRetrieveCallBack != null) {
          mLocationFailToRetrieveCallBack.onLocationFailToRetrieve(
            LocationFailToRetrieveCallBack.FailureReason.PERMISSION_DENIED, mFeature);
        }
        // Notify the user via a SnackBar that they have rejected a core permission for the
        // app, which makes the Activity useless. In a real app, core permissions would
        // typically be best requested during a welcome-screen flow.
        // Additionally, it is important to remember that a permission might have been
        // rejected without asking the user for permission (device policy or "Never ask
        // again" prompts). Therefore, a user interface affordance is typically implemented
        // when permissions are denied. Otherwise, your app could appear unresponsive to
        // touches or interactions which have required permissions.
        showSnackbar(R.string.permission_denied_explanation, R.string.settings,
          new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              // Build intent that displays the App settings screen.
              Intent intent = new Intent();
              intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
              Uri uri = Uri.fromParts("package", BuildConfig.APPLICATION_ID, null);
              intent.setData(uri);
              intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
              startActivity(intent);
            }
          });
      }
    }
  }

  protected void createLocationRequest() {
    mLocationRequest = new LocationRequest();
    mLocationRequest.setInterval(10000);
    mLocationRequest.setFastestInterval(5000);
    if(prefs.getBoolean("LocationAccuracy", true)) {
      mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    } else {
      mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
    }
  }

  /**
   * Uses a {@link com.google.android.gms.location.LocationSettingsRequest.Builder} to build
   * a {@link com.google.android.gms.location.LocationSettingsRequest} that is used for checking
   * if a device has the needed location settings.
   */
  protected void buildLocationSettingsRequest() {
    LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
    builder.addLocationRequest(mLocationRequest);
    mLocationSettingsRequest = builder.build();
  }

  /**
   * Creates a callback for receiving location events.
   */
  private void createLocationCallback() {
    mLocationCallback = new LocationCallback() {
      @Override
      public void onLocationResult(LocationResult locationResult) {
        super.onLocationResult(locationResult);
        LogEvent.Log(TAG, "onLocationResult() invoked");
        mLastLocation = locationResult.getLastLocation();
        if(mLastLocation != null) {
          Singleton.INSTANCE.setLatitude(mLastLocation.getLatitude());
          Singleton.INSTANCE.setLongitude(mLastLocation.getLongitude());
          if(ParseUser.getCurrentUser() != null) {
            ParseGeoPoint currentUserLocation =
              new ParseGeoPoint(mLastLocation.getLatitude(), mLastLocation.getLongitude());
            ParseUser.getCurrentUser().put("location", currentUserLocation);
            ParseUser.getCurrentUser().saveEventually();
            mCurrentLocationFromLocationUpdatesCallBack
              .onCurrentLocationRetrievedFromLocationUpdates(
                mLastLocation, mFeature);
          }
          stopLocationUpdates();
        } else {
          LogEvent.Log(TAG, "mLastLocation is null");
        }
      }
    };
  }

  @SuppressWarnings("MissingPermission")
  public void getLastLocation(final LastLocationCallBack lastLocationCallBack,
                              final LocationFailToRetrieveCallBack locationFailToRetrieveCallBack,
                              final LastLocationCallBack.Feature feature) {
    LogEvent.Log(TAG, "getLastLocation() invoked");
    if(lastLocationCallBack != null && locationFailToRetrieveCallBack != null && feature != null) {
      mLastLocationCallBack = lastLocationCallBack;
      mLocationFailToRetrieveCallBack = locationFailToRetrieveCallBack;
      mFeature = feature;
    }
    if(!checkPermissions()) {
      requestPermissions();
    } else {
      mFusedLocationClient.getLastLocation().addOnSuccessListener(getActivity(),
        new OnSuccessListener<Location>() {
          @Override
          public void onSuccess(Location location) {
            LogEvent.Log(TAG, "getLastLocation onSuccess listener invoked");
            if(location != null) {
              ParseGeoPoint currentUserLocation =
                new ParseGeoPoint(location.getLatitude(), location.getLongitude());
              ParseUser.getCurrentUser().put("location", currentUserLocation);
              ParseUser.getCurrentUser().saveEventually();
              LogEvent.Log(TAG, location.getLatitude() + "," + location.getLongitude());
              Singleton.INSTANCE.setLatitude(location.getLatitude());
              Singleton.INSTANCE.setLongitude(location.getLongitude());
              mLastLocationCallBack.onLastLocationRetrieved(location, mFeature);
            } else {
              mLocationFailToRetrieveCallBack.onLocationFailToRetrieve(
                LocationFailToRetrieveCallBack.FailureReason.UNKNOWN, mFeature);
            }
          }
        });
    }
  }

  @SuppressWarnings("MissingPermission")
  public void getCurrentLocationFromLocationUpdates(
    CurrentLocationFromLocationUpdatesCallBack currentLocationFromLocationUpdatesCallBack,
    LastLocationCallBack.Feature feature) {
    LogEvent.Log(TAG, "getCurrentLocationFromLocationUpdates() invoked");
    if(currentLocationFromLocationUpdatesCallBack != null && feature != null) {
      mCurrentLocationFromLocationUpdatesCallBack = currentLocationFromLocationUpdatesCallBack;
      mFeature = feature;
    }
    mSettingsClient.checkLocationSettings(mLocationSettingsRequest).addOnSuccessListener(
      getActivity(), new OnSuccessListener<LocationSettingsResponse>() {
        @Override
        public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
          LogEvent.Log(TAG, "All location settings are satisfied.");
          createLocationCallback();
          mFusedLocationClient.requestLocationUpdates(mLocationRequest, mLocationCallback,
            Looper.myLooper());
        }
      }).addOnFailureListener(getActivity(), new OnFailureListener() {
      @Override
      public void onFailure(@NonNull Exception e) {
        int statusCode = ((ApiException) e).getStatusCode();
        switch(statusCode) {
          case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
            LogEvent.Log(TAG, "Location settings are not satisfied. Attempting to upgrade " +
                                "location settings ");
            try {
              // Show the dialog by calling startResolutionForResult(), and check the
              // result in onActivityResult().
              ResolvableApiException rae = (ResolvableApiException) e;
              //rae.startResolutionForResult(getActivity(), REQUEST_CHECK_SETTINGS);
              startIntentSenderForResult(rae.getResolution().getIntentSender(),
                REQUEST_CHECK_SETTINGS, null, 0, 0, 0, null);
            } catch(IntentSender.SendIntentException sie) {
              LogEvent.Log(TAG, "PendingIntent unable to execute request.");
            }
            break;
          case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
            String errorMessage =
              "Location settings are inadequate, and cannot be " + "fixed here. Fix in Settings.";
            LogEvent.Log(TAG, errorMessage);
            getActivity();
            Singleton.sendToast(errorMessage);
        }
      }
    });
  }

  /**
   * Removes location updates from the FusedLocationApi.
   */
  public void stopLocationUpdates() {
    LogEvent.Log(TAG, "stopLocationUpdates() invoked");
    if(mFusedLocationClient == null || mLocationCallback == null) {
      return;
    }
    // It is a good practice to remove location requests when the activity is in a paused or
    // stopped state. Doing so helps battery performance and is especially
    // recommended in applications that request frequent location updates.
    if(getActivity() != null) {
      mFusedLocationClient.removeLocationUpdates(mLocationCallback).addOnCompleteListener(
        Singleton.INSTANCE.getCurrentActivity(), new OnCompleteListener<Void>() {
          @Override
          public void onComplete(@NonNull Task<Void> task) {
            LogEvent.Log(TAG, "LocationUpdate stopped");
          }
        });
    }
  }

  @Override
  public void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);
    LogEvent.Log(TAG, "onActivityResult() invoked");
    LogEvent.Log(TAG, "requestCode: " + requestCode + " resultCode: " + resultCode);
    if(requestCode == REQUEST_CHECK_SETTINGS) { // Check for the integer request code
      // originally supplied to startResolutionForResult()
      if(resultCode == getActivity().RESULT_OK) {
        LogEvent.Log(TAG, "User agreed to make required location settings changes.");
        //getLastLocation();
        getCurrentLocationFromLocationUpdates(null, mFeature);
      } else {
        if(mLocationFailToRetrieveCallBack != null) {
          mLocationFailToRetrieveCallBack.onLocationFailToRetrieve(
            LocationFailToRetrieveCallBack.FailureReason.SETTINGS_REQUEST_DENIED, mFeature);
        }
        LogEvent.Log(TAG, "User chose not to make required location settings changes.");
      }
    }
  }

  public void startLocationService() {
    LogEvent.Log(TAG, "startLocationService() invoked");
    SharedPreferences prefs = Singleton.INSTANCE.getAppPrefs();
    if(prefs.getBoolean("LocationUpdate", true)) {
      LogEvent.Log(TAG, "LocationUpdate is enabled");
      if(checkPermissions()) { // Check if permission is available
        LogEvent.Log(TAG, "Permissions available to access location");
        boolean somethingChanged = false;
        if(prefs.getBoolean("patrolModeDisabledByApp", false)) {
          somethingChanged = true;
          ParseUser.getCurrentUser().put("PatrolMode", 1);
        }
        if(prefs.getBoolean("newPublicCellAlertDisabledByApp", false)) {
          somethingChanged = true;
          ParseUser.getCurrentUser().put("newPublicCellAlert", 1);
        }
        if(prefs.getBoolean("rideRequestAlertDisabledByApp", false)) {
          somethingChanged = true;
          ParseUser.getCurrentUser().put("rideRequestAlert", true);
        }
        if(somethingChanged) {
          ParseUser.getCurrentUser().saveEventually();
        }
        if(getActivity() != null) {
          // Start the location service to update user's current location indefinitely
          //Intent intentLocationService = new Intent(getActivity(), LocationJobService.class);
          //getActivity().startService(intentLocationService);
          GpsReceiver.dispatchJob(getActivity());
        }
      } else {
        LogEvent.Log(TAG, "Permissions not available to access location");
      }
    } else {
      LogEvent.Log(TAG, "LocationUpdate is disabled");
      if(getActivity() != null) {
        // Stop the location service
        //Intent intentLocationService = new Intent(getActivity(), LocationJobService.class);
        //getActivity().stopService(intentLocationService);
        GpsReceiver.cancelJob(getActivity());
      }
    }
  }
}