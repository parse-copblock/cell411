package cell411.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.callback.FindCallback;
import com.safearx.cell411.R;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import cell411.Singleton;
import cell411.activity.MainActivity;
import cell411.activity.ProfileImageActivity;
import cell411.activity.UserActivity;
import cell411.methods.AddFriendModules;
import cell411.models.Footer;
import cell411.models.FriendRequest;
import cell411.utils.LogEvent;
import cell411.widgets.CircularImageView;
import cell411.widgets.Spinner;

/**
 * Created by Sachin on 18-04-2016.
 */
public class FriendRequestFragment extends Fragment {
  private final int LIMIT_PER_PAGE = 10;
  private String TAG = "FriendRequestFragment";
  private Spinner spinner;
  private RecyclerView recyclerView;
  private LinearLayoutManager linearLayoutManager;
  private RequestListAdapter requestListAdapter;
  private ArrayList<Object> requestArrayList;
  private int page = 0;
  private boolean noMoreData = false;
  private boolean apiCallInProgress = false;
  private int pastVisibleItems, visibleItemCount, totalItemCount;
  private Handler handler = new Handler();
  private boolean loading = true;
  private ParseQuery<ParseObject> parseQuery4FriendRequests;
  private TextView txtNoRequests;

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
                           Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.fragment_friend_requests, container, false);
    spinner = (Spinner) view.findViewById(R.id.spinner);
    recyclerView = (RecyclerView) view.findViewById(R.id.rv_requests);
    txtNoRequests = (TextView) view.findViewById(R.id.txt_no_requests);
    // use this setting to improve performance if you know that changes
    // in content do not change the layout size of the RecyclerView
    recyclerView.setHasFixedSize(true);
    // use a linear layout manager
    linearLayoutManager = new LinearLayoutManager(getActivity());
    recyclerView.setLayoutManager(linearLayoutManager);
    // Friend Request or Friend Invites received from users
    ArrayList<String> entryForList = new ArrayList<>();
    entryForList.add("FR");
    entryForList.add("FI");
    ArrayList<String> arrTo = new ArrayList<>();
    arrTo.add(ParseUser.getCurrentUser().getUsername());
    if(ParseUser.getCurrentUser().getEmail() != null) {
      arrTo.add(ParseUser.getCurrentUser().getEmail());
    }
    if(ParseUser.getCurrentUser().get("mobileNumber") != null &&
         ParseUser.getCurrentUser().getBoolean("phoneVerified")) {
      arrTo.add(ParseUser.getCurrentUser().getString("mobileNumber"));
    }
    parseQuery4FriendRequests = ParseQuery.getQuery("Cell411Alert");
    parseQuery4FriendRequests.whereContainedIn("to", arrTo);
    parseQuery4FriendRequests.whereContainedIn("entryFor", entryForList);
    parseQuery4FriendRequests.whereEqualTo("status", "PENDING");
    // Required to check if the alert is issued by the current user
    parseQuery4FriendRequests.include("issuedBy");
    // Sort the result as the most recent first
    parseQuery4FriendRequests.orderByDescending("createdAt");
    retrieveFriendRequests();
    return view;
  }

  private void retrieveFriendRequests() {
    if(requestArrayList == null) {
      requestArrayList = new ArrayList<>();
    }
    if(requestArrayList.size() == 0) {
      spinner.setVisibility(View.VISIBLE);
    }
    apiCallInProgress = true;
    parseQuery4FriendRequests.setLimit(LIMIT_PER_PAGE);
    parseQuery4FriendRequests.setSkip(LIMIT_PER_PAGE * page);
    parseQuery4FriendRequests.findInBackground(new FindCallback<ParseObject>() {
      @Override
      public void done(List<ParseObject> parseObjects, ParseException e) {
        spinner.setVisibility(View.GONE);
        apiCallInProgress = false;
        if(e == null) {
          if(!isAdded()) {
            return;
          }
          ArrayList<Object> requestList = new ArrayList<>();
          GregorianCalendar cal = new GregorianCalendar();
          GregorianCalendar calCurrentTime = new GregorianCalendar();
          calCurrentTime.setTimeInMillis(System.currentTimeMillis());
          calCurrentTime.set(Calendar.HOUR, 0);
          calCurrentTime.set(Calendar.MINUTE, 0);
          calCurrentTime.set(Calendar.SECOND, 0);
          calCurrentTime.set(Calendar.MILLISECOND, 0);
          if(parseObjects != null) {
            for(int i = 0; i < parseObjects.size(); i++) {
              ParseObject parseObject = parseObjects.get(i);
              ParseUser issuedBy = parseObject.getParseUser("issuedBy");
              if(issuedBy.getInt("isDeleted") == 1) {
                continue;
              }
              boolean requestAlreadyExists = false;
              for(int j = 0; j < requestArrayList.size(); j++) {
                if(issuedBy.getObjectId().equals(
                  ((FriendInviteOrRequest) requestArrayList.get(j)).getUserObjectId())) {
                  requestAlreadyExists = true;
                }
              }
              if(!requestAlreadyExists) {
                for(int j = 0; j < requestList.size(); j++) {
                  if(issuedBy.getObjectId().equals(
                    ((FriendInviteOrRequest) requestList.get(j)).getUserObjectId())) {
                    requestAlreadyExists = true;
                  }
                }
              }
              if(requestAlreadyExists) {
                continue;
              }
              long millis = parseObject.getCreatedAt().getTime();
              String firstName = issuedBy.getString("firstName");
              String lastName = issuedBy.getString("lastName");
              String imageName = "" + issuedBy.get("imageName");
              double lat = issuedBy.getParseGeoPoint("location").getLatitude();
              double lng = issuedBy.getParseGeoPoint("location").getLongitude();
              cal.setTimeInMillis(millis);
              int year = cal.get(Calendar.YEAR);
              int month = cal.get(Calendar.MONTH) + 1;
              int day = cal.get(Calendar.DAY_OF_MONTH);
              int hour = cal.get(Calendar.HOUR);
              int minute = cal.get(Calendar.MINUTE);
              int ampm = cal.get(Calendar.AM_PM);
              String ap;
              if(ampm == 0) {
                ap = getString(R.string.am);
              } else {
                ap = getString(R.string.pm);
              }
              if(hour == 0) {
                hour = 12;
              }
              String hourMinute = "" + hour;
              if(minute > 0 && minute < 10) {
                hourMinute += ":0" + minute;
              } else if(minute > 9) {
                hourMinute += ":" + minute;
              }
              String time;
              if(cal.getTimeInMillis() < calCurrentTime.getTimeInMillis()) {
                // Check if the alert was issued yesterday
                time = month + "/" + day + "/" + year + " " + getString(R.string.at) + " " +
                         hourMinute + " " + ap;
              } else {
                time = getString(R.string.today_at) + " " + hourMinute + " " + ap;
              }
              LogEvent.Log(TAG, "imageName: " + imageName);
              LogEvent.Log(TAG, "issuedBy.getObjectId(): " + issuedBy.getObjectId());
              FriendInviteOrRequest friendInviteOrRequest =
                new FriendInviteOrRequest(firstName, lastName, issuedBy.getUsername(),
                  issuedBy.getEmail(), imageName, issuedBy.getObjectId(), time, parseObject,
                  lat, lng);
              requestList.add(friendInviteOrRequest);
            }
            if(parseObjects.size() < 10) {
              noMoreData = true;
              LogEvent.Log(TAG, "No more data");
            } else {
              page++;
              LogEvent.Log(TAG, "More data available");
            }
          }
          requestArrayList.addAll(requestList);
          if(requestArrayList.size() == 0) {
            txtNoRequests.setVisibility(View.VISIBLE);
          }
          if(MainActivity.weakRef.get() != null && !MainActivity.weakRef.get().isFinishing() &&
               getActivity() != null) {
            if(requestListAdapter == null) {
              if(requestList.size() > 0) {
                // Add footer to the list that will display a progressbar
                requestList.add(new Footer(getString(R.string.load_more), true));
              } else {
                // No any cells found
                LogEvent.Log(TAG, "No any cells found, display empty note");
              }
              requestListAdapter = new RequestListAdapter(requestList);
              recyclerView.setAdapter(requestListAdapter);
              setListScrollListener();
            } else {
              if(requestListAdapter.arrayList.size() == 0 && requestList.size() > 0) {
                // Add footer to the list that will display a progressbar
                requestList.add(new Footer(getString(R.string.load_more), true));
                requestListAdapter.arrayList.addAll(requestListAdapter.arrayList.size(),
                  requestList);
              } else if(requestList.size() > 0) {
                requestListAdapter.arrayList.addAll(requestListAdapter.arrayList.size() - 1,
                  requestList);
              } else {
                // No any cells found
                LogEvent.Log(TAG, "No any cells found, display empty note");
              }
            }
            requestListAdapter.notifyDataSetChanged();
            if(noMoreData && requestList.size() > 0) {
              //recyclerView.clearOnScrollListeners();
              LogEvent.Log(TAG, "Scroll Listener Removed");
              ((Footer) requestListAdapter.arrayList.get(requestListAdapter.arrayList.size() - 1)).
                info = getString(R.string.no_more_requests_to_load);
              ((Footer) requestListAdapter.arrayList.get(requestListAdapter.arrayList.size() - 1)).
                spinnerVisibility = false;
              requestListAdapter.notifyItemChanged(requestListAdapter.arrayList.size() - 1);
            }
          }
        } else {
          if(MainActivity.weakRef.get() != null && !MainActivity.weakRef.get().isFinishing()) {
            MainActivity.INSTANCE.getApplicationContext();
            Singleton.sendToast(e);
          }
        }
      }
    });
  }

  private void setListScrollListener() {
    // register scroll listener to check when user reaches
    // bottom of the list so that we can load more items
    //recyclerView.clearOnScrollListeners();
    recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
      @Override
      public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        if(dy > 0) { //check for scroll down
          visibleItemCount = linearLayoutManager.getChildCount();
          totalItemCount = linearLayoutManager.getItemCount();
          pastVisibleItems = linearLayoutManager.findFirstVisibleItemPosition();
          if(loading) {
            if((visibleItemCount + pastVisibleItems) >= totalItemCount) {
              loading = false;
              LogEvent.Log("...", "Last Item Wow !");
              //Do pagination.. i.e. fetch new data
              handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                  if(!noMoreData) {
                    loadMoreItems();
                  }
                }
              }, 300);
            }
          }
        }
      }
    });
    LogEvent.Log(TAG, "Scroll Listener Added");
  }

  /**
   * This method will load more items when user reaches bottom of the list.
   * It will set the text at the footer to "No more videos to load" when
   * there will be no more videos available.
   */
  private void loadMoreItems() {
    LogEvent.Log(TAG, "loadMoreItems invoked");
    if(!apiCallInProgress) {
      if(!noMoreData) {
        retrieveFriendRequests();
      } else {
        ((Footer) requestListAdapter.arrayList.get(requestListAdapter.arrayList.size() - 1)).info =
          getString(R.string.no_more_requests_to_load);
        ((Footer) requestListAdapter.arrayList.get(
          requestListAdapter.arrayList.size() - 1)).spinnerVisibility = false;
        requestListAdapter.notifyItemChanged(requestListAdapter.arrayList.size() - 1);
      }
    }
  }

  private static class FriendInviteOrRequest {
    public final int STATUS_INITIALIZED = 0;
    public final int STATUS_PROCESSING = 1;
    public final int STATUS_ACCEPTED = 2;
    public final int STATUS_REJECTED = 3;
    public int status;
    private String firstName;
    private String lastName;
    private String username;
    private String email;
    private String imageName;
    private String userObjectId;
    private String createdAt;
    private ParseObject parseObject;
    private double lat;
    private double lng;

    public FriendInviteOrRequest(String firstName, String lastName, String username, String email,
                                 String imageName, String userObjectId, String createdAt,
                                 ParseObject parseObject, double lat, double lng) {
      this.firstName = firstName;
      this.lastName = lastName;
      this.username = username;
      this.email = email;
      this.imageName = imageName;
      this.userObjectId = userObjectId;
      this.createdAt = createdAt;
      this.parseObject = parseObject;
      this.lat = lat;
      this.lng = lng;
      this.status = STATUS_INITIALIZED;
    }

    public String getFirstName() {
      return firstName;
    }

    public String getLastName() {
      return lastName;
    }

    public String getCreatedAt() {
      return createdAt;
    }

    public ParseObject getParseObject() {
      return parseObject;
    }

    public String getEmail() {
      return email;
    }

    public String getImageName() {
      return imageName;
    }

    public String getUserObjectId() {
      return userObjectId;
    }

    public String getUsername() {
      return username;
    }

    public double getLat() {
      return lat;
    }

    public double getLng() {
      return lng;
    }
  }

  public class RequestListAdapter extends RecyclerView.Adapter<RequestListAdapter.ViewHolder> {
    private final int VIEW_TYPE_USER = 0;
    private final int VIEW_TYPE_FOOTER = 1;
    private final View.OnClickListener mOnClickListener = new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        int position = recyclerView.getChildAdapterPosition(v);
        if(requestArrayList.get(position) instanceof FriendInviteOrRequest) {
          FriendInviteOrRequest friendInviteOrRequest =
            (FriendInviteOrRequest) requestArrayList.get(position);
          LogEvent.Log(TAG,
            "friendInviteOrRequest.getImageName(): " + friendInviteOrRequest.getImageName());
          LogEvent.Log(TAG, "friendInviteOrRequest.getUserObjectId(): " +
                              friendInviteOrRequest.getUserObjectId());
          Intent intentUser = new Intent(getActivity(), UserActivity.class);
          intentUser.putExtra("userId", friendInviteOrRequest.getUserObjectId());
          intentUser.putExtra("imageName", "" + friendInviteOrRequest.getImageName());
          intentUser.putExtra("firstName", friendInviteOrRequest.getFirstName());
          intentUser.putExtra("lastName", friendInviteOrRequest.getLastName());
          intentUser.putExtra("username", friendInviteOrRequest.getUsername());
          intentUser.putExtra("email", friendInviteOrRequest.getEmail());
          intentUser.putExtra("lat", friendInviteOrRequest.getLat());
          intentUser.putExtra("lng", friendInviteOrRequest.getLng());
          startActivity(intentUser);
        }
      }
    };
    public ArrayList<Object> arrayList;

    // Provide a suitable constructor (depends on the kind of data set)
    public RequestListAdapter(ArrayList<Object> arrayList) {
      this.arrayList = arrayList;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public RequestListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
      // create a new view
      View v;
      if(viewType == VIEW_TYPE_USER) {
        v = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_friend_request, parent,
          false);
      } else {
        v = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_footer, parent, false);
      }
      ViewHolder vh = new ViewHolder(v, viewType);
      if(viewType == VIEW_TYPE_USER) {
        v.setOnClickListener(mOnClickListener);
      }
      return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int position) {
      // - get element from your data set at this position
      // - replace the contents of the view with that element
      if(getItemViewType(position) == VIEW_TYPE_USER) {
        final FriendInviteOrRequest friendInviteOrRequest =
          (FriendInviteOrRequest) arrayList.get(position);
        viewHolder.txtUserName.setText(
          friendInviteOrRequest.firstName + " " + friendInviteOrRequest.lastName);
        String email = friendInviteOrRequest.getEmail();
        if(email == null || email.isEmpty()) {
          email = friendInviteOrRequest.getUsername();
        }
        LogEvent.Log(TAG,
          "friendInviteOrRequest.getImageName(): " + friendInviteOrRequest.getImageName());
        LogEvent.Log(TAG,
          "friendInviteOrRequest.getUserObjectId(): " + friendInviteOrRequest.getUserObjectId());
        Singleton.INSTANCE.setImage(viewHolder.imgUser,
          friendInviteOrRequest.getUserObjectId() + friendInviteOrRequest.getImageName(), email);
        viewHolder.imgUser.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View view) {
            String email = null;
            if(friendInviteOrRequest.getEmail() != null &&
                 !friendInviteOrRequest.getEmail().isEmpty()) {
              email = friendInviteOrRequest.getEmail();
            } else {
              email = friendInviteOrRequest.getUsername();
            }
            Intent profileImageIntent = new Intent(getActivity(), ProfileImageActivity.class);
            profileImageIntent.putExtra("userId", "" + friendInviteOrRequest.getUserObjectId());
            profileImageIntent.putExtra("imageName", friendInviteOrRequest.getImageName());
            profileImageIntent.putExtra("email", email);
            startActivity(profileImageIntent);
          }
        });
        viewHolder.txtBtnAccept.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View view) {
            if(friendInviteOrRequest.status != friendInviteOrRequest.STATUS_PROCESSING) {
              friendInviteOrRequest.status = friendInviteOrRequest.STATUS_PROCESSING;
              notifyDataSetChanged();
              FriendRequest friendRequest = new FriendRequest(
                friendInviteOrRequest.firstName + " " + friendInviteOrRequest.lastName,
                friendInviteOrRequest.userObjectId,
                friendInviteOrRequest.parseObject.getObjectId());
              AddFriendModules.approveFriendRequest(getActivity(), friendRequest,
                new AddFriendModules.OnApproveFriendRequestListener() {
                  @Override
                  public void onRequestApproved() {
                    friendInviteOrRequest.status = friendInviteOrRequest.STATUS_ACCEPTED;
                    notifyDataSetChanged();
                  }

                  @Override
                  public void onRequestFailed() {
                    friendInviteOrRequest.status = friendInviteOrRequest.STATUS_INITIALIZED;
                    notifyDataSetChanged();
                  }
                });
            }
          }
        });
        viewHolder.txtBtnReject.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View view) {
            if(friendInviteOrRequest.status != friendInviteOrRequest.STATUS_PROCESSING) {
              friendInviteOrRequest.status = friendInviteOrRequest.STATUS_PROCESSING;
              notifyDataSetChanged();
              FriendRequest friendRequest = new FriendRequest(
                friendInviteOrRequest.firstName + " " + friendInviteOrRequest.lastName,
                friendInviteOrRequest.userObjectId,
                friendInviteOrRequest.parseObject.getObjectId());
              AddFriendModules.rejectFriendRequest(getActivity(), friendRequest,
                new AddFriendModules.OnRejectFriendRequestListener() {
                  @Override
                  public void onRequestRejected() {
                    friendInviteOrRequest.status = friendInviteOrRequest.STATUS_REJECTED;
                    notifyDataSetChanged();
                  }

                  @Override
                  public void onRequestFailed() {
                    friendInviteOrRequest.status = friendInviteOrRequest.STATUS_INITIALIZED;
                    notifyDataSetChanged();
                  }
                });
            }
          }
        });
        if(friendInviteOrRequest.status == friendInviteOrRequest.STATUS_INITIALIZED) {
          viewHolder.txtWorking.setVisibility(View.GONE);
          viewHolder.txtBtnAccept.setVisibility(View.VISIBLE);
          viewHolder.txtBtnReject.setVisibility(View.VISIBLE);
        } else if(friendInviteOrRequest.status == friendInviteOrRequest.STATUS_PROCESSING) {
          viewHolder.txtWorking.setText(R.string.working);
          viewHolder.txtWorking.setVisibility(View.VISIBLE);
          viewHolder.txtBtnAccept.setVisibility(View.GONE);
          viewHolder.txtBtnReject.setVisibility(View.GONE);
        } else if(friendInviteOrRequest.status == friendInviteOrRequest.STATUS_ACCEPTED) {
          viewHolder.txtWorking.setText(R.string.accepted);
          viewHolder.txtWorking.setVisibility(View.VISIBLE);
          viewHolder.txtBtnAccept.setVisibility(View.GONE);
          viewHolder.txtBtnReject.setVisibility(View.GONE);
        } else {
          viewHolder.txtWorking.setText(R.string.rejected);
          viewHolder.txtWorking.setVisibility(View.VISIBLE);
          viewHolder.txtBtnAccept.setVisibility(View.GONE);
          viewHolder.txtBtnReject.setVisibility(View.GONE);
        }
      } else {
        final Footer footer = (Footer) arrayList.get(position);
        viewHolder.txtInfo.setText(footer.info);
        if(footer.spinnerVisibility) {
          viewHolder.pb.setVisibility(View.VISIBLE);
        } else {
          viewHolder.pb.setVisibility(View.GONE);
        }
      }
    }

    // Return the size of your data set (invoked by the layout manager)
    @Override
    public int getItemCount() {
      return arrayList.size();
    }

    @Override
    public int getItemViewType(int position) {
      super.getItemViewType(position);
      if(arrayList.get(position) instanceof FriendInviteOrRequest) {
        return VIEW_TYPE_USER;
      } else {
        return VIEW_TYPE_FOOTER;
      }
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public class ViewHolder extends RecyclerView.ViewHolder {
      // each data item is just a string in this case
      private TextView txtUserName;
      private CircularImageView imgUser;
      private TextView txtBtnAccept;
      private TextView txtBtnReject;
      private TextView txtWorking;
      private TextView txtInfo;
      private ProgressBar pb;

      public ViewHolder(View view, int type) {
        super(view);
        if(type == VIEW_TYPE_USER) {
          txtUserName = (TextView) view.findViewById(R.id.txt_user_name);
          imgUser = (CircularImageView) view.findViewById(R.id.img_user);
          txtBtnAccept = (TextView) view.findViewById(R.id.txt_btn_accept);
          txtBtnReject = (TextView) view.findViewById(R.id.txt_btn_reject);
          txtWorking = (TextView) view.findViewById(R.id.txt_working);
        } else if(type == VIEW_TYPE_FOOTER) {
          txtInfo = (TextView) view.findViewById(R.id.txt_info);
          pb = (ProgressBar) view.findViewById(R.id.pb);
        }
      }
    }
  }
}