package cell411.fragments;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.SearchView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParsePush;
import com.parse.ParseQuery;
import com.parse.ParseRelation;
import com.parse.ParseUser;
import com.parse.callback.FindCallback;
import com.parse.callback.SaveCallback;
import com.safearx.cell411.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import cell411.Singleton;
import cell411.SingletonImageFactory;
import cell411.activity.MainActivity;
import cell411.fragment_tabs.TabChatsFragment;
import cell411.models.Contact;
import cell411.models.Footer;
import cell411.utils.LogEvent;
import cell411.widgets.CircularImageView;

import static cell411.SingletonImageFactory.getGravatarImageUrl;

/**
 * Created by Sachin on 05-07-2017.
 */
public class ContactFragment extends Fragment
  implements SearchView.OnQueryTextListener, SearchView.OnCloseListener {
  public static final String BROADCAST_ACTION_CONTACT = "com.safearx.cell411.CONTACT_RECEIVER";
  private static final String TAG = "ContactFragment";
  private SearchView searchView;
  private RecyclerView recyclerView;
  private LinearLayoutManager linearLayoutManager;
  private ContactListAdapter contactListAdapter;
  private ArrayList<Object> contactList;
  private LinearLayout llLoading;
  private TextView txtLoading;

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                           @Nullable Bundle savedInstanceState) {
    super.onCreateView(inflater, container, savedInstanceState);
    View view = inflater.inflate(R.layout.fragment_contact, container, false);
    recyclerView = (RecyclerView) view.findViewById(R.id.rv_contact);
    // use this setting to improve performance if you know that changes
    // in content do not change the layout size of the RecyclerView
    recyclerView.setHasFixedSize(true);
    // use a linear layout manager
    linearLayoutManager = new LinearLayoutManager(getActivity());
    recyclerView.setLayoutManager(linearLayoutManager);
    llLoading = (LinearLayout) view.findViewById(R.id.ll_loading_text);
    txtLoading = (TextView) view.findViewById(R.id.txt_loading);
    searchView = (SearchView) view.findViewById(R.id.searchview);
    int id =
      searchView.getContext().getResources().getIdentifier("android:id/search_src_text", null,
        null);
    //TextView textView = (TextView) searchView.findViewById(id);
    //textView.setTextColor(Color.BLACK);
    searchView.setIconifiedByDefault(false);
    searchView.setOnQueryTextListener(this);
    searchView.setOnCloseListener(this);
    // Register the receiver for listening to new messages on the currently selected circle from
    // notification service
    IntentFilter mContactReceiverIntentFilter = new IntentFilter(BROADCAST_ACTION_CONTACT);
    ContactReceiver contactReceiver = new ContactReceiver();
    LocalBroadcastManager.getInstance(getActivity()).registerReceiver(contactReceiver,
      mContactReceiverIntentFilter);
    return view;
  }

  @Override
  public boolean onQueryTextSubmit(String query) {
    LogEvent.Log(TAG, query);
    hideSoftKeyboard();
    return false;
  }

  @Override
  public boolean onQueryTextChange(String newText) {
    LogEvent.Log(TAG, newText);
    if(contactList != null && contactList.size() > 0) {
      displayQueryData(newText);
    }
    return false;
  }

  @Override
  public boolean onClose() {
    LogEvent.Log(TAG, "onClose invoked");
    return false;
  }

  private void displayAllData() {
    contactListAdapter = new ContactListAdapter(contactList, null);
    recyclerView.setAdapter(contactListAdapter);
  }

  private void displayQueryData(String queryText) {
    if(queryText.equals("")) {
      displayAllData();
      return;
    }
    final ArrayList<Object> mList = new ArrayList<>();
    for(int i = 0; i < contactList.size(); i++) {
      if(((Contact) contactList.get(i)).displayName.
                                                     toLowerCase(Locale.getDefault())
           .contains(queryText.toLowerCase())) {
        mList.add(contactList.get(i));
      }
    }
    contactListAdapter = new ContactListAdapter(mList, queryText);
    recyclerView.setAdapter(contactListAdapter);
  }

  /**
   * Used to hide the keyboard
   */
  public void hideSoftKeyboard() {
    if(getActivity().getCurrentFocus() != null) {
      InputMethodManager inputMethodManager =
        (InputMethodManager) getActivity().getSystemService(getActivity().INPUT_METHOD_SERVICE);
      inputMethodManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(),
        0);
    }
  }

  public class ContactReceiver extends BroadcastReceiver {
    public ContactReceiver() {
      super();
    }

    @Override
    public void onReceive(Context context, Intent intent) {
      LogEvent.Log(TAG, "onReceive invoked..");
      contactList = (ArrayList<Object>) intent.getSerializableExtra("contactList");
      LogEvent.Log(TAG, "contactList: " + contactList.size());
      if(contactList.size() > 0) {
        searchView.setVisibility(View.VISIBLE);
        llLoading.setVisibility(View.GONE);
        contactListAdapter = new ContactListAdapter(contactList, null);
        recyclerView.setAdapter(contactListAdapter);
      } else {
        if(getActivity() != null && ContactFragment.this != null && isAdded()) {
          txtLoading.setText(getString(R.string.no_contacts));
        } else {
          android.os.Handler handler = new android.os.Handler();
          handler.postDelayed(new Runnable() {
            @Override
            public void run() {
              if(getActivity() != null && ContactFragment.this != null && isAdded()) {
                txtLoading.setText(getString(R.string.no_contacts));
              }
            }
          }, 1000);
        }
      }
      if(Singleton.INSTANCE.getCurrentActivity() != null &&
           Singleton.INSTANCE.getCurrentActivity() instanceof MainActivity) {
        LogEvent.Log(TAG, "inside first if");
        if(Singleton.INSTANCE.getCurrentFragment() != null &&
             Singleton.INSTANCE.getCurrentFragment() instanceof TabChatsFragment) {
          LogEvent.Log(TAG, "inside second if");
          ((TabChatsFragment) Singleton.INSTANCE.getCurrentFragment()).refreshChatRoomsList();
        } else {
          LogEvent.Log(TAG, "inside second else");
        }
      } else {
        LogEvent.Log(TAG, "inside first else");
      }
    }
  }

  public class ContactListAdapter extends RecyclerView.Adapter<ContactListAdapter.ViewHolder> {
    private final int VIEW_TYPE_CONTACT = 0;
    private final int VIEW_TYPE_FOOTER = 1;
    private final View.OnClickListener mOnClickListener = new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        int position = recyclerView.getChildAdapterPosition(v);
      }
    };
    public ArrayList<Object> arrayList;
    private String query;

    // Provide a suitable constructor (depends on the kind of data set)
    public ContactListAdapter(ArrayList<Object> arrayList, String query) {
      this.arrayList = arrayList;
      this.query = query;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ContactListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
      // create a new view
      View v;
      if(viewType == VIEW_TYPE_CONTACT) {
        v = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_contact, parent, false);
      } else {
        v = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_footer, parent, false);
      }
      ViewHolder vh = new ViewHolder(v, viewType);
      if(viewType == VIEW_TYPE_CONTACT) {
        v.setOnClickListener(mOnClickListener);
      }
      return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int position) {
      // - get element from your data set at this position
      // - replace the contents of the view with that element
      if(getItemViewType(position) == VIEW_TYPE_CONTACT) {
        final Contact contact = (Contact) arrayList.get(position);
        if(query == null) {
          viewHolder.txtDisplayName.setText(contact.displayName);
        } else {
          String name = contact.displayName;
          //                    int index = name.indexOf(query, 0);
          //                    String preStr = name.substring(0, index);
          //                    String str = "<b>" + name.substring(index, index + query.length()) + "</b>";
          //                    String postStr = name.substring(index + query.length(), name.length());
          //                    membersViewHolder.txtListItem.setText(Html.fromHtml(preStr + str + postStr));
          name = name.replaceAll(query, "<b>" + query + "</b>");
          viewHolder.txtDisplayName.setText(Html.fromHtml(name));
        }
        viewHolder.imgUser.setImageResource(R.drawable.ic_placeholder_user);
        if(contact.email != null) {
          if(query == null) {
            viewHolder.txtEmail.setText(contact.email);
          } else {
            String name = contact.email;
            name = name.replaceAll(query, "<b>" + query + "</b>");
            viewHolder.txtEmail.setText(Html.fromHtml(name));
          }
          final ViewHolder finalHolder = viewHolder;
          SingletonImageFactory.getImageLoader().loadImage(
            getGravatarImageUrl(contact.email),
            new ImageLoadingListener() {
              @Override
              public void onLoadingStarted(String s, View view) {
              }

              @Override
              public void onLoadingFailed(String s, View view, FailReason failReason) {
                finalHolder.imgUser.setImageResource(R.drawable.ic_placeholder_user);
              }

              @Override
              public void onLoadingComplete(String s, View view, Bitmap bitmap) {
                finalHolder.imgUser.setImageBitmap(Singleton.getCroppedBitmap(bitmap));
              }

              @Override
              public void onLoadingCancelled(String s, View view) {
              }
            });
        } else {
          if(query == null) {
            viewHolder.txtEmail.setText(contact.phone);
          } else {
            String name = contact.phone;
            name = name.replaceAll(query, "<b>" + query + "</b>");
            viewHolder.txtEmail.setText(Html.fromHtml(name));
          }
          //contactViewHolder.imgUser.setImageResource(R.drawable.logo);
        }
        if(contact.status == Contact.Status.INITIALIZING) {
          viewHolder.txtOption.setVisibility(View.GONE);
        } else {
          viewHolder.txtOption.setVisibility(View.VISIBLE);
          if(contact.status == Contact.Status.FRIEND_REQUEST_PENDING) {
            viewHolder.txtOption.setText(R.string.resend);
            if(contact.inProcessing) {
              viewHolder.txtOption.setBackgroundResource(
                R.drawable.bg_add_invite_resend_reinvite_pressed);
            } else {
              viewHolder.txtOption.setBackgroundResource(R.drawable.bg_add_invite_resend_reinvite);
            }
          } else if(contact.status == Contact.Status.USER_EXIST) {
            viewHolder.txtOption.setText(R.string.add);
            if(contact.inProcessing) {
              viewHolder.txtOption.setBackgroundResource(
                R.drawable.bg_add_invite_resend_reinvite_pressed);
            } else {
              viewHolder.txtOption.setBackgroundResource(R.drawable.bg_add_invite_resend_reinvite);
            }
          }
        }
        final ViewHolder holder = viewHolder;
        viewHolder.txtOption.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View view) {
            if(!contact.inProcessing) {
              // Send friend request here
              contact.inProcessing = true;
              holder.txtOption.setBackgroundResource(
                R.drawable.bg_add_invite_resend_reinvite_pressed);
              sendFriendRequest(contact);
            }
          }
        });
      } else {
        final Footer footer = (Footer) arrayList.get(position);
        viewHolder.txtInfo.setText(footer.info);
        if(footer.spinnerVisibility) {
          viewHolder.pb.setVisibility(View.VISIBLE);
        } else {
          viewHolder.pb.setVisibility(View.GONE);
        }
      }
    }

    // Return the size of your data set (invoked by the layout manager)
    @Override
    public int getItemCount() {
      return arrayList.size();
    }

    @Override
    public int getItemViewType(int position) {
      super.getItemViewType(position);
      if(arrayList.get(position) instanceof Contact) {
        return VIEW_TYPE_CONTACT;
      } else {
        return VIEW_TYPE_FOOTER;
      }
    }

    private void sendFriendRequest(final Contact contact) {
      ParseRelation relation = ParseUser.getCurrentUser().getRelation("spammedBy");
      ParseQuery parseQuery = relation.getQuery();
      parseQuery.findInBackground(new FindCallback<ParseUser>() {
        @Override
        public void done(List<ParseUser> list, ParseException e) {
          if(e == null) {
            if(list != null) {
              boolean userHasSpammedCurrentUser = false;
              for(int i = 0; i < list.size(); i++) {
                if((((ParseUser) list.get(i)).getEmail() != null &&
                      ((ParseUser) list.get(i)).getEmail().equals(contact.email)) ||
                     ((ParseUser) list.get(i)).getUsername().equals(contact.email)) {
                  userHasSpammedCurrentUser = true;
                  break;
                }
              }
              if(userHasSpammedCurrentUser) {
                contact.inProcessing = false;
                contactListAdapter.notifyDataSetChanged();
                showAlertDialog();
              } else {
                ParseQuery<ParseObject> query = ParseQuery.getQuery("Cell411Alert");
                query.whereEqualTo("issuedBy", ParseUser.getCurrentUser());
                query.whereEqualTo("to", contact.user.getUsername());
                ArrayList<String> arrTo = new ArrayList<String>();
                arrTo.add("PENDING");
                arrTo.add("APPROVED");
                query.whereContainedIn("status", arrTo);
                query.whereEqualTo("entryFor", "FR");
                final String name = ParseUser.getCurrentUser().get("firstName") + " " +
                                      ParseUser.getCurrentUser().get("lastName");
                query.findInBackground(new FindCallback<ParseObject>() {
                  @Override
                  public void done(List<ParseObject> list, ParseException e) {
                    if(e == null) {
                      if(list != null && list.size() > 0) { // if true, the request already exists
                        // Send notification for friend request here
                        ParseQuery pushQuery = ParseInstallation.getQuery();
                        pushQuery.whereEqualTo("user", contact.user);
                        ParsePush push = new ParsePush();
                        push.setQuery(pushQuery);
                        JSONObject data = new JSONObject();
                        try {
                          data.put("alert", getString(R.string.notif_friend_request, name,
                            getString(R.string.app_name)));
                          data.put("userId", ParseUser.getCurrentUser().getObjectId());
                          data.put("sound", "default");
                          data.put("friendRequestObjectId", list.get(0).getObjectId());
                          data.put("name", name);
                          data.put("alertType", "FRIEND_REQUEST");
                          data.put("badge", "Increment");
                        } catch(JSONException e1) {
                          e1.printStackTrace();
                        }
                        push.setData(data);
                        push.sendInBackground();
                        contact.inProcessing = false;
                        contact.status = Contact.Status.FRIEND_REQUEST_PENDING;
                        contactListAdapter.notifyDataSetChanged();
                      } else { // the request does not exist
                        final ParseObject friendRequestObject = new ParseObject("Cell411Alert");
                        friendRequestObject.put("issuedBy", ParseUser.getCurrentUser());
                        friendRequestObject.put("issuerFirstName", name);
                        friendRequestObject.put("to", contact.user.getUsername());
                        friendRequestObject.put("status", "PENDING");
                        friendRequestObject.put("entryFor", "FR");
                        friendRequestObject.saveInBackground(new SaveCallback() {
                          @Override
                          public void done(ParseException e) {
                            if(e == null) {
                              // Send notification for friend request here
                              ParseQuery pushQuery = ParseInstallation.getQuery();
                              pushQuery.whereEqualTo("user", contact.user);
                              ParsePush push = new ParsePush();
                              push.setQuery(pushQuery);
                              JSONObject data = new JSONObject();
                              try {
                                data.put("alert", getString(R.string.notif_friend_request, name,
                                  getString(R.string.app_name)));
                                data.put("userId", ParseUser.getCurrentUser().getObjectId());
                                data.put("sound", "default");
                                data.put("friendRequestObjectId",
                                  friendRequestObject.getObjectId());
                                data.put("name", name);
                                data.put("alertType", "FRIEND_REQUEST");
                                data.put("badge", "Increment");
                              } catch(JSONException e1) {
                                e1.printStackTrace();
                              }
                              push.setData(data);
                              push.sendInBackground();
                              contact.inProcessing = false;
                              contact.status = Contact.Status.FRIEND_REQUEST_PENDING;
                            } else {
                              contact.inProcessing = false;
                              LogEvent.Log(TAG, "Exception: " + e.getMessage());
                            }
                            contactListAdapter.notifyDataSetChanged();
                          }
                        });
                      }
                    } else {
                      LogEvent.Log(TAG, "Exception: " + e.getMessage());
                      contact.inProcessing = false;
                      contactListAdapter.notifyDataSetChanged();
                    }
                  }
                });
              }
            } else {
              // Seems like the list is empty
              contact.inProcessing = false;
              contactListAdapter.notifyDataSetChanged();
              LogEvent.Log(TAG, "Spam users list is null");
            }
          } else {
            contact.inProcessing = false;
            contactListAdapter.notifyDataSetChanged();
            LogEvent.Log(TAG, "Exception: " + e.getMessage());
          }
        }
      });
    }

    private void showAlertDialog() {
      AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
      alert.setMessage(R.string.cannot_send_friend_request);
      alert.setPositiveButton(R.string.dialog_btn_ok, new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialogInterface, int i) {
        }
      });
      AlertDialog dialog = alert.create();
      dialog.show();
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public class ViewHolder extends RecyclerView.ViewHolder {
      // each data item is just a string in this case
      private TextView txtDisplayName;
      private CircularImageView imgUser;
      private TextView txtEmail;
      private TextView txtOption;
      private TextView txtInfo;
      private ProgressBar pb;

      public ViewHolder(View view, int type) {
        super(view);
        if(type == VIEW_TYPE_CONTACT) {
          txtDisplayName = (TextView) view.findViewById(R.id.txt_display_name);
          imgUser = (CircularImageView) view.findViewById(R.id.img_user);
          txtEmail = (TextView) view.findViewById(R.id.txt_email);
          txtOption = (TextView) view.findViewById(R.id.txt_option);
        } else if(type == VIEW_TYPE_FOOTER) {
          txtInfo = (TextView) view.findViewById(R.id.txt_info);
          pb = (ProgressBar) view.findViewById(R.id.pb);
        }
      }
    }
  }
}