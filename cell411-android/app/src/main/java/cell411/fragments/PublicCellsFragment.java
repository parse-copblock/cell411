package cell411.fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.parse.ParseException;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseRelation;
import com.parse.ParseUser;
import com.parse.callback.DeleteCallback;
import com.parse.callback.FindCallback;
import com.parse.callback.GetCallback;
import com.parse.callback.SaveCallback;
import com.safearx.cell411.R;

import java.util.ArrayList;
import java.util.List;

import cell411.Singleton;
import cell411.activity.ChatActivity;
import cell411.activity.CreateOrEditPublicCellActivity;
import cell411.activity.MainActivity;
import cell411.activity.PublicCellMembersActivity;
import cell411.db.DataSource;
import cell411.models.Cell;
import cell411.models.ChatRoom;
import cell411.models.Footer;
import cell411.models.PublicCell;
import cell411.utils.LogEvent;
import cell411.widgets.CircularImageView;
import cell411.widgets.Spinner;

/**
 * Created by Sachin on 18-04-2016.
 */
public class PublicCellsFragment extends Fragment {
  private final String TAG = "PublicCellsFragment";
  private final int LIMIT_PER_PAGE = 10;
  private Spinner spinner;
  private RecyclerView recyclerView;
  private LinearLayoutManager linearLayoutManager;
  private CellListAdapter adapterCell;
  private ArrayList<Object> myPublicCellsList;
  //private View footer4JoinedCells;
  private ProgressBar pbFooter4JoinedCells;
  private TextView moreInfoTV4JoinedCells;
  private List<ParseObject> cell411AlertsList4JoinedCells;
  //private ProgressBar pbMyPublicCells;
  //private ProgressBar pbPublicCells;
  //private TextView txtEmptyText;
  private FloatingActionButton fab;
  private int page4JoinedCells = 0;
  private boolean noMoreData4JoinedCell = false;
  private boolean loadingFirstPageOfJoinedCells = true;
  private boolean apiCallInProgress4JoinedCell = false;
  private Handler handler = new Handler();
  private boolean loading = true;
  private int pastVisibleItems, visibleItemCount, totalItemCount;

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
                           Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.fragment_public_cells, container, false);
    fab = (FloatingActionButton) view.findViewById(R.id.fab);
    fab.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        Intent intent = new Intent(getActivity(), CreateOrEditPublicCellActivity.class);
        startActivity(intent);
      }
    });
    fab.hide();
    spinner = (Spinner) view.findViewById(R.id.spinner);
    recyclerView = (RecyclerView) view.findViewById(R.id.rv_public_cells);
    // use this setting to improve performance if you know that changes
    // in content do not change the layout size of the RecyclerView
    recyclerView.setHasFixedSize(true);
    // use a linear layout manager
    linearLayoutManager = new LinearLayoutManager(getActivity());
    recyclerView.setLayoutManager(linearLayoutManager);
    if(Singleton.INSTANCE.getPublicCells() != null) {
      myPublicCellsList = (ArrayList<Object>) Singleton.INSTANCE.getPublicCells().clone();
    }
    if(myPublicCellsList == null ||
         myPublicCellsList != null) { // fake condition, please don't mind
      spinner.setVisibility(View.VISIBLE);
      ParseQuery<ParseObject> cellQuery = ParseQuery.getQuery("PublicCell");
      cellQuery.whereEqualTo("createdBy", ParseUser.getCurrentUser());
      cellQuery.findInBackground(new FindCallback<ParseObject>() {
        @Override
        public void done(List<ParseObject> parseObjects, ParseException e) {
          spinner.setVisibility(View.GONE);
          if(e == null) {
            myPublicCellsList = new ArrayList<>();
            if(parseObjects != null) {
              for(int i = 0; i < parseObjects.size(); i++) {
                ParseObject cell = parseObjects.get(i);
                Cell cl = new Cell((String) cell.get("name"), cell);
                cl.totalMembers = (int) cell.get("totalMembers");
                cl.verificationStatus = (int) cell.get("verificationStatus");
                cl.status = Cell.Status.OWNER;
                myPublicCellsList.add(cl);
              }
            }
            Singleton.INSTANCE.setPublicCells((ArrayList<Cell>) myPublicCellsList.clone());
            if(MainActivity.weakRef.get() != null && !MainActivity.weakRef.get().isFinishing() &&
                 isAdded()) {
              if(myPublicCellsList.size() > 0) {
                myPublicCellsList.add(0, getString(R.string.owned_cells));
              }
              adapterCell = new CellListAdapter(myPublicCellsList);
              recyclerView.setAdapter(adapterCell);
              ParseQuery<ParseObject> cell411Query = new ParseQuery<>("Cell411Alert");
              cell411Query.whereEqualTo("issuedBy", ParseUser.getCurrentUser());
              cell411Query.whereEqualTo("entryFor", "CR");
              cell411Query.whereEqualTo("status", "APPROVED");
              cell411Query.findInBackground(new FindCallback<ParseObject>() {
                @Override
                public void done(List<ParseObject> list, ParseException e) {
                  if(e == null) {
                    if(list == null) {
                      cell411AlertsList4JoinedCells = new ArrayList<>();
                    } else {
                      cell411AlertsList4JoinedCells = list;
                    }
                    retrieveJoinedPublicCells();
                  } else {
                    spinner.setVisibility(View.GONE);
                    if(MainActivity.weakRef.get() != null &&
                         !MainActivity.weakRef.get().isFinishing()) {
                      MainActivity.INSTANCE.getApplicationContext();
                      Singleton.sendToast(e);
                    }
                  }
                }
              });
            }
          } else {
            if(MainActivity.weakRef.get() != null && !MainActivity.weakRef.get().isFinishing()) {
              MainActivity.INSTANCE.getApplicationContext();
              Singleton.sendToast(e);
            }
          }
        }
      });
    } else {
      myPublicCellsList.add(0, getString(R.string.owned_cells));
      adapterCell = new CellListAdapter(myPublicCellsList);
      recyclerView.setAdapter(adapterCell);
      ParseQuery<ParseObject> cell411Query = new ParseQuery<>("Cell411Alert");
      cell411Query.whereEqualTo("issuedBy", ParseUser.getCurrentUser());
      cell411Query.whereEqualTo("entryFor", "CR");
      cell411Query.whereEqualTo("status", "APPROVED");
      cell411Query.findInBackground(new FindCallback<ParseObject>() {
        @Override
        public void done(List<ParseObject> list, ParseException e) {
          if(e == null) {
            if(list == null) {
              cell411AlertsList4JoinedCells = new ArrayList<>();
            } else {
              cell411AlertsList4JoinedCells = list;
            }
            retrieveJoinedPublicCells();
          } else {
            spinner.setVisibility(View.GONE);
            if(MainActivity.weakRef.get() != null && !MainActivity.weakRef.get().isFinishing()) {
              MainActivity.INSTANCE.getApplicationContext();
              Singleton.sendToast(e);
            }
          }
        }
      });
    }
    return view;
  }

  @Override
  public void onResume() {
    super.onResume();
    if(adapterCell != null) {
      if(Singleton.INSTANCE.isNewPublicCellCreated()) {
        Singleton.INSTANCE.setNewPublicCellCreated(false);
        if(adapterCell.arrayList.size() == 0) {
          adapterCell.arrayList.add(0, getString(R.string.owned_cells));
        }
        adapterCell.arrayList.add(1, Singleton.INSTANCE.getPublicCells().
                                                                          get(
                                                                            Singleton.INSTANCE
                                                                              .getPublicCells()
                                                                              .size() - 1));
      }
      if(Singleton.INSTANCE.getPublicCellStatus2() == Cell.Status.NOT_JOINED) {
        Singleton.INSTANCE.setPublicCellStatus2(Cell.Status.UN_CHANGED);
        Cell cell = Singleton.INSTANCE.getTappedPublicCell();
        for(int i = 0; i < adapterCell.arrayList.size(); i++) {
          if(adapterCell.arrayList.get(i) instanceof Cell) {
            Cell cell2 = (Cell) adapterCell.arrayList.get(i);
            if(cell2.status != Cell.Status.OWNER) {
              if(cell.cell411AlertId.equals(cell2.cell411AlertId)) {
                adapterCell.arrayList.remove(i);
                break;
              }
            }
          }
        }
      }
      if(Singleton.INSTANCE.isPublicCellDeleted()) {
        Singleton.INSTANCE.setIsPublicCellDeleted(false);
        Cell cell = Singleton.INSTANCE.getTappedPublicCell();
        for(int i = 0; i < adapterCell.arrayList.size(); i++) {
          if(adapterCell.arrayList.get(i) instanceof Cell) {
            Cell cell2 = (Cell) adapterCell.arrayList.get(i);
            if(cell2.status != Cell.Status.OWNER && cell.cell411AlertId != null) {
              if(cell.cell411AlertId.equals(cell2.cell411AlertId)) {
                adapterCell.arrayList.remove(i);
                break;
              }
            }
          }
        }
      }
      if(Singleton.INSTANCE.getTotalPlacesWherePublicCellChangeRequired() > 0) {
        Singleton.INSTANCE.setTotalPlacesWherePublicCellChangeRequired(0);
        PublicCell publicCell = Singleton.INSTANCE.getChangedPublicCell();
        for(int i = 0; i < adapterCell.arrayList.size(); i++) {
          if(adapterCell.arrayList.get(i) instanceof Cell) {
            Cell cell = (Cell) adapterCell.arrayList.get(i);
            if(cell.cell.getObjectId().equals(publicCell.getObjectId())) {
              ParseGeoPoint parseGeoPoint = new ParseGeoPoint(publicCell.lat, publicCell.lng);
              cell.name = publicCell.getName();
              cell.cell.put("name", publicCell.getName());
              cell.cell.put("description", publicCell.description);
              cell.cell.put("cellType", publicCell.cellType);
              cell.cell.put("geoTag", parseGeoPoint);
              cell.cell.put("city", publicCell.city);
              cell.cell.put("country", publicCell.country);
              cell.cell.put("fullAddress", publicCell.fullAddress);
              break;
            }
          }
        }
      }
      adapterCell.notifyDataSetChanged();
    }
  }

  private void retrieveJoinedPublicCells() {
    ParseQuery<ParseObject> cellQuery = ParseQuery.getQuery("PublicCell");
    cellQuery.whereNotEqualTo("createdBy", ParseUser.getCurrentUser());
    cellQuery.include("createdBy");
    cellQuery.whereEqualTo("members", ParseUser.getCurrentUser());
    cellQuery.addDescendingOrder("totalMembers");
    cellQuery.setLimit(LIMIT_PER_PAGE);
    cellQuery.setSkip(LIMIT_PER_PAGE * page4JoinedCells);
    cellQuery.findInBackground(new FindCallback<ParseObject>() {
      @Override
      public void done(List<ParseObject> parseObjects, ParseException e) {
        if(e == null) {
          if(!isAdded()) {
            return;
          }
          ArrayList<Cell> cellsList = new ArrayList<>();
          if(parseObjects != null && parseObjects.size() > 0) {
            for(int i = 0; i < parseObjects.size(); i++) {
              ParseObject cell = parseObjects.get(i);
              Cell cl = new Cell((String) cell.get("name"), cell);
              cl.totalMembers = (int) cell.get("totalMembers");
              cl.verificationStatus = (int) cell.get("verificationStatus");
              cl.status = Cell.Status.JOINED;
              LogEvent.Log(TAG, "verificationStatus: " + cl.verificationStatus);
              for(int j = 0; j < cell411AlertsList4JoinedCells.size(); j++) {
                if(cell411AlertsList4JoinedCells.get(j).get("cellId").equals(cell.getObjectId())) {
                  cl.cell411AlertId = cell411AlertsList4JoinedCells.get(j).getObjectId();
                  break;
                }
              }
              cellsList.add(cl);
            }
            if(parseObjects.size() < 10) {
              noMoreData4JoinedCell = true;
            } else {
              page4JoinedCells++;
            }
            if(loadingFirstPageOfJoinedCells) {
              myPublicCellsList.add(getString(R.string.joined_cells));
              myPublicCellsList.addAll(cellsList);
              if(!noMoreData4JoinedCell) {
                loadingFirstPageOfJoinedCells = false;
                // Add footer to the list that will display a progressbar
                myPublicCellsList.add(new Footer(getString(R.string.load_more), true));
                //adapterCell.notifyItemInserted(myPublicCellsList.size() - 1);
                //listViewMyPublicCells.addFooterView(footer, null, false);
              }
              setJoinedCellListScrollListener();
            } else {
              myPublicCellsList.addAll(myPublicCellsList.size() - 1, cellsList);
              ((Footer) adapterCell.arrayList.get(
                adapterCell.arrayList.size() - 1)).spinnerVisibility = false;
            }
            if(MainActivity.weakRef.get() != null && !MainActivity.weakRef.get().isFinishing() &&
                 getActivity() != null) {
              adapterCell.notifyDataSetChanged();
            }
          }
          if(MainActivity.weakRef.get() != null && !MainActivity.weakRef.get().isFinishing() &&
               getActivity() != null) {
            if(noMoreData4JoinedCell) {
              recyclerView.clearOnScrollListeners();
              //moreInfoTV4JoinedCells.setText("No more cells to load");
              //pbFooter4JoinedCells.setVisibility(View.GONE);
            }
          }
        } else {
          if(MainActivity.weakRef.get() != null && !MainActivity.weakRef.get().isFinishing()) {
            MainActivity.INSTANCE.getApplicationContext();
            Singleton.sendToast(e);
          }
        }
      }
    });
  }

  private void setJoinedCellListScrollListener() {
    recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
      @Override
      public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        if(dy > 0) { //check for scroll down
          visibleItemCount = linearLayoutManager.getChildCount();
          totalItemCount = linearLayoutManager.getItemCount();
          pastVisibleItems = linearLayoutManager.findFirstVisibleItemPosition();
          if(loading) {
            if((visibleItemCount + pastVisibleItems) >= totalItemCount) {
              loading = false;
              LogEvent.Log("...", "Last Item Wow !");
              //Do pagination.. i.e. fetch new data
              handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                  loadMoreJoinedCells();
                }
              }, 300);
            }
          }
        }
      }
    });
  }

  /**
   * This method will load more items when user reaches bottom of the list.
   * It will set the text at the footer to "No more videos to load" when
   * there will be no more videos available.
   */
  private void loadMoreJoinedCells() {
    LogEvent.Log(TAG, "loadMoreItems invoked");
    if(!apiCallInProgress4JoinedCell) {
      if(!noMoreData4JoinedCell) {
        retrieveJoinedPublicCells();
      } else {
        recyclerView.clearOnScrollListeners();
        ((Footer) adapterCell.arrayList.get(adapterCell.arrayList.size() - 1)).
          info = getString(R.string.no_more_cells_to_load);
        ((Footer) adapterCell.arrayList.get(adapterCell.arrayList.size() - 1)).
          spinnerVisibility = false;
        adapterCell.notifyItemChanged(adapterCell.arrayList.size() - 1);
        //moreInfoTV4JoinedCells.setText("No more articles to load");
        //pbFooter4JoinedCells.setVisibility(View.GONE);
      }
    }
  }

  public class CellListAdapter extends RecyclerView.Adapter<CellListAdapter.ViewHolder> {
    private final int VIEW_TYPE_TITLE = 0;
    private final int VIEW_TYPE_CELL = 1;
    private final int VIEW_TYPE_FOOTER = 2;
    private final View.OnClickListener mOnClickListener = new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        int position = recyclerView.getChildAdapterPosition(v);
        ParseGeoPoint parseGeoPoint =
          (ParseGeoPoint) ((Cell) myPublicCellsList.get(position)).cell.get("geoTag");
        int cellType = ((Cell) myPublicCellsList.
                                                  get(position)).cell.getInt("cellType");
        String description = (String) ((Cell) myPublicCellsList.
                                                                 get(position)).cell
                                        .get("description");
        Singleton.INSTANCE.setTappedPublicCell((Cell) myPublicCellsList.get(position));
        Intent intent = new Intent(getActivity(), PublicCellMembersActivity.class);
        intent.putExtra("cellId", ((Cell) myPublicCellsList.get(position)).cell.getObjectId());
        intent.putExtra("cellName", ((Cell) myPublicCellsList.get(position)).name);
        intent.putExtra("status", ((Cell) myPublicCellsList.get(position)).status.toString());
        intent.putExtra("totalMembers", ((Cell) myPublicCellsList.get(position)).totalMembers);
        intent.putExtra("lat", parseGeoPoint.getLatitude());
        intent.putExtra("lon", parseGeoPoint.getLongitude());
        intent.putExtra("city", ((Cell) myPublicCellsList.get(position)).cell.getString("city"));
        intent.putExtra("cellType", cellType);
        intent.putExtra("description", description);
        intent.putExtra("verificationStatus",
          ((Cell) myPublicCellsList.get(position)).verificationStatus);
        startActivity(intent);
      }
    };
    public ArrayList<Object> arrayList;
    private final View.OnLongClickListener mOnLongClickListener = new View.OnLongClickListener() {
      @Override
      public boolean onLongClick(View v) {
        int position = recyclerView.getChildAdapterPosition(v);
        ParseObject cell = ((Cell) myPublicCellsList.get(position)).cell;
        if(((Cell) myPublicCellsList.get(position)).status == Cell.Status.OWNER) {
          showDeleteCellDialog(cell, position);
          return true;
        } else {
          return false;
        }
      }
    };
    private boolean isChatEnabled;

    // Provide a suitable constructor (depends on the kind of dataset)
    public CellListAdapter(ArrayList<Object> arrayList) {
      this.arrayList = arrayList;
      isChatEnabled = getResources().getBoolean(R.bool.is_chat_enabled);
    }

    private void showDeleteCellDialog(final ParseObject cell, final int position) {
      AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
      alert.setMessage(getString(R.string.dialog_msg_delete_public_cell, cell.get("name")));
      alert.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialogInterface, int i) {
        }
      });
      alert.setPositiveButton(R.string.dialog_btn_ok, new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialogInterface, int i) {
          spinner.setVisibility(View.VISIBLE);
          cell.deleteInBackground(new DeleteCallback() {
            @Override
            public void done(ParseException e) {
              spinner.setVisibility(View.GONE);
              if(e == null) {
                Cell cl = (Cell) adapterCell.arrayList.get(position);
                Singleton.INSTANCE.removePublicCellFromList(cl);
                adapterCell.arrayList.remove(position);
                adapterCell.notifyDataSetChanged();
              } else {
                Singleton.sendToast(e);
              }
            }
          });
        }
      });
      AlertDialog dialog = alert.create();
      dialog.show();
    }

    // Create new views (invoked by the layout manager)
    @Override
    public CellListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
      // create a new view
      View v = null;
      if(viewType == VIEW_TYPE_TITLE) {
        v = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_public_cell_title,
          parent, false);
      } else if(viewType == VIEW_TYPE_CELL) {
        v = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_public_cell, parent,
          false);
      } else if(viewType == VIEW_TYPE_FOOTER) {
        v = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_footer, parent, false);
      }
      // set the view's size, margins, padding's and layout parameters
      ViewHolder vh = new ViewHolder(v, viewType);
      if(viewType == VIEW_TYPE_CELL) {
        v.setOnClickListener(mOnClickListener);
        v.setOnLongClickListener(mOnLongClickListener);
      }
      return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int position) {
      // - get element from your dataset at this position
      // - replace the contents of the view with that element
      if(getItemViewType(position) == VIEW_TYPE_CELL) {
        final Cell cell = (Cell) arrayList.get(position);
        viewHolder.txtCellName.setText(cell.name);
        if(cell.status == Cell.Status.OWNER) {
          viewHolder.txtBtnAction.setVisibility(View.VISIBLE);
          viewHolder.txtBtnAction.setText(R.string.delete);
          viewHolder.txtBtnAction.setTextColor(getResources().getColor(R.color.red));
          viewHolder.txtBtnAction.setBackgroundResource(R.drawable.bg_cell_leave);
          viewHolder.txtBtnAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              showDeleteCellDialog(cell.cell, position);
            }
          });
        } else {
          if(cell.status == Cell.Status.INITIALIZING) {
            viewHolder.txtBtnAction.setVisibility(View.GONE);
          } else {
            viewHolder.txtBtnAction.setVisibility(View.VISIBLE);
            viewHolder.txtBtnAction.setText(R.string.leave);
            viewHolder.txtBtnAction.setTextColor(getResources().getColor(R.color.red));
            if(cell.inProcessing) {
              viewHolder.txtBtnAction.setBackgroundColor(Color.parseColor("#662196F3"));
            } else {
              viewHolder.txtBtnAction.setBackgroundResource(R.drawable.bg_cell_leave);
            }
          }
          viewHolder.txtBtnAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              if(!cell.inProcessing) {
                // leave newPrivateCell here
                cell.inProcessing = true;
                viewHolder.txtBtnAction.setBackgroundColor(Color.parseColor("#662196F3"));
                unJoinCell(cell, position);
              }
            }
          });
        }
        LogEvent.Log(TAG, "verificationStatus: " + cell.verificationStatus);
        if(cell.verificationStatus == 1) {
          viewHolder.imgVerified.setVisibility(View.VISIBLE);
        } else {
          viewHolder.imgVerified.setVisibility(View.GONE);
        }
        if(!isChatEnabled) {
          viewHolder.imgChat.setVisibility(View.GONE);
        } else {
          viewHolder.imgChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              Intent intentChat = new Intent(getActivity(), ChatActivity.class);
              intentChat.putExtra("entityType", ChatRoom.EntityType.PUBLIC_CELL.toString());
              intentChat.putExtra("entityObjectId", cell.cell.getObjectId());
              intentChat.putExtra("entityName", cell.name);
              startActivity(intentChat);
            }
          });
        }
      } else if(getItemViewType(position) == VIEW_TYPE_FOOTER) {
        final Footer footer = (Footer) arrayList.get(position);
        viewHolder.txtInfo.setText(footer.info);
        if(footer.spinnerVisibility) {
          viewHolder.pb.setVisibility(View.VISIBLE);
        } else {
          viewHolder.pb.setVisibility(View.GONE);
        }
      } else {
        String title = (String) arrayList.get(position);
        viewHolder.txtTitle.setText(title);
      }
    }

    private void unJoinCell(final Cell cell, final int index) {
      ParseQuery<ParseObject> query = ParseQuery.getQuery("Cell411Alert");
      query.whereEqualTo("objectId", cell.cell411AlertId);
      query.getFirstInBackground(new GetCallback<ParseObject>() {
        @Override
        public void done(ParseObject parseObject, ParseException e) {
          if(e == null) {
            parseObject.put("status", "LEFT");
            parseObject.saveInBackground(new SaveCallback() {
              @Override
              public void done(ParseException e) {
                if(e == null) {
                  ParseQuery parseQuery = ParseQuery.getQuery("PublicCell");
                  parseQuery.whereEqualTo("objectId", cell.cell.getObjectId());
                  parseQuery.getFirstInBackground(new GetCallback<ParseObject>() {
                    @Override
                    public void done(ParseObject parseObject, ParseException e) {
                      if(e == null) {
                        ParseRelation relation = cell.cell.getRelation("members");
                        relation.remove(ParseUser.getCurrentUser());
                        cell.cell.increment("totalMembers", -1);
                        cell.cell.saveInBackground(new SaveCallback() {
                          @Override
                          public void done(ParseException e) {
                            if(e == null) {
                              cell.totalMembers--; // this step is not required
                              cell.inProcessing = false; // this step is not required
                              cell.status = Cell.Status.NOT_JOINED; // this step is not required
                              myPublicCellsList.remove(index);
                              DataSource ds = new DataSource(getActivity());
                              ds.open();
                              ds.updateIsRemovedFromChatRoom(cell.cell411AlertId, true);
                              ds.close();
                              // Stop listening to the public newPrivateCell for chat messages
                                                            /*Intent chatServiceIntent = new Intent(getActivity(), ChatService.class);
                                                            chatServiceIntent.putExtra("ChatServiceTask", ChatService.ChatServiceTask.DETACH_LISTENER);
                                                            chatServiceIntent.putExtra("entityType", ChatRoom.EntityType.PUBLIC_CELL);
                                                            chatServiceIntent.putExtra("entityObjectId", newPrivateCell.newPrivateCell.getObjectId());
                                                            chatServiceIntent.putExtra("entityCreatedAt", String.valueOf(newPrivateCell.newPrivateCell.getCreatedAt().getTime()));
                                                            chatServiceIntent.putExtra("entityName", newPrivateCell.name);
                                                            getActivity().startService(chatServiceIntent);*/
                            } else {
                              cell.inProcessing = false;
                            }
                            notifyDataSetChanged();
                          }
                        });
                      } else {
                        if(e.getCode() == 101) {
                          // Cell does not exists anymore, remove the newPrivateCell from the list
                          myPublicCellsList.remove(index);
                          notifyDataSetChanged();
                          DataSource ds = new DataSource(getActivity());
                          ds.open();
                          ds.updateIsRemovedFromChatRoom(cell.cell411AlertId, true);
                          ds.close();
                          // Stop listening to the public newPrivateCell for chat messages
                                                    /*Intent chatServiceIntent = new Intent(getActivity(), ChatService.class);
                                                    chatServiceIntent.putExtra("ChatServiceTask", ChatService.ChatServiceTask.DETACH_LISTENER);
                                                    chatServiceIntent.putExtra("entityType", ChatRoom.EntityType.PUBLIC_CELL);
                                                    chatServiceIntent.putExtra("entityObjectId", newPrivateCell.newPrivateCell.getObjectId());
                                                    chatServiceIntent.putExtra("entityCreatedAt", String.valueOf(newPrivateCell.newPrivateCell.getCreatedAt().getTime()));
                                                    chatServiceIntent.putExtra("entityName", newPrivateCell.name);
                                                    getActivity().startService(chatServiceIntent);*/
                        } else {
                          // Seems like the list is empty
                          cell.inProcessing = false;
                          notifyDataSetChanged();
                          if(MainActivity.weakRef.get() != null &&
                               !MainActivity.weakRef.get().isFinishing()) {
                            MainActivity.INSTANCE.getApplicationContext();
                            Singleton.sendToast(e);
                          }
                        }
                      }
                    }
                  });
                } else {
                  cell.inProcessing = false;
                  notifyDataSetChanged();
                  if(MainActivity.weakRef.get() != null &&
                       !MainActivity.weakRef.get().isFinishing()) {
                    MainActivity.INSTANCE.getApplicationContext();
                    Singleton.sendToast(e);
                  }
                }
              }
            });
          } else {
            cell.inProcessing = false;
            notifyDataSetChanged();
            if(MainActivity.weakRef.get() != null && !MainActivity.weakRef.get().isFinishing()) {
              MainActivity.INSTANCE.getApplicationContext();
              Singleton.sendToast(e);
            }
          }
        }
      });
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
      return arrayList.size();
    }

    @Override
    public int getItemViewType(int position) {
      super.getItemViewType(position);
      if(arrayList.get(position) instanceof Cell) {
        return VIEW_TYPE_CELL;
      } else if(arrayList.get(position) instanceof String) {
        return VIEW_TYPE_TITLE;
      } else {
        return VIEW_TYPE_FOOTER;
      }
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public class ViewHolder extends RecyclerView.ViewHolder {
      // each data item is just a string in this case
      private TextView txtCellName;
      private TextView txtBtnAction;
      private CircularImageView imgCell;
      private TextView txtTitle;
      private ImageView imgVerified;
      private ImageView imgChat;
      private TextView txtInfo;
      private ProgressBar pb;

      public ViewHolder(View view, int type) {
        super(view);
        if(type == VIEW_TYPE_TITLE) {
          txtTitle = (TextView) view.findViewById(R.id.txt_title);
        } else if(type == VIEW_TYPE_CELL) {
          txtCellName = (TextView) view.findViewById(R.id.txt_cell_name);
          txtBtnAction = (TextView) view.findViewById(R.id.txt_btn_action);
          imgCell = (CircularImageView) view.findViewById(R.id.img_cell);
          imgChat = (ImageView) view.findViewById(R.id.img_chat);
          imgVerified = (ImageView) view.findViewById(R.id.img_verified);
        } else if(type == VIEW_TYPE_FOOTER) {
          txtInfo = (TextView) view.findViewById(R.id.txt_info);
          pb = (ProgressBar) view.findViewById(R.id.pb);
        }
      }
    }
  }
}