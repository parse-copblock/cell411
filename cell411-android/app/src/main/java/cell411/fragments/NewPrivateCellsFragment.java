package cell411.fragments;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.callback.DeleteCallback;
import com.parse.callback.FindCallback;
import com.safearx.cell411.R;

import java.util.ArrayList;
import java.util.List;

import cell411.Singleton;
import cell411.activity.MainActivity;
import cell411.activity.PrivateCellMembersActivity;
import cell411.models.Footer;
import cell411.models.NewPrivateCell;
import cell411.utils.LogEvent;
import cell411.widgets.CircularImageView;
import cell411.widgets.Spinner;

/**
 * Created by Sachin on 18-04-2016.
 */
public class NewPrivateCellsFragment extends Fragment {
  public static final String BROADCAST_ACTION_NEW_PRIVATE_CELL =
    "com.safearx.cell411.NEW_PRIVATE_CELL_RECEIVER";
  private final String TAG = "NewPrivateCellsFragment";
  private Spinner spinner;
  private RecyclerView recyclerView;
  private CellListAdapter adapterCell;
  private ArrayList<Object> objectArrayList;
  private FloatingActionButton fab;

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
                           Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.fragment_private_cells, container, false);
    fab = view.findViewById(R.id.fab);
    fab.hide();
    spinner = (Spinner) view.findViewById(R.id.spinner);
    recyclerView = (RecyclerView) view.findViewById(R.id.rv_private_cells);
    // use this setting to improve performance if you know that changes
    // in content do not change the layout size of the RecyclerView
    recyclerView.setHasFixedSize(true);
    // use a linear layout manager
    recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
    objectArrayList = new ArrayList<>();
    adapterCell = new CellListAdapter(objectArrayList);
    recyclerView.setAdapter(adapterCell);
    retrievePrivateCells();
    // Register the receiver for listening to new messages on the currently selected circle from
    // notification service
    IntentFilter mContactReceiverIntentFilter = new IntentFilter(BROADCAST_ACTION_NEW_PRIVATE_CELL);
    NewPrivateCellReceiver newPrivateCellReceiver = new NewPrivateCellReceiver();
    LocalBroadcastManager.getInstance(getActivity()).registerReceiver(newPrivateCellReceiver,
      mContactReceiverIntentFilter);
    return view;
  }

  private void retrievePrivateCells() {
    if(Singleton.INSTANCE.getNewPrivateCells() != null) {
      objectArrayList.addAll(Singleton.INSTANCE.getNewPrivateCells());
      adapterCell.notifyDataSetChanged();
      spinner.setVisibility(View.GONE);
    } else {
      spinner.setVisibility(View.VISIBLE);
      ParseQuery<ParseObject> cellQuery = ParseQuery.getQuery("Cell");
      cellQuery.whereEqualTo("createdBy", ParseUser.getCurrentUser());
      cellQuery.whereNotEqualTo("type", 5);
      cellQuery.include("members");
      cellQuery.include("nauMembers");
      cellQuery.findInBackground(new FindCallback<ParseObject>() {
        @Override
        public void done(List<ParseObject> parseObjects, ParseException e) {
          spinner.setVisibility(View.GONE);
          if(e == null) {
            if(parseObjects != null && parseObjects.size() > 0) {
              ArrayList<NewPrivateCell> newPrivateCellArrayList = new ArrayList<>();
              for(int i = 0; i < parseObjects.size(); i++) {
                ParseObject privateCellObj = parseObjects.get(i);
                NewPrivateCell newPrivateCell =
                  new NewPrivateCell((String) privateCellObj.get("name"), privateCellObj,
                    privateCellObj.getInt("type"), privateCellObj.getInt("totalMembers"));
                if(privateCellObj.getList("members") != null) {
                  LogEvent.Log("Cell",
                    "There are some members in" + " " + privateCellObj.get("name") + " " +
                      "newPrivateCell");
                  List<ParseUser> members = privateCellObj.getList("members");
                  newPrivateCell.members.addAll(members);
                } else {
                  LogEvent.Log("Cell",
                    "There are no members in " + privateCellObj.get("name") + " newPrivateCell");
                }

                newPrivateCellArrayList.add(newPrivateCell);
              }
              Singleton.INSTANCE.setNewPrivateCells(
                (ArrayList<NewPrivateCell>) newPrivateCellArrayList.clone());
              objectArrayList.addAll(newPrivateCellArrayList);
              if(MainActivity.weakRef.get() != null && !MainActivity.weakRef.get().isFinishing()) {
                adapterCell.notifyDataSetChanged();
              }
            } else {
              LogEvent.Log(TAG, "No Cells found");
            }
          } else {
            if(MainActivity.weakRef.get() != null && !MainActivity.weakRef.get().isFinishing()) {
              MainActivity.INSTANCE.getApplicationContext();
              Singleton.sendToast(e);
            }
          }
        }
      });
    }
  }

  public class CellListAdapter extends RecyclerView.Adapter<CellListAdapter.ViewHolder> {
    private final int VIEW_TYPE_NEW_PRIVATE_CELL = 0;
    private final int VIEW_TYPE_FOOTER = 1;
    public ArrayList<Object> arrayList;
    private final View.OnClickListener mOnClickListener = new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        int position = recyclerView.getChildAdapterPosition(v);
        Intent intent = new Intent(getActivity(), PrivateCellMembersActivity.class);
        intent.putExtra("cellObjectId",
          ((NewPrivateCell) arrayList.get(position)).parseObject.getObjectId());
        intent.putExtra("name", ((NewPrivateCell) arrayList.get(position)).name);
        startActivity(intent);
      }
    };
    private final View.OnLongClickListener mOnLongClickListener = new View.OnLongClickListener() {
      @Override
      public boolean onLongClick(View v) {
        int position = recyclerView.getChildAdapterPosition(v);
        NewPrivateCell newPrivateCell = (NewPrivateCell) adapterCell.arrayList.get(position);
        if(newPrivateCell.type == null || newPrivateCell.type == 0) {
          // Delete this newPrivateCell
          showDeleteCellDialog(newPrivateCell.parseObject, position);
        } else {
          getActivity();
          Singleton.sendToast("Default Cells cannot be created");
        }
        return false;
      }
    };

    // Provide a suitable constructor (depends on the kind of dataset)
    public CellListAdapter(ArrayList<Object> arrayList) {
      this.arrayList = arrayList;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
      // create a new view
      View v = null;
      if(viewType == VIEW_TYPE_NEW_PRIVATE_CELL) {
        v = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_private_cell, parent,
          false);
      } else if(viewType == VIEW_TYPE_FOOTER) {
        v = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_footer, parent, false);
      }
      // set the view's size, margins, padding's and layout parameters
      ViewHolder vh = new ViewHolder(v, viewType);
      if(viewType == VIEW_TYPE_NEW_PRIVATE_CELL) {
        v.setOnClickListener(mOnClickListener);
        v.setOnLongClickListener(mOnLongClickListener);
      }
      return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int position) {
      // - get element from your data set at this position
      // - replace the contents of the view with that element
      if(getItemViewType(position) == VIEW_TYPE_NEW_PRIVATE_CELL) {
        final NewPrivateCell newPrivateCell = (NewPrivateCell) arrayList.get(position);
        viewHolder.txtCellName.setText(newPrivateCell.name);
      } else {
        final Footer footer = (Footer) arrayList.get(position);
        viewHolder.txtInfo.setText(footer.info);
        if(footer.spinnerVisibility) {
          viewHolder.pb.setVisibility(View.VISIBLE);
        } else {
          viewHolder.pb.setVisibility(View.GONE);
        }
      }
    }

    // Return the size of your data set (invoked by the layout manager)
    @Override
    public int getItemCount() {
      return arrayList.size();
    }

    @Override
    public int getItemViewType(int position) {
      super.getItemViewType(position);
      if(arrayList.get(position) instanceof NewPrivateCell) {
        return VIEW_TYPE_NEW_PRIVATE_CELL;
      } else {
        return VIEW_TYPE_FOOTER;
      }
    }

    private void showDeleteCellDialog(final ParseObject cell, final int position) {
      AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
      alert.setMessage(getString(R.string.dialog_msg_delete_cell, cell.get("name")));
      alert.setNegativeButton(R.string.dialog_btn_cancel, new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialogInterface, int i) {
        }
      });
      alert.setPositiveButton(R.string.dialog_btn_ok, new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialogInterface, int i) {
          spinner.setVisibility(View.VISIBLE);
          cell.deleteInBackground(new DeleteCallback() {
            @Override
            public void done(ParseException e) {
              spinner.setVisibility(View.GONE);
              if(e == null) {
                NewPrivateCell cl = (NewPrivateCell) adapterCell.arrayList.get(position);
                adapterCell.arrayList.remove(position);
                adapterCell.notifyDataSetChanged();
                Singleton.INSTANCE.removeNewPrivateCellFromList(cl);
              } else {
                if(MainActivity.weakRef.get() != null &&
                     !MainActivity.weakRef.get().isFinishing()) {
                  MainActivity.INSTANCE.getApplicationContext();
                  Singleton.sendToast(e);
                }
              }
            }
          });
        }
      });
      AlertDialog dialog = alert.create();
      dialog.show();
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public class ViewHolder extends RecyclerView.ViewHolder {
      // each data item is just a string in this case
      private TextView txtCellName;
      private CircularImageView imgUser;
      private TextView txtInfo;
      private ProgressBar pb;

      public ViewHolder(View view, int type) {
        super(view);
        if(type == VIEW_TYPE_NEW_PRIVATE_CELL) {
          txtCellName = (TextView) view.findViewById(R.id.txt_cell_name);
          imgUser = (CircularImageView) view.findViewById(R.id.img_cell);
        } else {
          txtInfo = (TextView) view.findViewById(R.id.txt_info);
          pb = (ProgressBar) view.findViewById(R.id.pb);
        }
      }
    }
  }

  public class NewPrivateCellReceiver extends BroadcastReceiver {
    public NewPrivateCellReceiver() {
      super();
    }

    @Override
    public void onReceive(Context context, Intent intent) {
      LogEvent.Log(TAG, "onReceive invoked..");
      NewPrivateCell newPrivateCell =
        (NewPrivateCell) intent.getSerializableExtra("newPrivateCell");
      if(adapterCell != null && adapterCell.arrayList != null &&
           adapterCell.arrayList.size() > 0 && isAdded()) {
        if(newPrivateCell != null) {
          LogEvent.Log(TAG, "New private newPrivateCell: " + newPrivateCell.name);
          adapterCell.arrayList.add(adapterCell.arrayList.size(), newPrivateCell);
          adapterCell.notifyDataSetChanged();
        }
      }
    }
  }
}