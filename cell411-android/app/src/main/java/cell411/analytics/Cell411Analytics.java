package cell411.analytics;

import android.app.Activity;
import android.os.Bundle;

import com.google.firebase.analytics.FirebaseAnalytics;

import cell411.Singleton;

public class Cell411Analytics {
  public static final String KEY_NAV_MENU = "nav_menu";
  public static final String VALUE_NAV_MENU_PROFILE = "Profile";
  public static final String VALUE_NAV_MENU_GENERATE_QR_CODE = "Generate QR Code";
  public static final String VALUE_NAV_MENU_SCAN_QR_CODE = "Scan QR Code";
  public static final String VALUE_NAV_MENU_SETTINGS = "Settings";
  public static final String VALUE_NAV_MENU_NOTIFICATIONS = "Notifications";
  public static final String VALUE_NAV_MENU_KNOW_YOUR_RIGHTS = "Know Your Rights";
  public static final String VALUE_NAV_MENU_FAQ_AND_TUTORIALS = "FAQ and Tutorials";
  public static final String VALUE_NAV_MENU_ABOUT = "About";
  public static final String VALUE_NAV_MENU_BROADCAST_MESSAGE = "Broadcast Message";
  public static final String VALUE_NAV_MENU_LOGOUT = "Logout";
  public static final String VALUE_NAV_MENU_RATE_THIS_APP = "Rate This App";
  public static final String VALUE_NAV_MENU_SHARE_THIS_APP = "Share This App";
  public static final String SCREEN_MAP = "Map Screen";
  private static FirebaseAnalytics mFirebaseAnalytics =
    FirebaseAnalytics.getInstance(Singleton.INSTANCE);

  public Cell411Analytics() {
    // Obtain the FirebaseAnalytics instance.
  }

  public static void logSelectContentEvent(String key, String value) {
    Bundle bundle = new Bundle();
    //bundle.putInt(FirebaseAnalytics.Param.ITEM_ID, 1);
    //bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, "Profile");
    //bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "menu");
    bundle.putString(key, value);
    mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);
  }

  public static void setScreen(Activity activity, String screen) {
    mFirebaseAnalytics.setCurrentScreen(activity, screen, null);
  }

  public static class Screen {
    public static final String SCREEN_MAP = "Map";
    public static final String SCREEN_FRIEND_SEARCH = "Friend Search";
    public static final String SCREEN_PUBLIC_CELL = "Private Cell";
    public static final String SCREEN_ALERT = "Alert";
    public static final String SCREEN_CHAT = "Chat";
  }
}