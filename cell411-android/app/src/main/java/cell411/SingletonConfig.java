package cell411;

public class SingletonConfig {
  public static final String TAG = "SingletonConfig";
  public static final SingletonConfig INSTANCE = new SingletonConfig();

  final public static String BASE_URL =
    "https://pro.copblock.app";
  public static final String BROADCAST_ALERT =
    (BASE_URL + "/webservice/api/v1") + "/broadcast_alert.php";
  public static final String ACK_ALERT =
    (BASE_URL + "/webservice/api/v1") + "/alert_received_ack.php";
  public static final String FIREBASE_ENV = "";
  final public static String VALUE_MODE = "d";
  public static final String COUNTRY_CODE_SOUTH_AFRICA = "ZA";
  public static final String COUNTRY_CODE_ROMANIA = "RO";
  public static final String COUNTRY_CODE_MOLDOVA = "MD";
  public static final String DIALING_CODE_ROMANIA = "40";
  public static final String DIALING_CODE_MOLDOVA = "373";
  // FIXME:  Except for a minimal amount of data for bootstrapping, all of this
  //         should be move onto the server, so it can be changed without redeploying
  //         the app.
  final public static String PARAM_IMAGE_TYPE = "image_type=";
  final public static String PARAM_USER_ID = "user_id=";
  final public static String HASH = "HASH";
  public static final String WOWZA_BASE_URL_DVR = "http://streamer.cell411inc.com:1935/dvr/";
  public static final String WOWZA_BASE_URL_DVR_POSTFIX = "/playlist.m3u8?DVR";
  public static final String DOWNLOAD_VIDEO_URL = "http://streamer.cell411inc.com:81/";
  public static final String HOST_ADDRESS = "streamer.cell411inc.com";
  public static final String APP_NAME = "dvr";
  public static final String GO_LIVE_BASE_URL = "http://streamer.cell411inc.com:81/golive.php?";
  public static final String ENDPOINT_NAME = "name=";
  public static final String ENDPOINT_STREAM = "stream=";
  public static final String ENDPOINT_CITY = "city=";
  public static final String ENDPOINT_COUNTRY = "country=";
  public static final String ENDPOINT_USER = "user=";
  public static final String ENDPOINT_YT = "yt=";
  public static final String ENDPOINT_YT_KEY = "ytkey=";
  public static final String ENDPOINT_YT_APP = "ytapp=";
  public static final String ENDPOINT_YT_HOST = "ythost=";
  public static final String ENDPOINT_YT_CELL411 = "ytcell411=";
  public static final String PARAM_SEPARATOR = "&";
  public static final String VALUE_YES = "yes";
  public static final String DEFAULT_VIDEO_SIZE_WIDTH = "640";
  public static final String DEFAULT_VIDEO_SIZE_HEIGHT = "480";
  final public static int DEFAULT_ALERTS_COUNT = 50;
  final public static int DEFAULT_NOTIFICATIONS_COUNT = 25;
  final public static int DEFAULT_MILES_FOR_NEW_PUBLIC_CELL_ALERT = 50;
  public static final int TIME_TO_LIVE_FOR_CHAT_ON_ALERTS = 259200000; // 72 hours
  public static final String MAP_URL = "https://maps.googleapis.com/maps/api/staticmap?";
  public static final String PARAM_MARKERS = "markers=color:blue|";
  public static final String PARAM_SIZE = "size=250x150";
  public static final String PARAM_SCALE = "scale=2";
  public static final String PARAM_ZOOM = "zoom=15";
  public static final String PARAM_KEY = "key=";
  public static final String CHANNEL_ID_EMERGENCY_ALERT = "emergency_alert";
  public static final String CHANNEL_ID_BLUETOOTH = "btooth_button";
  public static final String CHANNEL_ID_MESSAGE = "msg";
  public static final String CHANNEL_ID_LOCATION_DISABLED = "location_disabled";
  public static final String CHANNEL_ID_MISCELLANEOUS = "others";
  private static final String USERNAME = "cell411";
  //FIXME:  figure out where to put password.
  private static final String PASSWORD = "FIXME";
  public static int COUNTRY_CODE_INDEX_IER = 189;
  public static int COUNTRY_CODE_INDEX_RO112 = 173;
  public static String GOOGLE_DIRECTIONS_API_KEY;

  public static String getFirebaseEnv() {
    return FIREBASE_ENV;
  }

  public static String getUSERNAME() {
    return USERNAME;
  }

  public static String getPASSWORD() {
    return PASSWORD;
  }
}
