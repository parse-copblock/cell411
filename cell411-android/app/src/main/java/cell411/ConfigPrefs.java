package cell411;
/**
 * ConfigPrefs.java
 * gocoder-sdk-sampleapp
 * <p>
 * This is sample code provided by Wowza Media Systems, LLC.  All sample code is intended to be a reference for the
 * purpose of educating developers, and is not intended to be used in any production environment.
 * <p>
 * IN NO EVENT SHALL WOWZA MEDIA SYSTEMS, LLC BE LIABLE TO YOU OR ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, INCLUDING LOST PROFITS, ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION,
 * EVEN IF WOWZA MEDIA SYSTEMS, LLC HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * <p>
 * WOWZA MEDIA SYSTEMS, LLC SPECIFICALLY DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. ALL CODE PROVIDED HEREUNDER IS PROVIDED "AS IS".
 * WOWZA MEDIA SYSTEMS, LLC HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
 * <p>
 * Copyright © 2015 Wowza Media Systems, LLC. All rights reserved.
 */

import android.content.SharedPreferences;

import com.parse.ParseObject;
import com.parse.ParseUser;
import com.wowza.gocoder.sdk.api.configuration.WZMediaConfig;
import com.wowza.gocoder.sdk.api.configuration.WZStreamConfig;
import com.wowza.gocoder.sdk.api.configuration.WowzaConfig;

import static cell411.Singleton.getPASSWORD;
import static cell411.Singleton.getUSERNAME;
import static cell411.SingletonConfig.APP_NAME;
import static cell411.SingletonConfig.DEFAULT_VIDEO_SIZE_HEIGHT;
import static cell411.SingletonConfig.DEFAULT_VIDEO_SIZE_WIDTH;
import static cell411.SingletonConfig.HOST_ADDRESS;

public class ConfigPrefs {
  public static ParseObject cell411AlertObject;

  public static void updateConfigFromPrefs(SharedPreferences sharedPrefs,
                                           WZMediaConfig mediaConfig) {
    // video settings
    mediaConfig.setVideoEnabled(sharedPrefs.getBoolean("wz_video_enabled", true));
    SharedPreferences prefs = Singleton.INSTANCE.getAppPrefs();
    String videoSizeWidth = prefs.getString("VideoSizeWidth", DEFAULT_VIDEO_SIZE_WIDTH);
    String videoSizeHeight =
      prefs.getString("VideoSizeHeight", DEFAULT_VIDEO_SIZE_HEIGHT);
    mediaConfig.setVideoFrameWidth(
      sharedPrefs.getInt("wz_video_frame_width", Integer.parseInt(videoSizeWidth)));
    mediaConfig.setVideoFrameHeight(
      sharedPrefs.getInt("wz_video_frame_height", Integer.parseInt(videoSizeHeight)));
    //mediaConfig.setVideoFrameWidth(sharedPrefs.getInt("wz_video_frame_width", WZMediaConfig.DEFAULT_VIDEO_FRAME_WIDTH));
    //mediaConfig.setVideoFrameHeight(sharedPrefs.getInt("wz_video_frame_height", WZMediaConfig.DEFAULT_VIDEO_FRAME_HEIGHT));
    mediaConfig.setVideoFramerate(Integer.parseInt(sharedPrefs.getString("wz_video_frame_rate",
      String.valueOf(WZMediaConfig.DEFAULT_VIDEO_FRAME_RATE))));
    mediaConfig.setVideoKeyFrameInterval(Integer.parseInt(
      sharedPrefs.getString("wz_video_keyframe_interval",
        String.valueOf(WZMediaConfig.DEFAULT_VIDEO_KEYFRAME_INTERVAL))));
    mediaConfig.setVideoBitRate(Integer.parseInt(sharedPrefs.getString("wz_video_bitrate",
      String.valueOf(WZMediaConfig.DEFAULT_VIDEO_BITRATE))));
    // audio settings
    mediaConfig.setAudioEnabled(sharedPrefs.getBoolean("wz_audio_enabled", true));
    mediaConfig.setAudioSampleRate(Integer.parseInt(sharedPrefs.getString("wz_audio_sample_rate",
      String.valueOf(WZMediaConfig.DEFAULT_AUDIO_SAMPLE_RATE))));
    mediaConfig.setAudioChannels(
      sharedPrefs.getBoolean("wz_audio_stereo", true) ? WZMediaConfig.AUDIO_CHANNELS_STEREO :
        WZMediaConfig.AUDIO_CHANNELS_MONO);
    mediaConfig.setAudioBitRate(Integer.parseInt(sharedPrefs.getString("wz_audio_bitrate",
      String.valueOf(WZMediaConfig.DEFAULT_AUDIO_BITRATE))));
  }

  public static void updateConfigFromPrefs(SharedPreferences sharedPrefs,
                                           WZStreamConfig streamConfig,
                                           ParseObject cell411AlertObject) {
    ConfigPrefs.cell411AlertObject = cell411AlertObject;
    // connection settings
    streamConfig.setHostAddress(
      sharedPrefs.getString("wz_live_host_address", HOST_ADDRESS));
    streamConfig.setPortNumber(Integer.parseInt(
      sharedPrefs.getString("wz_live_port_number", String.valueOf(WowzaConfig.DEFAULT_PORT))));
    streamConfig.setApplicationName(sharedPrefs.getString("wz_live_app_name", APP_NAME));
    streamConfig.setStreamName(ParseUser.getCurrentUser().getObjectId() + "_" +
                                 cell411AlertObject.getCreatedAt().getTime());
    streamConfig.setUsername(sharedPrefs.getString("wz_live_username", getUSERNAME()));
    streamConfig.setPassword(sharedPrefs.getString("wz_live_password", getPASSWORD()));
    updateConfigFromPrefs(sharedPrefs, (WZMediaConfig) streamConfig);
  }

  public static void updateConfigFromPrefs(SharedPreferences sharedPrefs, WowzaConfig wowzaConfig) {
    // WowzaConfig-specific properties
    wowzaConfig.setCapturedVideoRotates(sharedPrefs.getBoolean("wz_captured_video_rotates", true));
    updateConfigFromPrefs(sharedPrefs, (WZStreamConfig) wowzaConfig);
  }
}