package cell411;

import android.app.Activity;
import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.fragment.app.Fragment;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParsePush;
import com.parse.ParseUser;
import com.safearx.cell411.R;

import java.lang.reflect.Field;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import cell411.db.DataSource;
import cell411.models.Cell;
import cell411.models.Cell411Alert;
import cell411.models.NewPrivateCell;
import cell411.models.PublicCell;
import cell411.models.User;
import cell411.utils.LogEvent;
import cell411.utils.StorageOperations;
import cell411.widgets.CircularImageView;

/**
 * Created by Sachin Tibrewal on 4/13/2015.
 */
public class Singleton extends Application implements Application.ActivityLifecycleCallbacks {
    final private static String TAG = Singleton.class.getSimpleName();
    final private static Handler smHandler = new Handler();
    public static Singleton INSTANCE;
    private static ParseObject cell411AlertObject;
    private static ParsePush push;
    private static boolean isGlobal;
    private static HashMap<String, Object> params;
    private static String ALERT_NOTIFICATION_TRAIL;
    private static String stateOfLifeCycle = "";
    private static boolean wasInBackground = true;
    private static boolean isInForeground = false;
    private static float density;
    private static int screenWidth;
    private static int screenHeight;
    private final List<String> spammedUsersIdList = new ArrayList<>();
    private final List<String> spammingUsersIdList = new ArrayList<>();
    private final ScreenOffReceiver screenOffReceiver = new ScreenOffReceiver();
    private LatLng customLocation;
    private boolean isDarkModeChanged = false;
    private ArrayList<User> friendsList;
    private ArrayList<Cell411Alert> cell411Alerts;
    private ArrayList<String> cell411AlertIds;
    private boolean isVideoDeleted;
    private Cell411Alert cell411VideoAlert;
    private ArrayList<Cell> cellsList;
    private ArrayList<NewPrivateCell> newPrivateCellArrayList;
    private ArrayList<Cell> myPublicCellsList;
    private Cell.Status status = Cell.Status.UN_CHANGED;
    private Cell.Status status2 = Cell.Status.UN_CHANGED;
    private Cell cell;
    private boolean newPublicCellCreated;
    private boolean isPublicCellDeleted;
    private boolean isPublicCellDeleted2;
    private boolean isSpamUserListRetrieved;
    private int totalPlacesRequiredToBeChanged;
    private PublicCell publicCell;
    private Activity activity;
    private Fragment fragment;

    public Singleton() {
        INSTANCE = this;
    }

    public static boolean isLoggedIn() {
        return INSTANCE.getAppPrefs().getBoolean(Prefs.IS_LOGGED_IN, true);
    }


    static final DecimalFormat decimalFormat = new DecimalFormat("##,##,##,###");
    public static String formatText(long value) {
        return decimalFormat.format(value);
    }

    public static String getTimeInHour(long millis) {
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(millis);
        int hour = cal.get(Calendar.HOUR);
        int minute = cal.get(Calendar.MINUTE);
        int ampm = cal.get(Calendar.AM_PM);
        String ap;
        if (ampm == 0) {
            ap = "AM";
        } else {
            ap = "PM";
        }
        if (hour == 0) {
            hour = 12;
        }
        String hourMinute = "" + hour;
        if (minute > 0 && minute < 10) {
            hourMinute += ":0" + minute;
        } else if (minute > 9) {
            hourMinute += ":" + minute;
        }
        return hourMinute + " " + ap;
    }

    public static String getFuzzyTimeOrDay(long millis) {
        Calendar calCurrentTime = Calendar.getInstance();
        calCurrentTime.setTimeInMillis(System.currentTimeMillis());
        calCurrentTime.set(Calendar.HOUR, 0);
        calCurrentTime.set(Calendar.MINUTE, 0);
        calCurrentTime.set(Calendar.SECOND, 0);
        calCurrentTime.set(Calendar.MILLISECOND, 0);
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(millis);
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH) + 1;
        int day = cal.get(Calendar.DAY_OF_MONTH);
        int hour = cal.get(Calendar.HOUR);
        int minute = cal.get(Calendar.MINUTE);
        int ampm = cal.get(Calendar.AM_PM);
        String ap;
        if (ampm == 0) {
            ap = "AM";
        } else {
            ap = "PM";
        }
        String hourMinute = "" + hour;
        if (minute > 0 && minute < 10) {
            hourMinute += ":0" + minute;
        } else if (minute > 9) {
            hourMinute += ":" + minute;
        }
        String time;
        if (cal.getTimeInMillis() <
                calCurrentTime.getTimeInMillis()) // Check if the alert was issued yesterday
        {
            time = month + "/" + day + "/" + year;
        } else {
            time = hourMinute + " " + ap;
        }
        return time;
    }

    public static int getResId(String variableName) {
        try {
            Class<com.safearx.cell411.R.drawable> res = com.safearx.cell411.R.drawable.class;
            Field idField = res.getField(variableName);
            return idField.getInt(null);
        } catch (Exception e) {
            if (variableName.equals("do")) {
                try {
                    Class<com.safearx.cell411.R.drawable> res = com.safearx.cell411.R.drawable.class;
                    Field idField = res.getField("dr");
                    return idField.getInt(null);
                } catch (Exception e2) {
                    e.printStackTrace();
                    return -1;
                }
            } else {
                e.printStackTrace();
                return -1;
            }
        }
    }

    public static int getResId(String variableName, Context context) {
        try {
            Class<com.safearx.cell411.R.drawable> res = com.safearx.cell411.R.drawable.class;
            Field idField = res.getField(variableName);
            return idField.getInt(null);
        } catch (Exception e) {
            if (variableName.equals("do")) {
                try {
                    Class<com.safearx.cell411.R.drawable> res = com.safearx.cell411.R.drawable.class;
                    Field idField = res.getField("dr");
                    return idField.getInt(null);
                } catch (Exception e2) {
                    LogEvent.Log(TAG,String.valueOf(context));
                    e.printStackTrace();
                    return -1;
                }
            } else {
                e.printStackTrace();
                return -1;
            }
        }
    }

    public static String getUSERNAME() {
        return SingletonConfig.getUSERNAME();
    }

    public static String getPASSWORD() {
        return SingletonConfig.getPASSWORD();
    }

    public static ParseObject getCell411AlertObject() {
        return cell411AlertObject;
    }

    public static void setCell411AlertObject(ParseObject cell411AlertObject) {
        Singleton.cell411AlertObject = cell411AlertObject;
    }

    public static ParsePush getPush() {
        return push;
    }

    public static void setPush(ParsePush push) {
        Singleton.push = push;
    }


    public static boolean isIsGlobal() {
        return isGlobal;
    }

    public static void setIsGlobal(boolean isGlobal) {
        Singleton.isGlobal = isGlobal;
    }

    public static HashMap<String, Object> getParams() {
        return params;
    }

    public static void setParams(HashMap<String, Object> params) {
        Singleton.params = params;
    }

    public static String getAlertNotificationTrail() {
        return ALERT_NOTIFICATION_TRAIL;
    }

    public static void setAlertNotificationTrail(String alertNotificationTrail) {
        ALERT_NOTIFICATION_TRAIL = alertNotificationTrail;
    }




    public static String getStateOfLifeCycle() {
        return stateOfLifeCycle;
    }

    public static void setStateOfLifeCycle(String stateOfLifeCycle) {
        Singleton.stateOfLifeCycle = stateOfLifeCycle;
    }

    public static boolean isWasInBackground() {
        return wasInBackground;
    }

    public static void setWasInBackground(boolean wasInBackground) {
        Singleton.wasInBackground = wasInBackground;
    }

    public static boolean isIsInForeground() {
        return isInForeground;
    }

    public static void setIsInForeground(boolean isInForeground) {
        Singleton.isInForeground = isInForeground;
    }

    public static float getDensity() {
        return density;
    }

    public static void setDensity(float density) {
        Singleton.density = density;
    }

    public static int getScreenWidth() {
        return screenWidth;
    }

    public static void setScreenWidth(int screenWidth) {
        Singleton.screenWidth = screenWidth;
    }

    public static int getScreenHeight() {
        return screenHeight;
    }

    public static void setScreenHeight(int screenHeight) {
        Singleton.screenHeight = screenHeight;
    }

    public static Handler getSmHandler() {
        return smHandler;
    }

    public static Bitmap getCroppedBitmapFromGravatar(Bitmap bitmap) {
        return SingletonImageFactory.getCroppedBitmapFromGravatar(bitmap);
    }

    public static Bitmap getCroppedBitmap4Map(Bitmap bitmap) {
        return SingletonImageFactory.getCroppedBitmap4Map(bitmap);
    }

    public static Bitmap getCroppedBitmap(Bitmap bitmap) {
        return SingletonImageFactory.getCroppedBitmap(bitmap);
    }

    public static void sendToast(Object obj) {
        // This builds the message from whatever object.  It evaluates
        // is set.
        // The runnable will keep posting itself until it
        // thread.
        later(new Toaster(obj));


    }

    public static void setLoggedIn(boolean b) {
        INSTANCE.getAppPrefs().edit()
                .putBoolean(Prefs.IS_LOGGED_IN, b)
                .apply();
    }

    static class Later implements Runnable {
        Runnable payload;
        Later(Runnable todo) {
            this.payload=todo;
        }
        public void run() {
            if (INSTANCE == null) {
                later(this);
                return;
            }

            if (getSmHandler().getLooper().getThread() != Thread.currentThread()) {
                later(this);
                return;
            }

            payload.run();
        }

    }

    static class Toaster implements Runnable {
        Object msg;
        Toaster(Object msg) {
            this.msg=msg;
        }
        public void run() {
            Toast.makeText(INSTANCE, new LazyMessage(msg).toString(), Toast.LENGTH_LONG).show();
        }
    }

    public static void later(Runnable run) {
        later(run, 100);
    }

    public static void later(final Runnable run, long delay) {
        getSmHandler().postDelayed(new Later(run), delay);
    }

    public List<String> getSpammedUsersIdList() {
        return spammedUsersIdList;
    }

    public List<String> getSpammingUsersIdList() {
        return spammingUsersIdList;
    }

    public ScreenOffReceiver getScreenOffReceiver() {
        return screenOffReceiver;
    }

    public ArrayList<User> getFriendsList() {
        return friendsList;
    }

    public void setFriendsList(ArrayList<User> friendsList) {
        this.friendsList = friendsList;
    }

    public ArrayList<Cell411Alert> getCell411Alerts() {
        return cell411Alerts;
    }

    public void setCell411Alerts(ArrayList<Cell411Alert> cell411Alerts) {
        this.cell411Alerts = cell411Alerts;
    }

    public ArrayList<String> getCell411AlertIds() {
        return cell411AlertIds;
    }

    public void setCell411AlertIds(ArrayList<String> cell411AlertIds) {
        this.cell411AlertIds = cell411AlertIds;
    }

    public Cell411Alert getCell411VideoAlert() {
        return cell411VideoAlert;
    }

    public void setCell411VideoAlert(Cell411Alert cell411VideoAlert) {
        this.cell411VideoAlert = cell411VideoAlert;
    }

    public ArrayList<Cell> getCellsList() {
        return cellsList;
    }

    public void setCellsList(ArrayList<Cell> cellsList) {
        this.cellsList = cellsList;
    }

    public ArrayList<NewPrivateCell> getNewPrivateCellArrayList() {
        return newPrivateCellArrayList;
    }

    public void setNewPrivateCellArrayList(
            ArrayList<NewPrivateCell> newPrivateCellArrayList) {
        this.newPrivateCellArrayList = newPrivateCellArrayList;
    }

    public ArrayList<Cell> getMyPublicCellsList() {
        return myPublicCellsList;
    }

    public void setMyPublicCellsList(ArrayList<Cell> myPublicCellsList) {
        this.myPublicCellsList = myPublicCellsList;
    }

    public Cell.Status getStatus() {
        return status;
    }

    public void setStatus(Cell.Status status) {
        this.status = status;
    }

    public Cell.Status getStatus2() {
        return status2;
    }

    public void setStatus2(Cell.Status status2) {
        this.status2 = status2;
    }

    public Cell getCell() {
        return cell;
    }

    public void setCell(Cell cell) {
        this.cell = cell;
    }

    public int getTotalPlacesRequiredToBeChanged() {
        return totalPlacesRequiredToBeChanged;
    }

    public void setTotalPlacesRequiredToBeChanged(int totalPlacesRequiredToBeChanged) {
        this.totalPlacesRequiredToBeChanged = totalPlacesRequiredToBeChanged;
    }

    public PublicCell getPublicCell() {
        return publicCell;
    }

    public void setPublicCell(PublicCell publicCell) {
        this.publicCell = publicCell;
    }

    public void setImageOnMap(Marker marker) {
        SingletonImageFactory.setImageOnMap(marker);
    }

    public void setImage(CircularImageView imgUser, String s, String issuerEmail) {
        SingletonImageFactory.setImage(imgUser, s, issuerEmail);
    }

    public void setImage(ImageView imgUser, String s, String issuerEmail) {
        SingletonImageFactory.setImage(imgUser, s, issuerEmail);
    }

    public void setImage(ImageView imgUser) {
        SingletonImageFactory.setImage(imgUser);
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        setDensity(getResources().getDisplayMetrics().density);
        setScreenWidth(getResources().getDisplayMetrics().widthPixels);
        setScreenHeight(getResources().getDisplayMetrics().heightPixels);
        Parse.setLogLevel(Parse.LOG_LEVEL_VERBOSE);
        LogEvent.Log(TAG,getString(R.string.parse_api_url));
        String applicationId = getString(R.string.parse_application_id);
        String clientKey = getString(R.string.parse_client_key);

        Parse.initialize(new Parse.Configuration.Builder(this)
                .applicationId(applicationId)
                .clientKey(clientKey)
                .server(getString(R.string.parse_api_url))
                .build());

        ParseInstallation installation = ParseInstallation.getCurrentInstallation();
        if(installation != null) {
            installation.put("GCMSenderId", getString(R.string.google_fcm_sender_id));
            installation.saveInBackground();
        }
        ParseUser.enableRevocableSessionInBackground();

        // For firebase chats to keep offline
        // FirebaseDatabase.getInstance().setPersistenceEnabled(true);
        setAlertNotificationTrail(getString(R.string.alert_trail));
        boolean isDarkModeEnabled = getAppPrefs().getBoolean("DarkMode", false);
        if (isDarkModeEnabled) {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
        } else {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }
        registerActivityLifecycleCallbacks(this);
        registerReceiver(getScreenOffReceiver(), new IntentFilter("android.intent.action.SCREEN_OFF"));
    }

    public void setIsDarkModeChanged() {
        setDarkModeChanged(!isDarkModeChanged());
    }

    public boolean isDarkModeChanged() {
        return isDarkModeChanged;
    }

    public void setDarkModeChanged(boolean darkModeChanged) {
        isDarkModeChanged = darkModeChanged;
    }

    public LatLng getCustomLocation() {
        return customLocation;
    }

    public void setCustomLocation(LatLng customLocation) {
        this.customLocation = customLocation;
    }

    public ArrayList<User> getFriends() {
        return getFriendsList();
    }

    public void setFriends(ArrayList<User> friendsList) {
        this.setFriendsList(friendsList);
    }

    public void addFriendToList(User user) {
        if (getFriendsList() != null) {
            getFriendsList().add(user);
        }
    }

    public void addDeletedVideoToList(Cell411Alert cell411Alert) {
        if (getCell411Alerts() == null) {
            setCell411Alerts(new ArrayList<>());
        }
        getCell411Alerts().add(cell411Alert);
    }

    public void addDownloadedCell411VideoAlertIdToList(String cell411AlertId) {
        if (getCell411AlertIds() == null) {
            setCell411AlertIds(new ArrayList<>());
        }
        getCell411AlertIds().add(cell411AlertId);
    }

    public boolean isVideoDeleted() {
        return isVideoDeleted;
    }

    public void setVideoDeleted(boolean isVideoDeleted) {
        this.isVideoDeleted = isVideoDeleted;
    }

    public Cell411Alert getTappedVideoAlert() {
        return getCell411VideoAlert();
    }

    public void setTappedVideoAlert(Cell411Alert cell411VideoAlert) {
        this.setCell411VideoAlert(cell411VideoAlert);
    }

    public void removeFriendFromList(User user) {
        getFriendsList().remove(user);
    }

    public void removeFriendFromList(String userObjectId) {
        for (int i = 0; i < getFriendsList().size(); i++) {
            if (getFriendsList().get(i).user.getObjectId().equals(userObjectId)) {
                getFriendsList().remove(i);
                break;
            }
        }
    }

    public ArrayList<Cell> getCells() {
        return getCellsList();
    }

    public void setCells(ArrayList<Cell> cellsList) {
        this.setCellsList(cellsList);
    }

    public ArrayList<NewPrivateCell> getNewPrivateCells() {
        return getNewPrivateCellArrayList();
    }

    public void setNewPrivateCells(ArrayList<NewPrivateCell> newPrivateCellArrayList) {
        this.setNewPrivateCellArrayList(newPrivateCellArrayList);
    }

    public void addNewPrivateCellToList(NewPrivateCell newPrivateCell) {
        if (getNewPrivateCellArrayList() == null) {
            setNewPrivateCellArrayList(new ArrayList<>());
        }
        getNewPrivateCellArrayList().add(newPrivateCell);
    }

    public void removeNewPrivateCellFromList(NewPrivateCell newPrivateCell) {
        getNewPrivateCellArrayList().remove(newPrivateCell);
    }

    public ArrayList<Cell> getPublicCells() {
        return getMyPublicCellsList();
    }

    public void setPublicCells(ArrayList<Cell> myPublicCellsList) {
        this.setMyPublicCellsList(myPublicCellsList);
    }

    public void addPublicCellToList(Cell cell) {
        if (getMyPublicCellsList() != null) {
            getMyPublicCellsList().add(cell);
        }
    }

    public void removePublicCellFromList(Cell cell) {
        getMyPublicCellsList().remove(cell);
    }

    public SharedPreferences getAppPrefs() {
        return getSharedPreferences("AppPrefs", Context.MODE_PRIVATE);
    }

    public double getLongitude() {
        SharedPreferences latLogPref = getSharedPreferences("LatLog", Context.MODE_PRIVATE);
        return latLogPref.getFloat("Longitude", 0.00000f);
    }

    public void setLongitude(double longitude) {
        SharedPreferences latLogPref = getSharedPreferences("LatLog", Context.MODE_PRIVATE);
        float lon = (float) (longitude);
        SharedPreferences.Editor edit = latLogPref.edit();
        edit.putFloat("Longitude", lon);
        edit.apply();
    }

    public double getLatitude() {
        SharedPreferences latLogPref = getSharedPreferences("LatLog", Context.MODE_PRIVATE);
        return latLogPref.getFloat("Latitude", 0.00000f);
    }

    public void setLatitude(double latitude) {
        SharedPreferences latLogPref = getSharedPreferences("LatLog", Context.MODE_PRIVATE);
        float lat = (float) (latitude);
        SharedPreferences.Editor edit = latLogPref.edit();
        edit.putFloat("Latitude", lat);
        edit.apply();
    }

    public Cell.Status getPublicCellStatus() {
        return getStatus();
    }

    public void setPublicCellStatus(Cell.Status status) {
        this.setStatus(status);
    }

    public Cell.Status getPublicCellStatus2() {
        return getStatus2();
    }

    public void setPublicCellStatus2(Cell.Status status2) {
        this.setStatus2(status2);
    }

    public Cell getTappedPublicCell() {
        return getCell();
    }

    public void setTappedPublicCell(Cell cell) {
        this.setCell(cell);
    }

    public boolean isNewPublicCellCreated() {
        return newPublicCellCreated;
    }

    public void setNewPublicCellCreated(boolean newPublicCellCreated) {
        this.newPublicCellCreated = newPublicCellCreated;
    }

    public boolean isPublicCellDeleted() {
        return isPublicCellDeleted;
    }

    public void setPublicCellDeleted(boolean publicCellDeleted) {
        isPublicCellDeleted = publicCellDeleted;
    }

    public void setIsPublicCellDeleted(boolean isPublicCellDeleted) {
        this.setPublicCellDeleted(isPublicCellDeleted);
    }

    public boolean isPublicCellDeleted2() {
        return isPublicCellDeleted2;
    }

    public void setPublicCellDeleted2(boolean publicCellDeleted2) {
        isPublicCellDeleted2 = publicCellDeleted2;
    }

    public void setIsPublicCellDeleted2(boolean isPublicCellDeleted2) {
        this.setPublicCellDeleted2(isPublicCellDeleted2);
    }

    public boolean isSpamUserListRetrieved() {
        return isSpamUserListRetrieved;
    }

    public void setSpamUserListRetrieved(boolean spamUserListRetrieved) {
        isSpamUserListRetrieved = spamUserListRetrieved;
    }

    public void setIsSpamUserListRetrieved(boolean isSpamUserListRetrieved) {
        this.setSpamUserListRetrieved(isSpamUserListRetrieved);
    }

    public void setTotalPlacesWherePublicCellChangeRequired(int totalPlacesRequiredToBeChanged,
                                                            PublicCell publicCell) {
        this.setTotalPlacesRequiredToBeChanged(totalPlacesRequiredToBeChanged);
        this.setPublicCell(publicCell);
    }

    public int getTotalPlacesWherePublicCellChangeRequired() {
        return getTotalPlacesRequiredToBeChanged();
    }

    public void setTotalPlacesWherePublicCellChangeRequired(int totalPlacesRequiredToBeChanged) {
        this.setTotalPlacesRequiredToBeChanged(totalPlacesRequiredToBeChanged);
    }

    public PublicCell getChangedPublicCell() {
        return getPublicCell();
    }

    public void clearData() {
        setCells(null);
        setFriends(null);
        setNewPrivateCells(null);
        setPublicCells(null);
        getAppPrefs().edit().clear().apply();
        DataSource dataSource = new DataSource(this);
        dataSource.open();
        dataSource.clearDatabase();
        dataSource.close();
        StorageOperations.clearData(this);
    }

    public Activity getCurrentActivity() {
        return activity;
    }

    public void setCurrentActivity(Activity currentActivity) {
        activity = currentActivity;
    }

    public Fragment getCurrentFragment() {
        return fragment;
    }

    public void setCurrentFragment(Fragment fragment) {
        this.fragment = fragment;
    }

    @Override
    public void onActivityCreated(Activity activity, Bundle arg1) {
        LogEvent.Log(TAG, "onActivityCreated " + activity.getLocalClassName());
        setWasInBackground(false);
        //isInForeground = true;
        setStateOfLifeCycle("Create");
    }

    @Override
    public void onActivityStarted(Activity activity) {
        LogEvent.Log(TAG, "onActivityStarted " + activity.getLocalClassName());
        setStateOfLifeCycle("Start");
    }

    @Override
    public void onActivityResumed(Activity activity) {
        LogEvent.Log(TAG, "onActivityResumed " + activity.getLocalClassName());
        setStateOfLifeCycle("Resume");
    }

    @Override
    public void onActivityPaused(Activity activity) {
        LogEvent.Log(TAG, "onActivityPaused " + activity.getLocalClassName());
        setStateOfLifeCycle("Pause");
    }

    @Override
    public void onActivityStopped(Activity activity) {
        LogEvent.Log(TAG, "onActivityStopped " + activity.getLocalClassName());
        setStateOfLifeCycle("Stop");
    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle arg1) {
        LogEvent.Log(TAG, "onActivitySaveInstanceState " + activity.getLocalClassName());
    }

    @Override
    public void onActivityDestroyed(Activity activity) {
        LogEvent.Log(TAG, "onActivityDestroyed " + activity.getLocalClassName());
        setWasInBackground(false);
        //isInForeground = true;
        setStateOfLifeCycle("Destroy");
    }

    @Override
    public void onTrimMemory(int level) {
        if (getStateOfLifeCycle().equals("Stop")) {
            setWasInBackground(true);
            //isInForeground = false;
        }
        super.onTrimMemory(level);
        LogEvent.Log(TAG, "onTrimMemory " + level);
    }

    public String getFIREBASE_ENV() {
        return SingletonConfig.getFirebaseEnv();
    }

    static class LazyMessage {
        Object mMsg;

        LazyMessage(Object msg) {
            mMsg = msg;
        }

        @NonNull
        public String toString() {
            if (mMsg instanceof ParseException) {
                ParseException pe = (ParseException) mMsg;
                if (pe.getCode() == 100) {
                    mMsg = R.string.low_connection_msg;
                }
            }

            if (mMsg instanceof Exception) {
                Exception e = (Exception) mMsg;
                mMsg = e.getMessage();
            }

            if (mMsg instanceof Integer) {
                mMsg = INSTANCE.getString((Integer) mMsg);
            }
            if (mMsg instanceof String) {
                return (String) mMsg;
            }
            return String.valueOf(mMsg);
        }


    }

    static class ScreenOffReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            setWasInBackground(true);
            //isInForeground = false;
        }
    }
}
