/**
 * GoCoderSDKActivity.java
 * gocoder-sdk-sampleapp
 * <p>
 * This is sample code provided by Wowza Media Systems, LLC.  All sample code is intended to be a reference for the
 * purpose of educating developers, and is not intended to be used in any production environment.
 * <p>
 * IN NO EVENT SHALL WOWZA MEDIA SYSTEMS, LLC BE LIABLE TO YOU OR ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, INCLUDING LOST PROFITS, ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION,
 * EVEN IF WOWZA MEDIA SYSTEMS, LLC HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * <p>
 * WOWZA MEDIA SYSTEMS, LLC SPECIFICALLY DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. ALL CODE PROVIDED HEREUNDER IS PROVIDED "AS IS".
 * WOWZA MEDIA SYSTEMS, LLC HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
 * <p>
 * Copyright © 2015 Wowza Media Systems, LLC. All rights reserved.
 */
package cell411.activity;

import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.WindowManager;

import androidx.core.app.ActivityCompat;

import com.parse.ParseObject;
import com.parse.ParseUser;
import com.wowza.gocoder.sdk.api.WowzaGoCoder;
import com.wowza.gocoder.sdk.api.broadcast.WZBroadcast;
import com.wowza.gocoder.sdk.api.broadcast.WZBroadcastConfig;
import com.wowza.gocoder.sdk.api.configuration.WZMediaConfig;
import com.wowza.gocoder.sdk.api.configuration.WZStreamConfig;
import com.wowza.gocoder.sdk.api.configuration.WowzaConfig;
import com.wowza.gocoder.sdk.api.errors.WZStreamingError;
import com.wowza.gocoder.sdk.api.logging.WZLog;
import com.wowza.gocoder.sdk.api.status.WZStatus;
import com.wowza.gocoder.sdk.api.status.WZStatusCallback;

import cell411.ConfigPrefs;
import cell411.Singleton;

import static cell411.SingletonConfig.HOST_ADDRESS;

public abstract class GoCoderSDKActivity extends BaseActivity implements WZStatusCallback {
  final private static String SDK_PRODUCTION_APP_LICENSE_KEY = "GSDK-2B42-0000-D810-EAED-34DB";
  private static final int PERMISSIONS_REQUEST_CODE = 0x1;
  protected static String TAG = GoCoderSDKActivity.class.getSimpleName();
  protected static String[] REQUIRED_PERMISSIONS = {};
  // GoCoder SDK top level interface
  protected static WowzaGoCoder sGoCoder = null;
  protected static ParseObject cell411AlertObject2;
  private static Object sBroadcastLock = new Object();
  private static boolean sBroadcastEnded = true;
  // GoCoder SDK Broadcast interface
  protected WZBroadcast mBroadcast = null;
  protected WZBroadcastConfig mBroadcastConfig = null;
  protected boolean mFullScreenActivity = true;
  protected boolean mPermissionsGranted = false;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    // Initialize the GoCoder SDK
    if(sGoCoder == null) {
      // Enable detailed logging from the GoCoder SDK
      WZLog.LOGGING_ENABLED = true;
      sGoCoder = WowzaGoCoder.init(this, SDK_PRODUCTION_APP_LICENSE_KEY);
      if(sGoCoder == null) {
        WZLog.error(TAG, WowzaGoCoder.getLastError());
      } else {
        WZLog.info("GoCoder SDK version number = " + WowzaGoCoder.SDK_VERSION);
        WZLog.info("Platform information = " + WowzaGoCoder.PLATFORM_INFO);
      }
    }
    if(sGoCoder != null) {
      mBroadcast = new WZBroadcast();
      mBroadcastConfig = new WZBroadcastConfig(sGoCoder.getConfig());
    }
  }

  /**
   * Android Activity lifecycle methods
   */
  @Override
  protected void onResume() {
    super.onResume();
    if(mBroadcast != null) {
      mPermissionsGranted = true;
      if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        mPermissionsGranted = (REQUIRED_PERMISSIONS.length > 0 ?
                                 WowzaGoCoder.hasPermissions(this, REQUIRED_PERMISSIONS) : true);
        if(!mPermissionsGranted) {
          ActivityCompat.requestPermissions(this, REQUIRED_PERMISSIONS, PERMISSIONS_REQUEST_CODE);
        }
      }
      if(mPermissionsGranted) {
        ConfigPrefs.updateConfigFromPrefs(PreferenceManager.getDefaultSharedPreferences(this),
          mBroadcastConfig, cell411AlertObject2);
      }
    }
  }

  private void setConfiguration(WZStreamConfig streamConfig) {
    streamConfig.setHostAddress(HOST_ADDRESS);
    streamConfig.setPortNumber(Integer.parseInt(String.valueOf(WowzaConfig.DEFAULT_PORT)));
    streamConfig.setApplicationName(WowzaConfig.DEFAULT_APP);
    streamConfig.setStreamName(ParseUser.getCurrentUser().getObjectId() + "_" +
                                 cell411AlertObject2.getCreatedAt().getTime());
    streamConfig.setUsername(Singleton.getUSERNAME());
    streamConfig.setPassword(Singleton.getPASSWORD());
    WZMediaConfig mediaConfig = (WZMediaConfig) streamConfig;
    // video settings
    mediaConfig.setVideoEnabled(true);
    mediaConfig.setVideoFrameWidth(WZMediaConfig.DEFAULT_VIDEO_FRAME_WIDTH);
    mediaConfig.setVideoFrameHeight(WZMediaConfig.DEFAULT_VIDEO_FRAME_HEIGHT);
    mediaConfig.setVideoFramerate(
      Integer.parseInt(String.valueOf(WZMediaConfig.DEFAULT_VIDEO_FRAME_RATE)));
    mediaConfig.setVideoKeyFrameInterval(
      Integer.parseInt(String.valueOf(WZMediaConfig.DEFAULT_VIDEO_KEYFRAME_INTERVAL)));
    mediaConfig.setVideoBitRate(
      Integer.parseInt(String.valueOf(WZMediaConfig.DEFAULT_VIDEO_BITRATE)));
    // audio settings
    mediaConfig.setAudioEnabled(true);
    mediaConfig.setAudioSampleRate(
      Integer.parseInt(String.valueOf(WZMediaConfig.DEFAULT_AUDIO_SAMPLE_RATE)));
    mediaConfig.setAudioChannels(WZMediaConfig.AUDIO_CHANNELS_STEREO);
    mediaConfig.setAudioBitRate(
      Integer.parseInt(String.valueOf(WZMediaConfig.DEFAULT_AUDIO_BITRATE)));
  }

  @Override
  protected void onPause() {
    // Stop any active live stream
    if(mBroadcast != null && mBroadcast.getBroadcastStatus().isRunning()) {
      endBroadcast(true);
    }
    super.onPause();
  }

  @Override
  public void onRequestPermissionsResult(int requestCode, String permissions[],
                                         int[] grantResults) {
    mPermissionsGranted = true;
    switch(requestCode) {
      case PERMISSIONS_REQUEST_CODE: {
        for(int grantResult : grantResults) {
          if(grantResult != PackageManager.PERMISSION_GRANTED) {
            mPermissionsGranted = false;
          }
        }
      }
    }
  }

  /**
   * Enable Android's sticky immersive full-screen mode
   * See http://developer.android.com/training/system-ui/immersive.html#sticky
   */
  @Override
  public void onWindowFocusChanged(boolean hasFocus) {
    super.onWindowFocusChanged(hasFocus);
    if(mFullScreenActivity && hasFocus) {
      View rootView = getWindow().getDecorView().findViewById(android.R.id.content);
      if(rootView != null) {
        rootView.setSystemUiVisibility(
          View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION |
            View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION |
            View.SYSTEM_UI_FLAG_FULLSCREEN | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
      }
    }
  }

  /**
   * WZStatusCallback interface methods
   */
  @Override
  public void onWZStatus(final WZStatus goCoderStatus) {
    new Handler(Looper.getMainLooper()).post(new Runnable() {
      @Override
      public void run() {
        if(goCoderStatus.isReady()) {
          // Keep the screen on while the broadcast is active
          getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        } else if(goCoderStatus.isIdle())
        // Clear the "keep screen on" flag
        {
          getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        }
        WZLog.debug(TAG, goCoderStatus.toString());
      }
    });
  }

  @Override
  public void onWZError(final WZStatus goCoderStatus) {
    new Handler(Looper.getMainLooper()).post(new Runnable() {
      @Override
      public void run() {
        WZLog.error(TAG, goCoderStatus.getLastError());
      }
    });
  }

  protected synchronized WZStreamingError startBroadcast() {
    WZStreamingError configValidationError = null;
    if(mBroadcast.getBroadcastStatus().isIdle()) {
      WZLog.info(TAG, "=============== Broadcast Configuration ===============\n" +
                        mBroadcastConfig.toString() +
                        "\n=======================================================");
      configValidationError = mBroadcastConfig.validateForBroadcast();
      if(configValidationError == null) {
        mBroadcast.startBroadcast(mBroadcastConfig, this);
      }
    } else {
      WZLog.error(TAG, "startBroadcast() called while another broadcast is active");
    }
    return configValidationError;
  }

  protected synchronized void endBroadcast(boolean appPausing) {
    if(!mBroadcast.getBroadcastStatus().isIdle()) {
      if(appPausing) {
        // Stop any active live stream
        sBroadcastEnded = false;
        mBroadcast.endBroadcast(new WZStatusCallback() {
          @Override
          public void onWZStatus(WZStatus wzStatus) {
            synchronized(sBroadcastLock) {
              sBroadcastEnded = true;
              sBroadcastLock.notifyAll();
            }
          }

          @Override
          public void onWZError(WZStatus wzStatus) {
            WZLog.error(TAG, wzStatus.getLastError());
            synchronized(sBroadcastLock) {
              sBroadcastEnded = true;
              sBroadcastLock.notifyAll();
            }
          }
        });
        while(!sBroadcastEnded) {
          try {
            sBroadcastLock.wait();
          } catch(InterruptedException e) {
          }
        }
      } else {
        mBroadcast.endBroadcast(this);
      }
    } else {
      WZLog.error(TAG, "endBroadcast() called without an active broadcast");
    }
  }

  protected synchronized void endBroadcast() {
    endBroadcast(false);
  }
}