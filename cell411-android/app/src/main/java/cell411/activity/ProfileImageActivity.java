package cell411.activity;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.ActionBar;

import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.safearx.cell411.R;

import java.lang.ref.WeakReference;

import cell411.Singleton;
import cell411.SingletonImageFactory;
import it.sephiroth.android.library.imagezoom.ImageViewTouch;

/**
 * Created by Sachin on 10/3/2015.
 */
public class ProfileImageActivity extends BaseActivity {
  public static WeakReference<ProfileImageActivity> weakRef;

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    switch(item.getItemId()) {
      case android.R.id.home:
        finish();
        return true;
      default:
        return super.onOptionsItemSelected(item);
    }
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_image);
    weakRef = new WeakReference<>(this);
    Singleton.INSTANCE.setCurrentActivity(this);
    // Set up the action bar.
    final ActionBar actionBar = getSupportActionBar();
    actionBar.setDisplayHomeAsUpEnabled(true);
    String userId = getIntent().getStringExtra("userId");
    String imageName = getIntent().getStringExtra("imageName");
    String email = getIntent().getStringExtra("email");
    if(email == null) {
      email = "";
    }
    final String finalEmail = email;
    // Set up the action bar.
    //final ActionBar actionBar = getSupportActionBar();
    //actionBar.setDisplayHomeAsUpEnabled(true);
    //actionBar.setDisplayShowHomeEnabled(true);
    final ImageView imageView = (ImageView) findViewById(R.id.img_placeholder);
    final ImageViewTouch imageViewTouch = (ImageViewTouch) findViewById(R.id.image);
    final TextView txtNoProfilePictureAvailable =
      (TextView) findViewById(R.id.txt_no_profile_picture);
    SingletonImageFactory.getImageLoader().loadImage(
      "https://s3.amazonaws.com/cell411/profile_pic/" + userId + imageName + ".png",
      new ImageLoadingListener() {
        @Override
        public void onLoadingStarted(String s, View view) {
        }

        @Override
        public void onLoadingFailed(String s, View view, FailReason failReason) {
          if(weakRef.get() != null && !weakRef.get().isFinishing()) {
            String res = getGravatarImageUrlFullScreen(finalEmail);
            SingletonImageFactory.getImageLoader().loadImage(res, new ImageLoadingListener() {
              @Override
              public void onLoadingStarted(String s, View view) {
              }

              @Override
              public void onLoadingFailed(String s, View view, FailReason failReason) {
                imageView.setImageResource(R.drawable.ic_placeholder_user);
                txtNoProfilePictureAvailable.setVisibility(View.VISIBLE);
              }

              @Override
              public void onLoadingComplete(String s, View view, Bitmap bitmap) {
                imageViewTouch.setImageBitmap(bitmap);
                imageViewTouch.setVisibility(View.VISIBLE);
              }

              @Override
              public void onLoadingCancelled(String s, View view) {
              }
            });
          }
        }

        @Override
        public void onLoadingComplete(String s, View view, Bitmap bitmap) {
          imageViewTouch.setImageBitmap(bitmap);
          imageViewTouch.setVisibility(View.VISIBLE);
        }

        @Override
        public void onLoadingCancelled(String s, View view) {
        }
      });
  }

  private String getGravatarImageUrlFullScreen(String finalEmail) {
    return SingletonImageFactory.class.getCanonicalName();
  }
}