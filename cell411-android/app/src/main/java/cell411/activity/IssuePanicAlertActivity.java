package cell411.activity;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.ResultReceiver;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.core.app.NotificationCompat;
import androidx.core.app.TaskStackBuilder;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.safearx.cell411.R;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

import cell411.Singleton;
import cell411.constants.Constants;
import cell411.methods.DialogModules4IssuingAlert;
import cell411.models.PrivateCell;
import cell411.models.PublicCell;
import cell411.services.FetchAddressIntentService;
import cell411.utils.LogEvent;
import cell411.utils.StorageOperations;

import static cell411.SingletonConfig.CHANNEL_ID_EMERGENCY_ALERT;

/**
 * Created by Sachin on 11-08-2016.
 */
public class IssuePanicAlertActivity extends BaseActivity implements OnMapReadyCallback {
  public static final String EXTRAS_DEVICE_ADDRESS = "DEVICE_ADDRESS";
  public static final String EXTRAS_ALERT_TYPE = "ALERT_TYPE";
  public static final String EXTRAS_SOFT_ALERT = "SOFT_ALERT";
  private static final String TAG = "IssuePanicAlertActivity";
  public static WeakReference<IssuePanicAlertActivity> weakRef;
  private boolean isTimerComplete;
  private AddressResultReceiver mResultReceiver;
  private TextView txtAddress;
  private CountDownTimer countDownTimer;
  private String mDeviceAddress;
  private String alertType;
  private boolean isSoftAlert;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    //setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    setContentView(R.layout.activity_issue_panic_alert);
    weakRef = new WeakReference<>(this);
    Singleton.INSTANCE.setCurrentActivity(this);
    final Intent intent = getIntent();
    mDeviceAddress = intent.getStringExtra(EXTRAS_DEVICE_ADDRESS);
    alertType = intent.getStringExtra(EXTRAS_ALERT_TYPE);
    isSoftAlert = intent.getBooleanExtra(EXTRAS_SOFT_ALERT, true);
    TextView txtTitle = (TextView) findViewById(R.id.txt_title);
    TextView txtDescription = (TextView) findViewById(R.id.txt_description);
    final TextView txtTimer = (TextView) findViewById(R.id.txt_timer);
    final TextView txtCancel = (TextView) findViewById(R.id.txt_btn_cancel);
    txtAddress = (TextView) findViewById(R.id.txt_address);
    if(alertType.equals("Panic")) {
      txtTitle.setText(R.string.panic_alert);
    } else {
      txtTitle.setText(R.string.fallen_alert);
    }
    final RelativeLayout rlContainer = (RelativeLayout) findViewById(R.id.rl_container);
    SharedPreferences prefs = Singleton.INSTANCE.getAppPrefs();
    final int alertTimeInSeconds = prefs.getInt("AlertTimeInSeconds", 5) + 1;
    boolean panicAlertToAllFriends = prefs.getBoolean("PanicAlertToAllFriends", true);
    boolean panicAlertToNearBy = prefs.getBoolean("PanicAlertToNearBy", true);
    boolean panicAlertToPrivateCells = prefs.getBoolean("PanicAlertToPrivateCells", false);
    //boolean panicAlertToNAUCells = prefs.getBoolean("PanicAlertToNAUCells", false);
    boolean panicAlertToPublicCells = prefs.getBoolean("PanicAlertToPublicCells", false);
    final String additionalText = prefs.getString("AdditionalNote", "");
    String description = getString(R.string.recipients) + ":";
    if(panicAlertToAllFriends) {
      description += "\n- " + getString(R.string.all_friends);
    } else if(panicAlertToPrivateCells) {
      description += "\n- " + getString(R.string.private_cells);
      ArrayList<PrivateCell> privateCells = StorageOperations.
                                                               getPrivateCellsForPanicAlert(
                                                                 Singleton.INSTANCE);
      for(int i = 0; i < privateCells.size(); i++) {
        if(i > 0) {
          description += ",";
        }
        description += " " + privateCells.get(i).getName();
      }
    }
        /*if (panicAlertToNAUCells) {
            description += "\n- " + getString(R.string.nau_cells);
            ArrayList<NAUCell> nauCellArrayList = StorageOperations.
                    getNAUCellsForPanicAlert(Singleton.INSTANCE);
            for (int i = 0; i < nauCellArrayList.size(); i++) {
                if (i > 0) {
                    description += ",";
                }
                description += " " + nauCellArrayList.get(i).name;
            }
        }*/
    if(panicAlertToNearBy) {
      description += "\n- " + getString(R.string.near_by_users);
    }
    if(panicAlertToPublicCells) {
      description += "\n- " + getString(R.string.public_cells);
      ArrayList<PublicCell> publicCells = StorageOperations.
                                                             getPublicCellsForPanicAlert(
                                                               Singleton.INSTANCE);
      for(int i = 0; i < publicCells.size(); i++) {
        if(i > 0) {
          description += ",";
        }
        description += " " + publicCells.get(i).getName();
      }
    }
    if(additionalText != null && !additionalText.isEmpty()) {
      description += "\n\n" + getString(R.string.additional_text) + ": " + additionalText;
    }
    txtDescription.setText(description);
    if(alertTimeInSeconds > 1 && isSoftAlert) {
      countDownTimer = new CountDownTimer(alertTimeInSeconds * 1000, 1000) {
        public void onTick(long millisUntilFinished) {
          txtTimer.setText(millisUntilFinished / 1000 + "");
        }

        public void onFinish() {
          isTimerComplete = true;
          txtCancel.setText(R.string.close);
          txtTimer.setVisibility(View.GONE);
          rlContainer.setVisibility(View.VISIBLE);
          DialogModules4IssuingAlert.sendPanicAlert(IssuePanicAlertActivity.this, "Panic");
          displayNotification(getString(R.string.alert_panic),
            getString(R.string.panic_alert_sent));
        }
      }.start();
    } else {
      if(!isSoftAlert) {
        LogEvent.Log(TAG, "Alert issued from Panic Button");
      }
      isTimerComplete = true;
      txtCancel.setText(R.string.close);
      txtTimer.setVisibility(View.GONE);
      rlContainer.setVisibility(View.VISIBLE);
      if(alertType.equals("Fallen")) {
        DialogModules4IssuingAlert.sendPanicAlert(IssuePanicAlertActivity.this, "Fallen");
        displayNotification(getString(R.string.alert_fallen),
          getString(R.string.fallen_alert_sent));
      } else {
        DialogModules4IssuingAlert.sendPanicAlert(IssuePanicAlertActivity.this, "Panic");
        displayNotification(getString(R.string.alert_panic), getString(R.string.panic_alert_sent));
      }
      // clear the flash on the panic button to save battery
      // scheduleClearFlashJob();
    }
    txtCancel.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        if(isTimerComplete) {
          finish();
        } else {
          countDownTimer.cancel();
          finish();
        }
      }
    });
    // Obtain the SupportMapFragment and get notified when the map is ready to be used.
    SupportMapFragment mapFragment =
      (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
    mapFragment.getMapAsync(this);
    // Initialize the receiver and start the service for reverse geo coded address
    mResultReceiver = new AddressResultReceiver(new Handler());
    startIntentService();
  }

  @Override
  public void onMapReady(GoogleMap googleMap) {
    setUpMap(googleMap);
    googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
      new LatLng(Singleton.INSTANCE.getLatitude(), Singleton.INSTANCE.getLongitude()),
      16f));
    Marker marker = googleMap.addMarker(new MarkerOptions().position(
      new LatLng(Singleton.INSTANCE.getLatitude(), Singleton.INSTANCE.getLongitude()))
                                          .title(
                                            getString(R.string.you_are_here)).icon(
        BitmapDescriptorFactory.fromResource(R.drawable.ic_map_marker)));
    marker.showInfoWindow();
  }

  private void setUpMap(GoogleMap googleMap) {
    googleMap.getUiSettings().setAllGesturesEnabled(true);
    googleMap.getUiSettings().setCompassEnabled(true);
    googleMap.getUiSettings().setMyLocationButtonEnabled(false);
    googleMap.getUiSettings().setRotateGesturesEnabled(true);
    googleMap.getUiSettings().setScrollGesturesEnabled(true);
    googleMap.getUiSettings().setTiltGesturesEnabled(true);
    googleMap.getUiSettings().setZoomControlsEnabled(false);
    googleMap.getUiSettings().setZoomGesturesEnabled(true);
  }

  protected void startIntentService() {
    Intent intent = new Intent(this, FetchAddressIntentService.class);
    intent.putExtra(Constants.RECEIVER, mResultReceiver);
    intent.putExtra(Constants.LOCATION_DATA_LAT, Singleton.INSTANCE.getLatitude());
    intent.putExtra(Constants.LOCATION_DATA_LNG, Singleton.INSTANCE.getLongitude());
    FetchAddressIntentService.enqueueWork(this, intent);
  }

  @Override
  protected void onPause() {
    super.onPause();
        /*if (mDeviceAddress != null) {
            Intent gattServiceIntent = new Intent(this, BluetoothLeService.class);
            gattServiceIntent.putExtra("btTask", BluetoothLeService.BTTask.CLEAR_RED_FLASHING.toString());
            gattServiceIntent.putExtra("mDeviceAddress", mDeviceAddress);
            startService(gattServiceIntent);
        }*/
  }

  /**
   * Displays a notification to the user when an object is added tot he notifications node on firebase and app is in foreground
   *
   * @param notificationTitle   Title of the notification
   * @param notificationMessage Content of the notification
   */
  public void displayNotification(String notificationTitle, String notificationMessage) {
    NotificationCompat.Builder mBuilder =
      new NotificationCompat.Builder(this, CHANNEL_ID_EMERGENCY_ALERT).setSmallIcon(
        R.drawable.logo).setContentTitle(getString(R.string.app_name)).setContentText(
        notificationMessage).setAutoCancel(true);
    // Creates an explicit intent for an Activity in your app
    //    Intent resultIntent = new Intent(this, BTDeviceActivity.class);
    //    resultIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);
    int notificationCode = (int) System.currentTimeMillis();
    // The stack builder object will contain an artificial back stack for the
    // started Activity.
    // This ensures that navigating backward from the Activity leads out of
    // your application to the Home screen.
    TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
    // Adds the back stack for the Intent (but not the Intent itself)
    stackBuilder.addParentStack(MainActivity.class);
    // Adds the Intent that starts the Activity to the top of the stack
    //    stackBuilder.addNextIntent(resultIntent);
    PendingIntent resultPendingIntent =
      stackBuilder.getPendingIntent(notificationCode, PendingIntent.FLAG_UPDATE_CURRENT);
    mBuilder.setContentIntent(resultPendingIntent);
    NotificationManager mNotificationManager =
      (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
    // mId allows you to update the notification later on.
    Notification notification = mBuilder.build();
    notification.defaults = Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE;
    mNotificationManager.notify(notificationCode, notification);
  }

  private class AddressResultReceiver extends ResultReceiver {
    public AddressResultReceiver(Handler handler) {
      super(handler);
    }

    @Override
    protected void onReceiveResult(int resultCode, Bundle resultData) {
      // Display the city
      if(resultCode == Constants.SUCCESS_RESULT) {
        String city = resultData.getString(Constants.RESULT_DATA_CITY);
        txtAddress.setText(city);
      } else {
        txtAddress.setText(R.string.city_not_found);
      }
    }
  }
}