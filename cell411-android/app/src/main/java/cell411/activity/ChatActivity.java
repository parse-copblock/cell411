package cell411.activity;

import android.app.AlertDialog;
import android.app.Service;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageMetadata;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.callback.FindCallback;
import com.parse.callback.FunctionCallback;
import com.safearx.cell411.R;

import java.io.File;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cell411.Singleton;
import cell411.SingletonImageFactory;
import cell411.db.DataSource;
import cell411.fragments.LocationFragment;
import cell411.interfaces.CurrentLocationFromLocationUpdatesCallBack;
import cell411.interfaces.LastLocationCallBack;
import cell411.interfaces.LocationFailToRetrieveCallBack;
import cell411.models.Chat;
import cell411.models.ChatRoom;
import cell411.models.ChatRoomSettings;
import cell411.utils.ImagePicker;
import cell411.utils.LogEvent;
import cell411.utils.StorageOperations;

import static cell411.SingletonConfig.MAP_URL;
import static cell411.SingletonConfig.PARAM_KEY;
import static cell411.SingletonConfig.PARAM_MARKERS;
import static cell411.SingletonConfig.PARAM_SCALE;
import static cell411.SingletonConfig.PARAM_SEPARATOR;
import static cell411.SingletonConfig.PARAM_SIZE;
import static cell411.SingletonConfig.PARAM_ZOOM;
import static cell411.SingletonConfig.TIME_TO_LIVE_FOR_CHAT_ON_ALERTS;

/**
 * Created by Sachin on 13-01-2016.
 */
public class ChatActivity extends BaseActivity
  implements LastLocationCallBack, LocationFailToRetrieveCallBack,
               CurrentLocationFromLocationUpdatesCallBack {
  // Pending work: Check if an entry for this entityId already exists in chatroom db table then
  // update the entry with the last message received in the value event listener so that the most
  // recent chat entry can be at the top in the recent chat list screen
  public static String entityObjectId;
  public static String entityName;
  public static WeakReference<ChatActivity> weakRef;
  public static ChatActivity INSTANCE;
  private final String TAG = "ChatActivity";
  private final int MESSAGES_PER_PAGE = 21;
  private ProgressBar pb;
  private RelativeLayout rlMessage;
  private TextView txtMsgEmpty;
  private ChatListAdapter chatListAdapter;
  private ArrayList<Object> chatArrayList;
  private RecyclerView recyclerView;
  private LinearLayoutManager mLayoutManager;
  private String uid;
  private String firstName;
  private String lastName;
  private ChatRoom.EntityType entityType;
  private String alertType;
  private String issuedBy;
  private String entityCreatedAt = "100000";
  private boolean chatLoaded;
  private EditText etMsg;
  private ImageView imgSend;
  private boolean isLoadingPreviousChatInProgress = false;
  private Calendar calToday, calYesterday, currentCal;
  private int totalChats = 0;
  private DatabaseReference firebaseRef4Chat;
  private DatabaseReference firebaseRef4Notification;
  private String node;
  private boolean isExpired = false;
  private LocationFragment locationFragment;
  private MenuItem miMuteUnmute;
  private ChatRoomSettings chatRoomSettings;
  private boolean muteEnabled = false;
  private ImagePicker imagePicker;
  private Query queryRef;
  private ChildEventListener childEventListener;
  private long lastMsgTime;

  @Override
  public void onLastLocationRetrieved(Location mLastLocation, Feature feature) {
    LogEvent.Log(TAG, "onLastLocationRetrieved() invoked");
    if(mLastLocation != null) {
      shareCurrentLocation();
    }
  }

  @Override
  public void onLocationFailToRetrieve(LocationFailToRetrieveCallBack.FailureReason failureReason,
                                       Feature feature) {
    LogEvent.Log(TAG, "onLocationFailToRetrieve() invoked: " + failureReason.toString());
    switch(failureReason) {
      case PERMISSION_DENIED:
      case SETTINGS_REQUEST_DENIED:
        getApplicationContext();
        Singleton.sendToast("Cannot share current location");
        break;
      default:
        locationFragment.getCurrentLocationFromLocationUpdates(this, feature);
    }
  }

  @Override
  public void onCurrentLocationRetrievedFromLocationUpdates(Location location, Feature feature) {
    LogEvent.Log(TAG, "onCurrentLocationRetrievedFromLocationUpdates() invoked");
    if(location != null) {
      shareCurrentLocation();
    }
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    getMenuInflater().inflate(R.menu.menu_chat_room, menu);
    LogEvent.Log(TAG, "onCreateOptionsMenu() invoked");
    miMuteUnmute = menu.findItem(R.id.action_mute_unmute);
    miMuteUnmute.setTitle(R.string.menu_mute);
    if(chatRoomSettings != null && chatRoomSettings.muteEnabled) {
      LogEvent.Log(TAG, "muteEnabled: " + muteEnabled);
      muteEnabled = true;
      miMuteUnmute.setTitle(R.string.menu_unmute);
    } else {
      LogEvent.Log(TAG, "chatRoomSettings is null or chatRoomSettings.muteEnabled is false");
    }
    return super.onCreateOptionsMenu(menu);
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    switch(item.getItemId()) {
      case android.R.id.home:
        finish();
        return true;
            /*case R.id.action_share_location:
                return true;*/
      case R.id.action_mute_unmute:
        if(muteEnabled) {
          toggleMute(false, false, null, 0, 0);
        } else {
          showMuteAlertDialog();
        }
        return true;
      case R.id.action_share_location:
        if(!isExpired) {
          shareLocationPrompt();
        } else {
          getApplicationContext();
          Singleton.sendToast(R.string.toast_msg_can_not_share_location);
        }
        return true;
      case R.id.action_share_image:
        if(!isExpired) {
          captureAndUploadImage();
        } else {
          getApplicationContext();
          Singleton.sendToast(R.string.toast_msg_can_not_share_location);
        }
        return true;
      default:
        return super.onOptionsItemSelected(item);
    }
  }

  private void captureAndUploadImage() {
    if(imagePicker == null) {
      imagePicker = new ImagePicker(this, null, false);
    }
    imagePicker.displayAddImageChooserDialog();
  }

  @Override
  public void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);
    LogEvent.Log(TAG, "requestCode: " + requestCode + " resultCode: " + resultCode);
    if(resultCode == RESULT_OK && requestCode == ImagePicker.REQUEST_IMAGE_CAPTURE) {
      final String imageName = firebaseRef4Chat.push().getKey() + ".png";
      imagePicker.saveCameraImage(imageName);
      Handler handler = new Handler();
      handler.postDelayed(new Runnable() {
        @Override
        public void run() {
          String path = getExternalFilesDir(null).getAbsolutePath() + "/Photo/" + imageName;
          addMessageToList(path, "img", Singleton.INSTANCE.getLatitude(),
            Singleton.INSTANCE.getLongitude());
          uploadImage(imageName);
        }
      }, 100);
    } else if(resultCode == RESULT_OK && requestCode == ImagePicker.REQUEST_LOAD_IMAGE) {
      if(data != null && data.getData() != null) {
        final String imageName = firebaseRef4Chat.push().getKey() + ".png";
        imagePicker.saveGalleryImage(data, imageName);
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
          @Override
          public void run() {
            String path = getExternalFilesDir(null).getAbsolutePath() + "/Photo/" + imageName;
            addMessageToList(path, "img", Singleton.INSTANCE.getLatitude(),
              Singleton.INSTANCE.getLongitude());
            uploadImage(imageName);
          }
        }, 100);
      } else {
        LogEvent.Log(TAG, "image data is null");
      }
    }
  }

  private void uploadImage(String imageName) {
    final int index = chatListAdapter.arrayList.size() - 1;
    FirebaseStorage storage = FirebaseStorage.getInstance();
    // Create a storage reference from our app
    StorageReference storageRef = storage.getReference();
    String path = getExternalFilesDir(null).getAbsolutePath() + "/Photo/" + imageName;
    // Create file metadata including the content type
    StorageMetadata metadata =
      new StorageMetadata.Builder().setContentType("image/jpg").setCustomMetadata("lat",
        String.valueOf(Singleton.INSTANCE.getLatitude())).setCustomMetadata("lng",
        String.valueOf(Singleton.INSTANCE.getLongitude())).setCustomMetadata("userId",
        ParseUser.getCurrentUser().getObjectId()).setCustomMetadata("entityId",
        entityObjectId).setCustomMetadata("entityType",
        entityType.toString()).setCustomMetadata("entityName", entityName).build();
    Uri file = Uri.fromFile(new File(path));
    final StorageReference imagesRef =
      storageRef.child(Singleton.INSTANCE.getFIREBASE_ENV()).child("images/" + imageName);
    UploadTask uploadTask = imagesRef.putFile(file, metadata);
    Task<Uri> urlTask =
      uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
        @Override
        public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
          if(!task.isSuccessful()) {
            throw task.getException();
          }
          // Continue with the task to get the download URL
          return imagesRef.getDownloadUrl();
        }
      }).addOnCompleteListener(new OnCompleteListener<Uri>() {
        @Override
        public void onComplete(@NonNull Task<Uri> task) {
          if(task.isSuccessful()) {
            Uri downloadUri = task.getResult();
            ((Chat) chatListAdapter.arrayList.get(index)).isImageUploadedToServer = true;
            ((Chat) chatListAdapter.arrayList.get(index)).imageUrl = downloadUri.toString();
            chatListAdapter.notifyItemChanged(index);
            sendMessage(downloadUri.toString(), "img", Singleton.INSTANCE.getLatitude(),
              Singleton.INSTANCE.getLongitude());
          } else {
            // Handle failures
            // ...
          }
        }
      });
  }

  private void shareLocationPrompt() {
    AlertDialog.Builder alert = new AlertDialog.Builder(this);
    alert.setTitle(R.string.dialog_title_share_location);
    alert.setMessage(R.string.dialog_msg_share_location);
    alert.setNegativeButton(getString(R.string.dialog_btn_no), new DialogInterface.
                                                                     OnClickListener() {
      @Override
      public void onClick(DialogInterface dialog, int which) {
      }
    });
    alert.setPositiveButton(R.string.dialog_btn_yes, new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialogInterface, int i) {
        locationFragment.getLastLocation(ChatActivity.this, ChatActivity.this,
          Feature.SHARE_CURRENT_LOCATION);
      }
    });
    AlertDialog dialog = alert.create();
    dialog.show();
  }

  private void shareCurrentLocation() {
    addMessageToList(
      "https://www.google.com/maps/place//@" + Singleton.INSTANCE.getLatitude() + "," +
        Singleton.INSTANCE.getLongitude() + ",16z/", "loc",
      Singleton.INSTANCE.getLatitude(),
      Singleton.INSTANCE.getLongitude());
    sendMessage(
      "https://www.google.com/maps/place//@" + Singleton.INSTANCE.getLatitude() + "," +
        Singleton.INSTANCE.getLongitude() + ",16z/", "loc",
      Singleton.INSTANCE.getLatitude(),
      Singleton.INSTANCE.getLongitude());
  }

  /**
   * Method to toggle between mute and unmute
   */
  private void toggleMute(boolean muteEnabled, boolean notificationDisabled, String muteFor,
                          long muteTime, long muteUntil) {
    this.muteEnabled = muteEnabled;
    if(chatRoomSettings == null) {
      chatRoomSettings = new ChatRoomSettings(entityType, entityObjectId, entityName, muteEnabled,
        notificationDisabled, muteFor, muteTime, muteUntil);
    } else {
      chatRoomSettings.muteEnabled = muteEnabled;
      chatRoomSettings.notificationDisabled = notificationDisabled;
      chatRoomSettings.muteFor = muteFor;
      chatRoomSettings.muteTime = muteTime;
      chatRoomSettings.muteUntil = muteUntil;
    }
    // save the setting into the database
    DataSource dataSource = new DataSource(this);
    dataSource.open();
    dataSource.insertOrUpdateSettings(chatRoomSettings, entityObjectId);
    dataSource.close();
    // refresh the option menu to toggle the title between mute and unmute
    invalidateOptionsMenu();
  }

  @Override
  public boolean onMenuOpened(int featureId, Menu menu) {
    if(chatRoomSettings != null && chatRoomSettings.muteEnabled) {
      if(System.currentTimeMillis() >= chatRoomSettings.muteUntil) {
        toggleMute(false, false, null, 0, 0);
      }
    }
    return super.onMenuOpened(featureId, menu);
  }

  @Override
  public void onPanelClosed(int featureId, Menu menu) {
    super.onPanelClosed(featureId, menu);
  }

  @Override
  public boolean onPrepareOptionsMenu(Menu menu) {
    if(muteEnabled) {
      miMuteUnmute.setTitle(R.string.menu_unmute);
    } else {
      miMuteUnmute.setTitle(R.string.menu_mute);
    }
    return super.onPrepareOptionsMenu(menu);
  }

  private void showMuteAlertDialog() {
    AlertDialog.Builder alert = new AlertDialog.Builder(this);
    alert.setTitle(R.string.dialog_mute_for);
    LayoutInflater inflater = (LayoutInflater) getSystemService(Service.LAYOUT_INFLATER_SERVICE);
    View view = inflater.inflate(R.layout.dialog_mute, null);
    final RadioButton rb1Hours = (RadioButton) view.findViewById(R.id.rb_1hour);
    final RadioButton rb6Hours = (RadioButton) view.findViewById(R.id.rb_6hours);
    final RadioButton rb24Hours = (RadioButton) view.findViewById(R.id.rb_24hours);
    final CheckBox cbShowNotifications = (CheckBox) view.findViewById(R.id.cb_show_notifications);
    cbShowNotifications.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
      @Override
      public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
      }
    });
    alert.setView(view);
    alert.setNegativeButton(R.string.dialog_btn_cancel, new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialogInterface, int i) {
      }
    });
    alert.setPositiveButton(R.string.dialog_btn_ok, new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialogInterface, int i) {
        long muteForMillis;
        String muteFor;
        if(rb1Hours.isChecked()) {
          muteFor = getString(R.string.mute_for_1_hour);
          muteForMillis = 3600000;
        } else if(rb6Hours.isChecked()) {
          muteFor = getString(R.string.mute_for_6_hours);
          muteForMillis = 21600000;
        } else if(rb24Hours.isChecked()) {
          muteFor = getString(R.string.mute_for_24_hours);
          muteForMillis = 86400000;
        } else {
          muteFor = getString(R.string.mute_for_1_month);
          muteForMillis = 108000000;
        }
        long muteTime = System.currentTimeMillis();
        long muteUntil = muteTime + muteForMillis;
        toggleMute(true, !cbShowNotifications.isChecked(), muteFor, muteTime, muteUntil);
      }
    });
    final AlertDialog dialog = alert.create();
    dialog.show();
  }

  @Override
  public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_chat);
    weakRef = new WeakReference<>(this);
    Singleton.INSTANCE.setCurrentActivity(this);
    getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    uid = ParseUser.getCurrentUser().getObjectId();
    firstName = ParseUser.getCurrentUser().getString("firstName");
    lastName = ParseUser.getCurrentUser().getString("lastName");
    entityObjectId = getIntent().getStringExtra("entityObjectId");
    entityName = getIntent().getStringExtra("entityName");
    entityType = ChatRoom.EntityType.valueOf(getIntent().getStringExtra("entityType"));
    initMenu();
    etMsg = (EditText) findViewById(R.id.et_msg);
    imgSend = (ImageView) findViewById(R.id.img_send);
    etMsg.setVisibility(View.GONE);
    imgSend.setVisibility(View.GONE);
    DataSource dataSource = new DataSource(this);
    dataSource.open();
    dataSource.setUnreadMessagesToZero(entityObjectId);
    boolean isRemoved = false;
    switch(entityType) {
      case PUBLIC_CELL:
        isRemoved = dataSource.isRemovedFromChatRoom(entityObjectId);
        node = "publicCells";
        setTitle(entityName);
        break;
      case PRIVATE_CELL:
        node = "privateCells";
        setTitle(entityName);
        break;
      case ALERT:
        node = "alerts";
        alertType = getIntent().getStringExtra("alertType");
        issuedBy = getIntent().getStringExtra("issuedBy");
        entityCreatedAt = getIntent().getStringExtra("time");
        LogEvent.Log(TAG, "time: " + entityCreatedAt);
        isExpired = isChatExpired();
        setTitle(getString(R.string.chat_title_alert, entityName));
        break;
    }
    dataSource.close();
    pb = (ProgressBar) findViewById(R.id.pb);
    rlMessage = (RelativeLayout) findViewById(R.id.rl_messages);
    txtMsgEmpty = (TextView) findViewById(R.id.txt_msg_empty);
    recyclerView = (RecyclerView) findViewById(R.id.rv_chat_list);
    recyclerView.setClipToPadding(true);
    recyclerView.setHasFixedSize(false);
    mLayoutManager = new LinearLayoutManager(this);
    recyclerView.setLayoutManager(mLayoutManager);
    calToday = Calendar.getInstance();
    calToday.set(Calendar.HOUR_OF_DAY, 0);
    calToday.set(Calendar.MINUTE, 0);
    calToday.set(Calendar.SECOND, 0);
    calToday.set(Calendar.MILLISECOND, 0);
    calYesterday = Calendar.getInstance();
    calYesterday.setTimeInMillis(calToday.getTimeInMillis());
    calYesterday.add(Calendar.DAY_OF_MONTH, -1);
    currentCal = Calendar.getInstance();
    currentCal.setTimeInMillis(calToday.getTimeInMillis());
    chatArrayList = new ArrayList<>();
    chatArrayList.add(new LoadMore(0));
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    firebaseRef4Chat = database.getReference(
      Singleton.INSTANCE.getFIREBASE_ENV() + "/messages/" + node + "/" + entityObjectId +
        "/chats");
    firebaseRef4Notification = database.getReference(
      Singleton.INSTANCE.getFIREBASE_ENV() + "/notifications/" + node + "/" + entityObjectId +
        "/chats");
    final boolean finalIsRemoved = isRemoved;
    final FirebaseAuth mAuth = FirebaseAuth.getInstance();
    FirebaseUser currentUser = mAuth.getCurrentUser();
    if(currentUser == null) {
      LogEvent.Log(TAG, "currentUser is null");
      mAuth.signInAnonymously().addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
        @Override
        public void onComplete(@NonNull Task<AuthResult> task) {
          if(task.isSuccessful()) {
            // Sign in success, update UI with the signed-in user's information
            LogEvent.Log(TAG, "signInAnonymously:success");
            retrieveMessage(finalIsRemoved);
          } else {
            // If sign in fails, display a message to the user.
            LogEvent.Log(TAG, "signInAnonymously:failure " + task.getException());
            Singleton.sendToast("Authentication failed.");
          }
        }
      });
    } else {
      LogEvent.Log(TAG, "currentUser is not null");
      retrieveMessage(finalIsRemoved);
    }
    imgSend.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        final String msg = etMsg.getText().toString().trim();
        if(!msg.isEmpty()) {
          addMessageToList(msg, "text", Singleton.INSTANCE.getLatitude(),
            Singleton.INSTANCE.getLongitude());
          sendMessage(msg, "text", Singleton.INSTANCE.getLatitude(),
            Singleton.INSTANCE.getLongitude());
        }
      }
    });
    locationFragment = new LocationFragment();
    FragmentManager fragmentManager = getSupportFragmentManager();
    fragmentManager.beginTransaction().add(locationFragment, "locationFragment").commit();
  }

  private void retrieveMessage(final boolean finalIsRemoved) {
    final boolean finalIsExpired = isExpired;
    firebaseRef4Chat.orderByChild("time").limitToLast(
      MESSAGES_PER_PAGE).addListenerForSingleValueEvent(new ValueEventListener() {
      @Override
      public void onDataChange(DataSnapshot dataSnapshot) {
        Chat lastChat = null;
        if(dataSnapshot.getValue() != null) {
          ArrayList<Object> chatList = new ArrayList<>();
          for(DataSnapshot messageSnapshot : dataSnapshot.getChildren()) {
            String senderId = (String) messageSnapshot.child("senderId").getValue();
            String senderFirstName = (String) messageSnapshot.child("senderFirstName").getValue();
            String senderLastName = (String) messageSnapshot.child("senderLastName").getValue();
            String msgType = "text";
            if(messageSnapshot.hasChild("msgType")) {
              msgType = (String) messageSnapshot.child("msgType").getValue();
            }
            String message = null;
            if(messageSnapshot.hasChild("msg")) {
              message = (String) messageSnapshot.child("msg").getValue();
            }
            double lat = 0;
            if(messageSnapshot.hasChild("lat")) {
              lat = Double.parseDouble(String.valueOf(messageSnapshot.child("lat").getValue()));
            }
            double lng = 0;
            if(messageSnapshot.hasChild("lng")) {
              lng = Double.parseDouble(String.valueOf(messageSnapshot.child("lng").getValue()));
            }
            long time = (long) messageSnapshot.child("time").getValue();
            String imageUrl = (String) messageSnapshot.child("imageUrl").getValue();
            Chat chat = new Chat(senderId, senderFirstName, senderLastName, message, msgType, time);
            if(msgType.equals("loc")) {
              if(lat == 0.1) {
                String[] mapUrlArr = message.split("@");
                String[] latLng = mapUrlArr[1].split(",");
                chat.lat = Double.parseDouble(latLng[0]);
                chat.lng = Double.parseDouble(latLng[1]);
              } else {
                chat.lat = lat;
                chat.lng = lng;
              }
            } else {
              chat.lat = lat;
              chat.lng = lng;
            }
            if(!senderId.equals(uid)) {
              chat.isReceived = true;
            }
            chat.imageUrl = imageUrl;
            LogEvent.Log(TAG, "chat.imageUrl: " + chat.imageUrl);
            chat.isMessageSentToServer = true;
            chat.isImageUploadedToServer = true;
            chatList.add(chat);
          }
          if(chatList.size() == MESSAGES_PER_PAGE) {
            lastChat = (Chat) chatList.get(0);
            chatList.remove(0);
          }
          if(lastChat == null) {
            chatArrayList.remove(0);
            Calendar cal = Calendar.getInstance();
            for(int i = chatList.size() - 1; i >= 0; i--) {
              cal.setTimeInMillis(((Chat) chatList.get(i)).time);
              LogEvent.Log(TAG, "cal: " + cal.getTimeInMillis());
              LogEvent.Log(TAG, "currentCal: " + currentCal.getTimeInMillis());
              if(cal.getTimeInMillis() < currentCal.getTimeInMillis()) {
                if(currentCal.getTimeInMillis() == calToday.getTimeInMillis() &&
                     (i != (chatList.size() - 1))) {
                  chatList.add(i + 1, new DateTitle(getString(R.string.today)));
                } else if(currentCal.getTimeInMillis() == calYesterday.getTimeInMillis() &&
                            (i != (chatList.size() - 1))) {
                  chatList.add(i + 1, new DateTitle(getString(R.string.yesterday)));
                } else if(i != (chatList.size() - 1)) {
                  chatList.add(i + 1, new DateTitle(currentCal.get(Calendar.DAY_OF_MONTH) + " " +
                                                      convertMonth(cal.get(Calendar.MONTH)) + ", " +
                                                      cal.get(Calendar.YEAR)));
                }
                currentCal.setTimeInMillis(cal.getTimeInMillis());
                currentCal.set(Calendar.HOUR_OF_DAY, 0);
                currentCal.set(Calendar.MINUTE, 0);
                currentCal.set(Calendar.SECOND, 0);
                currentCal.set(Calendar.MILLISECOND, 0);
              }
            }
            chatList.add(0, new DateTitle(
              cal.get(Calendar.DAY_OF_MONTH) + " " + convertMonth(cal.get(Calendar.MONTH)) +
                ", " + cal.get(Calendar.YEAR)));
            chatArrayList.addAll(chatList);
            LogEvent.Log(TAG, "lastChat is null");
          } else {
            ((LoadMore) chatArrayList.get(0)).startAt = lastChat.time;
            Calendar cal = Calendar.getInstance();
            for(int i = chatList.size() - 1; i > 0; i--) {
              cal.setTimeInMillis(((Chat) chatList.get(i)).time);
              if(cal.getTimeInMillis() < currentCal.getTimeInMillis()) {
                if(currentCal.getTimeInMillis() == calToday.getTimeInMillis() &&
                     (i != (chatList.size() - 1))) {
                  chatList.add(i + 1, new DateTitle(getString(R.string.today)));
                } else if(currentCal.getTimeInMillis() == calYesterday.getTimeInMillis() &&
                            (i != (chatList.size() - 1))) {
                  chatList.add(i + 1, new DateTitle(getString(R.string.yesterday)));
                } else if(i != (chatList.size() - 1)) {
                  chatList.add(i + 1, new DateTitle(currentCal.get(Calendar.DAY_OF_MONTH) + " " +
                                                      convertMonth(cal.get(Calendar.MONTH)) + ", " +
                                                      cal.get(Calendar.YEAR)));
                }
                currentCal.setTimeInMillis(cal.getTimeInMillis());
                currentCal.set(Calendar.HOUR_OF_DAY, 0);
                currentCal.set(Calendar.MINUTE, 0);
                currentCal.set(Calendar.SECOND, 0);
                currentCal.set(Calendar.MILLISECOND, 0);
              }
            }
            chatArrayList.addAll(chatList);
            LogEvent.Log(TAG, "lastChat startAt: " + lastChat.time);
          }
          if(chatArrayList.size() > 0) {
            lastMsgTime = ((Chat) chatArrayList.get(chatArrayList.size() - 1)).time;
          } else {
            lastMsgTime = 0;
          }
          if(chatArrayList.size() > 1) {
            LogEvent.Log(TAG, "chatArrayList.size() > 1");
            rlMessage.setVisibility(View.GONE);
            chatListAdapter = new ChatListAdapter(chatArrayList);
            recyclerView.setAdapter(chatListAdapter);
            recyclerView.scrollToPosition(chatListAdapter.arrayList.size() - 1);
            totalChats = chatArrayList.size();
          }
        } else {
          rlMessage.setVisibility(View.VISIBLE);
          if(entityType == ChatRoom.EntityType.PUBLIC_CELL) {
            txtMsgEmpty.setText(R.string.no_messages_in_cell);
          } else if(entityType == ChatRoom.EntityType.ALERT) {
            txtMsgEmpty.setText(R.string.no_messages_in_alert);
          }
          LogEvent.Log(TAG, "no messages to load");
        }
        ArrayList<Chat> chats = StorageOperations.getUnseenChats(Singleton.INSTANCE);
        if(chats == null) {
          chats = new ArrayList<>();
        }
        for(int i = 0; i < chats.size(); i++) {
          if(chats.get(i).entityObjectId.equals(entityObjectId)) {
            chats.remove(i);
            i--;
          }
        }
        StorageOperations.storeUnseenChats(Singleton.INSTANCE, chats);
        pb.setVisibility(View.GONE);
        chatLoaded = true;
        if(!finalIsRemoved && !finalIsExpired) {
          etMsg.setVisibility(View.VISIBLE);
          imgSend.setVisibility(View.VISIBLE);
        }
        attachListener();
      }

      @Override
      public void onCancelled(DatabaseError error) {
        pb.setVisibility(View.GONE);
        txtMsgEmpty.setText(R.string.could_not_load_messages);
        Singleton.sendToast(error.getMessage());
      }
    });
  }

  private void sendMessage(String msg, String msgType, double lat, double lng) {
    final int index = chatListAdapter.arrayList.size() - 1;
    long time = System.currentTimeMillis();
    final Map<String, Object> message = new HashMap<>();
    message.put("senderId", uid);
    message.put("senderFirstName", firstName);
    message.put("senderLastName", lastName);
    message.put("msgType", msgType);
    if(msgType.equals("img")) {
      message.put("msg", getString(R.string.chat_msg_image_download_latest_version,
        getString(R.string.app_url_download)));
      message.put("imageUrl", msg);
    } else {
      message.put("msg", msg);
    }
    message.put("lat", lat);
    message.put("lng", lng);
    message.put("time", ServerValue.TIMESTAMP);
    firebaseRef4Chat.push().setValue(message, new DatabaseReference.CompletionListener() {
      @Override
      public void onComplete(DatabaseError firebaseError, DatabaseReference firebase) {
        // The message is sent to the server
        ((Chat) chatListAdapter.arrayList.get(index)).isMessageSentToServer = true;
        chatListAdapter.notifyItemChanged(index);
      }
    });
    final Map<String, Object> data = new HashMap<>();
    data.put("message", message);
    data.put(uid, ServerValue.TIMESTAMP);
    firebaseRef4Notification.push().setValue(data);
    // call sendMessage cloud function here
    HashMap<String, Object> params = new HashMap<>();
    params.put("entityObjectId", entityObjectId);
    params.put("entityType", entityType.toString());
    params.put("entityName", entityName);
    params.put("senderId", uid);
    params.put("senderFirstName", firstName);
    params.put("senderLastName", lastName);
    params.put("msgType", msgType);
    params.put("lat", lat);
    params.put("lng", lng);
    if(msgType.equals("img")) {
      params.put("msg", getString(R.string.chat_msg_image_tap_to_view));
      params.put("imageUrl", msg);
    } else if(msgType.equals("loc")) {
      params.put("msg", getString(R.string.chat_msg_location_tap_to_view));
      params.put("imageUrl", msg);
    } else {
      params.put("msg", msg);
    }
    params.put("time", time);
    params.put("clientFirmId", 1);
    params.put("isLive", getResources().getBoolean(R.bool.enable_publish));
    params.put("languageCode", getString(R.string.language_code));
    params.put("platform", "android");
    ParseCloud.callFunctionInBackground("sendMessage", params, new FunctionCallback<String>() {
      public void done(String result, ParseException e) {
        if(e == null) {
          // Message sent successfully
          LogEvent.Log(TAG, "Message sent");
        } else {
          LogEvent.Log(TAG, "Something went wrong: " + e.getMessage());
        }
      }
    });
  }

  private void addMessageToList(String msg, String msgType, double lat, double lng) {
    String path = null;
    if(msgType.equals("img")) {
      path = msg;
      msg = getString(R.string.chat_msg_image_download_latest_version,
        getString(R.string.app_url_download));
    }
    long time = System.currentTimeMillis();
    ChatRoom chatRoom =
      new ChatRoom(entityType, entityObjectId, entityName, entityCreatedAt, uid, firstName,
        lastName, msg, time, msgType, false);
    DataSource dataSource = new DataSource(getBaseContext());
    dataSource.open();
    dataSource.insertOrUpdateChatRoom(chatRoom, false);
    dataSource.close();
    if(msgType.equals("text")) {
      etMsg.setText("");
    }
    Chat chat = new Chat(uid, firstName, lastName, msg, msgType, time);
    chat.isReceived = false;
    if(msgType.equals("img")) {
      chat.path = path;
    } else if(msgType.equals("loc")) {
      if(lat == 0.1) {
        String[] mapUrlArr = msg.split("@");
        String[] latLng = mapUrlArr[1].split(",");
        chat.lat = Double.parseDouble(latLng[0]);
        chat.lng = Double.parseDouble(latLng[1]);
      } else {
        chat.lat = lat;
        chat.lng = lng;
      }
    } else {
      chat.lat = lat;
      chat.lng = lng;
    }
    rlMessage.setVisibility(View.GONE);
    if(chatListAdapter == null) {
      chatArrayList.add(new DateTitle(getString(R.string.today)));
      chatArrayList.add(chat);
      chatListAdapter = new ChatListAdapter(chatArrayList);
      recyclerView.setAdapter(chatListAdapter);
      totalChats += 2;
    } else {
      Object chatObject = chatListAdapter.arrayList.get(chatListAdapter.arrayList.size() - 1);
      if(chatObject instanceof Chat) {
        if(((Chat) chatObject).time < calToday.getTimeInMillis()) {
          chatListAdapter.arrayList.add(new DateTitle(getString(R.string.today)));
          chatListAdapter.notifyItemInserted(chatListAdapter.arrayList.size() - 1);
          totalChats++;
        }
      }
      // Add msg into the list
      chatListAdapter.arrayList.add(chat);
      chatListAdapter.notifyItemInserted(chatListAdapter.arrayList.size() - 1);
      totalChats++;
    }
    recyclerView.scrollToPosition(chatListAdapter.arrayList.size() - 1);
  }

  private boolean isChatExpired() {
    LogEvent.Log(TAG, "isChatExpired() invoked");
    LogEvent.Log(TAG, "time: " + entityCreatedAt);
    if(entityCreatedAt != null && !entityCreatedAt.equals("100000") &&
         !entityCreatedAt.equals("10000000")) {
      LogEvent.Log(TAG, "time != null && !time.equals(\"100000\")");
      long time = Long.parseLong(this.entityCreatedAt);
      if(System.currentTimeMillis() >= time + TIME_TO_LIVE_FOR_CHAT_ON_ALERTS) {
        // the chat is expired
        LogEvent.Log(TAG, "the chat is expired");
        getApplicationContext();
        Singleton.sendToast(R.string.toast_msg_chat_expired);
        return true;
      } else {
        LogEvent.Log(TAG, "the chat is not expired");
        return false;
      }
    } else { // time is not stored in the database and probably this screen
      // is open after tapping on the notification
      retrieveAlertTime();
      return true;
    }
  }

  /**
   * Call this method if the time is not saved in the database
   */
  private void retrieveAlertTime() {
    LogEvent.Log(TAG, "retrieveAlertTime() invoked");
    ParseQuery<ParseObject> query = ParseQuery.getQuery("Cell411Alert");
    query.whereEqualTo("objectId", entityObjectId);
    query.findInBackground(new FindCallback<ParseObject>() {
      @Override
      public void done(List<ParseObject> list, ParseException e) {
        if(e == null) {
          if(list != null && list.size() > 0) {
            long time = list.get(0).getCreatedAt().getTime();
            if(System.currentTimeMillis() < time + TIME_TO_LIVE_FOR_CHAT_ON_ALERTS) {
              // the chat is not expired
              LogEvent.Log(TAG, "the chat is not expired");
              etMsg.setVisibility(View.VISIBLE);
              imgSend.setVisibility(View.VISIBLE);
            } else {
              getApplicationContext();
              Singleton.sendToast(R.string.toast_msg_chat_expired);
              LogEvent.Log(TAG, "the chat is expired");
            }
            // update the time in the database as well
            DataSource dataSource = new DataSource(ChatActivity.this);
            dataSource.open();
            dataSource.updateEntityCreatedAt(entityObjectId, String.valueOf(time));
            dataSource.close();
          }
        } else {
          LogEvent.Log(TAG, "Error: " + e.getMessage());
        }
      }
    });
  }

  private void initMenu() {
    DataSource ds = new DataSource(this);
    ds.open();
    chatRoomSettings = ds.getSettings(entityObjectId);
    ds.close();
    invalidateOptionsMenu();
  }

  private String convertMonth(int month) {
    switch(month + 1) {
      case 1:
        return getString(R.string.month_jan);
      case 2:
        return getString(R.string.month_feb);
      case 3:
        return getString(R.string.month_mar);
      case 4:
        return getString(R.string.month_apr);
      case 5:
        return getString(R.string.month_may);
      case 6:
        return getString(R.string.month_jun);
      case 7:
        return getString(R.string.month_jul);
      case 8:
        return getString(R.string.month_aug);
      case 9:
        return getString(R.string.month_sep);
      case 10:
        return getString(R.string.month_oct);
      case 11:
        return getString(R.string.month_nov);
      default:
        return getString(R.string.month_dec);
    }
  }

  private void loadPreviousChat(long startAt) {
    LogEvent.Log(TAG, "loadPreviousChat invoked: " + startAt);
    isLoadingPreviousChatInProgress = true;
    firebaseRef4Chat.orderByChild("time").limitToLast(MESSAGES_PER_PAGE).endAt(
      startAt).addListenerForSingleValueEvent(new ValueEventListener() {
      @Override
      public void onDataChange(DataSnapshot dataSnapshot) {
        Chat lastChat = null;
        if(dataSnapshot.getValue() != null) {
          ArrayList<Object> chatList = new ArrayList<>();
          for(DataSnapshot messageSnapshot : dataSnapshot.getChildren()) {
            String senderId = (String) messageSnapshot.child("senderId").getValue();
            String senderFirstName = (String) messageSnapshot.child("senderFirstName").getValue();
            String senderLastName = (String) messageSnapshot.child("senderLastName").getValue();
            String msgType = "text";
            if(messageSnapshot.hasChild("msgType")) {
              msgType = (String) messageSnapshot.child("msgType").getValue();
            }
            String message = null;
            if(messageSnapshot.hasChild("msg")) {
              message = (String) messageSnapshot.child("msg").getValue();
            }
            double lat = 0;
            if(messageSnapshot.hasChild("lat")) {
              lat = Double.parseDouble(String.valueOf(messageSnapshot.child("lat").getValue()));
            }
            double lng = 0;
            if(messageSnapshot.hasChild("lng")) {
              lng = Double.parseDouble(String.valueOf(messageSnapshot.child("lng").getValue()));
            }
            String imageUrl = (String) messageSnapshot.child("imageUrl").getValue();
            long time = (long) messageSnapshot.child("time").getValue();
            Chat chat = new Chat(senderId, senderFirstName, senderLastName, message, msgType, time);
            chat.lat = lat;
            chat.lng = lng;
            chat.imageUrl = imageUrl;
            chat.isMessageSentToServer = true;
            chat.isImageUploadedToServer = true;
            if(!senderId.equals(uid)) {
              chat.isReceived = true;
            }
            chatList.add(chat);
          }
          if(chatList.size() == MESSAGES_PER_PAGE) {
            lastChat = (Chat) chatList.get(0);
            chatList.remove(0);
          }
          if(lastChat == null) {
            chatArrayList.remove(0);
            Calendar cal = Calendar.getInstance();
            for(int i = chatList.size() - 1; i >= 0; i--) {
              cal.setTimeInMillis(((Chat) chatList.get(i)).time);
              if(cal.getTimeInMillis() < currentCal.getTimeInMillis()) {
                if(currentCal.getTimeInMillis() == calToday.getTimeInMillis() &&
                     (i != (chatList.size() - 1))) {
                  chatList.add(i + 1, new DateTitle(getString(R.string.today)));
                } else if(currentCal.getTimeInMillis() == calYesterday.getTimeInMillis() &&
                            (i != (chatList.size() - 1))) {
                  chatList.add(i + 1, new DateTitle(getString(R.string.yesterday)));
                } else if(i != (chatList.size() - 1)) {
                  chatList.add(i + 1, new DateTitle(currentCal.get(Calendar.DAY_OF_MONTH) + " " +
                                                      convertMonth(cal.get(Calendar.MONTH)) + ", " +
                                                      cal.get(Calendar.YEAR)));
                }
                currentCal.setTimeInMillis(cal.getTimeInMillis());
                currentCal.set(Calendar.HOUR_OF_DAY, 0);
                currentCal.set(Calendar.MINUTE, 0);
                currentCal.set(Calendar.SECOND, 0);
                currentCal.set(Calendar.MILLISECOND, 0);
              }
            }
            chatList.add(0, new DateTitle(
              cal.get(Calendar.DAY_OF_MONTH) + " " + convertMonth(cal.get(Calendar.MONTH)) +
                ", " + cal.get(Calendar.YEAR)));
            chatArrayList.addAll(0, chatList);
          } else {
            ((LoadMore) chatArrayList.get(0)).startAt = lastChat.time;
            Calendar cal = Calendar.getInstance();
            for(int i = chatList.size() - 1; i > 0; i--) {
              cal.setTimeInMillis(((Chat) chatList.get(i)).time);
              if(cal.getTimeInMillis() < currentCal.getTimeInMillis()) {
                if(currentCal.getTimeInMillis() == calToday.getTimeInMillis() &&
                     (i != (chatList.size() - 1))) {
                  chatList.add(i + 1, new DateTitle(getString(R.string.today)));
                } else if(currentCal.getTimeInMillis() == calYesterday.getTimeInMillis() &&
                            (i != (chatList.size() - 1))) {
                  chatList.add(i + 1, new DateTitle(getString(R.string.yesterday)));
                } else if(i != (chatList.size() - 1)) {
                  chatList.add(i + 1, new DateTitle(currentCal.get(Calendar.DAY_OF_MONTH) + " " +
                                                      convertMonth(cal.get(Calendar.MONTH)) + ", " +
                                                      cal.get(Calendar.YEAR)));
                }
                currentCal.setTimeInMillis(cal.getTimeInMillis());
                currentCal.set(Calendar.HOUR_OF_DAY, 0);
                currentCal.set(Calendar.MINUTE, 0);
                currentCal.set(Calendar.SECOND, 0);
                currentCal.set(Calendar.MILLISECOND, 0);
              }
            }
            chatArrayList.addAll(1, chatList);
            LogEvent.Log(TAG, "lastChat startAt: " + lastChat.time);
          }
          isLoadingPreviousChatInProgress = false;
          chatListAdapter.notifyDataSetChanged();
          mLayoutManager.scrollToPositionWithOffset(chatArrayList.size() - totalChats + 1, 60);
          //                    for(int i = 0 ; i < chatArrayList.size(); i++) {
          //                        //View view = recyclerView.findViewHolderForLayoutPosition(i).itemView;
          //                        if(recyclerView.findViewHolderForLayoutPosition(i) != null)
          //                            LogEvent.Log(TAG, "view holder found for item: " + i);
          //                        else
          //                            LogEvent.Log(TAG, "view holder not found item: " + i);
          //                    }
          //
          //                    View v = recyclerView.findChildViewUnder(0 , 0);
          //                    if(v != null) {
          //                        v.getHeight();
          //                        LogEvent.Log(TAG, "height: " + v.getHeight());
          //                        mLayoutManager.scrollToPositionWithOffset(chatArrayList.size() - totalChats, 60 - v.getHeight());
          //                    } else {
          //                        LogEvent.Log(TAG, "view is null");
          //                    }
          totalChats = chatArrayList.size();
        } else {
          isLoadingPreviousChatInProgress = false;
        }
      }

      @Override
      public void onCancelled(DatabaseError error) {
        isLoadingPreviousChatInProgress = false;
        Singleton.sendToast(error.getMessage());
      }
    });
  }

  public void newMessageArrived(Chat chat) {
    LogEvent.Log(TAG, "newMessageArrived");
    if(chatLoaded) {
      LogEvent.Log(TAG, "chat loaded");
      if(chatListAdapter == null) {
        LogEvent.Log(TAG, "chatListAdapter == null");
        chatArrayList.add(new DateTitle(getString(R.string.today)));
        chatArrayList.add(chat);
        chatListAdapter = new ChatListAdapter(chatArrayList);
        recyclerView.setAdapter(chatListAdapter);
        totalChats += 2;
      } else {
        LogEvent.Log(TAG, "chatListAdapter != null");
        Object chatObject = chatListAdapter.arrayList.get(chatListAdapter.arrayList.size() - 1);
        if(chatObject instanceof Chat) {
          if(((Chat) chatObject).time < calToday.getTimeInMillis()) {
            chatListAdapter.arrayList.add(new DateTitle(getString(R.string.today)));
            chatListAdapter.notifyItemInserted(chatListAdapter.arrayList.size() - 1);
            totalChats++;
          }
        }
        // Add msg into the list
        chatListAdapter.arrayList.add(chat);
        chatListAdapter.notifyItemInserted(chatListAdapter.arrayList.size() - 1);
        totalChats++;
      }
      recyclerView.scrollToPosition(chatListAdapter.arrayList.size() - 1);
    } else {
      LogEvent.Log(TAG, "chat not loaded");
    }
  }

  @Override
  public void onStart() {
    super.onStart();
    INSTANCE = this;
    if(chatLoaded) {
      attachListener();
            /*if (usersLoaded && circleUserArrayList.size() > 0) {
                etMsg.setEnabled(true);
                imgSend.setEnabled(true);
            }*/
      ArrayList<Chat> chats = StorageOperations.getUnseenChats(Singleton.INSTANCE);
      if(chats == null) {
        chats = new ArrayList<>();
      }
      for(int i = 0; i < chats.size(); i++) {
        if(chats.get(i).entityObjectId.equals(entityObjectId)) {
          //newMessageArrived(chats.get(i));
          chats.remove(i);
          i--;
        }
      }
      StorageOperations.storeUnseenChats(Singleton.INSTANCE, chats);
    }
  }

  @Override
  public void onStop() {
    super.onStop();
    INSTANCE = null;
    if(chatLoaded) {
      detachListener();
    }
  }

  /**
   * Method to attach the Firebase Listener to listen for new incoming instant messages
   */
  private void attachListener() {
    queryRef = firebaseRef4Chat.getRef().orderByChild("time").startAt(lastMsgTime + 1);
    childEventListener = new ChildEventListener() {
      @Override
      public void onChildAdded(DataSnapshot dataSnapshot, String s) {
        LogEvent.Log(TAG, dataSnapshot.toString());
        String senderId = (String) dataSnapshot.child("senderId").getValue();
        if(senderId.equals(uid)) {
          return;
        }
        String senderFirstName = (String) dataSnapshot.child("senderFirstName").getValue();
        String senderLastName = (String) dataSnapshot.child("senderLastName").getValue();
        String messageType = (String) dataSnapshot.child("msgType").getValue();
        String message = (String) dataSnapshot.child("msg").getValue();
        String imageUrl = (String) dataSnapshot.child("imageUrl").getValue();
        lastMsgTime = (long) dataSnapshot.child("time").getValue();
        double lat = 0;
        if(dataSnapshot.hasChild("lat")) {
          lat = Double.parseDouble(String.valueOf(dataSnapshot.child("lat").getValue()));
        }
        double lng = 0;
        if(dataSnapshot.hasChild("lng")) {
          lng = Double.parseDouble(String.valueOf(dataSnapshot.child("lng").getValue()));
        }
        Chat chat =
          new Chat(senderId, senderFirstName, senderLastName, message, messageType, lastMsgTime);
        chat.entityName = entityName;
        chat.entityObjectId = entityObjectId;
        chat.imageUrl = imageUrl;
        chat.isReceived = true;
        if(messageType.equals("loc")) {
          if(lat == 0.1) {
            String[] mapUrlArr = message.split("@");
            String[] latLng = mapUrlArr[1].split(",");
            chat.lat = Double.parseDouble(latLng[0]);
            chat.lng = Double.parseDouble(latLng[1]);
          } else {
            chat.lat = lat;
            chat.lng = lng;
          }
        } else {
          chat.lat = lat;
          chat.lng = lng;
        }
        String entityCreatedAt = "1000000";
        ChatRoom chatRoom =
          new ChatRoom(entityType, entityObjectId, entityName, entityCreatedAt, senderId,
            senderFirstName, senderLastName, message, lastMsgTime, messageType, true);
        DataSource ds = new DataSource(ChatActivity.this);
        ds.open();
        ds.insertOrUpdateChatRoom(chatRoom, false);
        ds.close();
        newMessageArrived(chat);
      }

      @Override
      public void onChildChanged(DataSnapshot dataSnapshot, String s) {
      }

      @Override
      public void onChildRemoved(DataSnapshot dataSnapshot) {
      }

      @Override
      public void onChildMoved(DataSnapshot dataSnapshot, String s) {
      }

      @Override
      public void onCancelled(DatabaseError databaseError) {
      }
    };
    // attach listener to the node
    queryRef.addChildEventListener(childEventListener);
  }

  /**
   * Method to detach the Firebase Listener for the provided objectId
   */
  private void detachListener() {
    // Remove the listener for the reference node
    if(queryRef != null && childEventListener != null) {
      queryRef.removeEventListener(childEventListener);
    }
  }

  private class LoadMore {
    public long startAt;

    public LoadMore(long startAt) {
      this.startAt = startAt;
    }
  }

  private class DateTitle {
    public String date;

    public DateTitle(String date) {
      this.date = date;
    }
  }

  public class ChatListAdapter extends RecyclerView.Adapter<ChatListAdapter.ViewHolder> {
    private final int VIEW_TYPE_RECEIVED = 0;
    private final int VIEW_TYPE_SENT = 1;
    private final int VIEW_TYPE_DATE = 2;
    private final int VIEW_TYPE_LOAD_MORE = 3;
    private final int VIEW_TYPE_RECEIVED_LOCATION = 4;
    private final int VIEW_TYPE_SENT_LOCATION = 5;
    private final int VIEW_TYPE_RECEIVED_IMAGE = 6;
    private final int VIEW_TYPE_SENT_IMAGE = 7;
    public ArrayList<Object> arrayList;
    private final View.OnClickListener mOnClickListener = new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        int position = recyclerView.getChildAdapterPosition(v);
                /*if (arrayList.get(position) instanceof Chat) {
                    if (((Chat) arrayList.get(position)).msgType.equals("loc")) {
                        openMapForNavigation(((Chat) arrayList.get(position)).lat,
                                ((Chat) arrayList.get(position)).lng);
                    }
                } else*/
        if(arrayList.get(position) instanceof LoadMore && !isLoadingPreviousChatInProgress) {
          // make api call to load more messages
          loadPreviousChat(((LoadMore) arrayList.get(position)).startAt);
        }
      }
    };
    private Handler handler;

    // Provide a suitable constructor (depends on the kind of dataset)
    public ChatListAdapter(ArrayList<Object> arrayList) {
      this.arrayList = arrayList;
      handler = new Handler();
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
      // create a new view
      View v = null;
      if(viewType == VIEW_TYPE_RECEIVED) {
        v = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_msg_received, parent,
          false);
      } else if(viewType == VIEW_TYPE_RECEIVED_LOCATION) {
        v = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_msg_received_loc, parent,
          false);
      } else if(viewType == VIEW_TYPE_RECEIVED_IMAGE) {
        v = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_msg_received_img, parent,
          false);
      } else if(viewType == VIEW_TYPE_SENT) {
        v = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_msg_sent, parent, false);
      } else if(viewType == VIEW_TYPE_SENT_LOCATION) {
        v = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_msg_sent_loc, parent,
          false);
      } else if(viewType == VIEW_TYPE_SENT_IMAGE) {
        v = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_msg_sent_img, parent,
          false);
      } else if(viewType == VIEW_TYPE_LOAD_MORE) {
        v = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_load_more, parent,
          false);
      } else if(viewType == VIEW_TYPE_DATE) {
        v = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_date, parent, false);
      }
      // set the view's size, margins, padding and layout parameters
      ViewHolder vh = new ViewHolder(v, viewType);
      v.setOnClickListener(mOnClickListener);
      return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ViewHolder requestViewHolder, final int position) {
      // - get element from your dataset at this position
      // - replace the contents of the view with that element
      if(getItemViewType(position) == VIEW_TYPE_RECEIVED) {
        final Chat chat = (Chat) arrayList.get(position);
        requestViewHolder.txtName.setText(chat.senderFirstName + " " + chat.senderLastName);
        requestViewHolder.txtTime.setText(Singleton.getTimeInHour(chat.time));
        requestViewHolder.txtMessage.setText(chat.msg);
        //requestViewHolder.txtMessage.setMovementMethod(LinkMovementMethod.getInstance());
        //CardView.LayoutParams params = new CardView.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        //requestViewHolder.llContainer.setLayoutParams(params);
      } else if(getItemViewType(position) == VIEW_TYPE_SENT) {
        final Chat chat = (Chat) arrayList.get(position);
        requestViewHolder.txtTime.setText(Singleton.getTimeInHour(chat.time));
        requestViewHolder.txtMessage.setText(chat.msg);
        if(chat.isMessageSentToServer) {
          requestViewHolder.imgStatus.setImageResource(R.drawable.ic_tick);
        } else {
          requestViewHolder.imgStatus.setImageResource(R.drawable.ic_clock);
        }
        //requestViewHolder.txtMessage.setMovementMethod(LinkMovementMethod.getInstance());
        //RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        //requestViewHolder.llContainer.setLayoutParams(params);
        //CardView.LayoutParams params = new CardView.LayoutParams(200, ViewGroup.LayoutParams.WRAP_CONTENT);
        //requestViewHolder.llContainer.setLayoutParams(params);
      } else if(getItemViewType(position) == VIEW_TYPE_RECEIVED_LOCATION) {
        final Chat chat = (Chat) arrayList.get(position);
        requestViewHolder.txtName.setText(chat.senderFirstName + " " + chat.senderLastName);
        requestViewHolder.txtTime.setText(Singleton.getTimeInHour(chat.time));
        String mapUrl = MAP_URL + PARAM_MARKERS + chat.lat + "," + chat.lng +
                          PARAM_SEPARATOR + PARAM_SIZE + PARAM_SEPARATOR +
                          PARAM_SCALE + PARAM_SEPARATOR + PARAM_ZOOM +
                          PARAM_SEPARATOR + PARAM_KEY +
                          getString(R.string.google_directions_api_key);
        LogEvent.Log(TAG, "mapUrl: " + mapUrl);
        SingletonImageFactory.getImageLoader().loadImage(mapUrl, new ImageLoadingListener() {
          @Override
          public void onLoadingStarted(String s, View view) {
          }

          @Override
          public void onLoadingFailed(String s, View view, FailReason failReason) {
          }

          @Override
          public void onLoadingComplete(String s, View view, Bitmap bitmap) {
            requestViewHolder.imgMap.setImageBitmap(bitmap);
          }

          @Override
          public void onLoadingCancelled(String s, View view) {
          }
        });
        requestViewHolder.imgMap.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View view) {
            String name;
            if(chat.senderId.equals(ParseUser.getCurrentUser().getObjectId())) {
              name = getString(R.string.chat_user_you);
            } else {
              name = chat.senderFirstName + " " + chat.senderLastName;
            }
            openMapForNavigation(chat.lat, chat.lng, name);
          }
        });
      } else if(getItemViewType(position) == VIEW_TYPE_SENT_LOCATION) {
        final Chat chat = (Chat) arrayList.get(position);
        requestViewHolder.txtTime.setText(Singleton.getTimeInHour(chat.time));
        String mapUrl = MAP_URL + PARAM_MARKERS + chat.lat + "," + chat.lng +
                          PARAM_SEPARATOR + PARAM_SIZE + PARAM_SEPARATOR +
                          PARAM_SCALE + PARAM_SEPARATOR + PARAM_ZOOM +
                          PARAM_SEPARATOR + PARAM_KEY +
                          getString(R.string.google_directions_api_key);
        LogEvent.Log(TAG, "mapUrl: " + mapUrl);
        SingletonImageFactory.getImageLoader().loadImage(mapUrl, new ImageLoadingListener() {
          @Override
          public void onLoadingStarted(String s, View view) {
          }

          @Override
          public void onLoadingFailed(String s, View view, FailReason failReason) {
          }

          @Override
          public void onLoadingComplete(String s, View view, Bitmap bitmap) {
            requestViewHolder.imgMap.setImageBitmap(bitmap);
          }

          @Override
          public void onLoadingCancelled(String s, View view) {
          }
        });
        if(chat.isMessageSentToServer) {
          requestViewHolder.imgStatus.setImageResource(R.drawable.ic_tick);
        } else {
          requestViewHolder.imgStatus.setImageResource(R.drawable.ic_clock);
        }
        requestViewHolder.imgMap.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View view) {
            String name;
            if(chat.senderId.equals(ParseUser.getCurrentUser().getObjectId())) {
              name = getString(R.string.chat_user_you);
            } else {
              name = chat.senderFirstName + " " + chat.senderLastName;
            }
            openMapForNavigation(chat.lat, chat.lng, name);
          }
        });
      } else if(getItemViewType(position) == VIEW_TYPE_RECEIVED_IMAGE) {
        requestViewHolder.img.setImageBitmap(null);
        final Chat chat = (Chat) arrayList.get(position);
        requestViewHolder.txtName.setText(chat.senderFirstName + " " + chat.senderLastName);
        requestViewHolder.txtTime.setText(Singleton.getTimeInHour(chat.time));
        LogEvent.Log(TAG, "msg: " + chat.msg);
        LogEvent.Log(TAG, "imageUrl: " + chat.imageUrl);
        SingletonImageFactory.getImageLoader().loadImage(chat.imageUrl, new ImageLoadingListener() {
          @Override
          public void onLoadingStarted(String s, View view) {
          }

          @Override
          public void onLoadingFailed(String s, View view, FailReason failReason) {
          }

          @Override
          public void onLoadingComplete(String s, View view, Bitmap bitmap) {
            requestViewHolder.img.setImageBitmap(bitmap);
            if(!chat.isImageDownloadedFromServer) {
              chat.isImageDownloadedFromServer = true;
              handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                  notifyItemChanged(position);
                }
              }, 50);
            }
          }

          @Override
          public void onLoadingCancelled(String s, View view) {
          }
        });
        if(chat.isImageDownloadedFromServer) {
          requestViewHolder.pbImg.setVisibility(View.GONE);
        } else {
          requestViewHolder.pbImg.setVisibility(View.VISIBLE);
        }
        requestViewHolder.img.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View view) {
            Intent myIntent = new Intent(ChatActivity.this, ImageScreenActivity.class);
            myIntent.putExtra("imageUrl", chat.imageUrl);
            startActivity(myIntent);
          }
        });
      } else if(getItemViewType(position) == VIEW_TYPE_SENT_IMAGE) {
        requestViewHolder.img.setImageBitmap(null);
        final Chat chat = (Chat) arrayList.get(position);
        requestViewHolder.txtTime.setText(Singleton.getTimeInHour(chat.time));
        LogEvent.Log(TAG, "msg: " + chat.msg);
        LogEvent.Log(TAG, "path: " + chat.path);
        LogEvent.Log(TAG, "imageUrl: " + chat.imageUrl);
        if(chat.imageUrl == null) {
          Uri file = Uri.fromFile(new File(chat.path));
          requestViewHolder.img.setImageURI(file);
          if(chat.isImageUploadedToServer) {
            requestViewHolder.pbImg.setVisibility(View.GONE);
          } else {
            requestViewHolder.pbImg.setVisibility(View.VISIBLE);
          }
        } else {
          if(chat.path != null) {
            Uri file = Uri.fromFile(new File(chat.path));
            requestViewHolder.img.setImageURI(file);
          }
          if(chat.isImageDownloadedFromServer) {
            requestViewHolder.pbImg.setVisibility(View.GONE);
          } else {
            requestViewHolder.pbImg.setVisibility(View.VISIBLE);
          }
          SingletonImageFactory.getImageLoader()
            .loadImage(chat.imageUrl, new ImageLoadingListener() {
              @Override
              public void onLoadingStarted(String s, View view) {
              }

              @Override
              public void onLoadingFailed(String s, View view, FailReason failReason) {
              }

              @Override
              public void onLoadingComplete(String s, View view, Bitmap bitmap) {
                requestViewHolder.img.setImageBitmap(bitmap);
                if(!chat.isImageDownloadedFromServer) {
                  chat.isImageDownloadedFromServer = true;
                  chat.path = null;
                  handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                      notifyItemChanged(position);
                    }
                  }, 50);
                }
              }

              @Override
              public void onLoadingCancelled(String s, View view) {
              }
            });
        }
        if(chat.isMessageSentToServer) {
          requestViewHolder.imgStatus.setImageResource(R.drawable.ic_tick);
        } else {
          requestViewHolder.imgStatus.setImageResource(R.drawable.ic_clock);
        }
        requestViewHolder.img.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View view) {
            Intent myIntent = new Intent(ChatActivity.this, ImageScreenActivity.class);
            myIntent.putExtra("path", chat.path);
            myIntent.putExtra("imageUrl", chat.imageUrl);
            startActivity(myIntent);
          }
        });
      } else if(getItemViewType(position) == VIEW_TYPE_DATE) {
        final DateTitle dateTitle = (DateTitle) arrayList.get(position);
        requestViewHolder.txtDate.setText(dateTitle.date);
      } else if(getItemViewType(position) == VIEW_TYPE_LOAD_MORE) {
      }
    }

    // Return the size of your data set (invoked by the layout manager)
    @Override
    public int getItemCount() {
      return arrayList.size();
    }

    @Override
    public int getItemViewType(int position) {
      super.getItemViewType(position);
      if(arrayList.get(position) instanceof Chat) {
        if(((Chat) arrayList.get(position)).isReceived) {
          if(((Chat) arrayList.get(position)).msgType.equals("loc")) {
            return VIEW_TYPE_RECEIVED_LOCATION;
          } else if(((Chat) arrayList.get(position)).msgType.equals("img")) {
            return VIEW_TYPE_RECEIVED_IMAGE;
          } else {
            return VIEW_TYPE_RECEIVED;
          }
        } else {
          if(((Chat) arrayList.get(position)).msgType.equals("loc")) {
            return VIEW_TYPE_SENT_LOCATION;
          } else if(((Chat) arrayList.get(position)).msgType.equals("img")) {
            return VIEW_TYPE_SENT_IMAGE;
          } else {
            return VIEW_TYPE_SENT;
          }
        }
      } else if(arrayList.get(position) instanceof LoadMore) {
        return VIEW_TYPE_LOAD_MORE;
      } else {
        return VIEW_TYPE_DATE;
      }
    }

    private void openMapForNavigation(double lat, double lng, String name) {
      try {
        Uri gmmIntentUri =
          Uri.parse("geo:q=" + lat + "," + lng + "?q=" + +lat + "," + lng + "(" + name + ")");
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
        mapIntent.setPackage("com.google.android.apps.maps");
        startActivity(mapIntent);
      } catch(ActivityNotFoundException e) {
        e.printStackTrace();
        if(weakRef.get() != null && !weakRef.get().isFinishing()) {
          getApplicationContext();
          Singleton.sendToast(R.string.maps_app_not_installed);
        }
      }
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public class ViewHolder extends RecyclerView.ViewHolder {
      // each data item is just a string in this case
      private TextView txtName;
      private TextView txtMessage;
      private TextView txtTime;
      private TextView txtLoadMore;
      private TextView txtDate;
      private ImageView imgMap;
      private ImageView img;
      private ImageView imgStatus;
      private ProgressBar pbImg;
      //private SupportMapFragment mapFragment;
      private CardView llContainer;

      public ViewHolder(View view, int type) {
        super(view);
        if(type == VIEW_TYPE_RECEIVED) {
          llContainer = (CardView) view.findViewById(R.id.card_view);
          txtName = (TextView) view.findViewById(R.id.txt_name);
          txtMessage = (TextView) view.findViewById(R.id.txt_msg);
          txtTime = (TextView) view.findViewById(R.id.txt_msg_time);
        } else if(type == VIEW_TYPE_RECEIVED_LOCATION) {
          llContainer = (CardView) view.findViewById(R.id.card_view);
          txtName = (TextView) view.findViewById(R.id.txt_name);
          imgMap = (ImageView) view.findViewById(R.id.img_map);
          txtTime = (TextView) view.findViewById(R.id.txt_msg_time);
        } else if(type == VIEW_TYPE_RECEIVED_IMAGE) {
          llContainer = (CardView) view.findViewById(R.id.card_view);
          txtName = (TextView) view.findViewById(R.id.txt_name);
          img = (ImageView) view.findViewById(R.id.img);
          txtTime = (TextView) view.findViewById(R.id.txt_msg_time);
          pbImg = (ProgressBar) view.findViewById(R.id.pb_img);
        } else if(type == VIEW_TYPE_SENT) {
          llContainer = (CardView) view.findViewById(R.id.card_view);
          txtMessage = (TextView) view.findViewById(R.id.txt_msg);
          txtTime = (TextView) view.findViewById(R.id.txt_msg_time);
          imgStatus = (ImageView) view.findViewById(R.id.img_status);
        } else if(type == VIEW_TYPE_SENT_LOCATION) {
          llContainer = (CardView) view.findViewById(R.id.card_view);
          imgMap = (ImageView) view.findViewById(R.id.img_map);
          txtTime = (TextView) view.findViewById(R.id.txt_msg_time);
          imgStatus = (ImageView) view.findViewById(R.id.img_status);
        } else if(type == VIEW_TYPE_SENT_IMAGE) {
          llContainer = (CardView) view.findViewById(R.id.card_view);
          img = (ImageView) view.findViewById(R.id.img);
          txtTime = (TextView) view.findViewById(R.id.txt_msg_time);
          imgStatus = (ImageView) view.findViewById(R.id.img_status);
          pbImg = (ProgressBar) view.findViewById(R.id.pb_img);
        } else if(type == VIEW_TYPE_LOAD_MORE) {
          txtLoadMore = (TextView) view.findViewById(R.id.txt_load_more);
        } else if(type == VIEW_TYPE_DATE) {
          txtDate = (TextView) view.findViewById(R.id.txt_date);
        }
      }
    }
  }
}