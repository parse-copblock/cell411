package cell411.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseRelation;
import com.parse.ParseUser;
import com.parse.callback.FindCallback;
import com.safearx.cell411.R;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import cell411.Singleton;
import cell411.constants.Alert;
import cell411.models.Cell;
import cell411.models.Cell411Alert;
import cell411.models.ChatRoom;
import cell411.models.Footer;
import cell411.utils.LogEvent;
import cell411.widgets.CircularImageView;

import static cell411.SingletonConfig.DEFAULT_ALERTS_COUNT;
import static cell411.SingletonConfig.TIME_TO_LIVE_FOR_CHAT_ON_ALERTS;

/**
 * Created by Sachin on 27-03-2017.
 */
public class NewChatActivity extends BaseActivity {
  private static final String TAG = "NewChatActivity.java";
  public static WeakReference<NewChatActivity> weakRef;
  private final int LIMIT_PER_PAGE = 10;
  private RecyclerView recyclerView;
  private LinearLayoutManager linearLayoutManager;
  private CellListAdapter adapterCell;
  private ArrayList<Object> newChatItemsList;
  private List<ParseObject> cell411AlertsList4JoinedCells;
  private int page4JoinedCells = 0;
  private boolean noMoreData4JoinedCell = false;
  private boolean loadingFirstPageOfJoinedCells = true;
  private boolean apiCallInProgress4JoinedCell = false;
  private Handler handler = new Handler();
  private boolean loading = true;
  private int pastVisibleItems, visibleItemCount, totalItemCount;
  private boolean isAlertsNotAvailable;
  private boolean isOwnedCellsNotAvailable;
  private boolean isJoinedCellsNotAvailable;
  private TextView txtEmpty;

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    switch(item.getItemId()) {
      case android.R.id.home:
        finish();
        return true;
      default:
        return super.onOptionsItemSelected(item);
    }
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_new_chat);
    weakRef = new WeakReference<>(this);
    Singleton.INSTANCE.setCurrentActivity(this);
    getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    recyclerView = (RecyclerView) findViewById(R.id.rv_new_chat);
    // use this setting to improve performance if you know that changes
    // in content do not change the layout size of the RecyclerView
    recyclerView.setHasFixedSize(true);
    // use a linear layout manager
    linearLayoutManager = new LinearLayoutManager(this);
    recyclerView.setLayoutManager(linearLayoutManager);
    txtEmpty = (TextView) findViewById(R.id.txt_empty);
    newChatItemsList = new ArrayList<>();
    newChatItemsList.add(
      new NewChatItemTitle(getString(R.string.title_alerts), NewChatItemTitle.ItemType.ALERTS));
    newChatItemsList.add(new NewChatItemTitle(getString(R.string.owned_cells),
      NewChatItemTitle.ItemType.OWNED_CELLS));
    newChatItemsList.add(new NewChatItemTitle(getString(R.string.joined_cells),
      NewChatItemTitle.ItemType.JOINED_CELLS));
    adapterCell = new CellListAdapter(newChatItemsList);
    recyclerView.setAdapter(adapterCell);
    // Check if the user is already marked as spam
    ParseRelation relation4SpamUsers = ParseUser.getCurrentUser().getRelation("spamUsers");
    final ParseQuery<ParseUser> parseQuery4SpamUsers = relation4SpamUsers.getQuery();
    parseQuery4SpamUsers.findInBackground(new FindCallback<ParseUser>() {
      @Override
      public void done(List<ParseUser> list, ParseException e) {
        if(e == null) {
          if(list != null) {
            for(int i = 0; i < list.size(); i++) {
              Singleton.INSTANCE.getSpammedUsersIdList().
                                                          add(list.get(i).getObjectId());
            }
          }
          retrieveCell411Alerts();
          retrieveOwnerPublicCells();
          retrieveJoinedPublicCells();
        } else {
          if(weakRef.get() != null && !weakRef.get().isFinishing()) {
            Singleton.sendToast(e);
          }
        }
      }
    });
  }

  private void retrieveCell411Alerts() {
    long alertTime = System.currentTimeMillis() - TIME_TO_LIVE_FOR_CHAT_ON_ALERTS;
    Date date = new Date(alertTime);
    ParseQuery<ParseObject> parseQuery4Cell411Alerts = ParseQuery.getQuery("Cell411Alert");
    parseQuery4Cell411Alerts.whereEqualTo("targetMembers", ParseUser.getCurrentUser());
    parseQuery4Cell411Alerts.whereExists("issuedBy");
    ParseQuery<ParseObject> parseQuery4Cell411ForwardedAlerts = ParseQuery.getQuery("Cell411Alert");
    parseQuery4Cell411ForwardedAlerts.whereEqualTo("forwardedToMembers",
      ParseUser.getCurrentUser());
    parseQuery4Cell411ForwardedAlerts.whereExists("issuedBy");
    ParseQuery<ParseObject> parseQuery4Cell411PublicAlerts = ParseQuery.getQuery("Cell411Alert");
    parseQuery4Cell411PublicAlerts.whereEqualTo("cellMembers", ParseUser.getCurrentUser());
    parseQuery4Cell411PublicAlerts.whereExists("issuedBy");
    ParseQuery<ParseObject> parseQuery4Cell411Alerts2 = ParseQuery.getQuery("Cell411Alert");
    parseQuery4Cell411Alerts2.whereEqualTo("audienceAU", ParseUser.getCurrentUser());
    parseQuery4Cell411Alerts2.whereExists("issuedBy");
    ParseQuery<ParseObject> parseQuery4SelfCell411Alerts = ParseQuery.getQuery("Cell411Alert");
    parseQuery4SelfCell411Alerts.whereEqualTo("issuedBy", ParseUser.getCurrentUser());
    parseQuery4SelfCell411Alerts.whereDoesNotExist("to");
    parseQuery4SelfCell411Alerts.whereExists("alertType");
    ParseQuery<ParseObject> parseQuery4SelfCell411Alerts2 = ParseQuery.getQuery("Cell411Alert");
    parseQuery4SelfCell411Alerts2.whereEqualTo("issuedBy", ParseUser.getCurrentUser());
    parseQuery4SelfCell411Alerts2.whereDoesNotExist("to");
    parseQuery4SelfCell411Alerts2.whereExists("alertId");
    ParseQuery<ParseObject> parseQuery4SelfForwardedCell411Alerts =
      ParseQuery.getQuery("Cell411Alert");
    parseQuery4SelfForwardedCell411Alerts.whereEqualTo("forwardedBy", ParseUser.getCurrentUser());
    parseQuery4SelfForwardedCell411Alerts.whereDoesNotExist("to");
    parseQuery4SelfForwardedCell411Alerts.whereExists("alertType");
    ParseQuery<ParseObject> parseQuery4SelfForwardedCell411Alerts2 =
      ParseQuery.getQuery("Cell411Alert");
    parseQuery4SelfForwardedCell411Alerts2.whereEqualTo("forwardedBy", ParseUser.getCurrentUser());
    parseQuery4SelfForwardedCell411Alerts2.whereDoesNotExist("to");
    parseQuery4SelfForwardedCell411Alerts2.whereExists("alertId");
    List<ParseQuery<ParseObject>> queries = new ArrayList<>();
    queries.add(parseQuery4Cell411Alerts);
    queries.add(parseQuery4Cell411ForwardedAlerts);
    queries.add(parseQuery4Cell411PublicAlerts);
    queries.add(parseQuery4Cell411Alerts2);
    queries.add(parseQuery4SelfCell411Alerts);
    queries.add(parseQuery4SelfCell411Alerts2);
    queries.add(parseQuery4SelfForwardedCell411Alerts);
    queries.add(parseQuery4SelfForwardedCell411Alerts2);
    ParseQuery<ParseObject> mainQuery = ParseQuery.or(queries);
    mainQuery.include("issuedBy");
    mainQuery.include("forwardedBy");
    mainQuery.orderByDescending("createdAt");
    mainQuery.setLimit(DEFAULT_ALERTS_COUNT);
    mainQuery.whereGreaterThan("createdAt", date);
    mainQuery.findInBackground(new FindCallback<ParseObject>() {
      public void done(List<ParseObject> results, ParseException e) {
        if(e == null) {
          ArrayList<Cell411Alert> cell411AlertsList = new ArrayList<Cell411Alert>();
          if(results != null && results.size() > 0) {
            cell411AlertsList = new ArrayList<>();
            GregorianCalendar cal = new GregorianCalendar();
            GregorianCalendar calCurrentTime = new GregorianCalendar();
            calCurrentTime.setTimeInMillis(System.currentTimeMillis());
            calCurrentTime.set(Calendar.HOUR, 0);
            calCurrentTime.set(Calendar.MINUTE, 0);
            calCurrentTime.set(Calendar.SECOND, 0);
            calCurrentTime.set(Calendar.MILLISECOND, 0);
            for(int i = 0; i < results.size(); i++) {
              ParseObject cell411Obj = results.get(i);
              ParseUser issuer = (ParseUser) cell411Obj.get("issuedBy");
              if(issuer == null || issuer.getObjectId() == null) {
                continue;
              }
              String additionalNote = (String) cell411Obj.get("additionalNote");
              String issuerEmail = null;
              if(cell411Obj.get("issuedBy") != null) {
                String email = null;
                if(((ParseUser) cell411Obj.get("issuedBy")).getEmail() != null &&
                     !((ParseUser) cell411Obj.get("issuedBy")).getEmail().isEmpty()) {
                  email = ((ParseUser) cell411Obj.get("issuedBy")).getEmail();
                } else {
                  email = ((ParseUser) cell411Obj.get("issuedBy")).getUsername();
                }
                issuerEmail = email;
              }
              String firstName = (String) issuer.get("firstName");
              String lastName = (String) issuer.get("lastName");
              String issuerName = firstName + " " + lastName;
              String forwarderEmail = null;
              if(cell411Obj.get("forwardedBy") != null) {
                String email = null;
                if(((ParseUser) cell411Obj.get("forwardedBy")).getEmail() != null &&
                     !((ParseUser) cell411Obj.get("forwardedBy")).getEmail().isEmpty()) {
                  email = ((ParseUser) cell411Obj.get("forwardedBy")).getEmail();
                } else {
                  email = ((ParseUser) cell411Obj.get("forwardedBy")).getUsername();
                }
                forwarderEmail = email;
              }
              ParseUser forwarder = (ParseUser) cell411Obj.get("forwardedBy");
              String forwardedBy = null;
              if(cell411Obj.get("forwardedBy") != null) {
                forwardedBy = forwarder.get("firstName") + " " + forwarder.get("lastName");
              }
              Cell411Alert.AlertType alertTypeEnum;
              if(cell411Obj.get("alertId") != null) {
                int alertId = cell411Obj.getInt("alertId");
                alertTypeEnum = Cell411Alert.AlertType.values()[alertId];
              } else {
                String alertType = (String) cell411Obj.get("alertType");
                alertTypeEnum = convertStringToEnum(alertType);
              }
              String cellName = (String) cell411Obj.get("cellName");
              String entryFor = (String) cell411Obj.get("entryFor");
              String status = (String) cell411Obj.get("status");
              long millis = cell411Obj.getCreatedAt().getTime();
              cal.setTimeInMillis(millis);
              int year = cal.get(Calendar.YEAR);
              int month = cal.get(Calendar.MONTH) + 1;
              int day = cal.get(Calendar.DAY_OF_MONTH);
              int hour = cal.get(Calendar.HOUR);
              int minute = cal.get(Calendar.MINUTE);
              int ampm = cal.get(Calendar.AM_PM);
              String ap;
              if(ampm == 0) {
                ap = getString(R.string.am);
              } else {
                ap = getString(R.string.pm);
              }
              if(hour == 0) {
                hour = 12;
              }
              String hourMinute = "" + hour;
              if(minute > 0 && minute < 10) {
                hourMinute += ":0" + minute;
              } else if(minute > 9) {
                hourMinute += ":" + minute;
              }
              String time;
              if(cal.getTimeInMillis() < calCurrentTime.getTimeInMillis()) {
                // Check if the alert was issued yesterday
                time = month + "/" + day + "/" + year + " " + getString(R.string.at) + " " +
                         hourMinute + " " + ap;
              } else {
                time = getString(R.string.today_at) + " " + hourMinute + " " + ap;
              }
              boolean isVideo = false;
              boolean isPhoto = false;
              boolean isMedical = false;
              boolean isFriendRequest = false;
              boolean isCellRequest = false;
              double lat = 0;
              double lng = 0;
              String fullAddress = null;
              String city = null;
              String country = null;
              if(entryFor == null || entryFor.equals("")) {
                if(cell411Obj.getParseGeoPoint("location") != null) {
                  lat = cell411Obj.getParseGeoPoint("location").getLatitude();
                  lng = cell411Obj.getParseGeoPoint("location").getLongitude();
                }
                fullAddress = cell411Obj.getString("fullAddress");
                city = cell411Obj.getString("city");
                country = cell411Obj.getString("country");
                if(alertTypeEnum == Cell411Alert.AlertType.VIDEO) {
                  isVideo = true;
                  if(!status.equals("VOD") &&
                       issuer.getObjectId().equals(ParseUser.getCurrentUser().getObjectId())) {
                    status = "VOD";
                    cell411Obj.put("status", "VOD");
                    cell411Obj.saveEventually();
                  }
                }
                if(alertTypeEnum == Cell411Alert.AlertType.PHOTO) {
                  isPhoto = true;
                }
                if(alertTypeEnum == Cell411Alert.AlertType.MEDICAL) {
                  isMedical = true;
                }
              } else {
                if(entryFor.equals("CR")) {
                  isCellRequest = true;
                } else {
                  isFriendRequest = true;
                }
              }
              boolean selfAlert = false;
              if(issuer.getObjectId().equals(ParseUser.getCurrentUser().getObjectId())) {
                // This alert is issued by the current user
                selfAlert = true;
              }
              Cell411Alert cell411Alert =
                new Cell411Alert(issuerName, issuer, issuerEmail, forwarder, isVideo, status,
                  millis, selfAlert, isPhoto, cell411Obj.getObjectId(), additionalNote, lat,
                  lng, isFriendRequest, isCellRequest, isMedical, forwardedBy, alertTypeEnum,
                  time, cellName, fullAddress, city, country);
              if(issuer == null || issuer.equals("") || issuer.equals("null")) {
                cell411Alert.isCustomAlert = true;
              }
              cell411AlertsList.add(cell411Alert);
                            /*if (issuer == null || issuer.equals("") || issuer.equals("null")) {
                                Cell411Alert cell411Alert = new Cell411Alert(issuerName, issuer, issuerEmail,
                                        isVideo, status, millis, false, isPhoto, cell411Obj.getObjectId(),
                                        additionalNote, lat, lng, isFriendRequest, isCellRequest, isMedical, forwardedBy,
                                        alertTypeEnum, time, cellName, fullAddress, city, country);
                                cell411Alert.isCustomAlert = true;
                                cell411AlertsList.add(cell411Alert);
                            } else if (issuer.getObjectId().equals(ParseUser.getCurrentUser().getObjectId())) {
                                // This alert is issued by the current user
                                cell411AlertsList.add(new Cell411Alert(issuerName, issuer, issuerEmail, isVideo,
                                        status, millis, true, isPhoto, cell411Obj.getObjectId(),
                                        additionalNote, lat, lng, isFriendRequest, isCellRequest, isMedical, forwardedBy,
                                        alertTypeEnum, time, cellName, fullAddress, city, country));
                            } else if (!issuer.getObjectId().equals(ParseUser.getCurrentUser().getObjectId())) {
                                if (forwardedBy != null) {
                                    cell411AlertsList.add(new Cell411Alert(issuerName, forwarder, forwarderEmail,
                                            isVideo, status, millis, false, isPhoto, cell411Obj.getObjectId(),
                                            additionalNote, lat, lng, isFriendRequest, isCellRequest, isMedical, forwardedBy,
                                            alertTypeEnum, time, cellName, fullAddress, city, country));
                                } else {
                                    cell411AlertsList.add(new Cell411Alert(issuerName, issuer, issuerEmail,
                                            isVideo, status, millis, false, isPhoto, cell411Obj.getObjectId(),
                                            additionalNote, lat, lng, isFriendRequest, isCellRequest, isMedical, forwardedBy,
                                            alertTypeEnum, time, cellName, fullAddress, city, country));
                                }
                            }*/
            }
            if(weakRef.get() != null && !weakRef.get().isFinishing()) {
              ((NewChatItemTitle) newChatItemsList.get(0)).itemLoaded = true;
              newChatItemsList.addAll(1, cell411AlertsList);
              adapterCell.notifyDataSetChanged();
            }
          } else {
            isAlertsNotAvailable = true;
            // No alerts founds within 72 hours
            if(weakRef.get() != null && !weakRef.get().isFinishing()) {
              LogEvent.Log(TAG, "No alerts found within 72 hours");
              newChatItemsList.remove(0);
              adapterCell.notifyItemRemoved(0);
              // Display empty text, since no any chat room exists
              if(isOwnedCellsNotAvailable && isJoinedCellsNotAvailable) {
                txtEmpty.setVisibility(View.VISIBLE);
              }
            }
          }
        } else {
          if(weakRef.get() != null && !weakRef.get().isFinishing()) {
            Singleton.sendToast(e);
          }
        }
      }
    });
  }

  private void retrieveOwnerPublicCells() {
    ParseQuery<ParseObject> cellQuery = ParseQuery.getQuery("PublicCell");
    cellQuery.whereEqualTo("createdBy", ParseUser.getCurrentUser());
    cellQuery.findInBackground(new FindCallback<ParseObject>() {
      @Override
      public void done(List<ParseObject> parseObjects, ParseException e) {
        if(e == null) {
          ArrayList<Cell> cellsList = new ArrayList<>();
          if(parseObjects != null) {
            for(int i = 0; i < parseObjects.size(); i++) {
              ParseObject cell = parseObjects.get(i);
              Cell cl = new Cell((String) cell.get("name"), cell);
              cl.totalMembers = (int) cell.get("totalMembers");
              cl.verificationStatus = (int) cell.get("verificationStatus");
              cl.status = Cell.Status.OWNER;
              cellsList.add(cl);
            }
          }
          if(weakRef.get() != null && !weakRef.get().isFinishing()) {
            if(cellsList.size() > 0) {
              if(weakRef.get() != null && !weakRef.get().isFinishing()) {
                // find the start index of the owned newPrivateCell section
                int index = 0;
                for(; index < newChatItemsList.size(); index++) {
                  if(newChatItemsList.get(index) instanceof NewChatItemTitle &&
                       ((NewChatItemTitle) newChatItemsList.get(index)).itemType ==
                         NewChatItemTitle.ItemType.OWNED_CELLS) {
                    LogEvent.Log(TAG, "index: " + index);
                    break;
                  }
                }
                LogEvent.Log(TAG, "index: " + index);
                ((NewChatItemTitle) newChatItemsList.get(index)).itemLoaded = true;
                newChatItemsList.addAll(index + 1, cellsList);
                adapterCell.notifyDataSetChanged();
              }
            } else {
              isOwnedCellsNotAvailable = true;
              // No owned public cells
              LogEvent.Log(TAG, "No alerts found within 72 hours");
              if(weakRef.get() != null && !weakRef.get().isFinishing()) {
                int index = 0;
                for(; index < newChatItemsList.size(); index++) {
                  if(newChatItemsList.get(index) instanceof NewChatItemTitle &&
                       ((NewChatItemTitle) newChatItemsList.get(index)).itemType ==
                         NewChatItemTitle.ItemType.OWNED_CELLS) {
                    LogEvent.Log(TAG, "index: " + index);
                    break;
                  }
                }
                newChatItemsList.remove(index);
                adapterCell.notifyItemRemoved(index);
                // Display empty text, since no any chatroom exists
                if(isAlertsNotAvailable && isJoinedCellsNotAvailable) {
                  txtEmpty.setVisibility(View.VISIBLE);
                }
              }
            }
          }
        } else {
          if(weakRef.get() != null && !weakRef.get().isFinishing()) {
            Singleton.sendToast(e);
          }
        }
      }
    });
  }

  private void retrieveJoinedPublicCells() {
    ParseQuery<ParseObject> cell411Query = new ParseQuery<>("Cell411Alert");
    cell411Query.whereEqualTo("issuedBy", ParseUser.getCurrentUser());
    cell411Query.whereEqualTo("entryFor", "CR");
    cell411Query.whereEqualTo("status", "APPROVED");
    cell411Query.findInBackground(new FindCallback<ParseObject>() {
      @Override
      public void done(List<ParseObject> list, ParseException e) {
        if(e == null) {
          if(list == null) {
            cell411AlertsList4JoinedCells = new ArrayList<>();
          } else {
            cell411AlertsList4JoinedCells = list;
          }
          retrievePublicCells();
        } else {
          if(weakRef.get() != null && !weakRef.get().isFinishing()) {
            Singleton.sendToast(e);
          }
        }
      }
    });
  }

  private void retrievePublicCells() {
    ParseQuery<ParseObject> cellQuery = ParseQuery.getQuery("PublicCell");
    cellQuery.whereNotEqualTo("createdBy", ParseUser.getCurrentUser());
    cellQuery.include("createdBy");
    cellQuery.whereEqualTo("members", ParseUser.getCurrentUser());
    cellQuery.addDescendingOrder("totalMembers");
    cellQuery.setLimit(LIMIT_PER_PAGE);
    cellQuery.setSkip(LIMIT_PER_PAGE * page4JoinedCells);
    cellQuery.findInBackground(new FindCallback<ParseObject>() {
      @Override
      public void done(List<ParseObject> parseObjects, ParseException e) {
        if(e == null) {
          ArrayList<Cell> cellsList = new ArrayList<>();
          if(parseObjects != null && parseObjects.size() > 0) {
            for(int i = 0; i < parseObjects.size(); i++) {
              ParseObject cell = parseObjects.get(i);
              Cell cl = new Cell((String) cell.get("name"), cell);
              cl.totalMembers = (int) cell.get("totalMembers");
              cl.verificationStatus = (int) cell.get("verificationStatus");
              cl.status = Cell.Status.JOINED;
              LogEvent.Log(TAG, "verificationStatus: " + cl.verificationStatus);
              for(int j = 0; j < cell411AlertsList4JoinedCells.size(); j++) {
                if(cell411AlertsList4JoinedCells.get(j).get("cellId").equals(cell.getObjectId())) {
                  cl.cell411AlertId = cell411AlertsList4JoinedCells.get(j).getObjectId();
                  break;
                }
              }
              cellsList.add(cl);
            }
            if(parseObjects.size() < 10) {
              noMoreData4JoinedCell = true;
            } else {
              page4JoinedCells++;
            }
            if(loadingFirstPageOfJoinedCells) {
              // find the start index of the owned newPrivateCell section
              int index = 0;
              for(; index < newChatItemsList.size(); index++) {
                if(newChatItemsList.get(index) instanceof NewChatItemTitle &&
                     ((NewChatItemTitle) newChatItemsList.get(index)).itemType ==
                       NewChatItemTitle.ItemType.JOINED_CELLS) {
                  break;
                }
              }
              ((NewChatItemTitle) newChatItemsList.get(index)).itemLoaded = true;
              newChatItemsList.addAll(index + 1, cellsList);
              loadingFirstPageOfJoinedCells = false;
              if(!noMoreData4JoinedCell) {
                // Add footer to the list that will display a progressbar
                newChatItemsList.add(new Footer(getString(R.string.load_more), true));
              } else {
                // Add footer to the list that will display a progressbar
                newChatItemsList.add(new Footer(getString(R.string.no_more_cells_to_load), false));
              }
              setJoinedCellListScrollListener();
            } else {
              newChatItemsList.addAll(newChatItemsList.size() - 1, cellsList);
              ((Footer) adapterCell.arrayList.get(
                adapterCell.arrayList.size() - 1)).spinnerVisibility = false;
            }
            if(weakRef.get() != null && !weakRef.get().isFinishing() &&
                 NewChatActivity.this != null) {
              adapterCell.notifyDataSetChanged();
            }
          } else {
            isJoinedCellsNotAvailable = true;
            // No joined cells
            if(loadingFirstPageOfJoinedCells) {
              LogEvent.Log(TAG, "No alerts found within 72 hours");
              if(weakRef.get() != null && !weakRef.get().isFinishing()) {
                int index = 0;
                for(; index < newChatItemsList.size(); index++) {
                  if(newChatItemsList.get(index) instanceof NewChatItemTitle &&
                       ((NewChatItemTitle) newChatItemsList.get(index)).itemType ==
                         NewChatItemTitle.ItemType.JOINED_CELLS) {
                    LogEvent.Log(TAG, "index: " + index);
                    break;
                  }
                }
                newChatItemsList.remove(index);
                adapterCell.notifyItemRemoved(index);
                // Display empty text, since no any chatroom exists
                if(isAlertsNotAvailable && isOwnedCellsNotAvailable) {
                  txtEmpty.setVisibility(View.VISIBLE);
                }
              }
            }
          }
          if(weakRef.get() != null && !weakRef.get().isFinishing() &&
               NewChatActivity.this != null) {
            if(noMoreData4JoinedCell && !loadingFirstPageOfJoinedCells) {
              recyclerView.clearOnScrollListeners();
              ((Footer) adapterCell.arrayList.get(adapterCell.arrayList.size() - 1)).info =
                getString(R.string.no_more_cells_to_load);
              ((Footer) adapterCell.arrayList.get(
                adapterCell.arrayList.size() - 1)).spinnerVisibility = false;
              adapterCell.notifyDataSetChanged();
            }
          }
        } else {
          if(weakRef.get() != null && !weakRef.get().isFinishing()) {
            Singleton.sendToast(e);
          }
        }
      }
    });
  }

  private void setJoinedCellListScrollListener() {
    recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
      @Override
      public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        if(dy > 0) { //check for scroll down
          visibleItemCount = linearLayoutManager.getChildCount();
          totalItemCount = linearLayoutManager.getItemCount();
          pastVisibleItems = linearLayoutManager.findFirstVisibleItemPosition();
          if(loading) {
            if((visibleItemCount + pastVisibleItems) >= totalItemCount) {
              loading = false;
              LogEvent.Log("...", "Last Item Wow !");
              //Do pagination.. i.e. fetch new data
              handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                  loadMoreJoinedCells();
                }
              }, 300);
            }
          }
        }
      }
    });
  }

  /**
   * This method will load more items when user reaches bottom of the list.
   * It will set the text at the footer to "No more videos to load" when
   * there will be no more videos available.
   */
  private void loadMoreJoinedCells() {
    LogEvent.Log(TAG, "loadMoreItems invoked");
    if(!apiCallInProgress4JoinedCell) {
      if(!noMoreData4JoinedCell) {
        retrievePublicCells();
      } else {
        recyclerView.clearOnScrollListeners();
        ((Footer) adapterCell.arrayList.get(adapterCell.arrayList.size() - 1)).
          info = getString(R.string.no_more_cells_to_load);
        ((Footer) adapterCell.arrayList.get(adapterCell.arrayList.size() - 1)).
          spinnerVisibility = false;
        adapterCell.notifyItemChanged(adapterCell.arrayList.size() - 1);
        //moreInfoTV4JoinedCells.setText("No more articles to load");
        //pbFooter4JoinedCells.setVisibility(View.GONE);
      }
    }
  }

  private String convertEnumToString(Cell411Alert.AlertType alertType) {
    switch(alertType) {
      case BROKEN_CAR:
        return getString(R.string.alert_broken_car);
      case BULLIED:
        return getString(R.string.alert_bullied);
      case CRIMINAL:
        return getString(R.string.alert_criminal);
      case DANGER:
        return getString(R.string.alert_danger);
      case FIRE:
        return getString(R.string.alert_fire);
      case GENERAL:
        return getString(R.string.alert_general);
      case MEDICAL:
        return getString(R.string.alert_medical);
      case PHOTO:
        return getString(R.string.alert_photo);
      case POLICE_ARREST:
        return getString(R.string.alert_police_arrest);
      case POLICE_INTERACTION:
        return getString(R.string.alert_police_interaction);
      case PULLED_OVER:
        return getString(R.string.alert_pulled_over);
      case VIDEO:
        return getString(R.string.alert_video);
      case HIJACK:
        return getString(R.string.alert_hijack);
      case PANIC:
        return getString(R.string.alert_panic);
      case FALLEN:
        return getString(R.string.alert_fallen);
      case PHYSICAL_ABUSE:
        return getString(R.string.alert_physical_abuse);
      case TRAPPED:
        return getString(R.string.alert_trapped);
      case CAR_ACCIDENT:
        return getString(R.string.alert_car_accident);
      case NATURAL_DISASTER:
        return getString(R.string.alert_natural_disaster);
      default:
        return getString(R.string.alert_un_recognized);
    }
  }

  private Cell411Alert.AlertType convertStringToEnum(String alertType) {
    if(alertType.equals(Alert.BROKEN_CAR)) {
      return Cell411Alert.AlertType.BROKEN_CAR;
    } else if(alertType.equals(Alert.BULLIED)) {
      return Cell411Alert.AlertType.BULLIED;
    } else if(alertType.equals(Alert.CRIMINAL)) {
      return Cell411Alert.AlertType.CRIMINAL;
    } else if(alertType.equals(Alert.DANGER)) {
      return Cell411Alert.AlertType.DANGER;
    } else if(alertType.equals(Alert.FIRE)) {
      return Cell411Alert.AlertType.FIRE;
    } else if(alertType.equals(Alert.GENERAL)) {
      return Cell411Alert.AlertType.GENERAL;
    } else if(alertType.equals(Alert.MEDICAL)) {
      return Cell411Alert.AlertType.MEDICAL;
    } else if(alertType.equals(Alert.PHOTO)) {
      return Cell411Alert.AlertType.PHOTO;
    } else if(alertType.equals(Alert.POLICE_ARREST)) {
      return Cell411Alert.AlertType.POLICE_ARREST;
    } else if(alertType.equals(Alert.POLICE_INTERACTION)) {
      return Cell411Alert.AlertType.POLICE_INTERACTION;
    } else if(alertType.equals(Alert.PULLED_OVER)) {
      return Cell411Alert.AlertType.PULLED_OVER;
    } else if(alertType.equals(Alert.VIDEO)) {
      return Cell411Alert.AlertType.VIDEO;
    } else if(alertType.equals(Alert.HIJACK)) {
      return Cell411Alert.AlertType.HIJACK;
    } else if(alertType.equals(Alert.PANIC)) {
      return Cell411Alert.AlertType.PANIC;
    } else if(alertType.equals(Alert.FALLEN)) {
      return Cell411Alert.AlertType.FALLEN;
    } else if(alertType.equals(Alert.PHYSICAL_ABUSE)) {
      return Cell411Alert.AlertType.PHYSICAL_ABUSE;
    } else if(alertType.equals(Alert.TRAPPED)) {
      return Cell411Alert.AlertType.TRAPPED;
    } else if(alertType.equals(Alert.CAR_ACCIDENT)) {
      return Cell411Alert.AlertType.CAR_ACCIDENT;
    } else if(alertType.equals(Alert.NATURAL_DISASTER)) {
      return Cell411Alert.AlertType.NATURAL_DISASTER;
    } else {
      return Cell411Alert.AlertType.UN_RECOGNIZED;
    }
  }

  private static class NewChatItemTitle {
    public String title;
    public boolean itemLoaded;
    public ItemType itemType;

    public NewChatItemTitle(String title, ItemType itemType) {
      this.title = title;
      this.itemType = itemType;
    }

    public enum ItemType {
      ALERTS, OWNED_CELLS, JOINED_CELLS
    }
  }

  public class CellListAdapter extends RecyclerView.Adapter<CellListAdapter.ViewHolder> {
    private final int VIEW_TYPE_TITLE = 0;
    private final int VIEW_TYPE_ALERT = 1;
    private final int VIEW_TYPE_CELL = 2;
    private final int VIEW_TYPE_FOOTER = 3;
    public ArrayList<Object> arrayList;
    private final View.OnClickListener mOnClickListener = new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        int position = recyclerView.getChildAdapterPosition(v);
        if(arrayList.get(position) instanceof Cell411Alert) {
          Intent intentChat = new Intent(NewChatActivity.this, ChatActivity.class);
          intentChat.putExtra("entityType", ChatRoom.EntityType.ALERT.toString());
          intentChat.putExtra("entityObjectId",
            ((Cell411Alert) arrayList.get(position)).cell411AlertId);
          intentChat.putExtra("entityName",
            convertEnumToString(((Cell411Alert) arrayList.get(position)).alertType));
          intentChat.putExtra("time",
            String.valueOf(((Cell411Alert) arrayList.get(position)).millis));
          startActivity(intentChat);
        } else if(arrayList.get(position) instanceof Cell) {
          Intent intentChat = new Intent(NewChatActivity.this, ChatActivity.class);
          intentChat.putExtra("entityType", ChatRoom.EntityType.PUBLIC_CELL.toString());
          intentChat.putExtra("entityObjectId",
            ((Cell) arrayList.get(position)).cell.getObjectId());
          intentChat.putExtra("entityName", ((Cell) arrayList.get(position)).name);
          intentChat.putExtra("time",
            String.valueOf(((Cell) arrayList.get(position)).cell.getCreatedAt().getTime()));
          startActivity(intentChat);
        }
      }
    };

    // Provide a suitable constructor (depends on the kind of data set)
    public CellListAdapter(ArrayList<Object> arrayList) {
      this.arrayList = arrayList;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public CellListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
      // create a new view
      View v = null;
      if(viewType == VIEW_TYPE_TITLE) {
        v = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_loading_items, parent,
          false);
      } else if(viewType == VIEW_TYPE_ALERT) {
        v = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_public_cell, parent,
          false);
      } else if(viewType == VIEW_TYPE_CELL) {
        v = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_public_cell, parent,
          false);
      } else if(viewType == VIEW_TYPE_FOOTER) {
        v = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_footer, parent, false);
      }
      // set the view's size, margins, padding's and layout parameters
      ViewHolder vh = new ViewHolder(v, viewType);
      if(viewType == VIEW_TYPE_CELL || viewType == VIEW_TYPE_ALERT) {
        v.setOnClickListener(mOnClickListener);
      }
      return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int position) {
      // - get element from your dataset at this position
      // - replace the contents of the view with that element
      if(getItemViewType(position) == VIEW_TYPE_CELL) {
        final Cell cell = (Cell) arrayList.get(position);
        viewHolder.txtCellName.setText(cell.name);
        LogEvent.Log(TAG, "verificationStatus: " + cell.verificationStatus);
        if(cell.verificationStatus == 1) {
          viewHolder.imgVerified.setVisibility(View.VISIBLE);
        } else {
          viewHolder.imgVerified.setVisibility(View.GONE);
        }
        viewHolder.imgCell.setImageResource(R.drawable.ic_placeholder_cell);
        viewHolder.imgChat.setVisibility(View.GONE);
        viewHolder.txtBtnAction.setVisibility(View.GONE);
        viewHolder.imgChevron.setVisibility(View.GONE);
      } else if(getItemViewType(position) == VIEW_TYPE_ALERT) {
        final Cell411Alert cell411Alert = (Cell411Alert) arrayList.get(position);
        viewHolder.txtCellName.setText(
          getString(R.string.chat_title_alert, convertEnumToString(cell411Alert.alertType)));
        viewHolder.imgVerified.setVisibility(View.GONE);
        viewHolder.imgCell.setImageResource(R.drawable.logo);
        viewHolder.imgChat.setVisibility(View.GONE);
        viewHolder.txtBtnAction.setVisibility(View.GONE);
        viewHolder.imgChevron.setVisibility(View.GONE);
      } else if(getItemViewType(position) == VIEW_TYPE_FOOTER) {
        final Footer footer = (Footer) arrayList.get(position);
        viewHolder.txtInfo.setText(footer.info);
        if(footer.spinnerVisibility) {
          viewHolder.pb.setVisibility(View.VISIBLE);
        } else {
          viewHolder.pb.setVisibility(View.GONE);
        }
      } else {
        NewChatItemTitle newChatItemTitle = (NewChatItemTitle) arrayList.get(position);
        viewHolder.txtTitle.setText(newChatItemTitle.title);
        if(newChatItemTitle.itemLoaded) {
          viewHolder.rlProgress.setVisibility(View.GONE);
        } else {
          viewHolder.rlProgress.setVisibility(View.VISIBLE);
        }
      }
    }

    // Return the size of your data set (invoked by the layout manager)
    @Override
    public int getItemCount() {
      return arrayList.size();
    }

    @Override
    public int getItemViewType(int position) {
      super.getItemViewType(position);
      if(arrayList.get(position) instanceof Cell) {
        return VIEW_TYPE_CELL;
      } else if(arrayList.get(position) instanceof Cell411Alert) {
        return VIEW_TYPE_ALERT;
      } else if(arrayList.get(position) instanceof NewChatItemTitle) {
        return VIEW_TYPE_TITLE;
      } else {
        return VIEW_TYPE_FOOTER;
      }
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public class ViewHolder extends RecyclerView.ViewHolder {
      // each data item is just a string in this case
      private TextView txtCellName;
      private TextView txtBtnAction;
      private CircularImageView imgCell;
      private ImageView imgChevron;
      private TextView txtTitle;
      private ImageView imgVerified;
      private ImageView imgChat;
      private TextView txtInfo;
      private ProgressBar pb;
      private RelativeLayout rlProgress;

      public ViewHolder(View view, int type) {
        super(view);
        if(type == VIEW_TYPE_TITLE) {
          txtTitle = (TextView) view.findViewById(R.id.txt_title);
          rlProgress = (RelativeLayout) view.findViewById(R.id.rl_progress);
        } else if(type == VIEW_TYPE_CELL || type == VIEW_TYPE_ALERT) {
          txtCellName = (TextView) view.findViewById(R.id.txt_cell_name);
          txtBtnAction = (TextView) view.findViewById(R.id.txt_btn_action);
          imgCell = (CircularImageView) view.findViewById(R.id.img_cell);
          imgChat = (ImageView) view.findViewById(R.id.img_chat);
          imgChevron = (ImageView) view.findViewById(R.id.img_chevron);
          imgVerified = (ImageView) view.findViewById(R.id.img_verified);
        } else if(type == VIEW_TYPE_FOOTER) {
          txtInfo = (TextView) view.findViewById(R.id.txt_info);
          pb = (ProgressBar) view.findViewById(R.id.pb);
        }
      }
    }
  }
}