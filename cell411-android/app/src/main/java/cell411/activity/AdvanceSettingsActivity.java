package cell411.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Service;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.ActionBar;

import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.callback.FindCallback;
import com.safearx.cell411.R;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import cell411.Singleton;
import cell411.methods.Modules;
import cell411.models.Cell;
import cell411.models.NewPrivateCell;
import cell411.models.PrivateCell;
import cell411.models.PublicCell;
import cell411.utils.LogEvent;
import cell411.utils.StorageOperations;
import cell411.widgets.Spinner;

/**
 * Created by Sachin on 09-08-2016.
 */
public class AdvanceSettingsActivity extends BaseActivity {
  public static WeakReference<AdvanceSettingsActivity> weakRef;
  private static ArrayList<PrivateCell> privateCells;
  //private static ArrayList<NAUCell> nauCells;
  private static ArrayList<PublicCell> publicCells;
  private int alertTimeInSeconds;
  private CheckBox cbAllFriends;
  private CheckBox cbNearBy;
  private CheckBox cbPrivateCells;
  //private CheckBox cbNAUCells;
  private CheckBox cbPublicCells;
  private TextView txtPrivateCells;
  //private TextView txtNAUCells;
  private TextView txtPublicCells;
  private EditText etAdditionalNote;
  private SharedPreferences prefs;

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    switch(item.getItemId()) {
      case android.R.id.home:
        finish();
        return true;
      default:
        return super.onOptionsItemSelected(item);
    }
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_advance_settings);
    weakRef = new WeakReference<>(this);
    Singleton.INSTANCE.setCurrentActivity(this);
    // Set up the action bar.
    final ActionBar actionBar = getSupportActionBar();
    actionBar.setDisplayHomeAsUpEnabled(true);
    prefs = Singleton.INSTANCE.getAppPrefs();
    alertTimeInSeconds = prefs.getInt("AlertTimeInSeconds", 5);
    String additionalNote = prefs.getString("AdditionalNote", null);
    privateCells = StorageOperations.getPrivateCellsForPanicAlert(this);
    //nauCells = StorageOperations.getNAUCellsForPanicAlert(this);
    publicCells = StorageOperations.getPublicCellsForPanicAlert(this);
    if(privateCells == null) {
      privateCells = new ArrayList<>();
    }
        /*if (nauCells == null) {
            nauCells = new ArrayList<>();
        }*/
    if(publicCells == null) {
      publicCells = new ArrayList<>();
    }
    boolean panicAlertToAllFriends = prefs.getBoolean("PanicAlertToAllFriends", true);
    boolean panicAlertToNearBy = prefs.getBoolean("PanicAlertToNearBy", false);
    boolean panicAlertToPrivateCells = prefs.getBoolean("PanicAlertToPrivateCells", false);
    //boolean panicAlertToNAUCells = prefs.getBoolean("PanicAlertToNAUCells", false);
    boolean panicAlertToPublicCells = prefs.getBoolean("PanicAlertToPublicCells", false);
    RelativeLayout rlInstant = (RelativeLayout) findViewById(R.id.rl_instant);
    RelativeLayout rl5Second = (RelativeLayout) findViewById(R.id.rl_5_second);
    RelativeLayout rl10Second = (RelativeLayout) findViewById(R.id.rl_10_second);
    final ImageView imgInstant = (ImageView) findViewById(R.id.img_instant);
    final ImageView img5Second = (ImageView) findViewById(R.id.img_5_second);
    final ImageView img10Second = (ImageView) findViewById(R.id.img_10_second);
    cbAllFriends = (CheckBox) findViewById(R.id.cb_all_friends);
    cbNearBy = (CheckBox) findViewById(R.id.cb_near_by);
    cbPrivateCells = (CheckBox) findViewById(R.id.cb_private_cell);
    //cbNAUCells = (CheckBox) findViewById(R.id.cb_nau_cell);
    cbPublicCells = (CheckBox) findViewById(R.id.cb_public_cell);
    txtPrivateCells = (TextView) findViewById(R.id.txt_private_cell);
    //txtNAUCells = (TextView) findViewById(R.id.txt_nau_cell);
    txtPublicCells = (TextView) findViewById(R.id.txt_public_cell);
    etAdditionalNote = (EditText) findViewById(R.id.et_additional_note);
    if(alertTimeInSeconds == 0) {
      imgInstant.setBackgroundResource(R.drawable.bg_alert_time_selected);
      imgInstant.setImageResource(R.drawable.ic_instant_selected);
    } else if(alertTimeInSeconds == 5) {
      img5Second.setBackgroundResource(R.drawable.bg_alert_time_selected);
      img5Second.setImageResource(R.drawable.ic_5_sec_selected);
    } else {
      img10Second.setBackgroundResource(R.drawable.bg_alert_time_selected);
      img10Second.setImageResource(R.drawable.ic_10_sec_selected);
    }
    findViewById(R.id.lbl_note).setVisibility(View.GONE);
    if(panicAlertToAllFriends) {
      cbAllFriends.setChecked(true);
    }
    if(panicAlertToNearBy) {
      cbNearBy.setChecked(true);
    }
    if(panicAlertToPrivateCells) {
      cbPrivateCells.setChecked(true);
    }
    if(panicAlertToPublicCells) {
      cbPublicCells.setChecked(true);
    }
    txtPrivateCells.setText(privateCells.size() + " " + getString(R.string.selected));
    //txtNAUCells.setText(nauCells.size() + " " + getString(R.string.selected));
    txtPublicCells.setText(publicCells.size() + " " + getString(R.string.selected));
    if(additionalNote != null) {
      etAdditionalNote.setText(additionalNote);
    }
    rlInstant.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        imgInstant.setBackgroundResource(R.drawable.bg_alert_time_selected);
        imgInstant.setImageResource(R.drawable.ic_instant_selected);
        img5Second.setBackgroundResource(R.drawable.bg_alert_time_unselected);
        img5Second.setImageResource(R.drawable.ic_5_sec_unselected);
        img10Second.setBackgroundResource(R.drawable.bg_alert_time_unselected);
        img10Second.setImageResource(R.drawable.ic_10_sec_unselected);
        alertTimeInSeconds = 0;
      }
    });
    rl5Second.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        img5Second.setBackgroundResource(R.drawable.bg_alert_time_selected);
        img5Second.setImageResource(R.drawable.ic_5_sec_selected);
        imgInstant.setBackgroundResource(R.drawable.bg_alert_time_unselected);
        imgInstant.setImageResource(R.drawable.ic_instant_unselected);
        img10Second.setBackgroundResource(R.drawable.bg_alert_time_unselected);
        img10Second.setImageResource(R.drawable.ic_10_sec_unselected);
        alertTimeInSeconds = 5;
      }
    });
    rl10Second.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        img10Second.setBackgroundResource(R.drawable.bg_alert_time_selected);
        img10Second.setImageResource(R.drawable.ic_10_sec_selected);
        imgInstant.setBackgroundResource(R.drawable.bg_alert_time_unselected);
        imgInstant.setImageResource(R.drawable.ic_instant_unselected);
        img5Second.setBackgroundResource(R.drawable.bg_alert_time_unselected);
        img5Second.setImageResource(R.drawable.ic_5_sec_unselected);
        alertTimeInSeconds = 10;
      }
    });
    cbAllFriends.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
      @Override
      public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if(isChecked) {
          cbPrivateCells.setChecked(false);
        }
      }
    });
    cbNearBy.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
      @Override
      public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if(isChecked) {
          showNearByAlertDialog();
        }
      }
    });
    cbPrivateCells.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
      @Override
      public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if(isChecked && privateCells.size() == 0) {
          displayPrivateCellSelectionList(true, CellType.PRIVATE);
        } else if(isChecked) {
          cbAllFriends.setChecked(false);
        }
      }
    });
    txtPrivateCells.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        displayPrivateCellSelectionList(true, CellType.PRIVATE);
      }
    });
        /*cbNAUCells.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked && nauCells.size() == 0) {
                    displayPrivateCellSelectionList(false, CellType.NAU);
                }
            }
        });
        txtNAUCells.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                displayPrivateCellSelectionList(false, CellType.NAU);
            }
        });*/
    cbPublicCells.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
      @Override
      public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if(isChecked && publicCells.size() == 0) {
          displayPrivateCellSelectionList(false, CellType.PUBLIC);
        }
      }
    });
    txtPublicCells.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        displayPrivateCellSelectionList(false, CellType.PUBLIC);
      }
    });
  }

  private void showNearByAlertDialog() {
    AlertDialog.Builder alert = new AlertDialog.Builder(this);
    alert.setTitle(R.string.dialog_title_warning);
    alert.setMessage(R.string.dialog_message_near_by_alert);
    alert.setOnCancelListener(new DialogInterface.OnCancelListener() {
      @Override
      public void onCancel(DialogInterface dialog) {
        cbNearBy.setChecked(false);
      }
    });
    alert.setNegativeButton(R.string.dialog_btn_no, new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int which) {
        cbNearBy.setChecked(false);
      }
    });
    alert.setPositiveButton(R.string.dialog_btn_yes, new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int which) {
      }
    });
    AlertDialog dialog = alert.create();
    dialog.show();
  }

  private void displayPrivateCellSelectionList(final boolean isPrivateCell,
                                               final CellType cellType) {
    AlertDialog.Builder alert = new AlertDialog.Builder(this);
    LayoutInflater inflater = (LayoutInflater) getSystemService(Service.LAYOUT_INFLATER_SERVICE);
    View view = inflater.inflate(R.layout.dialog_cell_selection_for_panic_alert, null);
    final TextView txtMessage = (TextView) view.findViewById(R.id.txt_message);
    final ListView listView = (ListView) view.findViewById(R.id.list_cells);
    final Spinner spinner = (Spinner) view.findViewById(R.id.spinner);
    if(cellType == CellType.PRIVATE) {
      txtMessage.setText(R.string.choose_private_cell);
    } /*else if (cellType == CellType.NAU) {
            txtMessage.setText(R.string.choose_nau_cell);
        }*/ else {
      txtMessage.setText(R.string.choose_public_cell);
    }
    alert.setView(view);
    alert.setOnCancelListener(new DialogInterface.OnCancelListener() {
      @Override
      public void onCancel(DialogInterface dialog) {
        if(cellType == CellType.PRIVATE) {
          if(privateCells.size() == 0) {
            cbPrivateCells.setChecked(false);
          } else {
            if(cbPrivateCells.isChecked()) {
              cbAllFriends.setChecked(false);
            }
          }
          txtPrivateCells.setText(privateCells.size() + " " + getString(R.string.selected));
        } /*else if (cellType == CellType.NAU) {
                    if (nauCells.size() == 0) {
                        cbNAUCells.setChecked(false);
                    }
                    txtNAUCells.setText(nauCells.size() + " " + getString(R.string.selected));
                }*/ else {
          if(publicCells.size() == 0) {
            cbPublicCells.setChecked(false);
          }
          txtPublicCells.setText(publicCells.size() + " " + getString(R.string.selected));
        }
      }
    });
    alert.setPositiveButton(R.string.dialog_btn_done, new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int which) {
        if(cellType == CellType.PRIVATE) {
          if(privateCells.size() == 0) {
            cbPrivateCells.setChecked(false);
          } else {
            if(cbPrivateCells.isChecked()) {
              cbAllFriends.setChecked(false);
            }
          }
          txtPrivateCells.setText(privateCells.size() + " " + getString(R.string.selected));
        } /*else if (cellType == CellType.NAU) {
                    if (nauCells.size() == 0) {
                        cbNAUCells.setChecked(false);
                    }
                    txtNAUCells.setText(nauCells.size() + " " + getString(R.string.selected));
                }*/ else {
          if(publicCells.size() == 0) {
            cbPublicCells.setChecked(false);
          }
          txtPublicCells.setText(publicCells.size() + " " + getString(R.string.selected));
        }
      }
    });
    final AlertDialog dialog = alert.create();
    dialog.show();
    if(cellType == CellType.PRIVATE) {
      ArrayList<NewPrivateCell> cellList = null;
      if(Singleton.INSTANCE.getNewPrivateCells() != null) {
        cellList = (ArrayList<NewPrivateCell>) Singleton.INSTANCE.getNewPrivateCells().clone();
      }
      if(cellList == null) {
        ParseQuery<ParseObject> cellQuery = ParseQuery.getQuery("Cell");
        cellQuery.whereEqualTo("createdBy", ParseUser.getCurrentUser());
        cellQuery.whereNotEqualTo("type", 5);
        cellQuery.include("members");
        cellQuery.include("nauMembers");
        cellQuery.findInBackground(new FindCallback<ParseObject>() {
          @Override
          public void done(List<ParseObject> parseObjects, ParseException e) {
            ArrayList<NewPrivateCell> cellList = new ArrayList<>();
            if(e == null) {
              if(parseObjects != null) {
                String[] cellArr = getResources().getStringArray(R.array.default_cells);
                for(int i = 0; i < parseObjects.size(); i++) {
                  ParseObject cell = parseObjects.get(i);
                  String cellName;
                  if(cell.getInt("type") == 1) {
                    cellName = cellArr[0];
                  } else if(cell.getInt("type") == 1) {
                    cellName = cellArr[1];
                  } else if(cell.getInt("type") == 1) {
                    cellName = cellArr[2];
                  } else if(cell.getInt("type") == 1) {
                    cellName = cellArr[3];
                  } else {
                    cellName = (String) cell.get("name");
                  }
                  NewPrivateCell newPrivateCell =
                    new NewPrivateCell(cellName, cell, cell.getInt("type"),
                      cell.getInt("totalMembers"));
                  if(cell.getList("members") != null) {
                    LogEvent.Log("Cell",
                      "There are some members in " + cell.get("name") + " newPrivateCell");
                    List<ParseUser> members = cell.getList("members");
                    newPrivateCell.members.addAll(members);
                  } else {
                    LogEvent.Log("Cell",
                      "There are no members in " + cell.get("name") + " newPrivateCell");
                  }
                  cellList.add(newPrivateCell);
                }
              }
            } else {
              if(Modules.isWeakReferenceValid()) {
                Singleton.sendToast(e);
              }
            }
            Singleton.INSTANCE
              .setNewPrivateCells((ArrayList<NewPrivateCell>) cellList.clone());
            spinner.setVisibility(View.GONE);
            createCellsList(listView, cellList, null, isPrivateCell, CellType.PRIVATE);
          }
        });
      } else {
        spinner.setVisibility(View.GONE);
        createCellsList(listView, cellList, null, isPrivateCell, CellType.PRIVATE);
      }
    } else {
      ParseQuery<ParseObject> cellQuery = ParseQuery.getQuery("PublicCell");
      cellQuery.whereGreaterThan("totalMembers", 1);
      cellQuery.whereEqualTo("createdBy", ParseUser.getCurrentUser());
      ParseQuery<ParseObject> cellQuery2 = ParseQuery.getQuery("PublicCell");
      cellQuery2.whereNotEqualTo("createdBy", ParseUser.getCurrentUser());
      cellQuery2.whereEqualTo("members", ParseUser.getCurrentUser());
      List<ParseQuery<ParseObject>> queries = new ArrayList<>();
      queries.add(cellQuery);
      queries.add(cellQuery2);
      ParseQuery<ParseObject> mainQuery = ParseQuery.or(queries);
      mainQuery.findInBackground(new FindCallback<ParseObject>() {
        @Override
        public void done(List<ParseObject> parseObjects, ParseException e) {
          ArrayList<Cell> cellList = new ArrayList<>();
          if(e == null) {
            if(parseObjects != null) {
              for(int i = 0; i < parseObjects.size(); i++) {
                ParseObject cell = parseObjects.get(i);
                Cell cl = new Cell((String) cell.get("name"), cell);
                if(cell.getList("members") != null) {
                  LogEvent.Log("Cell",
                    "There are some members in " + cell.get("name") + " newPrivateCell");
                  List<ParseUser> members = cell.getList("members");
                  cl.members.addAll(members);
                } else {
                  LogEvent.Log("Cell",
                    "There are no members in " + cell.get("name") + " newPrivateCell");
                }
                cellList.add(cl);
              }
            }
          } else {
            Singleton.sendToast(e);
          }
          spinner.setVisibility(View.GONE);
          createCellsList(listView, null, cellList, isPrivateCell, CellType.PUBLIC);
        }
      });
    }
  }

  private void createCellsList(final ListView listView, ArrayList<NewPrivateCell> cells,
                               ArrayList<Cell> newPublicCells, boolean isPrivateCell,
                               CellType cellType) {
    if(cellType == CellType.PRIVATE) {
      for(int j = 0; j < cells.size(); j++) {
        cells.get(j).selected = false;
      }
      for(int i = 0; i < privateCells.size(); i++) {
        for(int j = 0; j < cells.size(); j++) {
          if(privateCells.get(i).getObjectId().equals(cells.get(j).parseObject.getObjectId())) {
            cells.get(j).selected = true;
            break;
          }
        }
      }
      final CellListAdapter adapterCell =
        new CellListAdapter(this, R.layout.cell_cell_selection_for_panic_alert, cells, cellType);
      listView.setAdapter(adapterCell);
    } /*else if (cellType == CellType.NAU) {
            for (int i = 0; i < nauCells.size(); i++) {
                for (int j = 0; j < cells.size(); j++) {
                    if (nauCells.get(i).objectId.equals(cells.get(j).cell.getObjectId())) {
                        cells.get(j).selected = true;
                        break;
                    }
                }
            }
        }*/ else {
      for(int j = 0; j < newPublicCells.size(); j++) {
        newPublicCells.get(j).selected = false;
      }
      for(int i = 0; i < publicCells.size(); i++) {
        for(int j = 0; j < cells.size(); j++) {
          if(publicCells.get(i).getObjectId().equals(newPublicCells.get(j).cell.getObjectId())) {
            cells.get(j).selected = true;
            break;
          }
        }
      }
      final NewPublicCellListAdapter adapterCell =
        new NewPublicCellListAdapter(this, R.layout.cell_cell_selection_for_panic_alert,
          newPublicCells, cellType);
      listView.setAdapter(adapterCell);
    }
  }

  @Override
  protected void onPause() {
    super.onPause();
    SharedPreferences.Editor editor = prefs.edit();
    editor.putInt("AlertTimeInSeconds", alertTimeInSeconds);
    editor.putBoolean("PanicAlertToAllFriends", cbAllFriends.isChecked());
    editor.putBoolean("PanicAlertToNearBy", cbNearBy.isChecked());
    editor.putBoolean("PanicAlertToPrivateCells", cbPrivateCells.isChecked());
    //editor.putBoolean("PanicAlertToNAUCells", cbNAUCells.isChecked());
    editor.putBoolean("PanicAlertToPublicCells", cbPublicCells.isChecked());
    editor.putString("AdditionalNote", etAdditionalNote.getText().toString().trim());
    editor.commit();
    StorageOperations.storePrivateCellsForPanicAlert(this, privateCells);
    //StorageOperations.storeNAUCellsForPanicAlert(this, nauCells);
    StorageOperations.storePublicCellsForPanicAlert(this, publicCells);
  }

  private static enum CellType {
    PRIVATE, PUBLIC
  }

  private static class CellListAdapter extends ArrayAdapter<NewPrivateCell> {
    public ArrayList<NewPrivateCell> cellsList;
    private int resource;
    private LayoutInflater inflater;
    private boolean isPrivateCell;
    private CellType cellType;

    public CellListAdapter(Context context, int resource, ArrayList<NewPrivateCell> objects,
                           CellType cellType) {
      super(context, resource, objects);
      this.resource = resource;
      this.inflater = ((Activity) context).getLayoutInflater();
      this.cellsList = objects;
      this.cellType = cellType;
    }

    public int getCount() {
      return cellsList.size();
    }

    public View getView(final int position, View convertView1, ViewGroup parent) {
      View cellView = convertView1;
      ViewHolder holder = null;
      if(cellView == null) {
        holder = new ViewHolder();
        cellView = inflater.inflate(resource, null);
        holder.txtCellName = (TextView) cellView.findViewById(R.id.txt_cell_name);
        holder.cbCell = (CheckBox) cellView.findViewById(R.id.cb_cell);
        cellView.setTag(holder);
      } else {
        holder = (ViewHolder) cellView.getTag();
      }
      final NewPrivateCell cell = cellsList.get(position);
      holder.txtCellName.setText(cell.name);
      if(cell.selected) {
        holder.cbCell.setChecked(true);
      }
      holder.cbCell.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
          if(cellType == CellType.PRIVATE) {
            if(isChecked) {
              privateCells.add(new PrivateCell(cell.name, cell.parseObject.getObjectId()));
              cell.selected = true;
            } else {
              for(int i = 0; i < privateCells.size(); i++) {
                if(privateCells.get(i).getObjectId().equals(cell.parseObject.getObjectId())) {
                  privateCells.remove(i);
                  break;
                }
              }
              cell.selected = false;
            }
          } /*else if (cellType == CellType.NAU) {
                        if (isChecked) {
                            nauCells.add(new NAUCell(cell.name,
                                    cell.cell.getObjectId()));
                            cell.selected = true;
                        } else {
                            for (int i = 0; i < nauCells.size(); i++) {
                                if (nauCells.get(i).objectId.equals(cell.cell.getObjectId())) {
                                    nauCells.remove(i);
                                    break;
                                }
                            }
                            cell.selected = false;
                        }
                    }*/ else {
            if(isChecked) {
              publicCells.add(new PublicCell(cellsList.get(position).name,
                cellsList.get(position).parseObject.getObjectId()));
              cell.selected = true;
            } else {
              for(int i = 0; i < publicCells.size(); i++) {
                if(publicCells.get(i).getObjectId().equals(cell.parseObject.getObjectId())) {
                  publicCells.remove(i);
                  break;
                }
              }
              cell.selected = false;
            }
          }
        }
      });
      return cellView;
    }

    private class ViewHolder {
      private TextView txtCellName;
      private CheckBox cbCell;
    }
  }

  private static class NewPublicCellListAdapter extends ArrayAdapter<Cell> {
    public ArrayList<Cell> cellsList;
    private int resource;
    private LayoutInflater inflater;
    private boolean isPrivateCell;
    private CellType cellType;

    public NewPublicCellListAdapter(Context context, int resource, ArrayList<Cell> objects,
                                    CellType cellType) {
      super(context, resource, objects);
      this.resource = resource;
      this.inflater = ((Activity) context).getLayoutInflater();
      this.cellsList = objects;
      this.cellType = cellType;
    }

    public int getCount() {
      return cellsList.size();
    }

    public View getView(final int position, View convertView1, ViewGroup parent) {
      View cellView = convertView1;
      ViewHolder holder = null;
      if(cellView == null) {
        holder = new ViewHolder();
        cellView = inflater.inflate(resource, null);
        holder.txtCellName = (TextView) cellView.findViewById(R.id.txt_cell_name);
        holder.cbCell = (CheckBox) cellView.findViewById(R.id.cb_cell);
        cellView.setTag(holder);
      } else {
        holder = (ViewHolder) cellView.getTag();
      }
      final Cell cell = cellsList.get(position);
      holder.txtCellName.setText(cell.name);
      if(cell.selected) {
        holder.cbCell.setChecked(true);
      }
      holder.cbCell.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
          if(cellType == CellType.PRIVATE) {
            if(isChecked) {
              privateCells.add(new PrivateCell(cell.name, cell.cell.getObjectId()));
              cell.selected = true;
            } else {
              for(int i = 0; i < privateCells.size(); i++) {
                if(privateCells.get(i).getObjectId().equals(cell.cell.getObjectId())) {
                  privateCells.remove(i);
                  break;
                }
              }
              cell.selected = false;
            }
          } /*else if (cellType == CellType.NAU) {
                        if (isChecked) {
                            nauCells.add(new NAUCell(cell.name,
                                    cell.cell.getObjectId()));
                            cell.selected = true;
                        } else {
                            for (int i = 0; i < nauCells.size(); i++) {
                                if (nauCells.get(i).objectId.equals(cell.cell.getObjectId())) {
                                    nauCells.remove(i);
                                    break;
                                }
                            }
                            cell.selected = false;
                        }
                    }*/ else {
            if(isChecked) {
              publicCells.add(new PublicCell(cellsList.get(position).name,
                cellsList.get(position).cell.getObjectId()));
              cell.selected = true;
            } else {
              for(int i = 0; i < publicCells.size(); i++) {
                if(publicCells.get(i).getObjectId().equals(cell.cell.getObjectId())) {
                  publicCells.remove(i);
                  break;
                }
              }
              cell.selected = false;
            }
          }
        }
      });
      return cellView;
    }

    private class ViewHolder {
      private TextView txtCellName;
      private CheckBox cbCell;
    }
  }
}
