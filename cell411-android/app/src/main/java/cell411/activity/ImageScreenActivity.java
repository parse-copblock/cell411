package cell411.activity;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import androidx.appcompat.app.ActionBar;

import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.callback.GetCallback;
import com.parse.callback.GetDataCallback;
import com.safearx.cell411.R;

import java.io.File;
import java.lang.ref.WeakReference;

import cell411.Singleton;
import cell411.SingletonImageFactory;
import cell411.utils.LogEvent;
import it.sephiroth.android.library.imagezoom.ImageViewTouch;

/**
 * Created by Sachin on 10/3/2015.
 */
public class ImageScreenActivity extends BaseActivity {
  public static WeakReference<ImageScreenActivity> weakRef;

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    switch(item.getItemId()) {
      case android.R.id.home:
        finish();
        return true;
      default:
        return super.onOptionsItemSelected(item);
    }
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_image);
    weakRef = new WeakReference<>(this);
    Singleton.INSTANCE.setCurrentActivity(this);
    // Set up the action bar.
    final ActionBar actionBar = getSupportActionBar();
    assert actionBar != null;
    actionBar.setDisplayHomeAsUpEnabled(true);
    actionBar.setDisplayShowHomeEnabled(true);
    // FIXME Applinks
    Uri targetUrl = null; //AppLinks.getTargetUrlFromInboundIntent(this, getIntent());
    String cell411AlertId = getIntent().getStringExtra("cell411AlertId");
    String path = getIntent().getStringExtra("path");
    String imageUrl = getIntent().getStringExtra("imageUrl");
    if(targetUrl != null) {
      cell411AlertId = targetUrl.getQueryParameter("cell_411_alert_id");
    }
    LogEvent.Log("ImageScreenActivity.java", "cell411AlertId: " + cell411AlertId);
    LogEvent.Log("ImageScreenActivity.java", "path: " + path);
    LogEvent.Log("ImageScreenActivity.java", "imageUrl: " + imageUrl);
    final ImageViewTouch imageViewTouch = (ImageViewTouch) findViewById(R.id.image);
    if(cell411AlertId != null) {
      LogEvent.Log("ImageScreenActivity.java", "cell411AlertId exists");
      ParseQuery<ParseObject> query = ParseQuery.getQuery("Cell411Alert");
      query.whereEqualTo("objectId", cell411AlertId);
      query.getFirstInBackground(new GetCallback<ParseObject>() {
        @Override
        public void done(ParseObject cell411AlertObject, ParseException e) {
          if(e == null) {
            ParseFile applicantResume = (ParseFile) cell411AlertObject.get("photo");
            applicantResume.getDataInBackground(new GetDataCallback() {
              public void done(byte[] data, ParseException e) {
                if(e == null) {
                  // data has the bytes for the photo
                  BitmapFactory.Options options = new BitmapFactory.Options();
                  Bitmap bmp = BitmapFactory.decodeByteArray(data, 0, data.length, options);
                  imageViewTouch.setImageBitmap(bmp);
                  imageViewTouch.setVisibility(View.VISIBLE);
                } else {
                  if(weakRef.get() != null && !weakRef.get().isFinishing()) {
                    Singleton.sendToast(e);
                  }
                }
              }
            });
          } else {
            if(weakRef.get() != null && !weakRef.get().isFinishing()) {
              Singleton.sendToast(e);
            }
          }
        }
      });
    } else {
      // Coming from the chat window
      if(imageUrl == null) {
        LogEvent.Log("ImageScreenActivity.java", "path exists");
        Uri uri = Uri.fromFile(new File(path));
        imageViewTouch.setImageURI(uri);
        imageViewTouch.setVisibility(View.VISIBLE);
      } else {
        LogEvent.Log("ImageScreenActivity.java", "imageUrl exists");
        SingletonImageFactory.getImageLoader().loadImage(imageUrl, new ImageLoadingListener() {
          @Override
          public void onLoadingStarted(String s, View view) {
          }

          @Override
          public void onLoadingFailed(String s, View view, FailReason failReason) {
          }

          @Override
          public void onLoadingComplete(String s, View view, Bitmap bitmap) {
            imageViewTouch.setImageBitmap(bitmap);
            imageViewTouch.setVisibility(View.VISIBLE);
          }

          @Override
          public void onLoadingCancelled(String s, View view) {
          }
        });
      }
    }
  }
}
