package cell411.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.URLSpan;
import android.view.View;
import android.widget.TextView;

import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.callback.GetCallback;
import com.safearx.cell411.R;

import java.lang.ref.WeakReference;

import cell411.Singleton;
import cell411.constants.Constants;
import cell411.methods.AddFriendModules;
import cell411.services.FetchAddressIntentService;
import cell411.widgets.CircularImageView;

/**
 * Created by Sachin on 14-04-2016.
 */
public class NewMemberJoinedActivity extends BaseActivity implements View.OnClickListener {
  public static WeakReference<NewMemberJoinedActivity> weakRef;
  private final String TAG = "NewMemberJoinedActivity";
  private ParseUser parseUser;
  private TextView txtAddress;
  private String username;
  private AddressResultReceiver mResultReceiver;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    //setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    setContentView(R.layout.activity_new_member_joined);
    weakRef = new WeakReference<>(this);
    Singleton.INSTANCE.setCurrentActivity(this);
    String userObjectId = getIntent().getStringExtra("userObjectId");
    username = getIntent().getStringExtra("username");
    String name = getIntent().getStringExtra("name");
        /*String description = "<b><a href='profile'>" + name + "</a><b> has just joined "
                + getString(R.string.app_name) + ", would you like to add them as friend?";*/
    String description =
      getString(R.string.new_member_add_friend_message, name, getString(R.string.app_name));
    CharSequence sequence = Html.fromHtml(description);
    SpannableStringBuilder strBuilder = new SpannableStringBuilder(sequence);
    URLSpan[] urls = strBuilder.getSpans(0, sequence.length(), URLSpan.class);
    for(URLSpan span : urls) {
      makeLinkClickable(strBuilder, span);
    }
    TextView txtDescription = (TextView) findViewById(R.id.txt_description);
    txtDescription.setText(strBuilder);
    txtDescription.setMovementMethod(LinkMovementMethod.getInstance());
    findViewById(R.id.img_close).setOnClickListener(this);
    findViewById(R.id.txt_btn_decide_later).setOnClickListener(this);
    findViewById(R.id.txt_btn_add_friend).setOnClickListener(this);
    retrieveProfilePicAndContactInfo(userObjectId);
    // Initialize the receiver and start the service for reverse geo coded address
    mResultReceiver = new AddressResultReceiver(new Handler());
    startIntentService();
  }

  protected void makeLinkClickable(SpannableStringBuilder strBuilder, final URLSpan span) {
    int start = strBuilder.getSpanStart(span);
    int end = strBuilder.getSpanEnd(span);
    int flags = strBuilder.getSpanFlags(span);
    ClickableSpan clickable = new ClickableSpan() {
      public void onClick(View view) {
        Intent intentUser = new Intent(NewMemberJoinedActivity.this, UserActivity.class);
        intentUser.putExtra("userId", parseUser.getObjectId());
        intentUser.putExtra("imageName", "" + parseUser.get("imageName"));
        intentUser.putExtra("firstName", "" + parseUser.get("firstName"));
        intentUser.putExtra("lastName", "" + parseUser.get("lastName"));
        intentUser.putExtra("username", parseUser.getUsername());
        intentUser.putExtra("email", parseUser.getEmail());
        if(parseUser.getParseGeoPoint("location") != null) {
          intentUser.putExtra("lat", parseUser.getParseGeoPoint("location").getLatitude());
          intentUser.putExtra("lng", parseUser.getParseGeoPoint("location").getLongitude());
        }
        startActivity(intentUser);
      }
    };
    strBuilder.setSpan(clickable, start, end, flags);
    strBuilder.removeSpan(span);
  }

  private void retrieveProfilePicAndContactInfo(String userObjectId) {
    final CircularImageView imgUser = (CircularImageView) findViewById(R.id.img_user);
    ParseQuery<ParseUser> parseQuery = ParseUser.getQuery();
    parseQuery.whereEqualTo("objectId", userObjectId);
    parseQuery.getFirstInBackground(new GetCallback<ParseUser>() {
      @Override
      public void done(final ParseUser parseUser, ParseException e) {
        if(e == null) {
          NewMemberJoinedActivity.this.parseUser = parseUser;
          String email = null;
          if(parseUser.getEmail() != null && !parseUser.getEmail().isEmpty()) {
            email = (String) parseUser.getEmail();
          } else {
            email = parseUser.getUsername();
          }
          final String finalEmail = email;
          Singleton.INSTANCE
            .setImage(imgUser, parseUser.getObjectId() + parseUser.get("imageName"),
              email);
          imgUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              Intent profileImageIntent =
                new Intent(NewMemberJoinedActivity.this, ProfileImageActivity.class);
              profileImageIntent.putExtra("userId", parseUser.getObjectId());
              profileImageIntent.putExtra("imageName", "" + parseUser.get("imageName"));
              profileImageIntent.putExtra("email", finalEmail);
              startActivity(profileImageIntent);
            }
          });
        }
      }
    });
  }

  protected void startIntentService() {
    txtAddress = (TextView) findViewById(R.id.txt_address);
    Intent intent = new Intent(this, FetchAddressIntentService.class);
    intent.putExtra(Constants.RECEIVER, mResultReceiver);
    intent.putExtra(Constants.LOCATION_DATA_LAT, Singleton.INSTANCE.getLatitude());
    intent.putExtra(Constants.LOCATION_DATA_LNG, Singleton.INSTANCE.getLongitude());
    FetchAddressIntentService.enqueueWork(this, intent);
  }

  @Override
  public void onClick(View v) {
    int id = v.getId();
    switch(id) {
      case R.id.txt_btn_add_friend:
        sendFriendRequest();
        finish();
        break;
      case R.id.txt_btn_decide_later:
      case R.id.img_close:
        finish();
        break;
    }
  }

  private void sendFriendRequest() {
    AddFriendModules.addFriend(this, username, new AddFriendModules.OnSendFriendRequestListener() {
      @Override
      public void onRequestSent() {
        if(getApplicationContext() != null) {
          getApplicationContext();
          Singleton.sendToast(getString(R.string.dialog_message_invitation_sent, username));
        }
      }

      @Override
      public void onRequestFailed() {
        if(getApplicationContext() != null) {
          getApplicationContext();
          Singleton.sendToast(getString(R.string.dialog_message_cannot_send_friend_request));
        }
      }
    });
  }

  class AddressResultReceiver extends ResultReceiver {
    public AddressResultReceiver(Handler handler) {
      super(handler);
    }

    @Override
    protected void onReceiveResult(int resultCode, Bundle resultData) {
      // Display the city
      if(resultCode == Constants.SUCCESS_RESULT) {
        String city = resultData.getString(Constants.RESULT_DATA_CITY);
        txtAddress.setText(city);
      } else {
        txtAddress.setText(R.string.city_not_found);
      }
    }
  }
}