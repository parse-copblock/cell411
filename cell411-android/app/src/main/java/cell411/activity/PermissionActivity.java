package cell411.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.safearx.cell411.R;

import java.util.ArrayList;
import java.util.List;

import cell411.utils.ExitListener;
import cell411.utils.LogEvent;

public class PermissionActivity extends AppCompatActivity {
  private static String[] perms =
    {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.READ_CONTACTS,
      Manifest.permission.CALL_PHONE, Manifest.permission.CAMERA,
      Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.SEND_SMS,};
  private final String TAG = PermissionActivity.class.getSimpleName();
  private final int PERMISSION_REQUEST_CODE = 1000;
  private RelativeLayout rlPermissions;
  private RelativeLayout rlInactiveAccount;
  private ProgressBar pb;

  String[] missingPerms() {
    List<String> missing = new ArrayList<>();
    for(int i = 0; i < perms.length; i++) {
      if(!hasPerm(perms[i])) {
        missing.add(perms[i]);
      }
    }
    return missing.toArray(new String[missing.size()]);
  }

  private boolean hasPerm(String accessFineLocation) {
    int res = ActivityCompat.checkSelfPermission(this, accessFineLocation);
    LogEvent.Log(TAG, "res: " + res);
    return res == PackageManager.PERMISSION_GRANTED;
  }

  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_permission);
    rlPermissions = findViewById(R.id.rl_permissions);
    rlInactiveAccount = findViewById(R.id.rl_account_inactive);
    pb = findViewById(R.id.pb);
    rlInactiveAccount.setVisibility(View.GONE);
    rlPermissions.setVisibility(View.GONE);
    final String[] missing = missingPerms();
    if(missing.length == 0) {
      LogEvent.Log(TAG, "All permissions are granted");
      gotoMain();
    } else {
      rlPermissions.setVisibility(View.VISIBLE);
      rlInactiveAccount.setVisibility(View.GONE);
      TextView txtPermissionTitle = (TextView) findViewById(R.id.txt_title);
      txtPermissionTitle.setText(
        getString(R.string.permission_title, getString(R.string.app_name)));
      TextView txtBtnGrantAccess = findViewById(R.id.txt_btn_grant_access);
      txtBtnGrantAccess.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
          ActivityCompat.requestPermissions(PermissionActivity.this, missing,
            PERMISSION_REQUEST_CODE);
        }
      });
    }
  }

  @Override
  public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                         @NonNull int[] grantResults) {
    if(requestCode == PERMISSION_REQUEST_CODE) {
      String[] missing = missingPerms();
      for(int i = 0; i < missing.length; i++) {
        if(missing[i] == Manifest.permission.SEND_SMS) {
          AlertDialog.Builder builder = new AlertDialog.Builder(this);
          builder.setTitle("TITLE!");
          builder.setItems(new CharSequence[]{"Exit", "Quit", "Leave", "GoHome"},
            new ExitListener());
          builder.create().show();
        }
      }
      ;
      gotoMain();
    } else {
      super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }
  }

  private void gotoMain() {
    Intent intentMain = new Intent(this, MainActivity.class);
    startActivity(intentMain);
    finish();
  }
}