package cell411.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Service;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SearchView;
import android.widget.SectionIndexer;
import android.widget.TextView;

import androidx.appcompat.app.ActionBar;

import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParsePush;
import com.parse.ParseQuery;
import com.parse.ParseRelation;
import com.parse.ParseUser;
import com.parse.callback.FindCallback;
import com.parse.callback.FunctionCallback;
import com.parse.callback.SaveCallback;
import com.safearx.cell411.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import cell411.Singleton;
import cell411.SingletonImageFactory;
import cell411.models.Contact;
import cell411.utils.LogEvent;

/**
 * Created by Sachin on 30-05-2016.
 */
public class ImportContactsActivity extends BaseActivity
  implements SearchView.OnQueryTextListener, SearchView.OnCloseListener {
  private static final String TAG = "ImportContactsActivity";
  private static final String[] CONTACTS_SUMMARY_PROJECTION =
    new String[]{ContactsContract.Contacts._ID, ContactsContract.Contacts.DISPLAY_NAME,
      ContactsContract.Contacts.HAS_PHONE_NUMBER};
  public static WeakReference<ImportContactsActivity> weakRef;
  private ListView listView;
  private ArrayList<Contact> contactsList;
  private ArrayList<Object> list;
  private ArrayList<String> stringArrayList;
  private LinearLayout pb;
  private TextView txtLoadingText;
  private SearchView searchView;
  private ContactsListAdapter contactsListAdapter;

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    switch(item.getItemId()) {
      case android.R.id.home:
        finish();
        return true;
      default:
        return super.onOptionsItemSelected(item);
    }
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_contacts);
    weakRef = new WeakReference<>(this);
    Singleton.INSTANCE.setCurrentActivity(this);
    // Set up the action bar.
    final ActionBar actionBar = getSupportActionBar();
    actionBar.setDisplayHomeAsUpEnabled(true);
    actionBar.setDisplayShowHomeEnabled(true);
    listView = (ListView) findViewById(R.id.list_friends);
    pb = (LinearLayout) findViewById(R.id.ll_loading_text);
    txtLoadingText = (TextView) findViewById(R.id.txt_loading);
    pb.setVisibility(View.VISIBLE);
    searchView = (SearchView) findViewById(R.id.searchview);
    int id =
      searchView.getContext().getResources().getIdentifier("android:id/search_src_text", null,
        null);
    //TextView textView = (TextView) searchView.findViewById(id);
    //textView.setTextColor(Color.BLACK);
    //textView.setTextColor(Color.parseColor(getString(R.color.text_primary)));
    searchView.setIconifiedByDefault(false);
    searchView.setOnQueryTextListener(this);
    searchView.setOnCloseListener(this);
    android.os.Handler handler = new android.os.Handler();
    handler.postDelayed(new Runnable() {
      @Override
      public void run() {
        retrieveContacts();
      }
    }, 200);
  }

  /**
   * Loads contacts List
   */
  private void retrieveContacts() {
    ContentResolver cr = getContentResolver();
    Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
    final ArrayList<Contact> contactArrayList = new ArrayList<>();
    final ArrayList<String> idList = new ArrayList<>();
    final ArrayList<String> emailList = new ArrayList<>();
    if(cur.getCount() > 0) {
      while(cur.moveToNext()) {
        //String accountName = cur.getString(cur.getColumnIndex(ContactsContract.PRIMARY_ACCOUNT_NAME));
        //String accountType = cur.getString(cur.getColumnIndex(ContactsContract.PRIMARY_ACCOUNT_TYPE));
        String contactId = cur.getString(cur.getColumnIndex(ContactsContract.Contacts._ID));
        String displayName =
          cur.getString(cur.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
        idList.add(contactId);
        contactArrayList.add(new Contact(contactId, displayName));
        //LogEvent.Log(TAG, "accountName: " + accountName);
        //LogEvent.Log(TAG, "accountType: " + accountType);
      }
      cur.close();
      Cursor emailCur = cr.query(ContactsContract.CommonDataKinds.Email.CONTENT_URI, null,
        ContactsContract.CommonDataKinds.Email.CONTACT_ID + " IN (" +
          TextUtils.join(",", idList) + ")", null, null);
      while(emailCur.moveToNext()) {
        // This would allow you get several email addresses
        // if the email addresses were stored in an array
        String contactId = emailCur.getString(
          emailCur.getColumnIndex(ContactsContract.CommonDataKinds.Email.CONTACT_ID));
        String email = emailCur.getString(
          emailCur.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));
        String emailType = emailCur.getString(
          emailCur.getColumnIndex(ContactsContract.CommonDataKinds.Email.TYPE));
        emailList.add(email);
        if(!email.equalsIgnoreCase(ParseUser.getCurrentUser().getUsername()) &&
             (ParseUser.getCurrentUser().getEmail() == null ||
                !email.equalsIgnoreCase(ParseUser.getCurrentUser().getEmail()))) {
          for(int i = 0; i < contactArrayList.size(); i++) {
            if(contactArrayList.get(i).contactId.equals(contactId)) {
              contactArrayList.get(i).email = email.toLowerCase();
              break;
            }
          }
        }
        System.out.println("Email " + email);
      }
      emailCur.close();
      //            Cursor pCur = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
      //                    ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " IN (" + TextUtils.join(",", idList) + ")",
      //                    null, null);
      //            while (pCur.moveToNext()) {
      //                String contactId = pCur.getString(
      //                        pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.CONTACT_ID));
      //                String phone = pCur.getString(
      //                        pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
      //                for (int i = 0; i < contactArrayList.size(); i++) {
      //                    if (contactArrayList.get(i).contactId.equals(contactId)) {
      //                        contactArrayList.get(i).phone = phone;
      //                        break;
      //                    }
      //                }
      //                System.out.println("phone" + phone);
      //            }
      //            pCur.close();
      list = new ArrayList<>();
      stringArrayList = new ArrayList<>();
      contactsList = new ArrayList<>();
      String charAtZeroIndex = "";
      LogEvent.Log(TAG, "contactArrayList.size(): " + contactArrayList.size());
      for(int i = 0; i < contactArrayList.size(); i++) {
        LogEvent.Log(TAG,
          "contactArrayList.get(i).displayName: " + contactArrayList.get(i).displayName);
        //                if ((contactArrayList.get(i).email == null && contactArrayList.get(i).phone == null) ||
        //                        (contactArrayList.get(i).displayName == null))
        //                    continue;
        if((contactArrayList.get(i).email == null) ||
             (contactArrayList.get(i).displayName == null)) {
          continue;
        } else {
          boolean duplicate = false;
          for(int j = 0; j < contactsList.size(); j++) {
            if(contactsList.get(j).email.equalsIgnoreCase(contactArrayList.get(i).email)) {
              duplicate = true;
              break;
            }
          }
          if(duplicate) {
            continue;
          }
        }
        contactsList.add(contactArrayList.get(i));
      }
      LogEvent.Log(TAG, "contactsList.size(): " + contactArrayList.size());
      Collections.sort(contactsList, new Comparator<Contact>() {
        @Override
        public int compare(Contact lhs, Contact rhs) {
          return lhs.displayName.compareToIgnoreCase(rhs.displayName);
        }
      });
      for(int i = 0; i < contactsList.size(); i++) {
        if(charAtZeroIndex.equalsIgnoreCase(contactsList.get(i).
                                                                 displayName.substring(0, 1))) {
          list.add(contactsList.get(i));
        } else {
          charAtZeroIndex = contactsList.get(i).displayName.
                                                             substring(0, 1).toUpperCase();
          stringArrayList.add(charAtZeroIndex);
          list.add(charAtZeroIndex);
          list.add(contactsList.get(i));
        }
      }
      runOnUiThread(new Runnable() {
        @Override
        public void run() {
          displayAllData();
          pb.setVisibility(View.GONE);
        }
      });
    }
    if(contactsList == null || contactsList.size() == 0) {
      txtLoadingText.setText(getString(R.string.no_contacts));
      searchView.setVisibility(View.GONE);
      return;
    }
    final ArrayList<ParseUser> friendList = new ArrayList<>();
    final ArrayList<ParseObject> pendingRequestList = new ArrayList<>();
    ParseUser user = ParseUser.getCurrentUser();
    ParseRelation relation = user.getRelation("friends");
    ParseQuery queryFriends = relation.getQuery();
    queryFriends.findInBackground(new FindCallback<ParseObject>() {
      @Override
      public void done(List list, ParseException e) {
        if(e == null) {
          if(list != null && list.size() > 0) {
            friendList.addAll(list);
          }
          ParseQuery<ParseObject> cell411Query = new ParseQuery<>("Cell411Alert");
          cell411Query.whereEqualTo("issuedBy", ParseUser.getCurrentUser());
          cell411Query.whereContainedIn("to", emailList);
          cell411Query.whereEqualTo("status", "PENDING");
          cell411Query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> parseObjects, ParseException e) {
              if(e == null) {
                if(parseObjects != null && parseObjects.size() > 0) {
                  pendingRequestList.addAll(parseObjects);
                }
                ParseQuery<ParseUser> query = ParseUser.getQuery();
                query.whereContainedIn("username", emailList);
                query.findInBackground(new FindCallback<ParseUser>() {
                  @Override
                  public void done(List<ParseUser> parseUsers, ParseException e) {
                    if(e == null) {
                      if(parseUsers != null && parseUsers.size() > 0) {
                        for(int j = 0; j < contactsList.size(); j++) {
                          boolean accountExists = false;
                          for(int i = 0; i < parseUsers.size(); i++) {
                            if(contactsList.get(j).email != null &&
                                 (contactsList.get(j).email.equalsIgnoreCase(
                                   parseUsers.get(i).getUsername()) ||
                                    (parseUsers.get(i).getEmail() != null &&
                                       contactsList.get(j).email.equalsIgnoreCase(
                                         parseUsers.get(i).getEmail())))) {
                              contactsList.get(j).user = parseUsers.get(i);
                              accountExists = true;
                              break;
                            }
                          }
                          if(accountExists) {
                            boolean friend = false;
                            for(int k = 0; k < friendList.size(); k++) {
                              if(contactsList.get(j).email != null &&
                                   (friendList.get(k).getUsername().equalsIgnoreCase(
                                     contactsList.get(j).email) ||
                                      (friendList.get(k).getEmail() != null &&
                                         friendList.get(k).getEmail().equalsIgnoreCase(
                                           contactsList.get(j).email)))) {
                                contactsList.get(j).status = Contact.Status.FRIENDS;
                                friend = true;
                                break;
                              }
                            }
                            if(!friend) {
                              boolean requestSent = false;
                              for(int k = 0; k < pendingRequestList.size(); k++) {
                                if(contactsList.get(j).email != null &&
                                     ((String) pendingRequestList.get(k).get("to")).
                                                                                     equalsIgnoreCase(
                                                                                       contactsList
                                                                                         .get(
                                                                                           j).email)) {
                                  if(((String) pendingRequestList.get(k).get("entryFor")).
                                                                                           equalsIgnoreCase(
                                                                                             "FR")) {
                                    contactsList.get(j).status =
                                      Contact.Status.FRIEND_REQUEST_PENDING;
                                    requestSent = true;
                                  }
                                  break;
                                }
                              }
                              if(!requestSent) {
                                contactsList.get(j).status = Contact.Status.USER_EXIST;
                              }
                            }
                          } else {
                            boolean invited = false;
                            for(int k = 0; k < pendingRequestList.size(); k++) {
                              if(contactsList.get(j).email != null &&
                                   ((String) pendingRequestList.get(k).get("to")).
                                                                                   equalsIgnoreCase(
                                                                                     contactsList
                                                                                       .get(
                                                                                         j).email)) {
                                if(((String) pendingRequestList.get(k).get("entryFor")).
                                                                                         equalsIgnoreCase(
                                                                                           "FI")) {
                                  contactsList.get(j).status = Contact.Status.INVITATION_PENDING;
                                  invited = true;
                                }
                                break;
                              }
                            }
                            if(!invited) {
                              contactsList.get(j).status = Contact.Status.USER_DOES_NOT_EXIST;
                            }
                          }
                        }
                      } else {
                        for(int j = 0; j < contactsList.size(); j++) {
                          contactsList.get(j).status = Contact.Status.USER_DOES_NOT_EXIST;
                        }
                      }
                      runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                          contactsListAdapter.notifyDataSetChanged();
                        }
                      });
                    } else {
                    }
                  }
                });
              } else {
              }
            }
          });
        } else {
        }
      }
    });
  }

  @Override
  public boolean onQueryTextSubmit(String query) {
    LogEvent.Log(TAG, query);
    hideSoftKeyboard();
    return false;
  }

  @Override
  public boolean onQueryTextChange(String newText) {
    LogEvent.Log(TAG, newText);
    if(contactsList != null && contactsList.size() > 0) {
      displayQueryData(newText);
    }
    return false;
  }

  @Override
  public boolean onClose() {
    LogEvent.Log(TAG, "onClose invoked");
    return false;
  }

  private void displayAllData() {
    contactsListAdapter = new ContactsListAdapter(this, 0, list, null);
    listView.setAdapter(contactsListAdapter);
    listView.setFastScrollEnabled(true);
    listView.setFastScrollAlwaysVisible(false);
    listView.setDividerHeight(0);
  }

  private void displayQueryData(String queryText) {
    if(queryText.equals("")) {
      displayAllData();
      return;
    }
    final ArrayList<Object> mList = new ArrayList<>();
    for(int i = 0; i < contactsList.size(); i++) {
      if(contactsList.get(i).displayName.
                                          toLowerCase(Locale.getDefault())
           .contains(queryText.toLowerCase())) {
        mList.add(contactsList.get(i));
      }
    }
    contactsListAdapter = new ContactsListAdapter(this, 0, mList, queryText);
    listView.setAdapter(contactsListAdapter);
    listView.setFastScrollEnabled(false);
    listView.setFastScrollAlwaysVisible(false);
    listView.setDividerHeight(0);
  }

  private void showAlertDialog() {
    AlertDialog.Builder alert = new AlertDialog.Builder(this);
    alert.setMessage(R.string.cannot_send_friend_request);
    alert.setPositiveButton(R.string.dialog_btn_ok, new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialogInterface, int i) {
      }
    });
    AlertDialog dialog = alert.create();
    dialog.show();
  }

  /**
   * Used to hide the keyboard
   */
  public void hideSoftKeyboard() {
    if(getCurrentFocus() != null) {
      InputMethodManager inputMethodManager =
        (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
      inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
    }
  }

  private static class ContactViewHolder {
    private TextView txtDisplayName;
    private TextView txtEmail;
    private ImageView imgUser;
    private TextView txtOption;
  }

  private static class TitleViewHolder {
    private TextView txtAlphabet;
  }

  private class ContactsListAdapter extends ArrayAdapter<Object> implements SectionIndexer {
    private final int VIEW_TYPE_TITLE = 0;
    private final int VIEW_TYPE_CONTACT = 1;
    private LayoutInflater inflater;
    private ArrayList<Object> arrayList;
    private String query;

    public ContactsListAdapter(Context context, int resource, ArrayList<Object> objects,
                               String query) {
      super(context, resource, objects);
      inflater = ((Activity) context).getLayoutInflater();
      arrayList = objects;
      this.query = query;
    }

    public int getCount() {
      return arrayList.size();
    }

    @Override
    public int getItemViewType(int position) {
      super.getItemViewType(position);
      if(arrayList.get(position) instanceof Contact) {
        return VIEW_TYPE_CONTACT;
      } else {
        return VIEW_TYPE_TITLE;
      }
    }

    @Override
    public int getViewTypeCount() {
      super.getViewTypeCount();
      return 2;
    }

    public View getView(final int position, View convertView1, ViewGroup parent) {
      View cellView = convertView1;
      ContactViewHolder contactViewHolder = null;
      TitleViewHolder titleViewHolder = null;
      int type = getItemViewType(position);
      if(type == VIEW_TYPE_CONTACT) {
        final Contact contact = (Contact) getItem(position);
        if(cellView == null) {
          contactViewHolder = new ContactViewHolder();
          cellView = inflater.inflate(R.layout.cell_contact, null);
          contactViewHolder.txtDisplayName =
            (TextView) cellView.findViewById(R.id.txt_display_name);
          contactViewHolder.txtEmail = (TextView) cellView.findViewById(R.id.txt_email);
          contactViewHolder.imgUser = (ImageView) cellView.findViewById(R.id.img_user);
          contactViewHolder.txtOption = (TextView) cellView.findViewById(R.id.txt_option);
          cellView.setTag(contactViewHolder);
        } else {
          contactViewHolder = (ContactViewHolder) cellView.getTag();
          //contactViewHolder.imgUser.setImageResource(R.drawable.logo);
        }
        if(query == null) {
          contactViewHolder.txtDisplayName.setText(contact.displayName);
        } else {
          String name = contact.displayName;
          //                    int index = name.indexOf(query, 0);
          //                    String preStr = name.substring(0, index);
          //                    String str = "<b>" + name.substring(index, index + query.length()) + "</b>";
          //                    String postStr = name.substring(index + query.length(), name.length());
          //                    membersViewHolder.txtListItem.setText(Html.fromHtml(preStr + str + postStr));
          name = name.replaceAll(query, "<b>" + query + "</b>");
          contactViewHolder.txtDisplayName.setText(Html.fromHtml(name));
        }
        contactViewHolder.imgUser.setImageResource(R.drawable.ic_placeholder_user);
        if(contact.email != null) {
          if(query == null) {
            contactViewHolder.txtEmail.setText(contact.email);
          } else {
            String name = contact.email;
            name = name.replaceAll(query, "<b>" + query + "</b>");
            contactViewHolder.txtEmail.setText(Html.fromHtml(name));
          }
          final ContactViewHolder finalHolder = contactViewHolder;
          String url = SingletonImageFactory.getGravatarImageUrl(contact.email);
          SingletonImageFactory.getImageLoader().loadImage(url,
            new ImageLoadingListener() {
              @Override
              public void onLoadingStarted(String s, View view) {
              }

              @Override
              public void onLoadingFailed(String s, View view, FailReason failReason) {
                finalHolder.imgUser.setImageResource(R.drawable.ic_placeholder_user);
              }

              @Override
              public void onLoadingComplete(String s, View view, Bitmap bitmap) {
                finalHolder.imgUser.setImageBitmap(Singleton.getCroppedBitmap(bitmap));
              }

              @Override
              public void onLoadingCancelled(String s, View view) {
              }
            });
        } else {
          if(query == null) {
            contactViewHolder.txtEmail.setText(contact.phone);
          } else {
            String name = contact.phone;
            name = name.replaceAll(query, "<b>" + query + "</b>");
            contactViewHolder.txtEmail.setText(Html.fromHtml(name));
          }
          //contactViewHolder.imgUser.setImageResource(R.drawable.logo);
        }
        if(contact.status == Contact.Status.INITIALIZING) {
          contactViewHolder.txtOption.setVisibility(View.GONE);
        } else {
          contactViewHolder.txtOption.setVisibility(View.VISIBLE);
          if(contact.status == Contact.Status.FRIENDS) {
            contactViewHolder.txtOption.setText(R.string.friends);
            contactViewHolder.txtOption.setBackgroundResource(R.drawable.bg_friends);
          } else if(contact.status == Contact.Status.FRIEND_REQUEST_PENDING) {
            contactViewHolder.txtOption.setText(R.string.resend);
            if(contact.inProcessing) {
              contactViewHolder.txtOption.setBackgroundResource(
                R.drawable.bg_add_invite_resend_reinvite_pressed);
            } else {
              contactViewHolder.txtOption.setBackgroundResource(
                R.drawable.bg_add_invite_resend_reinvite);
            }
          } else if(contact.status == Contact.Status.INVITATION_PENDING) {
            contactViewHolder.txtOption.setText(R.string.re_invite);
            if(contact.inProcessing) {
              contactViewHolder.txtOption.setBackgroundResource(
                R.drawable.bg_add_invite_resend_reinvite_pressed);
            } else {
              contactViewHolder.txtOption.setBackgroundResource(
                R.drawable.bg_add_invite_resend_reinvite);
            }
          } else if(contact.status == Contact.Status.USER_EXIST) {
            contactViewHolder.txtOption.setText(R.string.add);
            if(contact.inProcessing) {
              contactViewHolder.txtOption.setBackgroundResource(
                R.drawable.bg_add_invite_resend_reinvite_pressed);
            } else {
              contactViewHolder.txtOption.setBackgroundResource(
                R.drawable.bg_add_invite_resend_reinvite);
            }
          } else {
            contactViewHolder.txtOption.setText(R.string.invite);
            if(contact.inProcessing) {
              contactViewHolder.txtOption.setBackgroundResource(
                R.drawable.bg_add_invite_resend_reinvite_pressed);
            } else {
              contactViewHolder.txtOption.setBackgroundResource(
                R.drawable.bg_add_invite_resend_reinvite);
            }
          }
        }
        final ContactViewHolder holder = contactViewHolder;
        contactViewHolder.txtOption.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View view) {
            if(contact.status == Contact.Status.USER_EXIST ||
                 contact.status == Contact.Status.FRIEND_REQUEST_PENDING) {
              // Send friend request here
              contact.inProcessing = true;
              holder.txtOption.setBackgroundResource(
                R.drawable.bg_add_invite_resend_reinvite_pressed);
              sendFriendRequest(contact);
            } else if(contact.status == Contact.Status.USER_DOES_NOT_EXIST ||
                        contact.status == Contact.Status.INVITATION_PENDING) {
              // Send invite here
              if(contact.email != null) {
                if(ParseUser.getCurrentUser().getUsername().contains("@") ||
                     (ParseUser.getCurrentUser().getEmail() != null &&
                        !ParseUser.getCurrentUser().getEmail().toString().isEmpty())) {
                  contact.inProcessing = true;
                  holder.txtOption.setBackgroundResource(
                    R.drawable.bg_add_invite_resend_reinvite_pressed);
                  sendInvite(contact);
                } else {
                  showEmailRequiredDialog(contact, holder.txtOption);
                }
              } else {
                getApplicationContext();
                Singleton.sendToast(R.string.email_not_available);
              }
            }
          }
        });
      } else {
        String str = (String) getItem(position);
        if(cellView == null) {
          titleViewHolder = new TitleViewHolder();
          cellView = inflater.inflate(R.layout.cell_title, null);
          titleViewHolder.txtAlphabet = (TextView) cellView.findViewById(R.id.txt_alphabet);
          cellView.setTag(titleViewHolder);
        } else {
          titleViewHolder = (TitleViewHolder) cellView.getTag();
        }
        titleViewHolder.txtAlphabet.setText(str);
      }
      return cellView;
    }

    @Override
    public Object[] getSections() {
      String[] sectionsArr = new String[stringArrayList.size()];
      for(int i = 0; i < stringArrayList.size(); i++) {
        sectionsArr[i] = "" + stringArrayList.get(i);
      }
      return sectionsArr;
    }

    @Override
    public int getPositionForSection(int sectionIndex) {
      for(int i = 0; i < this.getCount(); i++) {
        if(this.getItem(i) instanceof String) {
          String item = ((String) this.getItem(i)).toLowerCase();
          if(item.equalsIgnoreCase(stringArrayList.get(sectionIndex))) {
            return i;
          }
        }
      }
      return 0;
    }

    @Override
    public int getSectionForPosition(int position) {
      for(int i = 0; i < stringArrayList.size(); i++) {
        if(this.getItem(position) instanceof String) {
          String item = ((String) this.getItem(position));
          if(item.equalsIgnoreCase(stringArrayList.get(i))) {
            return i;
          }
        } else {
          String item = ((Contact) this.getItem(position)).displayName.substring(0, 1);
          if(item.equalsIgnoreCase(stringArrayList.get(i))) {
            return i;
          }
        }
      }
      return 0;
    }

    private void showEmailRequiredDialog(final Contact contact, final TextView txtOption) {
      android.app.AlertDialog.Builder alert =
        new android.app.AlertDialog.Builder(ImportContactsActivity.this);
      alert.setTitle(R.string.dialog_title_email);
      alert.setMessage(R.string.dialog_message_email_required);
      alert.setCancelable(false);
      LayoutInflater inflater = (LayoutInflater) getSystemService(Service.LAYOUT_INFLATER_SERVICE);
      View view = inflater.inflate(R.layout.dialog_email, null);
      final ProgressBar pb = (ProgressBar) view.findViewById(R.id.pb);
      final EditText etEmail = (EditText) view.findViewById(R.id.et_email);
      alert.setView(view);
      alert.setNegativeButton(R.string.dialog_btn_cancel, new DialogInterface.OnClickListener() {
        public void onClick(DialogInterface dialog, int arg1) {
          dialog.dismiss();
        }
      });
      alert.setPositiveButton(R.string.dialog_btn_submit, new DialogInterface.OnClickListener() {
        public void onClick(DialogInterface dialog, int which) {
        }
      });
      final android.app.AlertDialog dialog = alert.create();
      dialog.show();
      dialog.getButton(android.app.AlertDialog.BUTTON_POSITIVE).setOnClickListener(
        new View.OnClickListener() {
          @Override
          public void onClick(View v) {
            if(pb.getVisibility() != View.VISIBLE) {
              final String email = etEmail.getText().toString().trim();
              if(email.isEmpty()) {
                getApplicationContext();
                Singleton.sendToast(R.string.validation_email);
                return;
              } else if(!email.contains("@")) {
                getApplicationContext();
                Singleton.sendToast(R.string.validation_email_invalid);
                return;
              }
              pb.setVisibility(View.VISIBLE);
              ParseQuery<ParseUser> userParseQuery = ParseUser.getQuery();
              userParseQuery.whereEqualTo("username", email);
              ParseQuery<ParseUser> userParseQuery2 = ParseUser.getQuery();
              userParseQuery2.whereEqualTo("email", email);
              // Club all the queries into one master query
              List<ParseQuery<ParseUser>> queries = new ArrayList<>();
              queries.add(userParseQuery);
              queries.add(userParseQuery2);
              ParseQuery<ParseUser> mainQuery = ParseQuery.or(queries);
              mainQuery.findInBackground(new FindCallback<ParseUser>() {
                @Override
                public void done(List<ParseUser> list, ParseException e) {
                  if(e == null) {
                    if(list == null || list.size() == 0) {
                      ParseUser parseUserCurrent = ParseUser.getCurrentUser();
                      parseUserCurrent.setEmail(email.toLowerCase().trim());
                      parseUserCurrent.saveInBackground(new SaveCallback() {
                        @Override
                        public void done(ParseException e) {
                          pb.setVisibility(View.GONE);
                          if(weakRef.get() != null && !weakRef.get().isFinishing()) {
                            if(e == null) {
                              getApplicationContext();
                              Singleton.sendToast(R.string.email_updated_successfully);
                              contact.inProcessing = true;
                              txtOption.setBackgroundResource(
                                R.drawable.bg_add_invite_resend_reinvite_pressed);
                              sendInvite(contact);
                            } else {
                              Singleton.sendToast(e);
                            }
                          }
                          dialog.dismiss();
                        }
                      });
                    } else {
                      pb.setVisibility(View.GONE);
                      // display popup
                      showEmailAlreadyRegisteredAlert(email);
                    }
                  } else {
                    pb.setVisibility(View.GONE);
                    if(weakRef.get() != null && !weakRef.get().isFinishing()) {
                      Singleton.sendToast(e);
                    }
                  }
                }
              });
            }
          }
        });
    }

    public void showEmailAlreadyRegisteredAlert(String email) {
      AlertDialog.Builder alert = new AlertDialog.Builder(ImportContactsActivity.this);
      alert.setMessage(email + " " + getString(R.string.email_already_registered));
      alert.setPositiveButton(R.string.dialog_btn_ok, new DialogInterface.OnClickListener() {
        public void onClick(DialogInterface dialog, int which) {
          dialog.dismiss();
        }
      });
      alert.create().show();
    }

    private void sendInvite(final Contact contact) {
      String email = null;
      if(ParseUser.getCurrentUser().getUsername().contains("@")) {
        email = ParseUser.getCurrentUser().getUsername();
      } else {
        email = (String) ParseUser.getCurrentUser().getEmail();
      }
      final String name = ParseUser.getCurrentUser().get("firstName") + " " +
                            ParseUser.getCurrentUser().get("lastName");
      ParseQuery<ParseObject> query = ParseQuery.getQuery("Cell411Alert");
      query.whereEqualTo("issuedBy", ParseUser.getCurrentUser());
      query.whereEqualTo("to", contact.email);
      query.whereEqualTo("status", "PENDING");
      query.whereEqualTo("entryFor", "FI");
      final String finalEmail = email;
      query.findInBackground(new FindCallback<ParseObject>() {
        @Override
        public void done(List<ParseObject> list, ParseException e) {
          if(list != null && list.size() > 0) {
            contact.inProcessing = false;
            // An entry in Cell411Alert has already been created which is pending, so only send an email to the user
            // send email invitation here
            contact.status = Contact.Status.INVITATION_PENDING;
            // send email invitation here
            HashMap<String, Object> params = new HashMap<>();
            params.put("email", contact.email);
            params.put("name", name);
            params.put("senderEmail", finalEmail);
            params.put("clientFirmId", 1);
            params.put("isLive", getResources().getBoolean(R.bool.enable_publish));
            params.put("languageCode", getString(R.string.language_code));
            params.put("platform", "android");
            ParseCloud.callFunctionInBackground("sendInvite", params,
              new FunctionCallback<String>() {
                public void done(String result, ParseException e) {
                  if(e == null) {
                    // Email sent successfully
                    LogEvent.Log(TAG, "Email sent successfully");
                  } else {
                    LogEvent.Log(TAG, "Something went wrong: " + e.getMessage());
                  }
                }
              });
            contactsListAdapter.notifyDataSetChanged();
          } else {
            // make an entry in Invite table and send email invitation here
            final ParseObject inviteObject = new ParseObject("Cell411Alert");
            inviteObject.put("issuedBy", ParseUser.getCurrentUser());
            inviteObject.put("issuerFirstName", name);
            inviteObject.put("to", contact.email);
            inviteObject.put("status", "PENDING");
            inviteObject.put("entryFor", "FI");
            inviteObject.saveInBackground(new SaveCallback() {
              @Override
              public void done(ParseException e) {
                contact.inProcessing = false;
                if(e == null) {
                  // send email invitation here
                  contact.status = Contact.Status.INVITATION_PENDING;
                  // send email invitation here
                  HashMap<String, Object> params = new HashMap<>();
                  params.put("email", contact.email);
                  params.put("name", name);
                  params.put("senderEmail", finalEmail);
                  params.put("clientFirmId", 1);
                  params.put("isLive", getResources().getBoolean(R.bool.enable_publish));
                  params.put("languageCode", getString(R.string.language_code));
                  params.put("platform", "android");
                  ParseCloud.callFunctionInBackground("sendInvite", params,
                    new FunctionCallback<String>() {
                      public void done(String result, ParseException e) {
                        if(e == null) {
                          LogEvent.Log(TAG, "Email sent successfully");
                        } else {
                          LogEvent.Log(TAG, "Something went wrong: " + e.getMessage());
                        }
                      }
                    });
                }
                contactsListAdapter.notifyDataSetChanged();
              }
            });
          }
        }
      });
    }

    private void sendFriendRequest(final Contact contact) {
      ParseRelation relation = ParseUser.getCurrentUser().getRelation("spammedBy");
      ParseQuery parseQuery = relation.getQuery();
      parseQuery.findInBackground(new FindCallback<ParseObject>() {
        @Override
        public void done(List list, ParseException e) {
          if(e == null) {
            if(list != null) {
              boolean userHasSpammedCurrentUser = false;
              for(int i = 0; i < list.size(); i++) {
                if((((ParseUser) list.get(i)).getEmail() != null &&
                      ((ParseUser) list.get(i)).getEmail().equals(contact.email)) ||
                     ((ParseUser) list.get(i)).getUsername().equals(contact.email)) {
                  userHasSpammedCurrentUser = true;
                  break;
                }
              }
              if(userHasSpammedCurrentUser) {
                showAlertDialog();
              } else {
                final String name = ParseUser.getCurrentUser().get("firstName") + " " +
                                      ParseUser.getCurrentUser().get("lastName");
                final ParseObject friendRequestObject = new ParseObject("Cell411Alert");
                friendRequestObject.put("issuedBy", ParseUser.getCurrentUser());
                friendRequestObject.put("issuerFirstName", name);
                friendRequestObject.put("to", contact.email);
                friendRequestObject.put("status", "PENDING");
                friendRequestObject.put("entryFor", "FR");
                friendRequestObject.saveInBackground(new SaveCallback() {
                  @Override
                  public void done(ParseException e) {
                    if(e == null) {
                      // Send notification for friend request here
                      ParseQuery pushQuery = ParseInstallation.getQuery();
                      pushQuery.whereEqualTo("user", contact.user);
                      ParsePush push = new ParsePush();
                      push.setQuery(pushQuery);
                      JSONObject data = new JSONObject();
                      try {
                        data.put("alert", getString(R.string.notif_friend_request, name,
                          getString(R.string.app_name)));
                        data.put("userId", ParseUser.getCurrentUser().getObjectId());
                        data.put("sound", "default");
                        data.put("friendRequestObjectId", friendRequestObject.getObjectId());
                        data.put("name", name);
                        data.put("alertType", "FRIEND_REQUEST");
                        data.put("badge", "Increment");
                      } catch(JSONException e1) {
                        e1.printStackTrace();
                      }
                      push.setData(data);
                      push.sendInBackground();
                      contact.inProcessing = false;
                      contact.status = Contact.Status.FRIEND_REQUEST_PENDING;
                    } else {
                      contact.inProcessing = false;
                    }
                    contactsListAdapter.notifyDataSetChanged();
                  }
                });
              }
              contactsListAdapter.notifyDataSetChanged();
            } else {
              // Seems like the list is empty
              LogEvent.Log("ImportContactsActivity", "Spam users list is null");
            }
          } else {
            contact.inProcessing = false;
            contactsListAdapter.notifyDataSetChanged();
          }
        }
      });
    }
  }
}