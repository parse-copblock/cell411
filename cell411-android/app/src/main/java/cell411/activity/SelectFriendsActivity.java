package cell411.activity;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import androidx.appcompat.app.ActionBar;

import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseRelation;
import com.parse.ParseUser;
import com.parse.callback.FindCallback;
import com.safearx.cell411.R;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import cell411.Singleton;
import cell411.models.NewPrivateCell;
import cell411.models.User;
import cell411.utils.LogEvent;

/**
 * Created by Sachin on 7/13/2015.
 */
public class SelectFriendsActivity extends BaseActivity {
  private static final String TAG = "SelectFriendsActivity";
  public static NewPrivateCell newPrivateCell;
  public static WeakReference<SelectFriendsActivity> weakRef;
  private ListView listView;
  private ArrayList<User> friendList;
  private FriendsListAdapter adapterFriends;
  private ArrayList<ParseUser> membersArrayList;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_private_cell_friends);
    weakRef = new WeakReference<>(this);
    Singleton.INSTANCE.setCurrentActivity(this);
    final ActionBar actionBar = getSupportActionBar();
    actionBar.setDisplayHomeAsUpEnabled(true);
    actionBar.setDisplayShowHomeEnabled(true);
    listView = (ListView) findViewById(R.id.list_friends);
    String cellObjectId = getIntent().getStringExtra("cellObjectId");
    ArrayList<NewPrivateCell> newPrivateCellArrayList =
      Singleton.INSTANCE.getNewPrivateCells();
    for(int i = 0; i < newPrivateCellArrayList.size(); i++) {
      if(newPrivateCellArrayList.get(i).parseObject.getObjectId().equals(cellObjectId)) {
        newPrivateCell = newPrivateCellArrayList.get(i);
        break;
      }
    }
    //membersArrayList = (ArrayList<ParseUser>) newPrivateCell.members.clone();
    membersArrayList = new ArrayList<>();
    // add non-duplicate users into the array (this is done as the members attribute in Cell
    // class has duplicate users)
    for(int i = 0; i < newPrivateCell.members.size(); i++) {
      boolean duplicate = false;
      for(int j = 0; j < membersArrayList.size(); j++) {
        if(newPrivateCell.members.get(i).getObjectId().equals(
          ((ParseUser) membersArrayList.get(j)).getObjectId())) {
          duplicate = true;
          break;
        }
      }
      if(!duplicate) {
        membersArrayList.add(newPrivateCell.members.get(i));
      }
    }
    LogEvent.Log(TAG, "newPrivateCell.members.size(): " + membersArrayList.size());
    if(Singleton.INSTANCE.getFriends() != null) {
      friendList = (ArrayList<User>) Singleton.INSTANCE.getFriends().clone();
      for(int i = 0; i < friendList.size(); i++) {
        LogEvent.Log(TAG, "selected: " + friendList.get(i).selected);
        friendList.get(i).selected = false;
      }
    }
    if(friendList == null) {
      ParseUser user = ParseUser.getCurrentUser();
      ParseRelation relFriends = user.getRelation("friends");
      ParseQuery query4Friends = relFriends.getQuery();
      query4Friends.findInBackground(new FindCallback<ParseObject>() {
        @Override
        public void done(List list, ParseException e) {
          friendList = new ArrayList<>();
          if(e == null) {
            for(int i = 0; i < list.size(); i++) {
              ParseUser user = (ParseUser) list.get(i);
              // do not show deleted user
              if(user.getInt("isDeleted") == 1) {
                continue;
              }
              String email = null;
              if(user.getEmail() != null && !user.getEmail().isEmpty()) {
                email = user.getEmail();
              } else {
                email = user.getUsername();
              }
              friendList.add(
                new User(email, (String) user.get("firstName"), (String) user.get("lastName"),
                  user));
            }
            for(int i = 0; i < friendList.size(); i++) {
              for(int j = 0; j < membersArrayList.size(); j++) {
                if(membersArrayList.get(j).getObjectId().equals(
                  friendList.get(i).user.getObjectId())) {
                  friendList.get(i).selected = true;
                }
              }
            }
            adapterFriends =
              new FriendsListAdapter(SelectFriendsActivity.this, R.layout.cell_friends,
                friendList);
            listView.setAdapter(adapterFriends);
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
              @Override
              public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                if(adapterFriends.friendsList.get(position).selected) {
                  for(int i = 0; i < membersArrayList.size(); i++) {
                    if(membersArrayList.get(i).getObjectId().equals(
                      adapterFriends.friendsList.get(position).user.getObjectId())) {
                      membersArrayList.remove(i);
                      break;
                    }
                  }
                  adapterFriends.friendsList.get(position).selected = false;
                  adapterFriends.notifyDataSetChanged();
                } else {
                  membersArrayList.add(adapterFriends.friendsList.get(position).user);
                  adapterFriends.friendsList.get(position).selected = true;
                  adapterFriends.notifyDataSetChanged();
                }
              }
            });
          } else {
            if(weakRef.get() != null && !weakRef.get().isFinishing()) {
              Singleton.sendToast(e);
            }
          }
        }
      });
    } else {
      for(int i = 0; i < friendList.size(); i++) {
        if(friendList.get(i).user.getInt("isDeleted") == 1) {
          friendList.remove(i);
          continue;
        }
        for(int j = 0; j < membersArrayList.size(); j++) {
          if(membersArrayList.get(j).getObjectId().equals(friendList.get(i).user.getObjectId())) {
            friendList.get(i).selected = true;
          }
        }
      }
      adapterFriends = new FriendsListAdapter(SelectFriendsActivity.this, R.layout.cell_friends,
        (ArrayList<User>) friendList.clone());
      listView.setAdapter(adapterFriends);
      listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
          if(adapterFriends.friendsList.get(position).selected) {
            for(int i = 0; i < membersArrayList.size(); i++) {
              if(membersArrayList.get(i).getObjectId().equals(
                adapterFriends.friendsList.get(position).user.getObjectId())) {
                membersArrayList.remove(i);
                break;
              }
            }
            adapterFriends.friendsList.get(position).selected = false;
            adapterFriends.notifyDataSetChanged();
          } else {
            membersArrayList.add(adapterFriends.friendsList.get(position).user);
            adapterFriends.friendsList.get(position).selected = true;
            adapterFriends.notifyDataSetChanged();
          }
        }
      });
    }
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    switch(item.getItemId()) {
      case android.R.id.home:
        finish();
        return true;
      default:
        return super.onOptionsItemSelected(item);
    }
  }

  @Override
  protected void onPause() {
    super.onPause();
    newPrivateCell.members = membersArrayList;
    newPrivateCell.parseObject.put("members", membersArrayList);
    newPrivateCell.parseObject.saveInBackground();
  }

  private static class FriendsListAdapter extends ArrayAdapter<User> {
    private int resource;
    private LayoutInflater inflater;
    private ArrayList<User> friendsList;

    public FriendsListAdapter(Context context, int resource, ArrayList<User> objects) {
      super(context, resource, objects);
      this.resource = resource;
      inflater = ((Activity) context).getLayoutInflater();
      friendsList = objects;
    }

    public int getCount() {
      return friendsList.size();
    }

    public View getView(final int position, View convertView1, ViewGroup parent) {
      View cellView = convertView1;
      ViewHolder holder = null;
      if(cellView == null) {
        holder = new ViewHolder();
        cellView = inflater.inflate(resource, null);
        holder.txtUserName = (TextView) cellView.findViewById(R.id.txt_user_name);
        holder.imgUser = (ImageView) cellView.findViewById(R.id.img_user);
        holder.imgTick = (ImageView) cellView.findViewById(R.id.img_tick);
        cellView.setTag(holder);
      } else {
        holder = (ViewHolder) cellView.getTag();
        holder.imgUser.setImageBitmap(null);
      }
      holder.txtUserName.setText(
        friendsList.get(position).firstName + " " + friendsList.get(position).lastName);
      if(friendsList.get(position).user == null) {
        holder.imgTick.setVisibility(View.GONE);
      } else {
        holder.imgTick.setVisibility(View.VISIBLE);
        if(friendsList.get(position).selected) {
          holder.imgTick.setBackgroundResource(R.drawable.bg_friend_selected);
        } else {
          holder.imgTick.setBackgroundResource(R.drawable.bg_friend_unselected);
        }
      }
      //            Singleton.imageLoader.displayImage(Singleton.gravatarImageUrl.replace
      //                    ("HASH", MD5Util.md5Hex(friendsList.get(position).email)), holder.imgUser);
      final ViewHolder finalHolder = holder;
      if(friendsList.get(position).email != null) {
        Singleton.INSTANCE.setImage(finalHolder.imgUser,
          friendsList.get(position).user.getObjectId() +
            friendsList.get(position).user.get("imageName"), friendsList.get(position).email);
      } else {
        finalHolder.imgUser.setImageResource(R.drawable.logo);
      }
      return cellView;
    }
  }

  private static class ViewHolder {
    private TextView txtUserName;
    private ImageView imgUser;
    private ImageView imgTick;
  }
}
