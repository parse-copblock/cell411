package cell411.activity;

import android.graphics.ColorFilter;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.safearx.cell411.R;

import java.lang.ref.WeakReference;

import cell411.Singleton;
import cell411.models.OSMObjective;

;

public class OSMObjectiveActivity extends BaseActivity {
  public static WeakReference<OSMObjectiveActivity> weakRef;
  private final String TAG = "OSMObjectiveActivity";

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_osm_objective);
    weakRef = new WeakReference<>(this);
    Singleton.INSTANCE.setCurrentActivity(this);
    OSMObjective osmObjective = (OSMObjective) getIntent().getSerializableExtra("osmObjective");
    if(osmObjective == null) {
      finish();
      return;
    }
    RelativeLayout rlIconContainer = findViewById(R.id.rl_mo_icon_container);
    ImageView imgCategoryIcon = findViewById(R.id.img_mo_category_icon);
    View viewHeader = findViewById(R.id.view_mo_header);
    ImageView imgClose = findViewById(R.id.img_mo_close);
    final ImageView imgMapObjective = findViewById(R.id.img_map_objective);
    TextView txtName = findViewById(R.id.txt_mo_name);
    TextView txtCategory = findViewById(R.id.txt_mo_category);
    ImageView imgCategoryIconSmall = findViewById(R.id.img_mo_category_icon_small);
    TextView txtTags = findViewById(R.id.txt_tags);
    ColorFilter colorFilter = null;
    if(osmObjective.getAmenity().equals("pharmacy")) {
      rlIconContainer.setBackgroundResource(R.drawable.bg_mo_pharmacy);
      imgCategoryIcon.setImageResource(R.drawable.ic_category_pharmacy);
      viewHeader.setBackgroundColor(getResources().getColor(R.color.mo_pharmacy));
      imgCategoryIconSmall.setImageResource(R.drawable.ic_category_pharmacy);
      colorFilter = new PorterDuffColorFilter(getResources().getColor(R.color.mo_pharmacy),
        PorterDuff.Mode.MULTIPLY);
    } else if(osmObjective.getAmenity().equals("hospital")) {
      rlIconContainer.setBackgroundResource(R.drawable.bg_mo_hospital);
      imgCategoryIcon.setImageResource(R.drawable.ic_category_hospital);
      viewHeader.setBackgroundColor(getResources().getColor(R.color.mo_hospital));
      imgCategoryIconSmall.setImageResource(R.drawable.ic_category_hospital);
      colorFilter = new PorterDuffColorFilter(getResources().getColor(R.color.mo_hospital),
        PorterDuff.Mode.MULTIPLY);
    } else {
      rlIconContainer.setBackgroundResource(R.drawable.bg_mo_police);
      imgCategoryIcon.setImageResource(R.drawable.ic_category_police);
      viewHeader.setBackgroundColor(getResources().getColor(R.color.mo_police));
      imgCategoryIconSmall.setImageResource(R.drawable.ic_category_police);
      colorFilter = new PorterDuffColorFilter(getResources().getColor(R.color.mo_police),
        PorterDuff.Mode.MULTIPLY);
    }
        /*if (colorFilter != null) {
            imgUrlIcon.setColorFilter(colorFilter);
            imgAddressIcon.setColorFilter(colorFilter);
            imgHoursIcon.setColorFilter(colorFilter);
            imgPhoneIcon.setColorFilter(colorFilter);
        }*/
    txtName.setText(osmObjective.getName());
    txtCategory.setText(osmObjective.getAmenity());
    txtTags.setText(osmObjective.getTags());
    //txtUrl.setText(url);
    //txtDescription.setText(description);
    //txtfullAddress.setText(fullAddress);
    //txtHours.setText(hours);
    //txtPhone.setText(phone);
        /*LogEvent.Log(TAG, "imageUrl: " + imageUrl);
        if (imageUrl != null) {
            Singleton.imageLoader.loadImage(imageUrl, new ImageLoadingListener() {
                @Override
                public void onLoadingStarted(String s, View view) {
                    LogEvent.Log(TAG, "onLoadingStarted(): " + s);
                }
                @Override
                public void onLoadingFailed(String s, View view, FailReason failReason) {
                    LogEvent.Log(TAG, "onLoadingFailed(): " + s + " === " + failReason.getCause().getMessage());
                }
                @Override
                public void onLoadingComplete(String s, View view, Bitmap bitmap) {
                    if (bitmap == null) {
                        LogEvent.Log(TAG, "onLoadingComplete(): " + s + " bitmap is null");
                    } else {
                        LogEvent.Log(TAG, "onLoadingComplete(): " + s + " bitmap is not null");
                    }
                    imgMapObjective.setImageBitmap(bitmap);
                }
                @Override
                public void onLoadingCancelled(String s, View view) {
                    LogEvent.Log(TAG, "onLoadingCancelled(): " + s);
                }
            });
        }*/
    imgClose.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        finish();
      }
    });
    findViewById(R.id.rl_mo_outer_wrapper).setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        // Close the activity when tapped on the transparent area (which is outside rl_mo_container)
        finish();
      }
    });
    findViewById(R.id.rl_mo_container).setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        // To capture click event and prevent from passing it down to rl_mo_outer_wrapper
      }
    });
    findViewById(R.id.rl_mo_icon_container).setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        // To capture click event and prevent from passing it down to rl_mo_outer_wrapper
      }
    });
  }
}