package cell411.activity;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.OvershootInterpolator;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.ActionBar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.github.clans.fab.FloatingActionMenu;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseUser;
import com.parse.callback.SaveCallback;
import com.safearx.cell411.R;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

import cell411.Singleton;
import cell411.models.Footer;
import cell411.models.NewPrivateCell;
import cell411.widgets.CircularImageView;
import cell411.widgets.Spinner;

/**
 * Created by Sachin on 7/13/2015.
 */
public class PrivateCellMembersActivity extends BaseActivity {
  private static final String TAG = "PrivateCellMembersActivity";
  public static NewPrivateCell newPrivateCell;
  public static WeakReference<PrivateCellMembersActivity> weakRef;
  private Spinner spinner;
  private RecyclerView recyclerView;
  private LinearLayoutManager linearLayoutManager;
  private ArrayList<Object> membersArrayList;
  private NAUMemberListAdapter nauMemberListAdapter;
  private boolean isSecurityGuardsAlertsEnabled;
  private FloatingActionMenu menuAddMember;
  private RelativeLayout rlSticky;

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    switch(item.getItemId()) {
      case android.R.id.home:
        finish();
        return true;
      default:
        return super.onOptionsItemSelected(item);
    }
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_private_cell_members);
    weakRef = new WeakReference<>(this);
    Singleton.INSTANCE.setCurrentActivity(this);
    final ActionBar actionBar = getSupportActionBar();
    actionBar.setDisplayHomeAsUpEnabled(true);
    actionBar.setDisplayShowHomeEnabled(true);
    setTitle(getIntent().getStringExtra("name"));
    spinner = (Spinner) findViewById(R.id.spinner);
    rlSticky = (RelativeLayout) findViewById(R.id.rl_sticky);
    recyclerView = (RecyclerView) findViewById(R.id.rv_nau_cells);
    // use this setting to improve performance if you know that changes
    // in content do not change the layout size of the RecyclerView
    recyclerView.setHasFixedSize(true);
    // use a linear layout manager
    linearLayoutManager = new LinearLayoutManager(this);
    recyclerView.setLayoutManager(linearLayoutManager);
    isSecurityGuardsAlertsEnabled = false;
    final String cellObjectId = getIntent().getStringExtra("cellObjectId");
    ArrayList<NewPrivateCell> newPrivateCellArrayList =
      Singleton.INSTANCE.getNewPrivateCells();
    for(int i = 0; i < newPrivateCellArrayList.size(); i++) {
      if(newPrivateCellArrayList.get(i).parseObject.getObjectId().equals(cellObjectId)) {
        newPrivateCell = newPrivateCellArrayList.get(i);
        break;
      }
    }
    boolean isNonAppUserAlertsEnabled = false;
    FloatingActionButton floatingActionButton =
      (FloatingActionButton) findViewById(R.id.fab_add_friend);
    menuAddMember = (FloatingActionMenu) findViewById(R.id.menu_add_member);
    menuAddMember.setClosedOnTouchOutside(true);
    menuAddMember.setVisibility(View.GONE);
    floatingActionButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        Intent intentSelectContact =
          new Intent(PrivateCellMembersActivity.this, SelectFriendsActivity.class);
        intentSelectContact.putExtra("cellObjectId", cellObjectId);
        startActivity(intentSelectContact);
      }
    });
    com.github.clans.fab.FloatingActionButton fabAddFriend =
      (com.github.clans.fab.FloatingActionButton) findViewById(
        R.id.action_add_from_cell411_friend);
    com.github.clans.fab.FloatingActionButton fabAddFromPhoneContact =
      (com.github.clans.fab.FloatingActionButton) findViewById(
        R.id.action_add_from_phone_contact);
    fabAddFriend.setLabelText(
      getString(R.string.fab_btn_add_from_friends, getString(R.string.app_name)));
    fabAddFriend.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        if(menuAddMember.isOpened()) {
          menuAddMember.close(true);
        }
        Intent intentSelectContact =
          new Intent(PrivateCellMembersActivity.this, SelectFriendsActivity.class);
        intentSelectContact.putExtra("cellObjectId", cellObjectId);
        startActivity(intentSelectContact);
      }
    });
    fabAddFromPhoneContact.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        if(menuAddMember.isOpened()) {
          menuAddMember.close(true);
        }
        Intent intentSelectContact =
          new Intent(PrivateCellMembersActivity.this, SelectContactsActivity.class);
        intentSelectContact.putExtra("cellObjectId", cellObjectId);
        startActivity(intentSelectContact);
      }
    });
    createCustomAnimation();
  }

  private void createCustomAnimation() {
    AnimatorSet set = new AnimatorSet();
    ObjectAnimator scaleOutX =
      ObjectAnimator.ofFloat(menuAddMember.getMenuIconView(), "scaleX", 1.0f, 0.2f);
    ObjectAnimator scaleOutY =
      ObjectAnimator.ofFloat(menuAddMember.getMenuIconView(), "scaleY", 1.0f, 0.2f);
    ObjectAnimator scaleInX =
      ObjectAnimator.ofFloat(menuAddMember.getMenuIconView(), "scaleX", 0.2f, 1.0f);
    ObjectAnimator scaleInY =
      ObjectAnimator.ofFloat(menuAddMember.getMenuIconView(), "scaleY", 0.2f, 1.0f);
    scaleOutX.setDuration(50);
    scaleOutY.setDuration(50);
    scaleInX.setDuration(150);
    scaleInY.setDuration(150);
    scaleInX.addListener(new AnimatorListenerAdapter() {
      @Override
      public void onAnimationStart(Animator animation) {
        menuAddMember.getMenuIconView().setImageResource(
          menuAddMember.isOpened() ? R.drawable.fab_add_member_to_cell :
            R.drawable.fab_menu_close);
      }
    });
    set.play(scaleOutX).with(scaleOutY);
    set.play(scaleInX).with(scaleInY).after(scaleOutX);
    set.setInterpolator(new OvershootInterpolator(2));
    menuAddMember.setIconToggleAnimatorSet(set);
  }

  @Override
  protected void onResume() {
    super.onResume();
    displayMembers();
  }

  private void displayMembers() {
    // Add app user to the array list
    membersArrayList = new ArrayList<>();
    //membersArrayList.addAll(newPrivateCell.members);
    // add non-duplicate users into the array (this is done as the members attribute in Cell
    // class has duplicate users)
    for(int i = 0; i < newPrivateCell.members.size(); i++) {
      boolean duplicate = false;
      for(int j = 0; j < membersArrayList.size(); j++) {
        if(newPrivateCell.members.get(i).getObjectId().equals(
          ((ParseUser) membersArrayList.get(j)).getObjectId())) {
          duplicate = true;
          break;
        }
      }
      if(!duplicate) {
        membersArrayList.add(newPrivateCell.members.get(i));
      }
    }
    if(membersArrayList.size() == 0) {
      rlSticky.setVisibility(View.VISIBLE);
    } else {
      if(membersArrayList.size() == 1 && isSecurityGuardsAlertsEnabled) {
        rlSticky.setVisibility(View.VISIBLE);
      } else {
        rlSticky.setVisibility(View.GONE);
      }
      if(nauMemberListAdapter != null) {
        nauMemberListAdapter.arrayList.clear();
        nauMemberListAdapter.arrayList.addAll(membersArrayList);
        nauMemberListAdapter.notifyDataSetChanged();
      } else {
        nauMemberListAdapter = new NAUMemberListAdapter(membersArrayList);
        recyclerView.setAdapter(nauMemberListAdapter);
      }
    }
  }

  public class NAUMemberListAdapter extends RecyclerView.Adapter<NAUMemberListAdapter.ViewHolder> {
    private final int VIEW_TYPE_APP_USER = 0;
    private final int VIEW_TYPE_CALL_CENTER = 1;
    //private final int VIEW_TYPE_NAU_MEMBER = 2;
    private final int VIEW_TYPE_FOOTER = 3;
    private final View.OnClickListener mOnClickListener = new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        int position = recyclerView.getChildAdapterPosition(v);
      }
    };
    public ArrayList<Object> arrayList;

    // Provide a suitable constructor (depends on the kind of data set)
    public NAUMemberListAdapter(ArrayList<Object> arrayList) {
      this.arrayList = arrayList;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public NAUMemberListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
      // create a new view
      View v = null;
      if(viewType == VIEW_TYPE_APP_USER || viewType == VIEW_TYPE_CALL_CENTER) {
        v = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_friends_new, parent,
          false);
      } else if(viewType == VIEW_TYPE_FOOTER) {
        v = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_footer, parent, false);
      }
      // set the view's size, margins, paddings and layout parameters
      NAUMemberListAdapter.ViewHolder vh = new NAUMemberListAdapter.ViewHolder(v, viewType);
      if(viewType == VIEW_TYPE_APP_USER ) {
        v.setOnClickListener(mOnClickListener);
      }
      return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final NAUMemberListAdapter.ViewHolder viewHolder,
                                 final int position) {
      // - get element from your data set at this position
      // - replace the contents of the view with that element
      if(getItemViewType(position) == VIEW_TYPE_APP_USER) {
        final ParseUser parseUser = (ParseUser) arrayList.get(position);
        final String name = parseUser.get("firstName") + " " + parseUser.get("lastName");
        viewHolder.txtName.setText(name);
        if(parseUser.getInt("isDeleted") == 1) {
          viewHolder.txtName.setTextColor(getResources().getColor(R.color.text_disabled_hint_icon));
        } else {
          viewHolder.txtName.setTextColor(getResources().getColor(R.color.text_primary));
        }
        viewHolder.imgRemove.setVisibility(View.VISIBLE);
        viewHolder.imgRemove.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View v) {
            deleteMemberFromCell(position, name);
          }
        });
      } else if(getItemViewType(position) == VIEW_TYPE_CALL_CENTER) {
        final String str = (String) arrayList.get(position);
        viewHolder.txtName.setText(str);
        viewHolder.imgRemove.setVisibility(View.GONE);
      } else {
        final Footer footer = (Footer) arrayList.get(position);
        viewHolder.txtInfo.setText(footer.info);
        if(footer.spinnerVisibility) {
          viewHolder.pb.setVisibility(View.VISIBLE);
        } else {
          viewHolder.pb.setVisibility(View.GONE);
        }
      }
    }

    // Return the size of your data set (invoked by the layout manager)
    @Override
    public int getItemCount() {
      return arrayList.size();
    }

    @Override
    public int getItemViewType(int position) {
      super.getItemViewType(position);
      if(arrayList.get(position) instanceof ParseObject) {
        return VIEW_TYPE_APP_USER;
      } else if(arrayList.get(position) instanceof String) {
        return VIEW_TYPE_CALL_CENTER;
      } else {
        return VIEW_TYPE_FOOTER;
      }
    }

    private void deleteMemberFromCell(final int position, final String name) {
      AlertDialog.Builder alert = new AlertDialog.Builder(PrivateCellMembersActivity.this);
      alert.setMessage(getString(R.string.dialog_msg_remove_member_from_cell, name));
      alert.setNegativeButton(R.string.dialog_btn_cancel, new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialogInterface, int i) {
        }
      });
      alert.setPositiveButton(R.string.dialog_btn_yes, new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
          arrayList.remove(position);
          notifyDataSetChanged();
          if(arrayList.size() == 0) {
            rlSticky.setVisibility(View.VISIBLE);
          }
          final ArrayList<ParseUser> parseUsers = new ArrayList<ParseUser>();
          for(int i = 0; i < arrayList.size(); i++) {
            if(arrayList.get(i) instanceof ParseUser) {
              parseUsers.add((ParseUser) arrayList.get(i));
            } else {
              break;
            }
          }
          newPrivateCell.parseObject.put("members", parseUsers);
          newPrivateCell.parseObject.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
              if(e == null) {
                newPrivateCell.members = parseUsers;
              } else {
                if(weakRef.get() != null && !weakRef.get().isFinishing()) {
                  Singleton.sendToast(e);
                }
              }
            }
          });
        }
      });
      AlertDialog dialog = alert.create();
      dialog.show();
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public class ViewHolder extends RecyclerView.ViewHolder {
      // each data item is just a string in this case
      private CircularImageView imgUser;
      private TextView txtName;
      private TextView txtPhoneOrEmail;
      private ImageView imgRemove;
      private TextView txtInfo;
      private ProgressBar pb;

      public ViewHolder(View view, int type) {
        super(view);
        if(type == VIEW_TYPE_APP_USER || type == VIEW_TYPE_CALL_CENTER) {
          imgUser = (CircularImageView) view.findViewById(R.id.img_user);
          txtName = (TextView) view.findViewById(R.id.txt_name);
          imgRemove = (ImageView) view.findViewById(R.id.img_remove);
        } else if(type == VIEW_TYPE_FOOTER) {
          txtInfo = (TextView) view.findViewById(R.id.txt_info);
          pb = (ProgressBar) view.findViewById(R.id.pb);
        }
      }
    }
  }
}