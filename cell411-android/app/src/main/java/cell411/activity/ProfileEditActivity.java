package cell411.activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.ActionBar;

import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.callback.FindCallback;
import com.parse.callback.SaveCallback;
import com.safearx.cell411.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

import cell411.Singleton;
import cell411.constants.LMA;
import cell411.constants.ParseKeys;
import cell411.methods.UtilityMethods;
import cell411.models.CountryInfo;
import cell411.utils.LogEvent;
import cell411.widgets.Spinner;

/**
 * Created by Sachin on 19-04-2016.
 */
public class ProfileEditActivity extends BaseActivity implements View.OnClickListener {
  private static final String TAG = "ProfileEditActivity";
  public static WeakReference<ProfileEditActivity> weakRef;
  private EditText etFirstName;
  private EditText etLastName;
  private EditText etEmail;
  private EditText etMobileNumber;
  private EditText etEmergencyContactName;
  private EditText etEmergencyContactPhone;
  private EditText etAllergies;
  private EditText etOtherMedicalConditions;
  private android.widget.Spinner spCountryCode;
  private android.widget.Spinner spEmergencyCountryCode;
  private ArrayList<CountryInfo> list;
  private ArrayList<CountryInfo> listEmergencyPhoneCountryCode;
  private TextView txtLblAMinus;
  private TextView txtLblAPlus;
  private TextView txtLblBMinus;
  private TextView txtLblBPlus;
  private TextView txtLblABMinus;
  private TextView txtLblABPlus;
  private TextView txtLblOMinus;
  private TextView txtLblOPlus;
  private boolean isPhoneVerificationEnabled;
  private boolean mobileNumberChanged = false;
  private Spinner spinner;
  private BloodType bloodType = null;

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    MenuInflater inflater = getMenuInflater();
    inflater.inflate(R.menu.menu_profile_edit, menu);
    return true;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    switch(item.getItemId()) {
      case android.R.id.home:
        finish();
        return true;
      case R.id.action_done:
        updateProfile();
        return true;
      default:
        return super.onOptionsItemSelected(item);
    }
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_profile_edit);
    // Set up the action bar.
    final ActionBar actionBar = getSupportActionBar();
    actionBar.setDisplayHomeAsUpEnabled(true);
    weakRef = new WeakReference<>(this);
    Singleton.INSTANCE.setCurrentActivity(this);
    ParseUser user = ParseUser.getCurrentUser();
    spinner = (Spinner) findViewById(R.id.spinner);
    etFirstName = (EditText) findViewById(R.id.et_first_name);
    etLastName = (EditText) findViewById(R.id.et_last_name);
    etEmail = (EditText) findViewById(R.id.et_email);
    etMobileNumber = (EditText) findViewById(R.id.et_mobile);
    etEmergencyContactName = (EditText) findViewById(R.id.et_emergency_name);
    etEmergencyContactPhone = (EditText) findViewById(R.id.et_emergency_mobile);
    txtLblAMinus = (TextView) findViewById(R.id.txt_lbl_a_minus);
    txtLblAPlus = (TextView) findViewById(R.id.txt_lbl_a_plus);
    txtLblBMinus = (TextView) findViewById(R.id.txt_lbl_b_minus);
    txtLblBPlus = (TextView) findViewById(R.id.txt_lbl_b_plus);
    txtLblABMinus = (TextView) findViewById(R.id.txt_lbl_ab_minus);
    txtLblABPlus = (TextView) findViewById(R.id.txt_lbl_ab_plus);
    txtLblOMinus = (TextView) findViewById(R.id.txt_lbl_o_minus);
    txtLblOPlus = (TextView) findViewById(R.id.txt_lbl_o_plus);
    txtLblAMinus.setOnClickListener(this);
    txtLblAPlus.setOnClickListener(this);
    txtLblBMinus.setOnClickListener(this);
    txtLblBPlus.setOnClickListener(this);
    txtLblABMinus.setOnClickListener(this);
    txtLblABPlus.setOnClickListener(this);
    txtLblOMinus.setOnClickListener(this);
    txtLblOPlus.setOnClickListener(this);
    etAllergies = (EditText) findViewById(R.id.et_allergies);
    etOtherMedicalConditions = (EditText) findViewById(R.id.et_other_medical_conditions);
    etFirstName.setText((String) user.get("firstName"));
    etLastName.setText((String) user.get("lastName"));
    if(user.getUsername().contains("@")) {
      etEmail.setText(user.getUsername());
    } else if(user.get("email") != null && !user.get("email").toString().isEmpty()) {
      etEmail.setText(user.getEmail());
    } else {
      etEmail.setText("");
    }
    list = new ArrayList<CountryInfo>();
    UtilityMethods.initializeCountryCodeList(list,
      1,
      getResources().getBoolean(R.bool.enable_publish));
    list.add(0, new CountryInfo(getString(R.string.code), getString(R.string.code), null, 0));
    spCountryCode = (android.widget.Spinner) findViewById(R.id.sp_country_code);
    final CountryListAdapter countryListAdapter =
      new CountryListAdapter(this, R.layout.cell_country, list);
    countryListAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
    spCountryCode.setAdapter(countryListAdapter);
    spCountryCode.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
      @Override
      public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
        CountryInfo countryInfo = (CountryInfo) spCountryCode.getSelectedItem();
        for(int i = 0; i < list.size(); i++) {
          list.get(i).selected = false;
        }
        countryInfo.selected = true;
        LogEvent.Log(TAG,
          "countryInfo: " + countryInfo.name + " (" + countryInfo.shortCode + ") + " +
            countryInfo.dialingCode);
        LogEvent.Log(TAG, "position: " + position);
        countryListAdapter.notifyDataSetChanged();
      }

      @Override
      public void onNothingSelected(AdapterView<?> adapterView) {
      }
    });
    listEmergencyPhoneCountryCode = new ArrayList<CountryInfo>();
    UtilityMethods.initializeCountryCodeList(listEmergencyPhoneCountryCode,
      1,
      getResources().getBoolean(R.bool.enable_publish));
    listEmergencyPhoneCountryCode.add(0,
      new CountryInfo(getString(R.string.code), getString(R.string.code), null, 0));
    spEmergencyCountryCode =
      (android.widget.Spinner) findViewById(R.id.sp_emergency_mobile_country_code);
    final CountryListAdapter countryListAdapterEmergencyNumber =
      new CountryListAdapter(this, R.layout.cell_country, listEmergencyPhoneCountryCode);
    countryListAdapterEmergencyNumber.setDropDownViewResource(
      android.R.layout.simple_spinner_dropdown_item);
    spEmergencyCountryCode.setAdapter(countryListAdapterEmergencyNumber);
    spEmergencyCountryCode.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
      @Override
      public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
        CountryInfo countryInfo = (CountryInfo) spEmergencyCountryCode.getSelectedItem();
        for(int i = 0; i < listEmergencyPhoneCountryCode.size(); i++) {
          listEmergencyPhoneCountryCode.get(i).selected = false;
        }
        countryInfo.selected = true;
        countryListAdapterEmergencyNumber.notifyDataSetChanged();
        LogEvent.Log(TAG,
          "countryInfo: " + countryInfo.name + " (" + countryInfo.shortCode + ") + " +
            countryInfo.dialingCode);
        LogEvent.Log(TAG, "position: " + position);
      }

      @Override
      public void onNothingSelected(AdapterView<?> adapterView) {
      }
    });
    String mobileNumber = (String) user.get("mobileNumber");
    String emergencyContactNumber = (String) user.get("emergencyContactNumber");
    UtilityMethods.setPhoneAndCountryCode(mobileNumber, etMobileNumber, spCountryCode, list);
    UtilityMethods.setPhoneAndCountryCode(emergencyContactNumber, etEmergencyContactPhone,
      spEmergencyCountryCode, list);
    String bloodTypeStr = (String) user.get("bloodType");
    if(bloodTypeStr != null && !bloodTypeStr.isEmpty() && !bloodTypeStr.equals("null")) {
      selectBloodType(convertStringToEnum(bloodTypeStr));
    }
    String emergencyContactName = (String) user.get("emergencyContactName");
    if(emergencyContactName != null && !emergencyContactName.isEmpty() &&
         !emergencyContactName.equals("null")) {
      etEmergencyContactName.setText(emergencyContactName);
    }
    String allergies = (String) user.get("allergies");
    if(allergies != null && !allergies.isEmpty() && !allergies.equals("null")) {
      etAllergies.setText(allergies);
    }
    String otherMedicalConditions = (String) user.get("otherMedicalConditions");
    if(otherMedicalConditions != null && !otherMedicalConditions.isEmpty() &&
         !otherMedicalConditions.equals("null")) {
      etOtherMedicalConditions.setText(otherMedicalConditions);
    }
  }

  @Override
  protected void onResume() {
    super.onResume();
    UtilityMethods.setPhoneAndCountryCode(
      ParseUser.getCurrentUser().getString(ParseKeys.MOBILE_NUMBER), etMobileNumber,
      spCountryCode, list);
  }

  @Override
  public void onClick(View v) {
    int id = v.getId();
    switch(id) {
      case R.id.txt_lbl_a_minus:
        selectBloodType(BloodType.A_MINUS);
        break;
      case R.id.txt_lbl_a_plus:
        selectBloodType(BloodType.A_PLUS);
        break;
      case R.id.txt_lbl_b_minus:
        selectBloodType(BloodType.B_MINUS);
        break;
      case R.id.txt_lbl_b_plus:
        selectBloodType(BloodType.B_PLUS);
        break;
      case R.id.txt_lbl_ab_minus:
        selectBloodType(BloodType.AB_MINUS);
        break;
      case R.id.txt_lbl_ab_plus:
        selectBloodType(BloodType.AB_PLUS);
        break;
      case R.id.txt_lbl_o_minus:
        selectBloodType(BloodType.O_MINUS);
        break;
      case R.id.txt_lbl_o_plus:
        selectBloodType(BloodType.O_PLUS);
        break;
    }
  }

  private void updateProfile() {
    if(spinner.getVisibility() == View.VISIBLE) {
      return;
    }
    final String email = etEmail.getText().toString().toLowerCase().trim();
    String firstName = etFirstName.getText().toString().trim();
    String lastName = etLastName.getText().toString().trim();
    String mobileNumber = etMobileNumber.getText().toString().trim();
    String countryCode = "";
    if(spCountryCode.getSelectedItemPosition() > 0) {
      countryCode = ((CountryInfo) spCountryCode.getSelectedItem()).dialingCode;
    }
    // Check if the mobile number is changed, user needs to re-verify it
    // if the number is changed
    if(ParseUser.getCurrentUser().get(ParseKeys.MOBILE_NUMBER) == null ||
         !ParseUser.getCurrentUser().get(ParseKeys.MOBILE_NUMBER).toString().replaceAll("[\\D]+",
           "").equals(countryCode + mobileNumber)) {
      mobileNumberChanged = true;
    } else {
      mobileNumberChanged = false;
    }
    if(email.isEmpty() && ParseUser.getCurrentUser().getUsername().contains("@")) {
      getApplicationContext();
      Singleton.sendToast(R.string.please_enter_email);
    } else if(firstName.isEmpty()) {
      getApplicationContext();
      Singleton.sendToast(R.string.please_enter_first_name);
    } else if(lastName.isEmpty()) {
      getApplicationContext();
      Singleton.sendToast(R.string.please_enter_lastname);
    } else if(mobileNumber.isEmpty() && ParseUser.getCurrentUser().getUsername().contains("@")) {
      getApplicationContext();
      Singleton.sendToast(R.string.please_enter_mobile_number);
    } else if(spCountryCode.getSelectedItemPosition() < 1 &&
                ParseUser.getCurrentUser().getUsername().contains("@")) {
      getApplicationContext();
      Singleton.sendToast(R.string.please_select_country_code);
    } else {
      spinner.setVisibility(View.VISIBLE);
      final ParseUser parseUser = ParseUser.getCurrentUser();
      if(mobileNumberChanged) {
        // Check if the mobile number is not already registered
        ParseQuery<ParseUser> userParseQuery = ParseUser.getQuery();
        userParseQuery.whereNotEqualTo("objectId", ParseUser.getCurrentUser().getObjectId());
        userParseQuery.whereEqualTo("mobileNumber", countryCode + mobileNumber.trim());
        userParseQuery.findInBackground(new FindCallback<ParseUser>() {
          @Override
          public void done(List<ParseUser> objects, ParseException e) {
            if(e == null) {
              if(objects == null || objects.size() == 0) { // no records found, hence
                checkEmailAndSave(parseUser, email);
              } else {
                spinner.setVisibility(View.GONE);
                Singleton.sendToast(R.string.mobile_already_registered);
              }
            } else {
              spinner.setVisibility(View.GONE);
              Singleton.sendToast(e);
            }
          }
        });
      } else {
        checkEmailAndSave(parseUser, email);
      }
    }
  }

  private void checkEmailAndSave(final ParseUser parseUser, final String email) {
    final ParseUser currentUser = ParseUser.getCurrentUser();
    final String objectId = currentUser.getObjectId();
    if(currentUser.getUsername().contains("@")) {
      ParseQuery<ParseUser> userParseQuery = ParseUser.getQuery();
      userParseQuery.whereEqualTo("email", email);
      userParseQuery.findInBackground(new FindCallback<ParseUser>() {
        @Override
        public void done(List<ParseUser> list, ParseException e) {
          System.out.println("entering done");
          boolean fail = false;
          if(e != null) {
            Singleton.sendToast(e);
            return;
          }
          ;
          if(list != null && list.size() > 0) {
            System.out.println(currentUser.getObjectId());
            System.out.println(currentUser.getEmail());
            for(int i = 0; i < list.size(); i++) {
              ParseUser user = list.get(i);
              if(user.getObjectId().equals(objectId)) {
                continue;
              }
              fail = true;
              break;
            }
          }
          if(fail) {
            spinner.setVisibility(View.GONE);
            showEmailAlreadyRegisteredAlert(email);
          } else {
            parseUser.setUsername(email.toLowerCase().trim());
            saveUser(parseUser);
          }
        }

        ;
      });
    } else if(!email.isEmpty()) {
      LogEvent.Log("ProfileEditActivity", "!email.isEmpty()");
      ParseQuery<ParseUser> userParseQuery = ParseUser.getQuery();
      userParseQuery.whereEqualTo("username", email);
      userParseQuery.findInBackground(new FindCallback<ParseUser>() {
        @Override
        public void done(List<ParseUser> list, ParseException e) {
          if(e == null) {
            if(list == null || list.size() == 0) {
              parseUser.setEmail(email.toLowerCase().trim());
              saveUser(parseUser);
            } else {
              // show email already registered
              spinner.setVisibility(View.GONE);
              showEmailAlreadyRegisteredAlert(email);
            }
          } else {
            spinner.setVisibility(View.GONE);
            if(weakRef.get() != null && !weakRef.get().isFinishing()) {
              Singleton.sendToast(e);
            }
          }
        }
      });
    } else {
      saveUser(parseUser);
    }
  }

  private void showEmailAlreadyRegisteredAlert(String email) {
    AlertDialog.Builder alert = new AlertDialog.Builder(this);
    alert.setMessage(email + " " + getString(R.string.email_already_registered));
    alert.setPositiveButton(R.string.dialog_btn_ok, new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int which) {
        dialog.dismiss();
      }
    });
    alert.create().show();
  }

  /*
  Method to actually update values on Parse db
   */
  private void saveUser(final ParseUser parseUser) {
    String firstName = etFirstName.getText().toString().trim();
    String lastName = etLastName.getText().toString().trim();
    String countryCode = "";
    if(spCountryCode.getSelectedItemPosition() > 0) {
      countryCode = ((CountryInfo) spCountryCode.getSelectedItem()).dialingCode;
    }
    final String mobileNumber = etMobileNumber.getText().toString().trim();
    String emergencyCountryCode = "";
    if(spEmergencyCountryCode.getSelectedItemPosition() > 0) {
      emergencyCountryCode = ((CountryInfo) spEmergencyCountryCode.getSelectedItem()).dialingCode;
    }
    String emergencyContactName = etEmergencyContactName.getText().toString().trim();
    String emergencyContactPhone = etEmergencyContactPhone.getText().toString().trim();
    String bType = "";
    if(bloodType != null) {
      bType = convertEnumToString(bloodType);
    }
    String allergies = etAllergies.getText().toString().trim();
    String otherMedicalConditions = etOtherMedicalConditions.getText().toString().trim();
    if(!mobileNumber.isEmpty()) {
      parseUser.put("mobileNumber", countryCode + mobileNumber.trim());
    } else {
      parseUser.put("mobileNumber", mobileNumber.trim());
    }
    parseUser.put("firstName", firstName.trim());
    parseUser.put("lastName", lastName.trim());
    parseUser.put("emergencyContactName", emergencyContactName.trim());
    if(!emergencyContactPhone.isEmpty()) {
      parseUser.put("emergencyContactNumber", emergencyCountryCode + emergencyContactPhone.trim());
    } else {
      parseUser.put("emergencyContactNumber", emergencyContactPhone.trim());
    }
    parseUser.put("bloodType", bType);
    parseUser.put("allergies", allergies.trim());
    parseUser.put("otherMedicalConditions", otherMedicalConditions.trim());
    parseUser.saveInBackground(new SaveCallback() {
      @Override
      public void done(ParseException e) {
        spinner.setVisibility(View.GONE);
        if(weakRef.get() != null && !weakRef.get().isFinishing()) {
          if(e == null) {
            // Make an API call to LMA server for the updated user information
            Singleton.sendToast(R.string.account_updated_successfully);
            finish();
          } else {
            Singleton.sendToast(e);
          }
        }
      }
    });
  }

  private void selectBloodType(BloodType bType) {
    txtLblAMinus.setBackgroundResource(R.drawable.bg_blood_group_gray_border);
    txtLblAMinus.setTextColor(Color.parseColor("#999999"));
    txtLblAPlus.setBackgroundResource(R.drawable.bg_blood_group_gray_border);
    txtLblAPlus.setTextColor(Color.parseColor("#999999"));
    txtLblBMinus.setBackgroundResource(R.drawable.bg_blood_group_gray_border);
    txtLblBMinus.setTextColor(Color.parseColor("#999999"));
    txtLblBPlus.setBackgroundResource(R.drawable.bg_blood_group_gray_border);
    txtLblBPlus.setTextColor(Color.parseColor("#999999"));
    txtLblABMinus.setBackgroundResource(R.drawable.bg_blood_group_gray_border);
    txtLblABMinus.setTextColor(Color.parseColor("#999999"));
    txtLblABPlus.setBackgroundResource(R.drawable.bg_blood_group_gray_border);
    txtLblABPlus.setTextColor(Color.parseColor("#999999"));
    txtLblOMinus.setBackgroundResource(R.drawable.bg_blood_group_gray_border);
    txtLblOMinus.setTextColor(Color.parseColor("#999999"));
    txtLblOPlus.setBackgroundResource(R.drawable.bg_blood_group_gray_border);
    txtLblOPlus.setTextColor(Color.parseColor("#999999"));
    switch(bType) {
      case A_MINUS:
        if(bloodType != null && bloodType == BloodType.A_MINUS) {
          bloodType = null;
          return;
        }
        txtLblAMinus.setBackgroundResource(R.drawable.bg_blood_group_highlight);
        txtLblAMinus.setTextColor(Color.WHITE);
        bloodType = BloodType.A_MINUS;
        break;
      case A_PLUS:
        if(bloodType != null && bloodType == BloodType.A_PLUS) {
          bloodType = null;
          return;
        }
        txtLblAPlus.setBackgroundResource(R.drawable.bg_blood_group_highlight);
        txtLblAPlus.setTextColor(Color.WHITE);
        bloodType = BloodType.A_PLUS;
        break;
      case B_MINUS:
        if(bloodType != null && bloodType == BloodType.B_MINUS) {
          bloodType = null;
          return;
        }
        txtLblBMinus.setBackgroundResource(R.drawable.bg_blood_group_highlight);
        txtLblBMinus.setTextColor(Color.WHITE);
        bloodType = BloodType.B_MINUS;
        break;
      case B_PLUS:
        if(bloodType != null && bloodType == BloodType.B_PLUS) {
          bloodType = null;
          return;
        }
        txtLblBPlus.setBackgroundResource(R.drawable.bg_blood_group_highlight);
        txtLblBPlus.setTextColor(Color.WHITE);
        bloodType = BloodType.B_PLUS;
        break;
      case AB_MINUS:
        if(bloodType != null && bloodType == BloodType.AB_MINUS) {
          bloodType = null;
          return;
        }
        txtLblABMinus.setBackgroundResource(R.drawable.bg_blood_group_highlight);
        txtLblABMinus.setTextColor(Color.WHITE);
        bloodType = BloodType.AB_MINUS;
        break;
      case AB_PLUS:
        if(bloodType != null && bloodType == BloodType.AB_PLUS) {
          bloodType = null;
          return;
        }
        txtLblABPlus.setBackgroundResource(R.drawable.bg_blood_group_highlight);
        txtLblABPlus.setTextColor(Color.WHITE);
        bloodType = BloodType.AB_PLUS;
        break;
      case O_MINUS:
        if(bloodType != null && bloodType == BloodType.O_MINUS) {
          bloodType = null;
          return;
        }
        txtLblOMinus.setBackgroundResource(R.drawable.bg_blood_group_highlight);
        txtLblOMinus.setTextColor(Color.WHITE);
        bloodType = BloodType.O_MINUS;
        break;
      case O_PLUS:
        if(bloodType != null && bloodType == BloodType.O_PLUS) {
          bloodType = null;
          return;
        }
        txtLblOPlus.setBackgroundResource(R.drawable.bg_blood_group_highlight);
        txtLblOPlus.setTextColor(Color.WHITE);
        bloodType = BloodType.O_PLUS;
        break;
    }
  }

  private BloodType convertStringToEnum(String bloodType) {
    if(bloodType.equals("A-")) {
      return BloodType.A_MINUS;
    } else if(bloodType.equals("A+")) {
      return BloodType.A_PLUS;
    } else if(bloodType.equals("B-")) {
      return BloodType.B_MINUS;
    } else if(bloodType.equals("B+")) {
      return BloodType.B_PLUS;
    } else if(bloodType.equals("AB-")) {
      return BloodType.AB_MINUS;
    } else if(bloodType.equals("AB+")) {
      return BloodType.AB_PLUS;
    } else if(bloodType.equals("O-")) {
      return BloodType.O_MINUS;
    } else {
      return BloodType.O_PLUS;
    }
  }

  private String convertEnumToString(BloodType bloodType) {
    switch(bloodType) {
      case A_MINUS:
        return "A-";
      case A_PLUS:
        return "A+";
      case B_MINUS:
        return "B-";
      case B_PLUS:
        return "B+";
      case AB_MINUS:
        return "AB-";
      case AB_PLUS:
        return "AB+";
      case O_MINUS:
        return "O-";
      default:
        return "O+";
    }
  }

  private static enum BloodType {
    A_MINUS, A_PLUS, B_MINUS, B_PLUS, AB_MINUS, AB_PLUS, O_MINUS, O_PLUS
  }

  private static class ItemViewHolder {
    TextView txtCountryName;
    TextView txtCountryCode;
    ImageView imgFlag;
    ImageView imgTick;
  }

  private class CountryListAdapter extends ArrayAdapter<CountryInfo> {
    private final ArrayList<CountryInfo> list;
    private int resourceId;
    private LayoutInflater inflator;

    public CountryListAdapter(Context context, int resourceId, ArrayList<CountryInfo> list) {
      super(context, resourceId, list);
      this.list = list;
      this.resourceId = resourceId;
      inflator = getLayoutInflater();
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
      ItemViewHolder holder = null;
      CountryInfo item = list.get(position);
      if(convertView == null) {
        holder = new ItemViewHolder();
        convertView = inflator.inflate(resourceId, null);
        holder.txtCountryName = (TextView) convertView.findViewById(R.id.txt_country_name);
        holder.imgFlag = (ImageView) convertView.findViewById(R.id.img_flag);
        holder.imgTick = (ImageView) convertView.findViewById(R.id.img_tick);
        convertView.setTag(holder);
      }
      holder = (ItemViewHolder) convertView.getTag();
      if(item.shortCode == null) {
        holder.txtCountryName.setText(item.name);
        holder.imgFlag.setImageBitmap(null);
        holder.imgTick.setVisibility(View.GONE);
      } else {
        holder.txtCountryName.setText(item.name + " +" + item.dialingCode);
        holder.imgFlag.setImageResource(item.flagId);
        if(item.selected) {
          holder.imgTick.setVisibility(View.VISIBLE);
        } else {
          holder.imgTick.setVisibility(View.GONE);
        }
      }
      return convertView;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
      ItemViewHolder holder = null;
      CountryInfo item = list.get(position);
      if(convertView == null) {
        holder = new ItemViewHolder();
        convertView = inflator.inflate(R.layout.cell_country_code, null);
        holder.txtCountryCode = (TextView) convertView.findViewById(R.id.txt_country_code);
        convertView.setTag(holder);
      }
      holder = (ItemViewHolder) convertView.getTag();
      if(item.shortCode == null) {
        holder.txtCountryCode.setText(item.name);
        holder.txtCountryCode.setTextColor(getResources().getColor(R.color.highlight_color_light));
      } else {
        holder.txtCountryCode.setText("+" + item.dialingCode);
        //holder.txtCountryCode.setTextColor(getResources().getColor(R.color.highlight_color));
      }
      return convertView;
    }
  }
}
