package cell411.activity;

import android.app.AlertDialog;
import android.app.Service;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.parse.ParseCheater;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParseUser;
import com.parse.callback.LogInCallback;
import com.parse.callback.RequestPasswordResetCallback;
import com.parse.callback.SaveCallback;
import com.safearx.cell411.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.ref.WeakReference;

import javax.net.ssl.HttpsURLConnection;

import cell411.Singleton;
import cell411.constants.LMA;
import cell411.constants.ParseKeys;
import cell411.constants.Prefs;
import cell411.utils.LogEvent;
import cell411.widgets.Spinner;

/**
 * Created by Sachin on 14-04-2016.
 */
public class LoginActivity extends AppCompatActivity implements View.OnClickListener {
  private static final String TAG = "LoginActivity";
  private static WeakReference<LoginActivity> weakRef;
  private EditText etEmail;
  private EditText etPassword;
  private Spinner spinner;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_login);
    weakRef = new WeakReference<>(this);
    spinner = (Spinner) findViewById(R.id.spinner);
    etEmail = (EditText) findViewById(R.id.et_email);
    etPassword = (EditText) findViewById(R.id.et_password);
    TextView txtBtnLogin = (TextView) findViewById(R.id.txt_btn_login);
    TextView txtBtnForgotPassword = (TextView) findViewById(R.id.txt_btn_forgot_password);
    LinearLayout llBtnLoginWithFb = (LinearLayout) findViewById(R.id.ll_btn_login_with_fb);
    TextView txtBtnSignUp = (TextView) findViewById(R.id.txt_btn_sign_up);
    txtBtnLogin.setOnClickListener(this);
    txtBtnForgotPassword.setOnClickListener(this);
    llBtnLoginWithFb.setOnClickListener(this);
    txtBtnSignUp.setOnClickListener(this);
    findViewById(R.id.rl_separator).setVisibility(View.GONE);
    findViewById(R.id.ll_btn_login_with_fb).setVisibility(View.GONE);
  }

  @Override
  public void onClick(View v) {
    int id = v.getId();
    switch(id) {
      case R.id.txt_btn_login:
        LogEvent.Log(TAG,"attempting to log in");
        login();
        break;
      case R.id.txt_btn_forgot_password:
        showForgotPasswordDialog();
        break;
      case R.id.ll_btn_login_with_fb:
        break;
      case R.id.txt_btn_sign_up:
        Intent intentRegister = new Intent(LoginActivity.this, RegisterActivity.class);
        startActivity(intentRegister);
        finish();
        break;
    }
  }

  private void login() {
    if(spinner.getVisibility() == View.VISIBLE) {
      return;
    }
    String email = etEmail.getText().toString();
    LogEvent.Log(TAG,"email='"+email+"'");
    String password = etPassword.getText().toString();
    LogEvent.Log(TAG,"password='"+password+"'");
    if(email.isEmpty()) {
      getApplicationContext();
      Singleton.sendToast(R.string.validation_email);
    } else if(password.isEmpty()) {
      getApplicationContext();
      Singleton.sendToast(R.string.validation_password);
    } else {
      spinner.setVisibility(View.VISIBLE);
      ParseUser.logInInBackground(email.toLowerCase().trim(), password.trim(), new LogInCallback() {
        public void done(ParseUser user, ParseException e) {
          if(e != null) {
            LogEvent.Log(TAG,"failed to log in: "+e);
            e.printStackTrace();
            spinner.setVisibility(View.GONE);
            // Signup failed. Look at the ParseException to see what happened.
            Singleton.sendToast(e);
            return;
          }
          LogEvent.Log(TAG,"No Exception");
          String privilege = (String) user.get(ParseKeys.PRIVILEGE);
          if(privilege == null || privilege.equals("")) {
            login(user);
          } else if(privilege.equals(ParseKeys.PRIVILEGE_BANNED)) {
            // user is permanently banned
            spinner.setVisibility(View.GONE);
            showUnfulfilledPrivilegeAlert(getString(R.string.login_blocked_notice));
          } else if(privilege.contains(ParseKeys.PRIVILEGE_SUSPENDED)) {
            // blocked user temporarily
            spinner.setVisibility(View.GONE);
            showUnfulfilledPrivilegeAlert(getString(R.string.login_suspention_notice));
          }  else {
            login(user);
          }
        }
      });
    }
  }

  private void login(final ParseUser user) {
    ParseInstallation installation = ParseInstallation.getCurrentInstallation();
    installation.put(ParseKeys.USER, user);
    installation.saveInBackground(new SaveCallback() {
      @Override
      public void done(ParseException e) {
        ParseCheater.setup();
        spinner.setVisibility(View.GONE);
        if(e == null) {
          Singleton.setLoggedIn(true);

          Intent intentMain = new Intent(LoginActivity.this, PermissionActivity.class);
          startActivity(intentMain);
          finish();
        } else {
          Singleton.sendToast(e);
        }
      }
    });
  }

  public void showUnfulfilledPrivilegeAlert(String message) {
    AlertDialog.Builder alert = new AlertDialog.Builder(this);
    alert.setMessage(message);
    alert.setPositiveButton(R.string.dialog_btn_ok, new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int which) {
        dialog.dismiss();
      }
    });
    alert.create().show();
  }

  private void showForgotPasswordDialog() {
    android.app.AlertDialog.Builder alert = new android.app.AlertDialog.Builder(this);
    alert.setTitle(R.string.dialog_title_forgot_password);
    alert.setMessage(R.string.dialog_message_tap_submit);
    alert.setCancelable(false);
    LayoutInflater inflater = (LayoutInflater) getSystemService(Service.LAYOUT_INFLATER_SERVICE);
    View view = inflater.inflate(R.layout.dialog_forgot_password, null);
    final ProgressBar pb = (ProgressBar) view.findViewById(R.id.pb);
    final EditText etEmail = (EditText) view.findViewById(R.id.et_email);
    alert.setView(view);
    alert.setNegativeButton(R.string.dialog_btn_cancel, new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int arg1) {
        dialog.dismiss();
      }
    });
    alert.setPositiveButton(R.string.dialog_btn_submit, new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int which) {
      }
    });
    final android.app.AlertDialog dialog = alert.create();
    dialog.show();
    dialog.getButton(android.app.AlertDialog.BUTTON_POSITIVE).setOnClickListener(
      new View.OnClickListener() {
        @Override
        public void onClick(View v) {
          if(pb.getVisibility() != View.VISIBLE) {
            final String email = etEmail.getText().toString().trim();
            if(email.isEmpty()) {
              Singleton.sendToast(R.string.validation_email);
              return;
            }
            pb.setVisibility(View.VISIBLE);
            ParseUser.requestPasswordResetInBackground(email.toLowerCase(),
              new RequestPasswordResetCallback() {
                public void done(ParseException e) {
                  pb.setVisibility(View.GONE);
                  if(e == null) {
                    showAlertDialog();
                  } else {
                    Singleton.sendToast(e);
                  }
                  dialog.dismiss();
                }
              });
          }
        }
      });
  }

  private void showAlertDialog() {
    android.app.AlertDialog.Builder alert = new android.app.AlertDialog.Builder(this);
    alert.setMessage(R.string.dialog_message_reset_instructions_sent);
    alert.setPositiveButton(R.string.dialog_btn_ok, new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int which) {
      }
    });
    final android.app.AlertDialog dialog = alert.create();
    dialog.show();
  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);
  }

}
