package cell411.activity;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.appcompat.app.ActionBar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.callback.FindCallback;
import com.safearx.cell411.R;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import cell411.Singleton;
import cell411.models.Footer;
import cell411.utils.LogEvent;
import cell411.widgets.Spinner;

import static cell411.SingletonConfig.DEFAULT_NOTIFICATIONS_COUNT;

/**
 * Created by Sachin on 17-04-2017.
 */
public class CustomNotificationActivity extends BaseActivity {
  public static WeakReference<CustomNotificationActivity> weakRef;
  private final String TAG = "CustomNotificationActivity";
  private RecyclerView recyclerView;
  private CustomNotificationListAdapter customNotificationListAdapter;
  private ArrayList<Object> alertList;

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    switch(item.getItemId()) {
      case android.R.id.home:
        finish();
        return true;
      default:
        return super.onOptionsItemSelected(item);
    }
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_custom_notifications);
    // Set up the action bar.
    final ActionBar actionBar = getSupportActionBar();
    actionBar.setDisplayHomeAsUpEnabled(true);
    weakRef = new WeakReference<>(this);
    Singleton.INSTANCE.setCurrentActivity(this);
    final Spinner spinner = (Spinner) findViewById(R.id.spinner);
    recyclerView = (RecyclerView) findViewById(R.id.rv_notifications);
    // use this setting to improve performance if you know that changes
    // in content do not change the layout size of the RecyclerView
    recyclerView.setHasFixedSize(true);
    // use a linear layout manager
    LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
    recyclerView.setLayoutManager(linearLayoutManager);
    ParseQuery<ParseObject> parseQuery4CustomAlerts = ParseQuery.getQuery("Cell411Alert");
    parseQuery4CustomAlerts.whereEqualTo("alertType", "Custom");
    parseQuery4CustomAlerts.orderByDescending("createdAt");
    parseQuery4CustomAlerts.setLimit(DEFAULT_NOTIFICATIONS_COUNT);
    parseQuery4CustomAlerts.findInBackground(new FindCallback<ParseObject>() {
      public void done(List<ParseObject> results, ParseException e) {
        spinner.setVisibility(View.GONE);
        if(e == null) {
          if(results != null && results.size() > 0) {
            alertList = new ArrayList<>();
            GregorianCalendar cal = new GregorianCalendar();
            GregorianCalendar calCurrentTime = new GregorianCalendar();
            calCurrentTime.setTimeInMillis(System.currentTimeMillis());
            calCurrentTime.set(Calendar.HOUR, 0);
            calCurrentTime.set(Calendar.MINUTE, 0);
            calCurrentTime.set(Calendar.SECOND, 0);
            calCurrentTime.set(Calendar.MILLISECOND, 0);
            LogEvent.Log(TAG, "results.size(): " + results.size());
            for(int i = 0; i < results.size(); i++) {
              ParseObject cell411Obj = results.get(i);
              long millis = cell411Obj.getCreatedAt().getTime();
              cal.setTimeInMillis(millis);
              int year = cal.get(Calendar.YEAR);
              int month = cal.get(Calendar.MONTH) + 1;
              int day = cal.get(Calendar.DAY_OF_MONTH);
              int hour = cal.get(Calendar.HOUR);
              int minute = cal.get(Calendar.MINUTE);
              int ampm = cal.get(Calendar.AM_PM);
              String ap;
              if(ampm == 0) {
                ap = getString(R.string.am);
              } else {
                ap = getString(R.string.pm);
              }
              if(hour == 0) {
                hour = 12;
              }
              String hourMinute = "" + hour;
              if(minute > 0 && minute < 10) {
                hourMinute += ":0" + minute;
              } else if(minute > 9) {
                hourMinute += ":" + minute;
              }
              String time;
              if(cal.getTimeInMillis() < calCurrentTime.getTimeInMillis()) {
                // Check if the alert was issued yesterday
                time = month + "/" + day + "/" + year + " " + getString(R.string.at) + " " +
                         hourMinute + " " + ap;
              } else {
                time = getString(R.string.today_at) + " " + hourMinute + " " + ap;
              }
              String text = (String) cell411Obj.get("additionalNote");
              alertList.add(new CustomCell411Notification(text, time));
            }
            if(CustomNotificationActivity.weakRef.get() != null &&
                 !CustomNotificationActivity.weakRef.get().isFinishing()) {
              customNotificationListAdapter = new CustomNotificationListAdapter(alertList);
              recyclerView.setAdapter(customNotificationListAdapter);
            }
          } else {
            LogEvent.Log(TAG, "results == null || results.size() == 0");
          }
        } else {
          if(MainActivity.weakRef.get() != null && !MainActivity.weakRef.get().isFinishing()) {
            MainActivity.INSTANCE.getApplicationContext();
            Singleton.sendToast(e);
          }
        }
      }
    });
  }

  private class CustomCell411Notification {
    public String text;
    public String time;

    public CustomCell411Notification(String text, String time) {
      this.text = text;
      this.time = time;
    }
  }

  public class CustomNotificationListAdapter
    extends RecyclerView.Adapter<CustomNotificationListAdapter.ViewHolder> {
    private final int VIEW_TYPE_NOTIFICATION = 0;
    private final int VIEW_TYPE_FOOTER = 1;
    public ArrayList<Object> arrayList;
    private final View.OnClickListener mOnClickListener = new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        CustomCell411Notification customCell411Notification =
          ((CustomCell411Notification) customNotificationListAdapter.arrayList.get(
            recyclerView.getChildAdapterPosition(v)));
        //Intent intent = new Intent(MainActivity.INSTANCE, AlertDetailActivity2.class);
        //startActivity(intent);
      }
    };

    // Provide a suitable constructor (depends on the kind of dataset)
    public CustomNotificationListAdapter(ArrayList<Object> arrayList) {
      this.arrayList = arrayList;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public CustomNotificationListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                                       int viewType) {
      // create a new view
      View v = null;
      if(viewType == VIEW_TYPE_NOTIFICATION) {
        v = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_custom_notification,
          parent, false);
      } else if(viewType == VIEW_TYPE_FOOTER) {
        v = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_footer, parent, false);
      }
      // set the view's size, margins, padding(s) and layout parameters
      ViewHolder vh = new ViewHolder(v, viewType);
      v.setOnClickListener(mOnClickListener);
      return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int position) {
      // - get element from your data set at this position
      // - replace the contents of the view with that element
      if(getItemViewType(position) == VIEW_TYPE_NOTIFICATION) {
        final CustomCell411Notification customCell411Notification =
          (CustomCell411Notification) arrayList.get(position);
        viewHolder.txt.setText(customCell411Notification.text);
        viewHolder.txtTime.setText(customCell411Notification.time);
      } else {
        final Footer footer = (Footer) arrayList.get(position);
        viewHolder.txtInfo.setText(footer.info);
        if(footer.spinnerVisibility) {
          viewHolder.spinner.setVisibility(View.VISIBLE);
        } else {
          viewHolder.spinner.setVisibility(View.GONE);
        }
      }
    }

    // Return the size of your data set (invoked by the layout manager)
    @Override
    public int getItemCount() {
      return arrayList.size();
    }

    @Override
    public int getItemViewType(int position) {
      super.getItemViewType(position);
      if(arrayList.get(position) instanceof CustomCell411Notification) {
        return VIEW_TYPE_NOTIFICATION;
      } else {
        return VIEW_TYPE_FOOTER;
      }
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public class ViewHolder extends RecyclerView.ViewHolder {
      // each data item is just a string in this case
      private TextView txt;
      private TextView txtTime;
      private TextView txtInfo;
      private Spinner spinner;

      public ViewHolder(View view, int type) {
        super(view);
        if(type == VIEW_TYPE_NOTIFICATION) {
          txt = (TextView) view.findViewById(R.id.txt);
          txtTime = (TextView) view.findViewById(R.id.txt_time);
        } else if(type == VIEW_TYPE_FOOTER) {
          txtInfo = (TextView) view.findViewById(R.id.txt_info);
          spinner = (Spinner) view.findViewById(R.id.spinner);
        }
      }
    }
  }
}