package cell411.activity;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.text.SpannableString;
import android.text.style.TextAppearanceSpan;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.FileProvider;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.FragmentManager;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.callback.FindCallback;
import com.parse.callback.FunctionCallback;
import com.parse.callback.GetCallback;
import com.parse.callback.SaveCallback;
import com.safearx.cell411.R;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import cell411.Singleton;
import cell411.analytics.Cell411Analytics;
import cell411.constants.Contents;
import cell411.constants.ParseKeys;
import cell411.constants.Prefs;
import cell411.fragment_tabs.TabAlertsFragment;
import cell411.fragment_tabs.TabCellsFragment;
import cell411.fragment_tabs.TabChatsFragment;
import cell411.fragment_tabs.TabFriendsFragment;
import cell411.fragment_tabs.TabMapFragment;
import cell411.fragments.FriendsFragment;
import cell411.methods.AddFriendModules;
import cell411.methods.Modules;
import cell411.models.Chat;
import cell411.services.LocationJobService;
import cell411.utils.LogEvent;
import cell411.utils.QRCodeEncoder;
import cell411.utils.StorageOperations;
import cell411.widgets.Spinner;

import static com.parse.ParseUser.getCurrentUser;

public class MainActivity extends BaseActivity
  implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener {
  public static final int NUM_HASHED_BYTES = 9;
  public static final int NUM_BASE64_CHAR = 11;
  public static final String BROADCAST_ACTION_MESSAGE = "com.wiselysoft.connected.MessageReceiver";
  private static final String TAG = "MainActivity";
  private static final String COLOR_DIRTY_WHITE = "#f1f1f1";
  private static final String HASH_TYPE = "SHA-256";
  public static WeakReference<MainActivity> weakRef;
  public static MainActivity INSTANCE;
  private static RelativeLayout rlSpinner;
  private final int TAB_MARGIN_TOP = (int) (20 * Singleton.getDensity() + 0.5f);
  private final int TAB_HEIGHT_INACTIVE = (int) (30 * Singleton.getDensity() + 0.5f);
  private final int TAB_HEIGHT_ACTIVE = (int) (50 * Singleton.getDensity() + 0.5f);
  private final FirebaseAuth mAuth = FirebaseAuth.getInstance();
  private TabMapFragment tabMapFragment;
  private TabFriendsFragment tabFriendsFragment;
  private TabCellsFragment tabCellsFragment;
  private TabAlertsFragment tabAlertsFragment;
  private TabChatsFragment tabChatsFragment;
  private FrameLayout flTabMap;
  private FrameLayout flTabFriends;
  private FrameLayout flTabCells;
  private FrameLayout flTabAlerts;
  private FrameLayout flTabChats;
  private RelativeLayout rlTabFriends;
  private RelativeLayout rlTabCells;
  private RelativeLayout rlTabAlerts;
  private RelativeLayout rlTabChats;
  private ImageView imgTabMap;
  private ImageView imgTabFriends;
  private ImageView imgTabCells;
  private ImageView imgTabAlerts;
  private ImageView imgTabChats;
  private TextView txtTabMap;
  private TextView txtTabFriends;
  private TextView txtTabCells;
  private TextView txtTabAlerts;
  private TextView txtTabChats;
  private FragmentManager fragmentManager;
  private TABS selectedTab;
  private MenuItem miStandard, miSatellite, miHybrid, miMapType, miContacts, miRefresh,
    miSearchCells, miNotification;
  private String version = "";
  private boolean isBroadcastEnabled;
  private ImageView imgUser;
  private TextView txtName;
  private TextView txtEmail;
  private TextView txtBloodGroup;
  private RelativeLayout.LayoutParams paramsTabMap;
  private RelativeLayout.LayoutParams paramsTabFriends;
  private RelativeLayout.LayoutParams paramsTabCells;
  private RelativeLayout.LayoutParams paramsTabAlerts;
  private RelativeLayout.LayoutParams paramsTabChats;
  private long previousBackTapMillis = 0;
  private long waitToExitForMillis = 5000; // 1000 * 10 (10 seconds)

  private static boolean isSpinnerVisible() {
    return rlSpinner.getVisibility() == View.VISIBLE;
  }

  public static void setSpinnerVisible(boolean visible) {
    if(rlSpinner == null) {
      return;
    }
    if(visible) {
      rlSpinner.setVisibility(View.VISIBLE);
    } else {
      rlSpinner.setVisibility(View.GONE);
    }
  }

  @TargetApi(Build.VERSION_CODES.LOLLIPOP)
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    weakRef = new WeakReference<>(this);
    INSTANCE = this;
    if(getCurrentUser() == null) { // To prevent crash from another crash
      finish();
      return;
    }
    // check if this is the new user who chosen Facebook to login
    boolean newFBUser = getIntent().getBooleanExtra("newFBUser", false);
    Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
    setSupportActionBar(toolbar);
    DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
    ActionBarDrawerToggle toggle =
      new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open,
        R.string.navigation_drawer_close);
    drawer.setDrawerListener(toggle);
    toggle.syncState();
    boolean gotoChatTab = getIntent().getBooleanExtra("gotoChatTab", false);
    boolean gotoChatScreen = getIntent().getBooleanExtra("gotoChatScreen", false);
    String entityType = getIntent().getStringExtra("entityType");
    String publicCellObjectId = getIntent().getStringExtra("entityObjectId");
    String publicCellName = getIntent().getStringExtra("entityName");
    String entityCreatedAt = getIntent().getStringExtra("time");
    rlSpinner = (RelativeLayout) findViewById(R.id.rl_spinner);
    NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
    navigationView.setNavigationItemSelectedListener(this);
    // FIXME:  Do we want this on or off?
    isBroadcastEnabled = false;
    boolean isKnowYourRightsEnabled = getResources().getBoolean(R.bool.is_know_your_rights_enabled);
    Menu navMenu = navigationView.getMenu();
    navMenu.findItem(R.id.nav_share_this_app).setActionView(R.layout.layout_outside_link);
    navMenu.findItem(R.id.nav_rate_this_app).setActionView(R.layout.layout_outside_link);
    navMenu.findItem(R.id.nav_faq_and_tutorials).setActionView(R.layout.layout_outside_link);

    navMenu.findItem(R.id.nav_notifications).setVisible(isBroadcastEnabled);
    navMenu.findItem(R.id.nav_know_your_rights).setVisible(isKnowYourRightsEnabled);
    if(isBroadcastEnabled && getCurrentUser().getInt("roleId") == 1) {
      navMenu.findItem(R.id.nav_broadcast_message).setVisible(true);
      LogEvent.Log(TAG, "role is not null");
    } else {
      navMenu.findItem(R.id.nav_broadcast_message).setVisible(false);
      LogEvent.Log(TAG, "role is null: " + getCurrentUser().getInt("role"));
    }
    MenuItem miLogout = navMenu.findItem(R.id.nav_logout);
    SpannableString s = new SpannableString(miLogout.getTitle());
    s.setSpan(new TextAppearanceSpan(this, R.style.TextAppearanceLogout), 0, s.length(), 0);
    miLogout.setTitle(s);
    Drawable mDrawableLogout = getResources().getDrawable(R.drawable.nav_logout);
    mDrawableLogout.setColorFilter(
      new PorterDuffColorFilter(getResources().getColor(R.color.highlight_color_dark),
        PorterDuff.Mode.MULTIPLY));
    miLogout.setIcon(mDrawableLogout);
    setupTabBar();
    fragmentManager = getSupportFragmentManager();
    View headerLayout = navigationView.inflateHeaderView(R.layout.nav_header_main);
    imgUser = (ImageView) headerLayout.findViewById(R.id.img_user);
    txtName = (TextView) headerLayout.findViewById(R.id.txt_name);
    txtEmail = (TextView) headerLayout.findViewById(R.id.txt_email);
    txtBloodGroup = (TextView) headerLayout.findViewById(R.id.txt_blood_group);
    TextView txtVersion = findViewById(R.id.txt_version);
    try {
      PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
      version = pInfo.versionName;
    } catch(PackageManager.NameNotFoundException e) {
      e.printStackTrace();
    }
    txtVersion.setText("v" + version);
    Singleton.INSTANCE.setCurrentActivity(this);
    Modules.check4NewAlertsAndPrivilege(this);
    if(gotoChatTab) {
      setSelectedTab(TABS.CHATS, 0);
    } else {
      setSelectedTab(TABS.MAP, 0);
      if(gotoChatScreen) {
        Intent intent = new Intent(this, ChatActivity.class);
        intent.putExtra("entityType", entityType);
        intent.putExtra("entityObjectId", publicCellObjectId);
        intent.putExtra("entityName", publicCellName);
        intent.putExtra("time", entityCreatedAt);
        startActivity(intent);
      }
    }
    // Register the receiver for listening to new messages
    IntentFilter mMessageReceiverIntentFilter = new IntentFilter(BROADCAST_ACTION_MESSAGE);
    ChatReceiver chatReceiver = new ChatReceiver();
    LocalBroadcastManager.getInstance(this).registerReceiver(chatReceiver,
      mMessageReceiverIntentFilter);
    IntentFilter mLocationReceiverIntentFilter =
      new IntentFilter(LocationJobService.BROADCAST_ACTION_LOCATION_RECEIVED);
    LocationReceiver locationReceiver = new LocationReceiver();
    LocalBroadcastManager.getInstance(this).registerReceiver(locationReceiver,
      mLocationReceiverIntentFilter);
    checkPrivacyPolicy();
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    getMenuInflater().inflate(R.menu.menu_main, menu);
    miStandard = menu.findItem(R.id.action_standard_view);
    miSatellite = menu.findItem(R.id.action_satellite_view);
    miHybrid = menu.findItem(R.id.action_hybrid_view);
    miContacts = menu.findItem(R.id.action_invite_contacts);
    miRefresh = menu.findItem(R.id.action_refresh);
    miSearchCells = menu.findItem(R.id.action_search_cell);
    miNotification = menu.findItem(R.id.action_notification);
    miMapType = menu.findItem(R.id.action_map_type);
    return super.onCreateOptionsMenu(menu);
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    switch(item.getItemId()) {
      case android.R.id.home:
        finish();
        return true;
      case R.id.action_standard_view:
        tabMapFragment.setMapType(TabMapFragment.ViewType.STANDARD);
        miStandard.setIcon(R.drawable.standard_view_selected);
        miSatellite.setIcon(R.drawable.satellite_view_normal);
        miHybrid.setIcon(R.drawable.hybrid_view_normal);
        return true;
      case R.id.action_satellite_view:
        tabMapFragment.setMapType(TabMapFragment.ViewType.SATELLITE);
        miStandard.setIcon(R.drawable.standard_view_normal);
        miSatellite.setIcon(R.drawable.satellite_view_selected);
        miHybrid.setIcon(R.drawable.hybrid_view_normal);
        return true;
      case R.id.action_hybrid_view:
        tabMapFragment.setMapType(TabMapFragment.ViewType.HYBRID);
        miStandard.setIcon(R.drawable.standard_view_normal);
        miSatellite.setIcon(R.drawable.satellite_view_normal);
        miHybrid.setIcon(R.drawable.hybrid_view_selected);
        return true;
      case R.id.action_refresh:
        return true;
      case R.id.action_search_cell:
        Intent intentSearchCell = new Intent(this, ExploreCellActivity.class);
        startActivity(intentSearchCell);
        return true;
      case R.id.action_notification:
        Intent intentCustomNotification = new Intent(this, CustomNotificationActivity.class);
        startActivity(intentCustomNotification);
        return true;
      default:
        return super.onOptionsItemSelected(item);
    }
  }

  @Override
  public boolean onPrepareOptionsMenu(Menu menu) {
    boolean visibilityMap = selectedTab == TABS.MAP;
    miMapType.setVisible(visibilityMap);
    miContacts.setVisible(false); // to hide it all the time
    miNotification.setVisible(isBroadcastEnabled);
    miRefresh.setVisible(false);
    boolean visibilitySearchCells = selectedTab == TABS.CELLS;
    miSearchCells.setVisible(visibilitySearchCells);
    return super.onPrepareOptionsMenu(menu);
  }

  @Override
  protected void onStart() {
    super.onStart();
    if(getResources().getBoolean(R.bool.is_chat_enabled)) {
      FirebaseUser currentUser = mAuth.getCurrentUser();
      if(currentUser == null) {
        LogEvent.Log(TAG, "currentUser is null");
        signInFirebase();
      } else {
        LogEvent.Log(TAG, "currentUser is not null");
      }
    }
    ParseUser user = getCurrentUser();
    String mobileNumber = user.getString("mobileNumber");
    if(mobileNumber == null || mobileNumber.trim().equals("")) {
      startActivity(new Intent(getApplicationContext(), EnterPhoneActivity.class));
    }
  }

  @Override
  protected void onResume() {
    super.onResume();
    if(getCurrentUser() == null) {
      return;
    }
    updateUserInfo();
    if(Singleton.INSTANCE.isDarkModeChanged()) {
      Singleton.INSTANCE.setIsDarkModeChanged();
      recreate();
    }
  }

  private void checkPrivacyPolicy() {
    final ParseQuery<ParseObject> queryPrivacyPolicy = ParseQuery.getQuery("PrivacyPolicy");
    queryPrivacyPolicy.addDescendingOrder("createdAt");
    queryPrivacyPolicy.getFirstInBackground(new GetCallback<ParseObject>() {
      @Override
      public void done(final ParseObject objectPrivacyPolicy, ParseException e) {
        if(e == null) {
          LogEvent.Log(TAG,
            "objectPrivacyPolicy.getObjectId(): " + objectPrivacyPolicy.getObjectId());
          ParseQuery<ParseObject> queryUserConsent = ParseQuery.getQuery("UserConsent");
          queryUserConsent.whereEqualTo("privacyPolicyId", objectPrivacyPolicy.getObjectId());
          queryUserConsent.whereEqualTo("userId", getCurrentUser().getObjectId());
          queryUserConsent.getFirstInBackground(new GetCallback<ParseObject>() {
            @Override
            public void done(ParseObject objectUserConsent, ParseException e) {
              if(e == null) {
                LogEvent.Log(TAG,
                  "objectUserConsent.getObjectId(): " + objectUserConsent.getObjectId());
              } else {
                LogEvent.Log(TAG,
                  "Some exception occurred while retrieving user consent, e.getCode(): " +
                    e.getCode());
                if(e.getCode() == 101) {
                  if(Modules.isWeakReferenceValid()) {
                    Intent intentUserConsent =
                      new Intent(Singleton.INSTANCE.getCurrentActivity(),
                        UserConsentActivity.class);
                    intentUserConsent.putExtra("privacyPolicyId",
                      objectPrivacyPolicy.getObjectId());
                    if(objectPrivacyPolicy.getString("ppUrl") != null) {
                      intentUserConsent.putExtra("ppUrl", objectPrivacyPolicy.getString("ppUrl"));
                    }
                    if(objectPrivacyPolicy.getString("tosUrl") != null) {
                      intentUserConsent.putExtra("tosUrl", objectPrivacyPolicy.getString("tosUrl"));
                    }
                    Singleton.INSTANCE.getCurrentActivity().startActivity(intentUserConsent);
                  }
                }
              }
            }
          });
        } else {
          LogEvent.Log(TAG, "Some exception occurred while retrieving privacy policy");
        }
      }
    });
  }

  private void signInFirebase() {
    LogEvent.Log(TAG, "signInFirebase() invoked");
    mAuth.signInAnonymously().addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
      @Override
      public void onComplete(@NonNull Task<AuthResult> task) {
        if(task.isSuccessful()) {
          // Sign in success, update UI with the signed-in user's information
          LogEvent.Log(TAG, "signInAnonymously:success");
          FirebaseUser user = mAuth.getCurrentUser();
          LogEvent.Log(TAG, "user.isAnonymous: " + user.isAnonymous());
          LogEvent.Log(TAG, "uid: " + user.getUid());
        } else {
          // If sign in fails, display a message to the user.
          LogEvent.Log(TAG, "signInAnonymously:failure " + task.getException());
        }
      }
    });
  }

  private void updateUserInfo() {
    Singleton.INSTANCE.setImage(imgUser);
    ParseUser user = getCurrentUser();
    txtName.setText(
      (String) user.get(ParseKeys.FIRST_NAME) + " " + (String) user.get(ParseKeys.LAST_NAME));
    if(user.getUsername().contains("@")) {
      txtEmail.setText(user.getUsername());
    } else if(user.get("email") != null && !user.get("email").toString().isEmpty()) {
      txtEmail.setText(user.get("email").toString());
    } else {
      txtEmail.setVisibility(View.GONE);
    }
    String bloodType = (String) user.get(ParseKeys.BLOOD_TYPE);
    if(bloodType != null && !bloodType.isEmpty() && !bloodType.equals("null")) {
      txtBloodGroup.setText(bloodType);
    } else {
      txtBloodGroup.setText(R.string.not_available);
    }
  }

  @Override
  public boolean onNavigationItemSelected(MenuItem item) {
    if(!isSpinnerVisible()) {
      // Handle navigation view item clicks here.
      int id = item.getItemId();
      if(id == R.id.nav_my_profile) {
        Cell411Analytics.logSelectContentEvent(Cell411Analytics.KEY_NAV_MENU,
          Cell411Analytics.VALUE_NAV_MENU_PROFILE);
        Intent intentProfileView = new Intent(this, ProfileViewActivity.class);
        startActivity(intentProfileView);
      } else if(id == R.id.nav_generate_qr) {
        Cell411Analytics.logSelectContentEvent(Cell411Analytics.KEY_NAV_MENU,
          Cell411Analytics.VALUE_NAV_MENU_GENERATE_QR_CODE);
        if(getCurrentUser().getUsername().contains("@") || (getCurrentUser().getEmail() != null &&
                                                              !getCurrentUser().getEmail()
                                                                 .toString().isEmpty())) {
          QRCodeDialog();
        } else {
          showEmailRequiredDialog();
        }
      } else if(id == R.id.nav_scan_qr) {
        Cell411Analytics.logSelectContentEvent(Cell411Analytics.KEY_NAV_MENU,
          Cell411Analytics.VALUE_NAV_MENU_SCAN_QR_CODE);
        try {
          //IntentIntegrator.forSupportFragment(this).setCaptureActivity(CustomScannerActivity.class).initiateScan();
          IntentIntegrator integrator = new IntentIntegrator(this);
          integrator.setCaptureActivity(CustomScannerActivity.class);
          integrator.initiateScan();
        } catch(Exception e) {
          Uri marketUri = Uri.parse("market://details?id=com.google.zxing.client.android");
          Intent marketIntent = new Intent(Intent.ACTION_VIEW, marketUri);
          startActivity(marketIntent);
        }
      } else if(id == R.id.nav_settings) {
        Cell411Analytics.logSelectContentEvent(Cell411Analytics.KEY_NAV_MENU,
          Cell411Analytics.VALUE_NAV_MENU_SETTINGS);
        Intent intentProfileView = new Intent(this, SettingsActivity.class);
        startActivity(intentProfileView);
      } else if(id == R.id.nav_notifications) {
        Cell411Analytics.logSelectContentEvent(Cell411Analytics.KEY_NAV_MENU,
          Cell411Analytics.VALUE_NAV_MENU_NOTIFICATIONS);
        Intent intentCustomNotification = new Intent(this, CustomNotificationActivity.class);
        startActivity(intentCustomNotification);
      } else if(id == R.id.nav_know_your_rights) {
        Cell411Analytics.logSelectContentEvent(Cell411Analytics.KEY_NAV_MENU,
          Cell411Analytics.VALUE_NAV_MENU_KNOW_YOUR_RIGHTS);
        Intent intentKnowYourRights = new Intent(this, KnowYourRightsActivity.class);
        startActivity(intentKnowYourRights);
      } else if(id == R.id.nav_share_this_app) {
        shareThisApp();
      } else if(id == R.id.nav_rate_this_app) {
        rateThisApp();
      } else if(id == R.id.nav_faq_and_tutorials) {
        Cell411Analytics.logSelectContentEvent(Cell411Analytics.KEY_NAV_MENU,
          Cell411Analytics.VALUE_NAV_MENU_FAQ_AND_TUTORIALS);
        Intent intentWeb = new Intent(Intent.ACTION_VIEW);
        intentWeb.setData(Uri.parse(getString(R.string.faq_url)));
        startActivity(intentWeb);
      } else if(id == R.id.nav_change_password) {
        Intent intentChangePassword = new Intent(this, ChangePasswordActivity.class);
        startActivity(intentChangePassword);
      } else if(id == R.id.nav_about) {
        Cell411Analytics.logSelectContentEvent(Cell411Analytics.KEY_NAV_MENU,
          Cell411Analytics.VALUE_NAV_MENU_ABOUT);
        Intent intentAbout = new Intent(this, AboutActivity.class);
        startActivity(intentAbout);
      } else if(id == R.id.nav_broadcast_message) {
        Cell411Analytics.logSelectContentEvent(Cell411Analytics.KEY_NAV_MENU,
          Cell411Analytics.VALUE_NAV_MENU_BROADCAST_MESSAGE);
        Intent intentBroadcastMessage = new Intent(this, BroadcastMessageActivity.class);
        startActivity(intentBroadcastMessage);
      } else if(id == R.id.nav_logout) {
        Cell411Analytics.logSelectContentEvent(Cell411Analytics.KEY_NAV_MENU,
          Cell411Analytics.VALUE_NAV_MENU_LOGOUT);
        showLogoutAlertDialog();
      }
    }
    DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
    drawer.closeDrawer(GravityCompat.START);
    return true;
  }

  private void showEmailRequiredDialog() {
    android.app.AlertDialog.Builder alert = new android.app.AlertDialog.Builder(this);
    alert.setTitle(R.string.dialog_title_email);
    alert.setMessage(R.string.dialog_message_email_required);
    alert.setCancelable(false);
    LayoutInflater inflater = (LayoutInflater) getSystemService(Service.LAYOUT_INFLATER_SERVICE);
    View view = inflater.inflate(R.layout.dialog_email, null);
    final ProgressBar pb = (ProgressBar) view.findViewById(R.id.pb);
    final EditText etEmail = (EditText) view.findViewById(R.id.et_email);
    alert.setView(view);
    alert.setNegativeButton(R.string.dialog_btn_cancel, new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int arg1) {
        dialog.dismiss();
      }
    });
    alert.setPositiveButton(R.string.dialog_btn_submit, new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int which) {
      }
    });
    final android.app.AlertDialog dialog = alert.create();
    dialog.show();
    dialog.getButton(android.app.AlertDialog.BUTTON_POSITIVE).setOnClickListener(
      new View.OnClickListener() {
        @Override
        public void onClick(View v) {
          if(pb.getVisibility() != View.VISIBLE) {
            final String email = etEmail.getText().toString().trim();
            if(email.isEmpty()) {
              getApplicationContext();
              Singleton.sendToast(R.string.validation_email);
              return;
            } else if(!email.contains("@")) {
              getApplicationContext();
              Singleton.sendToast(R.string.validation_email_invalid);
              return;
            }
            pb.setVisibility(View.VISIBLE);
            ParseQuery<ParseUser> userParseQuery = ParseUser.getQuery();
            userParseQuery.whereEqualTo("username", email);
            ParseQuery<ParseUser> userParseQuery2 = ParseUser.getQuery();
            userParseQuery2.whereEqualTo("email", email);
            // Club all the queries into one master query
            List<ParseQuery<ParseUser>> queries = new ArrayList<>();
            queries.add(userParseQuery);
            queries.add(userParseQuery2);
            ParseQuery<ParseUser> mainQuery = ParseQuery.or(queries);
            mainQuery.findInBackground(new FindCallback<ParseUser>() {
              @Override
              public void done(List<ParseUser> list, ParseException e) {
                if(e == null) {
                  if(list == null || list.size() == 0) {
                    ParseUser parseUserCurrent = getCurrentUser();
                    parseUserCurrent.setEmail(email.toLowerCase().trim());
                    parseUserCurrent.saveInBackground(new SaveCallback() {
                      @Override
                      public void done(ParseException e) {
                        pb.setVisibility(View.GONE);
                        if(weakRef.get() != null && !weakRef.get().isFinishing()) {
                          if(e == null) {
                            if(weakRef.get() != null && !weakRef.get().isFinishing()) {
                              getApplicationContext();
                              Singleton.sendToast(R.string.email_updated_successfully);
                            }
                            QRCodeDialog();
                          } else {
                            if(weakRef.get() != null && !weakRef.get().isFinishing()) {
                              Singleton.sendToast(e);
                            }
                          }
                        }
                        dialog.dismiss();
                      }
                    });
                  } else {
                    pb.setVisibility(View.GONE);
                    // display popup
                    showEmailAlreadyRegisteredAlert(email);
                  }
                } else {
                  pb.setVisibility(View.GONE);
                  if(weakRef.get() != null && !weakRef.get().isFinishing()) {
                    Singleton.sendToast(e);
                  }
                }
              }
            });
          }
        }
      });
  }

  public void showEmailAlreadyRegisteredAlert(String email) {
    AlertDialog.Builder alert = new AlertDialog.Builder(this);
    alert.setMessage(email + " " + getString(R.string.email_already_registered));
    alert.setPositiveButton(R.string.dialog_btn_ok, new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int which) {
        dialog.dismiss();
      }
    });
    alert.create().show();
  }

  private void QRCodeDialog() {
    AlertDialog.Builder alert = new AlertDialog.Builder(this);
    alert.setTitle(R.string.dialog_title_qr_code);
    LayoutInflater inflater = (LayoutInflater) getSystemService(Service.LAYOUT_INFLATER_SERVICE);
    View view = inflater.inflate(R.layout.layout_qrcode, null);
    final LinearLayout linearLayout = (LinearLayout) view.findViewById(R.id.ll_qrcode);
    final ImageView imgQRCode = (ImageView) view.findViewById(R.id.img_qrcode);
    TextView txtEmail = (TextView) view.findViewById(R.id.txt_email);
    String email = null;
    if(getCurrentUser().getUsername().contains("@")) {
      email = getCurrentUser().getUsername();
    } else {
      email = (String) getCurrentUser().getEmail();
    }
    txtEmail.setText(email);
    //Find screen size
    int width = Singleton.getScreenWidth();
    int height = Singleton.getScreenHeight();
    int smallerDimension = width < height ? width : height;
    smallerDimension = smallerDimension * 3 / 4;
    //Encode with a QR Code image
    QRCodeEncoder qrCodeEncoder =
      new QRCodeEncoder(email, null, Contents.Type.TEXT, BarcodeFormat.QR_CODE.toString(),
        smallerDimension);
    Bitmap bitmap = null;
    try {
      bitmap = qrCodeEncoder.encodeAsBitmap();
      imgQRCode.setImageBitmap(bitmap);
    } catch(WriterException e) {
      e.printStackTrace();
    }
    alert.setView(view);
    alert.setNegativeButton(R.string.dialog_btn_cancel, new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int arg1) {
        dialog.dismiss();
      }
    });
    //final Bitmap finalBitmap = Bitmap.createBitmap(linearLayout.getDrawingCache());
    //final Bitmap finalBitmap = bitmap;
    alert.setPositiveButton(R.string.dialog_btn_share, new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int which) {
        linearLayout.measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
          View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
        linearLayout.layout(0, 0, linearLayout.getMeasuredWidth(),
          linearLayout.getMeasuredHeight());
        linearLayout.setDrawingCacheEnabled(true);
        linearLayout.buildDrawingCache(true);
        Bitmap finalBitmap = Bitmap.createBitmap(linearLayout.getDrawingCache());
        linearLayout.destroyDrawingCache();
        Intent share = new Intent(Intent.ACTION_SEND);
        share.setType("image/jpeg");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        finalBitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        File cacheDir;
        // Make sure external shared storage is available
        if(Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
          // We can read and write the media
          cacheDir = getExternalFilesDir(null);
        } else {
          // Load another directory, probably local memory
          cacheDir = getFilesDir();
        }
        File photoFile = new File(cacheDir + File.separator + "cell411.jpg");
        try {
          photoFile.createNewFile();
          FileOutputStream fo = new FileOutputStream(photoFile);
          fo.write(bytes.toByteArray());
        } catch(IOException e) {
          e.printStackTrace();
        }
        final String APPLICATION_ID = "com.safearx.cell411";

        Uri photoURI = FileProvider.getUriForFile(MainActivity.this,
          APPLICATION_ID + ".fileprovider", photoFile);
        share.putExtra(Intent.EXTRA_STREAM, photoURI);
        share.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.share_qr_code_subject));
        share.putExtra(Intent.EXTRA_TEXT, getString(R.string.share_qr_code_desc));
        startActivity(Intent.createChooser(share, getString(R.string.share_qr_code_title)));
      }
    });
    AlertDialog dialog = alert.create();
    dialog.show();
  }

  @Override
  public void onActivityResult(int requestCode, int resultCode, Intent intent) {
    super.onActivityResult(requestCode, resultCode, intent);
    LogEvent.Log(TAG,
      "onActivityResult invoked, requestCode: " + requestCode + " resultCode: " + resultCode);
    IntentResult scanResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);
    if(scanResult != null) {
      if(scanResult.getContents() != null) {
        AddFriendModules.addFriend(this, scanResult.getContents());
      }
    }
  }

  @Override
  public void onClick(View v) {
    int id = v.getId();
    switch(id) {
      case R.id.rl_tab_map:
        setSelectedTab(TABS.MAP, 0);
        break;
      case R.id.rl_tab_friends:
        setSelectedTab(TABS.FRIENDS, 0);
        break;
      case R.id.rl_tab_cells:
        setSelectedTab(TABS.CELLS, 0);
        break;
      case R.id.rl_tab_alerts:
        setSelectedTab(TABS.ALERTS, 0);
        break;
      case R.id.rl_tab_chats:
        setSelectedTab(TABS.CHATS, 0);
        break;
    }
  }

  private void setupTabBar() {
    flTabMap = (FrameLayout) findViewById(R.id.fl_tab_map);
    flTabFriends = (FrameLayout) findViewById(R.id.fl_tab_friends);
    flTabCells = (FrameLayout) findViewById(R.id.fl_tab_cells);
    flTabAlerts = (FrameLayout) findViewById(R.id.fl_tab_alerts);
    flTabChats = (FrameLayout) findViewById(R.id.fl_tab_chats);
    RelativeLayout rlTabMap = (RelativeLayout) findViewById(R.id.rl_tab_map);
    rlTabFriends = (RelativeLayout) findViewById(R.id.rl_tab_friends);
    rlTabCells = (RelativeLayout) findViewById(R.id.rl_tab_cells);
    rlTabAlerts = (RelativeLayout) findViewById(R.id.rl_tab_alerts);
    rlTabChats = (RelativeLayout) findViewById(R.id.rl_tab_chats);
    if(!getResources().getBoolean(R.bool.is_chat_enabled)) {
      rlTabChats.setVisibility(View.GONE);
      ((LinearLayout) findViewById(R.id.ll_tab_bar)).setWeightSum(4);
    }
    imgTabMap = (ImageView) findViewById(R.id.img_tab_map);
    imgTabFriends = (ImageView) findViewById(R.id.img_tab_friends);
    imgTabCells = (ImageView) findViewById(R.id.img_tab_cells);
    imgTabAlerts = (ImageView) findViewById(R.id.img_tab_alerts);
    imgTabChats = (ImageView) findViewById(R.id.img_tab_chats);
    txtTabMap = (TextView) findViewById(R.id.txt_tab_map);
    txtTabFriends = (TextView) findViewById(R.id.txt_tab_friends);
    txtTabCells = (TextView) findViewById(R.id.txt_tab_cells);
    txtTabAlerts = (TextView) findViewById(R.id.txt_tab_alerts);
    txtTabChats = (TextView) findViewById(R.id.txt_tab_chats);
    rlTabMap.setOnClickListener(this);
    rlTabFriends.setOnClickListener(this);
    rlTabCells.setOnClickListener(this);
    rlTabAlerts.setOnClickListener(this);
    rlTabChats.setOnClickListener(this);
    paramsTabMap = (RelativeLayout.LayoutParams) imgTabMap.getLayoutParams();
    paramsTabFriends = (RelativeLayout.LayoutParams) imgTabFriends.getLayoutParams();
    paramsTabCells = (RelativeLayout.LayoutParams) imgTabCells.getLayoutParams();
    paramsTabAlerts = (RelativeLayout.LayoutParams) imgTabAlerts.getLayoutParams();
    paramsTabChats = (RelativeLayout.LayoutParams) imgTabChats.getLayoutParams();
  }

  private void setSelectedTab(TABS tab, int index) {
    if(isSpinnerVisible()) {
      return;
    }
    flTabMap.setVisibility(View.GONE);
    flTabFriends.setVisibility(View.GONE);
    flTabCells.setVisibility(View.GONE);
    flTabAlerts.setVisibility(View.GONE);
    flTabChats.setVisibility(View.GONE);
    imgTabMap.setImageResource(R.drawable.tab_map_unselected);
    imgTabFriends.setImageResource(R.drawable.tab_friend_unselected);
    imgTabCells.setImageResource(R.drawable.tab_cell_unselected);
    imgTabAlerts.setImageResource(R.drawable.tab_alert_unselected);
    imgTabChats.setImageResource(R.drawable.tab_chat_unselected);
    txtTabMap.setTextColor(Color.parseColor(COLOR_DIRTY_WHITE));
    txtTabFriends.setTextColor(Color.parseColor(COLOR_DIRTY_WHITE));
    txtTabCells.setTextColor(Color.parseColor(COLOR_DIRTY_WHITE));
    txtTabAlerts.setTextColor(Color.parseColor(COLOR_DIRTY_WHITE));
    txtTabChats.setTextColor(Color.parseColor(COLOR_DIRTY_WHITE));
    paramsTabMap.setMargins(0, TAB_MARGIN_TOP, 0, 0);
    paramsTabMap.width = TAB_HEIGHT_INACTIVE;
    paramsTabMap.height = TAB_HEIGHT_INACTIVE;
    imgTabMap.setLayoutParams(paramsTabMap);
    paramsTabFriends.setMargins(0, TAB_MARGIN_TOP, 0, 0);
    paramsTabFriends.width = TAB_HEIGHT_INACTIVE;
    paramsTabFriends.height = TAB_HEIGHT_INACTIVE;
    imgTabFriends.setLayoutParams(paramsTabFriends);
    paramsTabCells.setMargins(0, TAB_MARGIN_TOP, 0, 0);
    paramsTabCells.width = TAB_HEIGHT_INACTIVE;
    paramsTabCells.height = TAB_HEIGHT_INACTIVE;
    imgTabCells.setLayoutParams(paramsTabCells);
    paramsTabAlerts.setMargins(0, TAB_MARGIN_TOP, 0, 0);
    paramsTabAlerts.width = TAB_HEIGHT_INACTIVE;
    paramsTabAlerts.height = TAB_HEIGHT_INACTIVE;
    imgTabAlerts.setLayoutParams(paramsTabAlerts);
    paramsTabChats.setMargins(0, TAB_MARGIN_TOP, 0, 0);
    paramsTabChats.width = TAB_HEIGHT_INACTIVE;
    paramsTabChats.height = TAB_HEIGHT_INACTIVE;
    imgTabChats.setLayoutParams(paramsTabChats);
    txtTabMap.setTypeface(txtTabMap.getTypeface(), Typeface.NORMAL);
    txtTabFriends.setTypeface(txtTabFriends.getTypeface(), Typeface.NORMAL);
    txtTabCells.setTypeface(txtTabCells.getTypeface(), Typeface.NORMAL);
    txtTabAlerts.setTypeface(txtTabAlerts.getTypeface(), Typeface.NORMAL);
    txtTabChats.setTypeface(txtTabChats.getTypeface(), Typeface.NORMAL);
    switch(tab) {
      case MAP:
        paramsTabMap.setMargins(0, 0, 0, 0);
        paramsTabMap.width = TAB_HEIGHT_ACTIVE;
        paramsTabMap.height = TAB_HEIGHT_ACTIVE;
        txtTabMap.setTypeface(txtTabMap.getTypeface(), Typeface.BOLD);
        imgTabMap.setImageResource(R.drawable.tab_map_selected);
        txtTabMap.setTextColor(Color.WHITE);
        setTitle(getString(R.string.app_name));
        if(tabMapFragment == null) {
          tabMapFragment = new TabMapFragment();
          fragmentManager.beginTransaction().replace(R.id.fl_tab_map, tabMapFragment).commit();
        }
        flTabMap.setVisibility(View.VISIBLE);
        selectedTab = TABS.MAP;
        invalidateOptionsMenu();
        Cell411Analytics.setScreen(this, Cell411Analytics.Screen.SCREEN_MAP);
        break;
      case FRIENDS:
        paramsTabFriends.setMargins(0, 0, 0, 0);
        paramsTabFriends.width = TAB_HEIGHT_ACTIVE;
        paramsTabFriends.height = TAB_HEIGHT_ACTIVE;
        txtTabFriends.setTypeface(txtTabFriends.getTypeface(), Typeface.BOLD);
        imgTabFriends.setImageResource(R.drawable.tab_friend_selected);
        txtTabFriends.setTextColor(Color.WHITE);
        setTitle(getString(R.string.title_friends));
        if(tabFriendsFragment == null) {
          tabFriendsFragment = new TabFriendsFragment();
          fragmentManager.beginTransaction().replace(R.id.fl_tab_friends,
            tabFriendsFragment).commit();
        }
        FriendsFragment.refreshFriendsList();
        flTabFriends.setVisibility(View.VISIBLE);
        selectedTab = TABS.FRIENDS;
        invalidateOptionsMenu();
        Cell411Analytics.setScreen(this, Cell411Analytics.Screen.SCREEN_FRIEND_SEARCH);
        break;
      case CELLS:
        paramsTabCells.setMargins(0, 0, 0, 0);
        paramsTabCells.width = TAB_HEIGHT_ACTIVE;
        paramsTabCells.height = TAB_HEIGHT_ACTIVE;
        txtTabCells.setTypeface(txtTabCells.getTypeface(), Typeface.BOLD);
        imgTabCells.setImageResource(R.drawable.tab_cell_selected);
        txtTabCells.setTextColor(Color.WHITE);
        //fab.show();
        setTitle(getString(R.string.title_cells));
        if(tabCellsFragment == null) {
          tabCellsFragment = new TabCellsFragment();
          fragmentManager.beginTransaction().replace(R.id.fl_tab_cells, tabCellsFragment).commit();
        }
        flTabCells.setVisibility(View.VISIBLE);
        selectedTab = TABS.CELLS;
        invalidateOptionsMenu();
        Cell411Analytics.setScreen(this, Cell411Analytics.Screen.SCREEN_PUBLIC_CELL);
        break;
      case ALERTS:
        paramsTabAlerts.setMargins(0, 0, 0, 0);
        paramsTabAlerts.width = TAB_HEIGHT_ACTIVE;
        paramsTabAlerts.height = TAB_HEIGHT_ACTIVE;
        txtTabAlerts.setTypeface(txtTabAlerts.getTypeface(), Typeface.BOLD);
        imgTabAlerts.setImageResource(R.drawable.tab_alert_selected);
        txtTabAlerts.setTextColor(Color.WHITE);
        setTitle(getString(R.string.title_alerts));
        if(tabAlertsFragment == null) {
          tabAlertsFragment = new TabAlertsFragment();
          fragmentManager.beginTransaction().replace(R.id.fl_tab_alerts,
            tabAlertsFragment).commit();
        }
        flTabAlerts.setVisibility(View.VISIBLE);
        selectedTab = TABS.ALERTS;
        invalidateOptionsMenu();
        Cell411Analytics.setScreen(this, Cell411Analytics.Screen.SCREEN_ALERT);
        break;
      case CHATS:
        paramsTabChats.setMargins(0, 0, 0, 0);
        paramsTabChats.width = TAB_HEIGHT_ACTIVE;
        paramsTabChats.height = TAB_HEIGHT_ACTIVE;
        txtTabChats.setTypeface(txtTabChats.getTypeface(), Typeface.BOLD);
        imgTabChats.setImageResource(R.drawable.tab_chat_selected);
        txtTabChats.setTextColor(Color.WHITE);
        //fab.show();
        setTitle(getString(R.string.title_chats));
        if(tabChatsFragment == null) {
          tabChatsFragment = new TabChatsFragment();
          fragmentManager.beginTransaction().replace(R.id.fl_tab_chats, tabChatsFragment).commit();
        }
        flTabChats.setVisibility(View.VISIBLE);
        selectedTab = TABS.CHATS;
        invalidateOptionsMenu();
        //sendMessageToEachPublicCell();
        Cell411Analytics.setScreen(this, Cell411Analytics.Screen.SCREEN_CHAT);
        break;
    }
  }

  private void shareThisApp() {
    Cell411Analytics.logSelectContentEvent(Cell411Analytics.KEY_NAV_MENU,
      Cell411Analytics.VALUE_NAV_MENU_SHARE_THIS_APP);
    Intent sharingIntent = new Intent(Intent.ACTION_SEND);
    //sharingIntent.setType("plain/text");
    sharingIntent.setType("text/plain");
    sharingIntent.putExtra(Intent.EXTRA_SUBJECT,
      getString(R.string.share_app_subject).replace("num", version));
    sharingIntent.putExtra(Intent.EXTRA_TEXT,
      getString(R.string.share_app_text).replace("link", getString(R.string.app_url)));
    startActivity(Intent.createChooser(sharingIntent, getString(R.string.share_app_title)));
  }

  private void rateThisApp() {
    Cell411Analytics.logSelectContentEvent(Cell411Analytics.KEY_NAV_MENU,
      Cell411Analytics.VALUE_NAV_MENU_RATE_THIS_APP);
    Intent rateIntent = new Intent(Intent.ACTION_VIEW);
    rateIntent.setData(Uri.parse(getString(R.string.play_store__url)));
    if(rateIntent.resolveActivity(getPackageManager()) != null) {
      startActivity(rateIntent);
    } else {
      getApplicationContext();
      Singleton.sendToast(R.string.no_browsers_installed_to_rate_app);
    }
  }

  private void showLogoutAlertDialog() {
    android.app.AlertDialog.Builder alert = new android.app.AlertDialog.Builder(this);
    alert.setCancelable(false);
    LayoutInflater inflater = (LayoutInflater) getSystemService(Service.LAYOUT_INFLATER_SERVICE);
    View view = inflater.inflate(R.layout.dialog_logout, null);
    final Spinner spinner = (Spinner) view.findViewById(R.id.spinner);
    alert.setView(view);
    alert.setNegativeButton(R.string.dialog_btn_cancel, new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int which) {
      }
    });
    alert.setPositiveButton(R.string.dialog_btn_logout, new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int which) {
      }
    });
    final android.app.AlertDialog dialog = alert.create();
    dialog.show();
    dialog.getButton(android.app.AlertDialog.BUTTON_POSITIVE).setOnClickListener(
            v -> {
              if(spinner.getVisibility() != View.VISIBLE) {
                logout(spinner, dialog);
              }
            });
  }

  private void logout(final Spinner spinner, final android.app.AlertDialog dialog) {
    spinner.setVisibility(View.VISIBLE);
    // First de-associate the current user from the Installation object so to prevent notifications when logged out
    ParseInstallation installation = ParseInstallation.getCurrentInstallation();
    installation.remove(ParseKeys.USER);
    installation.saveInBackground(new SaveCallback() {
      @Override
      public void done(ParseException e) {
        spinner.setVisibility(View.GONE);
        if(e == null) {
          //ParseUser.logOut(); // commented this to prevent app not able to logout when
          // installation mismatch between development and production
          ParseUser.logOutInBackground();
          Singleton.INSTANCE.clearData();

          Singleton.setLoggedIn(false);
          final FirebaseAuth mAuth = FirebaseAuth.getInstance();
          if(mAuth != null && mAuth.getCurrentUser() != null) { // logout firebase user
            mAuth.signOut();
          }
          Intent intent = new Intent(MainActivity.this, GalleryActivity.class);
          intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
          startActivity(intent);
        } else {
          if(e.getCode() == ParseException.INVALID_SESSION_TOKEN) {
            LogEvent.Log(TAG, "INVALID_SESSION_TOKEN");
            Modules.logoutUserHavingInvalidSession();
          } else if(e.getCode() == ParseException.OBJECT_NOT_FOUND) {
            LogEvent.Log(TAG, "OBJECT_NOT_FOUND");
            Modules.logoutUserHavingInvalidSession();
          } else {
            if(weakRef.get() != null && !weakRef.get().isFinishing()) {
              Singleton.sendToast(e);
            }
          }
        }
        dialog.dismiss();
      }
    });
  }

  @Override
  protected void onDestroy() {
    super.onDestroy();
    StorageOperations.deletePhotos(Singleton.INSTANCE);
  }

  @Override
  public void onBackPressed() {
    DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
    if(drawer.isDrawerOpen(GravityCompat.START)) {
      drawer.closeDrawer(GravityCompat.START);
    } else {
      if(System.currentTimeMillis() - previousBackTapMillis <= waitToExitForMillis) {
        super.onBackPressed();
        if(GalleryActivity.INSTANCE != null) {
          GalleryActivity.INSTANCE.finish();
        }
      } else {
        previousBackTapMillis = System.currentTimeMillis();
        getApplicationContext();
        Singleton.sendToast(getString(R.string.press_return_again_to_exit));
      }
    }
  }

  private static enum TABS {
    MAP, FRIENDS, CELLS, ALERTS, CHATS
  }

  public class ChatReceiver extends BroadcastReceiver {
    public ChatReceiver() {
      super();
    }

    @Override
    public void onReceive(Context context, Intent intent) {
      LogEvent.Log(TAG, "onReceive invoked..");
      Chat chat = (Chat) intent.getSerializableExtra("chat");
      chat.isReceived = true;
      LogEvent.Log(TAG, "message: " + chat.msg);
      if(Singleton.INSTANCE.getCurrentActivity() != null &&
           Singleton.INSTANCE.getCurrentActivity() instanceof MainActivity) {
        LogEvent.Log(TAG, "inside first if");
        if(Singleton.INSTANCE.getCurrentFragment() != null &&
             Singleton.INSTANCE.getCurrentFragment() instanceof TabChatsFragment) {
          LogEvent.Log(TAG, "inside second if");
          ((TabChatsFragment) Singleton.INSTANCE.getCurrentFragment()).refreshChatRoomsList();
        } else {
          LogEvent.Log(TAG, "inside second else");
        }
      } else {
        LogEvent.Log(TAG, "inside first else");
      }
    }
  }

  public class LocationReceiver extends BroadcastReceiver {
    private final String TAG = LocationReceiver.class.getSimpleName();

    public LocationReceiver() {
      super();
    }

    @Override
    public void onReceive(Context context, Intent intent) {
      LogEvent.Log(TAG, "onReceive invoked..");
      Location location = intent.getParcelableExtra("location");
      if(tabMapFragment != null) {
        tabMapFragment.onLocationChange(location);
      }
    }
  }
}
