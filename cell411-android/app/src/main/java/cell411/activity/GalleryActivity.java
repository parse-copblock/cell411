package cell411.activity;

import android.content.Intent;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.safearx.cell411.R;
import com.viewpagerindicator.CirclePageIndicator;

import cell411.fragments.GalleryFragment;

/**
 * Created by Sachin on 14-04-2016.
 */
public class GalleryActivity extends AppCompatActivity implements View.OnClickListener {
  public static GalleryActivity INSTANCE;
  private String TAG = "GalleryActivity.java";
  private ViewPager pagerGallery;
  private CirclePageIndicator indicatorGallery;
  private GalleryAdapter adapterGallery;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_gallery);
    INSTANCE = this;
    pagerGallery = (ViewPager) findViewById(R.id.pager_gallery);
    indicatorGallery = (CirclePageIndicator) findViewById(R.id.indicator_gallery);
    TypedArray imageArray = getResources().obtainTypedArray(R.array.gallery_images);
    String[] titlesArray = getResources().getStringArray(R.array.gallery_titles);
    String[] descArray = getResources().getStringArray(R.array.gallery_desc);
    adapterGallery =
      new GalleryAdapter(getSupportFragmentManager(), imageArray.length(), imageArray,
        titlesArray, descArray);
    pagerGallery.setAdapter(adapterGallery);
    indicatorGallery.setViewPager(pagerGallery);
    Button btnSignIn = (Button) findViewById(R.id.btn_signin);
    Button btnSignUp = (Button) findViewById(R.id.btn_signup);
    btnSignIn.setOnClickListener(this);
    btnSignUp.setOnClickListener(this);
  }

  @Override
  public void onClick(View v) {
    int id = v.getId();
    switch(id) {
      case R.id.btn_signin:
        Intent loginIntent = new Intent(this, LoginActivity.class);
        startActivity(loginIntent);
        break;
      case R.id.btn_signup:
        Intent registerIntent = new Intent(this, RegisterActivity.class);
        startActivity(registerIntent);
        break;
    }
  }

  @Override
  public void onBackPressed() {
    super.onBackPressed();
    if(MainActivity.INSTANCE != null) {
      MainActivity.INSTANCE.finish();
    }
  }

  private static class GalleryAdapter extends FragmentPagerAdapter {
    private int count;
    private TypedArray imageArray;
    private String[] titlesArray;
    private String[] descArray;

    public GalleryAdapter(FragmentManager fm, int count, TypedArray imageArray,
                          String[] titlesArray, String[] descArray) {
      super(fm);
      this.count = count;
      this.imageArray = imageArray;
      this.titlesArray = titlesArray;
      this.descArray = descArray;
    }

    @Override
    public Fragment getItem(int arg0) {
      return GalleryFragment.newInstance(imageArray.getResourceId(arg0, -1), titlesArray[arg0],
        descArray[arg0], arg0);
    }

    @Override
    public int getCount() {
      return count;
    }
  }
}