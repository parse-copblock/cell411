package cell411.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Service;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.callback.FindCallback;
import com.parse.callback.FunctionCallback;
import com.parse.callback.SaveCallback;
import com.safearx.cell411.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.lang.ref.WeakReference;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

import cell411.Singleton;
import cell411.constants.Alert;
import cell411.constants.Constants;
import cell411.constants.LMA;
import cell411.constants.ParseKeys;
import cell411.fragments.LocationFragment;
import cell411.interfaces.CurrentLocationFromLocationUpdatesCallBack;
import cell411.interfaces.LastLocationCallBack;
import cell411.interfaces.LocationFailToRetrieveCallBack;
import cell411.methods.Modules;
import cell411.methods.PrivilegeModules;
import cell411.models.Cell;
import cell411.models.NewPrivateCell;
import cell411.services.FetchAddressIntentService;
import cell411.utils.LogEvent;
import cell411.utils.StorageOperations;
import cell411.widgets.Spinner;

import static cell411.Singleton.sendToast;
import static cell411.Singleton.setCell411AlertObject;
import static cell411.Singleton.setParams;
import static cell411.SingletonConfig.ENDPOINT_CITY;
import static cell411.SingletonConfig.ENDPOINT_COUNTRY;
import static cell411.SingletonConfig.ENDPOINT_NAME;
import static cell411.SingletonConfig.ENDPOINT_STREAM;
import static cell411.SingletonConfig.ENDPOINT_USER;
import static cell411.SingletonConfig.ENDPOINT_YT;
import static cell411.SingletonConfig.ENDPOINT_YT_APP;
import static cell411.SingletonConfig.ENDPOINT_YT_CELL411;
import static cell411.SingletonConfig.ENDPOINT_YT_HOST;
import static cell411.SingletonConfig.ENDPOINT_YT_KEY;
import static cell411.SingletonConfig.GO_LIVE_BASE_URL;
import static cell411.SingletonConfig.PARAM_SEPARATOR;
import static cell411.SingletonConfig.VALUE_YES;

/**
 * Created by Sachin on 21-03-2018.
 */
public class AlertIssuingActivity extends BaseActivity
  implements View.OnClickListener, LastLocationCallBack, LocationFailToRetrieveCallBack,
               CurrentLocationFromLocationUpdatesCallBack {
  private static final String TAG = "AlertIssuingActivity";
  private static final int SELECT_CONTACTS = 10001;  // The request code
  private static final int PERMISSION_CAMERA_AND_RECORD_AUDIO = 1;
  public static WeakReference<AlertIssuingActivity> weakRef;
  private static String contactArray;
  private static boolean isVideoStreamingNotAvailable;
  private static boolean isVideoStreamingBtnTapped;
  private Spinner spinner;
  private String title;
  private String alertType;
  private String forwardedAlertId;
  private RelativeLayout rlContainer;
  private ImageView imgBack;
  private TextView txtAlertTitle;
  private EditText etAdditionalNote;
  private RecyclerView recyclerViewAudience;
  private AudienceListAdapter audienceListAdapter;
  private RecyclerView recyclerViewAudienceCells;
  private AudienceCellListAdapter audienceCellListAdapter;
  private HashMap<String, Object> audienceMap;
  //private int totalCells;
  private int totalMembers = -1;
  private int INDEX_NAU = -1;
  private int INDEX_CELLS = -1;
  private int INDEX_GLOBAL = -1;
  private LocationFragment locationFragment;
  private boolean isPostAlertToFacebookTapped;
  private boolean isSendAlertTapped;
  private boolean isAlertIssuingInProgress;
  private ProgressBar pb;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    //setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    setContentView(R.layout.activity_alert_issuing);
    Singleton.INSTANCE.setCurrentActivity(this);
    weakRef = new WeakReference<>(this);
    alertType = getIntent().getStringExtra("alertType");
    forwardedAlertId = getIntent().getStringExtra("forwardedAlertId");
    // Refresh the variables
    isVideoStreamingBtnTapped = false;
    isVideoStreamingNotAvailable = false;
    spinner = findViewById(R.id.spinner);
    pb = findViewById(R.id.pb);
    txtAlertTitle = findViewById(R.id.txt_title_alert);
    setPopupBackground(alertType);
    findViewById(R.id.txt_btn_cancel).setOnClickListener(this);
    findViewById(R.id.txt_btn_send).setOnClickListener(this);
    findViewById(R.id.img_close).setOnClickListener(this);
    etAdditionalNote = findViewById(R.id.et_additional_note);
    if(alertType.equals(Alert.GENERAL)) {
      etAdditionalNote.setHint(R.string.enter_alert_description);
    }
    rlContainer = findViewById(R.id.rl_container);
    imgBack = findViewById(R.id.img_back);
    imgBack.setOnClickListener(this);
    recyclerViewAudienceCells = findViewById(R.id.rv_audience_cells);
    recyclerViewAudienceCells.setHasFixedSize(true);
    LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
    recyclerViewAudienceCells.setLayoutManager(linearLayoutManager);
    recyclerViewAudience = findViewById(R.id.rv_audience);
    recyclerViewAudience.setHasFixedSize(true);
    LinearLayoutManager linearLayoutManager2 = new LinearLayoutManager(this);
    recyclerViewAudience.setLayoutManager(linearLayoutManager2);
    final boolean isNonAppUserAlertEnabled = false;
    audienceMap = StorageOperations.getAudience(this);
    if(Singleton.INSTANCE.getNewPrivateCells() == null) {
      final ArrayList<NewPrivateCell> cellList = new ArrayList<>();
      ParseQuery<ParseObject> cellQuery = ParseQuery.getQuery("Cell");
      cellQuery.whereEqualTo("createdBy", ParseUser.getCurrentUser());
      cellQuery.whereNotEqualTo("type", 5);
      cellQuery.include("members");
      cellQuery.include("nauMembers");
      cellQuery.findInBackground(new FindCallback<ParseObject>() {
        @Override
        public void done(List<ParseObject> parseObjects, ParseException e) {
          if(e == null) {
            if(parseObjects != null) {
              for(int i = 0; i < parseObjects.size(); i++) {
                ParseObject cell = parseObjects.get(i);
                NewPrivateCell newPrivateCell =
                  new NewPrivateCell((String) cell.get("name"), cell, cell.getInt("type"),
                    cell.getInt("totalMembers"));
                if(cell.getList("members") != null) {
                  LogEvent.Log("Cell",
                    "There are some members in " + cell.get("name") + " newPrivateCell");
                  List<ParseUser> members = cell.getList("members");
                  newPrivateCell.members.addAll(members);
                } else {
                  LogEvent.Log("Cell",
                    "There are no members in " + cell.get("name") + " newPrivateCell");
                }
                if(isNonAppUserAlertEnabled) {
                  if(cell.getJSONArray("nauMembers") != null) {
                    LogEvent.Log("Cell",
                      "There are some members in" + " " + cell.get("name") + " " +
                        "newPrivateCell");
                    newPrivateCell.nauMembers = cell.getJSONArray("nauMembers");
                  } else {
                    LogEvent.Log("Cell",
                      "There are no members in " + cell.get("name") + " newPrivateCell");
                  }
                }
                cellList.add(newPrivateCell);
              }
            }
          } else {
            if(Modules.isWeakReferenceValid()) {
              sendToast(e);
            }
          }
          Singleton.INSTANCE.setNewPrivateCells((ArrayList<NewPrivateCell>) cellList.clone());
          createAudienceList();
        }
      });
    } else {
      createAudienceList();
    }
    locationFragment = new LocationFragment();
    FragmentManager fragmentManager = getSupportFragmentManager();
    fragmentManager.beginTransaction().add(locationFragment, "locationFragment").commit();
    if(!alertType.equals(Alert.VIDEO) && !alertType.equals(Alert.PHOTO)) {
      boolean isDispatchModeEnabled =
        Singleton.INSTANCE.getAppPrefs().getBoolean("DispatchMode", false);
      if(isDispatchModeEnabled) {
        Intent intent = new Intent(this, PickCustomLocationActivity.class);
        startActivity(intent);
      }
    }
  }

  private void setPopupBackground(String alertType) {
    int BACKGROUND_COLOR = Color.BLACK;
    RelativeLayout rlMainContainer = findViewById(R.id.rl_main_container);
    if(alertType.equals(Alert.CRIMINAL)) {
      BACKGROUND_COLOR = getResources().getColor(R.color.alert_background_criminal);
      title = getString(R.string.send_crime_alert);
    } else if(alertType.equals(Alert.PULLED_OVER)) {
      BACKGROUND_COLOR = getResources().getColor(R.color.alert_background_pulled_over);
      title = getString(R.string.send_vehicle_pulled_over_alert);
    } else if(alertType.equals(Alert.POLICE_INTERACTION)) {
      BACKGROUND_COLOR = getResources().getColor(R.color.alert_background_police_interaction);
      title = getString(R.string.send_police_interaction_alert);
    } else if(alertType.equals(Alert.PRE_AUTHORISATION)) {
      BACKGROUND_COLOR = getResources().getColor(R.color.alert_background_pre_authorisation);
      title = getString(R.string.send_pre_authorisation_alert);
    } else if(alertType.equals(Alert.POLICE_ARREST)) {
      BACKGROUND_COLOR = getResources().getColor(R.color.alert_background_police_arrest);
      title = getString(R.string.send_arrested_alert);
    } else if(alertType.equals(Alert.DANGER)) {
      BACKGROUND_COLOR = getResources().getColor(R.color.alert_background_danger);
      title = getString(R.string.send_danger_alert);
    } else if(alertType.equals(Alert.MEDICAL)) {
      BACKGROUND_COLOR = getResources().getColor(R.color.alert_background_medical);
      title = getString(R.string.send_medical_attention_alert);
    } else if(alertType.equals(Alert.BULLIED)) {
      BACKGROUND_COLOR = getResources().getColor(R.color.alert_background_bullied);
      title = getString(R.string.send_bullied_alert);
    } else if(alertType.equals(Alert.GENERAL)) {
      BACKGROUND_COLOR = getResources().getColor(R.color.alert_background_general);
      title = getString(R.string.send_general_alert);
    } else if(alertType.equals(Alert.PHOTO)) {
      BACKGROUND_COLOR = getResources().getColor(R.color.alert_background_photo);
      title = getString(R.string.send_photo_alert);
    } else if(alertType.equals(Alert.VIDEO)) {
      BACKGROUND_COLOR = getResources().getColor(R.color.alert_background_video);
      title = getString(R.string.stream_and_share_live_video_with);
    } else if(alertType.equals(Alert.FIRE)) {
      BACKGROUND_COLOR = getResources().getColor(R.color.alert_background_fire);
      title = getString(R.string.send_fire_alert);
    } else if(alertType.equals(Alert.BROKEN_CAR)) {
      BACKGROUND_COLOR = getResources().getColor(R.color.alert_background_broken_car);
      title = getString(R.string.send_vehicle_broken_alert);
    } else if(alertType.equals(Alert.HIJACK)) {
      BACKGROUND_COLOR = getResources().getColor(R.color.alert_background_hijack);
      title = getString(R.string.send_hijack_alert);
    } else if(alertType.equals(Alert.PHYSICAL_ABUSE)) {
      BACKGROUND_COLOR = getResources().getColor(R.color.alert_background_physical_abuse);
      title = getString(R.string.send_physical_abuse_alert);
    } else if(alertType.equals(Alert.TRAPPED)) {
      BACKGROUND_COLOR = getResources().getColor(R.color.alert_background_trapped);
      title = getString(R.string.send_being_trapped_alert);
    } else if(alertType.equals(Alert.CAR_ACCIDENT)) {
      BACKGROUND_COLOR = getResources().getColor(R.color.alert_background_car_accident);
      title = getString(R.string.send_car_accident_alert);
    } else if(alertType.equals(Alert.NATURAL_DISASTER)) {
      BACKGROUND_COLOR = getResources().getColor(R.color.alert_background_natural_disaster);
      title = getString(R.string.send_natural_disaster_alert);
    }
    rlMainContainer.setBackgroundColor(BACKGROUND_COLOR);
    txtAlertTitle.setText(title);
  }

  private void createAudienceList() {
    spinner.setVisibility(View.GONE);
    ArrayList<Audience> audiences = new ArrayList<>();
    boolean isNonAppUserAlertsEnabled = false;
    //getResources().getBoolean(R.bool.is_non_app_user_alerts_enabled);
    String[] cellArr = getResources().getStringArray(R.array.default_cells);
    ArrayList<String> arraySelectedDefaultCells = null;
    if(audienceMap != null && audienceMap.containsKey("DefaultCells")) {
      arraySelectedDefaultCells = (ArrayList<String>) audienceMap.get("DefaultCells");
    }
    for(int i = 0; i < cellArr.length; i++) {
      ArrayList<NewPrivateCell> cellList =
        (ArrayList<NewPrivateCell>) Singleton.INSTANCE.getNewPrivateCells().clone();
      boolean isDisabled = true;
      boolean isChecked = true;
      String objectId = null;
      int type = 0;
      switch(i) {
        case 0: // Family
          type = 1;
          break;
        case 1: // Coworkers
          type = 2;
          break;
        case 2: // Schoolmates
          type = 3;
          break;
        case 3: // Neighbors
          type = 4;
          break;
      }
      for(int j = 0; j < cellList.size(); j++) {
        if(cellList.get(j).type != null && cellList.get(j).type == type) {
          int totalMembers = cellList.get(j).members.size();
          if(isNonAppUserAlertsEnabled) {
            totalMembers += cellList.get(j).nauMembers.length();
          }
          if(totalMembers > 0) { // If there are no members in cell it should be disabled
            isDisabled = false;
          }
          objectId = cellList.get(j).parseObject.getObjectId();
          break;
        }
      }
      if(i == 1 || i == 2) { // Friends, Family and Neighbors has to be checked by default
        if(arraySelectedDefaultCells != null && arraySelectedDefaultCells.contains(objectId) &&
             !isDisabled) {
          isChecked = true;
        } else {
          isChecked = false; // by default False
        }
      } else { // by default True
        if(isDisabled) { // If there are no members in cell, it cannot be selected either
          // by default or by the user
          isChecked = false;
        } else {
          if(arraySelectedDefaultCells != null && !arraySelectedDefaultCells.contains(objectId)) {
            isChecked = false;
          }
        }
      }
      audiences.add(
        new Audience(cellArr[i], Audience.AudienceType.DEFAULT_CELL, isChecked, isDisabled,
          objectId, 0));
    }
    boolean friendsSelected = true; // selected by default
    if(audienceMap != null && !audienceMap.containsKey("AllFriends")) {
      friendsSelected = false;
    }
    audiences.add(1,
      new Audience(getString(R.string.audience_friends), Audience.AudienceType.ALL_FRIENDS,
        friendsSelected, false, null, 0));
    if(isNonAppUserAlertsEnabled) {
      boolean nauSelected = false; // Not selected by default
      if(audienceMap != null && audienceMap.containsKey("NAU")) {
        if(audienceMap.get("NAU") != null && ((String) audienceMap.get("NAU")).length() > 2) {
          nauSelected = true;
          contactArray = (String) audienceMap.get("NAU");
        }
      }
      INDEX_NAU = audiences.size();
      audiences.add(new Audience(getString(R.string.audience_sms_email), Audience.AudienceType.NAU,
        nauSelected, false, null, 0));
    }
    boolean cellsSelected = true; // selected by default
    if(audienceMap != null && !audienceMap.containsKey("Cells")) {
      cellsSelected = false;
    }
        /*int totalPrivateCells = ParseUser.getCurrentUser().getInt("totalPrivateCells");
        int totalPublicCells = ParseUser.getCurrentUser().getInt("totalPublicCells");
        int privateCellsCount = 0;
        int publicCellsCount = 0;
        if (audienceMap != null) {
            if (audienceMap.containsKey("PrivateCellsCount")
                    && !(audienceMap.get("PrivateCellsCount") instanceof String)) { // NOT ALL
                privateCellsCount = (int) audienceMap.get("PrivateCellsCount");
                if (privateCellsCount < 0) {
                    totalPrivateCells += privateCellsCount;
                } else if (privateCellsCount > 0) {
                    totalPrivateCells = privateCellsCount;
                } else {
                    totalPrivateCells = 0;
                }
            }
            if (audienceMap.containsKey("PublicCellsCount")
                    && !(audienceMap.get("PublicCellsCount") instanceof String)) { // NOT ALL
                publicCellsCount = (int) audienceMap.get("PublicCellsCount");
                if (publicCellsCount < 0) {
                    totalPublicCells += publicCellsCount;
                } else if (publicCellsCount > 0) {
                    totalPublicCells = publicCellsCount;
                } else {
                    totalPublicCells = 0;
                }
            }
        }
        totalCells = totalPrivateCells + totalPublicCells;*/
        /*LogEvent.Log(TAG, "--------------CREATING--------------");
        int totalPrivateCellMembersCount = ParseUser.getCurrentUser().getInt("totalMembersInPrivateCells");
        int totalPublicCellMembersCount = ParseUser.getCurrentUser().getInt("totalMembersInPublicCells");
        LogEvent.Log(TAG, "Server totalPrivateCellMembersCount: " + totalPrivateCellMembersCount);
        LogEvent.Log(TAG, "Server totalPublicCellMembersCount: " + totalPublicCellMembersCount);
        int privateCellMembersCount = 0;
        int publicCellMembersCount = 0;
        if (audienceMap != null) {
            if (audienceMap.containsKey("PrivateCellsCount")
                    && !(audienceMap.get("PrivateCellsCount") instanceof String)) { // NOT ALL
                privateCellMembersCount = (int) audienceMap.get("PrivateCellMembersCount");
                int privateCellsCount = (int) audienceMap.get("PrivateCellsCount");
                LogEvent.Log(TAG, "Calculating totalPrivateCellMembersCount");
                if (privateCellsCount < 0) {
                    totalPrivateCellMembersCount -= privateCellMembersCount;
                    LogEvent.Log(TAG, "Type 0");
                } else if (privateCellsCount > 0) {
                    totalPrivateCellMembersCount = privateCellMembersCount;
                    LogEvent.Log(TAG, "Type 1");
                } else {
                    totalPrivateCellMembersCount = 0;
                    LogEvent.Log(TAG, "Else");
                }
            }
            if (audienceMap.containsKey("PublicCellMembersCount")
                    && !(audienceMap.get("PublicCellMembersCount") instanceof String)) { // NOT ALL
                publicCellMembersCount = (int) audienceMap.get("PublicCellMembersCount");
                int publicCellsCount = (int) audienceMap.get("PublicCellsCount");
                LogEvent.Log(TAG, "Calculating PublicCellMembersCount");
                if (publicCellsCount < 0) {
                    totalPublicCellMembersCount -= publicCellMembersCount;
                    LogEvent.Log(TAG, "Type 0");
                } else if (publicCellsCount > 0) {
                    totalPublicCellMembersCount = publicCellMembersCount;
                    LogEvent.Log(TAG, "Type 1");
                } else {
                    totalPublicCellMembersCount = 0;
                    LogEvent.Log(TAG, "Else");
                }
            }
            LogEvent.Log(TAG, "PrivateCellMembersCount: " + audienceMap.get("PrivateCellMembersCount"));
            LogEvent.Log(TAG, "PublicCellMembersCount: " + audienceMap.get("PublicCellMembersCount"));
        }
        totalMembers = totalPrivateCellMembersCount + totalPublicCellMembersCount;
        LogEvent.Log(TAG, "After cal totalPrivateCellMembersCount: " + totalPrivateCellMembersCount);
        LogEvent.Log(TAG, "After cal totalPublicCellMembersCount: " + totalPublicCellMembersCount);
        LogEvent.Log(TAG, "After cal totalMembers: " + totalMembers);
        LogEvent.Log(TAG, "--------------CREATED--------------");*/
    INDEX_CELLS = audiences.size();
    audiences.add(
      new Audience(getString(R.string.audience_cells), Audience.AudienceType.CELL, cellsSelected,
        false, null, totalMembers));
    boolean isPatrolModeFeatureEnabled =
      getResources().getBoolean(R.bool.is_patrol_mode_feature_enabled);
    if(isPatrolModeFeatureEnabled) {
      boolean globalSelected = false; // Selected by default
      boolean globalDisabled = true;
      if(audienceMap != null && audienceMap.containsKey("Global")) {
        globalSelected = true;
        globalDisabled = false;
      }
      INDEX_GLOBAL = audiences.size();
      audiences.add(
        new Audience(getString(R.string.audience_global_alert), Audience.AudienceType.GLOBAL,
          globalSelected, globalDisabled, null, 0));
    }
    boolean isSecurityGuardsAlertsEnabled = false;
    audienceListAdapter = new AudienceListAdapter(this, audiences, alertType, recyclerViewAudience,
      recyclerViewAudienceCells, recyclerViewAudienceCells, rlContainer, imgBack, txtAlertTitle);
    recyclerViewAudience.setAdapter(audienceListAdapter);
    createCellsList();
  }

  private void createCellsList() {
    ArrayList<String> privateCellSelectedObjectIdArrayList = new ArrayList<>();
    ArrayList<String> privateCellDeselectedObjectIdArrayList = new ArrayList<>();
    int privateCellSelectedMembersCount = 0;
    int privateCellDeselectedMembersCount = 0;
    boolean isAllPrivateCellSelected = true;
    boolean filterBySelectedPrivateCell = false;
    ArrayList<Object> cells = new ArrayList<>();
    if(Singleton.INSTANCE.getNewPrivateCells() != null) {
      ArrayList<NewPrivateCell> cellList =
        (ArrayList<NewPrivateCell>) Singleton.INSTANCE.getNewPrivateCells().clone();
      // default cells should not be part of the cell list as they are already added to the
      // main name
      for(int i = 0; i < cellList.size(); i++) {
        if(cellList.get(i).type == 0) {
          continue;
        }
        cellList.remove(i--);
      }
      if(audienceMap != null && audienceMap.containsKey("PrivateCells")) {
        Object privateCells = audienceMap.get("PrivateCells");
        if(privateCells instanceof HashMap) {
          HashMap<String, Object> privateCellMap =
            (HashMap<String, Object>) audienceMap.get("PrivateCells");
          int type = (int) privateCellMap.get("type");
          if(type == 1) {
            privateCellSelectedObjectIdArrayList = (ArrayList<String>) privateCellMap.get("array");
            privateCellSelectedMembersCount = (int) audienceMap.get("PrivateCellMembersCount");
          } else {
            privateCellDeselectedObjectIdArrayList =
              (ArrayList<String>) privateCellMap.get("array");
            privateCellDeselectedMembersCount = (int) audienceMap.get("PrivateCellMembersCount");
          }
          LogEvent.Log(TAG, "type: " + type);
                    /*if (privateCellMap.get("privateCellSelectedObjectIdList") != null) {
                        privateCellSelectedObjectIdArrayList = (ArrayList<String>) privateCellMap.get("privateCellSelectedObjectIdList");
                    }
                    if (privateCellMap.get("privateCellDeselectedObjectIdList") != null) {
                        privateCellDeselectedObjectIdArrayList = (ArrayList<String>) privateCellMap.get("privateCellDeselectedObjectIdList");
                    }*/
          if(privateCellSelectedObjectIdArrayList.size() > 0 ||
               privateCellDeselectedObjectIdArrayList.size() > 0) {
            isAllPrivateCellSelected = false;
            if(privateCellSelectedObjectIdArrayList.size() > 0) {
              filterBySelectedPrivateCell = true;
            }
          }
        }
      }
      if(true) {
        for(int i = 0; i < cellList.size(); i++) {
          final NewPrivateCell newPrivateCell = cellList.get(i);
          if((newPrivateCell.members != null && newPrivateCell.members.size() > 0) ||
               (newPrivateCell.nauMembers != null && newPrivateCell.nauMembers.length() > 0)) {
            if(isAllPrivateCellSelected) {
              newPrivateCell.selected = true;
            } else {
              if(filterBySelectedPrivateCell) {
                newPrivateCell.selected = privateCellSelectedObjectIdArrayList.contains(
                  newPrivateCell.parseObject.getObjectId());
              } else {
                newPrivateCell.selected = !privateCellDeselectedObjectIdArrayList.contains(
                  newPrivateCell.parseObject.getObjectId());
              }
            }
            cells.add(newPrivateCell);
          }
        }
      }
    }
    audienceCellListAdapter =
      new AudienceCellListAdapter(cells, totalMembers, privateCellSelectedObjectIdArrayList,
        privateCellDeselectedObjectIdArrayList, privateCellSelectedMembersCount,
        privateCellDeselectedMembersCount, filterBySelectedPrivateCell);
    recyclerViewAudienceCells.setAdapter(audienceCellListAdapter);
    ArrayList<String> publicCellSelectedObjectIdArrayList = new ArrayList<>();
    ArrayList<String> publicCellDeselectedObjectIdArrayList = new ArrayList<>();
    int publicCellSelectedMembersCount = 0;
    int publicCellDeselectedMembersCount = 0;
    boolean isAllPublicCellSelected = true;
    boolean filterBySelectedPublicCell = false;
    if(audienceMap != null && audienceMap.containsKey("PublicCells")) {
      Object publicCells = audienceMap.get("PublicCells");
      if(publicCells instanceof HashMap) {
        HashMap<String, Object> publicCellMap =
          (HashMap<String, Object>) audienceMap.get("PublicCells");
        int type = (int) publicCellMap.get("type");
        if(type == 1) {
          publicCellSelectedObjectIdArrayList = (ArrayList<String>) publicCellMap.get("array");
          publicCellSelectedMembersCount = (int) audienceMap.get("PublicCellMembersCount");
        } else {
          publicCellDeselectedObjectIdArrayList = (ArrayList<String>) publicCellMap.get("array");
          publicCellDeselectedMembersCount = (int) audienceMap.get("PublicCellMembersCount");
        }
        LogEvent.Log(TAG, "type: " + type);
                /*if (publicCellMap.get("publicCellSelectedObjectIdList") != null) {
                    publicCellSelectedObjectIdArrayList = (ArrayList<String>) publicCellMap.get("publicCellSelectedObjectIdList");
                }
                if (publicCellMap.get("publicCellDeselectedObjectIdList") != null) {
                    publicCellDeselectedObjectIdArrayList = (ArrayList<String>) publicCellMap.get("publicCellDeselectedObjectIdList");
                }*/
        if(publicCellSelectedObjectIdArrayList.size() > 0 ||
             publicCellDeselectedObjectIdArrayList.size() > 0) {
          isAllPublicCellSelected = false;
          if(publicCellSelectedObjectIdArrayList.size() > 0) {
            filterBySelectedPublicCell = true;
          }
        }
      }
    }
    audienceCellListAdapter.setPublicCellObjectIdArrayList(publicCellSelectedObjectIdArrayList,
      publicCellDeselectedObjectIdArrayList, publicCellSelectedMembersCount,
      publicCellDeselectedMembersCount, filterBySelectedPublicCell);
    JSONObject objData = new JSONObject();
    try {
      if(!isAllPrivateCellSelected) {
        JSONObject objPrivateCell = new JSONObject();
        if(filterBySelectedPrivateCell) {
          JSONArray jsonArrayPrivateCellSelectedObjectIdList = new JSONArray();
          for(int i = 0; i < privateCellSelectedObjectIdArrayList.size(); i++) {
            jsonArrayPrivateCellSelectedObjectIdList.put(
              privateCellSelectedObjectIdArrayList.get(i));
          }
          objPrivateCell.put("type", 1);
          objPrivateCell.put("array", jsonArrayPrivateCellSelectedObjectIdList);
        } else {
          JSONArray jsonArrayPrivateCellDeselectedObjectIdList = new JSONArray();
          for(int i = 0; i < privateCellDeselectedObjectIdArrayList.size(); i++) {
            jsonArrayPrivateCellDeselectedObjectIdList.put(
              privateCellDeselectedObjectIdArrayList.get(i));
          }
          objPrivateCell.put("type", 0);
          objPrivateCell.put("array", jsonArrayPrivateCellDeselectedObjectIdList);
        }
        objData.put("PrivateCells", objPrivateCell);
      } else {
        objData.put("PrivateCells", true);
      }
    } catch(JSONException e) {
      e.printStackTrace();
    }
    try {
      if(!isAllPublicCellSelected) {
        JSONObject objPublicCell = new JSONObject();
        if(filterBySelectedPublicCell) {
          JSONArray jsonArrayPublicCellSelectedObjectIdList = new JSONArray();
          for(int i = 0; i < publicCellSelectedObjectIdArrayList.size(); i++) {
            jsonArrayPublicCellSelectedObjectIdList.put(publicCellSelectedObjectIdArrayList.get(i));
          }
          objPublicCell.put("type", 1);
          objPublicCell.put("array", jsonArrayPublicCellSelectedObjectIdList);
        } else {
          JSONArray jsonArrayPublicCellDeselectedObjectIdList = new JSONArray();
          for(int i = 0; i < publicCellDeselectedObjectIdArrayList.size(); i++) {
            jsonArrayPublicCellDeselectedObjectIdList.put(
              publicCellDeselectedObjectIdArrayList.get(i));
          }
          objPublicCell.put("type", 0);
          objPublicCell.put("array", jsonArrayPublicCellDeselectedObjectIdList);
        }
        objData.put("PublicCells", objPublicCell);
      } else {
        objData.put("PublicCells", true);
      }
    } catch(JSONException e) {
      e.printStackTrace();
    }
    // Retrieve total members from server
    HashMap<String, Object> params = new HashMap<>();
    params.put("data", objData.toString());
    params.put("clientFirmId", 1);
    params.put("isLive", getResources().getInteger(R.integer.is_live));
    params.put("languageCode", getString(R.string.language_code));
    params.put("platform", "android");
    LogEvent.Log(TAG, "retrieveTotalSelectedMembers cloud function invoking");
    ParseCloud.callFunctionInBackground("retrieveTotalSelectedMembers", params,
      new FunctionCallback<HashMap<String, Object>>() {
        public void done(HashMap<String, Object> result, ParseException e) {
          LogEvent.Log(TAG, "result: " + result);
          if(e == null) {
            audienceCellListAdapter.setTotalMembers((Integer) result.get("users"));
            audienceListAdapter.arrayList.get(INDEX_CELLS).totalSelected =
              audienceCellListAdapter.getTotalMembers();
            if(audienceCellListAdapter.getTotalMembers() == 0) {
              audienceListAdapter.arrayList.get(INDEX_CELLS).isChecked = false;
            }
            audienceListAdapter.notifyItemChanged(INDEX_CELLS);
            if(INDEX_GLOBAL != -1) {
              if(((boolean) result.get("isGlobalAlertEnabled"))) {
                audienceListAdapter.arrayList.get(INDEX_GLOBAL).isDisabled = false;
              } else {
                audienceListAdapter.arrayList.get(INDEX_GLOBAL).privilegeResult =
                  (HashMap<String, Object>) result.get("privilegeResult");
              }
              audienceListAdapter.notifyItemChanged(INDEX_GLOBAL);
            }
          } else {
            sendToast(e);
          }
        }
      });
    ParseQuery<ParseObject> cellQuery = ParseQuery.getQuery("PublicCell");
    cellQuery.whereGreaterThan("totalMembers", 1);
    cellQuery.whereEqualTo("members", ParseUser.getCurrentUser());
    final ArrayList<Object> publicCellsList = new ArrayList<>();
    final boolean finalIsAllPublicCellSelected = isAllPublicCellSelected;
    final boolean finalFilterBySelectedPublicCell = filterBySelectedPublicCell;
    final ArrayList<String> finalPublicCellSelectedObjectIdArrayList =
      publicCellSelectedObjectIdArrayList;
    final ArrayList<String> finalPublicCellDeselectedObjectIdArrayList =
      publicCellDeselectedObjectIdArrayList;
    cellQuery.findInBackground(new FindCallback<ParseObject>() {
      @Override
      public void done(List<ParseObject> parseObjects, ParseException e) {
        if(e == null) {
          if(parseObjects != null) {
            for(int i = 0; i < parseObjects.size(); i++) {
              ParseObject cell = parseObjects.get(i);
              Cell cl = new Cell((String) cell.get("name"), cell);
              cl.isPublicCell = true;
              cl.totalMembers = cell.getInt("totalMembers");
              if(finalIsAllPublicCellSelected) {
                cl.selected = true;
              } else {
                if(finalFilterBySelectedPublicCell) {
                  if(finalPublicCellSelectedObjectIdArrayList.contains(cl.cell.getObjectId())) {
                    cl.selected = true;
                  } else {
                    cl.selected = false;
                  }
                } else {
                  if(finalPublicCellDeselectedObjectIdArrayList.contains(cl.cell.getObjectId())) {
                    cl.selected = false;
                  } else {
                    cl.selected = true;
                  }
                }
              }
              publicCellsList.add(cl);
            }
            audienceCellListAdapter.arrayList.addAll(publicCellsList);
            audienceCellListAdapter.notifyDataSetChanged();
          }
        } else {
          if(Modules.isWeakReferenceValid()) {
            sendToast(e);
          }
        }
      }
    });
  }

  private void saveAudienceSelectionState() {
    LogEvent.Log(TAG, "--------------SAVING--------------");
    audienceMap = new HashMap<>();
    ArrayList<Audience> audienceArrayList = audienceListAdapter.getSelectedAudience();
    ArrayList<String> arrayDefaultCells = new ArrayList<>();
    for(int i = 0; i < audienceArrayList.size(); i++) {
      Audience audience = audienceArrayList.get(i);
      if(audience.type == Audience.AudienceType.ALL_FRIENDS) { // All Friends
        audienceMap.put("AllFriends", true);
      }
      if(audience.type == Audience.AudienceType.DEFAULT_CELL) { // Default Cells
        arrayDefaultCells.add(audience.objectId);
      }
      if(audience.type == Audience.AudienceType.NAU) { // SMS/Email
        audienceMap.put("NAU", contactArray);
      }
      if(audience.type == Audience.AudienceType.CELL) { // CELL
        audienceMap.put("Cells", true);
      }
      if(audience.type == Audience.AudienceType.GLOBAL) { // GLOBAL ALERT
        audienceMap.put("Global", true);
      }
            /*if (audience.type == Audience.AudienceType.CALL_CENTER) { // CALL CENTER
                audienceMap.put("CallCenter", true);
            }*/
    }
    audienceMap.put("DefaultCells", arrayDefaultCells);
    // PRIVATE CELL
    ArrayList<String> selectedPrivateCellObjectIdArrayList =
      audienceCellListAdapter.getSelectedPrivateCellObjectIdArrayList();
    ArrayList<String> deselectedPrivateCellObjectIdArrayList =
      audienceCellListAdapter.getDeselectedPrivateCellObjectIdArrayList();
    boolean filteredByPrivateSelected = audienceCellListAdapter.isFilterBySelectedPrivateCells();
    //LogEvent.Log(TAG, "selectedPrivateCellObjectIdArrayList.size(): " + selectedPrivateCellObjectIdArrayList.size());
    //LogEvent.Log(TAG, "deselectedPrivateCellObjectIdArrayList.size(): " + deselectedPrivateCellObjectIdArrayList.size());
    if((selectedPrivateCellObjectIdArrayList != null &&
          selectedPrivateCellObjectIdArrayList.size() > 0) ||
         (deselectedPrivateCellObjectIdArrayList != null &&
            deselectedPrivateCellObjectIdArrayList.size() > 0)) {
      HashMap<String, Object> privateCellsMap = new HashMap<>();
      if(selectedPrivateCellObjectIdArrayList.size() > 0 &&
           ((deselectedPrivateCellObjectIdArrayList.size() != 0 &&
               selectedPrivateCellObjectIdArrayList.size() <=
                 deselectedPrivateCellObjectIdArrayList.size()) || filteredByPrivateSelected)) {
        privateCellsMap.put("type", 1);
        privateCellsMap.put("array", selectedPrivateCellObjectIdArrayList);
        audienceMap.put("PrivateCellsCount", selectedPrivateCellObjectIdArrayList.size());
        audienceMap.put("PrivateCellMembersCount",
          audienceCellListAdapter.getSelectedPrivateCellMembersCount());
        audienceMap.put("PrivateCells", privateCellsMap);
        LogEvent.Log(TAG, "type: 1");
        LogEvent.Log(TAG, "PrivateCellMembersCount: " +
                            audienceCellListAdapter.getSelectedPrivateCellMembersCount());
      } else if(deselectedPrivateCellObjectIdArrayList.size() > 0 &&
                  (deselectedPrivateCellObjectIdArrayList.size() <=
                     selectedPrivateCellObjectIdArrayList.size() ||
                     selectedPrivateCellObjectIdArrayList.size() ==
                       0)) { // selectedPrivateCellObjectIdArrayList.size() == 0 means all are selected
        privateCellsMap.put("type", 0);
        privateCellsMap.put("array", deselectedPrivateCellObjectIdArrayList);
        audienceMap.put("PrivateCellsCount", -deselectedPrivateCellObjectIdArrayList.size());
        audienceMap.put("PrivateCellMembersCount",
          audienceCellListAdapter.getDeselectedPrivateCellMembersCount());
        audienceMap.put("PrivateCells", privateCellsMap);
        LogEvent.Log(TAG, "type: 0");
        LogEvent.Log(TAG, "PrivateCellMembersCount: " +
                            audienceCellListAdapter.getDeselectedPrivateCellMembersCount());
      } else {
        audienceMap.put("PrivateCellsCount", "ALL");
        audienceMap.put("PrivateCellMembersCount", "ALL");
        audienceMap.put("PrivateCells", true);
        LogEvent.Log(TAG, "PrivateCellMembersCount: ALL");
      }
    } else {
      audienceMap.put("PrivateCellsCount", "ALL");
      audienceMap.put("PrivateCellMembersCount", "ALL");
      audienceMap.put("PrivateCells", true);
      LogEvent.Log(TAG, "PrivateCellMembersCount: ALL");
    }
    // PUBLIC CELL
    ArrayList<String> selectedPublicCellObjectIdArrayList =
      audienceCellListAdapter.getSelectedPublicCellObjectIdArrayList();
    ArrayList<String> deselectedPublicCellObjectIdArrayList =
      audienceCellListAdapter.getDeselectedPublicCellObjectIdArrayList();
    boolean filteredByPublicSelected = audienceCellListAdapter.isFilterBySelectedPublicCells();
    //LogEvent.Log(TAG, "selectedPublicCellObjectIdArrayList.size(): " + selectedPublicCellObjectIdArrayList.size());
    //LogEvent.Log(TAG, "deselectedPublicCellObjectIdArrayList.size(): " + deselectedPublicCellObjectIdArrayList.size());
    if((selectedPublicCellObjectIdArrayList != null &&
          selectedPublicCellObjectIdArrayList.size() > 0) ||
         (deselectedPublicCellObjectIdArrayList != null &&
            deselectedPublicCellObjectIdArrayList.size() > 0)) {
      HashMap<String, Object> publicCellsMap = new HashMap<>();
      if(selectedPublicCellObjectIdArrayList.size() > 0 &&
           ((deselectedPublicCellObjectIdArrayList.size() != 0 &&
               selectedPublicCellObjectIdArrayList.size() <=
                 deselectedPublicCellObjectIdArrayList.size()) || filteredByPublicSelected)) {
        publicCellsMap.put("type", 1);
        publicCellsMap.put("array", selectedPublicCellObjectIdArrayList);
        audienceMap.put("PublicCellsCount", selectedPublicCellObjectIdArrayList.size());
        audienceMap.put("PublicCellMembersCount",
          audienceCellListAdapter.getSelectedPublicCellMembersCount());
        audienceMap.put("PublicCells", publicCellsMap);
        LogEvent.Log(TAG, "type: 1");
        LogEvent.Log(TAG, "PublicCellMembersCount: " +
                            audienceCellListAdapter.getSelectedPublicCellMembersCount());
      } else if(deselectedPublicCellObjectIdArrayList.size() > 0 &&
                  (deselectedPublicCellObjectIdArrayList.size() <=
                     selectedPublicCellObjectIdArrayList.size() ||
                     selectedPublicCellObjectIdArrayList.size() ==
                       0)) { // selectedPublicCellObjectIdArrayList.size() == 0 means all are selected
        publicCellsMap.put("type", 0);
        publicCellsMap.put("array", deselectedPublicCellObjectIdArrayList);
        audienceMap.put("PublicCellsCount", -deselectedPublicCellObjectIdArrayList.size());
        audienceMap.put("PublicCellMembersCount",
          audienceCellListAdapter.getDeselectedPublicCellMembersCount());
        audienceMap.put("PublicCells", publicCellsMap);
        LogEvent.Log(TAG, "type: 0");
        LogEvent.Log(TAG, "PublicCellMembersCount: " +
                            audienceCellListAdapter.getDeselectedPublicCellMembersCount());
      } else {
        audienceMap.put("PublicCellsCount", "ALL");
        audienceMap.put("PublicCellMembersCount", "ALL");
        audienceMap.put("PublicCells", true);
        LogEvent.Log(TAG, "PublicCellMembersCount: ALL");
      }
    } else {
      audienceMap.put("PublicCellsCount", "ALL");
      audienceMap.put("PublicCellMembersCount", "ALL");
      audienceMap.put("PublicCells", true);
      LogEvent.Log(TAG, "PublicCellMembersCount: ALL");
    }
    LogEvent.Log(TAG, "--------------SAVED--------------");
    StorageOperations.storeAudience(this, audienceMap);
  }

  @Override
  protected void onPause() {
    super.onPause();
    if(audienceListAdapter != null) {
      saveAudienceSelectionState();
    }
  }

  @Override
  protected void onResume() {
    super.onResume();
    if(INDEX_NAU != -1 && audienceListAdapter != null) {
      if(audienceListAdapter.arrayList.get(INDEX_NAU).isChecked &&
           (contactArray == null || contactArray.length() <= 2)) {
        audienceListAdapter.arrayList.get(INDEX_NAU).isChecked = false;
        audienceListAdapter.notifyItemChanged(INDEX_NAU);
      }
    }
  }

  @Override
  public void onClick(View view) {
    switch(view.getId()) {
      case R.id.txt_btn_cancel:
      case R.id.img_close:
        if(!isSendAlertTapped) {
          finish();
        } else {
          getApplicationContext();
          sendToast(R.string.please_wait);
        }
        break;
      case R.id.txt_btn_send:
        if(!isSendAlertTapped) {
          setAlert();
          locationFragment.getLastLocation(this, this, Feature.ISSUE_ALERT);
        }
        break;
      case R.id.img_back:
        rlContainer.setVisibility(View.VISIBLE);
        recyclerViewAudienceCells.setVisibility(View.GONE);
        imgBack.setVisibility(View.GONE);
        txtAlertTitle.setText(title);
        if(audienceCellListAdapter.getTotalMembers() == 0) {
          audienceListAdapter.arrayList.get(INDEX_CELLS).isChecked = false;
        }
        audienceListAdapter.arrayList.get(INDEX_CELLS).totalSelected =
          audienceCellListAdapter.getTotalMembers();
        audienceListAdapter.notifyItemChanged(INDEX_CELLS);
        break;
    }
  }

  private void retrieveCurrentLocationAndIssueAlert() {
    if(alertType.equals(Alert.VIDEO)) {
      checkPermissionAndStreamVideo();
    } else {
      if(alertType.equals(Alert.GENERAL)) {
        String additionalText = etAdditionalNote.getText().toString().trim();
        if(additionalText.isEmpty()) {
          getApplicationContext();
          sendToast(R.string.validation_please_enter_description);
        } else {
          issueAlert();
        }
      } else {
        issueAlert();
      }
    }
  }

  private void setAlert() {
    pb.setVisibility(View.VISIBLE);
    isSendAlertTapped = true;
    findViewById(R.id.txt_btn_cancel).setEnabled(false);
    findViewById(R.id.txt_btn_send).setEnabled(false);
  }

  private void clearAlert() {
    pb.setVisibility(View.GONE);
    isSendAlertTapped = false;
    findViewById(R.id.txt_btn_cancel).setEnabled(true);
    findViewById(R.id.txt_btn_send).setEnabled(true);
  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);
    LogEvent.Log(TAG,
      "onActivityResult invoked, requestCode: " + requestCode + " resultCode: " + resultCode);
    // Check which request we're responding to
    if(requestCode == SELECT_CONTACTS) {
      // Make sure the request was successful
      if(resultCode == RESULT_OK) {
        // The user picked a contact.
        // The Intent's data Uri identifies which contact was selected.
        // Do something with the contact here (bigger example below)
        contactArray = data.getStringExtra("contactArray");
        LogEvent.Log(TAG, "contactArray: " + contactArray);
        if(contactArray == null || contactArray.length() == 0) {
          audienceListAdapter.arrayList.get(5).isChecked = false;
          audienceListAdapter.notifyItemChanged(5);
        }
      }
    } else {
    }
  }

  private HashMap<String, Object> createParams(String cell411AlertId,
                                               boolean isStreamingVideoForIssuedAlert) {
    if(audienceListAdapter == null) {
      getApplicationContext();
      sendToast(R.string.please_wait);
    }
    final boolean isDispatchModeEnabled =
      Singleton.INSTANCE.getAppPrefs().getBoolean("DispatchMode", false);
    final String name = ParseUser.getCurrentUser().get("firstName") + " " +
                          ParseUser.getCurrentUser().get("lastName");
    JSONObject objAlert = new JSONObject();
    try {
      if(!isStreamingVideoForIssuedAlert) {
        objAlert.put("title", alertType);
        objAlert.put("alertId", Modules.convertStringToEnum(alertType).getValue());
      } else {
        objAlert.put("title", Alert.VIDEO);
        objAlert.put("alertId", Modules.convertStringToEnum(Alert.VIDEO).getValue());
      }
      if(alertType.equals(Alert.VIDEO) || isStreamingVideoForIssuedAlert) {
        objAlert.put("cell411AlertId", cell411AlertId);
      }
      if(isDispatchModeEnabled && forwardedAlertId == null && !alertType.equals(Alert.PHOTO) &&
           !alertType.equals(Alert.VIDEO) && !isStreamingVideoForIssuedAlert) {
        // An alert cannot be dispatched when forwarding
        objAlert.put("isDispatched", true);
        objAlert.put("lat", Singleton.INSTANCE.getCustomLocation().latitude);
        objAlert.put("lng", Singleton.INSTANCE.getCustomLocation().longitude);
      } else {
        objAlert.put("lat", Singleton.INSTANCE.getLatitude());
        objAlert.put("lng", Singleton.INSTANCE.getLongitude());
      }
      String additionalText = etAdditionalNote.getText().toString().trim();
      if(!additionalText.isEmpty()) {
        objAlert.put("additionalNote", additionalText);
      }
      JSONObject objAudience = new JSONObject();
      ArrayList<Audience> audienceArrayList = audienceListAdapter.getSelectedAudience();
      JSONArray jsonArrayDefaultCells = new JSONArray();
      ArrayList<String> arrayDefaultCells = new ArrayList<>();
      for(int i = 0; i < audienceArrayList.size(); i++) {
        Audience audience = audienceArrayList.get(i);
        if(audience.type == Audience.AudienceType.ALL_FRIENDS) { // ALL FRIENDS
          objAudience.put("AllFriends", true);
        }
        if(audience.type == Audience.AudienceType.DEFAULT_CELL) { // Default Cells
          jsonArrayDefaultCells.put(audience.objectId);
          arrayDefaultCells.add(audience.objectId);
        }
        if(audience.type == Audience.AudienceType.NAU) { // SMS/Email
          objAudience.put("NAU", contactArray);
        }
        if(audience.type == Audience.AudienceType.CELL) { // CELL
          // PRIVATE CELL
          ArrayList<String> selectedPrivateCellObjectIdArrayList =
            audienceCellListAdapter.getSelectedPrivateCellObjectIdArrayList();
          ArrayList<String> deselectedPrivateCellObjectIdArrayList =
            audienceCellListAdapter.getDeselectedPrivateCellObjectIdArrayList();
          LogEvent.Log(TAG, "selectedPrivateCellObjectIdArrayList.size(): " +
                              selectedPrivateCellObjectIdArrayList.size());
          LogEvent.Log(TAG, "deselectedPrivateCellObjectIdArrayList.size(): " +
                              deselectedPrivateCellObjectIdArrayList.size());
          if(selectedPrivateCellObjectIdArrayList.size() > 0 ||
               deselectedPrivateCellObjectIdArrayList.size() > 0) {
            JSONObject objPrivateCells = new JSONObject();
            if(selectedPrivateCellObjectIdArrayList.size() > 0 &&
                 deselectedPrivateCellObjectIdArrayList.size() != 0 &&
                 selectedPrivateCellObjectIdArrayList.size() <=
                   deselectedPrivateCellObjectIdArrayList.size()) {
              JSONArray jsonArrayPrivateCellSelectedObjectIdList = new JSONArray();
              for(int j = 0; j < selectedPrivateCellObjectIdArrayList.size(); j++) {
                jsonArrayPrivateCellSelectedObjectIdList.put(
                  selectedPrivateCellObjectIdArrayList.get(j));
              }
              objPrivateCells.put("type", 1);
              objPrivateCells.put("array", jsonArrayPrivateCellSelectedObjectIdList);
              objAudience.put("PrivateCells", objPrivateCells);
            } else if(deselectedPrivateCellObjectIdArrayList.size() > 0 &&
                        (deselectedPrivateCellObjectIdArrayList.size() <=
                           selectedPrivateCellObjectIdArrayList.size() ||
                           selectedPrivateCellObjectIdArrayList.size() ==
                             0)) { // selectedPrivateCellObjectIdArrayList.size() == 0 means all are selected
              JSONArray jsonArrayPrivateCellDeselectedObjectIdList = new JSONArray();
              for(int j = 0; j < deselectedPrivateCellObjectIdArrayList.size(); j++) {
                jsonArrayPrivateCellDeselectedObjectIdList.put(
                  deselectedPrivateCellObjectIdArrayList.get(j));
              }
              objPrivateCells.put("type", 0);
              objPrivateCells.put("array", jsonArrayPrivateCellDeselectedObjectIdList);
              objAudience.put("PrivateCells", objPrivateCells);
            } else {
              objAudience.put("PrivateCells", true);
            }
          } else {
            objAudience.put("PrivateCells", true);
          }
          // PUBLIC CELL
          ArrayList<String> selectedPublicCellObjectIdArrayList =
            audienceCellListAdapter.getSelectedPublicCellObjectIdArrayList();
          ArrayList<String> deselectedPublicCellObjectIdArrayList =
            audienceCellListAdapter.getDeselectedPublicCellObjectIdArrayList();
          LogEvent.Log(TAG, "selectedPublicCellObjectIdArrayList.size(): " +
                              selectedPublicCellObjectIdArrayList.size());
          LogEvent.Log(TAG, "deselectedPublicCellObjectIdArrayList.size(): " +
                              deselectedPublicCellObjectIdArrayList.size());
          if(selectedPublicCellObjectIdArrayList.size() > 0 ||
               deselectedPublicCellObjectIdArrayList.size() > 0) {
            JSONObject objPublicCells = new JSONObject();
            if(selectedPublicCellObjectIdArrayList.size() > 0 &&
                 deselectedPublicCellObjectIdArrayList.size() != 0 &&
                 selectedPublicCellObjectIdArrayList.size() <=
                   deselectedPublicCellObjectIdArrayList.size()) {
              JSONArray jsonArrayPublicCellSelectedObjectIdList = new JSONArray();
              for(int j = 0; j < selectedPublicCellObjectIdArrayList.size(); j++) {
                jsonArrayPublicCellSelectedObjectIdList.put(
                  selectedPublicCellObjectIdArrayList.get(j));
              }
              objPublicCells.put("type", 1);
              objPublicCells.put("array", jsonArrayPublicCellSelectedObjectIdList);
              objAudience.put("PublicCells", objPublicCells);
            } else if(deselectedPublicCellObjectIdArrayList.size() > 0 &&
                        (deselectedPublicCellObjectIdArrayList.size() <=
                           selectedPublicCellObjectIdArrayList.size() ||
                           selectedPublicCellObjectIdArrayList.size() ==
                             0)) { // selectedPublicCellObjectIdArrayList.size() == 0 means all are selected
              JSONArray jsonArrayPublicCellDeselectedObjectIdList = new JSONArray();
              for(int j = 0; j < deselectedPublicCellObjectIdArrayList.size(); j++) {
                jsonArrayPublicCellDeselectedObjectIdList.put(
                  deselectedPublicCellObjectIdArrayList.get(j));
              }
              objPublicCells.put("type", 0);
              objPublicCells.put("array", jsonArrayPublicCellDeselectedObjectIdList);
              objAudience.put("PublicCells", objPublicCells);
            } else {
              objAudience.put("PublicCells", true);
            }
          } else {
            objAudience.put("PublicCells", true);
          }
        }
        if(audience.type == Audience.AudienceType.GLOBAL) { // GLOBAL ALERT
          int patrolModeRadius;
          String metric = Singleton.INSTANCE.getAppPrefs().getString("metric", "kms");
          if(metric.equals("kms")) {
            patrolModeRadius = Singleton.INSTANCE.getAppPrefs().getInt("PatrolModeRadius", 80);
          } else {
            patrolModeRadius = Singleton.INSTANCE.getAppPrefs().getInt("PatrolModeRadius", 50);
          }
          objAlert.put("metric", metric);
          objAlert.put("radius", patrolModeRadius);
          objAudience.put("Global", true);
        }
        if(audience.type == Audience.AudienceType.CALL_CENTER) { // CALL CENTER
          objAudience.put("CallCenter", true);
        }
      }
      if(jsonArrayDefaultCells.length() > 0) {
        objAudience.put("DefaultCells", jsonArrayDefaultCells);
      }
      objAlert.put("audience", objAudience);
      if(forwardedAlertId == null) {
        if(alertType.equals(Alert.PHOTO)) {
          objAlert.put("type", "PHOTO");
        } else if(alertType.equals(Alert.VIDEO) || isStreamingVideoForIssuedAlert) {
          objAlert.put("type", "VIDEO");
        } else {
          objAlert.put("type", "NEEDY");
        }
      } else {
        objAlert.put("type", "NEEDY_FORWARDED");
      }
      if(forwardedAlertId != null && !isStreamingVideoForIssuedAlert) {
        objAlert.put("forwardedAlertId", forwardedAlertId);
      }
      //objAlert.put("msg", getString(R.string.alert_msg_global, name, alertType));
    } catch(JSONException e) {
      e.printStackTrace();
    }
    LogEvent.Log(TAG, "objAlert: " + objAlert.toString());
    // send alert
    HashMap<String, Object> params = new HashMap<>();
    params.put("alert", objAlert.toString());
    params.put("clientFirmId", 1);
    params.put("isLive", getResources().getInteger(R.integer.is_live));
    params.put("languageCode", getString(R.string.language_code));
    params.put("platform", "android");
    if(alertType.equals(Alert.PHOTO) && !isStreamingVideoForIssuedAlert) {
      String path = getExternalFilesDir(null).getAbsolutePath() + "/Photo/photo_alert.png";
      Bitmap bmp = BitmapFactory.decodeFile(path);
      ByteArrayOutputStream baos = new ByteArrayOutputStream();
      bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
      byte[] imageBytes = baos.toByteArray();
      params.put("imageBytes", imageBytes);
    }
    return params;
  }

  private void issueAlert() {
    ParseCloud.callFunctionInBackground("sendAlertV3", createParams(null, false),
      new FunctionCallback<String>() {
        public void done(String result, ParseException e) {
          clearAlert();
          if(e == null) {
            LogEvent.Log(TAG, "result: " + result);
            // Check the privilege and update it, if required
            PrivilegeModules.checkAndUpdatePrivilege(true);
            JSONObject objResult = null;
            int targetMembersCount = 0;
            int targetNAUMembersCount = 0;
            try {
              objResult = new JSONObject(result);
              targetMembersCount = objResult.getInt("targetMembersCount");
              targetNAUMembersCount = objResult.getInt("targetNAUMembersCount");
            } catch(JSONException jsonException) {
              e.printStackTrace();
            }
            int totalUsers = targetMembersCount + targetNAUMembersCount;
            if(Modules.isWeakReferenceValid()) {
              if(totalUsers >= 1) {
                if(totalUsers == 1) {
                  getApplicationContext();
                  sendToast(R.string.toast_alert_sent_to_1_user);
                } else {
                  getApplicationContext();
                  sendToast(getString(R.string.toast_alert_sent, totalUsers));
                }
                if(forwardedAlertId == null) {
                  finalizeAlert(result, totalUsers, false);
                }
              } else {
                boolean isSecurityGuardsAlertsEnabled = false;
                getApplicationContext();
                sendToast(R.string.no_members_in_the_selected_cell);
                if(forwardedAlertId == null) {
                  finalizeAlert(result, totalUsers, false);
                }
              }
            }
          } else {
            if(Modules.isWeakReferenceValid()) {
              sendToast(e);
            }
          }
        }
      });
  }

  private void finalizeAlert(final String result, final int totalUsers,
                             final boolean displayToastForIER) {
    JSONObject objResult = null;
    String photoUrl = null;
    String cell411AlertId = null;
    long createdAt = 0;
    try {
      objResult = new JSONObject(result);
      cell411AlertId = objResult.getString("cell411AlertId");
      createdAt = objResult.getLong("createdAt");
      if(objResult.has("photoUrl")) {
        photoUrl = objResult.getString("photoUrl");
      }
    } catch(JSONException e) {
      e.printStackTrace();
    }
    // Make an API call to LMA server for the issued alert
    if(alertType.equals(Alert.MEDICAL)) {
      String message = "";
      ParseUser user = ParseUser.getCurrentUser();
      String bloodType = (String) user.get("bloodType");
      String allergies = (String) user.get("allergies");
      String otherMedicalConditions = (String) user.get("otherMedicalConditions");
      if(bloodType != null && !bloodType.isEmpty()) {
        message += "\n" + getString(R.string.medical_info_blood_type) + " " + bloodType + "\n";
      }
      if(allergies != null && !allergies.isEmpty()) {
        message += "\n" + getString(R.string.medical_info_allergies) + " " + allergies + "\n";
      }
      if(otherMedicalConditions != null && !otherMedicalConditions.isEmpty()) {
        message += "\n" + getString(R.string.medical_info_other_medical_conditions) + " " +
                     otherMedicalConditions + "\n";
      }
      if(!message.isEmpty()) {
        showMedicalDetailsDialog(message);
      }
    }
    boolean isLiveStreamingEnabled = getResources().getBoolean(R.bool.is_live_streaming_enabled);
    if(isLiveStreamingEnabled) {
      showVideoStreamingAlert(photoUrl, createdAt, cell411AlertId);
    } else {
      finish();
    }
  }

  @Override
  public void onLastLocationRetrieved(Location mLastLocation, Feature feature) {
    LogEvent.Log(TAG, "onLastLocationRetrieved() invoked");
    if(mLastLocation != null) {
      if(!isAlertIssuingInProgress) {
        isAlertIssuingInProgress = true;
        retrieveCurrentLocationAndIssueAlert();
      }
    }
  }

  @Override
  public void onCurrentLocationRetrievedFromLocationUpdates(Location location, Feature feature) {
    LogEvent.Log(TAG, "onCurrentLocationRetrievedFromLocationUpdates() invoked");
    if(location != null) {
      if(!isAlertIssuingInProgress) {
        isAlertIssuingInProgress = true;
        retrieveCurrentLocationAndIssueAlert();
      }
    }
  }

  @Override
  public void onLocationFailToRetrieve(FailureReason failureReason, Feature feature) {
    LogEvent.Log(TAG, "onLocationFailToRetrieve() invoked: " + failureReason.toString());
    clearAlert();
    switch(failureReason) {
      case PERMISSION_DENIED:
      case SETTINGS_REQUEST_DENIED:
        getApplicationContext();
        sendToast("Cannot issue alert without location: " + failureReason.toString());
        break;
      default:
        locationFragment.getCurrentLocationFromLocationUpdates(this, feature);
    }
  }

  private void showMedicalDetailsDialog(String message) {
    AlertDialog.Builder alert = new AlertDialog.Builder(Singleton.INSTANCE.getCurrentActivity());
    alert.setTitle(R.string.dialog_title_medical_details);
    alert.setMessage(message);
    alert.setPositiveButton(R.string.dialog_btn_done, new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialogInterface, int i) {
      }
    });
    AlertDialog dialog = alert.create();
    dialog.show();
  }

  private void showVideoStreamingAlert(final String photoUrl, final long createdAt,
                                       final String cell411AlertId) {
    AlertDialog.Builder alert = new AlertDialog.Builder(Singleton.INSTANCE.getCurrentActivity());
    alert.setMessage(R.string.dialog_msg_stream_video);
    LayoutInflater inflater = (LayoutInflater) getSystemService(Service.LAYOUT_INFLATER_SERVICE);
    View view = inflater.inflate(R.layout.layout_video_streaming, null);
    final TextView txtBtnStreamVideo = (TextView) view.findViewById(R.id.txt_btn_stream_video);
    final TextView txtBtnPostToFacebook =
      (TextView) view.findViewById(R.id.txt_btn_post_to_facebook);
    final TextView txtBtnNo = (TextView) view.findViewById(R.id.txt_btn_no);
    txtBtnStreamVideo.setVisibility(View.GONE);
    if(!alertType.equals(Alert.PHOTO) && !alertType.equals(Alert.VIDEO) &&
         forwardedAlertId == null) {
      // Alert for streaming video
      boolean isLiveStreamingEnabled = getResources().getBoolean(R.bool.is_live_streaming_enabled);
      if(isLiveStreamingEnabled) {
        txtBtnStreamVideo.setVisibility(View.VISIBLE);
      }
    }
    if(txtBtnStreamVideo.getVisibility() == View.VISIBLE) {
      isVideoStreamingNotAvailable = false;
    } else {
      isVideoStreamingNotAvailable = true;
    }
    alert.setView(view);
    alert.setNegativeButton(R.string.dialog_btn_close, new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int which) {
        finish();
      }
    });
    final AlertDialog dialog = alert.create();
    dialog.show();
    txtBtnStreamVideo.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        checkPermissionAndStreamVideo();
        txtBtnStreamVideo.setEnabled(false);
        isVideoStreamingBtnTapped = true;
        if(txtBtnPostToFacebook.getVisibility() == View.VISIBLE && isPostAlertToFacebookTapped) {
          dialog.dismiss();
          finish();
        }
      }
    });
    txtBtnPostToFacebook.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        final boolean isDispatchModeEnabled =
          Singleton.INSTANCE.getAppPrefs().getBoolean("DispatchMode", false);
        int dispatchMode = 0;
        if(isDispatchModeEnabled && !alertType.equals(Alert.PHOTO)) {
          dispatchMode = 1;
        }
        String additionalText = etAdditionalNote.getText().toString().trim();
        ParseUser user = ParseUser.getCurrentUser();
        String name =
          user.getString(ParseKeys.FIRST_NAME) + " " + user.getString(ParseKeys.LAST_NAME);
        // Initialize the receiver and start the service for reverse geo coded address
        txtBtnPostToFacebook.setEnabled(false);
        isPostAlertToFacebookTapped = true;
        if(txtBtnStreamVideo.getVisibility() == View.VISIBLE && isVideoStreamingBtnTapped) {
          dialog.dismiss();
        }
      }
    });
  }

  private void checkPermissionAndStreamVideo() {
    if(ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) !=
         PackageManager.PERMISSION_GRANTED ||
         ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) !=
           PackageManager.PERMISSION_GRANTED) {
      if(ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA) ||
           ActivityCompat.shouldShowRequestPermissionRationale(this,
             Manifest.permission.RECORD_AUDIO)) {
        ActivityCompat.requestPermissions(this,
          new String[]{Manifest.permission.CAMERA, Manifest.permission.RECORD_AUDIO},
          PERMISSION_CAMERA_AND_RECORD_AUDIO);
      } else {
        ActivityCompat.requestPermissions(this,
          new String[]{Manifest.permission.CAMERA, Manifest.permission.RECORD_AUDIO},
          PERMISSION_CAMERA_AND_RECORD_AUDIO);
      }
    } else {
      streamVideo();
    }
  }

  @Override
  public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                         @NonNull int[] grantResults) {
    if(requestCode == PERMISSION_CAMERA_AND_RECORD_AUDIO) {
      if(grantResults.length == 2 && grantResults[0] == PackageManager.PERMISSION_GRANTED &&
           grantResults[1] == PackageManager.PERMISSION_GRANTED) {
        streamVideo();
      }
    } else {
      super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }
  }

  private void streamVideo() {
    //alertType = Alert.VIDEO;
    //forwardedAlertId = null;
    // Send alert to selected cells
    final ParseObject cell411AlertObject = new ParseObject("Cell411Alert");
    cell411AlertObject.put("status", "PROC_VID");
    cell411AlertObject.saveInBackground(new SaveCallback() {
      @Override
      public void done(ParseException e) {
        if(e == null) {
                    /*parseUsersList = null;
                    isGlobal = false;
                    toastMessage = null;
                    push = null;*/
          setCell411AlertObject(cell411AlertObject);
          setParams(createParams(cell411AlertObject.getObjectId(), true));
          boolean streamVideoToFBPage =
            Singleton.INSTANCE.getAppPrefs().getBoolean("StreamVideoToFacebookPage", false);
          boolean streamVideoToMyFBWall =
            Singleton.INSTANCE.getAppPrefs().getBoolean("StreamVideoToMyFacebookWall", false);
          boolean streamVideoToYTChannel =
            Singleton.INSTANCE.getAppPrefs().getBoolean("StreamVideoToYouTubeChannel", false);
          boolean streamVideoToMyYTChannel =
            Singleton.INSTANCE
              .getAppPrefs().getBoolean("StreamVideoToMyYouTubeChannel", false);
          if(streamVideoToFBPage || streamVideoToMyFBWall || streamVideoToYTChannel ||
               streamVideoToMyYTChannel) {
            MainActivity.setSpinnerVisible(true);
            // Initialize the receiver and start the service for reverse geo coded address
            CityAndCountryReceiver mResultReceiver =
              new CityAndCountryReceiver(new Handler(), AlertIssuingActivity.this,
                cell411AlertObject.getCreatedAt().getTime());
            Intent intent = new Intent(Singleton.INSTANCE.getCurrentActivity(),
              FetchAddressIntentService.class);
            intent.putExtra(Constants.RECEIVER, mResultReceiver);
            intent.putExtra(Constants.LOCATION_DATA_LAT, Singleton.INSTANCE.getLatitude());
            intent.putExtra(Constants.LOCATION_DATA_LNG, Singleton.INSTANCE.getLongitude());
            FetchAddressIntentService.enqueueWork(AlertIssuingActivity.this, intent);
          } else {
            Intent intent =
              new Intent(Singleton.INSTANCE.getCurrentActivity(), VideoStreamingActivity.class);
            startActivity(intent);
          }
        } else {
          if(Modules.isWeakReferenceValid()) {
            sendToast(e);
          }
        }
      }
    });
    if(alertType.equals(Alert.VIDEO)) {
      finish();
    }
  }

  private static class AudienceListAdapter
    extends RecyclerView.Adapter<AudienceListAdapter.ViewHolder> {
    private Activity context;
    private ArrayList<Audience> arrayList;
    private String alertType;
    private RecyclerView recyclerView;
    private RecyclerView recyclerViewPrivateCells;
    private RecyclerView recyclerViewPublicCells;
    private RelativeLayout rlContainer;
    private ImageView imgBack;
    private TextView txtAlertTitle;
    private final View.OnClickListener mOnClickListener = new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        int position = recyclerView.getChildAdapterPosition(v);
        Audience audience = arrayList.get(position);
        switch(audience.type) {
          case NAU:
            Intent intentSelectContact = new Intent(context, SelectContactsActivity.class);
            intentSelectContact.putExtra("title", alertType);
            intentSelectContact.putExtra("contactArray", contactArray);
            context.startActivityForResult(intentSelectContact, SELECT_CONTACTS);
            break;
          case CELL:
            rlContainer.setVisibility(View.GONE);
            recyclerViewPrivateCells.setVisibility(View.VISIBLE);
            imgBack.setVisibility(View.VISIBLE);
            txtAlertTitle.setText(R.string.select_cells_to_send_alert);
            break;
        }
      }
    };

    // Provide a suitable constructor (depends on the kind of data set)
    public AudienceListAdapter(Activity context, ArrayList<Audience> arrayList, String alertType,
                               RecyclerView recyclerView, RecyclerView recyclerViewPrivateCells,
                               RecyclerView recyclerViewPublicCells, RelativeLayout rlContainer,
                               ImageView imgBack, TextView txtAlertTitle) {
      this.context = context;
      this.arrayList = arrayList;
      this.alertType = alertType;
      this.recyclerView = recyclerView;
      this.recyclerViewPrivateCells = recyclerViewPrivateCells;
      this.recyclerViewPublicCells = recyclerViewPublicCells;
      this.rlContainer = rlContainer;
      this.imgBack = imgBack;
      this.txtAlertTitle = txtAlertTitle;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public AudienceListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
      // create a new view
      View v =
        LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_audience, parent, false);
      AudienceListAdapter.ViewHolder vh = new AudienceListAdapter.ViewHolder(v, viewType);
      v.setOnClickListener(mOnClickListener);
      return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final AudienceListAdapter.ViewHolder viewHolder,
                                 final int position) {
      // - get element from your data set at this position
      // - replace the contents of the view with that element
      final Audience audience = arrayList.get(position);
      viewHolder.txtAudience.setText(audience.name);
      if(audience.type == Audience.AudienceType.CELL) {
        //viewHolder.txtTotalAudience.setVisibility(View.GONE);
        //viewHolder.imgExpand.setVisibility(View.VISIBLE);
        viewHolder.txtTotalAudience.setVisibility(View.VISIBLE);
        viewHolder.imgExpand.setVisibility(View.VISIBLE);
        viewHolder.imgInfo.setVisibility(View.GONE);
        LogEvent.Log(TAG, "audience.totalSelected: " + audience.totalSelected);
        if(audience.totalSelected != -1) {
          if(audience.totalSelected == 1) {
            viewHolder.txtTotalAudience.setText(
              context.getString(R.string.user_1, audience.totalSelected));
          } else {
            viewHolder.txtTotalAudience.setText(
              context.getString(R.string.user_x, audience.totalSelected));
          }
        } else {
          viewHolder.txtTotalAudience.setText("");
        }
      } else if(audience.type == Audience.AudienceType.NAU) {
        viewHolder.txtTotalAudience.setVisibility(View.GONE);
        viewHolder.imgExpand.setVisibility(View.VISIBLE);
        viewHolder.imgInfo.setVisibility(View.GONE);
      } else if(audience.type == Audience.AudienceType.GLOBAL) {
        viewHolder.txtTotalAudience.setVisibility(View.GONE);
        viewHolder.imgExpand.setVisibility(View.GONE);
        if(audience.privilegeResult != null) {
          viewHolder.imgInfo.setVisibility(View.VISIBLE);
          viewHolder.imgInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              displayInformationDialog(audience.privilegeResult);
            }
          });
        } else {
          viewHolder.imgInfo.setVisibility(View.GONE);
        }
      } else {
        viewHolder.txtTotalAudience.setVisibility(View.GONE);
        viewHolder.imgExpand.setVisibility(View.GONE);
        viewHolder.imgInfo.setVisibility(View.GONE);
      }
      viewHolder.cbAudience.setOnCheckedChangeListener(null);
      viewHolder.cbAudience.setChecked(audience.isChecked);
      if(audience.isDisabled) {
        viewHolder.cbAudience.setEnabled(false);
      } else {
        viewHolder.cbAudience.setEnabled(true);
        viewHolder.cbAudience.setOnCheckedChangeListener(
          new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
              audience.isChecked = isChecked;
              if(isChecked && audience.type == Audience.AudienceType.CELL &&
                   audience.totalSelected == 0) {
                rlContainer.setVisibility(View.GONE);
                recyclerViewPrivateCells.setVisibility(View.VISIBLE);
                imgBack.setVisibility(View.VISIBLE);
                txtAlertTitle.setText(R.string.select_cells_to_send_alert);
              } else if(isChecked && audience.type == Audience.AudienceType.NAU &&
                          (contactArray == null || contactArray.length() <= 2)) {
                Intent intentSelectContact = new Intent(context, SelectContactsActivity.class);
                intentSelectContact.putExtra("title", alertType);
                intentSelectContact.putExtra("contactArray", contactArray);
                context.startActivityForResult(intentSelectContact, SELECT_CONTACTS);
              }
              if(contactArray != null) {
                LogEvent.Log(TAG, "contactArray: " + contactArray);
              }
            }
          });
      }
    }

    // Return the size of your data set (invoked by the layout manager)
    @Override
    public int getItemCount() {
      return arrayList.size();
    }

    public ArrayList<Audience> getSelectedAudience() {
      ArrayList<Audience> selectedAudienceList = new ArrayList<>();
      for(int i = 0; i < arrayList.size(); i++) {
        if(arrayList.get(i).isChecked) {
          selectedAudienceList.add(arrayList.get(i));
        }
      }
      return selectedAudienceList;
    }

    private void displayInformationDialog(HashMap<String, Object> privilegeResult) {
      AlertDialog.Builder alert = new AlertDialog.Builder(Singleton.INSTANCE.getCurrentActivity());
      alert.setTitle(R.string.dialog_title_global_alert_requirements);
      String message = context.getString(R.string.dialog_message_global_alert_requirement);
      ArrayList<String> incompleteConditionsArrayList =
        (ArrayList<String>) privilegeResult.get("incompleteConditions");
      for(int i = 0; i < incompleteConditionsArrayList.size(); i++) {
        if(incompleteConditionsArrayList.get(i).equals("Friends")) {
          message += "\n- " +
                       context.getString(R.string.dialog_message_global_alert_requirement_friend,
                         context.getString(R.string.app_name));
        } else if(incompleteConditionsArrayList.get(i).equals("Cell join request")) {
          message += "\n- " +
                       context
                         .getString(R.string.dialog_message_global_alert_requirement_public_cell);
        } else if(incompleteConditionsArrayList.get(i).equals("Profile Completeness")) {
          message += "\n- " +
                       context.getString(R.string.dialog_message_global_alert_requirement_profile,
                         ((int) ((HashMap<String, Object>) privilegeResult
                                                             .get("profileCompleteness")).get(
                           "total")));
        } else {
          message += "\n- " +
                       context.getString(R.string.dialog_message_global_alert_requirement_signup,
                         context.getString(R.string.app_name));
        }
      }
      alert.setMessage(message);
            /*LayoutInflater inflater = (LayoutInflater) getSystemService(Service.LAYOUT_INFLATER_SERVICE);
            View view = inflater.inflate(R.layout.layout_goto_profile, null);
            final TextView txtBtnGoToProfile = (TextView) view.findViewById(R.id.txt_btn_stream_video);
            alert.setView(view);*/
      alert.setNegativeButton(R.string.dialog_btn_close, new DialogInterface.OnClickListener() {
        public void onClick(DialogInterface dialog, int which) {
        }
      });
      final AlertDialog dialog = alert.create();
      dialog.show();
            /*txtBtnGoToProfile.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });*/
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public class ViewHolder extends RecyclerView.ViewHolder {
      // each data item is just a string in this case
      private CheckBox cbAudience;
      private TextView txtAudience;
      private TextView txtTotalAudience;
      private ImageView imgExpand;
      private ImageView imgInfo;

      public ViewHolder(View view, int type) {
        super(view);
        cbAudience = view.findViewById(R.id.cb_audience);
        txtAudience = view.findViewById(R.id.txt_audience);
        txtTotalAudience = view.findViewById(R.id.txt_total_audience);
        imgExpand = view.findViewById(R.id.img_expand);
        imgInfo = view.findViewById(R.id.img_info);
      }
    }
  }

  private static class Audience implements Serializable {
    private static final long serialVersionUID = 101010L;
    private String name;
    private AudienceType type;
    private boolean isChecked;
    private boolean isDisabled;
    private String objectId;
    private int totalSelected;
    private HashMap<String, Object> privilegeResult;

    public Audience(String name, AudienceType type, boolean isChecked, boolean isDisabled,
                    String objectId, int totalSelected) {
      this.name = name;
      this.type = type;
      this.isChecked = isChecked;
      this.isDisabled = isDisabled;
      this.objectId = objectId;
      this.totalSelected = totalSelected;
    }

    private static enum AudienceType {
      ALL_FRIENDS, DEFAULT_CELL, NAU, CELL, GLOBAL, CALL_CENTER
    }
  }

  private static class AudienceCellListAdapter
    extends RecyclerView.Adapter<AudienceCellListAdapter.ViewHolder> {
    private final int VIEW_TYPE_PRIVATE_CELL = 0;
    private final int VIEW_TYPE_PUBLIC_CELL = 1;
    private final View.OnClickListener mOnClickListener = new View.OnClickListener() {
      @Override
      public void onClick(View v) {
      }
    };
    private ArrayList<Object> arrayList;
    private int totalMembers;
    private int temp = 0;
    private ArrayList<String> privateCellSelectedObjectIdArrayList;
    private ArrayList<String> privateCellDeselectedObjectIdArrayList;
    private ArrayList<String> publicCellSelectedObjectIdArrayList;
    private ArrayList<String> publicCellDeselectedObjectIdArrayList;
    private int privateCellSelectedMembersCount;
    private int privateCellDeselectedMembersCount;
    private int publicCellSelectedMembersCount;
    private int publicCellDeselectedMembersCount;
    private boolean filteredByPrivateSelected;
    private boolean filteredByPublicSelected;

    // Provide a suitable constructor (depends on the kind of data set)
    public AudienceCellListAdapter(ArrayList<Object> arrayList, int totalMembers,
                                   ArrayList<String> privateCellSelectedObjectIdArrayList,
                                   ArrayList<String> privateCellDeselectedObjectIdArrayList,
                                   int privateCellSelectedMembersCount,
                                   int privateCellDeselectedMembersCount,
                                   boolean filteredByPrivateSelected) {
      this.arrayList = arrayList;
      this.totalMembers = totalMembers;
      this.privateCellSelectedObjectIdArrayList = privateCellSelectedObjectIdArrayList;
      this.privateCellDeselectedObjectIdArrayList = privateCellDeselectedObjectIdArrayList;
      this.privateCellSelectedMembersCount = privateCellSelectedMembersCount;
      this.privateCellDeselectedMembersCount = privateCellDeselectedMembersCount;
      this.filteredByPrivateSelected = filteredByPrivateSelected;
    }

    public void setPublicCellObjectIdArrayList(
      ArrayList<String> publicCellSelectedObjectIdArrayList,
      ArrayList<String> publicCellDeselectedObjectIdArrayList, int publicCellSelectedMembersCount,
      int publicCellDeselectedMembersCount, boolean filteredByPublicSelected) {
      this.publicCellSelectedObjectIdArrayList = publicCellSelectedObjectIdArrayList;
      this.publicCellDeselectedObjectIdArrayList = publicCellDeselectedObjectIdArrayList;
      this.publicCellSelectedMembersCount = publicCellSelectedMembersCount;
      this.publicCellDeselectedMembersCount = publicCellDeselectedMembersCount;
      this.filteredByPublicSelected = filteredByPublicSelected;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public AudienceCellListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
      // create a new view
      View v = null;
      if(viewType == VIEW_TYPE_PRIVATE_CELL) {
        v = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_audience_cell, parent,
          false);
      } else if(viewType == VIEW_TYPE_PUBLIC_CELL) {
        v = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_audience_cell, parent,
          false);
      }
      AudienceCellListAdapter.ViewHolder vh = new AudienceCellListAdapter.ViewHolder(v, viewType);
      v.setOnClickListener(mOnClickListener);
      return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final AudienceCellListAdapter.ViewHolder viewHolder,
                                 final int position) {
      // - get element from your data set at this position
      // - replace the contents of the view with that element
      if(getItemViewType(position) == VIEW_TYPE_PRIVATE_CELL) {
        final NewPrivateCell newPrivateCell = (NewPrivateCell) arrayList.get(position);
        viewHolder.txtCellName.setText(newPrivateCell.name);
        viewHolder.txtCellTag.setText(R.string.tag_private_cell);
        viewHolder.imgVerified.setVisibility(View.GONE);
        viewHolder.cbCell.setOnCheckedChangeListener(null);
        viewHolder.cbCell.setChecked(newPrivateCell.selected);
        viewHolder.cbCell.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
          @Override
          public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            newPrivateCell.selected = isChecked;
            if(isChecked) {
              if(totalMembers == -1) {
                temp += newPrivateCell.members.size();
                temp += newPrivateCell.nauMembers.length();
              } else {
                totalMembers += newPrivateCell.members.size();
                totalMembers += newPrivateCell.nauMembers.length();
              }
              privateCellSelectedObjectIdArrayList.add(newPrivateCell.parseObject.getObjectId());
              privateCellDeselectedObjectIdArrayList.remove(
                newPrivateCell.parseObject.getObjectId());
              //privateCellSelectedMembersCount += newPrivateCell.totalMembers;
              //privateCellDeselectedMembersCount -= newPrivateCell.totalMembers;
            } else {
              if(totalMembers == -1) {
                temp -= newPrivateCell.members.size();
                temp -= newPrivateCell.nauMembers.length();
              } else {
                totalMembers -= newPrivateCell.members.size();
                totalMembers -= newPrivateCell.nauMembers.length();
              }
              privateCellDeselectedObjectIdArrayList.add(newPrivateCell.parseObject.getObjectId());
              privateCellSelectedObjectIdArrayList.remove(newPrivateCell.parseObject.getObjectId());
              //privateCellDeselectedMembersCount += newPrivateCell.totalMembers;
              //privateCellSelectedMembersCount -= newPrivateCell.totalMembers;
            }
            LogEvent.Log(TAG, "isChecked: " + isChecked);
            LogEvent.Log(TAG, "privateCellSelectedObjectIdArrayList.size(): " +
                                privateCellSelectedObjectIdArrayList.size());
            LogEvent.Log(TAG, "privateCellDeselectedObjectIdArrayList.size(): " +
                                privateCellDeselectedObjectIdArrayList.size());
          }
        });
      } else if(getItemViewType(position) == VIEW_TYPE_PUBLIC_CELL) {
        final Cell cell = (Cell) arrayList.get(position);
        viewHolder.txtCellName.setText(cell.name);
        viewHolder.txtCellTag.setText(R.string.tag_public_cell);
        if(cell.verificationStatus == 1) {
          viewHolder.imgVerified.setVisibility(View.VISIBLE);
        } else {
          viewHolder.imgVerified.setVisibility(View.GONE);
        }
        viewHolder.cbCell.setOnCheckedChangeListener(null);
        viewHolder.cbCell.setChecked(cell.selected);
        viewHolder.cbCell.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
          @Override
          public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            cell.selected = isChecked;
            if(isChecked) {
              //LogEvent.Log(TAG, "totalMembers: " + totalMembers);
              //LogEvent.Log(TAG, "cell.totalMembers: " + cell.totalMembers);
              if(totalMembers == -1) {
                temp += (cell.totalMembers - 1);
              } else {
                totalMembers += (cell.totalMembers - 1);
              }
              //LogEvent.Log(TAG, "totalMembers: " + totalMembers);
              publicCellSelectedObjectIdArrayList.add(cell.cell.getObjectId());
              publicCellDeselectedObjectIdArrayList.remove(cell.cell.getObjectId());
              //publicCellSelectedMembersCount += (cell.totalMembers - 1);
              //publicCellDeselectedMembersCount -= (cell.totalMembers - 1);
            } else {
              //LogEvent.Log(TAG, "totalMembers: " + totalMembers);
              //LogEvent.Log(TAG, "cell.totalMembers: " + cell.totalMembers);
              if(totalMembers == -1) {
                temp -= (cell.totalMembers - 1);
              } else {
                totalMembers -= (cell.totalMembers - 1);
              }
              //LogEvent.Log(TAG, "totalMembers: " + totalMembers);
              publicCellDeselectedObjectIdArrayList.add(cell.cell.getObjectId());
              publicCellSelectedObjectIdArrayList.remove(cell.cell.getObjectId());
              //publicCellDeselectedMembersCount += (cell.totalMembers - 1);
              //publicCellSelectedMembersCount -= (cell.totalMembers - 1);
            }
            LogEvent.Log(TAG, "isChecked: " + isChecked);
            LogEvent.Log(TAG, "publicCellSelectedObjectIdArrayList.size(): " +
                                publicCellSelectedObjectIdArrayList.size());
            LogEvent.Log(TAG, "publicCellDeselectedObjectIdArrayList.size(): " +
                                publicCellDeselectedObjectIdArrayList.size());
            // Issue: When 3 public cells were checked out of 5 public cells and 1 private
            // cell, user unchecked those 3 public cells and rather checked the other 2
            // public cells and 1 private cell, then after closing the popup and then
            // opening again, UI displays "0 selected" and when saw the second page all
            // cells were selected.
            // Expected: Only 3 should have been selected and UI should have displayed
            // "3 selected"
            // This issue came due to publicCellSelectedMembersCount was 0 and when
            // 3 public cells were unchecked then the value of publicCellSelectedMembersCount
            // became -3 and then after checking two public cells it became -1, so when
            // user opened the popup the count for PrivateCellMembersCount was 1 and
            // PublicCellMembersCount was -1 which after summing up became 0.
          }
        });
      }
    }

    // Return the size of your data set (invoked by the layout manager)
    @Override
    public int getItemCount() {
      return arrayList.size();
    }

    @Override
    public int getItemViewType(int position) {
      super.getItemViewType(position);
      if(arrayList.get(position) instanceof NewPrivateCell) {
        return VIEW_TYPE_PRIVATE_CELL;
      } else if(arrayList.get(position) instanceof Cell) {
        return VIEW_TYPE_PUBLIC_CELL;
      } else {
        return -1;
      }
    }

    public int getTotalMembers() {
      return totalMembers;
    }

    public void setTotalMembers(int totalMembers) {
      this.totalMembers = totalMembers + temp;
    }

    public ArrayList<String> getSelectedPrivateCellObjectIdArrayList() {
      return privateCellSelectedObjectIdArrayList;
    }

    public ArrayList<String> getDeselectedPrivateCellObjectIdArrayList() {
      return privateCellDeselectedObjectIdArrayList;
    }

    public ArrayList<String> getSelectedPublicCellObjectIdArrayList() {
      return publicCellSelectedObjectIdArrayList;
    }

    public ArrayList<String> getDeselectedPublicCellObjectIdArrayList() {
      return publicCellDeselectedObjectIdArrayList;
    }

    public int getSelectedPrivateCellMembersCount() {
      return privateCellSelectedMembersCount;
    }

    public int getDeselectedPrivateCellMembersCount() {
      return privateCellDeselectedMembersCount;
    }

    public int getSelectedPublicCellMembersCount() {
      return publicCellSelectedMembersCount;
    }

    public int getDeselectedPublicCellMembersCount() {
      return publicCellDeselectedMembersCount;
    }

    public boolean isFilterBySelectedPrivateCells() {
      return filteredByPrivateSelected;
    }

    public boolean isFilterBySelectedPublicCells() {
      return filteredByPublicSelected;
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public class ViewHolder extends RecyclerView.ViewHolder {
      // each data item is just a string in this case
      private CheckBox cbCell;
      private TextView txtCellName;
      private TextView txtCellTag;
      private ImageView imgVerified;

      public ViewHolder(View view, int type) {
        super(view);
        if(type == VIEW_TYPE_PRIVATE_CELL) {
          cbCell = view.findViewById(R.id.cb_cell);
          txtCellName = view.findViewById(R.id.txt_cell_name);
          imgVerified = view.findViewById(R.id.img_verified);
          txtCellTag = view.findViewById(R.id.txt_cell_tag);
        } else if(type == VIEW_TYPE_PUBLIC_CELL) {
          cbCell = view.findViewById(R.id.cb_cell);
          txtCellName = view.findViewById(R.id.txt_cell_name);
          imgVerified = view.findViewById(R.id.img_verified);
          txtCellTag = view.findViewById(R.id.txt_cell_tag);
        }
      }
    }
  }

  private static class CityAndCountryReceiver extends ResultReceiver {
    private Activity context;
    private long createdAt;

    public CityAndCountryReceiver(Handler handler, final Activity context, final long createdAt) {
      super(handler);
      this.context = context;
      this.createdAt = createdAt;
    }

    @Override
    protected void onReceiveResult(int resultCode, Bundle resultData) {
      String city = null;
      String country = null;
      if(resultCode == Constants.SUCCESS_RESULT) {
        city = resultData.getString(Constants.RESULT_DATA_CITY);
        country = resultData.getString(Constants.RESULT_DATA_COUNTRY);
      }
      new GoLiveApiCall(context, createdAt, city, country).execute();
    }
  }

  private static class GoLiveApiCall extends AsyncTask<String, Void, Boolean> {
    private Activity context;
    private long createdAt;
    private String city;
    private String country;

    public GoLiveApiCall(final Activity context, final long createdAt, String city,
                         String country) {
      this.context = context;
      this.createdAt = createdAt;
      this.city = city;
      this.country = country;
    }

    @Override
    protected Boolean doInBackground(String... params) {
      InputStream in = null;
      try {
        boolean streamVideoToFBPage =
          Singleton.INSTANCE.getAppPrefs().getBoolean("StreamVideoToFacebookPage", false);
        boolean streamVideoToMyFBWall =
          Singleton.INSTANCE.getAppPrefs().getBoolean("StreamVideoToMyFacebookWall", false);
        boolean streamVideoToYTChannel =
          Singleton.INSTANCE.getAppPrefs().getBoolean("StreamVideoToYouTubeChannel", false);
        boolean streamVideoToMyYTChannel =
          Singleton.INSTANCE.getAppPrefs().getBoolean("StreamVideoToMyYouTubeChannel", false);
        String name = ParseUser.getCurrentUser().get("firstName") + " " +
                        ParseUser.getCurrentUser().get("lastName");
        String url = GO_LIVE_BASE_URL + ENDPOINT_NAME +
                       URLEncoder.encode(name, "UTF-8") + PARAM_SEPARATOR +
                       ENDPOINT_STREAM + ParseUser.getCurrentUser().getObjectId() + "_" +
                       createdAt +
                       PARAM_SEPARATOR + ENDPOINT_CITY + URLEncoder.encode(city, "UTF-8") +
                       PARAM_SEPARATOR + ENDPOINT_COUNTRY +
                       URLEncoder.encode(country, "UTF-8") + PARAM_SEPARATOR +
                       ENDPOINT_USER + ParseUser.getCurrentUser().getObjectId();
        if(streamVideoToYTChannel) {
          url += PARAM_SEPARATOR + ENDPOINT_YT_CELL411 + VALUE_YES;
        }
        if(streamVideoToMyYTChannel) {
          SharedPreferences prefs = Singleton.INSTANCE.getAppPrefs();
          String serverURL = prefs.getString("YTServerURL",
            context.getString(R.string.default_youtube_server_url));
          String streamName = prefs.getString("StreamName", null);
          serverURL = serverURL.replace("rtmp://", "");
          String strArr[] = serverURL.split("/");
          String appName = "live2";
          if(strArr.length == 2) {
            appName = strArr[1];
            serverURL = strArr[0];
          }
          url += PARAM_SEPARATOR + ENDPOINT_YT + VALUE_YES +
                   PARAM_SEPARATOR + ENDPOINT_YT_APP + appName +
                   PARAM_SEPARATOR + ENDPOINT_YT_KEY + streamName +
                   PARAM_SEPARATOR + ENDPOINT_YT_HOST + serverURL;
        }
        LogEvent.Log("GoLiveApiCall", "URL: " + url);
        URL url2 = new URL(url);
        URLConnection urlConnection = url2.openConnection();
        urlConnection.setConnectTimeout(30000);
        urlConnection.setReadTimeout(30000);
        urlConnection.setDoInput(true);
        in = new BufferedInputStream(urlConnection.getInputStream());
        BufferedReader reader = new BufferedReader(new InputStreamReader(in), 8);
        StringBuilder sb = new StringBuilder();
        String line = null;
        while((line = reader.readLine()) != null) {
          sb.append(line);
        }
        LogEvent.Log("GoLiveApiCall response: ", sb.toString());
        return true;
      } catch(final Exception e) {
        LogEvent.Log("GoLiveApiCall", "Exception: " + e.toString());
        return null;
      }
    }

    @Override
    protected void onPostExecute(Boolean result) {
      super.onPostExecute(result);
      MainActivity.setSpinnerVisible(false);
      if(Modules.isWeakReferenceValid()) {
        if(result == null) {
          LogEvent.Log(TAG, "result is null");
        } else if(!result) {
          LogEvent.Log(TAG, "result is false");
        } else {
          LogEvent.Log(TAG, "result is true");
        }
        Intent intent = new Intent(context, VideoStreamingActivity.class);
        context.startActivity(intent);
      }
    }
  }
}
