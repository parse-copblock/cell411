package cell411.activity;

import android.app.Service;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.ActionBar;

import com.parse.ParseCheater;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParseUser;
import com.parse.callback.LogInCallback;
import com.parse.callback.RequestPasswordResetCallback;
import com.parse.callback.SaveCallback;
import com.safearx.cell411.R;

import java.lang.ref.WeakReference;

import cell411.Singleton;

/**
 * Created by Sachin on 14-04-2016.
 */
public class ChangePasswordActivity extends BaseActivity implements View.OnClickListener {
  public static WeakReference<ChangePasswordActivity> weakRef;
  private EditText etEmail;
  private EditText etPassword;
  private ProgressBar pb;

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    switch(item.getItemId()) {
      case android.R.id.home:
        finish();
        return true;
      default:
        return super.onOptionsItemSelected(item);
    }
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_login);
    // Set up the action bar.
    final ActionBar actionBar = getSupportActionBar();
    actionBar.setDisplayHomeAsUpEnabled(true);
    weakRef = new WeakReference<>(this);
    Singleton.INSTANCE.setCurrentActivity(this);
    pb = (ProgressBar) findViewById(R.id.pb);
    etEmail = (EditText) findViewById(R.id.et_email);
    etPassword = (EditText) findViewById(R.id.et_password);
    Button btnLogin = (Button) findViewById(R.id.btn_login);
    TextView txtBtnForgotPassword = (TextView) findViewById(R.id.txt_btn_forgot_password);
    TextView txtBtnSignUp = (TextView) findViewById(R.id.txt_btn_sign_up);
    btnLogin.setOnClickListener(this);
    txtBtnForgotPassword.setOnClickListener(this);
    txtBtnSignUp.setOnClickListener(this);
  }

  @Override
  public void onClick(View v) {
    int id = v.getId();
    switch(id) {
      case R.id.btn_login:
        login();
        break;
      case R.id.txt_btn_forgot_password:
        showForgotPasswordDialog();
        break;
      case R.id.txt_btn_sign_up:
        Intent intentRegister = new Intent(ChangePasswordActivity.this, RegisterActivity.class);
        startActivity(intentRegister);
        finish();
        break;
    }
  }

  private void login() {
    String email = etEmail.getText().toString();
    String password = etPassword.getText().toString();
    if(email.isEmpty()) {
      getApplicationContext();
      Singleton.sendToast("Please enter your email");
    } else if(password.isEmpty()) {
      getApplicationContext();
      Singleton.sendToast("Please enter your password");
    } else {
      pb.setVisibility(View.VISIBLE);
      ParseUser.logInInBackground(email.toLowerCase().trim(), password, new LogInCallback() {
        public void done(ParseUser user, ParseException e) {
          if(e == null) {
            // Hooray! The user is logged in.
            ParseInstallation installation = ParseInstallation.getCurrentInstallation();
            installation.put("user", user);
            installation.saveInBackground(new SaveCallback() {
              @Override
              public void done(ParseException e) {
                pb.setVisibility(View.GONE);
                if(e == null) {
                  ParseCheater.setup();
                  Intent intentMain = new Intent(ChangePasswordActivity.this, MainActivity.class);
                  startActivity(intentMain);
                  finish();
                } else {
                  if(weakRef.get() != null && !weakRef.get().isFinishing()) {
                    Singleton.sendToast(e);
                  }
                }
              }
            });
          } else {
            pb.setVisibility(View.GONE);
            // Signup failed. Look at the ParseException to see what happened.
            if(weakRef.get() != null && !weakRef.get().isFinishing()) {
              Singleton.sendToast(e);
            }
          }
        }
      });
    }
  }

  private void showForgotPasswordDialog() {
    android.app.AlertDialog.Builder alert = new android.app.AlertDialog.Builder(this);
    alert.setTitle("Forgot Password?");
    alert.setMessage("Please enter your email address and tap on Submit");
    alert.setCancelable(false);
    LayoutInflater inflater = (LayoutInflater) getSystemService(Service.LAYOUT_INFLATER_SERVICE);
    View view = inflater.inflate(R.layout.dialog_forgot_password, null);
    final ProgressBar pb = (ProgressBar) view.findViewById(R.id.pb);
    final EditText etEmail = (EditText) view.findViewById(R.id.et_email);
    alert.setView(view);
    alert.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int arg1) {
        dialog.dismiss();
      }
    });
    alert.setPositiveButton("SUBMIT", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int which) {
      }
    });
    final android.app.AlertDialog dialog = alert.create();
    dialog.show();
    dialog.getButton(android.app.AlertDialog.BUTTON_POSITIVE).setOnClickListener(
      new View.OnClickListener() {
        @Override
        public void onClick(View v) {
          if(pb.getVisibility() != View.VISIBLE) {
            final String email = etEmail.getText().toString().trim();
            if(email.isEmpty()) {
              Singleton.sendToast("Please enter your email");
              return;
            }
            pb.setVisibility(View.VISIBLE);
            ParseUser.requestPasswordResetInBackground(email, new RequestPasswordResetCallback() {
              public void done(ParseException e) {
                pb.setVisibility(View.GONE);
                if(e == null) {
                  // An email was successfully sent with reset instructions.
                  if(weakRef.get() != null && !weakRef.get().isFinishing()) {
                    showAlertDialog();
                  }
                } else {
                  if(weakRef.get() != null && !weakRef.get().isFinishing()) {
                    Singleton.sendToast(e);
                  }
                }
                dialog.dismiss();
              }
            });
          }
        }
      });
  }

  private void showAlertDialog() {
    android.app.AlertDialog.Builder alert = new android.app.AlertDialog.Builder(this);
    alert.setMessage("An email has been sent with reset instructions, please check your email");
    alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int which) {
      }
    });
    final android.app.AlertDialog dialog = alert.create();
    dialog.show();
  }
}