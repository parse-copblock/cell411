package cell411.activity;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.URLSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.parse.ParseCheater;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.callback.FindCallback;
import com.parse.callback.SaveCallback;
import com.parse.callback.SignUpCallback;
import com.safearx.cell411.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

import cell411.Singleton;
import cell411.constants.LMA;
import cell411.constants.ParseKeys;
import cell411.constants.Prefs;
import cell411.methods.UtilityMethods;
import cell411.models.CountryInfo;
import cell411.utils.LogEvent;
import cell411.widgets.Spinner;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by Sachin on 14-04-2016.
 */
public class RegisterActivity extends AppCompatActivity implements View.OnClickListener {
  private static final String TAG = "RegisterActivity";
  private Spinner spinner;
  private EditText etEmail;
  private EditText etPassword;
  private EditText etFirstName;
  private EditText etLastName;
  private EditText etMobile;
  private android.widget.Spinner spCountryCode;
  private ArrayList<CountryInfo> list;

  @TargetApi(Build.VERSION_CODES.LOLLIPOP)
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_register);
    spinner = (Spinner) findViewById(R.id.spinner);
    etEmail = (EditText) findViewById(R.id.et_email);
    etPassword = (EditText) findViewById(R.id.et_password);
    etFirstName = (EditText) findViewById(R.id.et_first_name);
    etLastName = (EditText) findViewById(R.id.et_last_name);
    etMobile = (EditText) findViewById(R.id.et_mobile);
    TextView txtBtnSignUp = (TextView) findViewById(R.id.txt_btn_sign_up);
    TextView txtBtnTermsAndConditions =
      (TextView) findViewById(R.id.lbl_terms_and_conditions_part_1);
    String text = getString(R.string.t_n_c, getString(R.string.app_name),
      getString(R.string.btn_terms_and_conditions));
    setTextViewHTML(txtBtnTermsAndConditions, text);
    TextView txtBtnSignIn = (TextView) findViewById(R.id.txt_btn_sign_in);
    LinearLayout llBtnSignUpWithFb = (LinearLayout) findViewById(R.id.ll_btn_sign_up_with_fb);
    txtBtnSignUp.setOnClickListener(this);
    txtBtnTermsAndConditions.setOnClickListener(this);
    txtBtnSignIn.setOnClickListener(this);
    llBtnSignUpWithFb.setOnClickListener(this);
    findViewById(R.id.rl_separator).setVisibility(View.GONE);
    findViewById(R.id.ll_btn_sign_up_with_fb).setVisibility(View.GONE);
    list = new ArrayList<CountryInfo>();
    // FIXME:  I think this can be removed.
    UtilityMethods
      .initializeCountryCodeList(list, 1, getResources().getBoolean(R.bool.enable_publish));
    spCountryCode = (android.widget.Spinner) findViewById(R.id.sp_country_code);
    CountryListAdapter countryListAdapter =
      new CountryListAdapter(this, R.layout.cell_country, list);
    countryListAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
    spCountryCode.setAdapter(countryListAdapter);
    spCountryCode.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
      @Override
      public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
        CountryInfo countryInfo = (CountryInfo) spCountryCode.getSelectedItem();
        for(int i = 0; i < list.size(); i++) {
          list.get(i).selected = false;
        }
        countryInfo.selected = true;
        LogEvent.Log(TAG,
          "countryInfo: " + countryInfo.name + " (" + countryInfo.shortCode + ") + " +
            countryInfo.dialingCode);
        LogEvent.Log(TAG, "position: " + position);
      }

      @Override
      public void onNothingSelected(AdapterView<?> adapterView) {
      }
    });
    // FIXME: Do we need this?
    spCountryCode.setSelection(UtilityMethods.getDefaultCountryCodeIndex(list));
  }

  protected void setTextViewHTML(TextView text, String html) {
    CharSequence sequence = Html.fromHtml(html);
    SpannableStringBuilder strBuilder = new SpannableStringBuilder(sequence);
    URLSpan[] urls = strBuilder.getSpans(0, sequence.length(), URLSpan.class);
    for(URLSpan span : urls) {
      makeLinkClickable(strBuilder, span);
    }
    text.setText(strBuilder);
    text.setMovementMethod(LinkMovementMethod.getInstance());
  }

  protected void makeLinkClickable(SpannableStringBuilder strBuilder, final URLSpan span) {
    int start = strBuilder.getSpanStart(span);
    int end = strBuilder.getSpanEnd(span);
    int flags = strBuilder.getSpanFlags(span);
    ClickableSpan clickable = new ClickableSpan() {
      public void onClick(View view) {
        if(span.getURL().equals("terms")) {
          Intent intentWeb = new Intent(Intent.ACTION_VIEW);
          intentWeb.setData(Uri.parse(getString(R.string.terms_and_conditions_url)));
          startActivity(intentWeb);
        }
      }
    };
    strBuilder.setSpan(clickable, start, end, flags);
    strBuilder.removeSpan(span);
  }

  @Override
  public void onClick(View v) {
    int id = v.getId();
    switch(id) {
      case R.id.txt_btn_sign_up:
        signUp();
        break;
      case R.id.txt_btn_terms_and_conditions:
        Intent intentWeb = new Intent(Intent.ACTION_VIEW);
        intentWeb.setData(Uri.parse(getString(R.string.terms_and_conditions_url)));
        startActivity(intentWeb);
        break;
      case R.id.txt_btn_sign_in:
        Intent intentRegister = new Intent(RegisterActivity.this, LoginActivity.class);
        startActivity(intentRegister);
        finish();
        break;
      case R.id.ll_btn_sign_up_with_fb:
        break;
    }
  }

  private void signUp() {
    if(spinner.getVisibility() == View.VISIBLE) {
      return;
    }
    final String email = etEmail.getText().toString();
    final String password = etPassword.getText().toString();
    final String firstName = etFirstName.getText().toString();
    final String lastName = etLastName.getText().toString();
    String mobileNumber = etMobile.getText().toString();
    if(email.isEmpty()) {
      Singleton.sendToast(R.string.validation_email);
    } else if(!email.contains("@")) {
      Singleton.sendToast(R.string.validation_email_invalid);
    } else if(password.isEmpty()) {
      Singleton.sendToast(R.string.validation_password);
    } else if(firstName.isEmpty()) {
      Singleton.sendToast(R.string.validation_firstname);
    } else if(lastName.isEmpty()) {
      Singleton.sendToast(R.string.validation_lastname);
    } else if(mobileNumber.isEmpty()) {
      Singleton.sendToast(R.string.validation_mobile_number);
    } else {
      spinner.setVisibility(View.VISIBLE);
      // Check if the mobile number is not already registered
      ParseQuery<ParseUser> userParseQuery = ParseUser.getQuery();
      userParseQuery.whereEqualTo("mobileNumber",
        ((CountryInfo) spCountryCode.getSelectedItem()).dialingCode + mobileNumber.trim());
      final String finalMobileNumber = mobileNumber;
      userParseQuery.findInBackground(new FindCallback<ParseUser>() {
        @Override
        public void done(List<ParseUser> objects, ParseException e) {
          spinner.setVisibility(View.GONE);
          if(e == null) {
            if(objects == null || objects.size() == 0) { // no records found, hence
              // mobile number is not already registered
              ParseUser user = new ParseUser();
              user.setUsername(email.toLowerCase().trim());
              user.setPassword(password);
              user.put(ParseKeys.FIRST_NAME, firstName.trim());
              user.put(ParseKeys.LAST_NAME, lastName.trim());
              user.put(ParseKeys.MOBILE_NUMBER,
                ((CountryInfo) spCountryCode.getSelectedItem()).dialingCode +
                  finalMobileNumber.trim());
              user.put(ParseKeys.PATROL_MODE, 1);
              user.put(ParseKeys.NEW_PUBLIC_CELL_ALERT, 1);
              user.put(ParseKeys.CLIENT_FIRM_ID, 1);
              showEmailConfirmationDialog(user);
            } else {
              Singleton.sendToast(R.string.mobile_already_registered);
            }
          } else {
            Singleton.sendToast(e);
          }
        }
      });
    }
  }

  private void showEmailConfirmationDialog(final ParseUser user) {
    AlertDialog.Builder alert = new AlertDialog.Builder(this);
    alert.setMessage(getString(R.string.dialog_msg_is_this_your_correct_email, user.getUsername()));
    alert.setNegativeButton(getString(R.string.dialog_btn_no), new DialogInterface.
                                                                     OnClickListener() {
      @Override
      public void onClick(DialogInterface dialog, int which) {
      }
    });
    alert.setPositiveButton(R.string.dialog_btn_yes, new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialogInterface, int i) {
        signUp(user);
      }
    });
    AlertDialog dialog = alert.create();
    dialog.show();
  }

  private void signUp(final ParseUser user) {
    spinner.setVisibility(View.VISIBLE);
    ParseQuery<ParseUser> userParseQuery = ParseUser.getQuery();
    userParseQuery.whereEqualTo("username", user.getUsername());
    ParseQuery<ParseUser> userParseQuery2 = ParseUser.getQuery();
    userParseQuery2.whereEqualTo("email", user.getUsername());
    // Club all the queries into one master query
    List<ParseQuery<ParseUser>> queries = new ArrayList<>();
    queries.add(userParseQuery);
    queries.add(userParseQuery2);
    ParseQuery<ParseUser> mainQuery = ParseQuery.or(queries);
    mainQuery.findInBackground(new FindCallback<ParseUser>() {
      @Override
      public void done(List<ParseUser> list, ParseException e) {
        if(e == null) {
          if(list == null || list.size() == 0) {
            user.signUpInBackground(new SignUpCallback() {
              public void done(ParseException e) {
                spinner.setVisibility(View.GONE);
                if(e == null) {
                  String[] cellArr = getResources().getStringArray(R.array.default_cells);
                  for(int i = 0; i < cellArr.length; i++) {
                    ParseObject cellObject = new ParseObject(ParseKeys.CLASS_CELL);
                    cellObject.put(ParseKeys.CREATED_BY, ParseUser.getCurrentUser());
                    cellObject.put(ParseKeys.NAME, cellArr[i]);
                    cellObject.put("type", (i + 1));
                    cellObject.saveEventually();
                  }
                  Singleton.sendToast(R.string.account_created_successfully);
                  // Hooray! Let them use the app now.
                  ParseInstallation installation = ParseInstallation.getCurrentInstallation();
                  installation.put(ParseKeys.USER, ParseUser.getCurrentUser());
                  installation.saveInBackground(new SaveCallback() {
                    @Override
                    public void done(ParseException e) {
                      if(e == null) {
                        ParseCheater.setup();
                      }
                    }
                  });
                  Singleton.setLoggedIn( true ).
                                                                                                apply();
                  Intent intent = new Intent(RegisterActivity.this, PermissionActivity.class);
                  startActivity(intent);
                  finish();
                } else {
                  Singleton.sendToast(e);
                }
              }
            });
          } else {
            // show popup here
            spinner.setVisibility(View.GONE);
            showEmailAlreadyRegisteredAlert(user.getUsername());
          }
        } else {
          spinner.setVisibility(View.GONE);
          if(true) {
            Singleton.sendToast(e);
          }
        }
      }
    });
  }

  public void showEmailAlreadyRegisteredAlert(String email) {
    AlertDialog.Builder alert = new AlertDialog.Builder(this);
    alert.setMessage(getString(R.string.dialog_msg_email_already_registered, email));
    alert.setPositiveButton(R.string.dialog_btn_ok, new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int which) {
        dialog.dismiss();
      }
    });
    alert.create().show();
  }

  public void showUnfulfilledPrivilegeAlert(String message) {
    AlertDialog.Builder alert = new AlertDialog.Builder(this);
    alert.setMessage(message);
    alert.setPositiveButton(R.string.dialog_btn_ok, new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int which) {
        dialog.dismiss();
      }
    });
    alert.create().show();
  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);
  }

  private static class ApiCallRegister extends AsyncTask<String, Void, Boolean> {
    private JSONObject obj;
    private OkHttpClient client;
    private MediaType mediaType = MediaType.parse("application/json; charset=utf-8");

    private ApiCallRegister(final ParseUser user) {
      client = new OkHttpClient();
      obj = new JSONObject();
      try {
        obj.put(LMA.PARAM_USER_ID, (String) user.getObjectId());
        obj.put(LMA.PARAM_FIRST_NAME, (String) user.get(ParseKeys.FIRST_NAME));
        obj.put(LMA.PARAM_SUR_NAME, (String) user.get(ParseKeys.LAST_NAME));
        obj.put(LMA.PARAM_CONTACT_EMAIL, user.getUsername());
        obj.put(LMA.PARAM_CONTACT_MOBILE, (String) user.get(ParseKeys.MOBILE_NUMBER));
      } catch(JSONException e1) {
        e1.printStackTrace();
      }
    }

    @Override
    protected void onPreExecute() {
      super.onPreExecute();
    }

    @Override
    protected Boolean doInBackground(String... params) {
      LogEvent.Log(TAG, "URL: " + params[0]);
      RequestBody body = RequestBody.create(mediaType, obj.toString());
      Request request = new Request.Builder().url(params[0]).addHeader(LMA.PARAM_KEY,
        LMA.KEY_VALUE_REGISTRATION).post(body).build();
      try {
        Response response = client.newCall(request).execute();
        if(response != null) {
          LogEvent.Log(TAG, "response code: " + String.valueOf(response.code()));
          if(response.code() == HttpsURLConnection.HTTP_OK) {
            String responseJSON = response.body().string();
            LogEvent.Log(TAG, "response JSON: " + responseJSON);
            return true;
          } else {
            return false;
          }
        }
        return null;
      } catch(IOException e) {
        return null;
      }
            /*InputStream is = null;
            try {
                HttpClient client = new DefaultHttpClient();
                HttpPost httpPost = new HttpPost(params[0]);
                List<BasicNameValuePair> basicNameValuePairs = new ArrayList<>();
                basicNameValuePairs.add(new BasicNameValuePair(LMA.PARAM_FIRST_NAME, (String) user.get(ParseKeys.FIRST_NAME)));
                basicNameValuePairs.add(new BasicNameValuePair(LMA.PARAM_SUR_NAME, (String) user.get(ParseKeys.LAST_NAME)));
                basicNameValuePairs.add(new BasicNameValuePair(LMA.PARAM_CONTACT_EMAIL, user.getUsername()));
                basicNameValuePairs.add(new BasicNameValuePair(LMA.PARAM_CONTACT_MOBILE, (String) user.get(ParseKeys.MOBILE_NUMBER)));
                httpPost.addHeader(LMA.PARAM_KEY, LMA.KEY_VALUE_REGISTRATION);
                httpPost.setEntity(new UrlEncodedFormEntity(basicNameValuePairs));
                HttpResponse response = client.execute(httpPost);
                int status = response.getStatusLine().getStatusCode();
                LogEvent.Log(TAG, "status: " + status);
                HttpEntity entity = response.getEntity();
                is = entity.getContent();
                BufferedInputStream in = new BufferedInputStream(is);
                BufferedReader reader = new BufferedReader(
                        new InputStreamReader(in), 8);
                StringBuilder sb = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }
                String json2 = sb.toString();
                LogEvent.Log("json response: ", json2);
                JSONObject resultObject = new JSONObject(json2);
                if (resultObject == null) {
                    return null;
                } else {
                    return true;
                }
            } catch (JSONException e) {
                LogEvent.Log("mediaType Parser", "Error parsing data " + e.toString());
                return null;
            }
            catch (UnsupportedEncodingException e) {
                e.printStackTrace();
                return null;
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            } finally {
                try {
                    if (is != null)
                        is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }*/
    }

    @Override
    protected void onPostExecute(Boolean result) {
      super.onPostExecute(result);
      if(result == null) {
      } else if(!result) {
      } else {
      }
    }
  }

  private static class ItemViewHolder {
    TextView txtCountryName;
    TextView txtCountryCode;
    ImageView imgFlag;
    ImageView imgTick;
  }

  private class CountryListAdapter extends ArrayAdapter<CountryInfo> {
    private final ArrayList<CountryInfo> list;
    private int resourceId;
    private LayoutInflater inflator;

    public CountryListAdapter(Context context, int resourceId, ArrayList<CountryInfo> list) {
      super(context, resourceId, list);
      this.list = list;
      this.resourceId = resourceId;
      inflator = getLayoutInflater();
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
      ItemViewHolder holder = null;
      CountryInfo item = list.get(position);
      if(convertView == null) {
        holder = new ItemViewHolder();
        convertView = inflator.inflate(resourceId, null);
        holder.txtCountryName = (TextView) convertView.findViewById(R.id.txt_country_name);
        holder.imgFlag = (ImageView) convertView.findViewById(R.id.img_flag);
        holder.imgTick = (ImageView) convertView.findViewById(R.id.img_tick);
        convertView.setTag(holder);
      }
      holder = (ItemViewHolder) convertView.getTag();
      holder.txtCountryName.setText(item.name + " +" + item.dialingCode);
      holder.imgFlag.setImageResource(item.flagId);
      if(item.selected) {
        holder.imgTick.setVisibility(View.VISIBLE);
      } else {
        holder.imgTick.setVisibility(View.GONE);
      }
      return convertView;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
      ItemViewHolder holder = null;
      CountryInfo item = list.get(position);
      if(convertView == null) {
        holder = new ItemViewHolder();
        convertView = inflator.inflate(R.layout.cell_country_code, null);
        holder.txtCountryCode = (TextView) convertView.findViewById(R.id.txt_country_code);
        convertView.setTag(holder);
      }
      holder = (ItemViewHolder) convertView.getTag();
      holder.txtCountryCode.setText("+" + item.dialingCode);
      return convertView;
    }
  }
}
