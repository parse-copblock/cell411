package cell411.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import androidx.core.view.GestureDetectorCompat;

import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.callback.FunctionCallback;
import com.safearx.cell411.R;
import com.wowza.gocoder.sdk.api.WowzaGoCoder;
import com.wowza.gocoder.sdk.api.broadcast.WZBroadcastConfig;
import com.wowza.gocoder.sdk.api.configuration.WZMediaConfig;
import com.wowza.gocoder.sdk.api.configuration.WowzaConfig;
import com.wowza.gocoder.sdk.api.devices.WZAudioDevice;
import com.wowza.gocoder.sdk.api.devices.WZCamera;
import com.wowza.gocoder.sdk.api.devices.WZCameraView;
import com.wowza.gocoder.sdk.api.errors.WZStreamingError;
import com.wowza.gocoder.sdk.api.geometry.WZSize;
import com.wowza.gocoder.sdk.api.logging.WZLog;
import com.wowza.gocoder.sdk.api.mp4.WZMP4Writer;
import com.wowza.gocoder.sdk.api.status.WZStatus;
import com.wowza.gocoder.sdk.api.status.WZStatusCallback;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

import javax.net.ssl.HttpsURLConnection;

import cell411.Singleton;
import cell411.SingletonConfig;
import cell411.methods.Modules;
import cell411.utils.LogEvent;
import cell411.widgets.ControlButton;
import cell411.widgets.StatusView;

/**
 * Created by Sachin on 26-05-2016.
 */
public class VideoStreamingActivity extends GoCoderSDKActivity implements WZStatusCallback {
  public static WeakReference<VideoStreamingActivity> weakRef;

  static {
    TAG = VideoStreamingActivity.class.getSimpleName();
    REQUIRED_PERMISSIONS =
      new String[]{android.Manifest.permission.CAMERA, android.Manifest.permission.RECORD_AUDIO};
  }

  protected WZMP4Writer mMP4Writer = null;
  private ParseObject cell411AlertObject;
  private boolean isSaveVideoToLocalStorageEnabled;
  private int SCALE_MODES[] = {WZBroadcastConfig.FILL_VIEW, WZBroadcastConfig.RESIZE_TO_ASPECT};
  private int mScaleModeIndex = 0;
  private Context mContext;
  // UI controls
  private ControlButton mBtnBroadcast;
  private ControlButton mBtnSwitchCamera;
  private ControlButton mBtnTorch;
  private ControlButton mBtnMic;
  private TextView mTxtFrameSize;
  private StatusView mStatusView;
  private WZCameraView mGoCoderCameraView;
  // Gestures are used to toggle the focus modes
  private GestureDetectorCompat mGestureDetector;
  private boolean mDevicesInitialized = false;
  private boolean alertSent = false;
  private boolean statusChanged = false;
  private Handler handler;
  private Runnable runnable;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    requestWindowFeature(Window.FEATURE_NO_TITLE);
    setContentView(R.layout.activity_video_streaming);
    weakRef = new WeakReference<>(this);
    mContext = new ContextThemeWrapper(this, R.style.AppTheme);
    cell411AlertObject = Singleton.getCell411AlertObject();
    cell411AlertObject2 = Singleton.getCell411AlertObject();
    isSaveVideoToLocalStorageEnabled =
      Singleton.INSTANCE.getAppPrefs().getBoolean("SaveVideoToLocalStorage", false);
        /*if (Singleton.INSTANCE.getAppPrefs().getBoolean("StreamVideoToFacebookPage", false)) {
            mResultReceiver = new AddressResultReceiver(new Handler());
            startIntentService();
        }*/
    mBtnBroadcast = new ControlButton(this, R.id.ic_broadcast, false, false, R.drawable.ic_stop,
      R.drawable.ic_start);
    mBtnSwitchCamera = new ControlButton(this, R.id.ic_switch_camera, false);
    mBtnTorch = new ControlButton(this, R.id.ic_torch, false, false, R.drawable.ic_torch_on,
      R.drawable.ic_torch_off);
    mBtnMic = new ControlButton(this, R.id.ic_mic, false, true, R.drawable.ic_mic_on,
      R.drawable.ic_mic_off);
    mTxtFrameSize = (TextView) findViewById(R.id.txtFrameSize);
    mStatusView = (StatusView) findViewById(R.id.statusView);
    if(sGoCoder != null) {
      mGoCoderCameraView = (WZCameraView) findViewById(R.id.cameraPreview);
      sGoCoder.setCameraView(mGoCoderCameraView);
      mBroadcastConfig.set(sGoCoder.getDefaultBroadcastConfig());
      mGestureDetector = new GestureDetectorCompat(this, new FocusGestureListener());
      if(isSaveVideoToLocalStorageEnabled) {
        mMP4Writer = new WZMP4Writer();
        mBroadcastConfig.registerVideoSink(mMP4Writer);
        mBroadcastConfig.registerAudioSink(mMP4Writer);
      }
    } else {
      mStatusView.setError(WowzaGoCoder.getLastError());
    }
  }

  @Override
  protected void onResume() {
    super.onResume();
    Log.d(TAG, "onResume");
    if(mGoCoderCameraView != null && mPermissionsGranted) {
      mGoCoderCameraView.setCameraConfig(mBroadcastConfig);
      mGoCoderCameraView.setScaleMode(SCALE_MODES[mScaleModeIndex]);
      mGoCoderCameraView.startPreview();
      mTxtFrameSize.setText(mGoCoderCameraView.getFrameSize().toString());
      WZCamera activeCamera = mGoCoderCameraView.getCamera();
      if(activeCamera != null && activeCamera.hasCapability(WZCamera.FOCUS_MODE_CONTINUOUS)) {
        activeCamera.setFocusMode(WZCamera.FOCUS_MODE_CONTINUOUS);
      }
      updateUIControls();
      if(!mDevicesInitialized) {
        initGoCoderDevices();
      }
      if(mBroadcast == null) {
        return;
      }
      if(mBroadcast.getBroadcastStatus().isIdle()) {
        if(isSaveVideoToLocalStorageEnabled) {
          Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MOVIES);
          File outputFile = getOutputMediaFile();
          if(outputFile != null) {
            mMP4Writer.setFilePath(outputFile.toString());
          } else {
            mStatusView.setErrorMessage(getString(R.string.could_not_create_directory));
          }
        }
        WZStreamingError configError = startBroadcast();
        if(configError != null) {
          mStatusView.setError(configError);
        }
      }
    }
  }

  protected void initGoCoderDevices() {
    if(sGoCoder != null && mPermissionsGranted) {
      // Initialize the camera preview
      if(mGoCoderCameraView != null) {
        WZCamera availableCameras[] = mGoCoderCameraView.getCameras();
        // Ensure we can access to at least one camera
        if(availableCameras.length > 0) {
          // Set the video broadcaster in the broadcast config
          mBroadcastConfig.setVideoBroadcaster(mGoCoderCameraView);
        } else {
          mStatusView.setErrorMessage(
            "Could not detect or gain access to any cameras on this device");
          mBroadcastConfig.setVideoEnabled(false);
        }
      } else {
        mBroadcastConfig.setVideoEnabled(false);
      }
      // Set the audio broadcaster in the broadcast config
      mBroadcastConfig.setAudioBroadcaster(new WZAudioDevice());
      mDevicesInitialized = true;
    }
  }

  @Override
  protected void onPause() {
    super.onPause();
    Log.d(TAG, "onPause");
    if(mGoCoderCameraView != null) {
      mGoCoderCameraView.stopPreview();
    }
  }

  /**
   * Click handler for the broadcast button
   */
  public void onToggleBroadcast(View v) {
    if(mBroadcast == null) {
      return;
    }
    if(mBroadcast.getBroadcastStatus().isIdle()) {
      if(isSaveVideoToLocalStorageEnabled) {
        Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MOVIES);
        File outputFile = getOutputMediaFile();
        if(outputFile != null) {
          mMP4Writer.setFilePath(outputFile.toString());
        } else {
          mStatusView.setErrorMessage(getString(R.string.could_not_create_directory));
        }
      }
      WZStreamingError configError = startBroadcast();
      if(configError != null) {
        mStatusView.setError(configError);
      }
    } else {
      endBroadcast();
      finish();
    }
  }

  /**
   * Create a File for saving an image or video
   */
  private File getOutputMediaFile() {
    // To be safe, you should check that the SDCard is mounted
    // using Environment.getExternalStorageState() before doing this.
    //
    // This location works best if you want the created images to be shared
    // between applications and persist after your app has been uninstalled.
    File mediaStorageDir =
      new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MOVIES),
        getString(R.string.app_name));
    // Create the storage directory if it does not exist
    if(!mediaStorageDir.exists()) {
      if(!mediaStorageDir.mkdirs()) {
        WZLog.warn(TAG, "failed to create the directory in which to store the MP4");
        return null;
      }
    }
    // Create a media file name
    String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
    return new File(mediaStorageDir.getPath() + File.separator + "cell411_" + timeStamp + ".mp4");
  }

  /**
   * Click handler for the switch camera button
   */
  public void onSwitchCamera(View v) {
    if(mGoCoderCameraView == null) {
      return;
    }
    WZCamera newCamera = mGoCoderCameraView.switchCamera();
    mBtnTorch.setStateOn(false);
    mBtnTorch.setEnabled(newCamera.hasCapability(WZCamera.TORCH));
  }

  /**
   * Click handler for the torch/flashlight button
   */
  public void onToggleTorch(View v) {
    if(mGoCoderCameraView == null) {
      return;
    }
    WZCamera activeCamera = mGoCoderCameraView.getCamera();
    activeCamera.setTorchOn(mBtnTorch.toggleState());
  }

  /**
   * Click handler for the mic/mute button
   */
  public void onToggleMute(View v) {
    if(sGoCoder == null) {
      return;
    }
    sGoCoder.getAudioDevice().setMuted(!mBtnMic.toggleState());
  }

  /**
   * Click handler for the settings button
   */
  public void onSettings(View v) {
    if(mBroadcastConfig == null) {
      return;
    }
    WZMediaConfig configs[] = getVideoConfigs(mGoCoderCameraView);
    Intent intent = new Intent(this, ConfigPrefsActivity.class);
    intent.putExtra("header_resource", ConfigPrefsActivity.ALL_PREFS);
    intent.putExtra("configs", configs);
    startActivity(intent);
  }

  /**
   * Click handler for the scale mode button
   */
  public void onScaleMode(View v) {
    if(mGoCoderCameraView == null) {
      return;
    }
    mScaleModeIndex = ++mScaleModeIndex % SCALE_MODES.length;
    mGoCoderCameraView.setScaleMode(SCALE_MODES[mScaleModeIndex]);
    Button btn = (Button) findViewById(R.id.btnScale);
    switch(SCALE_MODES[mScaleModeIndex]) {
      case WowzaConfig.FILL_FRAME:
        btn.setText(R.string.fill_mode);
        break;
      case WowzaConfig.CROP_TO_FRAME:
        btn.setText(R.string.crop_mode);
        break;
    }
  }

  @Override
  public boolean onTouchEvent(MotionEvent event) {
    mGestureDetector.onTouchEvent(event);
    return super.onTouchEvent(event);
  }

  /**
   * WZStatusCallback interface methods
   */
  @Override
  public void onWZStatus(final WZStatus goCoderStatus) {
    new Handler(Looper.getMainLooper()).post(new Runnable() {
      @Override
      public void run() {
        mStatusView.setState(goCoderStatus.getState());
        updateUIControls();
        if(!alertSent) {
          alertSent = true;
          handler = new Handler();
          runnable = new Runnable() {
            @Override
            public void run() {
                            /*String name = ParseUser.getCurrentUser().get("firstName")
                                    + " "
                                    + ParseUser.getCurrentUser().get("lastName");
                            Singleton.cell411AlertObject.put("issuedBy", ParseUser.getCurrentUser());
                            Singleton.cell411AlertObject.put("issuerFirstName", name);
                            Singleton.cell411AlertObject.put("issuerId", ParseUser.getCurrentUser().getObjectId());
                            Singleton.cell411AlertObject.put("alertType", "Video");
                            Singleton.cell411AlertObject.put("additionalNote", "");
                            Singleton.cell411AlertObject.put("status", "LIVE");
                            if (Singleton.parseUsersList != null) {
                                Singleton.cell411AlertObject.put("targetMembers", Singleton.parseUsersList);
                            }
                            Singleton.cell411AlertObject.put("location", new ParseGeoPoint(Singleton.INSTANCE.getLatitude(),
                                    Singleton.INSTANCE.getLongitude()));
                            if (Singleton.isGlobal)
                                Singleton.cell411AlertObject.put("isGlobal", 1);
                            Singleton.cell411AlertObject.saveInBackground(new SaveCallback() {
                                @Override
                                public void done(ParseException e) {
                                    if (e == null) {
                                        statusChanged = true;
                                        if (Singleton.push != null) {
                                            Singleton.push.sendInBackground(new SendCallback() {
                                                @Override
                                                public void done(ParseException e) {
                                                    if (weakRef.get() != null && !weakRef.get().isFinishing()) {
                                                        if (e == null) {
                                                            LogEvent.Log(TAG, Singleton.toastMessage);
                                                        } else {
                                                            UtilityMethods.displayToast(getApplicationContext(), e);
                                                        }
                                                    }
                                                }
                                            });
                                        }
                                        // Check if the feature is enabled in the build configuration
                                        boolean isSecurityGuardsAlertsEnabled = getResources()
                                                .getBoolean(R.bool.is_security_guards_alerts_enabled);
                                        if (isSecurityGuardsAlertsEnabled) {
                                            // Check if user has enabled security guards to receive the alerts
                                            boolean isIncludeSecurityGuardsEnabled = Singleton.INSTANCE.getAppPrefs()
                                                    .getBoolean(Prefs.INCLUDE_SECURITY_GUARDS, Defaults.INCLUDE_SECURITY_GUARDS);
                                            if (isIncludeSecurityGuardsEnabled) {
                                                new SendAlertToSecurityGuardsApiCall(VideoStreamingActivity.this,
                                                        getResources().getInteger(R.integer.client_firm_id),
                                                        cell411AlertObject.getObjectId(),
                                                        ParseUser.getCurrentUser().getObjectId()).execute();
                                            }
                                        }
                                    } else {
                                        if (weakRef.get() != null && !weakRef.get().isFinishing()) {
                                            UtilityMethods.displayToast(getApplicationContext(), e);
                                        }
                                    }
                                }
                            });*/
              ParseCloud.callFunctionInBackground("sendAlertV3", Singleton.getParams(),
                new FunctionCallback<String>() {
                  public void done(String result, ParseException e) {
                    if(e == null) {
                      statusChanged = true;
                      LogEvent.Log(TAG, "result: " + result);
                      JSONObject objResult = null;
                      int targetMembersCount = 0;
                      int targetNAUMembersCount = 0;
                      try {
                        objResult = new JSONObject(result);
                        targetMembersCount = objResult.getInt("targetMembersCount");
                        targetNAUMembersCount = objResult.getInt("targetNAUMembersCount");
                      } catch(JSONException jsonException) {
                        e.printStackTrace();
                      }
                      int totalUsers = targetMembersCount + targetNAUMembersCount;
                      if(Modules.isWeakReferenceValid()) {
                        if(totalUsers >= 1) {
                          if(totalUsers > 1) {
                            getApplicationContext();
                            Singleton.sendToast(getString(R.string.toast_alert_sent, totalUsers));
                          } else {
                            getApplicationContext();
                            Singleton.sendToast(R.string.toast_alert_sent_to_1_user);
                          }
                        } else {
                          getApplicationContext();
                          Singleton.sendToast(R.string.no_members_in_the_selected_cell);
                        }
                      }
                    } else {
                      if(Modules.isWeakReferenceValid()) {
                        Singleton.sendToast(e);
                      }
                    }
                  }
                });
            }
          };
          handler.postDelayed(runnable, 10000);
        }
      }
    });
  }

  @Override
  public void onWZError(final WZStatus goCoderStatus) {
    new Handler(Looper.getMainLooper()).post(new Runnable() {
      @Override
      public void run() {
        mStatusView.setError(goCoderStatus.getLastError());
        updateUIControls();
      }
    });
  }

  /**
   * Update the state of the UI controls
   */
  private void updateUIControls() {
    boolean disableControls = (mBroadcast == null ||
                                 !(mBroadcast.getBroadcastStatus().isIdle() ||
                                     mBroadcast.getBroadcastStatus().isRunning()));
    if(disableControls) {
      mBtnBroadcast.setEnabled(false);
      mBtnSwitchCamera.setEnabled(false);
      mBtnTorch.setEnabled(false);
      mBtnMic.setVisible(false);
      //mBtnSettings.setEnabled(false);
    } else {
      boolean isStreaming = mBroadcast.getBroadcastStatus().isRunning();
      mBtnBroadcast.setStateOn(isStreaming);
      mBtnBroadcast.setEnabled(true);
      //mBtnSettings.setEnabled(!isStreaming);
      mBtnSwitchCamera.setEnabled(
        mBroadcastConfig.isVideoEnabled() && mGoCoderCameraView.isSwitchCameraAvailable());
      WZCamera activeCamera = mGoCoderCameraView.getCamera();
      mBtnTorch.setEnabled(mBroadcastConfig.isVideoEnabled() && activeCamera != null &&
                             activeCamera.hasCapability(WZCamera.TORCH));
      mBtnMic.setStateOn(!sGoCoder.getAudioDevice().isMuted());
      mBtnMic.setVisible(isStreaming && mBroadcastConfig.isAudioEnabled());
    }
  }

  /**
   * Build an array of WZMediaConfigs from the frame sizes supported by the active camera
   *
   * @param goCoderCameraView the camera view
   * @return an array of WZMediaConfigs from the frame sizes supported by the active camera
   */
  private WZMediaConfig[] getVideoConfigs(WZCameraView goCoderCameraView) {
    WZMediaConfig configs[] = WowzaConfig.PRESET_CONFIGS;
    if(goCoderCameraView != null && goCoderCameraView.getCamera() != null) {
      WZMediaConfig cameraConfigs[] = goCoderCameraView.getCamera().getSupportedConfigs();
      Arrays.sort(cameraConfigs);
      configs = cameraConfigs;
    }
    return configs;
  }

  @Override
  public void onDestroy() {
    super.onDestroy();
    if(cell411AlertObject != null && statusChanged) {
      cell411AlertObject.put("status", "VOD");
      cell411AlertObject.saveEventually();
    } else {
      if(handler != null && runnable != null) {
        handler.removeCallbacks(runnable);
      }
      if(cell411AlertObject != null) {
        Singleton.getCell411AlertObject().deleteEventually();
      }
    }
    if(isSaveVideoToLocalStorageEnabled) {
      galleryAddVideo();
    }
  }

  // add picture to the gallery
  private void galleryAddVideo() {
    Intent mediaScanIntent = new Intent("android.intent.action.MEDIA_SCANNER_SCAN_FILE");
    Uri contentUri = Uri.fromFile(new File(mMP4Writer.getFilePath()));
    mediaScanIntent.setData(contentUri);
    sendBroadcast(mediaScanIntent);
  }

  private static class SendAlertToSecurityGuardsApiCall extends AsyncTask<String, Void, Boolean> {
    private Activity context;
    private String message;
    private String cell411AlertId;
    private String issuerId;
    private int clientFirmId;
    private int isLive;

    @Override
    protected Boolean doInBackground(String... params) {
      String urlBroadcastAlert = SingletonConfig.BROADCAST_ALERT;
      LogEvent.Log("SendAlertToSecurityGuardsApiCall", "URL: " + urlBroadcastAlert);
      try {
        JSONObject jsonObject = new JSONObject();
        jsonObject.accumulate("cell411AlertId", cell411AlertId);
        jsonObject.accumulate("issuerId", issuerId);
        jsonObject.accumulate("clientFirmId", clientFirmId);
        jsonObject.accumulate("isLive", isLive);
        String json = jsonObject.toString();
        LogEvent.Log(TAG, "json: " + json);
        URL url = new URL(urlBroadcastAlert);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setReadTimeout(context.getResources().getInteger(R.integer.maximum_timeout_to_server));
        conn.setConnectTimeout(
          context.getResources().getInteger(R.integer.maximum_timeout_to_server));
        conn.setRequestMethod("POST");
        conn.setDoInput(true);
        conn.setDoOutput(true);
        OutputStream os = conn.getOutputStream();
        byte[] outputBytes = json.getBytes("UTF-8");
        os.write(outputBytes);
        int responseCode = conn.getResponseCode();
        LogEvent.Log(TAG, "responseCode: " + responseCode);
        String response = "";
        if(responseCode == HttpsURLConnection.HTTP_OK) {
          LogEvent.Log(TAG, "HTTP_OK");
          String line;
          BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
          while((line = br.readLine()) != null) {
            response += line;
          }
        } else {
          LogEvent.Log(TAG, "HTTP_NOT_OK");
          response = "";
        }
        LogEvent.Log("json response: ", response);
        if(response.isEmpty()) {
          return null;
        } else {
          JSONObject jObj = new JSONObject(response);
          String resType = jObj.getString("res_type");
          if(resType.equalsIgnoreCase("Error")) {
            message = jObj.getString("msg");
            LogEvent.Log(TAG, "Error:" + jObj.getString("msg"));
            return false;
          } else {
            return true;
          }
        }
      } catch(Exception e) {
        LogEvent.Log("SendAlertToSecurityGuardsApiCall", "Exception: " + e.toString());
        return null;
      }
    }

    @Override
    protected void onPostExecute(Boolean result) {
      super.onPostExecute(result);
      if(Modules.isWeakReferenceValid()) {
        if(result == null) {
          LogEvent.Log(TAG, "result is null, message: " + message);
        } else if(!result) {
          LogEvent.Log(TAG, "result is false, message: " + message);
        } else {
          context.getApplicationContext();
          Singleton.sendToast(R.string.video_sent_to_security_guards);
        }
      }
    }
  }

  class FocusGestureListener extends GestureDetector.SimpleOnGestureListener {
    @Override
    public boolean onDown(MotionEvent event) {
      return true;
    }

    @Override
    public boolean onDoubleTap(MotionEvent event) {
      if(mGoCoderCameraView != null) {
        WZCamera activeCamera = mGoCoderCameraView.getCamera();
        if(activeCamera != null && activeCamera.hasCapability(WZCamera.FOCUS_MODE_CONTINUOUS)) {
          if(activeCamera.getFocusMode() != WZCamera.FOCUS_MODE_CONTINUOUS) {
            activeCamera.setFocusMode(WZCamera.FOCUS_MODE_CONTINUOUS);
            Singleton.sendToast(R.string.focus_on);
          } else {
            activeCamera.setFocusMode(WZCamera.FOCUS_MODE_OFF);
            Singleton.sendToast(R.string.focus_off);
          }
        }
      }
      return true;
    }

    @Override
    public boolean onSingleTapConfirmed(MotionEvent event) {
      if(mGoCoderCameraView != null) {
        WZCamera activeCamera = mGoCoderCameraView.getCamera();
        if(activeCamera != null && activeCamera.hasCapability(WZCamera.FOCUS_MODE_AUTO)) {
          DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
          WZSize previewScreenSize = mGoCoderCameraView.getScreenSize();
          WZSize previewFrameSize = mGoCoderCameraView.getFrameSize();
          int previewScreenLeft =
            Math.round((float) (displayMetrics.widthPixels - previewScreenSize.width) / 2f);
          int previewScreenTop =
            Math.round((float) (displayMetrics.heightPixels - previewScreenSize.height) / 2f);
          float previewScreenX = event.getX() - previewScreenLeft;
          float previewScreenY = event.getY() - previewScreenTop;
          if(previewScreenX < 0 || previewScreenX > previewScreenSize.width ||
               previewScreenY < 0 || previewScreenY > previewScreenSize.getHeight()) {
            return true;
          }
          float relX = (previewScreenX / (float) previewScreenSize.width) *
                         (float) previewFrameSize.getWidth();
          float relY = (previewScreenY / (float) previewScreenSize.height) *
                         (float) previewFrameSize.getHeight();
          Singleton.sendToast(
            getString(R.string.auto_focus_at) + " (" + Math.round(relX) + "," + Math.round(relY) +
              ")");
          activeCamera.setFocusPoint(relX, relY, 25);
        }
      }
      return true;
    }
  }
}
