package cell411.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.callback.FunctionCallback;
import com.safearx.cell411.R;

import java.lang.ref.WeakReference;
import java.util.HashMap;

import cell411.Singleton;
import cell411.utils.LogEvent;
import cell411.widgets.Spinner;

/**
 * Created by Sachin on 28-03-2017.
 */
public class BroadcastMessageActivity extends BaseActivity {
  public static WeakReference<BroadcastMessageActivity> weakRef;
  private final String TAG = "BroadcastMessageActivity.java";
  private Spinner spinner;
  private EditText etMessage;

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    switch(item.getItemId()) {
      case android.R.id.home:
        finish();
        return true;
      default:
        return super.onOptionsItemSelected(item);
    }
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_broadcast_message);
    weakRef = new WeakReference<>(this);
    getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    spinner = (Spinner) findViewById(R.id.spinner);
    etMessage = (EditText) findViewById(R.id.et_message);
    TextView txtSubmit = (TextView) findViewById(R.id.txt_btn_submit);
    txtSubmit.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        if(spinner.getVisibility() != View.VISIBLE) {
          if(isValidInput()) {
            showBroadcastConfirmationDialog();
          }
        }
      }
    });
  }

  private void showBroadcastConfirmationDialog() {
    AlertDialog.Builder alert = new AlertDialog.Builder(this);
    if(getResources().getBoolean(R.bool.enable_publish)) {
      alert.setMessage(
        "Are you sure you want to broadcast this message, it will be sent to the real app users?");
    } else {
      alert.setMessage(
        "Are you sure you want to broadcast this message, it will be sent to all the testers?");
    }
    alert.setNegativeButton(getString(R.string.dialog_btn_no), new DialogInterface.
                                                                     OnClickListener() {
      @Override
      public void onClick(DialogInterface dialog, int which) {
      }
    });
    alert.setPositiveButton(R.string.dialog_btn_yes, new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialogInterface, int i) {
        broadcastMessage();
      }
    });
    AlertDialog dialog = alert.create();
    dialog.show();
  }

  private boolean isValidInput() {
    String message = etMessage.getText().toString().trim();
    if(message.isEmpty()) {
      getApplicationContext();
      Singleton.sendToast("Please enter message");
      spinner.setVisibility(View.GONE);
      etMessage.setEnabled(true);
      return false;
    }
    return true;
  }

  private void broadcastMessage() {
    spinner.setVisibility(View.VISIBLE);
    etMessage.setEnabled(false);
    String message = etMessage.getText().toString().trim();
    HashMap<String, Object> params = new HashMap<>();
    params.put("message", message);
    params.put("clientFirmId", 1);
    params.put("isLive", getResources().getBoolean(R.bool.enable_publish));
    params.put("languageCode", getString(R.string.language_code));
    params.put("platform", "android");
    ParseCloud.callFunctionInBackground("broadcastMessage", params, new FunctionCallback<String>() {
      public void done(String result, ParseException e) {
        spinner.setVisibility(View.GONE);
        etMessage.setEnabled(true);
        if(e == null) {
          // Email sent successfully
          LogEvent.Log(TAG, "Message sent successfully");
          getApplicationContext();
          Singleton.sendToast("Message sent");
          etMessage.setText("");
        } else {
          LogEvent.Log(TAG, "Something went wrong: " + e.getMessage());
          Singleton.sendToast(e);
        }
      }
    });
  }
}
