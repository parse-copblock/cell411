package cell411.activity;

import android.app.AlertDialog;
import android.app.Service;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;

import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.callback.FindCallback;
import com.parse.callback.SaveCallback;
import com.safearx.cell411.R;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

import cell411.Singleton;
import cell411.constants.Constants;
import cell411.constants.ParseKeys;
import cell411.methods.UtilityMethods;
import cell411.models.CountryInfo;
import cell411.services.FetchAddressIntentService;
import cell411.utils.ImagePicker;
import cell411.utils.LogEvent;
import cell411.widgets.BlinkingRedSymbol;
import cell411.widgets.Spinner;

import static cell411.Singleton.sendToast;
import static cell411.SingletonConfig.BASE_URL;
import static cell411.SingletonConfig.PARAM_IMAGE_TYPE;
import static cell411.SingletonConfig.PARAM_SEPARATOR;
import static cell411.SingletonConfig.PARAM_USER_ID;
import static cell411.SingletonImageFactory.getCroppedBitmap;

/**
 * Created by Sachin on 19-04-2016.
 */
public class ProfileViewActivity extends BaseActivity implements View.OnClickListener {
  private static final String TAG = "ProfileViewActivity";
  public static WeakReference<ProfileViewActivity> weakRef;
  public static String ENDPOINT_UPDATE_PIC = "/update_pic.php?";
  private AddressResultReceiver mResultReceiver;
  private TextView txtCityName;
  private ImagePicker imagePicker;
  private Spinner spinner;
  private ImageView imgUser;
  private ImageView imgEdit;
  private TextView txtName;
  private TextView txtEmail;
  private TextView txtPhone;
  private TextView txtPhoneVerification;
  private TextView txtBloodGroup;
  private TextView txtEmergencyContactName;
  private TextView txtEmergencyContactNumber;
  private TextView txtAllergies;
  private TextView txtOtherMedicalConditions;
  private boolean isPhoneVerificationEnabled;
  private TextView txtProfileCompleteness;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_profile_view);
    // Set up the action bar.
    final ActionBar actionBar = getSupportActionBar();
    assert actionBar != null;
    actionBar.setDisplayHomeAsUpEnabled(true);
    weakRef = new WeakReference<>(this);
    Singleton.INSTANCE.setCurrentActivity(this);
    initialize();
    updateUI();
    setClickEvents();
    mResultReceiver = new AddressResultReceiver(new Handler());
    startIntentService();
  }

  private void initialize() {
    spinner = findViewById(R.id.spinner);
    imgUser = findViewById(R.id.img_user);
    imgEdit = findViewById(R.id.img_edit);
    txtName = findViewById(R.id.txt_name);
    txtEmail = findViewById(R.id.txt_email);
    txtCityName = findViewById(R.id.txt_city_name);
    txtPhone = findViewById(R.id.txt_phone);
    txtPhoneVerification = findViewById(R.id.txt_phone_verification);
    txtProfileCompleteness = findViewById(R.id.txt_profile_completeness);
    txtBloodGroup = findViewById(R.id.txt_blood_group);
    txtEmergencyContactName = findViewById(R.id.txt_emergency_contact_name);
    txtEmergencyContactNumber = findViewById(R.id.txt_emergency_contact_number);
    txtAllergies = findViewById(R.id.txt_allergies);
    txtOtherMedicalConditions = findViewById(R.id.txt_other_medical_conditions);
    boolean isCustomPicEnabled = getResources().getBoolean(R.bool.is_custom_pic_enabled);
    if(!isCustomPicEnabled) {
      imgEdit.setVisibility(View.GONE);
    }
  }

  private void updateUI() {
    Singleton.INSTANCE.setImage(imgUser);
    ParseUser user = ParseUser.getCurrentUser();
    txtName.setText(getFullName(user));
    if(user.getUsername().contains("@")) {
      txtEmail.setText(user.getUsername());
    } else if(user.get("email") != null && !user.get("email").toString().isEmpty()) {
      txtEmail.setText(user.get("email").toString());
    } else {
      txtEmail.setVisibility(View.GONE);
    }
    TextView txtPhoneNot = findViewById(R.id.txt_not);
    ImageView imgVerified = findViewById(R.id.img_verified);
    BlinkingRedSymbol blinkingRedSymbol = (BlinkingRedSymbol) findViewById(R.id.brs);
    TextView txtBtnAddPhone = findViewById(R.id.txt_btn_add_phone);
    txtPhoneNot.setOnClickListener(this);
    blinkingRedSymbol.setOnClickListener(this);
    txtBtnAddPhone.setOnClickListener(this);
    if(user.get("mobileNumber") == null || user.get("mobileNumber").toString().isEmpty()) {
      txtPhone.setText(R.string.not_available);
      // If phone number is not available hide the verification button
      txtPhoneNot.setVisibility(View.GONE);
      blinkingRedSymbol.setVisibility(View.GONE);
      imgVerified.setVisibility(View.GONE);
      txtBtnAddPhone.setVisibility(View.VISIBLE);
    } else {
      txtPhone.setText((String) user.get("mobileNumber"));
      txtBtnAddPhone.setVisibility(View.GONE);
    }
    txtPhoneNot.setVisibility(View.GONE);
    blinkingRedSymbol.setVisibility(View.GONE);
    imgVerified.setVisibility(View.GONE);
    String bloodType = (String) user.get("bloodType");
    if(bloodType != null && !bloodType.isEmpty() && !bloodType.equals("null")) {
      txtBloodGroup.setText(bloodType);
    } else {
      txtBloodGroup.setText(R.string.not_available);
    }
    String emergencyContactName = (String) user.get("emergencyContactName");
    if(emergencyContactName != null && !emergencyContactName.isEmpty() &&
         !emergencyContactName.equals("null")) {
      txtEmergencyContactName.setText((String) user.get("emergencyContactName"));
    }
    String emergencyContactNumber = (String) user.get("emergencyContactNumber");
    if(emergencyContactNumber != null && !emergencyContactNumber.isEmpty() &&
         !emergencyContactNumber.equals("null")) {
      txtEmergencyContactNumber.setText((String) user.get("emergencyContactNumber"));
    }
    String allergies = (String) user.get("allergies");
    if(allergies != null && !allergies.isEmpty() && !allergies.equals("null")) {
      txtAllergies.setText((String) user.get("allergies"));
    }
    String otherMedicalConditions = (String) user.get("otherMedicalConditions");
    if(otherMedicalConditions != null && !otherMedicalConditions.isEmpty() &&
         !otherMedicalConditions.equals("null")) {
      txtOtherMedicalConditions.setText((String) user.get("otherMedicalConditions"));
    }
    txtProfileCompleteness.setText(getProfileCompletePercentage(user));
  }

  private String getFullName(ParseUser user) {
    return (String) user.get("firstName") + " " + (String) user.get("lastName");
  }

  private String getProfileCompletePercentage(ParseUser user) {
    int percentage = 0;
    if(user.getString("firstName") != null && !user.getString("firstName").isEmpty()) {
      percentage += 10;
    }
    if(user.getString("lastName") != null && !user.getString("lastName").isEmpty()) {
      percentage += 10;
    }
    if(user.getString("mobileNumber") != null && !user.getString("mobileNumber").isEmpty() &&
         user.getBoolean("phoneVerified")) {
      percentage += 10;
    }
    if((user.get("email") != null && !user.get("email").toString().isEmpty()) ||
         user.getUsername().contains("@")) {
      percentage += 10;
    }
    if(user.getString(ParseKeys.BLOOD_TYPE) != null &&
         !user.getString(ParseKeys.BLOOD_TYPE).isEmpty()) {
      percentage += 10;
    }
    if(user.getString("emergencyContactName") != null &&
         !user.getString("emergencyContactName").isEmpty()) {
      percentage += 10;
    }
    if(user.getString("emergencyContactNumber") != null &&
         !user.getString("emergencyContactNumber").isEmpty()) {
      percentage += 10;
    }
    if(user.getString("bloodType") != null && !user.getString("bloodType").isEmpty()) {
      percentage += 10;
    }
    if(user.getString("allergies") != null && !user.getString("allergies").isEmpty()) {
      percentage += 10;
    }
    if(user.getString("otherMedicalConditions") != null &&
         !user.getString("otherMedicalConditions").isEmpty()) {
      percentage += 10;
    }
    return getString(R.string.profile_setup, percentage);
  }

  private void showAddPhoneDialog() {
    android.app.AlertDialog.Builder alert = new android.app.AlertDialog.Builder(this);
    alert.setTitle(R.string.dialog_title_add_phone);
    alert.setMessage(R.string.dialog_message_add_phone);
    alert.setCancelable(false);
    LayoutInflater inflater = (LayoutInflater) getSystemService(Service.LAYOUT_INFLATER_SERVICE);
    View view = inflater.inflate(R.layout.dialog_add_phone, null);
    final ProgressBar pb = (ProgressBar) view.findViewById(R.id.pb);
    final EditText etPhone = (EditText) view.findViewById(R.id.et_phone);
    final ArrayList<CountryInfo> list = new ArrayList<CountryInfo>();
    UtilityMethods.initializeCountryCodeList(list,
      1,
      getResources().getBoolean(R.bool.enable_publish));
    final android.widget.Spinner spCountryCode =
      (android.widget.Spinner) view.findViewById(R.id.sp_country_code);
    CountryListAdapter countryListAdapter =
      new CountryListAdapter(this, R.layout.cell_country, list);
    countryListAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
    spCountryCode.setAdapter(countryListAdapter);
    spCountryCode.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
      @Override
      public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
        CountryInfo countryInfo = (CountryInfo) spCountryCode.getSelectedItem();
        for(int i = 0; i < list.size(); i++) {
          list.get(i).selected = false;
        }
        countryInfo.selected = true;
        LogEvent.Log(TAG,
          "countryInfo: " + countryInfo.name + " (" + countryInfo.shortCode + ") + " +
            countryInfo.dialingCode);
        LogEvent.Log(TAG, "position: " + position);
      }

      @Override
      public void onNothingSelected(AdapterView<?> adapterView) {
      }
    });
    spCountryCode.setSelection(UtilityMethods.getDefaultCountryCodeIndex(list));
    alert.setView(view);
    alert.setNegativeButton(R.string.dialog_btn_cancel, new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int arg1) {
        dialog.dismiss();
      }
    });
    alert.setPositiveButton(R.string.dialog_btn_add, new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int which) {
      }
    });
    final android.app.AlertDialog dialog = alert.create();
    dialog.show();
    dialog.getButton(android.app.AlertDialog.BUTTON_POSITIVE).setOnClickListener(
      new View.OnClickListener() {
        @Override
        public void onClick(View v) {
          if(pb.getVisibility() != View.VISIBLE) {
            String phone = etPhone.getText().toString().trim();
            if(phone.isEmpty()) {
              sendToast(R.string.validation_mobile_number);
              return;
            }
            pb.setVisibility(View.VISIBLE);
            // Check if the mobile number is not already registered
            ParseQuery<ParseUser> userParseQuery = ParseUser.getQuery();
            userParseQuery.whereEqualTo("mobileNumber",
              ((CountryInfo) spCountryCode.getSelectedItem()).dialingCode + phone);
            final String finalPhone = phone;
            userParseQuery.findInBackground(new FindCallback<ParseUser>() {
              @Override
              public void done(List<ParseUser> objects, ParseException e) {
                if(e == null) {
                  if(objects == null || objects.size() == 0) { // no records found, hence
                    // mobile number is not already registered
                    if(!isPhoneVerificationEnabled) {
                      ParseUser user = ParseUser.getCurrentUser();
                      user.put("mobileNumber",
                        ((CountryInfo) spCountryCode.getSelectedItem()).dialingCode +
                          finalPhone);
                      user.saveInBackground(new SaveCallback() {
                        @Override
                        public void done(ParseException e) {
                          pb.setVisibility(View.GONE);
                          if(e == null) {
                            // Mobile Number was successfully updated
                            updateUI();
                          } else {
                            if(weakRef.get() != null && !weakRef.get().isFinishing()) {
                              sendToast(e);
                            }
                          }
                          dialog.dismiss();
                        }
                      });
                    }
                  } else {
                    pb.setVisibility(View.GONE);
                    if(weakRef.get() != null && !weakRef.get().isFinishing()) {
                      getApplicationContext();
                      sendToast(R.string.mobile_already_registered);
                    }
                  }
                } else {
                  pb.setVisibility(View.GONE);
                  if(weakRef.get() != null && !weakRef.get().isFinishing()) {
                    sendToast(e);
                  }
                }
              }
            });
          }
        }
      });
  }

  private void setClickEvents() {
    boolean isCustomPicEnabled = getResources().getBoolean(R.bool.is_custom_pic_enabled);
    if(isCustomPicEnabled) {
      imgEdit.setOnClickListener(this);
    }
  }

  @Override
  protected void onResume() {
    super.onResume();
    updateUI();
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    MenuInflater inflater = getMenuInflater();
    inflater.inflate(R.menu.menu_profile_view, menu);
    return true;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    switch(item.getItemId()) {
      case android.R.id.home:
        finish();
        return true;
      case R.id.action_edit_profile:
        Intent intentProfileEdit = new Intent(this, ProfileEditActivity.class);
        startActivity(intentProfileEdit);
        return true;
      default:
        return super.onOptionsItemSelected(item);
    }
  }

  protected void startIntentService() {
    Intent intent = new Intent(this, FetchAddressIntentService.class);
    intent.putExtra(Constants.RECEIVER, mResultReceiver);
    intent.putExtra(Constants.LOCATION_DATA_LAT, Singleton.INSTANCE.getLatitude());
    intent.putExtra(Constants.LOCATION_DATA_LNG, Singleton.INSTANCE.getLongitude());
    FetchAddressIntentService.enqueueWork(this, intent);
  }

  @Override
  public void onClick(View view) {
    int id = view.getId();
    switch(id) {
      case R.id.img_edit:
        captureAndUploadImage();
        break;
      case R.id.txt_btn_add_phone:
        // Add phone number
        showAddPhoneDialog();
        break;
    }
  }

  private void captureAndUploadImage() {
    if(imagePicker == null) {
      imagePicker = new ImagePicker(this, null, false);
    }
    imagePicker.displayAddImageChooserDialog();
  }

  @Override
  public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                         @NonNull int[] grantResults) {
    if(requestCode == ImagePicker.PERMISSION_CAMERA) {
      if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
        if(imagePicker == null) {
          imagePicker = new ImagePicker(this, null, false);
        }
        imagePicker.dispatchTakePictureIntent();
      } else {
        getApplicationContext();
        sendToast("App does not have permission to capture image");
      }
    } else {
      super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }
  }

  @Override
  public void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);
    LogEvent.Log(TAG, "requestCode: " + requestCode + " resultCode: " + resultCode);
    if(resultCode == RESULT_OK && requestCode == ImagePicker.REQUEST_IMAGE_CAPTURE) {
      final String update_pic = "/update_pic.php";

      new UploadImageApiCall().execute(
        BASE_URL + update_pic + PARAM_IMAGE_TYPE +
          "a" + PARAM_SEPARATOR +
          PARAM_USER_ID);
    } else if(resultCode == RESULT_OK && requestCode == ImagePicker.REQUEST_LOAD_IMAGE) {
      if(data != null && data.getData() != null) {
        imagePicker.saveGalleryImage(data, "avatar.png");
        new UploadImageApiCall().execute(
          BASE_URL + ENDPOINT_UPDATE_PIC + PARAM_IMAGE_TYPE +
            "a" + PARAM_SEPARATOR +
            PARAM_USER_ID);
      } else {
        LogEvent.Log(TAG, "image data is null");
      }
    }
  }

  private void displayNetworkAlert(final String alert) {
    AlertDialog.Builder dialog = new AlertDialog.Builder(this);
    dialog.setMessage(alert);
    dialog.setNegativeButton(R.string.dialog_btn_try_again, new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialog, int which) {
        new UploadImageApiCall().execute(
          BASE_URL + ENDPOINT_UPDATE_PIC + PARAM_IMAGE_TYPE +
            "a" + PARAM_SEPARATOR +
            PARAM_USER_ID);
      }
    });
    dialog.setPositiveButton(R.string.dialog_btn_cancel, new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialog, int which) {
      }
    });
    dialog.create().show();
  }

  private static class ItemViewHolder {
    TextView txtCountryName;
    TextView txtCountryCode;
    ImageView imgFlag;
    ImageView imgTick;
  }

  private class AddressResultReceiver extends ResultReceiver {
    public AddressResultReceiver(Handler handler) {
      super(handler);
    }

    @Override
    protected void onReceiveResult(int resultCode, Bundle resultData) {
      // Display the city
      if(resultCode == Constants.SUCCESS_RESULT) {
        String city = resultData.getString(Constants.RESULT_DATA_CITY);
        txtCityName.setText(city);
      } else {
        txtCityName.setText(R.string.city_not_found);
      }
    }
  }

  private class UploadImageApiCall extends AsyncTask<String, Void, Boolean> {
    private String message;
    private Bitmap bmp;

    @Override
    protected void onPreExecute() {
      super.onPreExecute();
      message = getString(R.string.low_connection_msg);
      spinner.setVisibility(View.VISIBLE);
    }

    @Override
    protected Boolean doInBackground(String... params) {
      LogEvent.Log("UploadImageApiCall", "URL: " + params[0]);
      try {
        String imageUrl = params[0];
        imageUrl += ParseUser.getCurrentUser().getObjectId() +
                      (ParseUser.getCurrentUser().getInt("imageName") + 1);
        String path = getExternalFilesDir(null).getAbsolutePath() + "/Photo/avatar.png";
        //Bitmap bmp = BitmapFactory.decodeFile(path);
        bmp = ImagePicker.decodeSampledBitmapFromResource(path, 300, 300);
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
        byte[] outputBytes = byteArrayOutputStream.toByteArray();
        URL url = new URL(imageUrl);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setReadTimeout(getResources().getInteger(R.integer.maximum_timeout_to_server));
        conn.setConnectTimeout(getResources().getInteger(R.integer.maximum_timeout_to_server));
        conn.setRequestMethod("POST");
        conn.setDoInput(true);
        conn.setDoOutput(true);
        OutputStream os = conn.getOutputStream();
        os.write(outputBytes);
        int responseCode = conn.getResponseCode();
        LogEvent.Log(TAG, "responseCode : " + responseCode);
        String response = "";
        if(responseCode == HttpsURLConnection.HTTP_OK) {
          LogEvent.Log(TAG, "HTTP_OK");
          String line;
          BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
          while((line = br.readLine()) != null) {
            response += line;
          }
          ParseUser user = ParseUser.getCurrentUser();
          user.increment("imageName");
          user.save();
        } else {
          LogEvent.Log(TAG, "HTTP_NOT_OK");
          response = "";
        }
        LogEvent.Log("json response: ", response);
        if(response == null || response.isEmpty()) {
          return null;
        } else {
          JSONObject jObj = new JSONObject(response);
          String resType = jObj.getString("res_type");
          if(resType.equalsIgnoreCase("Error")) {
            message = jObj.getString("msg");
            LogEvent.Log(TAG, "Error:" + jObj.getString("msg"));
            return false;
          } else {
            return true;
          }
        }
      } catch(Exception e) {
        LogEvent.Log("UploadImageApiCall", "Exception: " + e.toString());
        return null;
      }
    }

    @Override
    protected void onPostExecute(Boolean result) {
      super.onPostExecute(result);
      spinner.setVisibility(View.GONE);
      if(result == null) {
        if(weakRef.get() != null && !weakRef.get().isFinishing()) {
          displayNetworkAlert(message);
        }
      } else {
        //userPicChanged = false;
        imgUser.setImageBitmap(getCroppedBitmap(bmp));
        sendToast(R.string.avatar_updated_successfully);
      }
    }
  }

  private class CountryListAdapter extends ArrayAdapter<CountryInfo> {
    private final ArrayList<CountryInfo> list;
    private int resourceId;
    private LayoutInflater inflator;

    public CountryListAdapter(Context context, int resourceId, ArrayList<CountryInfo> list) {
      super(context, resourceId, list);
      this.list = list;
      this.resourceId = resourceId;
      inflator = getLayoutInflater();
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
      ItemViewHolder holder = null;
      CountryInfo item = list.get(position);
      if(convertView == null) {
        holder = new ItemViewHolder();
        convertView = inflator.inflate(resourceId, null);
        holder.txtCountryName = convertView.findViewById(R.id.txt_country_name);
        holder.imgFlag = convertView.findViewById(R.id.img_flag);
        holder.imgTick = convertView.findViewById(R.id.img_tick);
        convertView.setTag(holder);
      }
      holder = (ItemViewHolder) convertView.getTag();
      holder.txtCountryName.setText(item.name + " +" + item.dialingCode);
      holder.imgFlag.setImageResource(item.flagId);
      if(item.selected) {
        holder.imgTick.setVisibility(View.VISIBLE);
      } else {
        holder.imgTick.setVisibility(View.GONE);
      }
      return convertView;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
      ItemViewHolder holder = null;
      CountryInfo item = list.get(position);
      if(convertView == null) {
        holder = new ItemViewHolder();
        convertView = inflator.inflate(R.layout.cell_country_code, null);
        holder.txtCountryCode = convertView.findViewById(R.id.txt_country_code);
        holder.txtCountryCode.setTextColor(Color.BLACK);
        convertView.setTag(holder);
      }
      holder = (ItemViewHolder) convertView.getTag();
      holder.txtCountryCode.setText("+" + item.dialingCode);
      return convertView;
    }
  }
}
