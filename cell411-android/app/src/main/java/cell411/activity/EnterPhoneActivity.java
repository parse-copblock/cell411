package cell411.activity;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.callback.FindCallback;
import com.parse.callback.SaveCallback;
import com.safearx.cell411.R;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import cell411.Singleton;
import cell411.constants.ParseKeys;
import cell411.methods.UtilityMethods;
import cell411.models.CountryInfo;
import cell411.utils.LogEvent;
import cell411.widgets.Spinner;

/**
 * Created by Sachin on 28-07-2017.
 */
public class EnterPhoneActivity extends AppCompatActivity {
  private final String TAG = "EnterPhoneActivity";
  private WeakReference<EnterPhoneActivity> weakRef;
  private Spinner spinner;
  private EditText etMobile;
  private android.widget.Spinner spCountryCode;
  private ArrayList<CountryInfo> list;

  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_enter_phone);
    weakRef = new WeakReference<>(this);
    spinner = (Spinner) findViewById(R.id.spinner);
    etMobile = (EditText) findViewById(R.id.et_mobile);
    list = new ArrayList<CountryInfo>();
    UtilityMethods.initializeCountryCodeList(list,
      1,
      getResources().getBoolean(R.bool.enable_publish));
    spCountryCode = (android.widget.Spinner) findViewById(R.id.sp_country_code);
    CountryListAdapter countryListAdapter =
      new CountryListAdapter(this, R.layout.cell_country, list);
    countryListAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
    spCountryCode.setAdapter(countryListAdapter);
    spCountryCode.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
      @Override
      public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
        CountryInfo countryInfo = (CountryInfo) spCountryCode.getSelectedItem();
        for(int i = 0; i < list.size(); i++) {
          list.get(i).selected = false;
        }
        countryInfo.selected = true;
        LogEvent.Log(TAG,
          "countryInfo: " + countryInfo.name + " (" + countryInfo.shortCode + ") + " +
            countryInfo.dialingCode);
        LogEvent.Log(TAG, "position: " + position);
      }

      @Override
      public void onNothingSelected(AdapterView<?> adapterView) {
      }
    });
    spCountryCode.setSelection(UtilityMethods.getDefaultCountryCodeIndex(list));
    UtilityMethods.setPhoneAndCountryCode(
      ParseUser.getCurrentUser().getString(ParseKeys.MOBILE_NUMBER), etMobile, spCountryCode,
      list);
    TextView btnSubmit = (TextView) findViewById(R.id.txt_btn_submit);
    btnSubmit.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        submitPhone();
      }
    });
  }

  private void submitPhone() {
    if(spinner.getVisibility() == View.VISIBLE) {
      return;
    }
    String mobileNumber = etMobile.getText().toString();
    if(mobileNumber.isEmpty()) {
      getApplicationContext();
      Singleton.sendToast(R.string.validation_mobile_number);
    } else {
      spinner.setVisibility(View.VISIBLE);
      // Check if the mobile number is not already registered
      ParseQuery<ParseUser> userParseQuery = ParseUser.getQuery();
      userParseQuery.whereNotEqualTo("objectId", ParseUser.getCurrentUser().getObjectId());
      userParseQuery.whereEqualTo("mobileNumber",
        ((CountryInfo) spCountryCode.getSelectedItem()).dialingCode + mobileNumber.trim());
      userParseQuery.setLimit(1);
      final String finalMobileNumber = mobileNumber;
      userParseQuery.findInBackground(new FindCallback<ParseUser>() {
        @Override
        public void done(List<ParseUser> objects, ParseException e) {
          spinner.setVisibility(View.GONE);
          if(e == null) {
            if(objects == null || objects.size() == 0) { // no records found, hence
              // mobile number is not already registered
              ParseUser user = ParseUser.getCurrentUser();
              user.put(ParseKeys.MOBILE_NUMBER,
                ((CountryInfo) spCountryCode.getSelectedItem()).dialingCode +
                  finalMobileNumber.trim());
              user.saveInBackground(new SaveCallback() {
                @Override
                public void done(ParseException e) {
                  finish();
                }
              });
            } else {
              if(weakRef.get() != null && !weakRef.get().isFinishing()) {
                getApplicationContext();
                Singleton.sendToast(R.string.mobile_already_registered);
              }
            }
          } else {
            if(weakRef.get() != null && !weakRef.get().isFinishing()) {
              Singleton.sendToast(e);
            }
          }
        }
      });
    }
  }

  private static class ItemViewHolder {
    TextView txtCountryName;
    TextView txtCountryCode;
    ImageView imgFlag;
    ImageView imgTick;
  }

  private class CountryListAdapter extends ArrayAdapter<CountryInfo> {
    private final ArrayList<CountryInfo> list;
    private int resourceId;
    private LayoutInflater inflator;

    public CountryListAdapter(Context context, int resourceId, ArrayList<CountryInfo> list) {
      super(context, resourceId, list);
      this.list = list;
      this.resourceId = resourceId;
      inflator = getLayoutInflater();
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
      ItemViewHolder holder = null;
      CountryInfo item = list.get(position);
      if(convertView == null) {
        holder = new ItemViewHolder();
        convertView = inflator.inflate(resourceId, null);
        holder.txtCountryName = (TextView) convertView.findViewById(R.id.txt_country_name);
        holder.imgFlag = (ImageView) convertView.findViewById(R.id.img_flag);
        holder.imgTick = (ImageView) convertView.findViewById(R.id.img_tick);
        convertView.setTag(holder);
      }
      holder = (ItemViewHolder) convertView.getTag();
      holder.txtCountryName.setText(item.name + " +" + item.dialingCode);
      holder.imgFlag.setImageResource(item.flagId);
      if(item.selected) {
        holder.imgTick.setVisibility(View.VISIBLE);
      } else {
        holder.imgTick.setVisibility(View.GONE);
      }
      return convertView;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
      ItemViewHolder holder = null;
      CountryInfo item = list.get(position);
      if(convertView == null) {
        holder = new ItemViewHolder();
        convertView = inflator.inflate(R.layout.cell_country_code, null);
        holder.txtCountryCode = (TextView) convertView.findViewById(R.id.txt_country_code);
        convertView.setTag(holder);
      }
      holder = (ItemViewHolder) convertView.getTag();
      holder.txtCountryCode.setText("+" + item.dialingCode);
      holder.txtCountryCode.setTextColor(Color.BLACK);
      return convertView;
    }
  }
}