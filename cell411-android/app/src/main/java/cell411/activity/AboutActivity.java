package cell411.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.ColorFilter;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.net.Uri;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.ProgressBar;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatDelegate;

import com.safearx.cell411.R;

import java.lang.ref.WeakReference;

import cell411.Singleton;
import cell411.utils.LogEvent;

public class AboutActivity extends BaseActivity implements View.OnClickListener {
  private static final String TAG = AboutActivity.class.getSimpleName();
  public static WeakReference<AboutActivity> weakRef;
  private WebView wvAbout;
  private ProgressBar pb;

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    if(item.getItemId() == android.R.id.home) {
      finish();
      return true;
    }
    return super.onOptionsItemSelected(item);
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_about);
    weakRef = new WeakReference<>(this);
    Singleton.INSTANCE.setCurrentActivity(this);
    final ActionBar actionBar = getSupportActionBar();
    actionBar.setDisplayHomeAsUpEnabled(true);
    actionBar.setDisplayShowHomeEnabled(true);
    pb = findViewById(R.id.pb);
    final ImageView imgPrevious = findViewById(R.id.img_previous);
    final ImageView imgForward = findViewById(R.id.img_forward);
    final ImageView imgRefresh = findViewById(R.id.img_refresh);
    imgPrevious.setOnClickListener(this);
    imgForward.setOnClickListener(this);
    imgRefresh.setOnClickListener(this);
    final ColorFilter colorFilterActive =
      new PorterDuffColorFilter(getResources().getColor(R.color.highlight_color),
        PorterDuff.Mode.MULTIPLY);
    final ColorFilter colorFilterInActive =
      new PorterDuffColorFilter(getResources().getColor(R.color.gray_666),
        PorterDuff.Mode.MULTIPLY);
    wvAbout = findViewById(R.id.wv_about);
    wvAbout.getSettings().setJavaScriptEnabled(true);
    wvAbout.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
    wvAbout.getSettings().setAppCacheEnabled(true);
    wvAbout.getSettings().setDatabaseEnabled(true);
    wvAbout.getSettings().setDomStorageEnabled(true);
    //wvAbout.getSettings().setLoadWithOverviewMode(true);
        /*
        wvAbout.getSettings().setUseWideViewPort(true);
        wvAbout.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
        wvAbout.getSettings().setPluginState(WebSettings.PluginState.ON);
        wvAbout.setBackgroundColor(Color.argb(1, 0, 0, 0));
        wvAbout.getSettings().setBuiltInZoomControls(true);*/
    wvAbout.setWebViewClient(new WebViewClient() {
      @Override
      public void onPageStarted(WebView view, String url, Bitmap favicon) {
        super.onPageStarted(view, url, favicon);
        pb.setVisibility(View.VISIBLE);
        //txtTitle.setText("Loading...");
        imgRefresh.setImageResource(R.drawable.ic_web_cancel);
      }

      public void onPageFinished(WebView view, String url) {
        pb.setVisibility(View.GONE);
        //txtTitle.setText(webView.getTitle());
        imgRefresh.setImageResource(R.drawable.ic_web_refresh);
        if(wvAbout.canGoBack()) {
          imgPrevious.setColorFilter(colorFilterActive);
        } else {
          imgPrevious.setColorFilter(colorFilterInActive);
        }
        if(wvAbout.canGoForward()) {
          imgForward.setColorFilter(colorFilterActive);
        } else {
          imgForward.setColorFilter(colorFilterInActive);
        }
      }

      public boolean shouldOverrideUrlLoading(WebView view, String url) {
        if((url.contains("http") || url.contains("market://") || url.contains("mailto:") ||
              url.contains("play.google") || url.contains("tel:") || url.contains("vid:")) ==
             true) {
          // Load new URL Don't override URL Link
          view.getContext().startActivity(
            new Intent(Intent.ACTION_VIEW, Uri.parse(url.replace("file:///android_asset/", ""))));
          return true;
        } else {
          // do your handling codes here, which url is the requested url
          // probably you need to open that url rather than redirect:
          LogEvent.Log(TAG, "url: " + url);
          view.loadUrl(url);
          return false; // then it is not handled by default action
        }
      }
    });
    if(AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_NO) {
      wvAbout.loadUrl("file:///android_asset/about_en.html");
    } else {
      wvAbout.loadUrl("file:///android_asset/about_en_night.html");
    }
  }

  @Override
  public void onClick(View view) {
    switch(view.getId()) {
      case R.id.img_previous:
        wvAbout.goBack();
        break;
      case R.id.img_refresh:
        if(pb.getVisibility() == View.VISIBLE) {
          wvAbout.stopLoading();
        } else {
          wvAbout.reload();
        }
        break;
      case R.id.img_forward:
        wvAbout.goForward();
        break;
    }
  }
}