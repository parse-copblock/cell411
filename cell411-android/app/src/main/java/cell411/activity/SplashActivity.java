package cell411.activity;

import android.annotation.SuppressLint;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.widget.TextView;

import com.parse.ParseUser;
import com.safearx.cell411.R;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.ref.WeakReference;

import cell411.Singleton;
import cell411.constants.Prefs;
import cell411.utils.LogEvent;

import static cell411.SingletonConfig.CHANNEL_ID_BLUETOOTH;
import static cell411.SingletonConfig.CHANNEL_ID_EMERGENCY_ALERT;
import static cell411.SingletonConfig.CHANNEL_ID_LOCATION_DISABLED;
import static cell411.SingletonConfig.CHANNEL_ID_MESSAGE;
import static cell411.SingletonConfig.CHANNEL_ID_MISCELLANEOUS;

;

public class SplashActivity extends BaseBaseActivity {
  private static final String TAG = "SplashSuperActivity";
  //private final long WAIT_HOURS = 86400000;
  private final long WAIT_HOURS = 300000;
  public WeakReference<SplashActivity> weakRef;
  public TextView txtLblLoading;
  public boolean gotoChatTab;
  public boolean gotoChatScreen;
  public String entityObjectId;
  public String entityName;
  public String entityType;
  public String entityCreatedAt;
  private SharedPreferences prefs;
  TextView textView;
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    prefs = Singleton.INSTANCE.getAppPrefs();
    createNotificationChannel();
    if(true) {
      try {
        setContentView(R.layout.activity_splash);
      } catch (Throwable t) {
        textView = new TextView(this);
        StringWriter writer = new StringWriter();
        PrintWriter printer = new PrintWriter(writer);
        t.printStackTrace(printer);
        textView.setText(writer.toString());
        setContentView(textView);
        LogEvent.Log(TAG, "Unable to setContentView()");
      }
    }
    weakRef = new WeakReference<>(this);
    txtLblLoading = (TextView) findViewById(R.id.txt_lbl_loading);
    gotoChatTab = getIntent().getBooleanExtra("gotoChatTab", false);
    gotoChatScreen = getIntent().getBooleanExtra("gotoChatScreen", false);
    entityType = getIntent().getStringExtra("entityType");
    entityObjectId = getIntent().getStringExtra("entityObjectId");
    entityName = getIntent().getStringExtra("entityName");
    entityCreatedAt = getIntent().getStringExtra("time");
    proceedToApp();
  }


  private void proceedToApp() {
    boolean isLoggedIn = isLoggedIn();
    ParseUser currentUser = ParseUser.getCurrentUser();
    if(currentUser != null && isLoggedIn) {
      // do stuff with the user
      Intent intent = new Intent(SplashActivity.this, PermissionActivity.class);
      intent.putExtra("gotoChatTab", gotoChatTab);
      intent.putExtra("gotoChatScreen", gotoChatScreen);
      intent.putExtra("entityType", entityType);
      intent.putExtra("entityObjectId", entityObjectId);
      intent.putExtra("entityName", entityName);
      startActivity(intent);
      finish();
      return;
    }
    Handler handler = new Handler();
    handler.postDelayed(new Runnable() {
      @Override
      public void run() {
        LogEvent.Log("SplashActivity", "Time Over");
        if(ParseUser.getCurrentUser() != null && isLoggedIn()) {
          Intent intent = new Intent(SplashActivity.this, PermissionActivity.class);
          intent.putExtra("gotoChatTab", gotoChatTab);
          intent.putExtra("gotoChatScreen", gotoChatScreen);
          intent.putExtra("entityType", entityType);
          intent.putExtra("entityObjectId", entityObjectId);
          intent.putExtra("entityName", entityName);
          startActivity(intent);
          finish();
        } else {
          Intent intent = new Intent(SplashActivity.this, GalleryActivity.class);
          startActivity(intent);
          finish();
        }
      }
    }, 1000);
  }

  static private boolean isLoggedIn() {
    return Singleton.isLoggedIn();
  }

  private void createNotificationChannel() {
    // Create the NotificationChannel, but only on API 26+ because
    // the NotificationChannel class is new and not in the support library
    if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
      CharSequence name = getString(R.string.channel_name_emergency_alert);
      String description = getString(R.string.channel_description_emergency_alert);
      @SuppressLint("WrongConstant") NotificationChannel channel =
        new NotificationChannel(CHANNEL_ID_EMERGENCY_ALERT, name,
          NotificationManager.IMPORTANCE_HIGH);
      channel.setDescription(description);
      CharSequence name1 = getString(R.string.channel_name_bluetooth);
      String description1 = getString(R.string.channel_description_bluetooth);
      @SuppressLint("WrongConstant") NotificationChannel channel1 =
        new NotificationChannel(CHANNEL_ID_BLUETOOTH, name1,
          NotificationManager.IMPORTANCE_HIGH);
      channel1.setDescription(description1);
      CharSequence name2 = getString(R.string.channel_name_message);
      String description2 = getString(R.string.channel_description_message);
      @SuppressLint("WrongConstant") NotificationChannel channel2 =
        new NotificationChannel(CHANNEL_ID_MESSAGE, name2,
          NotificationManager.IMPORTANCE_DEFAULT);
      channel2.setDescription(description2);
      CharSequence name3 = getString(R.string.channel_name_location_disabled);
      String description3 = getString(R.string.channel_description_location_disabled);
      @SuppressLint("WrongConstant") NotificationChannel channel3 =
        new NotificationChannel(CHANNEL_ID_LOCATION_DISABLED, name3,
          NotificationManager.IMPORTANCE_DEFAULT);
      channel3.setDescription(description3);
      CharSequence name4 = getString(R.string.channel_name_miscellaneous);
      String description4 = getString(R.string.channel_description_miscellaneous);
      @SuppressLint("WrongConstant") NotificationChannel channel4 =
        new NotificationChannel(CHANNEL_ID_MISCELLANEOUS, name4,
          NotificationManager.IMPORTANCE_DEFAULT);
      channel4.setDescription(description4);
      // Register the channel with the system; you can't change the importance
      // or other notification behaviors after this
      NotificationManager notificationManager = getSystemService(NotificationManager.class);
      notificationManager.createNotificationChannel(channel);
      notificationManager.createNotificationChannel(channel1);
      notificationManager.createNotificationChannel(channel2);
      notificationManager.createNotificationChannel(channel3);
      notificationManager.createNotificationChannel(channel4);
    }
  }

}