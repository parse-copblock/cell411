package cell411.activity;

import android.graphics.Bitmap;
import android.graphics.ColorFilter;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.safearx.cell411.R;

import java.lang.ref.WeakReference;

import cell411.Singleton;
import cell411.SingletonImageFactory;
import cell411.utils.LogEvent;

public class MapObjectiveActivity extends BaseActivity {
  public static WeakReference<MapObjectiveActivity> weakRef;
  private final String TAG = "MapObjectiveActivity";

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(com.safearx.cell411.R.layout.activity_map_objective);
    weakRef = new WeakReference<>(this);
    Singleton.INSTANCE.setCurrentActivity(this);
    int categoryId = getIntent().getIntExtra("categoryId", -1);
    if(categoryId == -1) {
      finish();
      return;
    }
    String name = getIntent().getStringExtra("name");
    String url = getIntent().getStringExtra("url");
    String description = getIntent().getStringExtra("description");
    String fullAddress = getIntent().getStringExtra("fullAddress");
    String hours = getIntent().getStringExtra("hours");
    String phone = getIntent().getStringExtra("phone");
    String imageUrl = getIntent().getStringExtra("imageUrl");
    RelativeLayout rlIconContainer = findViewById(R.id.rl_mo_icon_container);
    ImageView imgCategoryIcon = findViewById(R.id.img_mo_category_icon);
    View viewHeader = findViewById(R.id.view_mo_header);
    ImageView imgClose = findViewById(R.id.img_mo_close);
    final ImageView imgMapObjective = findViewById(R.id.img_map_objective);
    TextView txtName = findViewById(R.id.txt_mo_name);
    TextView txtCategory = findViewById(R.id.txt_mo_category);
    ImageView imgCategoryIconSmall = findViewById(R.id.img_mo_category_icon_small);
    ImageView imgUrlIcon = findViewById(R.id.img_mo_url);
    TextView txtUrl = findViewById(R.id.txt_mo_url);
    TextView txtDescription = findViewById(R.id.txt_mo_description);
    ImageView imgAddressIcon = findViewById(R.id.img_mo_address);
    TextView txtfullAddress = findViewById(R.id.txt_mo_full_address);
    ImageView imgHoursIcon = findViewById(R.id.img_mo_hours);
    TextView txtHours = findViewById(R.id.txt_mo_hours);
    ImageView imgPhoneIcon = findViewById(R.id.img_mo_phone);
    TextView txtPhone = findViewById(R.id.txt_mo_phone);
    ColorFilter colorFilter = null;
    switch(categoryId) {
      case 1:
        rlIconContainer.setBackgroundResource(R.drawable.bg_mo_pharmacy);
        imgCategoryIcon.setImageResource(R.drawable.ic_category_pharmacy);
        viewHeader.setBackgroundColor(getResources().getColor(R.color.mo_pharmacy));
        imgCategoryIconSmall.setImageResource(R.drawable.ic_category_pharmacy);
        colorFilter = new PorterDuffColorFilter(getResources().getColor(R.color.mo_pharmacy),
          PorterDuff.Mode.MULTIPLY);
        break;
      case 2:
        rlIconContainer.setBackgroundResource(R.drawable.bg_mo_hospital);
        imgCategoryIcon.setImageResource(R.drawable.ic_category_hospital);
        viewHeader.setBackgroundColor(getResources().getColor(R.color.mo_hospital));
        imgCategoryIconSmall.setImageResource(R.drawable.ic_category_hospital);
        colorFilter = new PorterDuffColorFilter(getResources().getColor(R.color.mo_hospital),
          PorterDuff.Mode.MULTIPLY);
        break;
      case 3:
        rlIconContainer.setBackgroundResource(R.drawable.bg_mo_police);
        imgCategoryIcon.setImageResource(R.drawable.ic_category_police);
        viewHeader.setBackgroundColor(getResources().getColor(R.color.mo_police));
        imgCategoryIconSmall.setImageResource(R.drawable.ic_category_police);
        colorFilter = new PorterDuffColorFilter(getResources().getColor(R.color.mo_police),
          PorterDuff.Mode.MULTIPLY);
        break;
    }
    if(colorFilter != null) {
      imgUrlIcon.setColorFilter(colorFilter);
      imgAddressIcon.setColorFilter(colorFilter);
      imgHoursIcon.setColorFilter(colorFilter);
      imgPhoneIcon.setColorFilter(colorFilter);
    }
    txtName.setText(name);
    txtCategory.setText(getCategoryForId(categoryId));
    txtUrl.setText(url);
    txtDescription.setText(description);
    txtfullAddress.setText(fullAddress);
    txtHours.setText(hours);
    txtPhone.setText(phone);
    LogEvent.Log(TAG, "imageUrl: " + imageUrl);
    if(imageUrl != null) {
      SingletonImageFactory.getImageLoader().loadImage(imageUrl, new ImageLoadingListener() {
        @Override
        public void onLoadingStarted(String s, View view) {
          LogEvent.Log(TAG, "onLoadingStarted(): " + s);
        }

        @Override
        public void onLoadingFailed(String s, View view, FailReason failReason) {
          LogEvent.Log(TAG,
            "onLoadingFailed(): " + s + " === " + failReason.getCause().getMessage());
        }

        @Override
        public void onLoadingComplete(String s, View view, Bitmap bitmap) {
          if(bitmap == null) {
            LogEvent.Log(TAG, "onLoadingComplete(): " + s + " bitmap is null");
          } else {
            LogEvent.Log(TAG, "onLoadingComplete(): " + s + " bitmap is not null");
          }
          imgMapObjective.setImageBitmap(bitmap);
        }

        @Override
        public void onLoadingCancelled(String s, View view) {
          LogEvent.Log(TAG, "onLoadingCancelled(): " + s);
        }
      });
    }
    imgClose.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        finish();
      }
    });
    findViewById(R.id.rl_mo_outer_wrapper).setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        // Close the activity when tappeed on the transparent area (which is outside rl_mo_container)
        finish();
      }
    });
    findViewById(R.id.rl_mo_container).setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        // To capture click event and prevent from passing it down to rl_mo_outer_wrapper
      }
    });
    findViewById(R.id.rl_mo_icon_container).setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        // To capture click event and prevent from passing it down to rl_mo_outer_wrapper
      }
    });
  }

  private String getCategoryForId(int id) {
    switch(id) {
      case 1:
        return getString(R.string.category_pharmacy);
      case 2:
        return getString(R.string.category_hospital);
      case 3:
        return getString(R.string.category_police);
      default:
        return getString(R.string.alert_un_recognized);
    }
  }
}