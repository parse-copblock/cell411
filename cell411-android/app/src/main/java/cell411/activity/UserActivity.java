package cell411.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParsePush;
import com.parse.ParseQuery;
import com.parse.ParseRelation;
import com.parse.ParseUser;
import com.parse.callback.CountCallback;
import com.parse.callback.FindCallback;
import com.parse.callback.GetCallback;
import com.parse.callback.SaveCallback;
import com.safearx.cell411.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import cell411.Singleton;
import cell411.constants.Constants;
import cell411.methods.Modules;
import cell411.models.Cell;
import cell411.models.User;
import cell411.services.FetchAddressIntentService;
import cell411.utils.LogEvent;

/**
 * Created by Sachin on 08-08-2016.
 */
public class UserActivity extends BaseActivity {
  public static WeakReference<UserActivity> weakRef;
  private final String TAG = "UserActivity";
  private AddressResultReceiver mResultReceiver;
  private TextView txtCityName;
  private TextView txtAlertsSent;
  private TextView txtAlertsResponded;
  private LinearLayout llActions;
  private RelativeLayout rlBtnAddFriend;
  private ImageView imgBtnAddFriend;
  private TextView txtBtnAddFriend;
  private RelativeLayout rlBtnSpam;
  private ParseUser parseUser;
  private boolean isFriend;
  private boolean isFriendshipActionInProgress = false;
  private boolean friendshipChecked = false;
  private boolean isSpammingActionInProgress;
  private boolean isSpammed;
  private int COLOR_PRIMARY;
  private int COLOR_WHITE;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    //setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    setContentView(R.layout.activity_user);
    weakRef = new WeakReference<>(this);
    Singleton.INSTANCE.setCurrentActivity(this);
    final String userId = getIntent().getStringExtra("userId");
    final String imageName = getIntent().getStringExtra("imageName");
    final String firstName = getIntent().getStringExtra("firstName");
    final String lastName = getIntent().getStringExtra("lastName");
    final String userName = getIntent().getStringExtra("username");
    String email = getIntent().getStringExtra("email");
    final double lat = getIntent().getDoubleExtra("lat", 0);
    final double lng = getIntent().getDoubleExtra("lng", 0);
    if(email == null || email.isEmpty()) {
      email = userName;
    }
    COLOR_PRIMARY = getResources().getColor(R.color.highlight_color);
    COLOR_WHITE = getResources().getColor(R.color.white);
    // initialize views
    initViews(userId, firstName, lastName, email, imageName);
    if(lat == 0 || lng == 0) {
      txtCityName.setText(R.string.location_not_available);
    } else {
      // Retrieve reverse geo coded address
      mResultReceiver = new AddressResultReceiver(new Handler());
      startIntentService(lat, lng);
    }
    // initialize the buttons friend/un friend and spam
    initActions(userId, firstName, lastName, email);
    ImageView imgClose = (ImageView) findViewById(R.id.img_close);
    imgClose.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        finish();
      }
    });
  }

  private void initViews(final String userId, final String firstName, final String lastName,
                         final String email, final String imageName) {
    final ImageView imgUser = (ImageView) findViewById(R.id.img_user);
    TextView txtName = (TextView) findViewById(R.id.txt_name);
    TextView txtEmail = (TextView) findViewById(R.id.txt_email);
    txtCityName = (TextView) findViewById(R.id.txt_city_name);
    txtAlertsSent = (TextView) findViewById(R.id.txt_alerts_sent);
    txtAlertsResponded = (TextView) findViewById(R.id.txt_alerts_responded);
    llActions = (LinearLayout) findViewById(R.id.ll_actions);
    rlBtnAddFriend = (RelativeLayout) findViewById(R.id.rl_btn_add_friend);
    imgBtnAddFriend = (ImageView) findViewById(R.id.img_btn_add_friend);
    txtBtnAddFriend = (TextView) findViewById(R.id.txt_btn_add_friend);
    rlBtnSpam = (RelativeLayout) findViewById(R.id.rl_btn_spam);
    llActions.setVisibility(View.GONE);
    Singleton.INSTANCE.setImage(imgUser, userId + imageName, email);
    imgUser.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        Intent profileImageIntent = new Intent(UserActivity.this, ProfileImageActivity.class);
        profileImageIntent.putExtra("userId", userId);
        profileImageIntent.putExtra("imageName", imageName);
        profileImageIntent.putExtra("email", email);
        startActivity(profileImageIntent);
      }
    });
    txtName.setText(firstName + " " + lastName);
    if(isFriend && email != null && !email.isEmpty()) {
      txtEmail.setText(email);
    } else {
      txtEmail.setVisibility(View.GONE);
    }
    retrieveAlertsCount(userId);
  }

  private void initActions(final String userId, final String firstName, final String lastName,
                           final String email) {
    // init friendship button
    if(Singleton.INSTANCE.getFriends() !=
         null) { // friend list already retrieved somewhere in the app
      // Check if the current user is a friend of this user
      for(int i = 0; i < Singleton.INSTANCE.getFriends().size(); i++) {
        if(Singleton.INSTANCE.getFriends().get(i).user.getObjectId().equals(userId)) {
          isFriend = true;
          break;
        }
      }
      // Display the button to friend/un-friend now
      if(isActionButtonsSafeToShow()) {
        initListeners(userId, firstName, lastName, email);
      }
    } else { // check if the current user is a friend of this user
      ParseRelation relation = ParseUser.getCurrentUser().getRelation("friends");
      ParseQuery<ParseUser> query = relation.getQuery();
      query.findInBackground(new FindCallback<ParseUser>() {
        @Override
        public void done(List<ParseUser> list, ParseException e) {
          if(e == null) {
            friendshipChecked = true;
            if(list != null && list.size() > 0) {
              ArrayList<User> friendList = new ArrayList<>();
              if(list != null) {
                for(int i = 0; i < list.size(); i++) {
                  ParseUser user = (ParseUser) list.get(i);
                  String email = null;
                  if(user.getEmail() != null && !user.getEmail().isEmpty()) {
                    email = user.getEmail();
                  } else {
                    email = user.getUsername();
                  }
                  friendList.add(
                    new User(email, (String) user.get("firstName"), (String) user.get("lastName"),
                      user));
                }
              }
              Singleton.INSTANCE.setFriends((ArrayList<User>) friendList.clone());
              for(int i = 0; i < friendList.size(); i++) {
                if(friendList.get(i).user.getObjectId().equals(userId)) {
                  isFriend = true;
                  break;
                }
              }
            }
            // Display the button to friend/un-friend now
            if(isActionButtonsSafeToShow()) {
              initListeners(userId, firstName, lastName, email);
            }
          } else {
            Singleton.sendToast(e);
          }
        }
      });
    }
    // init spam button
    if(!Singleton.INSTANCE.isSpamUserListRetrieved()) {
      // Check if the user is already marked as spam
      ParseRelation relation4SpamUsers = ParseUser.getCurrentUser().getRelation("spamUsers");
      final ParseQuery<ParseUser> parseQuery4SpamUsers = relation4SpamUsers.getQuery();
      parseQuery4SpamUsers.findInBackground(new FindCallback<ParseUser>() {
        @Override
        public void done(List<ParseUser> list, ParseException e) {
          if(e == null) {
            if(list != null) {
              for(int i = 0; i < list.size(); i++) {
                Singleton.INSTANCE.getSpammedUsersIdList().
                                                            add(list.get(i).getObjectId());
              }
            }
            Singleton.INSTANCE.setIsSpamUserListRetrieved(true);
            if(isActionButtonsSafeToShow()) {
              initListeners(userId, firstName, lastName, email);
            }
          } else {
            if(MainActivity.weakRef.get() != null && !MainActivity.weakRef.get().isFinishing()) {
              Singleton.sendToast(e);
            }
          }
        }
      });
    } else {
      if(isActionButtonsSafeToShow()) {
        initListeners(userId, firstName, lastName, email);
      }
    }
  }

  private boolean isActionButtonsSafeToShow() {
    if((Singleton.INSTANCE.isSpamUserListRetrieved()) &&
         Singleton.INSTANCE.getFriends() != null ||
         friendshipChecked) {
      return true;
    }
    return false;
  }

  private void initListeners(final String userId, final String firstName, final String lastName,
                             final String email) {
    if(isFriend) {
      //imgBtnAddFriend.setImageResource(R.drawable.ic_un_friend);
      if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
        imgBtnAddFriend.setImageTintList(ColorStateList.valueOf(COLOR_PRIMARY));
      }
      rlBtnAddFriend.setBackgroundResource(R.drawable.bg_un_friend);
      txtBtnAddFriend.setText(R.string.un_friend);
      txtBtnAddFriend.setTextColor(getResources().getColor(R.color.highlight_color));
    }
    // add listener on friendship button
    rlBtnAddFriend.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        if(isFriend && !isFriendshipActionInProgress) {
          // Delete this friend
          if(parseUser != null) {
            showDeleteFriendDialog(parseUser, firstName, lastName);
          } else {
            getApplication();
            Singleton.sendToast(R.string.please_wait);
          }
        } else if(!isFriendshipActionInProgress) { // Add Friend (send friend request)
          if(parseUser != null) {
            isFriendshipActionInProgress = true;
            rlBtnAddFriend.setBackgroundResource(R.drawable.bg_cell_join_processing);
            sendFriendRequest(parseUser, email);
          } else {
            getApplication();
            Singleton.sendToast(R.string.please_wait);
          }
        } else {
          getApplication();
          Singleton.sendToast(R.string.please_wait);
        }
      }
    });
    // add listener on spam button if required
    if(Singleton.INSTANCE.getSpammedUsersIdList().contains(userId)) {
      // user is already spammed so hide the button
      rlBtnSpam.setVisibility(View.GONE);
      llActions.setWeightSum(1);
    } else {
      // set listener on the spam button
      rlBtnSpam.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
          if(!isSpammingActionInProgress) {
            if(!isSpammed) {
              showFlagAlertDialog(firstName, lastName, userId);
            } else {
              getApplication();
              Singleton.sendToast(
                firstName + " " + lastName + " " + getString(R.string.blocked_successfully));
            }
          } else {
            getApplication();
            Singleton.sendToast(R.string.please_wait);
          }
        }
      });
    }
    llActions.setVisibility(View.VISIBLE);
  }

  private void retrieveAlertsCount(String userId) {
    ParseQuery<ParseUser> userQuery = ParseUser.getQuery();
    userQuery.whereEqualTo("objectId", userId);
    userQuery.getFirstInBackground(new GetCallback<ParseUser>() {
      @Override
      public void done(ParseUser parseUser, ParseException e) {
        if(e == null && parseUser != null) {
          UserActivity.this.parseUser = parseUser;
          ParseQuery<ParseObject> queryAlertsSent = ParseQuery.getQuery("Cell411Alert");
          queryAlertsSent.whereEqualTo("issuedBy", parseUser);
          queryAlertsSent.whereExists("alertType");
          queryAlertsSent.whereDoesNotExist("to");
          ParseQuery<ParseObject> queryAlertsSent2 = ParseQuery.getQuery("Cell411Alert");
          queryAlertsSent2.whereEqualTo("issuedBy", parseUser);
          queryAlertsSent2.whereExists("alertId");
          queryAlertsSent2.whereDoesNotExist("to");
          List<ParseQuery<ParseObject>> queries = new ArrayList<>();
          queries.add(queryAlertsSent);
          queries.add(queryAlertsSent2);
          ParseQuery<ParseObject> mainQuery = ParseQuery.or(queries);
          mainQuery.countInBackground(new CountCallback() {
            public void done(int count, ParseException e) {
              if(e == null) {
                // The count request succeeded. Log the count
                txtAlertsSent.setText("" + count);
              } else {
                // The request failed
                if(Modules.isWeakReferenceValid()) {
                  Singleton.sendToast(e);
                }
              }
            }
          });
          ParseQuery<ParseObject> queryAlertsResponded = ParseQuery.getQuery("Cell411Alert");
          queryAlertsResponded.whereEqualTo("initiatedBy", parseUser);
          queryAlertsResponded.countInBackground(new CountCallback() {
            public void done(int count, ParseException e) {
              if(e == null) {
                // The count request succeeded. Log the count
                txtAlertsResponded.setText("" + count);
              } else {
                // The request failed
                if(Modules.isWeakReferenceValid()) {
                  Singleton.sendToast(e);
                }
              }
            }
          });
        } else {
          if(Modules.isWeakReferenceValid()) {
            Singleton.sendToast(e);
          }
        }
      }
    });
  }

  private void sendFriendRequest(final ParseUser parseUser, final String email) {
    ParseRelation relation = ParseUser.getCurrentUser().getRelation("spammedBy");
    ParseQuery parseQuery = relation.getQuery();
    parseQuery.findInBackground(new FindCallback<ParseObject>() {
      @Override
      public void done(List list, ParseException e) {
        if(e == null) {
          if(list != null) {
            boolean userHasSpammedCurrentUser = false;
            for(int i = 0; i < list.size(); i++) {
              if((((ParseUser) list.get(i)).getEmail() != null &&
                    ((ParseUser) list.get(i)).getEmail().equals(email)) ||
                   ((ParseUser) list.get(i)).getUsername().equals(email)) {
                userHasSpammedCurrentUser = true;
                break;
              }
            }
            if(userHasSpammedCurrentUser) {
              isFriendshipActionInProgress = false;
              rlBtnAddFriend.setBackgroundResource(R.drawable.bg_cell_join);
              showAlertDialog();
            } else {
              // Before sending request, check if the user has not already sent request which is pending
              ParseQuery<ParseObject> query = ParseQuery.getQuery("Cell411Alert");
              query.whereEqualTo("issuedBy", ParseUser.getCurrentUser());
              query.whereEqualTo("to", email);
              query.whereEqualTo("status", "PENDING");
              query.whereEqualTo("entryFor", "FR");
              final String name = ParseUser.getCurrentUser().get("firstName") + " " +
                                    ParseUser.getCurrentUser().get("lastName");
              query.findInBackground(new FindCallback<ParseObject>() {
                @Override
                public void done(List<ParseObject> parseObjects, ParseException e) {
                  if(e == null) {
                    if(parseObjects != null &&
                         parseObjects.size() > 0) { // if true, the request already exists
                      // An approval request has already been sent which is pending,
                      // so display an alert to the current user
                      // Send notification for friend request here
                      ParseQuery pushQuery = ParseInstallation.getQuery();
                      pushQuery.whereEqualTo("user", parseUser);
                      ParsePush push = new ParsePush();
                      push.setQuery(pushQuery);
                      JSONObject data = new JSONObject();
                      try {
                        data.put("alert", getString(R.string.notif_friend_request, name,
                          getString(R.string.app_name)));
                        data.put("userId", ParseUser.getCurrentUser().getObjectId());
                        data.put("sound", "default");
                        data.put("friendRequestObjectId", parseObjects.get(0).getObjectId());
                        data.put("name", name);
                        data.put("alertType", "FRIEND_REQUEST");
                        data.put("badge", "Increment");
                      } catch(JSONException e1) {
                        e1.printStackTrace();
                      }
                      push.setData(data);
                      push.sendInBackground();
                      // Request sent
                      txtBtnAddFriend.setText(R.string.resend);
                      isFriendshipActionInProgress = false;
                      rlBtnAddFriend.setBackgroundResource(R.drawable.bg_cell_join);
                    } else {
                      final ParseObject friendRequestObject = new ParseObject("Cell411Alert");
                      friendRequestObject.put("issuedBy", ParseUser.getCurrentUser());
                      friendRequestObject.put("issuerFirstName", name);
                      friendRequestObject.put("to", email);
                      friendRequestObject.put("status", "PENDING");
                      friendRequestObject.put("entryFor", "FR");
                      friendRequestObject.saveInBackground(new SaveCallback() {
                        @Override
                        public void done(ParseException e) {
                          if(e == null) {
                            // Send notification for friend request here
                            ParseQuery pushQuery = ParseInstallation.getQuery();
                            pushQuery.whereEqualTo("user", parseUser);
                            ParsePush push = new ParsePush();
                            push.setQuery(pushQuery);
                            JSONObject data = new JSONObject();
                            try {
                              data.put("alert", getString(R.string.notif_friend_request, name,
                                getString(R.string.app_name)));
                              data.put("userId", ParseUser.getCurrentUser().getObjectId());
                              data.put("sound", "default");
                              data.put("friendRequestObjectId", friendRequestObject.getObjectId());
                              data.put("name", name);
                              data.put("alertType", "FRIEND_REQUEST");
                              data.put("badge", "Increment");
                            } catch(JSONException e1) {
                              e1.printStackTrace();
                            }
                            push.setData(data);
                            push.sendInBackground();
                            // Request sent
                            txtBtnAddFriend.setText(R.string.resend);
                          } else {
                            if(Modules.isWeakReferenceValid()) {
                              Singleton.sendToast(e);
                            }
                          }
                          isFriendshipActionInProgress = false;
                          rlBtnAddFriend.setBackgroundResource(R.drawable.bg_cell_join);
                        }
                      });
                    }
                  } else {
                    isFriendshipActionInProgress = false;
                    rlBtnAddFriend.setBackgroundResource(R.drawable.bg_cell_join);
                    if(Modules.isWeakReferenceValid()) {
                      Singleton.sendToast(e);
                    }
                  }
                }
              });
            }
          } else {
            isFriendshipActionInProgress = false;
            rlBtnAddFriend.setBackgroundResource(R.drawable.bg_cell_join);
            // Seems like the list is empty (Control might never come in this clause)
            LogEvent.Log("ImportContactsActivity", "Spam users list is null");
          }
        } else {
          if(Modules.isWeakReferenceValid()) {
            Singleton.sendToast(e);
          }
          isFriendshipActionInProgress = false;
          rlBtnAddFriend.setBackgroundResource(R.drawable.bg_cell_join);
        }
      }
    });
  }

  private void showAlertDialog() {
    AlertDialog.Builder alert = new AlertDialog.Builder(this);
    alert.setMessage(R.string.cannot_send_friend_request);
    alert.setPositiveButton(R.string.dialog_btn_ok, new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialogInterface, int i) {
      }
    });
    AlertDialog dialog = alert.create();
    dialog.show();
  }

  private void showDeleteFriendDialog(final ParseUser parseUser, String firstName,
                                      String lastName) {
    AlertDialog.Builder alert = new AlertDialog.Builder(this);
    alert.setMessage(getString(R.string.dialog_msg_unfriend, firstName + " " + lastName));
    alert.setNegativeButton(R.string.dialog_btn_cancel, new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialogInterface, int i) {
      }
    });
    alert.setPositiveButton(R.string.dialog_btn_ok, new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialogInterface, int i) {
        isFriendshipActionInProgress = true;
        rlBtnAddFriend.setBackgroundResource(R.drawable.bg_cell_join_processing);
        ParseUser currentUser = ParseUser.getCurrentUser();
        ParseRelation<ParseUser> relation = currentUser.getRelation("friends");
        relation.remove(parseUser);
        currentUser.saveInBackground(new SaveCallback() {
          @Override
          public void done(ParseException e) {
            if(e == null) {
              Singleton.INSTANCE.removeFriendFromList(parseUser.getObjectId());
              //imgBtnAddFriend.setImageResource(R.drawable.ic_add_friend);
              if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                imgBtnAddFriend.setImageTintList(ColorStateList.valueOf(COLOR_WHITE));
              }
              rlBtnAddFriend.setBackgroundResource(R.drawable.bg_cell_join);
              txtBtnAddFriend.setText(R.string.add_friend);
              txtBtnAddFriend.setTextColor(Color.WHITE);
              isFriend = false;
              if(Singleton.INSTANCE.getCells() == null) {
                ParseQuery<ParseObject> cellQuery = ParseQuery.getQuery("Cell");
                cellQuery.whereEqualTo("createdBy", ParseUser.getCurrentUser());
                cellQuery.whereNotEqualTo("type", 5);
                cellQuery.include("members");
                cellQuery.findInBackground(new FindCallback<ParseObject>() {
                  @Override
                  public void done(List<ParseObject> parseObjects, ParseException e) {
                    if(e == null) {
                      ArrayList<Cell> cellList = new ArrayList<>();
                      if(parseObjects != null) {
                        for(int i = 0; i < parseObjects.size(); i++) {
                          ParseObject cell = parseObjects.get(i);
                          Cell cl = new Cell((String) cell.get("name"), cell);
                          if(cell.getList("members") != null) {
                            LogEvent.Log("Cell",
                              "There are some members in" + " " + cell.get("name") + " " +
                                "newPrivateCell");
                            List<ParseUser> members = cell.getList("members");
                            // Check if deleted friend exists in this newPrivateCell, then delete it
                            boolean friendFoundInCell = false;
                            for(int j = 0; j < members.size(); j++) {
                              if(members.get(j).getObjectId().equals(parseUser.getObjectId())) {
                                members.remove(j);
                                --j;
                                friendFoundInCell = true;
                              }
                            }
                            if(friendFoundInCell) {
                              cell.put("members", members);
                              cell.saveEventually();
                            }
                            cl.members.addAll(members);
                          } else {
                            LogEvent.Log("Cell",
                              "There are no members in " + cell.get("name") + " newPrivateCell");
                          }
                          cellList.add(cl);
                        }
                        Singleton.INSTANCE.setCells((ArrayList<Cell>) cellList.clone());
                      }
                    } else {
                      if(MainActivity.weakRef.get() != null &&
                           !MainActivity.weakRef.get().isFinishing()) {
                        Singleton.sendToast(e);
                      }
                    }
                  }
                });
              } else {
                ArrayList<Cell> cellList =
                  (ArrayList<Cell>) Singleton.INSTANCE.getCells().clone();
                // iterate through cells
                for(int i = 0; i < cellList.size(); i++) {
                  Cell cell = cellList.get(i);
                  if(cell.members != null && cell.members.size() != 0) {
                    // Check if deleted friend exists in this newPrivateCell, then delete it
                    boolean friendFoundInCell = false;
                    for(int j = 0; j < cell.members.size(); j++) {
                      if(cell.members.get(j).getObjectId().equals(parseUser.getObjectId())) {
                        cell.members.remove(j);
                        --j;
                        friendFoundInCell = true;
                      }
                    }
                    if(friendFoundInCell) {
                      cell.cell.put("members", cell.members);
                      cell.cell.saveEventually();
                    }
                  }
                }
                Singleton.INSTANCE.setCells((ArrayList<Cell>) cellList.clone());
              }
            } else {
              rlBtnAddFriend.setBackgroundResource(R.drawable.bg_un_friend);
              if(MainActivity.weakRef.get() != null && !MainActivity.weakRef.get().isFinishing()) {
                Singleton.sendToast(e);
              }
            }
            isFriendshipActionInProgress = false;
          }
        });
      }
    });
    AlertDialog dialog = alert.create();
    dialog.show();
  }

  private void showFlagAlertDialog(final String firstName, final String lastName,
                                   final String userObjectId) {
    AlertDialog.Builder alert = new AlertDialog.Builder(this);
    alert.setMessage(getString(R.string.dialog_msg_flag_user, firstName + " " + lastName));
    alert.setPositiveButton(R.string.dialog_btn_yes, new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialogInterface, int i) {
        flagUser(firstName, lastName, userObjectId);
      }
    });
    alert.setNegativeButton(R.string.dialog_btn_no, new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialogInterface, int i) {
      }
    });
    AlertDialog dialog = alert.create();
    dialog.show();
  }

  private void flagUser(final String firstName, final String lastName, final String userObjectId) {
    isSpammingActionInProgress = true;
    rlBtnSpam.setBackgroundResource(R.drawable.bg_user_flagging);
    // Make an entry on both end
    ParseQuery<ParseUser> userQuery = ParseUser.getQuery();
    userQuery.whereEqualTo("objectId", userObjectId);
    userQuery.getFirstInBackground(new GetCallback<ParseUser>() {
      @Override
      public void done(ParseUser parseUser, ParseException e) {
        if(e == null) {
          ParseRelation<ParseUser> relation = ParseUser.getCurrentUser().getRelation("spamUsers");
          relation.add(parseUser);
          ParseUser.getCurrentUser().saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
              if(e == null) {
                ParseObject taskObject = new ParseObject("Task");
                taskObject.put("userId", userObjectId);
                taskObject.put("assigneeUserId", ParseUser.getCurrentUser().getObjectId());
                taskObject.put("task", "SPAM_ADD");
                taskObject.put("status", "PENDING");
                taskObject.saveInBackground(new SaveCallback() {
                  @Override
                  public void done(ParseException e) {
                    if(e == null) {
                      isSpammingActionInProgress = false;
                      isSpammed = true;
                      rlBtnSpam.setVisibility(View.GONE);
                      llActions.setWeightSum(1);
                      Singleton.INSTANCE.getSpammedUsersIdList().add(userObjectId);
                      if(weakRef.get() != null && !weakRef.get().isFinishing()) {
                        getApplicationContext();
                        Singleton.sendToast(firstName + " " + lastName + " " +
                                              getString(
                                                R.string.blocked_successfully));
                      }
                    } else {
                      isSpammingActionInProgress = false;
                      isSpammed = false;
                      rlBtnSpam.setBackgroundResource(R.drawable.bg_user_flag);
                      if(weakRef.get() != null && !weakRef.get().isFinishing()) {
                        Singleton.sendToast(e);
                      }
                    }
                    Singleton.INSTANCE.getSpammingUsersIdList().remove(userObjectId);
                  }
                });
              } else {
                isSpammingActionInProgress = false;
                isSpammed = false;
                Singleton.INSTANCE.getSpammingUsersIdList().remove(userObjectId);
                rlBtnSpam.setBackgroundResource(R.drawable.bg_user_flag);
                if(weakRef.get() != null && !weakRef.get().isFinishing()) {
                  Singleton.sendToast(e);
                }
              }
            }
          });
        } else {
          isSpammingActionInProgress = false;
          isSpammed = false;
          Singleton.INSTANCE.getSpammingUsersIdList().remove(userObjectId);
          rlBtnSpam.setBackgroundResource(R.drawable.bg_user_flag);
          if(weakRef.get() != null && !weakRef.get().isFinishing()) {
            Singleton.sendToast(e);
          }
        }
      }
    });
  }

  protected void startIntentService(double lat, double lng) {
    Intent intent = new Intent(this, FetchAddressIntentService.class);
    intent.putExtra(Constants.RECEIVER, mResultReceiver);
    intent.putExtra(Constants.LOCATION_DATA_LAT, lat);
    intent.putExtra(Constants.LOCATION_DATA_LNG, lng);
    FetchAddressIntentService.enqueueWork(this, intent);
  }

  class AddressResultReceiver extends ResultReceiver {
    public AddressResultReceiver(Handler handler) {
      super(handler);
    }

    @Override
    protected void onReceiveResult(int resultCode, Bundle resultData) {
      // Display the city
      if(resultCode == Constants.SUCCESS_RESULT) {
        String city = resultData.getString(Constants.RESULT_DATA_CITY);
        txtCityName.setText(city);
      } else {
        txtCityName.setText(R.string.city_not_found);
      }
    }
  }
}