package cell411.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Service;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.ActionBar;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.parse.ParseException;
import com.parse.ParseGeoPoint;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParsePush;
import com.parse.ParseQuery;
import com.parse.ParseRelation;
import com.parse.ParseUser;
import com.parse.callback.FindCallback;
import com.parse.callback.GetCallback;
import com.parse.callback.SaveCallback;
import com.safearx.cell411.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import cell411.Singleton;
import cell411.constants.Constants;
import cell411.db.DataSource;
import cell411.models.Cell;
import cell411.models.ChatRoom;
import cell411.models.PublicCell;
import cell411.models.User;
import cell411.services.FetchAddressIntentService;
import cell411.utils.LogEvent;
import cell411.widgets.CircularImageView;
import cell411.widgets.Spinner;

/**
 * Created by Sachin on 7/13/2015.
 */
public class PublicCellMembersActivity extends BaseActivity {
  private static final String TAG = "PublicCellMembersActivity";
  public static WeakReference<PublicCellMembersActivity> weakRef;
  private final int LIMIT_PER_PAGE = 10;
  private ListView listView;
  private ArrayList<User> membersList;
  private FriendsListAdapter adapterFriends;
  private boolean showJoin;
  private Spinner pb;
  private int page = 0;
  private boolean noMoreData = false;
  private boolean apiCallInProgress = false;
  private Handler handler = new Handler();
  // Views from the Footer
  private View footer;
  private ProgressBar pbFooter;
  private TextView moreInfoTV;
  private ParseObject parseObjectPublicCell;
  private ParseObject parseObjectVR;
  private boolean vrCheckCompleted;
  private String initialStatus;
  private String status;
  private int members;
  private TextView txtStatus;
  private TextView txtTotalMembers;
  private String cellId;
  private String cellName;
  private int verificationStatus;
  private boolean isAdmin = false;
  private TextView txtAddress;
  private AddressResultReceiver mResultReceiver;
  private double lat;
  private double lon;
  private String description;
  //private String category;
  private Integer cellType;
  private boolean isSecurityGuardsAlertsEnabled;
  private MenuItem miJoin;
  private boolean isProcessing = false;
  private User currentUser;
  private FloatingActionButton fabChat;
  private String city;
  private String country;
  private String fullAddress;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_public_cell_members);
    Singleton.INSTANCE.setCurrentActivity(this);
    weakRef = new WeakReference<>(this);
    isSecurityGuardsAlertsEnabled = false;
    cellId = getIntent().getStringExtra("cellId");
    cellName = getIntent().getStringExtra("cellName");
    initialStatus = status = getIntent().getStringExtra("status");
    members = getIntent().getIntExtra("totalMembers", 1);
    lat = getIntent().getDoubleExtra("lat", 0);
    lon = getIntent().getDoubleExtra("lon", 0);
    description = getIntent().getStringExtra("description");
    //category = getIntent().getStringExtra("category");
    cellType = getIntent().getIntExtra("cellType", 1);
    verificationStatus = getIntent().getIntExtra("verificationStatus", 0);
    txtAddress = (TextView) findViewById(R.id.txt_address);
    city = getIntent().getStringExtra("city");
    country = getIntent().getStringExtra("country");
    fullAddress = getIntent().getStringExtra("fullAddress");
    initUI();
    final ActionBar actionBar = getSupportActionBar();
    actionBar.setDisplayHomeAsUpEnabled(true);
    actionBar.setDisplayShowHomeEnabled(true);
    txtStatus = (TextView) findViewById(R.id.txt_status);
    txtTotalMembers = (TextView) findViewById(R.id.txt_total_members);
    fabChat = (FloatingActionButton) findViewById(R.id.fab_request_ride);
    if(members == 1) {
      txtTotalMembers.setText(R.string.one_meber);
    } else {
      txtTotalMembers.setText(members + " " + getString(R.string.members));
    }
    if(status.equals("OWNER")) {
      txtStatus.setText(R.string.you_are_the_owner_of_this_cell);
      showJoin = false;
      isAdmin = true;
    } else if(status.equals("JOINED")) {
      txtStatus.setText(R.string.you_are_a_member_of_this_cell);
      showJoin = true;
    } else if(status.equals("PENDING")) {
      fabChat.hide();
      txtStatus.setText(R.string.request_sent_for_approval);
      showJoin = false;
    } else {
      fabChat.hide();
      txtStatus.setText(R.string.you_are_not_a_member_of_this_cell);
      showJoin = true;
      ParseUser user = ParseUser.getCurrentUser();
      String email = null;
      if(user.getEmail() != null && !user.getEmail().isEmpty()) {
        email = user.getEmail();
      } else {
        email = user.getUsername();
      }
      currentUser =
        new User(email, (String) user.get("firstName"), (String) user.get("lastName"), user);
    }
    if(!getResources().getBoolean(R.bool.is_chat_enabled)) {
      fabChat.hide();
    }
    membersList = new ArrayList<>();
    listView = (ListView) findViewById(R.id.list_members);
    pb = (Spinner) findViewById(R.id.spinner);
    // Add footer to the list that will display a progressbar
    footer = getLayoutInflater().inflate(R.layout.cell_footer, null);
    pbFooter = (ProgressBar) footer.findViewById(R.id.pb);
    moreInfoTV = (TextView) footer.findViewById(R.id.txt_info);
    listView.addFooterView(footer, null, false);
    pb.setVisibility(View.VISIBLE);
    ParseQuery parseQuery = ParseQuery.getQuery("PublicCell");
    parseQuery.whereEqualTo("objectId", cellId);
    parseQuery.getFirstInBackground(new GetCallback<ParseObject>() {
      @Override
      public void done(ParseObject parseObject, ParseException e) {
        if(e == null) {
          PublicCellMembersActivity.this.parseObjectPublicCell = parseObject;
                    /*ParseQuery parseQueryVR = ParseQuery.getQuery("VerificationRequest");
                    parseQueryVR.whereEqualTo("cell", parseObject);
                    parseQueryVR.getFirstInBackground(new GetCallback<ParseObject>() {
                        @Override
                        public void done(ParseObject parseObject, ParseException e) {
                            vrCheckCompleted = true;
                            if (e == null) {
                                PublicCellMembersActivity.this.parseObjectVR = parseObject;
                            } else {
                            }
                        }
                    });*/
          // init friendship button
          if(Singleton.INSTANCE.getFriends() !=
               null) { // friend list already retrieved somewhere in the app
            retrieveCellMembers();
            setListScrollListener();
          } else { // check if the current user is a friend of this user
            ParseRelation relation = ParseUser.getCurrentUser().getRelation("friends");
            ParseQuery<ParseUser> query = relation.getQuery();
            query.findInBackground(new FindCallback<ParseUser>() {
              @Override
              public void done(List<ParseUser> list, ParseException e) {
                if(e == null) {
                  if(list != null && list.size() > 0) {
                    ArrayList<User> friendList = new ArrayList<>();
                    if(list != null) {
                      for(int i = 0; i < list.size(); i++) {
                        ParseUser user = (ParseUser) list.get(i);
                        String email = null;
                        if(user.getEmail() != null && !user.getEmail().isEmpty()) {
                          email = user.getEmail();
                        } else {
                          email = user.getUsername();
                        }
                        friendList.add(new User(email, (String) user.get("firstName"),
                          (String) user.get("lastName"), user));
                      }
                    }
                    Singleton.INSTANCE.setFriends((ArrayList<User>) friendList.clone());
                  }
                  retrieveCellMembers();
                  setListScrollListener();
                } else {
                  Singleton.sendToast(e);
                }
              }
            });
          }
        } else {
          if(e.getCode() == 101) {
            // Cell does not exists anymore, so show appropriate message to the user and then exit
            showCellDeletedAlertDialog();
          } else {
            pb.setVisibility(View.GONE);
            Singleton.sendToast(e);
          }
        }
      }
    });
    fabChat.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        Intent intentChat = new Intent(PublicCellMembersActivity.this, ChatActivity.class);
        intentChat.putExtra("entityType", ChatRoom.EntityType.PUBLIC_CELL.toString());
        intentChat.putExtra("entityObjectId", cellId);
        intentChat.putExtra("entityName", cellName);
        startActivity(intentChat);
      }
    });
  }

  private void initUI() {
    setTitle(cellName);
    TextView txtCellName = (TextView) findViewById(R.id.txt_cell_name);
    TextView txtCellCategory = (TextView) findViewById(R.id.txt_cell_category);
    TextView txtDescription = (TextView) findViewById(R.id.txt_description);
    txtCellName.setText(cellName);
    if(cellType != null) {
      switch(cellType) {
        case 1:
          txtCellCategory.setText(getString(R.string.category_cell_activism));
          break;
        case 2:
          txtCellCategory.setText(getString(R.string.category_cell_commercial));
          break;
        case 3:
          txtCellCategory.setText(getString(R.string.category_cell_community_safety));
          break;
        case 4:
          txtCellCategory.setText(getString(R.string.category_cell_education));
          break;
        case 5:
          txtCellCategory.setText(getString(R.string.category_cell_government));
          break;
        case 6:
          txtCellCategory.setText(getString(R.string.category_cell_journalism));
          break;
        case 7:
          txtCellCategory.setText(getString(R.string.category_cell_personal_safety));
          break;
      }
    } else {
      txtCellCategory.setText(R.string.category_not_available);
    }
    if(description != null) {
      txtDescription.setText(description);
    } else {
      txtDescription.setText(R.string.not_available);
    }
    if(city != null && !city.isEmpty()) {
      txtAddress.setText(city);
    } else {
      // Initialize the receiver and start the service for reverse geo coded address
      mResultReceiver = new AddressResultReceiver(new Handler());
      startIntentService();
    }
  }

  protected void startIntentService() {
    Intent intent = new Intent(this, FetchAddressIntentService.class);
    intent.putExtra(Constants.RECEIVER, mResultReceiver);
    intent.putExtra(Constants.LOCATION_DATA_LAT, lat);
    intent.putExtra(Constants.LOCATION_DATA_LNG, lon);
    FetchAddressIntentService.enqueueWork(this, intent);
  }

  @Override
  protected void onResume() {
    super.onResume();
    refreshList();
  }

  public void refreshList() {
    if(adapterFriends != null) {
      for(int i = 0; i < adapterFriends.friendsList.size(); i++) {
        if(Singleton.INSTANCE.getFriends() != null) {
          // Check if the current user is a friend of this user
          for(int j = 0; j < Singleton.INSTANCE.getFriends().size(); j++) {
            if(adapterFriends.friendsList.get(i).user == null
                 // If null, then this user row is Call Center
                 || Singleton.INSTANCE.getFriends().get(j).user.getObjectId().equals(
              adapterFriends.friendsList.get(i).user.getObjectId())) {
              adapterFriends.friendsList.get(i).isFriend = true;
              break;
            }
          }
        }
      }
      adapterFriends.notifyDataSetChanged();
    }
    if(Singleton.INSTANCE.getTotalPlacesWherePublicCellChangeRequired() > 0) {
      Singleton.INSTANCE.setTotalPlacesWherePublicCellChangeRequired(1);
      PublicCell publicCell = Singleton.INSTANCE.getChangedPublicCell();
      cellName = publicCell.getName();
      cellType = publicCell.cellType;
      description = publicCell.description;
      lat = publicCell.lat;
      lon = publicCell.lng;
      city = publicCell.city;
      country = publicCell.country;
      fullAddress = publicCell.fullAddress;
      initUI();
    }
  }

  @Override
  protected void onPause() {
    super.onPause();
    if(initialStatus.equals(status)) {
      Singleton.INSTANCE.setPublicCellStatus(Cell.Status.UN_CHANGED);
    } else if(status.equals("PENDING")) {
      Singleton.INSTANCE.setPublicCellStatus(Cell.Status.PENDING);
    } else {
      Singleton.INSTANCE.setPublicCellStatus(Cell.Status.NOT_JOINED);
      Singleton.INSTANCE.setPublicCellStatus2(Cell.Status.NOT_JOINED);
    }
  }

  private void retrieveCellMembers() {
    apiCallInProgress = true;
    ParseRelation relMembers = parseObjectPublicCell.getRelation("members");
    ParseQuery query4Members = relMembers.getQuery();
    query4Members.setLimit(LIMIT_PER_PAGE);
    query4Members.setSkip(LIMIT_PER_PAGE * page);
    query4Members.findInBackground(new FindCallback<ParseObject>() {
      @Override
      public void done(List list, ParseException e) {
        pb.setVisibility(View.GONE);
        apiCallInProgress = false;
        if(e == null) {
          ArrayList<User> friendList = new ArrayList<>();
          for(int i = 0; i < list.size(); i++) {
            ParseUser parseUser = (ParseUser) list.get(i);
            String email = null;
            if(parseUser.getEmail() != null && !parseUser.getEmail().isEmpty()) {
              email = parseUser.getEmail();
            } else {
              email = parseUser.getUsername();
            }
            User user = new User(email, (String) parseUser.get("firstName"),
              (String) parseUser.get("lastName"), parseUser);
            if(currentUser == null) {
              if(parseUser.getObjectId().
                                          equals(ParseUser.getCurrentUser().getObjectId())) {
                currentUser = user;
              }
            }
            if(Singleton.INSTANCE.getFriends() != null) {
              // Check if the current user is a friend of this user
              for(int j = 0; j < Singleton.INSTANCE.getFriends().size(); j++) {
                if(Singleton.INSTANCE.getFriends().get(j).user.getObjectId().equals(
                  parseUser.getObjectId())) {
                  user.isFriend = true;
                  break;
                }
              }
            }
            friendList.add(user);
          }
          if(list.size() < 10) {
            noMoreData = true;
          } else {
            page++;
          }
          if(isSecurityGuardsAlertsEnabled && membersList.size() == 0) {
            friendList.add(0, new User(null, getString(R.string.app_name), "Call Centre", null));
          }
          membersList.addAll(friendList);
          if(weakRef.get() != null && !weakRef.get().isFinishing()) {
            if(adapterFriends == null) {
              adapterFriends =
                new FriendsListAdapter(PublicCellMembersActivity.this, R.layout.cell_friends,
                  friendList);
              listView.setAdapter(adapterFriends);
              listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int position,
                                        long l) {
                  if((membersList.get(position)).user == null) {
                    // This user row is Call Center
                    return;
                  }
                  if((membersList.get(position)).user.getInt("isDeleted") == 1) {
                    // This user is deleted
                    return;
                  }
                  if((membersList.get(position)).user.getObjectId().equals(
                    ParseUser.getCurrentUser().getObjectId())) {
                    Intent intentProfileView =
                      new Intent(PublicCellMembersActivity.this, ProfileViewActivity.class);
                    startActivity(intentProfileView);
                  } else {
                    ParseGeoPoint parseGeoPoint =
                      (ParseGeoPoint) ((User) membersList.get(position)).user.get("location");
                    Intent intentUser =
                      new Intent(PublicCellMembersActivity.this, UserActivity.class);
                    intentUser.putExtra("userId", membersList.get(position).user.getObjectId());
                    intentUser.putExtra("imageName",
                      "" + membersList.get(position).user.get("imageName"));
                    intentUser.putExtra("firstName", membersList.get(position).firstName);
                    intentUser.putExtra("lastName", membersList.get(position).lastName);
                    intentUser.putExtra("username", membersList.get(position).user.getUsername());
                    intentUser.putExtra("email", membersList.get(position).user.getEmail());
                    if(parseGeoPoint != null) {
                      intentUser.putExtra("lat", parseGeoPoint.getLatitude());
                      intentUser.putExtra("lng", parseGeoPoint.getLongitude());
                    }
                    startActivity(intentUser);
                  }
                }
              });
            } else {
              adapterFriends.addAll(friendList);
            }
            adapterFriends.notifyDataSetChanged();
            if(noMoreData) {
              listView.setOnScrollListener(null);
              moreInfoTV.setText(R.string.no_more_members_to_load);
              pbFooter.setVisibility(View.GONE);
            }
          }
        } else {
          if(weakRef.get() != null && !weakRef.get().isFinishing()) {
            Singleton.sendToast(e);
          }
        }
      }
    });
  }

  private void setListScrollListener() {
    // register scroll listener to check when user reaches bottom of the list so that we can load more items
    listView.setOnScrollListener(new AbsListView.OnScrollListener() {
      boolean flag = false;

      @Override
      public void onScrollStateChanged(AbsListView absListView, int scrollState) {
        if(scrollState == SCROLL_STATE_FLING && !noMoreData) {
          flag = true;
          LogEvent.Log(TAG, "onScrollStateChanged invoked");
        }
      }

      @Override
      public void onScroll(AbsListView absListView, int firstVisibleItem, int visibleItemCount,
                           int totalItemCount) {
        int lastItem = firstVisibleItem + visibleItemCount;
        LogEvent.Log(TAG, "onScroll: " + lastItem);
        LogEvent.Log(TAG, "flag: " + flag);
        LogEvent.Log(TAG, "noMoreData: " + noMoreData);
        // inSearchMode is true, we will not load more items
        if(lastItem == totalItemCount && lastItem != 0 && !noMoreData) {
          LogEvent.Log(TAG, "onScroll: condition true");
          // Last item is fully visible.
          flag = false;
          handler.postDelayed(new Runnable() {
            @Override
            public void run() {
              loadMoreItems();
            }
          }, 300);
        } else {
          LogEvent.Log(TAG, "onScroll: condition false");
        }
      }
    });
  }

  /**
   * This method will load more items when user reaches bottom of the list.
   * It will set the text at the footer to "No more videos to load" when
   * there will be no more videos available.
   */
  private void loadMoreItems() {
    LogEvent.Log(TAG, "loadMoreItems invoked");
    if(!apiCallInProgress) {
      if(!noMoreData) {
        retrieveCellMembers();
      } else {
        listView.setOnScrollListener(null);
        moreInfoTV.setText(R.string.no_more_members_to_load);
        pbFooter.setVisibility(View.GONE);
      }
    }
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    if(showJoin) {
      MenuInflater inflater = getMenuInflater();
      inflater.inflate(R.menu.menu_public_cell_members, menu);
      miJoin = menu.findItem(R.id.action_join);
      return true;
    } else {
      if(status.equals("OWNER")) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_request_verification, menu);
        return true;
      } else {
        return super.onCreateOptionsMenu(menu);
      }
    }
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    switch(item.getItemId()) {
      case android.R.id.home:
        finish();
        return true;
      case R.id.action_join:
        if(showJoin) {
          if(parseObjectPublicCell != null && !isProcessing) {
            isProcessing = true;
            if(status.equals("JOINED")) {
              unJoinCell();
            } else {
              joinCell();
            }
          } else {
            Singleton.sendToast(R.string.please_wait);
          }
        }
        return true;
      case R.id.action_request_verification:
        showRequestVerificationDialog();
        return true;
      case R.id.action_edit_cell:
        Intent intentEditPublicCell =
          new Intent(PublicCellMembersActivity.this, CreateOrEditPublicCellActivity.class);
        intentEditPublicCell.putExtra("isInEditMode", true);
        intentEditPublicCell.putExtra("cellId", cellId);
        intentEditPublicCell.putExtra("cellName", cellName);
        intentEditPublicCell.putExtra("cellType", cellType);
        intentEditPublicCell.putExtra("description", description);
        intentEditPublicCell.putExtra("lat", lat);
        intentEditPublicCell.putExtra("lon", lon);
        intentEditPublicCell.putExtra("city", city);
        intentEditPublicCell.putExtra("country", country);
        intentEditPublicCell.putExtra("fullAddress", fullAddress);
        startActivity(intentEditPublicCell);
        return true;
      default:
        return super.onOptionsItemSelected(item);
    }
  }

  private void showRequestVerificationDialog() {
    AlertDialog.Builder alert = new AlertDialog.Builder(this);
    alert.setTitle(R.string.request_verification);
    LayoutInflater inflater = (LayoutInflater) getSystemService(Service.LAYOUT_INFLATER_SERVICE);
    View view = inflater.inflate(R.layout.layout_request_verification, null);
    final TextView txtBtnRequestVerification =
      (TextView) view.findViewById(R.id.txt_btn_request_verification);
    final ProgressBar pb = (ProgressBar) view.findViewById(R.id.pb);
    LogEvent.Log(TAG, "verificationStatus: " + verificationStatus);
    if(verificationStatus == 1) { // APPROVED
      txtBtnRequestVerification.setText(R.string.officially_verified);
      txtBtnRequestVerification.setBackgroundColor(Color.parseColor("#008000"));
      txtBtnRequestVerification.setEnabled(false);
    } else if(verificationStatus == -1) { // PENDING
      txtBtnRequestVerification.setText(R.string.verification_pending);
      txtBtnRequestVerification.setBackgroundColor(Color.GRAY);
      txtBtnRequestVerification.setEnabled(false);
    } else if(verificationStatus == -2) { // REJECTED
      txtBtnRequestVerification.setText(R.string.not_verified);
      txtBtnRequestVerification.setBackgroundColor(Color.RED);
      txtBtnRequestVerification.setEnabled(false);
    }
        /*if (parseObjectVR != null) {
            String status = (String) parseObjectVR.get("status");
            if (status.equals("APPROVED")) {
                txtBtnRequestVerification.setText(R.string.officially_verified);
                txtBtnRequestVerification.setBackgroundColor(Color.parseColor("#008000"));
                txtBtnRequestVerification.setEnabled(false);
            } else if (status.equals("PENDING")) {
                txtBtnRequestVerification.setText(R.string.verification_pending);
                txtBtnRequestVerification.setBackgroundColor(Color.GRAY);
                txtBtnRequestVerification.setEnabled(false);
            } else if (status.equals("REJECTED")) {
                txtBtnRequestVerification.setText(R.string.not_verified);
                txtBtnRequestVerification.setBackgroundColor(Color.RED);
                txtBtnRequestVerification.setEnabled(false);
            }
        }*/
    alert.setView(view);
    alert.setPositiveButton(R.string.dialog_btn_done, new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialog, int which) {
        dialog.dismiss();
      }
    });
    final AlertDialog dialog = alert.create();
    txtBtnRequestVerification.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        if(pb.getVisibility() != View.VISIBLE) {
          String email = null;
          if(ParseUser.getCurrentUser().getUsername().contains("@")) {
            email = ParseUser.getCurrentUser().getUsername();
          } else if(ParseUser.getCurrentUser().getEmail() != null &&
                      !ParseUser.getCurrentUser().getEmail().toString().isEmpty()) {
            email = ParseUser.getCurrentUser().getEmail();
          } else {
            showEmailRequiredDialog(pb, txtBtnRequestVerification);
            return;
          }
          requestVerificationOfPublicCell(email, pb, txtBtnRequestVerification);
        }
      }
    });
    dialog.show();
  }

  private void requestVerificationOfPublicCell(final String email, final ProgressBar pb,
                                               final TextView txtBtnRequestVerification) {
    pb.setVisibility(View.VISIBLE);
    {
      parseObjectPublicCell.put("verificationStatus", -1);
      parseObjectPublicCell.saveInBackground(new SaveCallback() {
        @Override
        public void done(ParseException e) {
          pb.setVisibility(View.GONE);
          if(e == null) {
            // Verification request sent successfully
            txtBtnRequestVerification.setText("VERIFICATION PENDING");
            txtBtnRequestVerification.setBackgroundColor(Color.GRAY);
            txtBtnRequestVerification.setEnabled(false);
          } else {
            Singleton.sendToast(e);
          }
        }
      });
    }
  }

  private void showEmailRequiredDialog(final ProgressBar pbRV,
                                       final TextView txtBtnRequestVerification) {
    android.app.AlertDialog.Builder alert = new android.app.AlertDialog.Builder(this);
    alert.setTitle(R.string.dialog_title_email);
    alert.setMessage(R.string.dialog_message_email_required);
    alert.setCancelable(false);
    LayoutInflater inflater = (LayoutInflater) getSystemService(Service.LAYOUT_INFLATER_SERVICE);
    View view = inflater.inflate(R.layout.dialog_email, null);
    final ProgressBar pb = (ProgressBar) view.findViewById(R.id.pb);
    final EditText etEmail = (EditText) view.findViewById(R.id.et_email);
    alert.setView(view);
    alert.setNegativeButton(R.string.dialog_btn_cancel, new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int arg1) {
        dialog.dismiss();
      }
    });
    alert.setPositiveButton(R.string.dialog_btn_submit, new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int which) {
      }
    });
    final android.app.AlertDialog dialog = alert.create();
    dialog.show();
    dialog.getButton(android.app.AlertDialog.BUTTON_POSITIVE).setOnClickListener(
      new View.OnClickListener() {
        @Override
        public void onClick(View v) {
          if(pb.getVisibility() != View.VISIBLE) {
            final String email = etEmail.getText().toString().trim();
            if(email.isEmpty()) {
              getApplicationContext();
              Singleton.sendToast(R.string.validation_email);
              return;
            } else if(!email.contains("@")) {
              getApplicationContext();
              Singleton.sendToast(R.string.validation_email_invalid);
              return;
            }
            pb.setVisibility(View.VISIBLE);
            ParseQuery<ParseUser> userParseQuery = ParseUser.getQuery();
            userParseQuery.whereEqualTo("username", email);
            ParseQuery<ParseUser> userParseQuery2 = ParseUser.getQuery();
            userParseQuery2.whereEqualTo("email", email);
            // Club all the queries into one master query
            List<ParseQuery<ParseUser>> queries = new ArrayList<>();
            queries.add(userParseQuery);
            queries.add(userParseQuery2);
            ParseQuery<ParseUser> mainQuery = ParseQuery.or(queries);
            mainQuery.findInBackground(new FindCallback<ParseUser>() {
              @Override
              public void done(List<ParseUser> list, ParseException e) {
                if(e == null) {
                  if(list == null || list.size() == 0) {
                    ParseUser parseUserCurrent = ParseUser.getCurrentUser();
                    parseUserCurrent.setEmail(email.toLowerCase().trim());
                    parseUserCurrent.saveInBackground(new SaveCallback() {
                      @Override
                      public void done(ParseException e) {
                        pb.setVisibility(View.GONE);
                        if(weakRef.get() != null && !weakRef.get().isFinishing()) {
                          if(e == null) {
                            getApplicationContext();
                            Singleton.sendToast(R.string.email_updated_successfully);
                            requestVerificationOfPublicCell(email, pbRV,
                              txtBtnRequestVerification);
                          } else {
                            Singleton.sendToast(e);
                          }
                        }
                        dialog.dismiss();
                      }
                    });
                  } else {
                    pb.setVisibility(View.GONE);
                    // display popup
                    showEmailAlreadyRegisteredAlert(email);
                  }
                } else {
                  pb.setVisibility(View.GONE);
                  if(weakRef.get() != null && !weakRef.get().isFinishing()) {
                    Singleton.sendToast(e);
                  }
                }
              }
            });
          }
        }
      });
  }

  public void showEmailAlreadyRegisteredAlert(String email) {
    AlertDialog.Builder alert = new AlertDialog.Builder(this);
    alert.setMessage(email + " " + getString(R.string.email_already_registered));
    alert.setPositiveButton(R.string.dialog_btn_ok, new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int which) {
        dialog.dismiss();
      }
    });
    alert.create().show();
  }

  @Override
  public boolean onPrepareOptionsMenu(Menu menu) {
    if(!showJoin && miJoin != null) {
      miJoin.setVisible(false);
    } else if(miJoin != null) {
      if(status.equals("JOINED")) {
        miJoin.setTitle(R.string.leave_this_cell);
      } else {
        miJoin.setTitle(R.string.join_this_cell);
      }
    }
    return super.onPrepareOptionsMenu(menu);
  }

  private void joinCell() {
    // Send Cell Add Request here
    final Cell cell = Singleton.INSTANCE.getTappedPublicCell();
    final ParseUser userCellOwner = cell.cell.getParseUser("createdBy");
    ParseRelation relation = ParseUser.getCurrentUser().getRelation("spammedBy");
    ParseQuery parseQuery = relation.getQuery();
    parseQuery.findInBackground(new FindCallback<ParseObject>() {
      @Override
      public void done(List list, ParseException e) {
        if(e == null) {
          if(list != null) {
            boolean userHasSpammedCurrentUser = false;
            for(int i = 0; i < list.size(); i++) {
              if(((ParseUser) list.get(i)).getObjectId().equals(userCellOwner.getObjectId())) {
                userHasSpammedCurrentUser = true;
                break;
              }
            }
            if(userHasSpammedCurrentUser) {
              showAlertDialog(getString(R.string.cannot_send_cell_join_request));
            } else {
              // Retrieve the new object from parse to check if it still exists or the owner has deleted
              ParseQuery parseQuery = ParseQuery.getQuery("PublicCell");
              parseQuery.whereEqualTo("objectId", parseObjectPublicCell.getObjectId());
              parseQuery.getFirstInBackground(new GetCallback<ParseObject>() {
                @Override
                public void done(ParseObject parseObject, ParseException e) {
                  if(e == null) {
                    final String name = ParseUser.getCurrentUser().get("firstName") + " " +
                                          ParseUser.getCurrentUser().get("lastName");
                    final ParseObject cellRequestObject = new ParseObject("Cell411Alert");
                    cellRequestObject.put("issuedBy", ParseUser.getCurrentUser());
                    cellRequestObject.put("issuerFirstName", name);
                    cellRequestObject.put("to", userCellOwner.getUsername());
                    cellRequestObject.put("status", "PENDING");
                    cellRequestObject.put("entryFor", "CR");
                    cellRequestObject.put("cellId", cellId);
                    cellRequestObject.put("cellName", cellName);
                    cellRequestObject.saveInBackground(new SaveCallback() {
                      @Override
                      public void done(ParseException e) {
                        if(e == null) {
                          // Send notification for friend request here
                          ParseQuery pushQuery = ParseInstallation.getQuery();
                          pushQuery.whereEqualTo("user", userCellOwner);
                          ParsePush push = new ParsePush();
                          push.setQuery(pushQuery);
                          JSONObject data = new JSONObject();
                          try {
                            data.put("alert",
                              getString(R.string.notif_cell_join_request_received, name,
                                cellName));
                            data.put("userId", ParseUser.getCurrentUser().getObjectId());
                            data.put("sound", "default");
                            data.put("cellRequestObjectId", cellRequestObject.getObjectId());
                            data.put("name", name);
                            data.put("cellId", cellId);
                            data.put("cellName", cellName);
                            data.put("alertType", "CELL_REQUEST");
                            data.put("badge", "Increment");
                          } catch(JSONException e1) {
                            e1.printStackTrace();
                          }
                          push.setData(data);
                          push.sendInBackground();
                          isProcessing = false;
                          status = "PENDING";
                          txtStatus.setText(R.string.request_sent_for_approval);
                          showJoin = false;
                          invalidateOptionsMenu();
                          if(weakRef.get() != null && !weakRef.get().isFinishing()) {
                            getApplicationContext();
                            Singleton.sendToast(
                              getString(R.string.notif_cell_join_request_sent, cellName));
                          }
                        } else {
                          isProcessing = false;
                          if(weakRef.get() != null && !weakRef.get().isFinishing()) {
                            Singleton.sendToast(e);
                          }
                        }
                      }
                    });
                  } else {
                    if(e.getCode() == 101) {
                      // Cell does not exists anymore, so show appropriate message
                      // to the user and then exit
                      showCellDeletedAlertDialog();
                    } else {
                      isProcessing = false;
                      if(weakRef.get() != null && !weakRef.get().isFinishing()) {
                        Singleton.sendToast(e);
                      }
                    }
                  }
                }
              });
            }
          } else {
            // Seems like the list is empty
            isProcessing = false;
            LogEvent.Log("SettingsActivity", "Spam users list is null");
          }
        } else {
          isProcessing = false;
          if(weakRef.get() != null && !weakRef.get().isFinishing()) {
            Singleton.sendToast(e);
          }
        }
      }
    });
  }

  private void showAlertDialog(String alertMessage) {
    AlertDialog.Builder alert = new AlertDialog.Builder(PublicCellMembersActivity.this);
    alert.setMessage(alertMessage);
    alert.setPositiveButton(R.string.dialog_btn_ok, new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialogInterface, int i) {
      }
    });
    AlertDialog dialog = alert.create();
    dialog.show();
  }

  private void showCellDeletedAlertDialog() {
    DataSource ds = new DataSource(this);
    ds.open();
    ds.updateIsRemovedFromChatRoom(cellId, true);
    ds.close();
    AlertDialog.Builder alert = new AlertDialog.Builder(PublicCellMembersActivity.this);
    alert.setMessage(R.string.opps_this_cell_no_longer_exist);
    alert.setOnCancelListener(new DialogInterface.OnCancelListener() {
      @Override
      public void onCancel(DialogInterface dialog) {
        Singleton.INSTANCE.setIsPublicCellDeleted(true);
        Singleton.INSTANCE.setIsPublicCellDeleted2(true);
        finish();
      }
    });
    alert.setPositiveButton(R.string.dialog_btn_ok, new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialogInterface, int i) {
        Singleton.INSTANCE.setIsPublicCellDeleted(true);
        Singleton.INSTANCE.setIsPublicCellDeleted2(true);
        finish();
      }
    });
    AlertDialog dialog = alert.create();
    dialog.show();
  }

  private void unJoinCell() {
    fabChat.hide();
    final Cell cell = Singleton.INSTANCE.getTappedPublicCell();
    ParseQuery<ParseObject> query = ParseQuery.getQuery("Cell411Alert");
    query.whereEqualTo("objectId", cell.cell411AlertId);
    LogEvent.Log(TAG, "cell.cell411AlertId: " + cell.cell411AlertId);
    query.getFirstInBackground(new GetCallback<ParseObject>() {
      @Override
      public void done(ParseObject parseObjectCell411Alert, ParseException e) {
        if(e == null) {
          parseObjectCell411Alert.put("status", "LEFT");
          parseObjectCell411Alert.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
              if(e == null) {
                // Retrieve the new object from parse to check if it still exists or the owner has deleted
                ParseQuery parseQuery = ParseQuery.getQuery("PublicCell");
                parseQuery.whereEqualTo("objectId", cell.cell.getObjectId());
                parseQuery.getFirstInBackground(new GetCallback<ParseObject>() {
                  @Override
                  public void done(final ParseObject parseObject, ParseException e) {
                    if(e == null) {
                      ParseRelation relation = parseObject.getRelation("members");
                      relation.remove(ParseUser.getCurrentUser());
                      parseObject.increment("totalMembers", -1);
                      parseObject.saveInBackground(new SaveCallback() {
                        @Override
                        public void done(ParseException e) {
                          isProcessing = false;
                          if(e == null) {
                            status = "NOT_JOINED";
                            members--;
                            txtStatus.setText(R.string.you_are_not_a_member_of_this_cell);
                            txtTotalMembers.setText(members + " " + getString(R.string.members));
                            invalidateOptionsMenu();
                            if(adapterFriends != null && currentUser != null) {
                              adapterFriends.friendsList.remove(currentUser);
                              adapterFriends.notifyDataSetChanged();
                            }
                            DataSource ds = new DataSource(PublicCellMembersActivity.this);
                            ds.open();
                            ds.updateIsRemovedFromChatRoom(cellId, true);
                            ds.close();
                            // Stop listening to the public newPrivateCell for chat messages
                                                        /*Intent chatServiceIntent = new Intent(PublicCellMembersActivity.this, ChatService.class);
                                                        chatServiceIntent.putExtra("ChatServiceTask", ChatService.ChatServiceTask.DETACH_LISTENER);
                                                        chatServiceIntent.putExtra("entityType", ChatRoom.EntityType.PUBLIC_CELL);
                                                        chatServiceIntent.putExtra("entityObjectId", cellId);
                                                        chatServiceIntent.putExtra("entityCreatedAt", String.valueOf(parseObject.getCreatedAt().getTime()));
                                                        chatServiceIntent.putExtra("entityName", cellName);
                                                        startService(chatServiceIntent);*/
                          } else {
                            LogEvent.Log(TAG, "Error in fourth block");
                            if(weakRef.get() != null && !weakRef.get().isFinishing()) {
                              Singleton.sendToast(e);
                            }
                          }
                        }
                      });
                    } else {
                      if(e.getCode() == 101) {
                        // Cell does not exists anymore, so show appropriate message
                        // to the user and then exit
                        showCellDeletedAlertDialog();
                      } else {
                        isProcessing = false;
                        LogEvent.Log(TAG, "Error in third block");
                        if(weakRef.get() != null && !weakRef.get().isFinishing()) {
                          Singleton.sendToast(e);
                        }
                      }
                    }
                  }
                });
              } else {
                LogEvent.Log(TAG, "Error in second block");
                isProcessing = false;
                if(weakRef.get() != null && !weakRef.get().isFinishing()) {
                  Singleton.sendToast(e);
                }
              }
            }
          });
        } else {
          LogEvent.Log(TAG, "Error in first block");
          isProcessing = false;
          if(weakRef.get() != null && !weakRef.get().isFinishing()) {
            Singleton.sendToast(e);
          }
        }
      }
    });
  }

  private static class ViewHolder {
    private TextView txtUserName;
    private CircularImageView imgUser;
    private ImageView imgTick;
    private TextView txtRemove;
    private TextView txtBtnAddFriend;
  }

  private class FriendsListAdapter extends ArrayAdapter<User> {
    private int resource;
    private LayoutInflater inflater;
    private Context context;
    private ArrayList<User> friendsList;

    public FriendsListAdapter(Context context, int resource, ArrayList<User> objects) {
      super(context, resource, objects);
      this.resource = resource;
      inflater = ((Activity) context).getLayoutInflater();
      this.context = context;
      friendsList = objects;
    }

    public int getCount() {
      return friendsList.size();
    }

    public View getView(final int position, View convertView1, ViewGroup parent) {
      View cellView = convertView1;
      ViewHolder holder = null;
      if(cellView == null) {
        holder = new ViewHolder();
        cellView = inflater.inflate(resource, null);
        holder.txtUserName = (TextView) cellView.findViewById(R.id.txt_user_name);
        holder.imgUser = (CircularImageView) cellView.findViewById(R.id.img_user);
        holder.imgTick = (ImageView) cellView.findViewById(R.id.img_tick);
        holder.txtRemove = (TextView) cellView.findViewById(R.id.txt_remove);
        holder.txtBtnAddFriend = (TextView) cellView.findViewById(R.id.txt_btn_add_friend);
        cellView.setTag(holder);
      } else {
        holder = (ViewHolder) cellView.getTag();
        holder.imgUser.setImageBitmap(null);
      }
      holder.txtUserName.setText(
        friendsList.get(position).firstName + " " + friendsList.get(position).lastName);
      if(friendsList.get(position).user != null &&
           friendsList.get(position).user.getInt("isDeleted") == 1) {
        holder.imgUser.setOnClickListener(null);
        holder.txtUserName.setTextColor(getResources().getColor(R.color.text_disabled_hint_icon));
      } else {
        holder.txtUserName.setTextColor(getResources().getColor(R.color.text_primary));
        holder.imgUser.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View view) {
            if((friendsList.get(position)).user == null) { // This user row is Call Center
              return;
            }
            String email = friendsList.get(position).user.getEmail();
            if(email == null || email.isEmpty()) {
              email = friendsList.get(position).user.getUsername();
            }
            Intent profileImageIntent =
              new Intent(PublicCellMembersActivity.this, ProfileImageActivity.class);
            profileImageIntent.putExtra("userId",
              "" + friendsList.get(position).user.getObjectId());
            profileImageIntent.putExtra("imageName",
              "" + friendsList.get(position).user.get("imageName"));
            profileImageIntent.putExtra("email", email);
            startActivity(profileImageIntent);
          }
        });
      }
      holder.imgTick.setVisibility(View.GONE);
      final ViewHolder finalHolder = holder;
      if(friendsList.get(position).email != null &&
           friendsList.get(position).user.getInt("isDeleted") != 1) {
        Singleton.INSTANCE.setImage(finalHolder.imgUser,
          friendsList.get(position).user.getObjectId() +
            friendsList.get(position).user.get("imageName"), friendsList.get(position).email);
      } else {
        finalHolder.imgUser.setImageResource(R.drawable.ic_placeholder_user);
      }
      if(friendsList.get(position).user != null && !friendsList.get(position).isFriend &&
           !friendsList.get(position).user.getObjectId().equals(
             ParseUser.getCurrentUser().getObjectId()) &&
           friendsList.get(position).user.getInt("isDeleted") != 1) {
        holder.txtBtnAddFriend.setVisibility(View.VISIBLE);
        holder.txtBtnAddFriend.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View v) {
            if(!friendsList.get(position).friendAddInProgress) {
              // Send friend request here
              friendsList.get(position).friendAddInProgress = true;
              finalHolder.txtBtnAddFriend.setBackgroundResource(R.drawable.bg_cell_join_processing);
              sendFriendRequest(friendsList.get(position).user, friendsList.get(position).email,
                position, finalHolder.txtBtnAddFriend);
            }
          }
        });
        if(friendsList.get(position).friendAddInProgress) {
          holder.txtBtnAddFriend.setBackgroundResource(R.drawable.bg_cell_join_processing);
        } else {
          holder.txtBtnAddFriend.setBackgroundResource(R.drawable.bg_cell_join);
        }
      } else {
        holder.txtBtnAddFriend.setVisibility(View.GONE);
      }
      if(friendsList.get(position).user != null && isAdmin &&
           !friendsList.get(position).user.getObjectId().equals(
             ParseUser.getCurrentUser().getObjectId()) &&
           friendsList.get(position).user.getInt("isDeleted") != 1) {
        holder.txtRemove.setVisibility(View.VISIBLE);
        holder.txtRemove.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View v) {
            if(!friendsList.get(position).friendRemoveInProgress) {
              // Send friend request here
              friendsList.get(position).friendRemoveInProgress = true;
              finalHolder.txtRemove.setBackgroundResource(R.drawable.bg_cell_join_processing);
              showRemoveMemberDialog(position);
            }
          }
        });
        if(friendsList.get(position).friendRemoveInProgress) {
          holder.txtRemove.setBackgroundResource(R.drawable.bg_cell_join_processing);
        } else {
          holder.txtRemove.setBackgroundResource(R.drawable.bg_cell_join);
        }
      } else {
        holder.txtRemove.setVisibility(View.GONE);
      }
      return cellView;
    }

    private void showRemoveMemberDialog(final int position) {
      AlertDialog.Builder alert = new AlertDialog.Builder(PublicCellMembersActivity.this);
      alert.setMessage(getString(R.string.dialog_msg_remove_member,
        friendsList.get(position).firstName + " " + friendsList.get(position).lastName));
      alert.setNegativeButton(R.string.dialog_btn_no, new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialogInterface, int i) {
          friendsList.get(position).friendRemoveInProgress = false;
          notifyDataSetChanged();
        }
      });
      alert.setPositiveButton(R.string.dialog_btn_yes, new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialogInterface, int i) {
          removeMemberFromCell(position);
        }
      });
      AlertDialog dialog = alert.create();
      dialog.show();
    }

    private void removeMemberFromCell(final int index) {
      ParseQuery<ParseObject> query = ParseQuery.getQuery("Cell411Alert");
      query.whereEqualTo("issuedBy", friendsList.get(index).user);
      query.whereEqualTo("entryFor", "CR");
      query.whereEqualTo("status", "APPROVED");
      query.whereEqualTo("cellId", cellId);
      query.getFirstInBackground(new GetCallback<ParseObject>() {
        @Override
        public void done(ParseObject parseObjectCell411Alert, ParseException e) {
          if(e == null) {
            parseObjectCell411Alert.put("status", "REMOVED");
            parseObjectCell411Alert.saveInBackground(new SaveCallback() {
              @Override
              public void done(ParseException e) {
                if(e == null) {
                  ParseRelation relation = parseObjectPublicCell.getRelation("members");
                  relation.remove(friendsList.get(index).user);
                  parseObjectPublicCell.increment("totalMembers", -1);
                  parseObjectPublicCell.saveInBackground(new SaveCallback() {
                    @Override
                    public void done(ParseException e) {
                      if(e == null) {
                        // Send notification
                        // Send notification for newPrivateCell approved here
                        ParseQuery pushQuery = ParseInstallation.getQuery();
                        pushQuery.whereEqualTo("user", friendsList.get(index).user);
                        ParsePush push = new ParsePush();
                        push.setQuery(pushQuery);
                        String name = ParseUser.getCurrentUser().get("firstName") + " " +
                                        ParseUser.getCurrentUser().get("lastName");
                        JSONObject data = new JSONObject();
                        try {
                          data.put("alert",
                            context.getString(R.string.notification_cell_removed, cellName));
                          data.put("userId", ParseUser.getCurrentUser().getObjectId());
                          data.put("sound", "");
                          data.put("name", name);
                          data.put("cellId", cellId);
                          data.put("cellName", cellName);
                          data.put("alertType", "CELL_REMOVED");
                          data.put("badge", "Increment");
                        } catch(JSONException e1) {
                          e1.printStackTrace();
                        }
                        push.setData(data);
                        push.sendInBackground();
                        members--;
                        txtTotalMembers.setText(members + " members");
                        friendsList.remove(index);
                        notifyDataSetChanged();
                        Singleton.INSTANCE.getTappedPublicCell().totalMembers--;
                      } else {
                        friendsList.get(index).friendRemoveInProgress = false;
                        notifyDataSetChanged();
                        if(weakRef.get() != null && !weakRef.get().isFinishing()) {
                          Singleton.sendToast(e);
                        }
                      }
                    }
                  });
                } else {
                  friendsList.get(index).friendRemoveInProgress = false;
                  notifyDataSetChanged();
                  if(weakRef.get() != null && !weakRef.get().isFinishing()) {
                    Singleton.sendToast(e);
                  }
                }
              }
            });
          } else {
            friendsList.get(index).friendRemoveInProgress = false;
            notifyDataSetChanged();
            if(weakRef.get() != null && !weakRef.get().isFinishing()) {
              Singleton.sendToast(e);
            }
          }
        }
      });
    }

    private void sendFriendRequest(final ParseUser parseUser, final String email,
                                   final int position, final TextView txtBtnAddFriend) {
      ParseRelation relation = ParseUser.getCurrentUser().getRelation("spammedBy");
      ParseQuery parseQuery = relation.getQuery();
      parseQuery.findInBackground(new FindCallback<ParseObject>() {
        @Override
        public void done(List list, ParseException e) {
          if(e == null) {
            if(list != null) {
              boolean userHasSpammedCurrentUser = false;
              for(int i = 0; i < list.size(); i++) {
                if((((ParseUser) list.get(i)).getEmail() != null &&
                      ((ParseUser) list.get(i)).getEmail().equals(email)) ||
                     ((ParseUser) list.get(i)).getUsername().equals(email)) {
                  userHasSpammedCurrentUser = true;
                  break;
                }
              }
              if(userHasSpammedCurrentUser) {
                friendsList.get(position).friendAddInProgress = false;
                txtBtnAddFriend.setBackgroundResource(R.drawable.bg_cell_join);
                showAlertDialog();
              } else {
                final String name = ParseUser.getCurrentUser().get("firstName") + " " +
                                      ParseUser.getCurrentUser().get("lastName");
                final ParseObject friendRequestObject = new ParseObject("Cell411Alert");
                friendRequestObject.put("issuedBy", ParseUser.getCurrentUser());
                friendRequestObject.put("issuerFirstName", name);
                friendRequestObject.put("to", email);
                friendRequestObject.put("status", "PENDING");
                friendRequestObject.put("entryFor", "FR");
                friendRequestObject.saveInBackground(new SaveCallback() {
                  @Override
                  public void done(ParseException e) {
                    if(e == null) {
                      // Send notification for friend request here
                      ParseQuery pushQuery = ParseInstallation.getQuery();
                      pushQuery.whereEqualTo("user", parseUser);
                      ParsePush push = new ParsePush();
                      push.setQuery(pushQuery);
                      JSONObject data = new JSONObject();
                      try {
                        data.put("alert", getString(R.string.notif_friend_request, name,
                          getString(R.string.app_name)));
                        data.put("userId", ParseUser.getCurrentUser().getObjectId());
                        data.put("sound", "default");
                        data.put("friendRequestObjectId", friendRequestObject.getObjectId());
                        data.put("name", name);
                        data.put("alertType", "FRIEND_REQUEST");
                        data.put("badge", "Increment");
                      } catch(JSONException e1) {
                        e1.printStackTrace();
                      }
                      push.setData(data);
                      push.sendInBackground();
                      // Request sent
                      txtBtnAddFriend.setText(R.string.resend);
                    } else {
                      Singleton.sendToast(e);
                    }
                    friendsList.get(position).friendAddInProgress = false;
                    txtBtnAddFriend.setBackgroundResource(R.drawable.bg_cell_join);
                  }
                });
              }
            } else {
              // Seems like the list is empty (Control might never come in this clause)
              LogEvent.Log("ImportContactsActivity", "Spam users list is null");
            }
          } else {
            friendsList.get(position).friendAddInProgress = false;
            txtBtnAddFriend.setBackgroundResource(R.drawable.bg_cell_join);
          }
        }
      });
    }

    private void showAlertDialog() {
      AlertDialog.Builder alert = new AlertDialog.Builder(PublicCellMembersActivity.this);
      alert.setMessage(R.string.cannot_send_friend_request);
      alert.setPositiveButton(R.string.dialog_btn_ok, new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialogInterface, int i) {
        }
      });
      AlertDialog dialog = alert.create();
      dialog.show();
    }
  }

  @SuppressLint("ParcelCreator")
  class AddressResultReceiver extends ResultReceiver {
    public AddressResultReceiver(Handler handler) {
      super(handler);
    }

    @Override
    protected void onReceiveResult(int resultCode, Bundle resultData) {
      // Display the city
      if(resultCode == Constants.SUCCESS_RESULT) {
        String city = resultData.getString(Constants.RESULT_DATA_CITY);
        txtAddress.setText(city);
      } else {
        txtAddress.setText(R.string.city_not_found);
      }
    }
  }
}
