package cell411.activity;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import androidx.appcompat.app.ActionBar;

import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseRelation;
import com.parse.ParseUser;
import com.parse.callback.FindCallback;
import com.parse.callback.GetCallback;
import com.parse.callback.SaveCallback;
import com.safearx.cell411.R;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import cell411.Singleton;
import cell411.models.User;
import cell411.utils.LogEvent;

/**
 * Created by Sachin on 7/13/2015.
 */
public class SpammedUsersActivity extends BaseActivity {
  private static final String TAG = "SpammedUsersActivity";
  public static WeakReference<SpammedUsersActivity> weakRef;
  private ListView listView;
  private ArrayList<User> spammedUsersArrayList;
  private SpamUsersListAdapter spamUsersListAdapter;
  private LinearLayout pb;
  private TextView txtLoading;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_spammed_users);
    weakRef = new WeakReference<>(this);
    Singleton.INSTANCE.setCurrentActivity(this);
    final ActionBar actionBar = getSupportActionBar();
    actionBar.setDisplayHomeAsUpEnabled(true);
    actionBar.setDisplayShowHomeEnabled(true);
    listView = (ListView) findViewById(R.id.list_friends);
    pb = (LinearLayout) findViewById(R.id.ll_loading_text);
    txtLoading = (TextView) findViewById(R.id.txt_loading);
    pb.setVisibility(View.VISIBLE);
    txtLoading.setText(R.string.loading_spammed_users);
    ParseRelation relation4SpamUsers = ParseUser.getCurrentUser().getRelation("spamUsers");
    ParseQuery<ParseUser> parseQuery4SpamUsers = relation4SpamUsers.getQuery();
    parseQuery4SpamUsers.findInBackground(new FindCallback<ParseUser>() {
      @Override
      public void done(List<ParseUser> parseUsers, ParseException e) {
        pb.setVisibility(View.GONE);
        if(e == null) {
          spammedUsersArrayList = new ArrayList<>();
          if(parseUsers != null) {
            for(int i = 0; i < parseUsers.size(); i++) {
              ParseUser user = parseUsers.get(i);
              if(user.getInt("isDeleted") == 1) {
                continue;
              }
              String email = null;
              if(user.getEmail() != null && !user.getEmail().isEmpty()) {
                email = user.getEmail();
              } else {
                email = user.getUsername();
              }
              spammedUsersArrayList.add(
                new User(email, (String) user.get("firstName"), (String) user.get("lastName"),
                  user));
            }
          }
          if(weakRef.get() != null && !weakRef.get().isFinishing()) {
            if(spammedUsersArrayList.size() == 0) {
              pb.setVisibility(View.VISIBLE);
              txtLoading.setText(R.string.you_have_not_spammed_any_user_as_spam);
            } else {
              spamUsersListAdapter =
                new SpamUsersListAdapter(SpammedUsersActivity.this, 0, spammedUsersArrayList);
              listView.setAdapter(spamUsersListAdapter);
            }
          }
        } else {
          Singleton.sendToast(e);
        }
      }
    });
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    switch(item.getItemId()) {
      case android.R.id.home:
        finish();
        return true;
      default:
        return super.onOptionsItemSelected(item);
    }
  }

  private static class ContactViewHolder {
    private TextView txtDisplayName;
    private ImageView imgUser;
    private TextView txtUnSpam;
  }

  private class SpamUsersListAdapter extends ArrayAdapter<User> {
    private LayoutInflater inflater;
    private ArrayList<User> arrayList;

    public SpamUsersListAdapter(Context context, int resource, ArrayList<User> objects) {
      super(context, resource, objects);
      inflater = ((Activity) context).getLayoutInflater();
      arrayList = objects;
    }

    public int getCount() {
      return arrayList.size();
    }

    public View getView(final int position, View convertView1, ViewGroup parent) {
      View cellView = convertView1;
      ContactViewHolder contactViewHolder = null;
      final User user = getItem(position);
      if(cellView == null) {
        contactViewHolder = new ContactViewHolder();
        cellView = inflater.inflate(R.layout.cell_spammed_user, null);
        contactViewHolder.txtDisplayName = (TextView) cellView.findViewById(R.id.txt_display_name);
        contactViewHolder.imgUser = (ImageView) cellView.findViewById(R.id.img_user);
        contactViewHolder.txtUnSpam = (TextView) cellView.findViewById(R.id.txt_un_spam);
        cellView.setTag(contactViewHolder);
      } else {
        contactViewHolder = (ContactViewHolder) cellView.getTag();
        //contactViewHolder.imgUser.setImageResource(R.drawable.logo);
      }
      contactViewHolder.txtDisplayName.setText(user.firstName + " " + user.lastName);
      contactViewHolder.imgUser.setImageResource(R.drawable.ic_placeholder_user);
      final ContactViewHolder finalHolder = contactViewHolder;
      Singleton.INSTANCE.setImage(finalHolder.imgUser,
        user.user.getObjectId() + user.user.get("imageName"), user.email);
      final ContactViewHolder holder = contactViewHolder;
      contactViewHolder.txtUnSpam.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
          unSpamUser(user);
          //remove(user);
          //notifyDataSetChanged();
        }
      });
      return cellView;
    }

    private void unSpamUser(final User user) {
      ParseQuery<ParseObject> parseQuery = ParseQuery.getQuery("Task");
      parseQuery.whereEqualTo("assigneeUserId", ParseUser.getCurrentUser().getObjectId());
      parseQuery.whereEqualTo("userId", user.user.getObjectId());
      parseQuery.whereEqualTo("task", "SPAM_ADD");
      parseQuery.whereEqualTo("status", "PENDING");
      parseQuery.getFirstInBackground(new GetCallback<ParseObject>() {
        @Override
        public void done(ParseObject parseObject, ParseException e) {
          spamUsersListAdapter.arrayList.remove(user);
          Singleton.INSTANCE.getSpammedUsersIdList().remove(user.user.getObjectId());
          spamUsersListAdapter.notifyDataSetChanged();
          if(spamUsersListAdapter.arrayList.size() == 0) {
            pb.setVisibility(View.VISIBLE);
            txtLoading.setText(R.string.you_have_not_spammed_any_user_as_spam);
          }
          if(e == null) {
            if(parseObject != null) {
              // The task was created which wasn't completed by the recipient, so lets remove it
              parseObject.deleteEventually();
              ParseRelation<ParseUser> relation =
                ParseUser.getCurrentUser().getRelation("spamUsers");
              relation.remove(user.user);
              ParseUser.getCurrentUser().saveInBackground();
            } else {
              ParseObject taskObject = new ParseObject("Task");
              taskObject.put("userId", user.user.getObjectId());
              taskObject.put("assigneeUserId", ParseUser.getCurrentUser().getObjectId());
              taskObject.put("task", "SPAM_REMOVE");
              taskObject.put("status", "PENDING");
              taskObject.saveInBackground(new SaveCallback() {
                @Override
                public void done(ParseException e) {
                  if(e == null) {
                    ParseRelation<ParseUser> relation =
                      ParseUser.getCurrentUser().getRelation("spamUsers");
                    relation.remove(user.user);
                    ParseUser.getCurrentUser().saveInBackground();
                  } else {
                    if(weakRef.get() != null && !weakRef.get().isFinishing()) {
                      Singleton.sendToast(e);
                    }
                  }
                }
              });
            }
          } else {
            LogEvent.Log(TAG, "Exception");
            if(e.getMessage().contains("no results found")) {
              ParseObject taskObject = new ParseObject("Task");
              taskObject.put("userId", user.user.getObjectId());
              taskObject.put("assigneeUserId", ParseUser.getCurrentUser().getObjectId());
              taskObject.put("task", "SPAM_REMOVE");
              taskObject.put("status", "PENDING");
              taskObject.saveInBackground(new SaveCallback() {
                @Override
                public void done(ParseException e) {
                  if(e == null) {
                    ParseRelation<ParseUser> relation =
                      ParseUser.getCurrentUser().getRelation("spamUsers");
                    relation.remove(user.user);
                    ParseUser.getCurrentUser().saveInBackground();
                  } else {
                    if(weakRef.get() != null && !weakRef.get().isFinishing()) {
                      Singleton.sendToast(e);
                    }
                  }
                }
              });
            } else {
              spamUsersListAdapter.arrayList.add(user);
              spamUsersListAdapter.notifyDataSetChanged();
              pb.setVisibility(View.GONE);
              if(weakRef.get() != null && !weakRef.get().isFinishing()) {
                Singleton.sendToast(e);
              }
            }
          }
        }
      });
    }
  }
}