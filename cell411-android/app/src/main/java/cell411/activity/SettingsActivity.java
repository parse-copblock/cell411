package cell411.activity;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentManager;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.callback.FunctionCallback;
import com.parse.callback.GetCallback;
import com.parse.callback.SaveCallback;
import com.safearx.cell411.R;

import java.lang.ref.WeakReference;
import java.util.Date;
import java.util.HashMap;

import cell411.Singleton;
import cell411.constants.ParseKeys;
import cell411.constants.Prefs;
import cell411.fragments.LocationFragment;
import cell411.interfaces.CurrentLocationFromLocationUpdatesCallBack;
import cell411.interfaces.LastLocationCallBack;
import cell411.interfaces.LocationFailToRetrieveCallBack;
import cell411.methods.Modules;
import cell411.receivers.GpsReceiver;
import cell411.utils.LogEvent;

/**
 * Created by Sachin on 19-04-2016.
 */
public class SettingsActivity extends BaseActivity
  implements LastLocationCallBack, LocationFailToRetrieveCallBack,
               CurrentLocationFromLocationUpdatesCallBack {
  private static final String TAG = SettingsActivity.class.getSimpleName();
  private static final int PERMISSION_CAMERA = 1;
  public static WeakReference<SettingsActivity> weakRef;
  private boolean isLocationAccuracyEnabled;
  private boolean isLocationUpdateEnabled;
  private boolean isDarkModeEnabled;
  private boolean isDispatchModeEnabled;
  private boolean isDeleteVideoEnabled;
  private boolean isUploadContactsEnabled;
  private boolean rideRequests = true;
  private int patrolModeRadius;
  private int patrolMode;
  private int newPublicCellAlert;
  private String metric;
  private FloatingActionButton fabRideRequests;
  private FloatingActionButton fabNewPublicCellAlert;
  private FloatingActionButton fabPatrolMode;
  private FloatingActionButton fabUploadContacts;
  private FloatingActionButton fabLocationUpdates;
  private FloatingActionButton fabPublishAlertToFacebook;
  private FloatingActionButton fabDarkMode;
  private RelativeLayout rlConnectToFacebook;
  private LinearLayout llConnectToFacebook;
  private LinearLayout llDisconnectFromFacebook;
  private TextView txtDownloadData;
  private int COLOR_ACCENT;
  private int COLOR_GRAY_CCC;
  private SharedPreferences prefs;
  private LocationFragment locationFragment;
  private FloatingActionButton fabDeleteVideoOption;

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    switch(item.getItemId()) {
      case android.R.id.home:
        finish();
        return true;
      default:
        return super.onOptionsItemSelected(item);
    }
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_settings);
    // Set up the action bar.
    final ActionBar actionBar = getSupportActionBar();
    actionBar.setDisplayHomeAsUpEnabled(true);
    Singleton.INSTANCE.setCurrentActivity(this);
    weakRef = new WeakReference<>(this);
    locationFragment = new LocationFragment();
    FragmentManager fragmentManager = getSupportFragmentManager();
    fragmentManager.beginTransaction().add(locationFragment, "locationFragment").commit();
    COLOR_ACCENT = getResources().getColor(R.color.colorAccent);
    COLOR_GRAY_CCC = getResources().getColor(R.color.gray_ccc);
    fabDarkMode = findViewById(R.id.fab_dark_mode);
    fabNewPublicCellAlert = findViewById(R.id.fab_new_public_cell_alert);
    final TextView txtLblMiles = (TextView) findViewById(R.id.txt_lbl_miles);
    final TextView txtLblKilometers = (TextView) findViewById(R.id.txt_lbl_kilometers);
    final TextView txtLblPatrolRange = (TextView) findViewById(R.id.txt_lbl_patrol_range);
    final TextView txtPatrolRadius = (TextView) findViewById(R.id.txt_patrol_radius);
    final FloatingActionButton fabGPSAccurateTracking =
      (FloatingActionButton) findViewById(R.id.fab_gps_accurate_tracking);
    fabUploadContacts = (FloatingActionButton) findViewById(R.id.fab_upload_contact_option);
    fabLocationUpdates = (FloatingActionButton) findViewById(R.id.fab_location_updates);
    // disable contacts upload setting for iER
        /*if (getResources().getInteger(R.integer.client_firm_id) == 2) {
            findViewById(R.id.rl_upload_contact_option).setVisibility(View.GONE);
        }*/
    prefs = Singleton.INSTANCE.getAppPrefs();
    metric = prefs.getString("metric", "kms");
    isDarkModeEnabled = prefs.getBoolean("DarkMode", false);
    isLocationAccuracyEnabled = prefs.getBoolean("LocationAccuracy", true);
    isLocationUpdateEnabled = prefs.getBoolean("LocationUpdate", true);
    if(ParseUser.getCurrentUser().get("newPublicCellAlert") != null) {
      newPublicCellAlert = (int) ParseUser.getCurrentUser().get("newPublicCellAlert");
    }
    fabDarkMode.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        Singleton.INSTANCE.setIsDarkModeChanged();
        if(isDarkModeEnabled) {
          isDarkModeEnabled = false;
          fabDarkMode.setBackgroundTintList(ColorStateList.valueOf(COLOR_GRAY_CCC));
          fabDarkMode.setImageResource(R.drawable.fab_dark_mode);
          AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
          recreate();
          displayToast("Dark Mode", false);
        } else {
          isDarkModeEnabled = true;
          fabDarkMode.setBackgroundTintList(ColorStateList.valueOf(COLOR_ACCENT));
          fabDarkMode.setImageResource(R.drawable.fab_dark_mode);
          AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
          recreate();
          displayToast("Dark Mode", true);
        }
      }
    });
    fabNewPublicCellAlert.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        if(newPublicCellAlert == 0) {
          locationFragment.getLastLocation(SettingsActivity.this, SettingsActivity.this,
            Feature.NEW_PUBLIC_CELL_ALERT);
        } else {
          newPublicCellAlert = 0;
          fabNewPublicCellAlert.setBackgroundTintList(ColorStateList.valueOf(COLOR_GRAY_CCC));
          fabNewPublicCellAlert.setImageResource(R.drawable.fab_new_public_cell_disabled);
          ParseUser.getCurrentUser().put("newPublicCellAlert", 0);
          ParseUser.getCurrentUser().saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
              if(e == null) {
                // Turn off location updates and enable the Location Update button
                // cbLocationUpdate.setEnabled(true);
                displayToast(getString(R.string.new_public_cell_alert), false);
                prefs.edit().putBoolean("newPublicCellAlertDisabledByApp", false).commit();
              } else {
                newPublicCellAlert = 1;
                fabNewPublicCellAlert.setBackgroundTintList(ColorStateList.valueOf(COLOR_ACCENT));
                fabNewPublicCellAlert.setImageResource(R.drawable.fab_new_public_cell_enabled);
              }
            }
          });
        }
      }
    });
    if(!metric.equals("kms")) {
      txtLblMiles.setBackgroundResource(R.drawable.bg_metric_selected);
      txtLblMiles.setTextColor(getResources().getColor(R.color.white));
      txtLblKilometers.setBackgroundResource(R.drawable.bg_metric_un_selected);
      txtLblKilometers.setTextColor(getResources().getColor(R.color.gray_333));
      txtLblPatrolRange.setText(R.string.miles_1_50);
    } else {
      txtLblMiles.setBackgroundResource(R.drawable.bg_metric_un_selected);
      txtLblMiles.setTextColor(getResources().getColor(R.color.gray_333));
      txtLblKilometers.setBackgroundResource(R.drawable.bg_metric_selected);
      txtLblKilometers.setTextColor(getResources().getColor(R.color.white));
      txtLblPatrolRange.setText(R.string.km_1_80);
    }
    txtLblMiles.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        metric = "miles";
        txtLblMiles.setBackgroundResource(R.drawable.bg_metric_selected);
        txtLblMiles.setTextColor(getResources().getColor(R.color.white));
        txtLblKilometers.setBackgroundResource(R.drawable.bg_metric_un_selected);
        txtLblKilometers.setTextColor(getResources().getColor(R.color.gray_333));
        if(patrolModeRadius <= 1) {
          txtPatrolRadius.setText(
            String.format("%.2f", (patrolModeRadius / 1.6f)) + " " + getString(R.string.mile));
        } else {
          txtPatrolRadius.setText(
            String.format("%.2f", (patrolModeRadius / 1.6f)) + " " + getString(R.string.miles));
        }
        txtLblPatrolRange.setText(R.string.miles_1_50);
      }
    });
    txtLblKilometers.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        metric = "kms";
        txtLblMiles.setBackgroundResource(R.drawable.bg_metric_un_selected);
        txtLblMiles.setTextColor(getResources().getColor(R.color.gray_333));
        txtLblKilometers.setBackgroundResource(R.drawable.bg_metric_selected);
        txtLblKilometers.setTextColor(getResources().getColor(R.color.white));
        if(patrolModeRadius <= 1) {
          txtPatrolRadius.setText(patrolModeRadius + " " + getString(R.string.km));
        } else {
          txtPatrolRadius.setText(patrolModeRadius + " " + getString(R.string.kms));
        }
        txtLblPatrolRange.setText(R.string.km_1_80);
      }
    });
    // Fixme:  remove fabUploadContacts button.
//    fabUploadContacts.setOnClickListener(new View.OnClickListener() {
//      @Override
//      public void onClick(View v) {
//        if(isUploadContactsEnabled) {
//          showDeleteUploadedContactAlertDialog();
//        } else {
//          Intent intentUploadContact =
//            new Intent(SettingsActivity.this, UploadContactsActivity.class);
//          startActivity(intentUploadContact);
//        }
//      }
//    });
    fabGPSAccurateTracking.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        if(isLocationAccuracyEnabled) {
          isLocationAccuracyEnabled = false;
          fabGPSAccurateTracking.setBackgroundTintList(ColorStateList.valueOf(COLOR_GRAY_CCC));
          fabGPSAccurateTracking.setImageResource(R.drawable.fab_gps_disabled);
          displayToast(getString(R.string.gps_accurate_tracking), false);
        } else {
          isLocationAccuracyEnabled = true;
          fabGPSAccurateTracking.setBackgroundTintList(ColorStateList.valueOf(COLOR_ACCENT));
          fabGPSAccurateTracking.setImageResource(R.drawable.fab_gps_enabled);
          displayToast(getString(R.string.gps_accurate_tracking), true);
        }
      }
    });
    final boolean isRideFeatureEnabled = false;
    final boolean isPatrolModeFeatureEnabled =
      getResources().getBoolean(R.bool.is_patrol_mode_feature_enabled);
    fabLocationUpdates.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        if(isLocationUpdateEnabled) {
          isLocationUpdateEnabled = false;
          fabLocationUpdates.setBackgroundTintList(ColorStateList.valueOf(COLOR_GRAY_CCC));
          fabLocationUpdates.setImageResource(R.drawable.fab_location_disabled);
          // Check if Patrol mode, new Public Cell alert or Ride request alert is ON,
          // then user cannot turn off location updates
          if((isPatrolModeFeatureEnabled && patrolMode == 1) && newPublicCellAlert == 1) {
            showPatrolModeAlertDialog(1, 1, false);
          } else if((isPatrolModeFeatureEnabled && patrolMode == 1)) {
            showPatrolModeAlertDialog(1, 0, false);
          } else if(newPublicCellAlert == 1) {
            showPatrolModeAlertDialog(0, 1, false);
          } else {
            // Stop the location service
            //Intent intentLocationService = new Intent(SettingsActivity.this, LocationJobService.class);
            //stopService(intentLocationService);
            GpsReceiver.cancelJob(SettingsActivity.this);
            displayToast(getString(R.string.location_updates), false);
          }
        } else {
          isLocationUpdateEnabled = true;
          fabLocationUpdates.setBackgroundTintList(ColorStateList.valueOf(COLOR_ACCENT));
          fabLocationUpdates.setImageResource(R.drawable.fab_location_enabled);
          //Intent intentLocationService = new Intent(SettingsActivity.this, LocationJobService.class);
          //startService(intentLocationService);
          GpsReceiver.dispatchJob(SettingsActivity.this);
          displayToast(getString(R.string.location_updates), true);
        }
      }
    });
    if(isDarkModeEnabled) {
      fabDarkMode.setBackgroundTintList(ColorStateList.valueOf(COLOR_ACCENT));
      fabDarkMode.setImageResource(R.drawable.fab_dark_mode);
    } else {
      fabDarkMode.setBackgroundTintList(ColorStateList.valueOf(COLOR_GRAY_CCC));
      fabDarkMode.setImageResource(R.drawable.fab_dark_mode);
    }
    if(newPublicCellAlert == 1) {
      fabNewPublicCellAlert.setBackgroundTintList(ColorStateList.valueOf(COLOR_ACCENT));
      fabNewPublicCellAlert.setImageResource(R.drawable.fab_new_public_cell_enabled);
    } else {
      fabNewPublicCellAlert.setBackgroundTintList(ColorStateList.valueOf(COLOR_GRAY_CCC));
      fabNewPublicCellAlert.setImageResource(R.drawable.fab_new_public_cell_disabled);
    }
    if(isLocationAccuracyEnabled) {
      fabGPSAccurateTracking.setBackgroundTintList(ColorStateList.valueOf(COLOR_ACCENT));
      fabGPSAccurateTracking.setImageResource(R.drawable.fab_gps_enabled);
    } else {
      fabGPSAccurateTracking.setBackgroundTintList(ColorStateList.valueOf(COLOR_GRAY_CCC));
      fabGPSAccurateTracking.setImageResource(R.drawable.fab_gps_disabled);
    }
    if(isLocationUpdateEnabled) {
      fabLocationUpdates.setBackgroundTintList(ColorStateList.valueOf(COLOR_ACCENT));
      fabLocationUpdates.setImageResource(R.drawable.fab_location_enabled);
    } else {
      fabLocationUpdates.setBackgroundTintList(ColorStateList.valueOf(COLOR_GRAY_CCC));
      fabLocationUpdates.setImageResource(R.drawable.fab_location_disabled);
    }
    TextView txtSpammedUsers = (TextView) findViewById(R.id.txt_btn_spammed_users);
    txtSpammedUsers.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        Intent intent = new Intent(SettingsActivity.this, SpammedUsersActivity.class);
        startActivity(intent);
      }
    });
    txtDownloadData = (TextView) findViewById(R.id.txt_btn_download_data);
    txtDownloadData.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        downloadUserData();
      }
    });
    if(System.currentTimeMillis() < prefs.getLong("DisableDownloadUntil", 0)) {
      // If current time is less than the time until which the button should be disabled,
      // then gray out the button and disabled it
      txtDownloadData.setBackgroundColor(Color.GRAY);
      //txtDownloadData.setEnabled(false);
    }
    TextView txtTestFirebase = findViewById(R.id.txt_btn_test_firebase);
    txtTestFirebase.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        final ProgressDialog dialog = new ProgressDialog(SettingsActivity.this);
        dialog.setMessage("Processing");
        dialog.setCancelable(false);
        dialog.show();
        HashMap<String, Object> params = new HashMap<>();
        ParseCloud.callFunctionInBackground("testFirebase", params, new FunctionCallback<String>() {
          @Override
          public void done(String response, ParseException e) {
            LogEvent.Log(TAG, "response: " + response);
            dialog.dismiss();
            if(e == null) {
              if(Modules.isWeakReferenceValid()) {
                Singleton.sendToast(response);
              }
            } else {
              if(Modules.isWeakReferenceValid()) {
                Singleton.sendToast(e);
              }
            }
          }
        });
      }
    });
    TextView txtDeleteAccount = (TextView) findViewById(R.id.txt_btn_delete_account);
    txtDeleteAccount.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        showConfirmDeletionAlertDialog();
      }
    });
    findViewById(R.id.rl_panic_button).setVisibility(View.GONE);
    findViewById(R.id.rl_ride_requests).setVisibility(View.GONE);
    applyPatrolModeSettingsIfEnabled();
    applyDispatchModeSettingsIfEnabled();
    applyFacebookSettingsIfEnabled();
    applyLiveStreamingSettingsIfEnabled();
  }

  private void downloadUserData() {
    txtDownloadData.setBackgroundColor(Color.GRAY);
    //txtDownloadData.setEnabled(false);
    Date dateBefore7Days = new Date();
    //dateBefore7Days.setTime(System.currentTimeMillis() - (1000 * 60 * 60 * 24 * 7));
    dateBefore7Days.setTime(System.currentTimeMillis() - (1000 * 60 * 60 * 24));
    ParseQuery<ParseObject> queryAppUserLog = ParseQuery.getQuery("AppUserLog");
    queryAppUserLog.whereEqualTo("user", ParseUser.getCurrentUser());
    queryAppUserLog.whereEqualTo("action", 1);
    queryAppUserLog.whereGreaterThanOrEqualTo("createdAt", dateBefore7Days); // 7 days
    queryAppUserLog.orderByDescending("createdAt");
    queryAppUserLog.getFirstInBackground(new GetCallback<ParseObject>() {
      @Override
      public void done(ParseObject object, ParseException e) {
        if(e == null) {
          LogEvent.Log(TAG, "success");
          Date downloadDate = object.getCreatedAt();
          int difference = new Date().compareTo(downloadDate);
          LogEvent.Log(TAG, "difference: " + difference);
          if(Modules.isWeakReferenceValid()) {
            String msg = getString(R.string.msg_download_data_error, (7 - difference));
            showDownloadDataSuccessAlertDialog(msg);
          }
          txtDownloadData.setBackgroundColor(Color.GRAY);
          //txtDownloadData.setEnabled(true);
        } else {
          LogEvent.Log(TAG, "error: " + e.getCode() + " - " + e.getLocalizedMessage());
          if(e.getCode() == 101) { // Response success but query found no results to return
            if(Modules.isWeakReferenceValid()) {
              showDownloadDataSuccessAlertDialog(getString(R.string.msg_download_data_success));
            }
            txtDownloadData.setText(R.string.btn_download_data_processing);
            HashMap<String, Object> params = new HashMap<>();
            params.put("clientFirmId", 1);
            params.put("isLive", getResources().getInteger(R.integer.is_live));
            params.put("languageCode", getString(R.string.language_code));
            params.put("platform", "android");
            ParseCloud.callFunctionInBackground("downloadUserData", params,
              new FunctionCallback<String>() {
                @Override
                public void done(String response, ParseException e) {
                  LogEvent.Log(TAG, "response: " + response);
                  txtDownloadData.setText(R.string.btn_download_data);
                  if(e == null) {
                    long disableForMillis = 86400000; // 1000 * 60 * 60 * 24 (1 day)
                    //long disableForMillis = 604800000; // 1000 * 60 * 60 * 24 * 7 (7 days)
                    long disableTime = System.currentTimeMillis();
                    long disableUntil = disableTime + disableForMillis;
                    Singleton.INSTANCE.getAppPrefs().edit().putLong("DisableDownloadUntil",
                      disableUntil).apply();
                  } else {
                    txtDownloadData.setBackgroundResource(R.drawable.ripple_btn_primary);
                    //txtDownloadData.setEnabled(true);
                    if(Modules.isWeakReferenceValid()) {
                      Singleton.sendToast(e);
                    }
                  }
                }
              });
          }
        }
      }
    });
        /*String url = "https://cell-411-9a02f.firebaseapp.com";
        CustomTabsIntent.Builder builder = new CustomTabsIntent.Builder();
        CustomTabsIntent customTabsIntent = builder.build();
        customTabsIntent.launchUrl(this, Uri.parse(url));*/
        /*final ProgressDialog dialog = new ProgressDialog(SettingsActivity.this);
        dialog.setMessage(getString(R.string.dialog_msg_download_data_processing));
        dialog.setCancelable(false);
        dialog.show();*/
        /*HashMap<String, Object> params = new HashMap<>();
        ParseCloud.callFunctionInBackground("downloadUserData", params, new FunctionCallback<String>() {
            @Override
            public void done(String response, ParseException e) {
                LogEvent.Log(TAG, "response: " + response);
                // dialog.dismiss();
                if (e == null) {
                    if(Modules.isWeakReferenceValid()) {
                        showDownloadDataSuccessAlertDialog();
                    }
                } else {
                    if(Modules.isWeakReferenceValid()) {
                        UtilityMethods.displayToast(Singleton.INSTANCE, e);
                    }
                }
            }
        });*/
  }

  private void showDownloadDataSuccessAlertDialog(String msg) {
    AlertDialog.Builder alert = new AlertDialog.Builder(this);
    alert.setMessage(msg);
    alert.setPositiveButton(R.string.dialog_btn_ok, new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialogInterface, int i) {
      }
    });
    AlertDialog dialog = alert.create();
    dialog.show();
  }

  private void showConfirmDeletionAlertDialog() {
    AlertDialog.Builder alert = new AlertDialog.Builder(this);
    alert.setMessage(getString(R.string.dialog_msg_delete_my_account));
    alert.setNegativeButton(R.string.dialog_btn_no, new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialog, int which) {
      }
    });
    alert.setPositiveButton(R.string.dialog_btn_yes, new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialogInterface, int i) {
        final ProgressDialog dialog = new ProgressDialog(SettingsActivity.this);
        dialog.setMessage(getString(R.string.dialog_msg_deleting_account));
        dialog.setCancelable(false);
        dialog.show();
        HashMap<String, Object> params = new HashMap<>();
        params.put("clientFirmId", 1);
        params.put("isLive", getResources().getInteger(R.integer.is_live));
        params.put("languageCode", getString(R.string.language_code));
        params.put("platform", "android");
        ParseCloud.callFunctionInBackground("deleteUser", params, new FunctionCallback<String>() {
          @Override
          public void done(String response, ParseException e) {
            LogEvent.Log(TAG, "response: " + response);
            dialog.dismiss();
            if(e == null) {
              final ParseInstallation installation = ParseInstallation.getCurrentInstallation();
              installation.remove(ParseKeys.USER);
              installation.saveInBackground(new SaveCallback() {
                @Override
                public void done(ParseException e) {
                  if(e != null) {
                    installation.saveEventually();
                  }
                }
              });
              ParseUser.logOutInBackground();
              Singleton.INSTANCE.clearData();
              Singleton.INSTANCE.setLoggedIn(false);
              final FirebaseAuth mAuth = FirebaseAuth.getInstance();
              if(mAuth != null && mAuth.getCurrentUser() != null) { // logout firebase user
                mAuth.signOut();
              }
              Intent intent =
                new Intent(Singleton.INSTANCE.getCurrentActivity(), GalleryActivity.class);
              intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
              startActivity(intent);
              if(Modules.isWeakReferenceValid()) {
                Singleton.sendToast(R.string.toast_msg_account_deleted);
              }
            } else {
              if(Modules.isWeakReferenceValid()) {
                Singleton.sendToast(e);
              }
            }
          }
        });
      }
    });
    AlertDialog dialog = alert.create();
    dialog.show();
  }

  private void displayToast(String feature, boolean isEnabled) {
    String featureStatus;
    if(isEnabled) {
      featureStatus = getString(R.string.enabled);
    } else {
      featureStatus = getString(R.string.disabled);
    }
    String message = feature + " " + featureStatus;
    if(Modules.isWeakReferenceValid()) {
      getApplicationContext();
      Singleton.sendToast(message);
    }
  }

  private void applyPatrolModeSettingsIfEnabled() {
    boolean isPatrolModeFeatureEnabled =
      getResources().getBoolean(R.bool.is_patrol_mode_feature_enabled);
    if(isPatrolModeFeatureEnabled) {
      fabPatrolMode = (FloatingActionButton) findViewById(R.id.fab_patrol_mode);
      if(ParseUser.getCurrentUser().get("PatrolMode") != null) {
        patrolMode = (int) ParseUser.getCurrentUser().get("PatrolMode");
      }
      final SeekBar seekBarPatrolModeRadius = (SeekBar) findViewById(R.id.sb_patrol_radius);
      final TextView txtPatrolRadius = (TextView) findViewById(R.id.txt_patrol_radius);
      patrolModeRadius = prefs.getInt("PatrolModeRadius", 50);
      fabPatrolMode.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
          if(patrolMode == 0) {
            locationFragment.getLastLocation(SettingsActivity.this, SettingsActivity.this,
              Feature.PATROL_MODE);
          } else {
            patrolMode = 0;
            fabPatrolMode.setBackgroundTintList(ColorStateList.valueOf(COLOR_GRAY_CCC));
            fabPatrolMode.setImageResource(R.drawable.fab_patrol_mode_disabled);
            ParseUser.getCurrentUser().put("PatrolMode", 0);
            ParseUser.getCurrentUser().saveInBackground(new SaveCallback() {
              @Override
              public void done(ParseException e) {
                if(e == null) {
                  // Turn off location updates and enable the Location Update button
                  // cbLocationUpdate.setEnabled(true);
                  displayToast(getString(R.string.patrol_mode), false);
                  prefs.edit().putBoolean("patrolModeDisabledByApp", false).commit();
                } else {
                  patrolMode = 1;
                  fabPatrolMode.setBackgroundTintList(ColorStateList.valueOf(COLOR_ACCENT));
                  fabPatrolMode.setImageResource(R.drawable.fab_patrol_mode_enabled);
                }
              }
            });
          }
        }
      });
      seekBarPatrolModeRadius.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {
        }

        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
          if(metric.equals("kms")) {
            if(progress <= 1) {
              txtPatrolRadius.setText(progress + " " + getString(R.string.km));
            } else {
              txtPatrolRadius.setText(progress + " " + getString(R.string.kms));
            }
          } else {
            if(progress <= 1) {
              txtPatrolRadius.setText(
                String.format("%.2f", (progress / 1.6f)) + " " + getString(R.string.mile));
            } else {
              txtPatrolRadius.setText(
                String.format("%.2f", (progress / 1.6f)) + " " + getString(R.string.miles));
            }
          }
          patrolModeRadius = progress;
        }
      });
      seekBarPatrolModeRadius.setProgress(patrolModeRadius);
      if(patrolMode == 1) {
        fabPatrolMode.setBackgroundTintList(ColorStateList.valueOf(COLOR_ACCENT));
        fabPatrolMode.setImageResource(R.drawable.fab_patrol_mode_enabled);
      } else {
        fabPatrolMode.setBackgroundTintList(ColorStateList.valueOf(COLOR_GRAY_CCC));
        fabPatrolMode.setImageResource(R.drawable.fab_patrol_mode_disabled);
      }
    } else {
      findViewById(R.id.rl_patrol_mode).setVisibility(View.GONE);
    }
  }

  private void applyDispatchModeSettingsIfEnabled() {
    boolean isDispatchModeFeatureEnabled =
      getResources().getBoolean(R.bool.is_dispatch_mode_feature_enabled);
    if(isDispatchModeFeatureEnabled) {
      final FloatingActionButton fabDispatchMode =
        (FloatingActionButton) findViewById(R.id.fab_dispatch_mode);
      isDispatchModeEnabled = prefs.getBoolean("DispatchMode", false);
      fabDispatchMode.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
          if(isDispatchModeEnabled) {
            isDispatchModeEnabled = false;
            fabDispatchMode.setBackgroundTintList(ColorStateList.valueOf(COLOR_GRAY_CCC));
            fabDispatchMode.setImageResource(R.drawable.fab_dispatch_disabled);
            displayToast(getString(R.string.dispatch_mode), false);
          } else {
            isDispatchModeEnabled = true;
            fabDispatchMode.setBackgroundTintList(ColorStateList.valueOf(COLOR_ACCENT));
            fabDispatchMode.setImageResource(R.drawable.fab_dispatch_enabled);
            displayToast(getString(R.string.dispatch_mode), true);
          }
        }
      });
      if(isDispatchModeEnabled) {
        fabDispatchMode.setBackgroundTintList(ColorStateList.valueOf(COLOR_ACCENT));
        fabDispatchMode.setImageResource(R.drawable.fab_dispatch_enabled);
      } else {
        fabDispatchMode.setBackgroundTintList(ColorStateList.valueOf(COLOR_GRAY_CCC));
        fabDispatchMode.setImageResource(R.drawable.fab_dispatch_disabled);
      }
    } else {
      findViewById(R.id.rl_dispatch_mode).setVisibility(View.GONE);
    }
  }

  private void applyLiveStreamingSettingsIfEnabled() {
    boolean isLiveStreamingEnabled = getResources().getBoolean(R.bool.is_live_streaming_enabled);
    if(isLiveStreamingEnabled) {
      isDeleteVideoEnabled = prefs.getBoolean("DeleteVideo", false);
      fabDeleteVideoOption = (FloatingActionButton) findViewById(R.id.fab_delete_video_option);
      fabDeleteVideoOption.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
          if(isDeleteVideoEnabled) {
            isDeleteVideoEnabled = false;
            fabDeleteVideoOption.setBackgroundTintList(ColorStateList.valueOf(COLOR_GRAY_CCC));
            fabDeleteVideoOption.setImageResource(R.drawable.fab_delete_video_disabled);
            displayToast(getString(R.string.delete_video), false);
          } else {
            if(ContextCompat.checkSelfPermission(SettingsActivity.this,
              Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
              requestCameraPermission();
            } else {
              isDeleteVideoEnabled = true;
              fabDeleteVideoOption.setBackgroundTintList(ColorStateList.valueOf(COLOR_ACCENT));
              fabDeleteVideoOption.setImageResource(R.drawable.fab_delete_video_enabled);
              showDeleteVideoAlertDialog();
            }
          }
        }
      });
      if(isDeleteVideoEnabled &&
           ContextCompat.checkSelfPermission(SettingsActivity.this, Manifest.permission.CAMERA) ==
             PackageManager.PERMISSION_GRANTED) {
        fabDeleteVideoOption.setBackgroundTintList(ColorStateList.valueOf(COLOR_ACCENT));
        fabDeleteVideoOption.setImageResource(R.drawable.fab_delete_video_enabled);
      } else {
        isDeleteVideoEnabled = false;
        fabDeleteVideoOption.setBackgroundTintList(ColorStateList.valueOf(COLOR_GRAY_CCC));
        fabDeleteVideoOption.setImageResource(R.drawable.fab_delete_video_disabled);
      }
      TextView txtVideoSettings = (TextView) findViewById(R.id.txt_btn_video_settings);
      txtVideoSettings.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
          Intent intent = new Intent(SettingsActivity.this, VideoSettingsActivity.class);
          startActivity(intent);
        }
      });
    } else {
      findViewById(R.id.rl_delete_video_option).setVisibility(View.GONE);
      findViewById(R.id.txt_btn_video_settings).setVisibility(View.GONE);
    }
  }

  private void requestCameraPermission() {
    if(ActivityCompat.shouldShowRequestPermissionRationale(this,
      android.Manifest.permission.CAMERA)) {
      ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.CAMERA},
        PERMISSION_CAMERA);
    } else {
      ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA},
        PERMISSION_CAMERA);
    }
  }

  @Override
  public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                         @NonNull int[] grantResults) {
    if(requestCode == PERMISSION_CAMERA) {
      if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
        isDeleteVideoEnabled = true;
        fabDeleteVideoOption.setBackgroundTintList(ColorStateList.valueOf(COLOR_ACCENT));
        fabDeleteVideoOption.setImageResource(R.drawable.fab_delete_video_enabled);
        showDeleteVideoAlertDialog();
      }
    } else {
      super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }
  }

  private void applyFacebookSettingsIfEnabled() {
    rlConnectToFacebook = (RelativeLayout) findViewById(R.id.rl_connect_to_facebook);
    llConnectToFacebook = (LinearLayout) findViewById(R.id.ll_btn_connect_to_fb);
    fabPublishAlertToFacebook =
      (FloatingActionButton) findViewById(R.id.fab_publish_alerts_to_facebook);
    llDisconnectFromFacebook = (LinearLayout) findViewById(R.id.ll_btn_disconnect_from_fb);
    rlConnectToFacebook.setVisibility(View.GONE);
    findViewById(R.id.rl_publish_alert_to_facebook).setVisibility(View.GONE);
  }

  @Override
  protected void onResume() {
    super.onResume();
    //isUploadContactsEnabled = prefs.getBoolean("UploadContacts", false);
    int syncContacts = ParseUser.getCurrentUser().getInt("syncContacts");
    isUploadContactsEnabled = false;
    fabUploadContacts.setBackgroundTintList(ColorStateList.valueOf(COLOR_GRAY_CCC));
    fabUploadContacts.setImageResource(R.drawable.fab_upload_contact_disabled);
  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);
  }

  @Override
  public void onPause() {
    super.onPause();
    SharedPreferences.Editor editor = prefs.edit();
    //editor.putBoolean("UploadContacts", isUploadContactsEnabled);
    editor.putBoolean("LocationAccuracy", isLocationAccuracyEnabled);
    editor.putString("metric", metric);
    editor.putBoolean("LocationUpdate", isLocationUpdateEnabled);
    editor.putBoolean("DarkMode", isDarkModeEnabled);
    boolean isFacebookIntegrationEnabled = false;
    //getResources().getBoolean(R.bool.is_facebook_integration_enabled);
    boolean isLiveStreamingEnabled = getResources().getBoolean(R.bool.is_live_streaming_enabled);
    if(isLiveStreamingEnabled) {
      editor.putBoolean("DeleteVideo", isDeleteVideoEnabled);
    }
    boolean isDispatchModeFeatureEnabled =
      getResources().getBoolean(R.bool.is_dispatch_mode_feature_enabled);
    if(isDispatchModeFeatureEnabled) {
      editor.putBoolean("DispatchMode", isDispatchModeEnabled);
    }
    boolean isPatrolModeFeatureEnabled =
      getResources().getBoolean(R.bool.is_patrol_mode_feature_enabled);
    if(isPatrolModeFeatureEnabled) {
      editor.putInt("PatrolModeRadius", patrolModeRadius);
    }
    editor.apply();
  }

  private void showPatrolModeAlertDialog(final int patrolMode, final int newPublicCell,
                                         final boolean rideRequestAlert) {
    AlertDialog.Builder alert = new AlertDialog.Builder(this);
    String feature;
    String message;
    if(patrolMode == 1 && newPublicCell == 1 && rideRequestAlert) {
      // Not translated
      message =
        getString(R.string.dialog_message_disable_three_features, getString(R.string.patrol_mode),
          getString(R.string.new_public_cell_alert),
          getString(R.string.feature_ride_request_alert));
      feature = getString(R.string.three_features_disabled, getString(R.string.patrol_mode),
        getString(R.string.new_public_cell_alert),
        getString(R.string.feature_ride_request_alert));
    } else if(patrolMode == 1 && newPublicCell == 1) {
      message =
        getString(R.string.dialog_message_disable_two_features, getString(R.string.patrol_mode),
          getString(R.string.new_public_cell_alert));
      feature = getString(R.string.two_features_disabled, getString(R.string.patrol_mode),
        getString(R.string.new_public_cell_alert));
    } else if(newPublicCell == 1 && rideRequestAlert) {
      // Not translated
      message = getString(R.string.dialog_message_disable_two_features,
        getString(R.string.new_public_cell_alert),
        getString(R.string.feature_ride_request_alert));
      feature = getString(R.string.two_features_disabled, getString(R.string.new_public_cell_alert),
        getString(R.string.feature_ride_request_alert));
    } else if(patrolMode == 1 && rideRequestAlert) {
      // Not translated
      message =
        getString(R.string.dialog_message_disable_two_features, getString(R.string.patrol_mode),
          getString(R.string.feature_ride_request_alert));
      feature = getString(R.string.two_features_disabled, getString(R.string.patrol_mode),
        getString(R.string.feature_ride_request_alert));
    } else if(patrolMode == 1) {
      message =
        getString(R.string.dialog_message_disable_one_feature, getString(R.string.patrol_mode));
      feature = getString(R.string.patrol_mode);
    } else if(newPublicCell == 1) {
      message = getString(R.string.dialog_message_disable_one_feature,
        getString(R.string.new_public_cell_alert));
      feature = getString(R.string.new_public_cell_alert);
    } else {
      // Not translated
      message = getString(R.string.dialog_message_disable_one_feature,
        getString(R.string.feature_ride_request_alert));
      feature = getString(R.string.feature_ride_request_alert);
    }
    alert.setMessage(message);
    final String featureFinal = feature;
    alert.setNegativeButton(R.string.dialog_btn_cancel, new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int arg1) {
        isLocationUpdateEnabled = true;
        fabLocationUpdates.setBackgroundTintList(ColorStateList.valueOf(COLOR_ACCENT));
        fabLocationUpdates.setImageResource(R.drawable.fab_location_enabled);
      }
    });
    alert.setPositiveButton(R.string.dialog_btn_yes, new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int which) {
        if(fabPatrolMode != null) {
          SettingsActivity.this.patrolMode = 0;
          fabPatrolMode.setBackgroundTintList(ColorStateList.valueOf(COLOR_GRAY_CCC));
          fabPatrolMode.setImageResource(R.drawable.fab_patrol_mode_disabled);
        }
        SettingsActivity.this.newPublicCellAlert = 0;
        fabNewPublicCellAlert.setBackgroundTintList(ColorStateList.valueOf(COLOR_GRAY_CCC));
        fabNewPublicCellAlert.setImageResource(R.drawable.fab_new_public_cell_disabled);
        if(fabRideRequests != null) {
          SettingsActivity.this.rideRequests = false;
          fabRideRequests.setBackgroundTintList(ColorStateList.valueOf(COLOR_GRAY_CCC));
          fabRideRequests.setImageResource(R.drawable.fab_ride_disabled);
        }
        if(patrolMode == 1) {
          ParseUser.getCurrentUser().put("PatrolMode", 0);
        }
        if(newPublicCell == 1) {
          ParseUser.getCurrentUser().put("newPublicCellAlert", 0);
        }
        if(rideRequestAlert) {
          ParseUser.getCurrentUser().put(ParseKeys.RIDE_REQUEST_ALERT, false);
        }
        ParseUser.getCurrentUser().saveInBackground();
        // Stop the location service
        //Intent intentLocationService = new Intent(SettingsActivity.this, LocationJobService.class);
        //stopService(intentLocationService);
        GpsReceiver.cancelJob(SettingsActivity.this);
        displayToast(getString(R.string.location_updates), false);
        displayToast(featureFinal, false);
      }
    });
    AlertDialog dialog = alert.create();
    dialog.show();
  }

  private void showDeleteVideoAlertDialog() {
    AlertDialog.Builder alert = new AlertDialog.Builder(this);
    alert.setMessage(R.string.dialog_message_video_delete_alert);
    alert.setOnCancelListener(new DialogInterface.OnCancelListener() {
      @Override
      public void onCancel(DialogInterface dialogInterface) {
        displayToast(getString(R.string.delete_video), true);
      }
    });
    alert.setPositiveButton(R.string.dialog_btn_ok, new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int which) {
        displayToast(getString(R.string.delete_video), true);
      }
    });
    AlertDialog dialog = alert.create();
    dialog.show();
  }

  private void showDeleteUploadedContactAlertDialog() {
    AlertDialog.Builder alert = new AlertDialog.Builder(this);
    alert.setMessage(R.string.dialog_message_delete_uploaded_contact);
    alert.setNegativeButton(R.string.dialog_btn_cancel, new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int which) {
      }
    });
    alert.setPositiveButton(R.string.dialog_btn_disable, new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int which) {
        isUploadContactsEnabled = false;
        fabUploadContacts.setBackgroundTintList(ColorStateList.valueOf(COLOR_GRAY_CCC));
        fabUploadContacts.setImageResource(R.drawable.fab_upload_contact_disabled);
        ParseUser.getCurrentUser().put("syncContacts", 0);
        ParseUser.getCurrentUser().saveInBackground(new SaveCallback() {
          @Override
          public void done(ParseException e) {
            if(e == null) {
              HashMap<String, Object> params = new HashMap<>();
              params.put("clientFirmId", 1);
              params.put("isLive", getResources().getBoolean(R.bool.enable_publish));
              params.put("languageCode", getString(R.string.language_code));
              params.put("platform", "android");
              ParseCloud.callFunctionInBackground("deleteContacts", params,
                new FunctionCallback<String>() {
                  public void done(String result, ParseException e) {
                    // spinner.setVisibility(View.GONE);
                    if(e == null) {
                      // Contact sync successfully
                      if(Modules.isWeakReferenceValid()) {
                        getApplicationContext();
                        Singleton.sendToast(R.string.toast_msg_contacts_deleted_successfully);
                      }
                    } else {
                      if(Modules.isWeakReferenceValid()) {
                        Singleton.sendToast(e);
                      }
                    }
                  }
                });
              // Turn on location updates and disable the Location Update slider button
              displayToast(getString(R.string.upload_contacts), false);
            } else {
              isUploadContactsEnabled = true;
              fabUploadContacts.setBackgroundTintList(ColorStateList.valueOf(COLOR_ACCENT));
              fabUploadContacts.setImageResource(R.drawable.fab_upload_contact_enabled);
            }
          }
        });
      }
    });
    AlertDialog dialog = alert.create();
    dialog.show();
  }

  @Override
  public void onLastLocationRetrieved(Location mLastLocation, Feature feature) {
    enableSetting(feature);
  }

  @Override
  public void onLocationFailToRetrieve(FailureReason failureReason, Feature feature) {
    LogEvent.Log(TAG, "onLocationFailToRetrieve() invoked: " + failureReason.toString());
    switch(failureReason) {
      case PERMISSION_DENIED:
      case SETTINGS_REQUEST_DENIED:
        break;
      default:
        locationFragment.getCurrentLocationFromLocationUpdates(this, feature);
    }
  }

  @Override
  public void onCurrentLocationRetrievedFromLocationUpdates(Location location, Feature feature) {
    enableSetting(feature);
  }

  private void enableSetting(Feature feature) {
    switch(feature) {
      case PATROL_MODE:
        patrolMode = 1;
        fabPatrolMode.setBackgroundTintList(ColorStateList.valueOf(COLOR_ACCENT));
        fabPatrolMode.setImageResource(R.drawable.fab_patrol_mode_enabled);
        ParseUser.getCurrentUser().put("PatrolMode", 1);
        ParseUser.getCurrentUser().saveInBackground(new SaveCallback() {
          @Override
          public void done(ParseException e) {
            if(e == null) {
              // Turn on location updates and disable the Location Update slider button
              // cbLocationUpdate.setEnabled(false);
              displayToast(getString(R.string.patrol_mode), true);
            } else {
              patrolMode = 0;
              fabPatrolMode.setBackgroundTintList(ColorStateList.valueOf(COLOR_GRAY_CCC));
              fabPatrolMode.setImageResource(R.drawable.fab_patrol_mode_disabled);
            }
          }
        });
        break;
      case NEW_PUBLIC_CELL_ALERT:
        newPublicCellAlert = 1;
        fabNewPublicCellAlert.setBackgroundTintList(ColorStateList.valueOf(COLOR_ACCENT));
        fabNewPublicCellAlert.setImageResource(R.drawable.fab_new_public_cell_enabled);
        ParseUser.getCurrentUser().put("newPublicCellAlert", 1);
        ParseUser.getCurrentUser().saveInBackground(new SaveCallback() {
          @Override
          public void done(ParseException e) {
            if(e == null) {
              // Turn on location updates and disable the Location Update slider button
              displayToast(getString(R.string.new_public_cell_alert), true);
            } else {
              newPublicCellAlert = 0;
              fabNewPublicCellAlert.setBackgroundTintList(ColorStateList.valueOf(COLOR_GRAY_CCC));
              fabNewPublicCellAlert.setImageResource(R.drawable.fab_new_public_cell_disabled);
            }
          }
        });
        break;
      case RIDE_ALERT:
        rideRequests = true;
        fabRideRequests.setBackgroundTintList(ColorStateList.valueOf(COLOR_ACCENT));
        fabRideRequests.setImageResource(R.drawable.fab_ride_enabled);
        ParseUser.getCurrentUser().put(ParseKeys.RIDE_REQUEST_ALERT, true);
        ParseUser.getCurrentUser().saveInBackground(new SaveCallback() {
          @Override
          public void done(ParseException e) {
            if(e == null) {
              // Turn on location updates and disable the Location Update slider button
              // cbLocationUpdate.setEnabled(false);
              displayToast(getString(R.string.ride_sharing), true);
            } else {
              rideRequests = false;
              fabRideRequests.setBackgroundTintList(ColorStateList.valueOf(COLOR_GRAY_CCC));
              fabRideRequests.setImageResource(R.drawable.fab_ride_disabled);
            }
          }
        });
        break;
    }
    if(!isLocationUpdateEnabled) { // If location update is not enabled then enable it now
      isLocationUpdateEnabled = true;
      fabLocationUpdates.setBackgroundTintList(ColorStateList.valueOf(COLOR_ACCENT));
      fabLocationUpdates.setImageResource(R.drawable.fab_location_enabled);
      //Intent intentLocationService = new Intent(SettingsActivity.this, LocationJobService.class);
      //startService(intentLocationService);
      GpsReceiver.dispatchJob(SettingsActivity.this);
      displayToast(getString(R.string.location_updates), true);
    }
  }
}
