package cell411.activity;

import android.content.Context;

import androidx.appcompat.app.AppCompatActivity;

import cell411.Singleton;

public class BaseBaseActivity extends AppCompatActivity {
  Singleton singleton() {
    return Singleton.INSTANCE;
  }
}
