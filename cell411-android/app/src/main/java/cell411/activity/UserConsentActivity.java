package cell411.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.URLSpan;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseUser;
import com.parse.callback.SaveCallback;
import com.safearx.cell411.R;

import java.lang.ref.WeakReference;

import cell411.Singleton;

public class UserConsentActivity extends BaseActivity {
  public static WeakReference<UserConsentActivity> weakRef;
  private String ppUrl;
  private String tosUrl;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_user_consent);
    Singleton.INSTANCE.setCurrentActivity(this);
    weakRef = new WeakReference<>(this);
    final String privacyPolicyId = getIntent().getStringExtra("privacyPolicyId");
    if(privacyPolicyId == null) {
      finish();
    }
    ppUrl = getIntent().getStringExtra("ppUrl");
    tosUrl = getIntent().getStringExtra("tosUrl");
    TextView txtTitle = findViewById(R.id.title);
    String textTitle = getString(R.string.title_privacy_policy);
    setTextViewHTML(txtTitle, textTitle);
    TextView txtDescription = findViewById(R.id.description);
    String textDescription = getString(R.string.description_privacy_policy);
    setTextViewHTML(txtDescription, textDescription);
    final CheckBox cbWeDontSellOrGiveData = findViewById(R.id.cb_we_dont_sell_or_give_data);
    final CheckBox cbCanDeleteAccount = findViewById(R.id.cb_can_delete_account);
    final CheckBox cbYouAgreeToProcessData = findViewById(R.id.cb_you_agree_to_process_data);
    final CheckBox cbReadPrivacyPolicy = findViewById(R.id.cb_read_privacy_policy);
    final TextView txtBtnOk = findViewById(R.id.txt_btn_ok);
    cbWeDontSellOrGiveData.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
      @Override
      public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
        if(isChecked && cbCanDeleteAccount.isChecked() && cbYouAgreeToProcessData.isChecked() &&
             cbReadPrivacyPolicy.isChecked()) {
          txtBtnOk.setBackgroundResource(R.drawable.ripple_btn);
        } else {
          txtBtnOk.setBackgroundColor(getResources().getColor(R.color.gray_999));
        }
      }
    });
    cbCanDeleteAccount.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
      @Override
      public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
        if(isChecked && cbWeDontSellOrGiveData.isChecked() &&
             cbYouAgreeToProcessData.isChecked() && cbReadPrivacyPolicy.isChecked()) {
          txtBtnOk.setBackgroundResource(R.drawable.ripple_btn);
        } else {
          txtBtnOk.setBackgroundColor(getResources().getColor(R.color.gray_999));
        }
      }
    });
    cbYouAgreeToProcessData.setOnCheckedChangeListener(
      new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
          if(isChecked && cbWeDontSellOrGiveData.isChecked() && cbCanDeleteAccount.isChecked() &&
               cbReadPrivacyPolicy.isChecked()) {
            txtBtnOk.setBackgroundResource(R.drawable.ripple_btn);
          } else {
            txtBtnOk.setBackgroundColor(getResources().getColor(R.color.gray_999));
          }
        }
      });
    cbReadPrivacyPolicy.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
      @Override
      public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
        if(isChecked && cbWeDontSellOrGiveData.isChecked() && cbCanDeleteAccount.isChecked() &&
             cbYouAgreeToProcessData.isChecked()) {
          txtBtnOk.setBackgroundResource(R.drawable.ripple_btn);
        } else {
          txtBtnOk.setBackgroundColor(getResources().getColor(R.color.gray_999));
        }
      }
    });
    txtBtnOk.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        if(cbWeDontSellOrGiveData.isChecked() && cbYouAgreeToProcessData.isChecked() &&
             cbReadPrivacyPolicy.isChecked() && cbCanDeleteAccount.isChecked()) {
          final ParseObject objUserConsent = new ParseObject("UserConsent");
          objUserConsent.put("privacyPolicyId", privacyPolicyId);
          objUserConsent.put("userId", ParseUser.getCurrentUser().getObjectId());
          objUserConsent.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
              if(e != null) {
                objUserConsent.saveEventually();
              }
            }
          });
          finish();
        } else {
          getApplicationContext();
          Singleton.sendToast(R.string.consent_instruction);
        }
      }
    });
  }

  protected void setTextViewHTML(TextView text, String html) {
    CharSequence sequence = Html.fromHtml(html);
    SpannableStringBuilder strBuilder = new SpannableStringBuilder(sequence);
    URLSpan[] urls = strBuilder.getSpans(0, sequence.length(), URLSpan.class);
    for(URLSpan span : urls) {
      makeLinkClickable(strBuilder, span);
    }
    text.setText(strBuilder);
    text.setMovementMethod(LinkMovementMethod.getInstance());
  }

  protected void makeLinkClickable(SpannableStringBuilder strBuilder, final URLSpan span) {
    int start = strBuilder.getSpanStart(span);
    int end = strBuilder.getSpanEnd(span);
    int flags = strBuilder.getSpanFlags(span);
    ClickableSpan clickable = new ClickableSpan() {
      public void onClick(View view) {
        if(span.getURL().equals("terms")) {
          Intent intentWeb = new Intent(Intent.ACTION_VIEW);
          if(tosUrl != null && !tosUrl.isEmpty()) {
            intentWeb.setData(Uri.parse(tosUrl));
          } else {
            intentWeb.setData(Uri.parse(getString(R.string.terms_and_conditions_url)));
          }
          startActivity(intentWeb);
        } else if(span.getURL().equals("privacy_policy")) {
          Intent intentWeb = new Intent(Intent.ACTION_VIEW);
          if(ppUrl != null && !ppUrl.isEmpty()) {
            intentWeb.setData(Uri.parse(ppUrl));
          } else {
            intentWeb.setData(Uri.parse(getString(R.string.privacy_policy_url)));
          }
          startActivity(intentWeb);
        }
      }
    };
    strBuilder.setSpan(clickable, start, end, flags);
    strBuilder.removeSpan(span);
  }

  @Override
  public void onBackPressed() {
    //super.onBackPressed();
  }
}