package cell411.activity;

import android.app.NotificationManager;
import android.content.Context;
import android.content.IntentFilter;
import android.location.LocationManager;
import android.os.Bundle;

import com.parse.ParseUser;

import cell411.Singleton;
import cell411.constants.Prefs;
import cell411.interfaces.LocationStateChangeCallBack;
import cell411.methods.Dialogs;
import cell411.methods.Modules;
import cell411.receivers.GpsReceiver;
import cell411.utils.LogEvent;

/**
 * Created by Sachin on 01-06-2016.
 */
public class BaseActivity extends BaseBaseActivity {
  private static final String TAG = "BaseActivity";
  private NotificationManager notificationManager;
  private GpsReceiver gpsReceiver;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
    // Register the receiver for listening to the location state changes
    gpsReceiver = new GpsReceiver(new LocationStateChangeCallBack() {
      @Override
      public void onLocationStateChanged(boolean isEnabled) {
        //Location state changed
        String message;
        if(isEnabled) {
          message = "Location enabled";
        } else {
          message = "Location disabled";
        }
        Singleton.sendToast(message);
      }
    });
    registerReceiver(gpsReceiver, new IntentFilter(LocationManager.PROVIDERS_CHANGED_ACTION));
  }

  @Override
  protected void onResume() {
    super.onResume();
    LogEvent.Log(TAG, "onResume() invoked");
    notificationManager.cancelAll();
    Singleton.INSTANCE.setCurrentActivity(this);
    LogEvent.Log(TAG, Singleton.INSTANCE.getCurrentActivity().getClass().getName());
    if(Singleton.isWasInBackground()) {
      Singleton.setWasInBackground(false);
      if(ParseUser.getCurrentUser() != null && Singleton.isLoggedIn()) {
        Modules.check4NewAlertsAndPrivilege(this);
      } else {
        Dialogs.showSessionExpiredAlertDialog();
      }
    }
    Singleton.setIsInForeground(true);
  }

  @Override
  protected void onPause() {
    super.onPause();
    LogEvent.Log(TAG, "onPause() invoked");
    Singleton.setIsInForeground(false);
  }

  @Override
  protected void onDestroy() {
    super.onDestroy();
    if(gpsReceiver != null) {
      unregisterReceiver(gpsReceiver);
    }
  }
}