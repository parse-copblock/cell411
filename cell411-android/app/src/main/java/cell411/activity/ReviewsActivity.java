package cell411.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.ActionBar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.callback.FindCallback;
import com.parse.callback.FunctionCallback;
import com.parse.callback.SaveCallback;
import com.safearx.cell411.R;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;

import cell411.Singleton;
import cell411.methods.Modules;
import cell411.models.Footer;
import cell411.utils.LogEvent;
import cell411.widgets.CircularImageView;
import cell411.widgets.Spinner;

import static cell411.SingletonConfig.DEFAULT_ALERTS_COUNT;

/**
 * Created by Sachin on 27-10-2016.
 */
public class ReviewsActivity extends BaseActivity {
  private static final String TAG = "ReviewsActivity";
  public static WeakReference<ReviewsActivity> weakRef;
  private Spinner spinner;
  private RecyclerView recyclerView;
  private LinearLayoutManager linearLayoutManager;
  private ReviewsListAdapter reviewsListAdapter;
  private ArrayList<Object> reviewsList;
  private RelativeLayout rlReviewDialog;
  private ProgressBar pb;
  private String fullName;
  private String userId;
  private ParseUser parseUser;
  private boolean isRatedByCurrentUser;
  private ParseObject objReview;
  private boolean canReview;
  private int rating = 0;
  private ImageView[] imageViews;

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    switch(item.getItemId()) {
      case android.R.id.home:
        finish();
        return true;
      default:
        return super.onOptionsItemSelected(item);
    }
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_reviews);
    // Set up the action bar.
    final ActionBar actionBar = getSupportActionBar();
    actionBar.setDisplayHomeAsUpEnabled(true);
    weakRef = new WeakReference<>(this);
    Singleton.INSTANCE.setCurrentActivity(this);
    userId = getIntent().getStringExtra("userId");
    canReview = getIntent().getBooleanExtra("canReview", false);
    fullName = ParseUser.getCurrentUser().getString("firstName") + " " +
                 ParseUser.getCurrentUser().getString("lastName");
    spinner = (Spinner) findViewById(R.id.spinner);
    //spinner = (Spinner) findViewById(R.id.spinner);
    recyclerView = (RecyclerView) findViewById(R.id.rv_reviews);
    // use this setting to improve performance if you know that changes
    // in content do not change the layout size of the RecyclerView
    recyclerView.setHasFixedSize(false);
    // use a linear layout manager
    linearLayoutManager = new LinearLayoutManager(this);
    recyclerView.setLayoutManager(linearLayoutManager);
    retrieve();
  }

  private void initializeReviewDialog() {
    pb = (ProgressBar) findViewById(R.id.pb);
    rlReviewDialog = (RelativeLayout) findViewById(R.id.rl_review_dialog);
    rlReviewDialog.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        closeRatingDialog();
      }
    });
    LinearLayout rlLinearLayout = (LinearLayout) findViewById(R.id.rl_review_add);
    rlLinearLayout.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
      }
    });
    ImageView imgUser = (ImageView) findViewById(R.id.img_user);
    imgUser.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
      }
    });
    Singleton.INSTANCE.setImage(imgUser);
    TextView txtReviewedBy = (TextView) findViewById(R.id.txt_reviewed_by);
    txtReviewedBy.setText(getString(R.string.reviewed_by) + " " + fullName);
    imageViews = new ImageView[5];
    imageViews[0] = (ImageView) findViewById(R.id.img_star_1);
    imageViews[1] = (ImageView) findViewById(R.id.img_star_2);
    imageViews[2] = (ImageView) findViewById(R.id.img_star_3);
    imageViews[3] = (ImageView) findViewById(R.id.img_star_4);
    imageViews[4] = (ImageView) findViewById(R.id.img_star_5);
    final EditText etTitle = (EditText) findViewById(R.id.et_title);
    final EditText etComment = (EditText) findViewById(R.id.et_description);
    if(objReview != null) {
      rating = objReview.getInt("rating");
      setRating(rating, imageViews);
      setLabelForRating(rating);
      String title = objReview.getString("title");
      String comment = objReview.getString("comment");
      if(title != null && !title.isEmpty()) {
        etTitle.setText(title);
      }
      if(comment != null && !comment.isEmpty()) {
        etComment.setText(comment);
      }
    }
    imageViews[0].setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        rating = 1;
        setRating(1, imageViews);
        setLabelForRating(1);
      }
    });
    imageViews[1].setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        rating = 2;
        setRating(2, imageViews);
        setLabelForRating(2);
      }
    });
    imageViews[2].setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        rating = 3;
        setRating(3, imageViews);
        setLabelForRating(3);
      }
    });
    imageViews[3].setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        rating = 4;
        setRating(4, imageViews);
        setLabelForRating(4);
      }
    });
    imageViews[4].setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        rating = 5;
        setRating(5, imageViews);
        setLabelForRating(5);
      }
    });
    TextView txtBtnSubmit = (TextView) findViewById(R.id.txt_btn_save);
    if(isRatedByCurrentUser) {
      // Display Update button
      txtBtnSubmit.setText(R.string.update);
    } else {
      // Display Submit button
      txtBtnSubmit.setText(R.string.submit);
    }
    txtBtnSubmit.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        pb.setVisibility(View.VISIBLE);
        if(objReview == null) {
          objReview = new ParseObject("Review");
          objReview.put("ratedBy", ParseUser.getCurrentUser());
          objReview.put("ratedUser", parseUser);
          LogEvent.Log(TAG, "creating new Review");
        }
        LogEvent.Log(TAG, "rating: " + rating);
        objReview.put("rating", rating);
        objReview.put("title", etTitle.getText().toString().trim());
        objReview.put("comment", etComment.getText().toString().trim());
        objReview.saveInBackground(new SaveCallback() {
          @Override
          public void done(ParseException e) {
            pb.setVisibility(View.GONE);
            if(e == null) {
              long millis = objReview.getCreatedAt().getTime();
              GregorianCalendar cal = new GregorianCalendar();
              GregorianCalendar calCurrentTime = new GregorianCalendar();
              calCurrentTime.setTimeInMillis(System.currentTimeMillis());
              calCurrentTime.set(Calendar.HOUR, 0);
              calCurrentTime.set(Calendar.MINUTE, 0);
              calCurrentTime.set(Calendar.SECOND, 0);
              calCurrentTime.set(Calendar.MILLISECOND, 0);
              cal.setTimeInMillis(millis);
              int year = cal.get(Calendar.YEAR);
              int month = cal.get(Calendar.MONTH) + 1;
              int day = cal.get(Calendar.DAY_OF_MONTH);
              int hour = cal.get(Calendar.HOUR);
              int minute = cal.get(Calendar.MINUTE);
              int ampm = cal.get(Calendar.AM_PM);
              String ap;
              if(ampm == 0) {
                ap = getString(R.string.am);
              } else {
                ap = getString(R.string.pm);
              }
              if(hour == 0) {
                hour = 12;
              }
              String hourMinute = "" + hour;
              if(minute > 0 && minute < 10) {
                hourMinute += ":0" + minute;
              } else if(minute > 9) {
                hourMinute += ":" + minute;
              }
              String time;
              if(cal.getTimeInMillis() < calCurrentTime.getTimeInMillis()) {
                // Check if the alert was issued yesterday
                time = month + "/" + day + "/" + year + " " + getString(R.string.at) + " " +
                         hourMinute + " " + ap;
              } else {
                time = getString(R.string.today_at) + " " + hourMinute + " " + ap;
              }
              if(isRatedByCurrentUser) {
                // Current user has edited their rating
                ((ReviewMine) reviewsListAdapter.arrayList.get(0)).rating = rating;
                ((ReviewMine) reviewsListAdapter.arrayList.get(0)).title =
                  objReview.getString("title");
                ((ReviewMine) reviewsListAdapter.arrayList.get(0)).comment =
                  objReview.getString("comment");
                ((ReviewMine) reviewsListAdapter.arrayList.get(0)).reviewTime = time;
              } else {
                // Current user has added their rating
                isRatedByCurrentUser = true;
                reviewsListAdapter.arrayList.remove(0);
                reviewsListAdapter.arrayList.add(0,
                  new ReviewMine(fullName, rating, time, objReview.getString("title"),
                    objReview.getString("comment")));
              }
              retrieveAverageStars();
              reviewsListAdapter.notifyDataSetChanged();
              closeRatingDialog();
            } else {
              if(Modules.isWeakReferenceValid()) {
                Singleton.sendToast(e);
              }
            }
          }
        });
      }
    });
  }

  private void showCannotSubmitReviewAlertDialog() {
    AlertDialog.Builder alert = new AlertDialog.Builder(this);
    alert.setMessage(R.string.you_can_add_review_once_the_ride_is_confirmed);
    alert.setPositiveButton(R.string.dialog_btn_ok, new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialog, int which) {
      }
    });
    AlertDialog dialog = alert.create();
    dialog.show();
  }

  private void retrieve() {
    ParseQuery<ParseUser> parseUserParseQuery = ParseUser.getQuery();
    parseUserParseQuery.whereEqualTo("objectId", userId);
    parseUserParseQuery.findInBackground(new FindCallback<ParseUser>() {
      @Override
      public void done(List<ParseUser> list, ParseException e) {
        if(e == null) {
          if(list != null && list.size() > 0) {
            parseUser = list.get(0);
          }
          ParseQuery<ParseObject> parseQuery = new ParseQuery<ParseObject>("Review");
          parseQuery.whereEqualTo("ratedBy", ParseUser.getCurrentUser());
          parseQuery.whereEqualTo("ratedUser", parseUser);
          parseQuery.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> list, ParseException e) {
              if(e == null) {
                reviewsList = new ArrayList<>();
                if(list != null && list.size() > 0) {
                  isRatedByCurrentUser = true;
                  objReview = list.get(0);
                  long millis = objReview.getCreatedAt().getTime();
                  GregorianCalendar cal = new GregorianCalendar();
                  GregorianCalendar calCurrentTime = new GregorianCalendar();
                  calCurrentTime.setTimeInMillis(System.currentTimeMillis());
                  calCurrentTime.set(Calendar.HOUR, 0);
                  calCurrentTime.set(Calendar.MINUTE, 0);
                  calCurrentTime.set(Calendar.SECOND, 0);
                  calCurrentTime.set(Calendar.MILLISECOND, 0);
                  cal.setTimeInMillis(millis);
                  int year = cal.get(Calendar.YEAR);
                  int month = cal.get(Calendar.MONTH) + 1;
                  int day = cal.get(Calendar.DAY_OF_MONTH);
                  int hour = cal.get(Calendar.HOUR);
                  int minute = cal.get(Calendar.MINUTE);
                  int ampm = cal.get(Calendar.AM_PM);
                  String ap;
                  if(ampm == 0) {
                    ap = getString(R.string.am);
                  } else {
                    ap = getString(R.string.pm);
                  }
                  if(hour == 0) {
                    hour = 12;
                  }
                  String hourMinute = "" + hour;
                  if(minute > 0 && minute < 10) {
                    hourMinute += ":0" + minute;
                  } else if(minute > 9) {
                    hourMinute += ":" + minute;
                  }
                  String time;
                  if(cal.getTimeInMillis() < calCurrentTime.getTimeInMillis()) {
                    // Check if the alert was issued yesterday
                    time = month + "/" + day + "/" + year + " " + getString(R.string.at) + " " +
                             hourMinute + " " + ap;
                  } else {
                    time = getString(R.string.today_at) + " " + hourMinute + " " + ap;
                  }
                  int rating = objReview.getInt("rating");
                  String title = objReview.getString("title");
                  String comment = objReview.getString("comment");
                  reviewsList.add(new ReviewMine(fullName, rating, time, title, comment));
                } else {
                  reviewsList.add(new ReviewAdd(fullName));
                }
                reviewsListAdapter = new ReviewsListAdapter(reviewsList);
                recyclerView.setAdapter(reviewsListAdapter);
                initializeReviewDialog();
                retrieveAverageStars();
                retrieveReviews();
              } else {
                spinner.setVisibility(View.GONE);
                if(Modules.isWeakReferenceValid()) {
                  Singleton.sendToast(e);
                }
              }
            }
          });
        } else {
          spinner.setVisibility(View.GONE);
          if(Modules.isWeakReferenceValid()) {
            Singleton.sendToast(e);
          }
        }
      }
    });
  }

  private void retrieveAverageStars() {
    HashMap<String, Object> params = new HashMap<>();
    params.put("userId", userId);
    ParseCloud.callFunctionInBackground("averageStars", params, new FunctionCallback<String>() {
      public void done(String result, ParseException e) {
        spinner.setVisibility(View.GONE);
        if(e == null) {
          LogEvent.Log(TAG, "result: " + result);
          String[] totalRatingAndCountArr = result.split(",");
          int totalReviews = Integer.parseInt(totalRatingAndCountArr[0]);
          if(reviewsList.size() >= 2) {
            if(reviewsList.get(1) instanceof ReviewTotal) {
              ((ReviewTotal) reviewsList.get(1)).averageRating =
                Float.parseFloat(totalRatingAndCountArr[1]);
              ((ReviewTotal) reviewsList.get(1)).totalReviews =
                Long.parseLong(totalRatingAndCountArr[0]);
            } else {
              if(totalReviews >
                   0) { // To prevent displaying the total rating newPrivateCell when the user has not yet been rated
                reviewsList.add(1, new ReviewTotal(Long.parseLong(totalRatingAndCountArr[0]),
                  Float.parseFloat(totalRatingAndCountArr[1])));
              }
            }
          } else {
            if(totalReviews >
                 0) { // To prevent displaying the total rating newPrivateCell when the user has not yet been rated
              reviewsList.add(1, new ReviewTotal(Long.parseLong(totalRatingAndCountArr[0]),
                Float.parseFloat(totalRatingAndCountArr[1])));
            }
          }
          reviewsListAdapter.notifyDataSetChanged();
        } else {
          if(Modules.isWeakReferenceValid()) {
            Singleton.sendToast(e);
          }
        }
      }
    });
  }

  private void retrieveReviews() {
    ParseQuery<ParseObject> parseQuery = new ParseQuery<>("Review");
    parseQuery.whereNotEqualTo("ratedBy", ParseUser.getCurrentUser());
    parseQuery.whereEqualTo("ratedUser", parseUser);
    parseQuery.include("ratedBy");
    parseQuery.orderByDescending("createdAt");
    parseQuery.setLimit(DEFAULT_ALERTS_COUNT);
    parseQuery.findInBackground(new FindCallback<ParseObject>() {
      @Override
      public void done(List<ParseObject> list, ParseException e) {
        if(e == null) {
          if(list != null && list.size() > 0) {
            GregorianCalendar cal = new GregorianCalendar();
            GregorianCalendar calCurrentTime = new GregorianCalendar();
            calCurrentTime.setTimeInMillis(System.currentTimeMillis());
            calCurrentTime.set(Calendar.HOUR, 0);
            calCurrentTime.set(Calendar.MINUTE, 0);
            calCurrentTime.set(Calendar.SECOND, 0);
            calCurrentTime.set(Calendar.MILLISECOND, 0);
            for(int i = 0; i < list.size(); i++) {
              ParseObject objReview = list.get(i);
              ParseUser ratedBy = objReview.getParseUser("ratedBy");
              String fullName = ratedBy.get("firstName") + " " + ratedBy.get("lastName");
              String email = null;
              if(ratedBy.getEmail() != null && !ratedBy.getEmail().isEmpty()) {
                email = ratedBy.getEmail();
              } else {
                email = ratedBy.getUsername();
              }
              String userId = ratedBy.getObjectId();
              int imageName = ratedBy.getInt("imageName");
              long millis = objReview.getCreatedAt().getTime();
              cal.setTimeInMillis(millis);
              int year = cal.get(Calendar.YEAR);
              int month = cal.get(Calendar.MONTH) + 1;
              int day = cal.get(Calendar.DAY_OF_MONTH);
              int hour = cal.get(Calendar.HOUR);
              int minute = cal.get(Calendar.MINUTE);
              int ampm = cal.get(Calendar.AM_PM);
              String ap;
              if(ampm == 0) {
                ap = getString(R.string.am);
              } else {
                ap = getString(R.string.pm);
              }
              if(hour == 0) {
                hour = 12;
              }
              String hourMinute = "" + hour;
              if(minute > 0 && minute < 10) {
                hourMinute += ":0" + minute;
              } else if(minute > 9) {
                hourMinute += ":" + minute;
              }
              String time;
              if(cal.getTimeInMillis() < calCurrentTime.getTimeInMillis()) {
                // Check if the alert was issued yesterday
                time = month + "/" + day + "/" + year + " " + getString(R.string.at) + " " +
                         hourMinute + " " + ap;
              } else {
                time = getString(R.string.today_at) + " " + hourMinute + " " + ap;
              }
              int rating = objReview.getInt("rating");
              String title = objReview.getString("title");
              String comment = objReview.getString("comment");
              reviewsList.add(
                new Review(fullName, email, userId, "" + imageName, rating, time, title, comment,
                  ratedBy.getInt("isDeleted")));
              reviewsListAdapter.notifyDataSetChanged();
            }
          } else {
            // No reviews found
          }
        } else {
        }
      }
    });
  }

  @Override
  public void onBackPressed() {
    if(rlReviewDialog.getVisibility() == View.VISIBLE) {
      closeRatingDialog();
    } else {
      super.onBackPressed();
    }
  }

  private void closeRatingDialog() {
    rlReviewDialog.setVisibility(View.GONE);
    if(objReview != null) {
      rating = objReview.getInt("rating");
      setRating(rating, imageViews);
    } else {
      setRating(0, imageViews);
    }
    reviewsListAdapter.notifyDataSetChanged();
  }

  private void setLabelForRating(int rate) {
    final TextView txtLblRating = (TextView) findViewById(R.id.txt_lbl_rating);
    switch(rate) {
      case 1:
        txtLblRating.setText(R.string.hated_it);
        break;
      case 2:
        txtLblRating.setText(R.string.disliked_it);
        break;
      case 3:
        txtLblRating.setText(R.string.its_ok);
        break;
      case 4:
        txtLblRating.setText(R.string.liked_it);
        break;
      case 5:
        txtLblRating.setText(R.string.loved_it);
        break;
    }
  }

  @Override
  public void onResume() {
    super.onResume();
    if(reviewsListAdapter != null) {
      reviewsListAdapter.notifyDataSetChanged();
    }
  }

  private void setRating(int rate, ImageView[] imageViews) {
    for(int i = 0; i < 5; i++) {
      if(i < rate) {
        imageViews[i].setImageResource(R.drawable.ic_star_large_gold);
      } else {
        imageViews[i].setImageResource(R.drawable.ic_star_large_gray);
      }
    }
  }

  private void setRating(float rate, ImageView[] imageViews) {
    int fullStars = (int) rate;
    for(int i = 0; i < 5; i++) {
      if(i < rate) {
        if(i == fullStars) {
          imageViews[i].setImageResource(R.drawable.ic_star_large_half_gold);
        } else {
          imageViews[i].setImageResource(R.drawable.ic_star_large_gold);
        }
      } else {
        imageViews[i].setImageResource(R.drawable.ic_star_large_gray);
      }
    }
  }

  public static class ReviewAdd {
    public String fullName;

    public ReviewAdd(String fullName) {
      this.fullName = fullName;
    }
  }

  public static class ReviewMine {
    public String fullName;
    public int rating;
    public String reviewTime;
    public String title;
    public String comment;

    public ReviewMine(String fullName, int rating, String reviewTime, String title,
                      String comment) {
      this.fullName = fullName;
      this.rating = rating;
      this.reviewTime = reviewTime;
      this.title = title;
      this.comment = comment;
    }
  }

  private static class ReviewTotal {
    public long totalReviews;
    public float averageRating;

    public ReviewTotal(long totalReviews, float averageRating) {
      this.totalReviews = totalReviews;
      this.averageRating = averageRating;
    }
  }

  public static class Review {
    public String ratedByFullName;
    public String ratedByEmail;
    public String ratedByUserId;
    public String ratedByImageName;
    public int rating;
    public String reviewTime;
    public String title;
    public String comment;
    public int isDeleted;

    public Review(String ratedByFullName, String ratedByEmail, String ratedByUserId,
                  String ratedByImageName, int rating, String reviewTime, String title,
                  String comment, int isDeleted) {
      this.ratedByFullName = ratedByFullName;
      this.ratedByEmail = ratedByEmail;
      this.ratedByUserId = ratedByUserId;
      this.ratedByImageName = ratedByImageName;
      this.rating = rating;
      this.reviewTime = reviewTime;
      this.title = title;
      this.comment = comment;
      this.isDeleted = isDeleted;
    }
  }

  public class ReviewsListAdapter extends RecyclerView.Adapter<ReviewsListAdapter.ViewHolder> {
    private final int VIEW_TYPE_REVIEW_ADD = 0;
    private final int VIEW_TYPE_REVIEW_TOTAL = 1;
    private final int VIEW_TYPE_REVIEW_MINE = 2;
    private final int VIEW_TYPE_REVIEW = 3;
    private final int VIEW_TYPE_FOOTER = 4;
    private final View.OnClickListener mOnClickListener = new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        int index = recyclerView.getChildAdapterPosition(v);
      }
    };
    public ArrayList<Object> arrayList;

    // Provide a suitable constructor (depends on the kind of data set)
    public ReviewsListAdapter(ArrayList<Object> arrayList) {
      this.arrayList = arrayList;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ReviewsListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
      // create a new view
      View v = null;
      if(viewType == VIEW_TYPE_REVIEW_ADD) {
        v = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_review_add, parent,
          false);
      } else if(viewType == VIEW_TYPE_REVIEW_MINE) {
        v = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_review_mine, parent,
          false);
      } else if(viewType == VIEW_TYPE_REVIEW_TOTAL) {
        v = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_review_total, parent,
          false);
      } else if(viewType == VIEW_TYPE_REVIEW) {
        v = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_review, parent, false);
      } else if(viewType == VIEW_TYPE_FOOTER) {
        v = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_footer, parent, false);
      }
      // set the view's size, margins, paddings and layout parameters
      ViewHolder vh = new ViewHolder(v, viewType);
      //v.setOnClickListener(mOnClickListener);
      return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int position) {
      // - get element from your data set at this position
      // - replace the contents of the view with that element
      if(getItemViewType(position) == VIEW_TYPE_REVIEW_ADD) {
        final ReviewAdd reviewAdd = (ReviewAdd) arrayList.get(position);
        Singleton.INSTANCE.setImage(viewHolder.imgUser);
        viewHolder.txtName.setText(reviewAdd.fullName);
        setRating(0, viewHolder.imageViews);
        viewHolder.imageViews[0].setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View view) {
            if(!canReview) {
              showCannotSubmitReviewAlertDialog();
              return;
            }
            rating = 1;
            rlReviewDialog.setVisibility(View.VISIBLE);
            setRating(1, viewHolder.imageViews);
            setRating(1, imageViews);
            setLabelForRating(1);
          }
        });
        viewHolder.imageViews[1].setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View view) {
            if(!canReview) {
              showCannotSubmitReviewAlertDialog();
              return;
            }
            rating = 2;
            rlReviewDialog.setVisibility(View.VISIBLE);
            setRating(2, viewHolder.imageViews);
            setRating(2, imageViews);
            setLabelForRating(2);
          }
        });
        viewHolder.imageViews[2].setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View view) {
            if(!canReview) {
              showCannotSubmitReviewAlertDialog();
              return;
            }
            rating = 3;
            rlReviewDialog.setVisibility(View.VISIBLE);
            setRating(3, viewHolder.imageViews);
            setRating(3, imageViews);
            setLabelForRating(3);
          }
        });
        viewHolder.imageViews[3].setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View view) {
            if(!canReview) {
              showCannotSubmitReviewAlertDialog();
              return;
            }
            rating = 4;
            rlReviewDialog.setVisibility(View.VISIBLE);
            setRating(4, viewHolder.imageViews);
            setRating(4, imageViews);
            setLabelForRating(4);
          }
        });
        viewHolder.imageViews[4].setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View view) {
            if(!canReview) {
              showCannotSubmitReviewAlertDialog();
              return;
            }
            rating = 5;
            rlReviewDialog.setVisibility(View.VISIBLE);
            setRating(5, viewHolder.imageViews);
            setRating(5, imageViews);
            setLabelForRating(5);
          }
        });
      } else if(getItemViewType(position) == VIEW_TYPE_REVIEW_MINE) {
        final ReviewMine reviewMine = (ReviewMine) arrayList.get(position);
        viewHolder.imgEditReview.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View view) {
            rlReviewDialog.setVisibility(View.VISIBLE);
          }
        });
        setRating(reviewMine.rating, viewHolder.imageViews);
        viewHolder.txtName.setText(reviewMine.fullName);
        viewHolder.txtTime.setText(reviewMine.reviewTime);
        viewHolder.txtTitle.setText(reviewMine.title);
        viewHolder.txtComment.setText(reviewMine.comment);
        Singleton.INSTANCE.setImage(viewHolder.imgUser);
        if(reviewMine.title == null || reviewMine.title.isEmpty()) {
          viewHolder.txtTitle.setVisibility(View.GONE);
        } else {
          viewHolder.txtTitle.setVisibility(View.VISIBLE);
        }
        if(reviewMine.comment == null || reviewMine.comment.isEmpty()) {
          viewHolder.txtComment.setVisibility(View.GONE);
        } else {
          viewHolder.txtComment.setVisibility(View.VISIBLE);
        }
      } else if(getItemViewType(position) == VIEW_TYPE_REVIEW_TOTAL) {
        final ReviewTotal reviewTotal = (ReviewTotal) arrayList.get(position);
        setRating(reviewTotal.averageRating, viewHolder.imageViews);
        if(reviewTotal.totalReviews == 0) {
          // not yet rated
        } else {
          viewHolder.txtCount.setText(Singleton.formatText(reviewTotal.totalReviews));
          viewHolder.txtTotalRating.setText(String.format("%.1f", reviewTotal.averageRating));
        }
      } else if(getItemViewType(position) == VIEW_TYPE_REVIEW) {
        final Review review = (Review) arrayList.get(position);
        setRating(review.rating, viewHolder.imageViews);
        viewHolder.txtName.setText(review.ratedByFullName);
        viewHolder.txtTime.setText(review.reviewTime);
        viewHolder.txtTitle.setText(review.title);
        viewHolder.txtComment.setText(review.comment);
        if(review.isDeleted == 1) {
          viewHolder.imgUser.setImageResource(R.drawable.ic_placeholder_user);
        } else {
          Singleton.INSTANCE.setImage(viewHolder.imgUser,
            review.ratedByUserId + review.ratedByImageName, review.ratedByEmail);
        }
        if(review.title == null || review.title.isEmpty()) {
          viewHolder.txtTitle.setVisibility(View.GONE);
        } else {
          viewHolder.txtTitle.setVisibility(View.VISIBLE);
        }
        if(review.comment == null || review.comment.isEmpty()) {
          viewHolder.txtComment.setVisibility(View.GONE);
        } else {
          viewHolder.txtComment.setVisibility(View.VISIBLE);
        }
      } else {
        final Footer footer = (Footer) arrayList.get(position);
        viewHolder.txtInfo.setText(footer.info);
        if(footer.spinnerVisibility) {
          viewHolder.spinner.setVisibility(View.VISIBLE);
        } else {
          viewHolder.spinner.setVisibility(View.GONE);
        }
      }
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
      return arrayList.size();
    }

    @Override
    public int getItemViewType(int position) {
      super.getItemViewType(position);
      if(arrayList.get(position) instanceof ReviewAdd) {
        return VIEW_TYPE_REVIEW_ADD;
      } else if(arrayList.get(position) instanceof ReviewMine) {
        return VIEW_TYPE_REVIEW_MINE;
      } else if(arrayList.get(position) instanceof ReviewTotal) {
        return VIEW_TYPE_REVIEW_TOTAL;
      } else if(arrayList.get(position) instanceof Review) {
        return VIEW_TYPE_REVIEW;
      } else {
        return VIEW_TYPE_FOOTER;
      }
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public class ViewHolder extends RecyclerView.ViewHolder {
      // each data item is just a string in this case
      private CircularImageView imgUser;
      private TextView txtName;
      private ImageView[] imageViews;
      private TextView txtTime;
      private ImageView imgEditReview;
      private TextView txtTitle;
      private TextView txtComment;
      private TextView txtTotalRating;
      private TextView txtCount;
      // Footer
      private TextView txtInfo;
      private Spinner spinner;

      public ViewHolder(View view, int type) {
        super(view);
        imageViews = new ImageView[5];
        if(type == VIEW_TYPE_REVIEW_ADD) {
          imgUser = (CircularImageView) view.findViewById(R.id.img_user);
          txtName = (TextView) view.findViewById(R.id.txt_name);
          imageViews[0] = (ImageView) view.findViewById(R.id.img_star_1);
          imageViews[1] = (ImageView) view.findViewById(R.id.img_star_2);
          imageViews[2] = (ImageView) view.findViewById(R.id.img_star_3);
          imageViews[3] = (ImageView) view.findViewById(R.id.img_star_4);
          imageViews[4] = (ImageView) view.findViewById(R.id.img_star_5);
        } else if(type == VIEW_TYPE_REVIEW_MINE) {
          imgUser = (CircularImageView) view.findViewById(R.id.img_user);
          txtName = (TextView) view.findViewById(R.id.txt_name);
          imageViews[0] = (ImageView) view.findViewById(R.id.img_star_1);
          imageViews[1] = (ImageView) view.findViewById(R.id.img_star_2);
          imageViews[2] = (ImageView) view.findViewById(R.id.img_star_3);
          imageViews[3] = (ImageView) view.findViewById(R.id.img_star_4);
          imageViews[4] = (ImageView) view.findViewById(R.id.img_star_5);
          txtTime = (TextView) view.findViewById(R.id.txt_time);
          imgEditReview = (ImageView) view.findViewById(R.id.img_edit_review);
          txtTitle = (TextView) view.findViewById(R.id.txt_title);
          txtComment = (TextView) view.findViewById(R.id.txt_comment);
        } else if(type == VIEW_TYPE_REVIEW_TOTAL) {
          txtTotalRating = (TextView) view.findViewById(R.id.txt_total_rating);
          imageViews[0] = (ImageView) view.findViewById(R.id.img_star_1);
          imageViews[1] = (ImageView) view.findViewById(R.id.img_star_2);
          imageViews[2] = (ImageView) view.findViewById(R.id.img_star_3);
          imageViews[3] = (ImageView) view.findViewById(R.id.img_star_4);
          imageViews[4] = (ImageView) view.findViewById(R.id.img_star_5);
          txtCount = (TextView) view.findViewById(R.id.txt_count);
        } else if(type == VIEW_TYPE_REVIEW) {
          imgUser = (CircularImageView) view.findViewById(R.id.img_user);
          txtName = (TextView) view.findViewById(R.id.txt_name);
          imageViews[0] = (ImageView) view.findViewById(R.id.img_star_1);
          imageViews[1] = (ImageView) view.findViewById(R.id.img_star_2);
          imageViews[2] = (ImageView) view.findViewById(R.id.img_star_3);
          imageViews[3] = (ImageView) view.findViewById(R.id.img_star_4);
          imageViews[4] = (ImageView) view.findViewById(R.id.img_star_5);
          txtTime = (TextView) view.findViewById(R.id.txt_time);
          txtTitle = (TextView) view.findViewById(R.id.txt_title);
          txtComment = (TextView) view.findViewById(R.id.txt_comment);
        } else if(type == VIEW_TYPE_FOOTER) {
          txtInfo = (TextView) view.findViewById(R.id.txt_info);
          spinner = (Spinner) view.findViewById(R.id.spinner);
        }
      }
    }
  }
}