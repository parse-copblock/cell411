package cell411.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.callback.FunctionCallback;
import com.safearx.cell411.R;

import java.util.HashMap;
import java.util.List;

import cell411.Singleton;
import cell411.utils.LogEvent;
import cell411.widgets.Spinner;

/**
 * Created by Sachin on 18-02-2017.
 */
public class ResetPasswordActivity extends AppCompatActivity implements View.OnClickListener {
  private static String TAG = "ResetPasswordActivity";
  private EditText etNewPassword;
  private EditText etConfirmNewPassword;
  private String userId;
  private Spinner spinner;

  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_reset_password);
    Uri data = getIntent().getData();
    List<String> params = data.getPathSegments();
    userId = params.get(0); // "objectId"
    //LogEvent.Log(TAG, "userId: " + userId);
    etNewPassword = (EditText) findViewById(R.id.et_new_password);
    etConfirmNewPassword = (EditText) findViewById(R.id.et_confirm_new_password);
    spinner = (Spinner) findViewById(R.id.spinner);
    TextView txtSubmit = (TextView) findViewById(R.id.txt_btn_submit);
    txtSubmit.setOnClickListener(this);
  }

  @Override
  public void onClick(View view) {
    int id = view.getId();
    switch(id) {
      case R.id.txt_btn_submit:
        if(spinner.getVisibility() != View.VISIBLE) {
          if(isValidPassword()) {
            resetPassword();
          }
        } else {
          Singleton.sendToast("Please wait");
        }
        break;
    }
  }

  private void resetPassword() {
    spinner.setVisibility(View.VISIBLE);
    String newPassword = etNewPassword.getText().toString().trim();
    // call forgotPassword cloud function here
    HashMap<String, Object> params = new HashMap<>();
    params.put("userId", userId);
    params.put("password", newPassword);
    ParseCloud.callFunctionInBackground("resetPassword", params, new FunctionCallback<String>() {
      public void done(String result, ParseException e) {
        spinner.setVisibility(View.GONE);
        if(e == null) {
          // Password reset successfully
          LogEvent.Log(TAG, "result: " + result);
          Singleton.sendToast(result);
          Intent intentSplash = new Intent(ResetPasswordActivity.this, SplashActivity.class);
          startActivity(intentSplash);
          finish();
        } else {
          LogEvent.Log(TAG, "Something went wrong: " + e.getMessage());
          Singleton.sendToast(e);
        }
      }
    });
  }

  private boolean isValidPassword() {
    String newPassword = etNewPassword.getText().toString().trim();
    String confirmNewPassword = etConfirmNewPassword.getText().toString().trim();
    if(newPassword.isEmpty()) {
      Singleton.sendToast("New password field cannot be empty");
      return false;
    } else if(confirmNewPassword.isEmpty()) {
      Singleton.sendToast("Confirm new password field cannot be empty");
      return false;
    } else if(!newPassword.equals(confirmNewPassword)) {
      Singleton.sendToast("Password does not match");
      return false;
    } else {
      return true;
    }
  }
}
