package cell411.activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.ActionBar;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseGeoPoint;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParsePush;
import com.parse.ParseQuery;
import com.parse.ParseRelation;
import com.parse.ParseUser;
import com.parse.callback.FindCallback;
import com.parse.callback.FunctionCallback;
import com.parse.callback.GetCallback;
import com.parse.callback.SaveCallback;
import com.safearx.cell411.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import cell411.Singleton;
import cell411.constants.Constants;
import cell411.models.Cell;
import cell411.models.CellCategory;
import cell411.models.PublicCell;
import cell411.services.FetchAddressIntentService;
import cell411.utils.LogEvent;
import cell411.widgets.Spinner;

import static cell411.Singleton.sendToast;
import static cell411.SingletonConfig.DEFAULT_MILES_FOR_NEW_PUBLIC_CELL_ALERT;

/**
 * Created by Sachin on 19-04-2016.
 */
public class CreateOrEditPublicCellActivity extends BaseActivity implements OnMapReadyCallback {
  public static WeakReference<CreateOrEditPublicCellActivity> weakRef;
  private final String TAG = "CreateOrEditPublicCellActivity";
  private GoogleMap mMap;
  private Spinner spinner;
  private TextView txtAddress;
  private EditText etCellName;
  private EditText etCellDescription;
  private android.widget.Spinner spCellCategory;
  private AddressResultReceiver mResultReceiver;
  private double lat;
  private double lng;
  private String cellId;
  private String cellName;
  private int cellType;
  private String description;
  private String city;
  private String country;
  private String fullAddress;
  private double oldLat;
  private double oldLng;
  private boolean isInEditMode;
  private MenuItem miCreate;
  private MenuItem miUpdate;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_create_public_cell);
    // Set up the action bar.
    final ActionBar actionBar = getSupportActionBar();
    actionBar.setDisplayHomeAsUpEnabled(true);
    weakRef = new WeakReference<>(this);
    Singleton.INSTANCE.setCurrentActivity(this);
    spinner = (Spinner) findViewById(R.id.spinner);
    txtAddress = (TextView) findViewById(R.id.txt_address);
    etCellName = (EditText) findViewById(R.id.et_cell_name);
    etCellDescription = (EditText) findViewById(R.id.et_cell_description);
    spCellCategory = (android.widget.Spinner) findViewById(R.id.sp_cell_category);
        /*String[] categoryArray = getResources().getStringArray(
                R.array.category_cell);*/
    ArrayList<CellCategory> categoryArray = new ArrayList<>();
    categoryArray.add(new CellCategory(getString(R.string.category_cell_activism), 1));
    categoryArray.add(new CellCategory(getString(R.string.category_cell_journalism), 6));
    categoryArray.add(new CellCategory(getString(R.string.category_cell_personal_safety), 7));
    categoryArray.add(new CellCategory(getString(R.string.category_cell_community_safety), 3));
    categoryArray.add(new CellCategory(getString(R.string.category_cell_government), 5));
    categoryArray.add(new CellCategory(getString(R.string.category_cell_education), 4));
    categoryArray.add(new CellCategory(getString(R.string.category_cell_commercial), 2));
    CategoryListAdapter adapterCategory =
      new CategoryListAdapter(this, R.layout.cell_public_cell_category, categoryArray);
    adapterCategory.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
    spCellCategory.setAdapter(adapterCategory);
        /*ArrayAdapter<CellCategory> adapterCategory = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_item, categoryArray);
        adapterCategory.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spCellCategory.setAdapter(adapterCategory);*/
    spCellCategory.setSelection(0);
    isInEditMode = getIntent().getBooleanExtra("isInEditMode", false);
    if(isInEditMode) {
      cellId = getIntent().getStringExtra("cellId");
      cellName = getIntent().getStringExtra("cellName");
      cellType = getIntent().getIntExtra("cellType", 1);
      description = getIntent().getStringExtra("description");
      oldLat = lat = getIntent().getDoubleExtra("lat", 0);
      oldLng = lng = getIntent().getDoubleExtra("lon", 0);
      etCellName.setText(cellName);
      if(description != null) {
        etCellDescription.setText(description);
      }
      for(int i = 0; i < categoryArray.size(); i++) {
        if(categoryArray.get(i).getCellType() == cellType) {
          spCellCategory.setSelection(i);
          break;
        }
      }
      actionBar.setTitle(R.string.title_update_public_cell);
    } else {
      lat = Singleton.INSTANCE.getLatitude();
      lng = Singleton.INSTANCE.getLongitude();
      actionBar.setTitle(R.string.title_create_public_cell);
    }
    // Obtain the SupportMapFragment and get notified when the map is ready to be used.
    SupportMapFragment mapFragment =
      (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
    mapFragment.getMapAsync(this);
    // Initialize the receiver and start the service for reverse geo coded address
    mResultReceiver = new AddressResultReceiver(new Handler());
    startIntentService();
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    switch(item.getItemId()) {
      case android.R.id.home:
        finish();
        return true;
      case R.id.create:
        createPublicCellIfNameIsValid();
        return true;
      case R.id.update:
        updatePublicCell();
        return true;
      default:
        return super.onOptionsItemSelected(item);
    }
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    getMenuInflater().inflate(R.menu.menu_create_public_cell, menu);
    miCreate = menu.findItem(R.id.create);
    miUpdate = menu.findItem(R.id.update);
    return super.onCreateOptionsMenu(menu);
  }

  @Override
  public boolean onPrepareOptionsMenu(Menu menu) {
    if(isInEditMode) {
      miCreate.setVisible(false);
      miUpdate.setVisible(true);
    } else {
      miCreate.setVisible(true);
      miUpdate.setVisible(false);
    }
    return super.onPrepareOptionsMenu(menu);
  }

  private void createPublicCellIfNameIsValid() {
    if(spinner.getVisibility() != View.VISIBLE) {
      LatLng latLng = mMap.getCameraPosition().target;
      LogEvent.Log(TAG, "latlng: " + latLng.toString());
      if(Singleton.INSTANCE.getPublicCells() == null) {
        ParseQuery<ParseObject> cellQuery = ParseQuery.getQuery("PublicCell");
        cellQuery.whereEqualTo("createdBy", ParseUser.getCurrentUser());
        cellQuery.findInBackground(new FindCallback<ParseObject>() {
          @Override
          public void done(List<ParseObject> parseObjects, ParseException e) {
            if(e == null) {
              ArrayList<Object> myPublicCellsList = new ArrayList<>();
              if(parseObjects != null) {
                for(int i = 0; i < parseObjects.size(); i++) {
                  ParseObject cell = parseObjects.get(i);
                  Cell cl = new Cell((String) cell.get("name"), cell);
                  cl.totalMembers = (int) cell.get("totalMembers");
                  cl.verificationStatus = (int) cell.get("verificationStatus");
                  cl.status = Cell.Status.OWNER;
                  myPublicCellsList.add(cl);
                }
              }
              Singleton.INSTANCE.setPublicCells((ArrayList<Cell>) myPublicCellsList.clone());
              createPublicCell();
            } else {
              doSendToast(e);
            }
          }
        });
      } else {
        createPublicCell();
      }
    } else {
      sendToast(R.string.please_wait);
    }
  }

  private void doSendToast(ParseException e) {
    sendToast(e);
  }

  private void createPublicCell() {
    final String cellName = etCellName.getText().toString().trim();
    final String description = etCellDescription.getText().toString().trim();
    if(Singleton.INSTANCE.getPublicCells() != null) {
      for(int i = 0; i < Singleton.INSTANCE.getPublicCells().size(); i++) {
        if(Singleton.INSTANCE.getPublicCells().get(i).cell != null) {
          if(Singleton.INSTANCE.getPublicCells().get(i).name.equals(cellName)) {
            getApplicationContext();
            sendToast(cellName + " " + getString(R.string.already_created));
            return;
          }
        }
      }
    }
    if(cellName.isEmpty()) {
      sendToast(R.string.please_enter_cell_name);
    } else if(description.isEmpty()) {
      sendToast(R.string.please_enter_cell_description);
    } else {
      spinner.setVisibility(View.VISIBLE);
      final ParseGeoPoint parseGeoPoint =
        new ParseGeoPoint(mMap.getCameraPosition().target.latitude,
          mMap.getCameraPosition().target.longitude);
      final ParseObject cellObject = new ParseObject("PublicCell");
      cellObject.put("createdBy", ParseUser.getCurrentUser());
      cellObject.put("name", cellName);
      cellObject.put("description", description);
      cellObject.put("cellType", ((CellCategory) spCellCategory.getSelectedItem()).getCellType());
      cellObject.put("verificationStatus", 0);
      cellObject.put("totalMembers", 1);
      cellObject.put("geoTag", parseGeoPoint);
      if(city == null) {
        cellObject.remove("city");
      } else {
        cellObject.put("city", city);
      }
      if(city == null) {
        cellObject.remove("country");
      } else {
        cellObject.put("country", country);
      }
      if(city == null) {
        cellObject.remove("fullAddress");
      } else {
        cellObject.put("fullAddress", fullAddress);
      }
      ParseRelation relation = cellObject.getRelation("members");
      relation.add(ParseUser.getCurrentUser());
      cellObject.saveInBackground(new SaveCallback() {
        @Override
        public void done(ParseException e) {
          spinner.setVisibility(View.GONE);
          if(e == null) {
            // Start listening to the newly created newPrivateCell for chat messages
                        /*Intent chatServiceIntent = new Intent(CreateOrEditPublicCellActivity.this, ChatService.class);
                        chatServiceIntent.putExtra("ChatServiceTask", ChatService.ChatServiceTask.ATTACH_LISTENER);
                        chatServiceIntent.putExtra("entityType", ChatRoom.EntityType.PUBLIC_CELL);
                        chatServiceIntent.putExtra("entityObjectId", cellObject.getObjectId());
                        chatServiceIntent.putExtra("entityCreatedAt", String.valueOf(cellObject.getCreatedAt().getTime()));
                        chatServiceIntent.putExtra("entityName", cellName);
                        startService(chatServiceIntent);*/
            final Cell cell = new Cell(cellName, cellObject);
            cell.totalMembers = 1;
            cell.verificationStatus = 0;
            cell.status = Cell.Status.OWNER;
            Singleton.INSTANCE.addPublicCellToList(cell);
            Singleton.INSTANCE.setNewPublicCellCreated(true);
            sendToast(R.string.cell_added_successfully);
            ParseQuery<ParseUser> query = ParseUser.getQuery();
            query.whereEqualTo("newPublicCellAlert", 1);
            query.whereNotEqualTo("objectId", ParseUser.getCurrentUser().getObjectId());
            query.whereWithinMiles("location", parseGeoPoint,
              DEFAULT_MILES_FOR_NEW_PUBLIC_CELL_ALERT);
            ParseUser user = ParseUser.getCurrentUser();
            ParseRelation relation = user.getRelation("spammedBy");
            ParseQuery parseQuery = relation.getQuery();
            query.whereDoesNotMatchKeyInQuery("objectId", "objectId", parseQuery);
            query.findInBackground(new FindCallback<ParseUser>() {
              @Override
              public void done(List<ParseUser> list, ParseException e) {
                if(e == null) {
                  if(list != null && list.size() > 0) {
                    ParseQuery pushQuery = ParseInstallation.getQuery();
                    pushQuery.whereContainedIn("user", list);
                    ParsePush push = new ParsePush();
                    push.setQuery(pushQuery);
                    JSONObject data = new JSONObject();
                    try {
                      data.put("alert", getString(R.string.notif_new_public_cell_alert, cellName));
                      data.put("sound", "default");
                      data.put("cellName", cellName);
                      data.put("cellId", cellObject.getObjectId());
                      data.put("userId", ParseUser.getCurrentUser().getObjectId());
                      data.put("alertType", "NEW_PUBLIC_CELL");
                      data.put("badge", "Increment");
                    } catch(JSONException e1) {
                      e1.printStackTrace();
                    }
                    push.setData(data);
                    push.sendInBackground();
                    LogEvent.Log(TAG,
                      "There are " + list.size() + " members patrolling within 50 miles");
                  } else {
                    LogEvent.Log(TAG, "There are no members patrolling within 50 miles");
                  }
                } else {
                  doSendToast(e);
                }
              }
            });
            finish();
          } else {
            doSendToast(e);
          }
        }
      });
    }
  }

  private void updatePublicCell() {
    // Check what are the things that are changed
    boolean isNameChanged = false;
    boolean isCategoryChanged = false;
    boolean isDescriptionChanged = false;
    boolean isLocationChanged = false;
    final String cellName = etCellName.getText().toString().trim();
    final String description = etCellDescription.getText().toString().trim();
    final int cellType = ((CellCategory) spCellCategory.getSelectedItem()).getCellType();
    if(!cellName.equals(this.cellName)) {
      isNameChanged = true;
    }
    if(!description.equals(this.description)) {
      isDescriptionChanged = true;
    }
    if(this.cellType != cellType) {
      isCategoryChanged = true;
    }
    LogEvent.Log(TAG, "oldLat: " + oldLat);
    LogEvent.Log(TAG, "mewLat: " + lat);
    LogEvent.Log(TAG, "oldLng: " + oldLng);
    LogEvent.Log(TAG, "newLng: " + lng);
    final ParseGeoPoint oldParseGeoPoint = new ParseGeoPoint(oldLat, oldLng);
    final ParseGeoPoint newParseGeoPoint = new ParseGeoPoint(lat, lng);
    double km = oldParseGeoPoint.distanceInKilometersTo(newParseGeoPoint);
    double miles = oldParseGeoPoint.distanceInMilesTo(newParseGeoPoint);
    double radians = oldParseGeoPoint.distanceInRadiansTo(newParseGeoPoint);
    LogEvent.Log(TAG, "difference in km: " + km);
    LogEvent.Log(TAG, "difference in miles: " + miles);
    LogEvent.Log(TAG, "difference in radians: " + radians);
    if(km > 0.1) {
      isLocationChanged = true;
    }
    if(!isNameChanged && !isDescriptionChanged && !isCategoryChanged && !isLocationChanged) {
      getApplicationContext();
      sendToast("Nothing to update");
      return;
    }
    if(spinner.getVisibility() != View.VISIBLE) {
      LatLng latLng = mMap.getCameraPosition().target;
      LogEvent.Log(TAG, "latlng: " + latLng.toString());
      if(Singleton.INSTANCE.getPublicCells() != null) {
        for(int i = 0; i < Singleton.INSTANCE.getPublicCells().size(); i++) {
          if(Singleton.INSTANCE.getPublicCells().get(i).cell != null) {
            if(Singleton.INSTANCE.getPublicCells().get(i).name.equals(
              CreateOrEditPublicCellActivity.this.cellName)) {
              continue;
            } else if(Singleton.INSTANCE.getPublicCells().get(i).name.equals(cellName)) {
              getApplicationContext();
              sendToast(cellName + " " + getString(R.string.already_created));
              return;
            }
          }
        }
      }
      if(cellName.isEmpty()) {
        sendToast(R.string.please_enter_cell_name);
      } else if(description.isEmpty()) {
        sendToast(R.string.please_enter_cell_description);
      } else {
        spinner.setVisibility(View.VISIBLE);
        final ParseGeoPoint parseGeoPoint =
          new ParseGeoPoint(mMap.getCameraPosition().target.latitude,
            mMap.getCameraPosition().target.longitude);
        ParseQuery<ParseObject> parseQuery = new ParseQuery<>("PublicCell");
        parseQuery.whereEqualTo("objectId", cellId);
        final boolean finalIsNameChanged = isNameChanged;
        final boolean finalIsCategoryChanged = isCategoryChanged;
        final boolean finalIsDescriptionChanged = isDescriptionChanged;
        final boolean finalIsLocationChanged = isLocationChanged;
        parseQuery.getFirstInBackground(new GetCallback<ParseObject>() {
          @Override
          public void done(ParseObject cellObject, ParseException e) {
            if(e == null) {
              cellObject.put("name", cellName);
              cellObject.put("description", description);
              cellObject.put("cellType", cellType);
              cellObject.put("geoTag", parseGeoPoint);
              if(city == null) {
                cellObject.remove("city");
              } else {
                cellObject.put("city", city);
              }
              if(city == null) {
                cellObject.remove("country");
              } else {
                cellObject.put("country", country);
              }
              if(city == null) {
                cellObject.remove("fullAddress");
              } else {
                cellObject.put("fullAddress", fullAddress);
              }
              cellObject.saveInBackground(new SaveCallback() {
                @Override
                public void done(ParseException e) {
                  if(e == null) {
                    PublicCell publicCell = new PublicCell(cellName, cellId);
                    publicCell.cellType = cellType;
                    publicCell.description = description;
                    publicCell.lat = parseGeoPoint.getLatitude();
                    publicCell.lng = parseGeoPoint.getLongitude();
                    publicCell.city = city;
                    publicCell.country = country;
                    publicCell.fullAddress = fullAddress;
                    Singleton.INSTANCE
                      .setTotalPlacesWherePublicCellChangeRequired(2, publicCell);
                    // replicate the changes on the locally saved array, as this is presented while issuing alerts
                    if(Singleton.INSTANCE.getPublicCells() != null) {
                      for(int i = 0; i < Singleton.INSTANCE.getPublicCells().size(); i++) {
                        if(Singleton.INSTANCE.getPublicCells().get(i).cell != null) {
                          if(Singleton.INSTANCE.getPublicCells().get(i).cell.getObjectId()
                               .equals(
                                 cellId)) {
                            ParseGeoPoint parseGeoPoint =
                              new ParseGeoPoint(publicCell.lat, publicCell.lng);
                            Singleton.INSTANCE.getPublicCells().get(i).name =
                              publicCell.getName();
                            Singleton.INSTANCE.getPublicCells().get(i).cell.put("name",
                              publicCell.getName());
                            Singleton.INSTANCE.getPublicCells().get(i).cell.put("description",
                              publicCell.description);
                            Singleton.INSTANCE.getPublicCells().get(i).cell.put("cellType",
                              publicCell.cellType);
                            Singleton.INSTANCE.getPublicCells().get(i).cell.put("geoTag",
                              parseGeoPoint);
                            if(city == null) {
                              Singleton.INSTANCE.getPublicCells().get(i).cell.remove("city");
                            } else {
                              Singleton.INSTANCE.getPublicCells().get(i).cell.put("city",
                                publicCell.city);
                            }
                            if(city == null) {
                              Singleton.INSTANCE.getPublicCells().get(i).cell
                                .remove("country");
                            } else {
                              Singleton.INSTANCE.getPublicCells().get(i).cell.put("country",
                                publicCell.country);
                            }
                            if(city == null) {
                              Singleton.INSTANCE.getPublicCells().get(i).cell
                                .remove("fullAddress");
                            } else {
                              Singleton.INSTANCE.getPublicCells().get(i).cell
                                .put("fullAddress",
                                  publicCell.fullAddress);
                            }
                            break;
                          }
                        }
                      }
                    }
                    LogEvent.Log(TAG, "CreateOrEditPublicCellActivity.this.cellName: " +
                                        CreateOrEditPublicCellActivity.this.cellName);
                    // Invoke cloud function to send alert to all members of this
                    // newPrivateCell about the newPrivateCell being changed
                    HashMap<String, Object> params = new HashMap<>();
                    params.put("publicCellObjectId", cellId);
                    params.put("publicCellName", CreateOrEditPublicCellActivity.this.cellName);
                    params.put("isNameChanged", finalIsNameChanged);
                    params.put("isCategoryChanged", finalIsCategoryChanged);
                    params.put("isDescriptionChanged", finalIsDescriptionChanged);
                    params.put("isLocationChanged", finalIsLocationChanged);
                    params.put("clientFirmId", 1);
                    params.put("isLive", getResources().getBoolean(R.bool.enable_publish));
                    params.put("languageCode", getString(R.string.language_code));
                    params.put("platform", "android");
                    ParseCloud.callFunctionInBackground("sendChangeIntimationToPublicCellMembers",
                      params, new FunctionCallback<String>() {
                        @Override
                        public void done(String result, ParseException e) {
                          spinner.setVisibility(View.GONE);
                          if(e == null) {
                            sendToast(
                              R.string.cell_modified_successfully_and_intimation_sent_to_members);
                            LogEvent.Log(TAG, "Change intimation sent: " + result);
                          } else {
                            sendToast(R.string.cell_modified_successfully);
                            LogEvent.Log(TAG, "Change intimation not sent: " + e.getMessage());
                          }
                          finish();
                        }
                      });
                  } else {
                    spinner.setVisibility(View.GONE);
                    doSendToast(e);
                  }
                }
              });
            } else {
              spinner.setVisibility(View.GONE);
              doSendToast(e);
            }
          }
        });
      }
    } else {
      sendToast(R.string.please_wait);
    }
  }

  @Override
  public void onMapReady(GoogleMap googleMap) {
    mMap = googleMap;
    setUpMap();
    if(isInEditMode) {
      mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lat, lng), 14f));
    } else {
      mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
        new LatLng(Singleton.INSTANCE.getLatitude(), Singleton.INSTANCE.getLongitude()),
        14f));
    }
    mMap.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
      @Override
      public void onCameraChange(CameraPosition cameraPosition) {
        lat = cameraPosition.target.latitude;
        lng = cameraPosition.target.longitude;
        startIntentService();
      }
    });
  }

  private void setUpMap() {
    mMap.getUiSettings().setAllGesturesEnabled(true);
    mMap.getUiSettings().setCompassEnabled(true);
    mMap.getUiSettings().setMyLocationButtonEnabled(false);
    mMap.getUiSettings().setRotateGesturesEnabled(true);
    mMap.getUiSettings().setScrollGesturesEnabled(true);
    mMap.getUiSettings().setTiltGesturesEnabled(true);
    mMap.getUiSettings().setZoomControlsEnabled(false);
    mMap.getUiSettings().setZoomGesturesEnabled(true);
  }

  protected void startIntentService() {
    Intent intent = new Intent(this, FetchAddressIntentService.class);
    intent.putExtra(Constants.RECEIVER, mResultReceiver);
    intent.putExtra(Constants.LOCATION_DATA_LAT, lat);
    intent.putExtra(Constants.LOCATION_DATA_LNG, lng);
    FetchAddressIntentService.enqueueWork(this, intent);
  }

  private static class ItemViewHolder {
    TextView txtPublicCellCategory;
  }

  @SuppressLint("ParcelCreator")
  class AddressResultReceiver extends ResultReceiver {
    public AddressResultReceiver(Handler handler) {
      super(handler);
    }

    @Override
    protected void onReceiveResult(int resultCode, Bundle resultData) {
      // Display the city
      if(resultCode == Constants.SUCCESS_RESULT) {
        city = resultData.getString(Constants.RESULT_DATA_CITY);
        country = resultData.getString(Constants.RESULT_DATA_COUNTRY);
        fullAddress = resultData.getString(Constants.RESULT_DATA_DETAIL_ADDRESS);
        txtAddress.setText(city);
      } else {
        city = country = fullAddress = null;
        txtAddress.setText(R.string.city_not_found);
      }
    }
  }

  private class CategoryListAdapter extends ArrayAdapter<CellCategory> {
    private final ArrayList<CellCategory> list;
    private int resourceId;
    private LayoutInflater inflator;

    public CategoryListAdapter(Context context, int resourceId, ArrayList<CellCategory> list) {
      super(context, resourceId, list);
      this.list = list;
      this.resourceId = resourceId;
      inflator = getLayoutInflater();
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
      ItemViewHolder holder = null;
      CellCategory item = list.get(position);
      if(convertView == null) {
        holder = new ItemViewHolder();
        convertView = inflator.inflate(resourceId, null);
        holder.txtPublicCellCategory = convertView.findViewById(R.id.txt_public_cell_category);
        convertView.setTag(holder);
      }
      holder = (ItemViewHolder) convertView.getTag();
      holder.txtPublicCellCategory.setText(item.getText());
      return convertView;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
      ItemViewHolder holder = null;
      CellCategory item = list.get(position);
      if(convertView == null) {
        holder = new ItemViewHolder();
        convertView = inflator.inflate(R.layout.cell_public_cell_category, null);
        holder.txtPublicCellCategory = convertView.findViewById(R.id.txt_public_cell_category);
        convertView.setTag(holder);
      }
      holder = (ItemViewHolder) convertView.getTag();
      holder.txtPublicCellCategory.setText(item.getText());
      return convertView;
    }
  }
}
