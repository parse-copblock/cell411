package cell411.activity;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Service;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.URLSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentManager;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.maps.DirectionsApi;
import com.google.maps.DirectionsApiRequest;
import com.google.maps.GeoApiContext;
import com.google.maps.model.DirectionsResult;
import com.google.maps.model.DirectionsRoute;
import com.parse.ParseException;
import com.parse.ParseGeoPoint;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParsePush;
import com.parse.ParseQuery;
import com.parse.ParseRelation;
import com.parse.ParseUser;
import com.parse.callback.FindCallback;
import com.parse.callback.GetCallback;
import com.parse.callback.SaveCallback;
import com.safearx.cell411.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import cell411.Singleton;
import cell411.constants.Alert;
import cell411.constants.Constants;
import cell411.constants.Prefs;
import cell411.fragments.LocationFragment;
import cell411.interfaces.CurrentLocationFromLocationUpdatesCallBack;
import cell411.interfaces.LastLocationCallBack;
import cell411.interfaces.LocationFailToRetrieveCallBack;
import cell411.methods.Modules;
import cell411.models.Cell411Alert;
import cell411.models.ChatRoom;
import cell411.models.ReceivedCell411Alert;
import cell411.services.FetchAddressIntentService;
import cell411.utils.LogEvent;
import cell411.widgets.CircularImageView;

import static cell411.Singleton.sendToast;
import static cell411.SingletonConfig.GOOGLE_DIRECTIONS_API_KEY;

/**
 * Created by Sachin on 14-04-2016.
 */
public class AlertActivity extends BaseActivity
  implements View.OnClickListener, OnMapReadyCallback, LastLocationCallBack,
               LocationFailToRetrieveCallBack, CurrentLocationFromLocationUpdatesCallBack {
  private static final long TIME_TO_LIVE = 7200000; // Two Hours
  private final static int PERMISSION_CALL_PHONE = 1;
  public static WeakReference<AlertActivity> weakRef;
  private final String TAG = AlertActivity.class.getSimpleName();
  private RelativeLayout rlAdditionalInfoCollapsed;
  private RelativeLayout rlAdditionalInfoExpanded;
  private ProgressBar pbHelp;
  private ProgressBar pbCannotHelp;
  private TextView txtBtnClose;
  private ImageView imgClose;
  private ReceivedCell411Alert receivedCell411Alert;
  private ParseUser parseUser;
  private TextView txtAddress;
  private String mobileNumber;
  private String emergencyContactNumber;
  private AddressResultReceiver mResultReceiver;
  private boolean isAlertFromFacebook;
  private LocationFragment locationFragment;
  private TextView txtAlert;
  private String number;

  private static String convertEnumToAlertKey(Cell411Alert.AlertType alertType) {
    switch(alertType) {
      case BROKEN_CAR:
        return Alert.BROKEN_CAR;
      case BULLIED:
        return Alert.BULLIED;
      case CRIMINAL:
        return Alert.CRIMINAL;
      case DANGER:
        return Alert.DANGER;
      case FIRE:
        return Alert.FIRE;
      case GENERAL:
        return Alert.GENERAL;
      case MEDICAL:
        return Alert.MEDICAL;
      case PHOTO:
        return Alert.PHOTO;
      case POLICE_ARREST:
        return Alert.POLICE_ARREST;
      case POLICE_INTERACTION:
        return Alert.POLICE_INTERACTION;
      case PULLED_OVER:
        return Alert.PULLED_OVER;
      case VIDEO:
        return Alert.VIDEO;
      case PANIC:
        return Alert.PANIC;
      case HIJACK:
        return Alert.HIJACK;
      case FALLEN:
        return Alert.FALLEN;
      case PHYSICAL_ABUSE:
        return Alert.PHYSICAL_ABUSE;
      case TRAPPED:
        return Alert.TRAPPED;
      case CAR_ACCIDENT:
        return Alert.CAR_ACCIDENT;
      case NATURAL_DISASTER:
        return Alert.NATURAL_DISASTER;
      case PRE_AUTHORISATION:
        return Alert.PRE_AUTHORISATION;
      default:
        return Alert.UNRECOGNIZED;
    }
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    //setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    setContentView(R.layout.activity_alert);
    weakRef = new WeakReference<>(this);
    Singleton.INSTANCE.setCurrentActivity(this);
    Uri targetUrl = null;//AppLinks.getTargetUrlFromInboundIntent(this, getIntent());
    if(targetUrl != null) {
      LogEvent.Log(TAG, "appLinkTargetURL: " + targetUrl.toString());
      String name = targetUrl.getQueryParameter("issuer_name");
      String userId = targetUrl.getQueryParameter("issuer_id");
      double lat = 0;
      if(targetUrl.getQueryParameter("lat") != null) {
        lat = Double.parseDouble(targetUrl.getQueryParameter("lat"));
      }
      double lon = 0;
      if(targetUrl.getQueryParameter("lon") != null) {
        lon = Double.parseDouble(targetUrl.getQueryParameter("lon"));
      }
      long createdAt = 0;
      if(targetUrl.getQueryParameter("created_at") != null) {
        createdAt = Long.parseLong(targetUrl.getQueryParameter("created_at"));
      }
      String cell411AlertId = targetUrl.getQueryParameter("cell_411_alert_id");
      String _additionalNote = targetUrl.getQueryParameter("additional_note");
      int _dispatchMode = 0;
      if(targetUrl.getQueryParameter("dispatch_mode") != null) {
        _dispatchMode = Integer.parseInt(targetUrl.getQueryParameter("dispatch_mode"));
      }
      int _isGlobal = 0;
      if(targetUrl.getQueryParameter("is_global") != null) {
        _isGlobal = Integer.parseInt(targetUrl.getQueryParameter("is_global"));
      }
      String alertRegarding = targetUrl.getQueryParameter("alert_type");
      String forwardedBy = targetUrl.getQueryParameter("forwarded_by");
      String cellName = targetUrl.getQueryParameter("cell_name");
      String cellId = targetUrl.getQueryParameter("cell_id");
      String city = targetUrl.getQueryParameter("city");
      String country = targetUrl.getQueryParameter("country");
      String fullAddress = targetUrl.getQueryParameter("full_address");
      LogEvent.Log(TAG, "name: " + name);
      LogEvent.Log(TAG, "userId: " + userId);
      LogEvent.Log(TAG, "lat: " + lat);
      LogEvent.Log(TAG, "lon: " + lon);
      LogEvent.Log(TAG, "createdAt: " + createdAt);
      LogEvent.Log(TAG, "cell411AlertId: " + cell411AlertId);
      LogEvent.Log(TAG, "_additionalNote: " + _additionalNote);
      LogEvent.Log(TAG, "_dispatchMode: " + _dispatchMode);
      LogEvent.Log(TAG, "_isGlobal: " + _isGlobal);
      LogEvent.Log(TAG, "alertRegarding: " + alertRegarding);
      LogEvent.Log(TAG, "forwardedBy: " + forwardedBy);
      LogEvent.Log(TAG, "cellName: " + cellName);
      LogEvent.Log(TAG, "cellId: " + cellId);
      if(name == null || userId == null || cell411AlertId == null || alertRegarding == null ||
           lat == 0 || lon == 0 || createdAt == 0) {
        showOldAppVersionAlertDialog();
        return;
      }
      String[] nameArr = name.split(" ");
      receivedCell411Alert =
        new ReceivedCell411Alert(nameArr[0], nameArr[1], userId, lat, lon, createdAt, null,
          cell411AlertId, _additionalNote, _dispatchMode, _isGlobal, forwardedBy, cellName,
          cellId, Modules.convertStringToEnum(alertRegarding), null, fullAddress, city,
          country);
      // Need to add the value for forwardingAlertId in the above line when fixing facebook story
      isAlertFromFacebook = true;
    } else {
      receivedCell411Alert =
        (ReceivedCell411Alert) getIntent().getSerializableExtra("receivedCell411Alert");
      updateSeenBy();
    }
    pbHelp = (ProgressBar) findViewById(R.id.pb_help);
    pbCannotHelp = (ProgressBar) findViewById(R.id.pb_cannot_help);
    txtBtnClose = (TextView) findViewById(R.id.txt_btn_close);
    imgClose = (ImageView) findViewById(R.id.img_close);
    imgClose.setOnClickListener(this);
    // Obtain the SupportMapFragment and get notified when the map is ready to be used.
    SupportMapFragment mapFragment =
      (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
    mapFragment.getMapAsync(this);
    locationFragment = new LocationFragment();
    FragmentManager fragmentManager = getSupportFragmentManager();
    fragmentManager.beginTransaction().add(locationFragment, "locationFragment").commit();
  }

  @Override
  protected void onPostCreate(@Nullable Bundle savedInstanceState) {
    super.onPostCreate(savedInstanceState);
    setAlertDetails();
    applyTagIfRequired();
    initializeCollapsedAndExpandedInfoViewAndProfilePic();
    applyAlertTheme();
    if(isAlertFromFacebook) {
      // check if the alert is expired
      double timeElapsed = System.currentTimeMillis() - receivedCell411Alert.createdAt;
      if(timeElapsed > TIME_TO_LIVE) {
        showAlertExpiredDialog();
        LogEvent.Log(TAG, "An alert from " + receivedCell411Alert.issuerFirstName + " is expired");
      } else if(ParseUser.getCurrentUser() != null) {

        if(Singleton.isLoggedIn() &&
             ParseUser.getCurrentUser().getObjectId().equals(receivedCell411Alert.userId)) {
          showSelfAlertDialog();
        }
      }
    }
  }

  private void updateSeenBy() {
    ParseQuery<ParseObject> query = ParseQuery.getQuery("Cell411Alert");
    if(receivedCell411Alert.forwardingAlertId != null) {
      ArrayList<String> arrayList = new ArrayList<>();
      arrayList.add(receivedCell411Alert.cell411AlertId);
      arrayList.add(receivedCell411Alert.forwardingAlertId);
      query.whereContainedIn("objectId", arrayList);
    } else {
      query.whereEqualTo("objectId", receivedCell411Alert.cell411AlertId);
    }
    query.findInBackground(new FindCallback<ParseObject>() {
      @Override
      public void done(List<ParseObject> parseObjects, ParseException e) {
        if(e == null) {
          if(receivedCell411Alert.forwardedBy != null) {
            for(int i = 0; i < 2; i++) {
              final ParseObject cell411AlertObject = parseObjects.get(i);
              if(cell411AlertObject.getObjectId().equals(receivedCell411Alert.cell411AlertId)) {
                ParseRelation relation = cell411AlertObject.getRelation("seenByForwardedMember");
                relation.add(ParseUser.getCurrentUser());
                cell411AlertObject.saveInBackground(new SaveCallback() {
                  @Override
                  public void done(ParseException e) {
                    if(e != null) {
                      cell411AlertObject.saveEventually();
                    }
                  }
                });
              } else {
                ParseRelation relation = cell411AlertObject.getRelation("seenBy");
                relation.add(ParseUser.getCurrentUser());
                cell411AlertObject.saveInBackground(new SaveCallback() {
                  @Override
                  public void done(ParseException e) {
                    if(e != null) {
                      cell411AlertObject.saveEventually();
                    }
                  }
                });
              }
            }
          } else {
            final ParseObject cell411AlertObject = parseObjects.get(0);
            ParseRelation relation = cell411AlertObject.getRelation("seenBy");
            relation.add(ParseUser.getCurrentUser());
            cell411AlertObject.saveInBackground(new SaveCallback() {
              @Override
              public void done(ParseException e) {
                if(e != null) {
                  cell411AlertObject.saveEventually();
                }
              }
            });
          }
        }
      }
    });
  }

  private void showAlertExpiredDialog() {
    AlertDialog.Builder alert = new AlertDialog.Builder(this);
    alert.setMessage(R.string.dialog_message_alert_expired);
    alert.setOnCancelListener(new DialogInterface.OnCancelListener() {
      @Override
      public void onCancel(DialogInterface dialog) {
        finish();
      }
    });
    alert.setPositiveButton(R.string.dialog_btn_ok, new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialog, int which) {
        finish();
      }
    });
    AlertDialog dialog = alert.create();
    dialog.show();
  }

  private void showSelfAlertDialog() {
    AlertDialog.Builder alert = new AlertDialog.Builder(this);
    alert.setMessage(R.string.dialog_message_response_not_allowed);
    alert.setOnCancelListener(new DialogInterface.OnCancelListener() {
      @Override
      public void onCancel(DialogInterface dialog) {
        finish();
      }
    });
    alert.setPositiveButton(R.string.dialog_btn_ok, new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialog, int which) {
        finish();
      }
    });
    AlertDialog dialog = alert.create();
    dialog.show();
  }

  private void showOldAppVersionAlertDialog() {
    AlertDialog.Builder alert = new AlertDialog.Builder(this);
    alert.setMessage(R.string.dialog_message_latest_version_required);
    alert.setOnCancelListener(new DialogInterface.OnCancelListener() {
      @Override
      public void onCancel(DialogInterface dialog) {
        finish();
      }
    });
    alert.setPositiveButton(R.string.dialog_btn_ok, new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialog, int which) {
        finish();
      }
    });
    AlertDialog dialog = alert.create();
    dialog.show();
  }

  private void applyTagIfRequired() {
    TextView txtLblTag = (TextView) findViewById(R.id.txt_lbl_tag);
    RelativeLayout rlForwardedBy = (RelativeLayout) findViewById(R.id.rl_forwarded_by);
    TextView txtLblForwardedBy = (TextView) findViewById(R.id.txt_lbl_forwarded_by);
    TextView txtForwardedBy = (TextView) findViewById(R.id.txt_forwarded_by);
    if(receivedCell411Alert._isGlobal == 1 && receivedCell411Alert.forwardedBy != null) {
      txtLblTag.setText(
        getString(R.string.tag_global_alert) + "\n" + getString(R.string.tag_forwarded_alert));
      txtForwardedBy.setText(receivedCell411Alert.forwardedBy);
    } else if(receivedCell411Alert._isGlobal == 1) {
      txtLblTag.setText(R.string.tag_global_alert);
      rlForwardedBy.setVisibility(View.GONE);
    } else if(receivedCell411Alert.forwardedBy != null) {
      txtLblTag.setText(R.string.tag_forwarded_alert);
      txtForwardedBy.setText(receivedCell411Alert.forwardedBy);
    } else if(receivedCell411Alert.cellName != null) {
      txtLblTag.setVisibility(View.GONE);
      txtLblForwardedBy.setText(R.string.alert_on_public_cell);
      txtForwardedBy.setText(receivedCell411Alert.cellName);
    } else {
      txtLblTag.setVisibility(View.GONE);
      rlForwardedBy.setVisibility(View.GONE);
    }
  }

  private void setAlertDetails() {
    txtAlert = (TextView) findViewById(R.id.txt_alert);
    TextView txtAlertTime = (TextView) findViewById(R.id.txt_alert_time);
    //TextView txtDistance = (TextView) findViewById(R.id.txt_distance);
    String description = null;
    if(receivedCell411Alert._dispatchMode == 1) {
      description = getString(R.string.alert_message_dispatched,
        receivedCell411Alert.issuerFirstName + " " + receivedCell411Alert.issuerLastName,
        convertEnumToString(receivedCell411Alert.alertType));
            /*description = "<b>" + receivedCell411Alert.issuerFirstName + " "
                    + receivedCell411Alert.issuerLastName + "</b> " + getString(R.string.dispatched) + " <b>"
                    + convertEnumToString(receivedCell411Alert.alertType) + "</b> "
                    + ALERT_NOTIFICATION_TRAIL;*/
    } else {
      description = getString(R.string.alert_message,
        receivedCell411Alert.issuerFirstName + " " + receivedCell411Alert.issuerLastName,
        convertEnumToString(receivedCell411Alert.alertType));
            /*description = "<b>" + receivedCell411Alert.issuerFirstName + " "
                    + receivedCell411Alert.issuerLastName + "</b> " + getString(R.string.issued_a) + " <b>"
                    + convertEnumToString(receivedCell411Alert.alertType) + "</b> "
                    + ALERT_NOTIFICATION_TRAIL;*/
    }
    txtAlert.setText(Html.fromHtml(description));
    txtAlertTime.setText(getFormattedTime());
    //txtDistance.setText(getFormattedDistance());
    locationFragment.getLastLocation(this, this, Feature.CALCULATE_DISTANCE);
    txtAddress = (TextView) findViewById(R.id.txt_address);
    if(receivedCell411Alert.city != null && !receivedCell411Alert.city.equals("")) {
      txtAddress.setText(receivedCell411Alert.city);
    } else {
      // Initialize the receiver and start the service for reverse geo coded address
      mResultReceiver = new AddressResultReceiver(new Handler());
      startIntentService();
    }
  }

  protected void setTextViewHTML() {
    String description = null;
    if(receivedCell411Alert._dispatchMode == 1) {
      description = getString(R.string.alert_message_dispatched,
        "<a href='profile'>" + receivedCell411Alert.issuerFirstName + " " +
          receivedCell411Alert.issuerLastName + "</a>",
        convertEnumToString(receivedCell411Alert.alertType));
            /*description = "<b><a href='profile'>" + receivedCell411Alert.issuerFirstName + " "
                    + receivedCell411Alert.issuerLastName + "</a></b> " + getString(R.string.dispatched) + " <b>"
                    + convertEnumToString(receivedCell411Alert.alertType) + "</b> "
                    + ALERT_NOTIFICATION_TRAIL;*/
    } else {
      description = getString(R.string.alert_message,
        "<a href='profile'>" + receivedCell411Alert.issuerFirstName + " " +
          receivedCell411Alert.issuerLastName + "</a>",
        convertEnumToString(receivedCell411Alert.alertType));
            /*description = "<b><a href='profile'>" + receivedCell411Alert.issuerFirstName + " "
                    + receivedCell411Alert.issuerLastName + "</a></b> " + getString(R.string.issued_a) + " <b>"
                    + convertEnumToString(receivedCell411Alert.alertType) + "</b> "
                    + ALERT_NOTIFICATION_TRAIL;*/
    }
    CharSequence sequence = Html.fromHtml(description);
    SpannableStringBuilder strBuilder = new SpannableStringBuilder(sequence);
    URLSpan[] urls = strBuilder.getSpans(0, sequence.length(), URLSpan.class);
    for(URLSpan span : urls) {
      makeLinkClickable(strBuilder, span);
    }
    txtAlert.setText(strBuilder);
    txtAlert.setMovementMethod(LinkMovementMethod.getInstance());
  }

  protected void makeLinkClickable(SpannableStringBuilder strBuilder, final URLSpan span) {
    int start = strBuilder.getSpanStart(span);
    int end = strBuilder.getSpanEnd(span);
    int flags = strBuilder.getSpanFlags(span);
    ClickableSpan clickable = new ClickableSpan() {
      public void onClick(View view) {
        Intent intentUser = new Intent(AlertActivity.this, UserActivity.class);
        intentUser.putExtra("userId", parseUser.getObjectId());
        intentUser.putExtra("imageName", "" + parseUser.get("imageName"));
        intentUser.putExtra("firstName", "" + parseUser.get("firstName"));
        intentUser.putExtra("lastName", "" + parseUser.get("lastName"));
        intentUser.putExtra("username", parseUser.getUsername());
        intentUser.putExtra("email", parseUser.getEmail());
        if(parseUser.getParseGeoPoint("location") != null) {
          intentUser.putExtra("lat", parseUser.getParseGeoPoint("location").getLatitude());
          intentUser.putExtra("lng", parseUser.getParseGeoPoint("location").getLongitude());
        }
        startActivity(intentUser);
      }
    };
    strBuilder.setSpan(clickable, start, end, flags);
    strBuilder.removeSpan(span);
  }

  private String getFormattedTime() {
    GregorianCalendar cal = new GregorianCalendar();
    GregorianCalendar calCurrentTime = new GregorianCalendar();
    calCurrentTime.setTimeInMillis(System.currentTimeMillis());
    calCurrentTime.set(Calendar.HOUR, 0);
    calCurrentTime.set(Calendar.MINUTE, 0);
    calCurrentTime.set(Calendar.SECOND, 0);
    calCurrentTime.set(Calendar.MILLISECOND, 0);
    cal.setTimeInMillis(receivedCell411Alert.createdAt);
    int year = cal.get(Calendar.YEAR);
    int month = cal.get(Calendar.MONTH) + 1;
    int day = cal.get(Calendar.DAY_OF_MONTH);
    int hour = cal.get(Calendar.HOUR);
    int minute = cal.get(Calendar.MINUTE);
    int ampm = cal.get(Calendar.AM_PM);
    String ap;
    if(ampm == 0) {
      ap = getString(R.string.am);
    } else {
      ap = getString(R.string.pm);
    }
    if(hour == 0) {
      hour = 12;
    }
    String hourMinute = "" + hour;
    if(minute > 0 && minute < 10) {
      hourMinute += ":0" + minute;
    } else if(minute > 9) {
      hourMinute += ":" + minute;
    }
    String time;
    if(cal.getTimeInMillis() < calCurrentTime.getTimeInMillis()) {
      // Check if the alert was issued yesterday
      time =
        month + "/" + day + "/" + year + " " + getString(R.string.at) + " " + hourMinute + " " +
          ap;
    } else {
      time = hourMinute + " " + ap;
    }
    return time;
  }

  private String getFormattedDistance() {
    ParseGeoPoint origin =
      new ParseGeoPoint(Singleton.INSTANCE.getLatitude(),
        Singleton.INSTANCE.getLongitude());
    ParseGeoPoint destination =
      new ParseGeoPoint(receivedCell411Alert.lat, receivedCell411Alert.lon);
    double distance;
    final String metric = Singleton.INSTANCE.getAppPrefs().getString("metric", "kms");
    if(metric.equals("kms")) {
      distance = origin.distanceInKilometersTo(destination);
    } else {
      distance = origin.distanceInMilesTo(destination);
    }
    DecimalFormat df = new DecimalFormat("0.0");
    String distanceString = df.format(distance);
    if(distance > 1) {
      if(metric.equals("kms")) {
        distanceString += " " + getString(R.string.kilometers_away);
      } else {
        distanceString += " " + getString(R.string.miles_away);
      }
    } else {
      if(metric.equals("kms")) {
        distanceString += " " + getString(R.string.kilometers_away);
      } else {
        distanceString += " " + getString(R.string.miles_away);
      }
    }
    return distanceString;
  }

  private void initializeActionButtons() {
    FloatingActionButton fabChat = (FloatingActionButton) findViewById(R.id.fab_chat);
    FloatingActionButton fabNavigate = (FloatingActionButton) findViewById(R.id.fab_navigate);
    FloatingActionButton fabPhone = (FloatingActionButton) findViewById(R.id.fab_phone);
    if(getResources().getBoolean(R.bool.is_chat_enabled)) {
      fabChat.setOnClickListener(this);
    } else {
      fabChat.hide();
    }
    fabNavigate.setOnClickListener(this);
    if(mobileNumber != null && !mobileNumber.isEmpty()) {
      fabPhone.setOnClickListener(this);
    } else {
      fabPhone.hide();
    }
    RelativeLayout rlBtnCallEmergencyContact =
      (RelativeLayout) findViewById(R.id.rl_btn_call_emergency_contact);
    RelativeLayout rlBtnForwardAlert = (RelativeLayout) findViewById(R.id.rl_btn_forward_alert);
    if(receivedCell411Alert.forwardedBy == null && receivedCell411Alert.cellName == null &&
         !isAlertFromFacebook) {
      // Support alert forwarding feature to only one level, i.e.,
      // 1. if the forwardedBy var contains some name then it means alert is forwarded
      // by some user to the current user or,
      // 2. if the cellName var contains some name then it means alert is issued
      // to some Public newPrivateCell
      rlBtnForwardAlert.setOnClickListener(this);
    } else {
      rlBtnForwardAlert.setVisibility(View.GONE);
    }
    if(emergencyContactNumber != null && !emergencyContactNumber.isEmpty()) {
      rlBtnCallEmergencyContact.setOnClickListener(this);
    } else {
      rlBtnCallEmergencyContact.setVisibility(View.GONE);
    }
    TextView txtBtnCannotHelp = (TextView) findViewById(R.id.txt_btn_cannot_help);
    TextView txtBtnHelp = (TextView) findViewById(R.id.txt_btn_help);
    if(receivedCell411Alert.alertType.equals(Cell411Alert.AlertType.GENERAL)) {
      txtBtnCannotHelp.setText(R.string.reject);
      txtBtnHelp.setText(R.string.accept);
    }
    txtBtnCannotHelp.setOnClickListener(this);
    txtBtnHelp.setOnClickListener(this);
    txtBtnClose.setOnClickListener(this);
  }

  private void initializeCollapsedAndExpandedInfoViewAndProfilePic() {
    // For collapsed additional info window
    rlAdditionalInfoCollapsed = (RelativeLayout) findViewById(R.id.rl_additional_info_collapsed);
    TextView txtLblAdditionalNote = (TextView) findViewById(R.id.txt_lbl_additional_note);
    TextView txtAdditionalNote = (TextView) findViewById(R.id.txt_additional_note);
    TextView txtBtnExpand = (TextView) findViewById(R.id.txt_btn_expand);
    txtBtnExpand.setOnClickListener(this);
    // For expanded additional info window
    rlAdditionalInfoExpanded = (RelativeLayout) findViewById(R.id.rl_additional_info_expanded);
    TextView txtLblAdditionalNote2 = (TextView) findViewById(R.id.txt_lbl_additional_note2);
    TextView txtAdditionalNote2 = (TextView) findViewById(R.id.txt_additional_note2);
    TextView txtBtnCollapse = (TextView) findViewById(R.id.txt_btn_collapse);
    txtBtnCollapse.setOnClickListener(this);
    if(receivedCell411Alert._additionalNote != null &&
         !receivedCell411Alert._additionalNote.equals("")) {
      txtAdditionalNote.setText(receivedCell411Alert._additionalNote);
      txtAdditionalNote2.setText(receivedCell411Alert._additionalNote);
    } else {
      txtLblAdditionalNote.setVisibility(View.GONE);
      txtAdditionalNote.setVisibility(View.GONE);
      txtAdditionalNote2.setVisibility(View.GONE);
      txtLblAdditionalNote2.setVisibility(View.GONE);
    }
    if(receivedCell411Alert.alertType == Cell411Alert.AlertType.MEDICAL) {
      retrieveMedicalInfoAndProfilePicAndContactInfo();
    } else {
      // Hide the collapsed and expanded view as we don't have additional info to be displayed
      if(receivedCell411Alert._additionalNote == null ||
           receivedCell411Alert._additionalNote.equals("")) {
        rlAdditionalInfoCollapsed.setVisibility(View.GONE);
        rlAdditionalInfoExpanded.setVisibility(View.GONE);
      }
      retrieveProfilePicAndContactInfo();
    }
  }

  private void retrieveMedicalInfoAndProfilePicAndContactInfo() {
    final CircularImageView imgUser = (CircularImageView) findViewById(R.id.img_user);
    final TextView txtLblBloodGroup = (TextView) findViewById(R.id.txt_lbl_blood_group);
    final TextView txtBloodGroup = (TextView) findViewById(R.id.txt_blood_group);
    final TextView txtLblAllergies = (TextView) findViewById(R.id.txt_lbl_allergies);
    final TextView txtAllergies = (TextView) findViewById(R.id.txt_allergies);
    final TextView txtLblOtherMedicalConditions =
      (TextView) findViewById(R.id.txt_lbl_other_medical_conditions);
    final TextView txtOtherMedicalConditions =
      (TextView) findViewById(R.id.txt_other_medical_conditions);
    final TextView txtLblBloodGroup2 = (TextView) findViewById(R.id.txt_lbl_blood_group2);
    final TextView txtBloodGroup2 = (TextView) findViewById(R.id.txt_blood_group2);
    final TextView txtLblAllergies2 = (TextView) findViewById(R.id.txt_lbl_allergies2);
    final TextView txtAllergies2 = (TextView) findViewById(R.id.txt_allergies2);
    final TextView txtLblOtherMedicalConditions2 =
      (TextView) findViewById(R.id.txt_lbl_other_medical_conditions2);
    final TextView txtOtherMedicalConditions2 =
      (TextView) findViewById(R.id.txt_other_medical_conditions2);
    txtLblBloodGroup.setVisibility(View.VISIBLE);
    txtBloodGroup.setVisibility(View.VISIBLE);
    txtLblAllergies.setVisibility(View.VISIBLE);
    txtAllergies.setVisibility(View.VISIBLE);
    txtLblOtherMedicalConditions.setVisibility(View.VISIBLE);
    txtOtherMedicalConditions.setVisibility(View.VISIBLE);
    txtLblBloodGroup2.setVisibility(View.VISIBLE);
    txtBloodGroup2.setVisibility(View.VISIBLE);
    txtLblAllergies2.setVisibility(View.VISIBLE);
    txtAllergies2.setVisibility(View.VISIBLE);
    txtLblOtherMedicalConditions2.setVisibility(View.VISIBLE);
    txtOtherMedicalConditions2.setVisibility(View.VISIBLE);
    ParseQuery<ParseUser> parseQuery = ParseUser.getQuery();
    parseQuery.whereEqualTo("objectId", receivedCell411Alert.userId);
    parseQuery.getFirstInBackground(new GetCallback<ParseUser>() {
      @Override
      public void done(final ParseUser parseUser, ParseException e) {
        if(e == null) {
          AlertActivity.this.parseUser = parseUser;
          String bloodType = (String) parseUser.get("bloodType");
          String allergies = (String) parseUser.get("allergies");
          String otherMedicalConditions = (String) parseUser.get("otherMedicalConditions");
          if(bloodType != null && !bloodType.isEmpty()) {
            txtBloodGroup.setText(bloodType);
            txtBloodGroup2.setText(bloodType);
          } else {
            txtBloodGroup.setText(R.string.not_applicable);
            txtBloodGroup2.setText(R.string.not_applicable);
          }
          if(allergies != null && !allergies.isEmpty()) {
            txtAllergies.setText(bloodType);
            txtAllergies2.setText(bloodType);
          } else {
            txtAllergies.setText(R.string.not_applicable);
            txtAllergies2.setText(R.string.not_applicable);
          }
          if(otherMedicalConditions != null && !otherMedicalConditions.isEmpty()) {
            txtOtherMedicalConditions.setText(bloodType);
            txtOtherMedicalConditions2.setText(bloodType);
          } else {
            txtOtherMedicalConditions.setText(R.string.not_applicable);
            txtOtherMedicalConditions2.setText(R.string.not_applicable);
          }
          String email = null;
          if(parseUser.getEmail() != null && !parseUser.getEmail().isEmpty()) {
            email = (String) parseUser.getEmail();
          } else {
            email = parseUser.getUsername();
          }
          final String finalEmail = email;
          Singleton.INSTANCE
            .setImage(imgUser, parseUser.getObjectId() + parseUser.get("imageName"),
              email);
          imgUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              Intent profileImageIntent =
                new Intent(AlertActivity.this, ProfileImageActivity.class);
              profileImageIntent.putExtra("userId", parseUser.getObjectId());
              profileImageIntent.putExtra("imageName", "" + parseUser.get("imageName"));
              profileImageIntent.putExtra("email", finalEmail);
              startActivity(profileImageIntent);
            }
          });
          setTextViewHTML();
          mobileNumber = (String) parseUser.get("mobileNumber");
          emergencyContactNumber = (String) parseUser.get("emergencyContactNumber");
          initializeActionButtons();
        }
      }
    });
  }

  private void retrieveProfilePicAndContactInfo() {
    final CircularImageView imgUser = (CircularImageView) findViewById(R.id.img_user);
    ParseQuery<ParseUser> parseQuery = ParseUser.getQuery();
    parseQuery.whereEqualTo("objectId", receivedCell411Alert.userId);
    parseQuery.getFirstInBackground(new GetCallback<ParseUser>() {
      @Override
      public void done(final ParseUser parseUser, ParseException e) {
        if(e == null) {
          AlertActivity.this.parseUser = parseUser;
          String email = null;
          if(parseUser.getEmail() != null && !parseUser.getEmail().isEmpty()) {
            email = (String) parseUser.getEmail();
          } else {
            email = parseUser.getUsername();
          }
          final String finalEmail = email;
          Singleton.INSTANCE
            .setImage(imgUser, parseUser.getObjectId() + parseUser.get("imageName"),
              email);
          imgUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              Intent profileImageIntent =
                new Intent(AlertActivity.this, ProfileImageActivity.class);
              profileImageIntent.putExtra("userId", parseUser.getObjectId());
              profileImageIntent.putExtra("imageName", "" + parseUser.get("imageName"));
              profileImageIntent.putExtra("email", finalEmail);
              startActivity(profileImageIntent);
            }
          });
          setTextViewHTML();
          mobileNumber = (String) parseUser.get("mobileNumber");
          emergencyContactNumber = (String) parseUser.get("emergencyContactNumber");
          initializeActionButtons();
        }
      }
    });
  }

  private String convertEnumToString(Cell411Alert.AlertType alertType) {
    switch(alertType) {
      case BROKEN_CAR:
        return getString(R.string.alert_broken_car);
      case BULLIED:
        return getString(R.string.alert_bullied);
      case CRIMINAL:
        return getString(R.string.alert_criminal);
      case DANGER:
        return getString(R.string.alert_danger);
      case FIRE:
        return getString(R.string.alert_fire);
      case GENERAL:
        return getString(R.string.alert_general);
      case MEDICAL:
        return getString(R.string.alert_medical);
      case PHOTO:
        return getString(R.string.alert_photo);
      case POLICE_ARREST:
        return getString(R.string.alert_police_arrest);
      case POLICE_INTERACTION:
        return getString(R.string.alert_police_interaction);
      case PULLED_OVER:
        return getString(R.string.alert_pulled_over);
      case VIDEO:
        return getString(R.string.alert_video);
      case HIJACK:
        return getString(R.string.alert_hijack);
      case PANIC:
        return getString(R.string.alert_panic);
      case FALLEN:
        return getString(R.string.alert_fallen);
      case PHYSICAL_ABUSE:
        return getString(R.string.alert_physical_abuse);
      case TRAPPED:
        return getString(R.string.alert_trapped);
      case CAR_ACCIDENT:
        return getString(R.string.alert_car_accident);
      case NATURAL_DISASTER:
        return getString(R.string.alert_natural_disaster);
      case PRE_AUTHORISATION:
        return getString(R.string.alert_pre_authorisation);
      default:
        return getString(R.string.alert_un_recognized);
    }
  }

  private void applyAlertTheme() {
    RelativeLayout rlAlertContainer = (RelativeLayout) findViewById(R.id.rl_alert_container);
    ImageView imgAlertType = (ImageView) findViewById(R.id.img_alert_type);
    rlAlertContainer.setVisibility(View.VISIBLE);
    imgAlertType.setVisibility(View.VISIBLE);
    RelativeLayout rlAddress = (RelativeLayout) findViewById(R.id.rl_address);
    View viewSeparatorHorizontal = findViewById(R.id.view_separator_vertical);
    LinearLayout llAction = (LinearLayout) findViewById(R.id.ll_action);
    View viewSeparatorVertical = findViewById(R.id.view_separator_vertical);
    switch(receivedCell411Alert.alertType) {
      case FIRE:
        rlAlertContainer.setBackgroundResource(R.drawable.bg_alert_fire);
        imgAlertType.setImageResource(R.drawable.alert_head_fire);
        rlAdditionalInfoCollapsed.setBackgroundColor(
          getResources().getColor(R.color.alert_fire_dark));
        rlAdditionalInfoExpanded.setBackgroundColor(
          getResources().getColor(R.color.alert_fire_dark));
        rlAddress.setBackgroundColor(getResources().getColor(R.color.alert_fire_dark));
        llAction.setBackgroundResource(R.drawable.bg_alert_fire_action);
        txtBtnClose.setBackgroundResource(R.drawable.bg_alert_fire_action);
        viewSeparatorVertical.setBackgroundColor(getResources().getColor(R.color.alert_fire));
        viewSeparatorHorizontal.setBackgroundColor(getResources().getColor(R.color.alert_fire));
        break;
      case VIDEO:
        rlAlertContainer.setBackgroundResource(R.drawable.bg_alert_video);
        imgAlertType.setImageResource(R.drawable.alert_head_video);
        rlAdditionalInfoCollapsed.setBackgroundColor(
          getResources().getColor(R.color.alert_video_dark));
        rlAdditionalInfoExpanded.setBackgroundColor(
          getResources().getColor(R.color.alert_video_dark));
        rlAddress.setBackgroundColor(getResources().getColor(R.color.alert_video_dark));
        llAction.setBackgroundResource(R.drawable.bg_alert_video_action);
        txtBtnClose.setBackgroundResource(R.drawable.bg_alert_video_action);
        viewSeparatorVertical.setBackgroundColor(getResources().getColor(R.color.alert_video));
        viewSeparatorHorizontal.setBackgroundColor(getResources().getColor(R.color.alert_video));
        break;
      case DANGER:
        rlAlertContainer.setBackgroundResource(R.drawable.bg_alert_danger);
        imgAlertType.setImageResource(R.drawable.alert_head_danger);
        rlAdditionalInfoCollapsed.setBackgroundColor(
          getResources().getColor(R.color.alert_danger_dark));
        rlAdditionalInfoExpanded.setBackgroundColor(
          getResources().getColor(R.color.alert_danger_dark));
        rlAddress.setBackgroundColor(getResources().getColor(R.color.alert_danger_dark));
        llAction.setBackgroundResource(R.drawable.bg_alert_danger_action);
        txtBtnClose.setBackgroundResource(R.drawable.bg_alert_danger_action);
        viewSeparatorVertical.setBackgroundColor(getResources().getColor(R.color.alert_danger));
        viewSeparatorHorizontal.setBackgroundColor(getResources().getColor(R.color.alert_danger));
        break;
      case PULLED_OVER:
        rlAlertContainer.setBackgroundResource(R.drawable.bg_alert_pulled_over);
        imgAlertType.setImageResource(R.drawable.alert_head_pulled_over);
        rlAdditionalInfoCollapsed.setBackgroundColor(
          getResources().getColor(R.color.alert_pulled_over_dark));
        rlAdditionalInfoExpanded.setBackgroundColor(
          getResources().getColor(R.color.alert_pulled_over_dark));
        rlAddress.setBackgroundColor(getResources().getColor(R.color.alert_pulled_over_dark));
        llAction.setBackgroundResource(R.drawable.bg_alert_pulled_over_action);
        txtBtnClose.setBackgroundResource(R.drawable.bg_alert_pulled_over_action);
        viewSeparatorVertical.setBackgroundColor(
          getResources().getColor(R.color.alert_pulled_over));
        viewSeparatorHorizontal.setBackgroundColor(
          getResources().getColor(R.color.alert_pulled_over));
        break;
      case POLICE_INTERACTION:
        rlAlertContainer.setBackgroundResource(R.drawable.bg_alert_police_interaction);
        imgAlertType.setImageResource(R.drawable.alert_head_police_interaction);
        rlAdditionalInfoCollapsed.setBackgroundColor(
          getResources().getColor(R.color.alert_police_interaction_dark));
        rlAdditionalInfoExpanded.setBackgroundColor(
          getResources().getColor(R.color.alert_police_interaction_dark));
        rlAddress.setBackgroundColor(
          getResources().getColor(R.color.alert_police_interaction_dark));
        llAction.setBackgroundResource(R.drawable.bg_alert_police_interaction_action);
        txtBtnClose.setBackgroundResource(R.drawable.bg_alert_police_interaction_action);
        viewSeparatorVertical.setBackgroundColor(
          getResources().getColor(R.color.alert_police_interaction));
        viewSeparatorHorizontal.setBackgroundColor(
          getResources().getColor(R.color.alert_police_interaction));
        break;
      case BROKEN_CAR:
        rlAlertContainer.setBackgroundResource(R.drawable.bg_alert_broken_car);
        imgAlertType.setImageResource(R.drawable.alert_head_broken_car);
        rlAdditionalInfoCollapsed.setBackgroundColor(
          getResources().getColor(R.color.alert_broken_car_dark));
        rlAdditionalInfoExpanded.setBackgroundColor(
          getResources().getColor(R.color.alert_broken_car_dark));
        rlAddress.setBackgroundColor(getResources().getColor(R.color.alert_broken_car_dark));
        llAction.setBackgroundResource(R.drawable.bg_alert_broken_car_action);
        txtBtnClose.setBackgroundResource(R.drawable.bg_alert_broken_car_action);
        viewSeparatorVertical.setBackgroundColor(getResources().getColor(R.color.alert_broken_car));
        viewSeparatorHorizontal.setBackgroundColor(
          getResources().getColor(R.color.alert_broken_car));
        break;
      case BULLIED:
        rlAlertContainer.setBackgroundResource(R.drawable.bg_alert_bullied);
        imgAlertType.setImageResource(R.drawable.alert_head_bullied);
        rlAdditionalInfoCollapsed.setBackgroundColor(
          getResources().getColor(R.color.alert_bullied_dark));
        rlAdditionalInfoExpanded.setBackgroundColor(
          getResources().getColor(R.color.alert_bullied_dark));
        rlAddress.setBackgroundColor(getResources().getColor(R.color.alert_bullied_dark));
        llAction.setBackgroundResource(R.drawable.bg_alert_bullied_action);
        txtBtnClose.setBackgroundResource(R.drawable.bg_alert_bullied_action);
        viewSeparatorVertical.setBackgroundColor(getResources().getColor(R.color.alert_bullied));
        viewSeparatorHorizontal.setBackgroundColor(getResources().getColor(R.color.alert_bullied));
        break;
      case CRIMINAL:
        rlAlertContainer.setBackgroundResource(R.drawable.bg_alert_criminal);
        imgAlertType.setImageResource(R.drawable.alert_head_criminal);
        rlAdditionalInfoCollapsed.setBackgroundColor(
          getResources().getColor(R.color.alert_criminal_dark));
        rlAdditionalInfoExpanded.setBackgroundColor(
          getResources().getColor(R.color.alert_criminal_dark));
        rlAddress.setBackgroundColor(getResources().getColor(R.color.alert_criminal_dark));
        llAction.setBackgroundResource(R.drawable.bg_alert_criminal_action);
        txtBtnClose.setBackgroundResource(R.drawable.bg_alert_criminal_action);
        viewSeparatorVertical.setBackgroundColor(getResources().getColor(R.color.alert_criminal));
        viewSeparatorHorizontal.setBackgroundColor(getResources().getColor(R.color.alert_criminal));
        break;
      case GENERAL:
        rlAlertContainer.setBackgroundResource(R.drawable.bg_alert_general);
        imgAlertType.setImageResource(R.drawable.alert_head_general);
        rlAdditionalInfoCollapsed.setBackgroundColor(
          getResources().getColor(R.color.alert_general_dark));
        rlAdditionalInfoExpanded.setBackgroundColor(
          getResources().getColor(R.color.alert_general_dark));
        rlAddress.setBackgroundColor(getResources().getColor(R.color.alert_general_dark));
        llAction.setBackgroundResource(R.drawable.bg_alert_general_action);
        txtBtnClose.setBackgroundResource(R.drawable.bg_alert_general_action);
        viewSeparatorVertical.setBackgroundColor(getResources().getColor(R.color.alert_general));
        viewSeparatorHorizontal.setBackgroundColor(getResources().getColor(R.color.alert_general));
        break;
      case MEDICAL:
        rlAlertContainer.setBackgroundResource(R.drawable.bg_alert_medical);
        imgAlertType.setImageResource(R.drawable.alert_head_medical);
        rlAdditionalInfoCollapsed.setBackgroundColor(
          getResources().getColor(R.color.alert_medical_dark));
        rlAdditionalInfoExpanded.setBackgroundColor(
          getResources().getColor(R.color.alert_medical_dark));
        rlAddress.setBackgroundColor(getResources().getColor(R.color.alert_medical_dark));
        llAction.setBackgroundResource(R.drawable.bg_alert_medical_action);
        txtBtnClose.setBackgroundResource(R.drawable.bg_alert_medical_action);
        viewSeparatorVertical.setBackgroundColor(getResources().getColor(R.color.alert_medical));
        viewSeparatorHorizontal.setBackgroundColor(getResources().getColor(R.color.alert_medical));
        break;
      case PHOTO:
        rlAlertContainer.setBackgroundResource(R.drawable.bg_alert_photo);
        imgAlertType.setImageResource(R.drawable.alert_head_photo);
        rlAdditionalInfoCollapsed.setBackgroundColor(
          getResources().getColor(R.color.alert_photo_dark));
        rlAdditionalInfoExpanded.setBackgroundColor(
          getResources().getColor(R.color.alert_photo_dark));
        rlAddress.setBackgroundColor(getResources().getColor(R.color.alert_photo_dark));
        llAction.setBackgroundResource(R.drawable.bg_alert_photo_action);
        txtBtnClose.setBackgroundResource(R.drawable.bg_alert_photo_action);
        viewSeparatorVertical.setBackgroundColor(getResources().getColor(R.color.alert_photo));
        viewSeparatorHorizontal.setBackgroundColor(getResources().getColor(R.color.alert_photo));
        break;
      case POLICE_ARREST:
        rlAlertContainer.setBackgroundResource(R.drawable.bg_alert_police_arrest);
        imgAlertType.setImageResource(R.drawable.alert_head_police_arrest);
        rlAdditionalInfoCollapsed.setBackgroundColor(
          getResources().getColor(R.color.alert_police_arrest_dark));
        rlAdditionalInfoExpanded.setBackgroundColor(
          getResources().getColor(R.color.alert_police_arrest_dark));
        rlAddress.setBackgroundColor(getResources().getColor(R.color.alert_police_arrest_dark));
        llAction.setBackgroundResource(R.drawable.bg_alert_police_arrest_action);
        txtBtnClose.setBackgroundResource(R.drawable.bg_alert_police_arrest_action);
        viewSeparatorVertical.setBackgroundColor(
          getResources().getColor(R.color.alert_police_arrest));
        viewSeparatorHorizontal.setBackgroundColor(
          getResources().getColor(R.color.alert_police_arrest));
        break;
      case HIJACK:
        rlAlertContainer.setBackgroundResource(R.drawable.bg_alert_hijack);
        imgAlertType.setImageResource(R.drawable.alert_head_hijack);
        rlAdditionalInfoCollapsed.setBackgroundColor(
          getResources().getColor(R.color.alert_hijack_dark));
        rlAdditionalInfoExpanded.setBackgroundColor(
          getResources().getColor(R.color.alert_hijack_dark));
        rlAddress.setBackgroundColor(getResources().getColor(R.color.alert_hijack_dark));
        llAction.setBackgroundResource(R.drawable.bg_alert_hijack_action);
        txtBtnClose.setBackgroundResource(R.drawable.bg_alert_hijack_action);
        viewSeparatorVertical.setBackgroundColor(getResources().getColor(R.color.alert_hijack));
        viewSeparatorHorizontal.setBackgroundColor(getResources().getColor(R.color.alert_hijack));
        break;
      case PANIC:
        rlAlertContainer.setBackgroundResource(R.drawable.bg_alert_panic);
        imgAlertType.setImageResource(R.drawable.alert_head_panic);
        rlAdditionalInfoCollapsed.setBackgroundColor(
          getResources().getColor(R.color.alert_panic_dark));
        rlAdditionalInfoExpanded.setBackgroundColor(
          getResources().getColor(R.color.alert_panic_dark));
        rlAddress.setBackgroundColor(getResources().getColor(R.color.alert_panic_dark));
        llAction.setBackgroundResource(R.drawable.bg_alert_panic_action);
        txtBtnClose.setBackgroundResource(R.drawable.bg_alert_panic_action);
        viewSeparatorVertical.setBackgroundColor(getResources().getColor(R.color.alert_panic));
        viewSeparatorHorizontal.setBackgroundColor(getResources().getColor(R.color.alert_panic));
        break;
      case FALLEN:
        rlAlertContainer.setBackgroundResource(R.drawable.bg_alert_fallen);
        imgAlertType.setImageResource(R.drawable.alert_head_fallen);
        rlAdditionalInfoCollapsed.setBackgroundColor(
          getResources().getColor(R.color.alert_fallen_dark));
        rlAdditionalInfoExpanded.setBackgroundColor(
          getResources().getColor(R.color.alert_fallen_dark));
        rlAddress.setBackgroundColor(getResources().getColor(R.color.alert_fallen_dark));
        llAction.setBackgroundResource(R.drawable.bg_alert_fallen_action);
        txtBtnClose.setBackgroundResource(R.drawable.bg_alert_fallen_action);
        viewSeparatorVertical.setBackgroundColor(getResources().getColor(R.color.alert_fallen));
        viewSeparatorHorizontal.setBackgroundColor(getResources().getColor(R.color.alert_fallen));
        break;
      case PHYSICAL_ABUSE:
        rlAlertContainer.setBackgroundResource(R.drawable.bg_alert_physical_abuse);
        imgAlertType.setImageResource(R.drawable.alert_head_physical_abuse);
        rlAdditionalInfoCollapsed.setBackgroundColor(
          getResources().getColor(R.color.alert_physical_abuse_dark));
        rlAdditionalInfoExpanded.setBackgroundColor(
          getResources().getColor(R.color.alert_physical_abuse_dark));
        rlAddress.setBackgroundColor(getResources().getColor(R.color.alert_physical_abuse_dark));
        llAction.setBackgroundResource(R.drawable.bg_alert_physical_abuse_action);
        txtBtnClose.setBackgroundResource(R.drawable.bg_alert_physical_abuse_action);
        viewSeparatorVertical.setBackgroundColor(
          getResources().getColor(R.color.alert_physical_abuse));
        viewSeparatorHorizontal.setBackgroundColor(
          getResources().getColor(R.color.alert_physical_abuse));
        break;
      case TRAPPED:
        rlAlertContainer.setBackgroundResource(R.drawable.bg_alert_trapped);
        imgAlertType.setImageResource(R.drawable.alert_head_trapped);
        rlAdditionalInfoCollapsed.setBackgroundColor(
          getResources().getColor(R.color.alert_trapped_dark));
        rlAdditionalInfoExpanded.setBackgroundColor(
          getResources().getColor(R.color.alert_trapped_dark));
        rlAddress.setBackgroundColor(getResources().getColor(R.color.alert_trapped_dark));
        llAction.setBackgroundResource(R.drawable.bg_alert_trapped_action);
        txtBtnClose.setBackgroundResource(R.drawable.bg_alert_trapped_action);
        viewSeparatorVertical.setBackgroundColor(getResources().getColor(R.color.alert_trapped));
        viewSeparatorHorizontal.setBackgroundColor(getResources().getColor(R.color.alert_trapped));
        break;
      case CAR_ACCIDENT:
        rlAlertContainer.setBackgroundResource(R.drawable.bg_alert_car_accident);
        imgAlertType.setImageResource(R.drawable.alert_head_car_accident);
        rlAdditionalInfoCollapsed.setBackgroundColor(
          getResources().getColor(R.color.alert_car_accident_dark));
        rlAdditionalInfoExpanded.setBackgroundColor(
          getResources().getColor(R.color.alert_car_accident_dark));
        rlAddress.setBackgroundColor(getResources().getColor(R.color.alert_car_accident_dark));
        llAction.setBackgroundResource(R.drawable.bg_alert_car_accident_action);
        txtBtnClose.setBackgroundResource(R.drawable.bg_alert_car_accident_action);
        viewSeparatorVertical.setBackgroundColor(
          getResources().getColor(R.color.alert_car_accident));
        viewSeparatorHorizontal.setBackgroundColor(
          getResources().getColor(R.color.alert_car_accident));
        break;
      case NATURAL_DISASTER:
        rlAlertContainer.setBackgroundResource(R.drawable.bg_alert_natural_disaster);
        imgAlertType.setImageResource(R.drawable.alert_head_natural_disaster);
        rlAdditionalInfoCollapsed.setBackgroundColor(
          getResources().getColor(R.color.alert_natural_disaster_dark));
        rlAdditionalInfoExpanded.setBackgroundColor(
          getResources().getColor(R.color.alert_natural_disaster_dark));
        rlAddress.setBackgroundColor(getResources().getColor(R.color.alert_natural_disaster_dark));
        llAction.setBackgroundResource(R.drawable.bg_alert_natural_disaster_action);
        txtBtnClose.setBackgroundResource(R.drawable.bg_alert_natural_disaster_action);
        viewSeparatorVertical.setBackgroundColor(
          getResources().getColor(R.color.alert_natural_disaster));
        viewSeparatorHorizontal.setBackgroundColor(
          getResources().getColor(R.color.alert_natural_disaster));
        break;
      case PRE_AUTHORISATION:
        rlAlertContainer.setBackgroundResource(R.drawable.bg_alert_pre_authorisation);
        imgAlertType.setImageResource(R.drawable.alert_head_pre_authorisation);
        rlAdditionalInfoCollapsed.setBackgroundColor(
          getResources().getColor(R.color.alert_pre_authorisation_dark));
        rlAdditionalInfoExpanded.setBackgroundColor(
          getResources().getColor(R.color.alert_pre_authorisation_dark));
        rlAddress.setBackgroundColor(getResources().getColor(R.color.alert_pre_authorisation_dark));
        llAction.setBackgroundResource(R.drawable.bg_alert_pre_authorisation_action);
        txtBtnClose.setBackgroundResource(R.drawable.bg_alert_pre_authorisation_action);
        viewSeparatorVertical.setBackgroundColor(
          getResources().getColor(R.color.alert_pre_authorisation));
        viewSeparatorHorizontal.setBackgroundColor(
          getResources().getColor(R.color.alert_pre_authorisation));
        break;
      case UN_RECOGNIZED:
        rlAlertContainer.setBackgroundResource(R.drawable.bg_alert_un_recognized);
        imgAlertType.setImageResource(R.drawable.alert_head_un_recognized);
        rlAdditionalInfoCollapsed.setBackgroundColor(
          getResources().getColor(R.color.alert_un_recognized_dark));
        rlAdditionalInfoExpanded.setBackgroundColor(
          getResources().getColor(R.color.alert_un_recognized_dark));
        rlAddress.setBackgroundColor(getResources().getColor(R.color.alert_un_recognized_dark));
        llAction.setBackgroundResource(R.drawable.bg_alert_un_recognized_action);
        txtBtnClose.setBackgroundResource(R.drawable.bg_alert_un_recognized_action);
        viewSeparatorVertical.setBackgroundColor(
          getResources().getColor(R.color.alert_un_recognized));
        viewSeparatorHorizontal.setBackgroundColor(
          getResources().getColor(R.color.alert_un_recognized));
        break;
    }
  }

  @Override
  public void onMapReady(GoogleMap googleMap) {
    setUpMap(googleMap);
    googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
      new LatLng(receivedCell411Alert.lat, receivedCell411Alert.lon), 16f));
    Marker marker = googleMap.addMarker(new MarkerOptions().position(
      new LatLng(receivedCell411Alert.lat, receivedCell411Alert.lon)).title(
      receivedCell411Alert.issuerFirstName + " " + receivedCell411Alert.issuerLastName).icon(
      BitmapDescriptorFactory.fromResource(R.drawable.ic_map_marker)));
    //marker.showInfoWindow();
  }

  private void setUpMap(GoogleMap googleMap) {
    googleMap.getUiSettings().setAllGesturesEnabled(true);
    googleMap.getUiSettings().setCompassEnabled(true);
    googleMap.getUiSettings().setMyLocationButtonEnabled(false);
    googleMap.getUiSettings().setRotateGesturesEnabled(true);
    googleMap.getUiSettings().setScrollGesturesEnabled(true);
    googleMap.getUiSettings().setTiltGesturesEnabled(true);
    googleMap.getUiSettings().setZoomControlsEnabled(false);
    googleMap.getUiSettings().setZoomGesturesEnabled(true);
  }

  protected void startIntentService() {
    Intent intent = new Intent(this, FetchAddressIntentService.class);
    intent.putExtra(Constants.RECEIVER, mResultReceiver);
    intent.putExtra(Constants.LOCATION_DATA_LAT, Singleton.INSTANCE.getLatitude());
    intent.putExtra(Constants.LOCATION_DATA_LNG, Singleton.INSTANCE.getLongitude());
    FetchAddressIntentService.enqueueWork(this, intent);
  }

  @Override
  public void onLastLocationRetrieved(Location mLastLocation, Feature feature) {
    LogEvent.Log(TAG, "onLastLocationRetrieved() invoked");
    if(mLastLocation != null) {
      TextView txtDistance = (TextView) findViewById(R.id.txt_distance);
      txtDistance.setText(getFormattedDistance());
    }
  }

  @Override
  public void onLocationFailToRetrieve(FailureReason failureReason, Feature feature) {
    LogEvent.Log(TAG, "onLocationFailToRetrieve() invoked: " + failureReason.toString());
    switch(failureReason) {
      case PERMISSION_DENIED:
      case SETTINGS_REQUEST_DENIED:
        TextView txtDistance = (TextView) findViewById(R.id.txt_distance);
        txtDistance.setVisibility(View.GONE);
        break;
      default:
        locationFragment.getCurrentLocationFromLocationUpdates(this, feature);
    }
  }

  @Override
  public void onCurrentLocationRetrievedFromLocationUpdates(Location location, Feature feature) {
    LogEvent.Log(TAG, "onCurrentLocationRetrievedFromLocationUpdates() invoked");
    if(location != null) {
      TextView txtDistance = (TextView) findViewById(R.id.txt_distance);
      txtDistance.setText(getFormattedDistance());
    }
  }

  @Override
  public void onClick(View v) {
    int id = v.getId();
    switch(id) {
      case R.id.txt_btn_expand:
        rlAdditionalInfoCollapsed.setVisibility(View.GONE);
        rlAdditionalInfoExpanded.setVisibility(View.VISIBLE);
        break;
      case R.id.txt_btn_collapse:
        rlAdditionalInfoCollapsed.setVisibility(View.VISIBLE);
        rlAdditionalInfoExpanded.setVisibility(View.GONE);
        break;
      case R.id.fab_chat:
        openChat();
        break;
      case R.id.fab_navigate:
        if(!isApiCallInProgress()) {
          openMapForNavigation();
        } else {
          sendToast(R.string.please_wait);
        }
        break;
      case R.id.fab_phone:
        if(!isApiCallInProgress()) {
          number = mobileNumber;
          checkPermissionAndDialEmergencyNumber();
        } else {
          sendToast(R.string.please_wait);
        }
        break;
      case R.id.rl_btn_call_emergency_contact:
        if(!isApiCallInProgress()) {
          number = emergencyContactNumber;
          checkPermissionAndDialEmergencyNumber();
        } else {
          sendToast(R.string.please_wait);
        }
        break;
      case R.id.rl_btn_forward_alert:
        if(!isApiCallInProgress()) {
          Intent intentAlertIssuing = new Intent(this, AlertIssuingActivity.class);
          intentAlertIssuing.putExtra("alertType",
            convertEnumToAlertKey(receivedCell411Alert.alertType));
          intentAlertIssuing.putExtra("forwardedAlertId", receivedCell411Alert.cell411AlertId);
          startActivity(intentAlertIssuing);
        } else {
          sendToast(R.string.please_wait);
        }
        break;
      case R.id.txt_btn_cannot_help:
        if(!isApiCallInProgress()) {
          ParseUser currentUser = ParseUser.getCurrentUser();
          if(currentUser != null && Singleton.isLoggedIn() && !isAlertFromFacebook) {
            cannotHelpTapped(false);
          } else {
            finish();
          }
        } else {
          sendToast(R.string.please_wait);
        }
        break;
      case R.id.txt_btn_help:
        if(!isApiCallInProgress()) {
          boolean isLoggedIn = Singleton.isLoggedIn();
          ParseUser currentUser = ParseUser.getCurrentUser();
          if(currentUser != null && isLoggedIn) {
            helpTapped();
          } else {
            helpTappedByAnonymousUser();
          }
        } else {
          sendToast(R.string.please_wait);
        }
        break;
      case R.id.img_close:
      case R.id.txt_btn_close:
        if(!isApiCallInProgress()) {
          finish();
        } else {
          sendToast(R.string.please_wait);
        }
        break;
    }
  }

  private boolean isApiCallInProgress() {
    return pbHelp.getVisibility() == View.VISIBLE || pbCannotHelp.getVisibility() == View.VISIBLE ?
             true : false;
  }

  private void checkPermissionAndDialEmergencyNumber() {
    if(ContextCompat.checkSelfPermission(this, android.Manifest.permission.CALL_PHONE) !=
         PackageManager.PERMISSION_GRANTED) {
      if(ActivityCompat.shouldShowRequestPermissionRationale(this,
        android.Manifest.permission.CALL_PHONE)) {
        ActivityCompat.requestPermissions(this,
          new String[]{android.Manifest.permission.CALL_PHONE}, PERMISSION_CALL_PHONE);
      } else {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CALL_PHONE},
          PERMISSION_CALL_PHONE);
      }
    } else {
      dialNumber();
    }
  }

  @Override
  public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                         @NonNull int[] grantResults) {
    if(requestCode == PERMISSION_CALL_PHONE) {
      if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
        dialNumber();
      } else {
        sendToast("App does not have permission to make a call");
      }
    } else {
      super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }
  }

  private void dialNumber() {
    // initiate 112 phone call
    Intent callIntent = new Intent(Intent.ACTION_CALL);
    callIntent.setData(Uri.parse("tel:" + number));
    startActivity(callIntent);
  }

  private void openChat() {
    Intent intentChat = new Intent(AlertActivity.this, ChatActivity.class);
    intentChat.putExtra("entityType", ChatRoom.EntityType.ALERT.toString());
    intentChat.putExtra("entityObjectId", receivedCell411Alert.cell411AlertId);
    intentChat.putExtra("entityName", convertEnumToString(receivedCell411Alert.alertType));
    intentChat.putExtra("alertType", convertEnumToString(receivedCell411Alert.alertType));
    intentChat.putExtra("issuedBy", receivedCell411Alert.issuerFirstName);
    intentChat.putExtra("time", String.valueOf(receivedCell411Alert.createdAt));
    startActivity(intentChat);
  }

  private void openMapForNavigation() {
    try {
      Uri gmmIntentUri = Uri.parse(
        "google.navigation:q=" + receivedCell411Alert.lat + "," + receivedCell411Alert.lon);
      Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
      mapIntent.setPackage("com.google.android.apps.maps");
      startActivity(mapIntent);
    } catch(ActivityNotFoundException e) {
      e.printStackTrace();
      sendToast(R.string.maps_app_not_installed);
    }
  }

  private void cannotHelpTapped(final boolean crossButton) {
    pbCannotHelp.setVisibility(View.VISIBLE);
    txtBtnClose.setText(R.string.working);
    txtBtnClose.setVisibility(View.VISIBLE);
    ParseRelation relation = ParseUser.getCurrentUser().getRelation("spammedBy");
    ParseQuery parseQuery = relation.getQuery();
    parseQuery.findInBackground(new FindCallback<ParseUser>() {
      @Override
      public void done(List list, ParseException e) {
        if(e == null) {
          if(list != null) {
            boolean userHasSpammedCurrentUser = false;
            for(int i = 0; i < list.size(); i++) {
              if(receivedCell411Alert.userId.equals(((ParseUser) list.get(i)).getObjectId())) {
                userHasSpammedCurrentUser = true;
                break;
              }
            }
            if(userHasSpammedCurrentUser) {
              pbCannotHelp.setVisibility(View.GONE);
              txtBtnClose.setText(R.string.close);
              LogEvent.Log(TAG, "User is spammed so cannot make an entry for rejectedBy");
              if(crossButton) {
                finish();
              }
            } else {
              // Whenever user taps on Cannot Help to initiate reject, update rejectedBy
              // column on Parse by mapping current user
              ParseQuery<ParseObject> query = ParseQuery.getQuery("Cell411Alert");
              if(receivedCell411Alert.forwardingAlertId != null) {
                ArrayList<String> arrayList = new ArrayList<>();
                arrayList.add(receivedCell411Alert.cell411AlertId);
                arrayList.add(receivedCell411Alert.forwardingAlertId);
                query.whereContainedIn("objectId", arrayList);
              } else {
                query.whereEqualTo("objectId", receivedCell411Alert.cell411AlertId);
              }
              query.findInBackground(new FindCallback<ParseObject>() {
                @Override
                public void done(List<ParseObject> parseObjects, ParseException e) {
                  pbCannotHelp.setVisibility(View.GONE);
                  txtBtnClose.setText(R.string.close);
                  if(e == null) {
                    if(receivedCell411Alert.forwardedBy != null) {
                      for(int i = 0; i < 2; i++) {
                        ParseObject cell411AlertObject = parseObjects.get(i);
                        if(cell411AlertObject.getObjectId().equals(
                          receivedCell411Alert.cell411AlertId)) {
                          ParseRelation relation =
                            cell411AlertObject.getRelation("rejectedByForwardedMember");
                          relation.add(ParseUser.getCurrentUser());
                          cell411AlertObject.saveInBackground();
                        } else {
                          ParseRelation relation = cell411AlertObject.getRelation("rejectedBy");
                          relation.add(ParseUser.getCurrentUser());
                          cell411AlertObject.saveInBackground();
                        }
                      }
                    } else {
                      ParseObject cell411AlertObject = parseObjects.get(0);
                      ParseRelation relation = cell411AlertObject.getRelation("rejectedBy");
                      relation.add(ParseUser.getCurrentUser());
                      cell411AlertObject.saveInBackground();
                    }
                    if(receivedCell411Alert.forwardedBy == null &&
                         receivedCell411Alert.cellName == null) {
                      if(weakRef.get() != null && !weakRef.get().isFinishing()) {
                        // Do not send alert if: 1. the alert is forwarded to current user
                        // and current user is rejecting to help or, 2. the alert is issued
                        // to Public Cell
                        showAdditionalNoteDialog4Rejector(crossButton);
                      }
                    } else {
                      if(crossButton) {
                        finish();
                      }
                    }
                  } else {
                    sendToast("App does not have permission to make a call");
                    if(crossButton) {
                      finish();
                    }
                  }
                }
              });
            }
          } else {
            // Seems like the list is empty
            LogEvent.Log("MapActivity", "Spam users list is null");
            pbCannotHelp.setVisibility(View.GONE);
            txtBtnClose.setText(R.string.close);
            if(crossButton) {
              finish();
            }
          }
        } else {
          sendToast("App does not have permission to make a call");
          pbCannotHelp.setVisibility(View.GONE);
          txtBtnClose.setText(R.string.close);
          if(crossButton) {
            finish();
          }
        }
      }
    });
  }

  private void helpTappedByAnonymousUser() {
    pbHelp.setVisibility(View.VISIBLE);
    txtBtnClose.setText(R.string.working);
    txtBtnClose.setVisibility(View.VISIBLE);
    if(Singleton.INSTANCE.getLatitude() == 0) {
      pbHelp.setVisibility(View.GONE);
      txtBtnClose.setText(R.string.close);
      showAdditionalNoteDialog4AnonymousHelper(getString(R.string.few_minutes));
      return;
    }
    GeoApiContext contextGeoApi =
      new GeoApiContext.Builder().apiKey(GOOGLE_DIRECTIONS_API_KEY).build();
    String origin =
      Singleton.INSTANCE.getLatitude() + "," + Singleton.INSTANCE.getLongitude();
    String destination = receivedCell411Alert.lat + "," + receivedCell411Alert.lon;
    DirectionsApiRequest directionsApiRequest =
      DirectionsApi.getDirections(contextGeoApi, origin, destination);
    directionsApiRequest.setCallback(
      new com.google.maps.PendingResult.Callback<DirectionsResult>() {
        @Override
        public void onResult(final DirectionsResult resultDirection) {
          DirectionsRoute[] result = resultDirection.routes;
          for(int i = 0; i < result.length; i++) {
            LogEvent.Log(TAG, "result[" + i + "] " + result.toString());
          }
          LogEvent.Log(TAG, result[0].overviewPolyline.getEncodedPath().toString());
          LogEvent.Log(TAG, "Duration: " + result[0].legs[0].duration.humanReadable);
          final String duration = result[0].legs[0].duration.humanReadable;
          runOnUiThread(new Runnable() {
            @Override
            public void run() {
              pbHelp.setVisibility(View.GONE);
              txtBtnClose.setText(R.string.close);
              showAdditionalNoteDialog4AnonymousHelper(duration);
            }
          });
        }

        @Override
        public void onFailure(final Throwable e) {
          runOnUiThread(new Runnable() {
            @Override
            public void run() {
              e.printStackTrace();
              LogEvent.Log(TAG, "Error retrieving direction: " + e.getMessage());
              sendToast(R.string.unable_to_retrieve_direction);
              pbHelp.setVisibility(View.GONE);
              txtBtnClose.setText(R.string.close);
            }
          });
        }
      });
  }

  private void showAdditionalNoteDialog4AnonymousHelper(final String duration) {
    String receiverName = (String) parseUser.get("firstName");
    AlertDialog.Builder alert =
      new AlertDialog.Builder(Singleton.INSTANCE.getCurrentActivity());
    alert.setTitle(R.string.dialog_title_enter_your_name);
    alert.setMessage(getString(R.string.dialog_msg_please_enter_your_name, receiverName));
    alert.setCancelable(false);
    LayoutInflater inflater = (LayoutInflater) getSystemService(Service.LAYOUT_INFLATER_SERVICE);
    View view = inflater.inflate(R.layout.layout_add_text_anonymous_user, null);
    final EditText etName = (EditText) view.findViewById(R.id.et_name);
    final EditText etAdditionalNote = (EditText) view.findViewById(R.id.et_additional_text);
    alert.setView(view);
    alert.setPositiveButton(R.string.dialog_btn_send, new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int which) {
      }
    });
    final AlertDialog dialog = alert.create();
    dialog.show();
    dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        final String name = etName.getText().toString().trim();
        if(name.isEmpty()) {
          doSendToast2();
          return;
        }
        final String additionalText = etAdditionalNote.getText().toString().trim();
        if(!additionalText.isEmpty()) {
          final ParseObject additionalNoteObj = new ParseObject("AdditionalNote");
          additionalNoteObj.put("writerId", "anonymous");
          additionalNoteObj.put("writerName", name);
          if(receivedCell411Alert.forwardedBy != null) {
            additionalNoteObj.put("forwardedBy", receivedCell411Alert.forwardedBy);
          }
          if(receivedCell411Alert.cellName != null) {
            additionalNoteObj.put("cellName", receivedCell411Alert.cellName);
            additionalNoteObj.put("cellId", receivedCell411Alert.cellId);
          }
          additionalNoteObj.put("writerDuration", duration);
          additionalNoteObj.put("alertType", "HELPER");
          additionalNoteObj.put("note", additionalText);
          additionalNoteObj.put("seen", 0); // 0 for unseen and 1 for seen
          additionalNoteObj.put("cell411AlertId", receivedCell411Alert.cell411AlertId);
          if(isAlertFromFacebook) {
            additionalNoteObj.put("userType", "FB");
          }
          additionalNoteObj.saveEventually(new SaveCallback() {
            @Override
            public void done(ParseException e) {
              if(e == null) {
                // Send push notification to the needy to inform that user is coming to help
                ParseQuery pushQuery = ParseInstallation.getQuery();
                pushQuery.whereEqualTo("user", parseUser);
                ParsePush push = new ParsePush();
                push.setQuery(pushQuery);
                JSONObject data = new JSONObject();
                try {
                  if((receivedCell411Alert.forwardedBy == null ||
                        receivedCell411Alert.forwardedBy.isEmpty()) &&
                       (receivedCell411Alert.cellName == null ||
                          receivedCell411Alert.cellName.isEmpty())) {
                    data.put("alert", getString(R.string.alert_from_fb_user, name));
                  } else if(receivedCell411Alert.cellName != null &&
                              !receivedCell411Alert.cellName.isEmpty()) {
                    data.put("alert", getString(R.string.alert_from_public_cell_user, name,
                      receivedCell411Alert.cellName));
                    data.put("cellName", receivedCell411Alert.cellName);
                    data.put("cellId", receivedCell411Alert.cellId);
                  } else {
                    data.put("alert", getString(R.string.alert_from_forwarded_user, name,
                      receivedCell411Alert.forwardedBy));
                    data.put("forwardedBy", receivedCell411Alert.forwardedBy);
                  }
                  data.put("userId", "anonymous");
                  data.put("name", name);
                  data.put("sound", "default");
                  data.put("createdAt", receivedCell411Alert.createdAt);
                  data.put("duration", duration);
                  data.put("additionalNote", additionalText);
                  data.put("additionalNoteId", additionalNoteObj.getObjectId());
                  data.put("alertType", "HELPER");
                  data.put("badge", "Increment");
                  if(isAlertFromFacebook) {
                    data.put("userType", "FB");
                  }
                } catch(JSONException e1) {
                  e1.printStackTrace();
                }
                push.setData(data);
                push.sendInBackground();
              } else {
              }
            }
          });
        } else {
          // Send push notification to the needy to inform that user is coming to help
          ParseQuery pushQuery = ParseInstallation.getQuery();
          pushQuery.whereEqualTo("user", parseUser);
          ParsePush push = new ParsePush();
          push.setQuery(pushQuery);
          JSONObject data = new JSONObject();
          try {
            if((receivedCell411Alert.forwardedBy == null ||
                  receivedCell411Alert.forwardedBy.isEmpty()) &&
                 (receivedCell411Alert.cellName == null ||
                    receivedCell411Alert.cellName.isEmpty())) {
              data.put("alert", getString(R.string.alert_from_fb_user, name));
            } else if(receivedCell411Alert.cellName != null &&
                        !receivedCell411Alert.cellName.isEmpty()) {
              data.put("alert", getString(R.string.alert_from_public_cell_user, name,
                receivedCell411Alert.cellName));
              data.put("cellName", receivedCell411Alert.cellName);
              data.put("cellId", receivedCell411Alert.cellId);
            } else {
              data.put("alert", getString(R.string.alert_from_forwarded_user, name,
                receivedCell411Alert.forwardedBy));
              data.put("forwardedBy", receivedCell411Alert.forwardedBy);
            }
            data.put("userId", "anonymous");
            data.put("name", name);
            data.put("sound", "default");
            data.put("createdAt", receivedCell411Alert.createdAt);
            data.put("duration", duration);
            data.put("alertType", "HELPER");
            data.put("badge", "Increment");
          } catch(JSONException e1) {
            e1.printStackTrace();
          }
          push.setData(data);
          push.sendInBackground();
        }
        dialog.dismiss();
      }
    });
  }

  private void doSendToast2() {
    if(Modules.isWeakReferenceValid()) {
      sendToast(R.string.validation_please_enter_name);
    }
  }

  private void showAdditionalNoteDialog4Helper(final String duration) {
    String receiverName = (String) parseUser.get("firstName");
    AlertDialog.Builder alert =
      new AlertDialog.Builder(Singleton.INSTANCE.getCurrentActivity());
    alert.setTitle(R.string.dialog_title_additional_note);
    alert.setMessage(getString(R.string.dialog_msg_additional_note, receiverName));
    alert.setCancelable(false);
    LayoutInflater inflater = (LayoutInflater) getSystemService(Service.LAYOUT_INFLATER_SERVICE);
    View view = inflater.inflate(R.layout.layout_add_text, null);
    final EditText etAdditionalNote = (EditText) view.findViewById(R.id.et_additional_text);
    alert.setView(view);
    alert.setPositiveButton(R.string.dialog_btn_send, new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int which) {
        final String name = ParseUser.getCurrentUser().get("firstName") + " " +
                              ParseUser.getCurrentUser().get("lastName");
        final String additionalText = etAdditionalNote.getText().toString().trim();
        if(!additionalText.isEmpty()) {
          final ParseObject additionalNoteObj = new ParseObject("AdditionalNote");
          additionalNoteObj.put("writerId", ParseUser.getCurrentUser().getObjectId());
          additionalNoteObj.put("writerName", name);
          if(receivedCell411Alert.forwardedBy != null) {
            additionalNoteObj.put("forwardedBy", receivedCell411Alert.forwardedBy);
          }
          if(receivedCell411Alert.cellName != null) {
            additionalNoteObj.put("cellName", receivedCell411Alert.cellName);
            additionalNoteObj.put("cellId", receivedCell411Alert.cellId);
          }
          additionalNoteObj.put("writerDuration", duration);
          additionalNoteObj.put("alertType", "HELPER");
          additionalNoteObj.put("note", additionalText);
          additionalNoteObj.put("seen", 0); // 0 for unseen and 1 for seen
          additionalNoteObj.put("cell411AlertId", receivedCell411Alert.cell411AlertId);
          if(isAlertFromFacebook) {
            additionalNoteObj.put("userType", "FB");
          }
          additionalNoteObj.saveEventually(new SaveCallback() {
            @Override
            public void done(ParseException e) {
              if(e == null) {
                // Send push notification to the needy to inform that user is coming to help
                ParseQuery pushQuery = ParseInstallation.getQuery();
                pushQuery.whereEqualTo("user", parseUser);
                ParsePush push = new ParsePush();
                push.setQuery(pushQuery);
                JSONObject data = new JSONObject();
                try {
                  if((receivedCell411Alert.forwardedBy == null ||
                        receivedCell411Alert.forwardedBy.isEmpty()) &&
                       (receivedCell411Alert.cellName == null ||
                          receivedCell411Alert.cellName.isEmpty())) {
                    if(isAlertFromFacebook) {
                      data.put("alert", getString(R.string.alert_from_fb_user, name));
                    } else {
                      data.put("alert", name + " " + getString(R.string.alert_from_known_user));
                    }
                  } else if(receivedCell411Alert.cellName != null &&
                              !receivedCell411Alert.cellName.isEmpty()) {
                    data.put("alert", getString(R.string.alert_from_public_cell_user, name,
                      receivedCell411Alert.cellName));
                    data.put("cellName", receivedCell411Alert.cellName);
                    data.put("cellId", receivedCell411Alert.cellId);
                  } else {
                    data.put("alert", getString(R.string.alert_from_forwarded_user, name,
                      receivedCell411Alert.forwardedBy));
                    data.put("forwardedBy", receivedCell411Alert.forwardedBy);
                  }
                  data.put("userId", ParseUser.getCurrentUser().getObjectId());
                  data.put("name", name);
                  data.put("sound", "default");
                  data.put("createdAt", receivedCell411Alert.createdAt);
                  data.put("duration", duration);
                  data.put("additionalNote", additionalText);
                  data.put("additionalNoteId", additionalNoteObj.getObjectId());
                  data.put("alertType", "HELPER");
                  data.put("badge", "Increment");
                  if(isAlertFromFacebook) {
                    data.put("userType", "FB");
                  }
                } catch(JSONException e1) {
                  e1.printStackTrace();
                }
                push.setData(data);
                push.sendInBackground();
              } else {
              }
            }
          });
        } else {
          // Send push notification to the needy to inform that user is coming to help
          ParseQuery pushQuery = ParseInstallation.getQuery();
          pushQuery.whereEqualTo("user", parseUser);
          ParsePush push = new ParsePush();
          push.setQuery(pushQuery);
          JSONObject data = new JSONObject();
          try {
            if((receivedCell411Alert.forwardedBy == null ||
                  receivedCell411Alert.forwardedBy.isEmpty()) &&
                 (receivedCell411Alert.cellName == null ||
                    receivedCell411Alert.cellName.isEmpty())) {
              if(isAlertFromFacebook) {
                data.put("alert", getString(R.string.alert_from_fb_user, name));
              } else {
                data.put("alert", name + " " + getString(R.string.alert_from_known_user));
              }
            } else if(receivedCell411Alert.cellName != null &&
                        !receivedCell411Alert.cellName.isEmpty()) {
              data.put("alert", getString(R.string.alert_from_public_cell_user, name,
                receivedCell411Alert.cellName));
              data.put("cellName", receivedCell411Alert.cellName);
              data.put("cellId", receivedCell411Alert.cellId);
            } else {
              data.put("alert", getString(R.string.alert_from_forwarded_user, name,
                receivedCell411Alert.forwardedBy));
              data.put("forwardedBy", receivedCell411Alert.forwardedBy);
            }
            data.put("userId", ParseUser.getCurrentUser().getObjectId());
            data.put("name", name);
            data.put("sound", "default");
            data.put("createdAt", receivedCell411Alert.createdAt);
            data.put("duration", duration);
            data.put("alertType", "HELPER");
            data.put("badge", "Increment");
          } catch(JSONException e1) {
            e1.printStackTrace();
          }
          push.setData(data);
          push.sendInBackground();
        }
        //LogEvent.Log(TAG, "showAdditionalNoteDialog4Helper issuerName2: " + issuerName);
      }
    });
    AlertDialog dialog = alert.create();
    dialog.show();
  }

  private void helpTapped() {
    pbHelp.setVisibility(View.VISIBLE);
    txtBtnClose.setText(R.string.working);
    txtBtnClose.setVisibility(View.VISIBLE);
    ParseRelation relation = ParseUser.getCurrentUser().getRelation("spammedBy");
    ParseQuery parseQuery = relation.getQuery();
    parseQuery.findInBackground(new FindCallback<ParseUser>() {
      @Override
      public void done(List list, ParseException e) {
        if(e == null) {
          if(list != null) {
            boolean userHasSpammedCurrentUser = false;
            for(int i = 0; i < list.size(); i++) {
              if(receivedCell411Alert.userId.equals(((ParseUser) list.get(i)).getObjectId())) {
                userHasSpammedCurrentUser = true;
                break;
              }
            }
            if(userHasSpammedCurrentUser) {
              pbHelp.setVisibility(View.GONE);
              txtBtnClose.setText(R.string.close);
              LogEvent.Log(TAG, "User is spammed so cannot make an entry for initiatedBy");
            } else {
              // LogEvent.Log(TAG, "Directions Key: " + GOOGLE_DIRECTIONS_API_KEY);
              // Find route and duration utilizing Directions API to display it on map
                            /*GeoApiContext contextGeoApi = new GeoApiContext()
                                    .setApiKey(GOOGLE_DIRECTIONS_API_KEY);*/
              //        .setApiKey("AIzaSyDqNhzic0TchaIfLKVlm3PHYuyWA1XchK4");
              GeoApiContext contextGeoApi =
                new GeoApiContext.Builder().apiKey(GOOGLE_DIRECTIONS_API_KEY).build();
              String origin =
                Singleton.INSTANCE.getLatitude() + "," +
                  Singleton.INSTANCE.getLongitude();
              String destination = receivedCell411Alert.lat + "," + receivedCell411Alert.lon;
              //String destination = "25.2800,82.9600";
              DirectionsApiRequest directionsApiRequest =
                DirectionsApi.getDirections(contextGeoApi, origin, destination);
              directionsApiRequest.setCallback(
                new com.google.maps.PendingResult.Callback<DirectionsResult>() {
                  @Override
                  public void onResult(final DirectionsResult resultDirection) {
                    final DirectionsRoute[] result = resultDirection.routes;
                    for(int i = 0; i < result.length; i++) {
                      LogEvent.Log(TAG, "result[" + i + "] " + result.toString());
                    }
                    LogEvent.Log(TAG, result[0].overviewPolyline.getEncodedPath().toString());
                    LogEvent.Log(TAG, "Duration: " + result[0].legs[0].duration.humanReadable);
                    final String duration = result[0].legs[0].duration.humanReadable;
                    //final List<LatLng> decodedPath = PolyUtil.decode(result[0]
                    // .overviewPolyline.getEncodedPath());
                    runOnUiThread(new Runnable() {
                      @Override
                      public void run() {
                        //if (INSTANCE.getCurrentActivity() instanceof MapActivity)
                        //((MapActivity) INSTANCE.getCurrentActivity())
                        // .drawPolyLine(decodedPath, lat, lon, issuerName, parseUser);
                        // Whenever user taps on Yes to initiate help,
                        // update initiatedBy column on Parse by mapping current user
                        ParseQuery<ParseObject> query = ParseQuery.getQuery("Cell411Alert");
                        if(receivedCell411Alert.forwardingAlertId != null) {
                          ArrayList<String> arrayList = new ArrayList<>();
                          arrayList.add(receivedCell411Alert.cell411AlertId);
                          arrayList.add(receivedCell411Alert.forwardingAlertId);
                          query.whereContainedIn("objectId", arrayList);
                        } else {
                          query.whereEqualTo("objectId", receivedCell411Alert.cell411AlertId);
                        }
                        query.findInBackground(new FindCallback<ParseObject>() {
                          @Override
                          public void done(List<ParseObject> parseObjects, ParseException e) {
                            pbHelp.setVisibility(View.GONE);
                            txtBtnClose.setText(R.string.close);
                            if(e == null) {
                              if(receivedCell411Alert.forwardedBy != null) {
                                for(int i = 0; i < 2; i++) {
                                  ParseObject cell411AlertObject = parseObjects.get(i);
                                  if(cell411AlertObject.getObjectId().equals(
                                    receivedCell411Alert.cell411AlertId)) {
                                    ParseRelation relation = cell411AlertObject.getRelation(
                                      "initiatedByForwardedMember");
                                    relation.add(ParseUser.getCurrentUser());
                                    cell411AlertObject.saveInBackground();
                                  } else {
                                    ParseRelation relation =
                                      cell411AlertObject.getRelation("initiatedBy");
                                    relation.add(ParseUser.getCurrentUser());
                                    cell411AlertObject.saveInBackground();
                                  }
                                }
                              } else {
                                ParseObject cell411AlertObject = parseObjects.get(0);
                                ParseRelation relation =
                                  cell411AlertObject.getRelation("initiatedBy");
                                relation.add(ParseUser.getCurrentUser());
                                cell411AlertObject.saveInBackground();
                              }
                              if(weakRef.get() != null && !weakRef.get().isFinishing()) {
                                showAdditionalNoteDialog4Helper(duration);
                              }
                                                        /*ParseObject cell411AlertObject = parseObjects.get(0);
                                                        ParseRelation relation = cell411AlertObject
                                                                .getRelation("initiatedBy");
                                                        relation.add(ParseUser.getCurrentUser());
                                                        cell411AlertObject.saveInBackground(new SaveCallback() {
                                                            @Override
                                                            public void done(ParseException e) {
                                                                pbHelp.setVisibility(View.GONE);
                                                                txtBtnClose.setText(R.string.close);
                                                                if (e == null) {
                                                                    if (weakRef.get() != null && !weakRef.get().isFinishing()) {
                                                                        showAdditionalNoteDialog4Helper(duration);
                                                                    }
                                                                } else {
                                                                    if (weakRef.get() != null && !weakRef.get()
                                                                            .isFinishing()) {
                                                                        UtilityMethods.displayToast(getApplicationContext(), e);
                                                                    }
                                                                }
                                                            }
                                                        });*/
                            } else {
                              if(weakRef.get() != null && !weakRef.get().isFinishing()) {
                                sendToast("App does not have permission to make a call");
                              }
                            }
                          }
                        });
                      }
                    });
                  }

                  @Override
                  public void onFailure(final Throwable e) {
                    runOnUiThread(new Runnable() {
                      @Override
                      public void run() {
                        e.printStackTrace();
                        LogEvent.Log(TAG, "Error retrieving direction: " + e.getMessage());
                        //                                            if (weakRef.get() != null && !weakRef.get().isFinishing()) {
                        //
                        //                                            }
                        //                                            pbHelp.setVisibility(View.GONE);
                        //                                            txtBtnClose.setText(R.string.close);
                        final String duration = "few minutes";
                        //final List<LatLng> decodedPath = PolyUtil.decode(result[0]
                        // .overviewPolyline.getEncodedPath());
                        runOnUiThread(new Runnable() {
                          @Override
                          public void run() {
                            //if (INSTANCE.getCurrentActivity() instanceof MapActivity)
                            //((MapActivity) INSTANCE.getCurrentActivity())
                            // .drawPolyLine(decodedPath, lat, lon, issuerName, parseUser);
                            // Whenever user taps on Yes to initiate help,
                            // update initiatedBy column on Parse by mapping current user
                            ParseQuery<ParseObject> query = ParseQuery.getQuery("Cell411Alert");
                            if(receivedCell411Alert.forwardingAlertId != null) {
                              ArrayList<String> arrayList = new ArrayList<>();
                              arrayList.add(receivedCell411Alert.cell411AlertId);
                              arrayList.add(receivedCell411Alert.forwardingAlertId);
                              query.whereContainedIn("objectId", arrayList);
                            } else {
                              query.whereEqualTo("objectId", receivedCell411Alert.cell411AlertId);
                            }
                            query.findInBackground(new FindCallback<ParseObject>() {
                              @Override
                              public void done(List<ParseObject> parseObjects, ParseException e) {
                                pbHelp.setVisibility(View.GONE);
                                txtBtnClose.setText(R.string.close);
                                if(e == null) {
                                  if(receivedCell411Alert.forwardedBy != null) {
                                    for(int i = 0; i < 2; i++) {
                                      ParseObject cell411AlertObject = parseObjects.get(i);
                                      if(cell411AlertObject.getObjectId().equals(
                                        receivedCell411Alert.cell411AlertId)) {
                                        ParseRelation relation = cell411AlertObject.getRelation(
                                          "initiatedByForwardedMember");
                                        relation.add(ParseUser.getCurrentUser());
                                        cell411AlertObject.saveInBackground();
                                      } else {
                                        ParseRelation relation =
                                          cell411AlertObject.getRelation("initiatedBy");
                                        relation.add(ParseUser.getCurrentUser());
                                        cell411AlertObject.saveInBackground();
                                      }
                                    }
                                  } else {
                                    ParseObject cell411AlertObject = parseObjects.get(0);
                                    ParseRelation relation =
                                      cell411AlertObject.getRelation("initiatedBy");
                                    relation.add(ParseUser.getCurrentUser());
                                    cell411AlertObject.saveInBackground();
                                  }
                                  if(weakRef.get() != null && !weakRef.get().isFinishing()) {
                                    showAdditionalNoteDialog4Helper(duration);
                                  }
                                                                /*ParseObject cell411AlertObject = parseObjects.get(0);
                                                                ParseRelation relation = cell411AlertObject
                                                                        .getRelation("initiatedBy");
                                                                relation.add(ParseUser.getCurrentUser());
                                                                cell411AlertObject.saveInBackground(new SaveCallback() {
                                                                    @Override
                                                                    public void done(ParseException e) {
                                                                        pbHelp.setVisibility(View.GONE);
                                                                        txtBtnClose.setText(R.string.close);
                                                                        if (e == null) {
                                                                            if (weakRef.get() != null && !weakRef.get().isFinishing()) {
                                                                                showAdditionalNoteDialog4Helper(duration);
                                                                            }
                                                                        } else {
                                                                            if (weakRef.get() != null && !weakRef.get()
                                                                                    .isFinishing()) {
                                                                                UtilityMethods.displayToast(getApplicationContext(), e);
                                                                            }
                                                                        }
                                                                    }
                                                                });*/
                                } else {
                                  if(weakRef.get() != null && !weakRef.get().isFinishing()) {
                                    sendToast("App does not have permission to make a call");
                                  }
                                }
                              }
                            });
                          }
                        });
                      }
                    });
                  }
                });
            }
          } else {
            // Seems like the list is empty
            pbHelp.setVisibility(View.GONE);
            txtBtnClose.setText(R.string.close);
            LogEvent.Log("MapActivity", "Spam users list is null");
          }
        } else {
          if(weakRef.get() != null && !weakRef.get().isFinishing()) {
            sendToast("App does not have permission to make a call");
          }
          pbHelp.setVisibility(View.GONE);
          txtBtnClose.setText(R.string.close);
        }
      }
    });
  }

  private void showAdditionalNoteDialog4Rejector(final boolean crossButton) {
        /*
        Stack Trace:
        Fatal Exception: java.lang.NullPointerException: Attempt to invoke virtual method 'java.lang.Object com.parse.ParseUser.get(java.lang.String)' on a null object reference
        at com.safearx.cell411.activity.AlertActivity.showAdditionalNoteDialog4Rejector(AlertActivity.java:2178)
        at com.safearx.cell411.activity.AlertActivity.access$800(AlertActivity.java:81)
        at com.safearx.cell411.activity.AlertActivity$10$1.done(AlertActivity.java:1426)
        at com.safearx.cell411.activity.AlertActivity$10$1.done(AlertActivity.java:1405)
        at com.parse.ParseTaskUtils$2$1.run(ParseTaskUtils.java:116)
        at android.os.Handler.handleCallback(Handler.java:739)
        at android.os.Handler.dispatchMessage(Handler.java:95)
        at android.os.Looper.loop(Looper.java:148)
        at android.app.ActivityThread.main(ActivityThread.java:7409)
        at java.lang.reflect.Method.invoke(Method.java)
        at com.android.internal.os.ZygoteInit$MethodAndArgsCaller.run(ZygoteInit.java:1230)
        at com.android.internal.os.ZygoteInit.main(ZygoteInit.java:1120)
        */
    String receiverName = (String) parseUser.get("firstName");
    AlertDialog.Builder alert = new AlertDialog.Builder(this);
    alert.setTitle(R.string.dialog_title_additional_note);
    alert.setMessage(getString(R.string.dialog_msg_additional_note, receiverName));
    alert.setCancelable(false);
    LayoutInflater inflater = (LayoutInflater) getSystemService(Service.LAYOUT_INFLATER_SERVICE);
    View view = inflater.inflate(R.layout.layout_add_text, null);
    final EditText etAdditionalNote = (EditText) view.findViewById(R.id.et_additional_text);
    alert.setView(view);
    alert.setPositiveButton(R.string.dialog_btn_send, new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int which) {
        final String name = ParseUser.getCurrentUser().get("firstName") + " " +
                              ParseUser.getCurrentUser().get("lastName");
        final String additionalText = etAdditionalNote.getText().toString().trim();
        if(!additionalText.isEmpty()) {
          final ParseObject additionalNoteObj = new ParseObject("AdditionalNote");
          additionalNoteObj.put("writerId", ParseUser.getCurrentUser().getObjectId());
          additionalNoteObj.put("writerName", name);
          additionalNoteObj.put("alertType", "REJECTOR");
          additionalNoteObj.put("note", additionalText);
          additionalNoteObj.put("seen", 0); // 0 for unseen and 1 for seen
          additionalNoteObj.put("cell411AlertId", receivedCell411Alert.cell411AlertId);
          additionalNoteObj.saveEventually(new SaveCallback() {
            @Override
            public void done(ParseException e) {
              if(e == null) {
                // Send push notification to the needy to inform that user is coming to help
                ParseQuery pushQuery = ParseInstallation.getQuery();
                pushQuery.whereEqualTo("user", parseUser);
                ParsePush push = new ParsePush();
                push.setQuery(pushQuery);
                JSONObject data = new JSONObject();
                try {
                  data.put("alert", name + " " + getString(R.string.reject_response_alert));
                  data.put("userId", ParseUser.getCurrentUser().getObjectId());
                  data.put("name", name);
                  data.put("sound", "default");
                  data.put("createdAt", receivedCell411Alert.createdAt);
                  data.put("additionalNote", additionalText);
                  data.put("additionalNoteId", additionalNoteObj.getObjectId());
                  data.put("alertType", "REJECTOR");
                  data.put("badge", "Increment");
                } catch(JSONException e1) {
                  e1.printStackTrace();
                }
                push.setData(data);
                push.sendInBackground();
              } else {
              }
            }
          });
        } else {
          // Send push notification to the needy to inform that user is coming to help
          ParseQuery pushQuery = ParseInstallation.getQuery();
          pushQuery.whereEqualTo("user", parseUser);
          ParsePush push = new ParsePush();
          push.setQuery(pushQuery);
          JSONObject data = new JSONObject();
          try {
            data.put("alert", name + " " + getString(R.string.reject_response_alert));
            data.put("userId", ParseUser.getCurrentUser().getObjectId());
            data.put("name", name);
            data.put("sound", "default");
            data.put("createdAt", receivedCell411Alert.createdAt);
            data.put("alertType", "REJECTOR");
            data.put("badge", "Increment");
          } catch(JSONException e1) {
            e1.printStackTrace();
          }
          push.setData(data);
          push.sendInBackground();
        }
        if(crossButton) {
          finish();
        }
        //LogEvent.Log(TAG, "showAdditionalNoteDialog4Rejector issuerName2: " + issuerName);
      }
    });
    AlertDialog dialog = alert.create();
    dialog.show();
  }

  class AddressResultReceiver extends ResultReceiver {
    public AddressResultReceiver(Handler handler) {
      super(handler);
    }

    @Override
    protected void onReceiveResult(int resultCode, Bundle resultData) {
      // Display the city
      if(resultCode == Constants.SUCCESS_RESULT) {
        String city = resultData.getString(Constants.RESULT_DATA_CITY);
        txtAddress.setText(city);
      } else {
        txtAddress.setText(R.string.city_not_found);
      }
    }
  }
    /*@Override
    public void onBackPressed() {
        //super.onBackPressed();
    }*/
}
