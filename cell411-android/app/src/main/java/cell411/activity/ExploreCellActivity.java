package cell411.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.SearchView;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.ViewCompat;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.parse.ParseException;
import com.parse.ParseGeoPoint;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParsePush;
import com.parse.ParseQuery;
import com.parse.ParseRelation;
import com.parse.ParseUser;
import com.parse.callback.FindCallback;
import com.parse.callback.GetCallback;
import com.parse.callback.SaveCallback;
import com.safearx.cell411.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import cell411.Singleton;
import cell411.db.DataSource;
import cell411.fragments.LocationFragment;
import cell411.interfaces.CurrentLocationFromLocationUpdatesCallBack;
import cell411.interfaces.LastLocationCallBack;
import cell411.interfaces.LocationFailToRetrieveCallBack;
import cell411.models.Cell;
import cell411.models.ChatRoom;
import cell411.models.Footer;
import cell411.utils.LogEvent;
import cell411.widgets.CircularImageView;
import cell411.widgets.Spinner;

public class ExploreCellActivity extends BaseActivity
  implements SearchView.OnQueryTextListener, SearchView.OnCloseListener, LastLocationCallBack,
               LocationFailToRetrieveCallBack, CurrentLocationFromLocationUpdatesCallBack {
  public static WeakReference<ExploreCellActivity> weakRef;
  private final String TAG = "ExploreCellActivity";
  private final int LIMIT_PER_PAGE = 10;
  private Spinner spinner;
  private RecyclerView recyclerView;
  private LinearLayoutManager linearLayoutManager;
  private CellListAdapter adapterCell;
  private ArrayList<Object> publicCellsList;
  private List<ParseObject> cell411AlertsList4JoinedAndPendingCells;
  private boolean nearbySearch = true;
  private int page = 0;
  private boolean noMoreData = false;
  private boolean apiCallInProgress = false;
  private Handler handler = new Handler();
  private int searchRadius;
  private boolean loading = true;
  private int pastVisibleItems, visibleItemCount, totalItemCount;
  private String searchString = null;
  private String metric;
  private int COLOR_ACCENT;
  private int COLOR_PRIMARY;
  private int COLOR_GRAY_CCC;
  private LocationFragment locationFragment;
  private SearchView searchView;
  private SeekBar sbRadius;
  private RadioButton rbNearBy;
  private RadioButton rbExactMatch;

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    switch(item.getItemId()) {
      case android.R.id.home:
        finish();
        return true;
      default:
        return super.onOptionsItemSelected(item);
    }
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_scrolling);
    Singleton.INSTANCE.setCurrentActivity(this);
    weakRef = new WeakReference<>(this);
    Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
    setSupportActionBar(toolbar);
    toolbar.setTitle("");
    getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    final CollapsingToolbarLayout collapsingToolbarLayout =
      (CollapsingToolbarLayout) findViewById(R.id.toolbar_layout);
    collapsingToolbarLayout.setTitleEnabled(false);
    AppBarLayout appBarLayout = (AppBarLayout) findViewById(R.id.app_bar);
    appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
      @Override
      public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
        if(collapsingToolbarLayout.getHeight() + verticalOffset <
             2 * ViewCompat.getMinimumHeight(collapsingToolbarLayout)) {
          LogEvent.Log(TAG, "IF");
          collapsingToolbarLayout.setTitleEnabled(true);
        } else {
          LogEvent.Log(TAG, "ELSE");
          collapsingToolbarLayout.setTitleEnabled(false);
        }
      }
    });
    collapsingToolbarLayout.setTitle(getString(R.string.explore_cells));
    COLOR_ACCENT = getResources().getColor(R.color.colorAccent);
    COLOR_GRAY_CCC = getResources().getColor(R.color.gray_ccc);
    COLOR_PRIMARY = getResources().getColor(R.color.highlight_color);
    spinner = (Spinner) findViewById(R.id.spinner);
    recyclerView = (RecyclerView) findViewById(R.id.rv_public_cells);
    // use this setting to improve performance if you know that changes
    // in content do not change the layout size of the RecyclerView
    recyclerView.setHasFixedSize(true);
    // use a linear layout manager
    linearLayoutManager = new LinearLayoutManager(this);
    recyclerView.setLayoutManager(linearLayoutManager);
    searchView = (SearchView) findViewById(R.id.searchview);
    int searchPlateId =
      searchView.getContext().getResources().getIdentifier("android:id/search_plate", null, null);
    View v = searchView.findViewById(searchPlateId);
    //v.setBackgroundColor(Color.parseColor("#c3e4ff"));
    int id =
      searchView.getContext().getResources().getIdentifier("android:id/search_src_text", null,
        null);
    //TextView textView = (TextView) searchView.findViewById(id);
    //textView.setTextColor(Color.WHITE);
    //textView.setTextColor(Color.parseColor(getString(R.color.text_primary)));
    searchView.setIconifiedByDefault(false);
    searchView.setOnQueryTextListener(this);
    searchView.setOnCloseListener(this);
    searchRadius = Singleton.INSTANCE.getAppPrefs().getInt("SearchRadius", 100);
    final TextView txtRadius = (TextView) findViewById(R.id.txt_radius);
    sbRadius = (SeekBar) findViewById(R.id.sb_radius);
    metric = Singleton.INSTANCE.getAppPrefs().getString("metric", "kms");
    sbRadius.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
      @Override
      public void onStopTrackingTouch(SeekBar seekBar) {
        refreshListAndSearchCell(searchString);
      }

      @Override
      public void onStartTrackingTouch(SeekBar seekBar) {
      }

      @Override
      public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        if(metric.equals("kms")) {
          if(progress <= 1) {
            txtRadius.setText(progress + " " + getString(R.string.km));
          } else {
            txtRadius.setText(progress + " " + getString(R.string.kms));
          }
        } else {
          if(progress <= 1) {
            txtRadius.setText(
              String.format("%.2f", (progress / 1.6f)) + " " + getString(R.string.mile));
          } else {
            txtRadius.setText(
              String.format("%.2f", (progress / 1.6f)) + " " + getString(R.string.miles));
          }
        }
        searchRadius = progress;
      }
    });
    sbRadius.setProgress(searchRadius);
    rbNearBy = (RadioButton) findViewById(R.id.rb_nearby);
    rbNearBy.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
      @Override
      public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if(isChecked) {
          // Search Nearby
          nearbySearch = true;
          refreshListAndSearchCell(searchString);
        }
      }
    });
    rbExactMatch = (RadioButton) findViewById(R.id.rb_exact_match);
    rbExactMatch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
      @Override
      public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if(isChecked) {
          // Search by exact match
          nearbySearch = false;
          refreshListAndSearchCell(searchString);
        }
      }
    });
    publicCellsList = new ArrayList<>();
    searchView.setEnabled(false);
    sbRadius.setEnabled(false);
    rbNearBy.setEnabled(false);
    rbExactMatch.setEnabled(false);
    locationFragment = new LocationFragment();
    FragmentManager fragmentManager = getSupportFragmentManager();
    fragmentManager.beginTransaction().add(locationFragment, "locationFragment").commit();
  }

  private void retrievePublicCells() {
    ParseQuery<ParseObject> cell411Query = new ParseQuery<>("Cell411Alert");
    cell411Query.whereEqualTo("issuedBy", ParseUser.getCurrentUser());
    cell411Query.whereEqualTo("entryFor", "CR");
    ArrayList<String> statusList = new ArrayList<>();
    statusList.add("APPROVED");
    statusList.add("PENDING");
    cell411Query.whereContainedIn("status", statusList);
    cell411Query.findInBackground(new FindCallback<ParseObject>() {
      @Override
      public void done(List<ParseObject> list, ParseException e) {
        if(e == null) {
          if(list == null) {
            cell411AlertsList4JoinedAndPendingCells = new ArrayList<>();
          } else {
            cell411AlertsList4JoinedAndPendingCells = list;
          }
          searchView.setEnabled(true);
          sbRadius.setEnabled(true);
          rbNearBy.setEnabled(true);
          rbExactMatch.setEnabled(true);
          setListScrollListener(null);
          retrievePublicCells(null);
        } else {
          spinner.setVisibility(View.GONE);
          if(weakRef.get() != null && !weakRef.get().isFinishing()) {
            Singleton.sendToast(e);
          }
        }
      }
    });
  }

  @Override
  protected void onPostCreate(@Nullable Bundle savedInstanceState) {
    super.onPostCreate(savedInstanceState);
    locationFragment.getLastLocation(this, this, Feature.RETRIEVE_NEAR_BY_CELLS);
  }

  @Override
  public void onLastLocationRetrieved(Location mLastLocation, Feature feature) {
    LogEvent.Log(TAG, "onLastLocationRetrieved() invoked");
    if(mLastLocation != null) {
      retrievePublicCells();
    }
  }

  @Override
  public void onLocationFailToRetrieve(LocationFailToRetrieveCallBack.FailureReason failureReason,
                                       Feature feature) {
    LogEvent.Log(TAG, "onLocationFailToRetrieve() invoked: " + failureReason.toString());
    switch(failureReason) {
      case PERMISSION_DENIED:
      case SETTINGS_REQUEST_DENIED:
        getApplicationContext();
        Singleton.sendToast("Using old location");
        retrievePublicCells();
        break;
      default:
        locationFragment.getCurrentLocationFromLocationUpdates(this, feature);
    }
  }

  @Override
  public void onCurrentLocationRetrievedFromLocationUpdates(Location location, Feature feature) {
    LogEvent.Log(TAG, "onCurrentLocationRetrievedFromLocationUpdates() invoked");
    if(location != null) {
      retrievePublicCells();
    }
  }

  @Override
  public void onResume() {
    super.onResume();
    if(adapterCell != null) {
      if(Singleton.INSTANCE.getPublicCellStatus() == Cell.Status.PENDING) {
        Singleton.INSTANCE.setPublicCellStatus(Cell.Status.UN_CHANGED);
        Cell cell = Singleton.INSTANCE.getTappedPublicCell();
        for(int i = 0; i < adapterCell.arrayList.size(); i++) {
          if(adapterCell.arrayList.get(i) instanceof Cell) {
            Cell cell2 = (Cell) adapterCell.arrayList.get(i);
            if(cell.cell.getObjectId().equals(cell2.cell.getObjectId())) {
              cell2.status = Cell.Status.PENDING;
              break;
            }
          }
        }
        adapterCell.notifyDataSetChanged();
      } else if(Singleton.INSTANCE.getPublicCellStatus() == Cell.Status.NOT_JOINED) {
        Singleton.INSTANCE.setPublicCellStatus(Cell.Status.UN_CHANGED);
        Cell cell = Singleton.INSTANCE.getTappedPublicCell();
        for(int i = 0; i < adapterCell.arrayList.size(); i++) {
          if(adapterCell.arrayList.get(i) instanceof Cell) {
            Cell cell2 = (Cell) adapterCell.arrayList.get(i);
            if(cell.cell.getObjectId().equals(cell2.cell.getObjectId())) {
              cell2.status = Cell.Status.NOT_JOINED;
              cell2.totalMembers--;
              break;
            }
          }
        }
        //newPrivateCell.status = Cell.Status.NOT_JOINED;
        //newPrivateCell.totalMembers--;
        adapterCell.notifyDataSetChanged();
      }
      if(Singleton.INSTANCE.isPublicCellDeleted2()) {
        Singleton.INSTANCE.setIsPublicCellDeleted(false);
        Cell cell = Singleton.INSTANCE.getTappedPublicCell();
        for(int i = 0; i < adapterCell.arrayList.size(); i++) {
          if(adapterCell.arrayList.get(i) instanceof Cell) {
            Cell cell2 = (Cell) adapterCell.arrayList.get(i);
            if(cell.cell.getObjectId().equals(cell2.cell.getObjectId())) {
              adapterCell.arrayList.remove(i);
              break;
            }
          }
        }
        adapterCell.notifyDataSetChanged();
      }
    }
  }

  @Override
  public void onPause() {
    super.onPause();
    SharedPreferences.Editor editor = Singleton.INSTANCE.getAppPrefs().edit();
    editor.putInt("SearchRadius", searchRadius);
    editor.commit();
  }

  private void refreshListAndSearchCell(String query) {
    page = 0;
    noMoreData = false;
    loading = true;
    if(adapterCell != null) {
      adapterCell.arrayList.clear();
      adapterCell.notifyDataSetChanged();
      publicCellsList.clear();
    }
    retrievePublicCells(query);
  }

  private void retrievePublicCells(final String query) {
    if(publicCellsList == null) {
      publicCellsList = new ArrayList<>();
    }
    if(publicCellsList.size() == 0) {
      spinner.setVisibility(View.VISIBLE);
    }
    apiCallInProgress = true;
    if(nearbySearch) {
      LogEvent.Log(TAG, "nearbySearch");
      ParseQuery<ParseObject> cellQuery = ParseQuery.getQuery("PublicCell");
      cellQuery.whereNotEqualTo("createdBy", ParseUser.getCurrentUser());
      cellQuery.include("createdBy");
      ParseGeoPoint currentLocation =
        new ParseGeoPoint(Singleton.INSTANCE.getLatitude(),
          Singleton.INSTANCE.getLongitude());
      if(metric.equals("kms")) {
        cellQuery.whereWithinKilometers("geoTag", currentLocation, searchRadius);
      } else {
        cellQuery.whereWithinMiles("geoTag", currentLocation, searchRadius);
      }
      cellQuery.addDescendingOrder("totalMembers");
      cellQuery.setLimit(LIMIT_PER_PAGE);
      cellQuery.setSkip(LIMIT_PER_PAGE * page);
      if(query != null) {
        LogEvent.Log(TAG, "queryString: " + query);
        LogEvent.Log(TAG, "queryString after trim: " + query.trim());
        String[] queryStringArr = query.trim().split(" ");
        String searchRejex = queryStringArr[0];
        for(int i = 1; i < queryStringArr.length; i++) {
          searchRejex += "|" + queryStringArr[i];
        }
        //searchRejex += ")";
        LogEvent.Log(TAG, "searchRejex: " + searchRejex);
        cellQuery.whereMatches("name", searchRejex, "i");
        // cellQuery.whereContains("name", query);
      }
      cellQuery.findInBackground(new FindCallback<ParseObject>() {
        @Override
        public void done(List<ParseObject> parseObjects, ParseException e) {
          spinner.setVisibility(View.GONE);
          apiCallInProgress = false;
          if(e == null) {
            ArrayList<Object> cellsList = new ArrayList<>();
            if(parseObjects != null) {
              for(int i = 0; i < parseObjects.size(); i++) {
                ParseObject cell = parseObjects.get(i);
                if(cell.getParseUser("createdBy") == null) {
                  continue;
                }
                Cell cl = new Cell((String) cell.get("name"), cell);
                cl.totalMembers = (int) cell.get("totalMembers");
                cl.verificationStatus = (int) cell.get("verificationStatus");
                LogEvent.Log(TAG, "verificationStatus: " + cl.verificationStatus);
                boolean isCell411RequestExist = false;
                for(int j = 0; j < cell411AlertsList4JoinedAndPendingCells.size(); j++) {
                  if(cell411AlertsList4JoinedAndPendingCells.get(j).get("cellId").
                                                                                   equals(cell
                                                                                            .getObjectId())) {
                    if(cell411AlertsList4JoinedAndPendingCells.get(j).get("status").
                                                                                     equals(
                                                                                       "PENDING")) {
                      cl.status = Cell.Status.PENDING;
                    } else {
                      cl.status = Cell.Status.JOINED;
                    }
                    cl.cell411AlertId = cell411AlertsList4JoinedAndPendingCells.
                                                                                 get(j)
                                          .getObjectId();
                    isCell411RequestExist = true;
                    break;
                  }
                }
                if(!isCell411RequestExist) {
                  cl.status = Cell.Status.NOT_JOINED;
                }
                cellsList.add(cl);
              }
              if(page == 0 && query != null) {
                if(adapterCell != null) {
                  adapterCell.arrayList.clear();
                  adapterCell.notifyDataSetChanged();
                }
                publicCellsList.clear();
              }
              if(parseObjects.size() < 10) {
                noMoreData = true;
                LogEvent.Log(TAG, "No more data");
              } else {
                LogEvent.Log(TAG, "More data available");
                page++;
              }
            }
            publicCellsList.addAll(cellsList);
            if(weakRef.get() != null && !weakRef.get().isFinishing()) {
              if(adapterCell == null) {
                if(cellsList.size() > 0) {
                  // Add footer to the list that will display a progressbar
                  cellsList.add(new Footer(getString(R.string.load_more), true));
                } else {
                  // No any cells found
                  LogEvent.Log(TAG, "No any cells found, display empty note");
                }
                adapterCell = new CellListAdapter(cellsList);
                recyclerView.setAdapter(adapterCell);
              } else {
                if(adapterCell.arrayList.size() == 0 && cellsList.size() > 0) {
                  // Add footer to the list that will display a progressbar
                  cellsList.add(new Footer(getString(R.string.load_more), true));
                  adapterCell.arrayList.addAll(adapterCell.arrayList.size(), cellsList);
                } else if(cellsList.size() > 0) {
                  adapterCell.arrayList.addAll(adapterCell.arrayList.size() - 1, cellsList);
                } else {
                  // No any cells found
                  LogEvent.Log(TAG, "No any cells found, display empty note");
                }
              }
              adapterCell.notifyDataSetChanged();
              if(noMoreData && cellsList.size() > 0) {
                //recyclerView.clearOnScrollListeners();
                LogEvent.Log(TAG, "Scroll Listener Removed");
                ((Footer) adapterCell.arrayList.get(adapterCell.arrayList.size() - 1)).
                  info = getString(R.string.no_more_cells_to_load);
                ((Footer) adapterCell.arrayList.get(adapterCell.arrayList.size() - 1)).
                  spinnerVisibility = false;
                adapterCell.notifyItemChanged(adapterCell.arrayList.size() - 1);
              }
            }
          } else {
            if(weakRef.get() != null && !weakRef.get().isFinishing()) {
              Singleton.sendToast(e);
            }
          }
        }
      });
    } else {
      LogEvent.Log(TAG, "exactMatchSearch");
      ParseQuery<ParseObject> cellQuery = ParseQuery.getQuery("PublicCell");
      cellQuery.whereNotEqualTo("createdBy", ParseUser.getCurrentUser());
      if(query != null) {
        cellQuery.whereMatches("name", "^" + query.trim() + "$", "i");
      } else {
        cellQuery.whereEqualTo("name", "");
      }
      LogEvent.Log(TAG, "query: " + query);
      cellQuery.include("createdBy");
      cellQuery.addDescendingOrder("totalMembers");
      cellQuery.setLimit(LIMIT_PER_PAGE);
      cellQuery.setSkip(LIMIT_PER_PAGE * page);
      cellQuery.findInBackground(new FindCallback<ParseObject>() {
        @Override
        public void done(List<ParseObject> parseObjects, ParseException e) {
          spinner.setVisibility(View.GONE);
          apiCallInProgress = false;
          if(e == null) {
            ArrayList<Object> cellsList = new ArrayList<>();
            if(parseObjects != null) {
              for(int i = 0; i < parseObjects.size(); i++) {
                ParseObject cell = parseObjects.get(i);
                if(cell.getParseUser("createdBy") == null) {
                  continue;
                }
                Cell cl = new Cell((String) cell.get("name"), cell);
                cl.totalMembers = (int) cell.get("totalMembers");
                cl.verificationStatus = (int) cell.get("verificationStatus");
                LogEvent.Log(TAG, "verificationStatus: " + cl.verificationStatus);
                boolean isCell411RequestExist = false;
                for(int j = 0; j < cell411AlertsList4JoinedAndPendingCells.size(); j++) {
                  if(cell411AlertsList4JoinedAndPendingCells.get(j).get("cellId").
                                                                                   equals(cell
                                                                                            .getObjectId())) {
                    if(cell411AlertsList4JoinedAndPendingCells.get(j).get("status").
                                                                                     equals(
                                                                                       "PENDING")) {
                      cl.status = Cell.Status.PENDING;
                    } else {
                      cl.status = Cell.Status.JOINED;
                    }
                    cl.cell411AlertId = cell411AlertsList4JoinedAndPendingCells.get(j).
                                                                                        getObjectId();
                    isCell411RequestExist = true;
                    break;
                  }
                }
                if(!isCell411RequestExist) {
                  cl.status = Cell.Status.NOT_JOINED;
                }
                cellsList.add(cl);
              }
              if(page == 0 && query != null && adapterCell != null) {
                adapterCell.arrayList.clear();
                adapterCell.notifyDataSetChanged();
                publicCellsList.clear();
              }
              if(parseObjects.size() < 10) {
                noMoreData = true;
                LogEvent.Log(TAG, "No more data");
              } else {
                page++;
                LogEvent.Log(TAG, "More data available");
              }
            }
            publicCellsList.addAll(cellsList);
            if(weakRef.get() != null && !weakRef.get().isFinishing()) {
              if(adapterCell == null) {
                if(cellsList.size() > 0) {
                  // Add footer to the list that will display a progressbar
                  cellsList.add(new Footer(getString(R.string.load_more), true));
                } else {
                  // No any cells found
                  LogEvent.Log(TAG, "No any cells found, display empty note");
                }
                adapterCell = new CellListAdapter(cellsList);
                recyclerView.setAdapter(adapterCell);
              } else {
                if(adapterCell.arrayList.size() == 0 && cellsList.size() > 0) {
                  // Add footer to the list that will display a progressbar
                  cellsList.add(new Footer(getString(R.string.load_more), true));
                  adapterCell.arrayList.addAll(adapterCell.arrayList.size(), cellsList);
                } else if(cellsList.size() > 0) {
                  adapterCell.arrayList.addAll(adapterCell.arrayList.size() - 1, cellsList);
                } else {
                  // No any cells found
                  LogEvent.Log(TAG, "No any cells found, display empty note");
                }
              }
              adapterCell.notifyDataSetChanged();
              if(noMoreData && cellsList.size() > 0) {
                //recyclerView.clearOnScrollListeners();
                LogEvent.Log(TAG, "Scroll Listener Removed");
                ((Footer) adapterCell.arrayList.get(adapterCell.arrayList.size() - 1)).
                  info = getString(R.string.no_more_cells_to_load);
                ((Footer) adapterCell.arrayList.get(adapterCell.arrayList.size() - 1)).
                  spinnerVisibility = false;
                adapterCell.notifyItemChanged(adapterCell.arrayList.size() - 1);
              }
            }
          } else {
            if(weakRef.get() != null && !weakRef.get().isFinishing()) {
              Singleton.sendToast(e);
            }
          }
        }
      });
    }
  }

  private void setListScrollListener(final String query) {
    // register scroll listener to check when user reaches
    // bottom of the list so that we can load more items
    //recyclerView.clearOnScrollListeners();
    recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
      @Override
      public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        if(dy > 0) { //check for scroll down
          visibleItemCount = linearLayoutManager.getChildCount();
          totalItemCount = linearLayoutManager.getItemCount();
          pastVisibleItems = linearLayoutManager.findFirstVisibleItemPosition();
          if(loading) {
            if((visibleItemCount + pastVisibleItems) >= totalItemCount) {
              loading = false;
              LogEvent.Log("...", "Last Item Wow !");
              //Do pagination.. i.e. fetch new data
              handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                  if(!noMoreData) {
                    loadMoreItems(query);
                  }
                }
              }, 300);
            }
          }
        }
      }
    });
    LogEvent.Log(TAG, "Scroll Listener Added");
  }

  /**
   * This method will load more items when user reaches bottom of the list.
   * It will set the text at the footer to "No more videos to load" when
   * there will be no more videos available.
   */
  private void loadMoreItems(String query) {
    LogEvent.Log(TAG, "loadMoreItems invoked");
    if(!apiCallInProgress) {
      if(!noMoreData) {
        retrievePublicCells(query);
      } else {
        ((Footer) adapterCell.arrayList.get(adapterCell.arrayList.size() - 1)).info =
          getString(R.string.no_more_cells_to_load);
        ((Footer) adapterCell.arrayList.get(adapterCell.arrayList.size() - 1)).spinnerVisibility =
          false;
        adapterCell.notifyItemChanged(adapterCell.arrayList.size() - 1);
      }
    }
  }

  @Override
  public boolean onQueryTextSubmit(String query) {
    hideSoftKeyboard();
    return false;
  }

  @Override
  public boolean onQueryTextChange(String newText) {
    if(cell411AlertsList4JoinedAndPendingCells == null) {
      getApplicationContext();
      Singleton.sendToast(R.string.please_wait);
      return false;
    }
    if(newText.isEmpty()) {
      searchString = null;
      refreshListAndSearchCell(searchString);
    } else {
      searchString = newText;
      refreshListAndSearchCell(searchString);
    }
    return false;
  }

  @Override
  public boolean onClose() {
    hideSoftKeyboard();
    return false;
  }

  /**
   * Used to hide the keyboard
   */
  public void hideSoftKeyboard() {
    if(getCurrentFocus() != null) {
      InputMethodManager inputMethodManager =
        (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
      inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
    }
  }

  public class CellListAdapter extends RecyclerView.Adapter<CellListAdapter.ViewHolder> {
    private final int VIEW_TYPE_CELL = 0;
    private final int VIEW_TYPE_FOOTER = 1;
    private final View.OnClickListener mOnClickListener = new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        int position = recyclerView.getChildAdapterPosition(v);
        ParseGeoPoint parseGeoPoint =
          (ParseGeoPoint) ((Cell) publicCellsList.get(position)).cell.get("geoTag");
        int cellType = ((Cell) publicCellsList.
                                                get(position)).cell.getInt("cellType");
        String description = (String) ((Cell) publicCellsList.
                                                               get(position)).cell
                                        .get("description");
        Singleton.INSTANCE.setTappedPublicCell((Cell) publicCellsList.get(position));
        Intent intent = new Intent(ExploreCellActivity.this, PublicCellMembersActivity.class);
        intent.putExtra("cellId", ((Cell) publicCellsList.get(position)).cell.getObjectId());
        intent.putExtra("cellName", ((Cell) publicCellsList.get(position)).name);
        intent.putExtra("status", ((Cell) publicCellsList.get(position)).status.toString());
        intent.putExtra("totalMembers", ((Cell) publicCellsList.get(position)).totalMembers);
        intent.putExtra("lat", parseGeoPoint.getLatitude());
        intent.putExtra("lon", parseGeoPoint.getLongitude());
        intent.putExtra("city", ((Cell) publicCellsList.get(position)).cell.getString("city"));
        intent.putExtra("cellType", cellType);
        intent.putExtra("description", description);
        startActivity(intent);
      }
    };
    public ArrayList<Object> arrayList;
    private boolean isChatEnabled;

    // Provide a suitable constructor (depends on the kind of dataset)
    public CellListAdapter(ArrayList<Object> arrayList) {
      this.arrayList = arrayList;
      isChatEnabled = getResources().getBoolean(R.bool.is_chat_enabled);
    }

    // Create new views (invoked by the layout manager)
    @Override
    public CellListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
      // create a new view
      View v = null;
      if(viewType == VIEW_TYPE_CELL) {
        v = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_public_cell, parent,
          false);
      } else if(viewType == VIEW_TYPE_FOOTER) {
        v = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_footer, parent, false);
      }
      // set the view's size, margins, paddings and layout parameters
      CellListAdapter.ViewHolder vh = new CellListAdapter.ViewHolder(v, viewType);
      if(viewType == VIEW_TYPE_CELL) {
        v.setOnClickListener(mOnClickListener);
      }
      return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int position) {
      // - get element from your dataset at this position
      // - replace the contents of the view with that element
      if(getItemViewType(position) == VIEW_TYPE_CELL) {
        final Cell cell = (Cell) arrayList.get(position);
        viewHolder.txtCellName.setText(cell.name);
        if(cell.status == Cell.Status.OWNER) {
          viewHolder.txtBtnAction.setVisibility(View.GONE);
          if(isChatEnabled) {
            viewHolder.imgChat.setVisibility(View.VISIBLE);
          }
        } else {
          if(cell.status == Cell.Status.INITIALIZING) {
            viewHolder.txtBtnAction.setVisibility(View.GONE);
            viewHolder.imgChat.setVisibility(View.GONE);
          } else {
            viewHolder.txtBtnAction.setVisibility(View.VISIBLE);
            if(cell.status == Cell.Status.JOINED) {
              viewHolder.txtBtnAction.setText(R.string.leave);
              if(cell.inProcessing) {
                viewHolder.txtBtnAction.setBackgroundResource(R.drawable.bg_cell_leave_processing);
                viewHolder.txtBtnAction.setTextColor(getResources().getColor(R.color.red_pressed));
              } else {
                viewHolder.txtBtnAction.setBackgroundResource(R.drawable.bg_cell_leave);
                viewHolder.txtBtnAction.setTextColor(getResources().getColor(R.color.red));
              }
              if(isChatEnabled) {
                viewHolder.imgChat.setVisibility(View.VISIBLE);
              }
            } else if(cell.status == Cell.Status.PENDING) {
              viewHolder.txtBtnAction.setText(R.string.pending);
              viewHolder.txtBtnAction.setTextColor(Color.WHITE);
              viewHolder.txtBtnAction.setBackgroundResource(R.drawable.bg_cell_pending);
              viewHolder.imgChat.setVisibility(View.GONE);
            } else {
              viewHolder.txtBtnAction.setText(R.string.join);
              viewHolder.txtBtnAction.setTextColor(Color.WHITE);
              if(cell.inProcessing) {
                viewHolder.txtBtnAction.setBackgroundResource(R.drawable.bg_cell_join_processing);
              } else {
                viewHolder.txtBtnAction.setBackgroundResource(R.drawable.bg_cell_join);
              }
              viewHolder.imgChat.setVisibility(View.GONE);
            }
          }
        }
        LogEvent.Log(TAG, "verificationStatus: " + cell.verificationStatus);
        if(cell.verificationStatus == 1) {
          viewHolder.imgVerified.setVisibility(View.VISIBLE);
        } else {
          viewHolder.imgVerified.setVisibility(View.GONE);
        }
        viewHolder.txtBtnAction.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View view) {
            if(!cell.inProcessing) {
              if(cell.status == Cell.Status.NOT_JOINED) {
                // Send newPrivateCell join request here
                cell.inProcessing = true;
                viewHolder.txtBtnAction.setBackgroundResource(R.drawable.bg_cell_join_processing);
                joinCell(cell, position);
              } else if(cell.status == Cell.Status.JOINED) {
                // leave newPrivateCell here
                cell.inProcessing = true;
                viewHolder.txtBtnAction.setBackgroundResource(R.drawable.bg_cell_join_processing);
                unJoinCell(cell, position);
              }
            }
          }
        });
        if(!isChatEnabled) {
          viewHolder.imgChat.setVisibility(View.GONE);
        } else {
          viewHolder.imgChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              Intent intentChat = new Intent(ExploreCellActivity.this, ChatActivity.class);
              intentChat.putExtra("entityType", ChatRoom.EntityType.PUBLIC_CELL.toString());
              intentChat.putExtra("entityObjectId", cell.cell.getObjectId());
              intentChat.putExtra("entityName", cell.name);
              startActivity(intentChat);
            }
          });
        }
      } else {
        final Footer footer = (Footer) arrayList.get(position);
        viewHolder.txtInfo.setText(footer.info);
        if(footer.spinnerVisibility) {
          viewHolder.pb.setVisibility(View.VISIBLE);
        } else {
          viewHolder.pb.setVisibility(View.GONE);
        }
      }
    }

    private void joinCell(final Cell cell, final int index) {
      // Send Cell Join Request here
      final ParseUser userCellOwner = cell.cell.getParseUser("createdBy");
      // Before sending request, check if the user has not already sent request which is pending
      ParseQuery<ParseObject> query = ParseQuery.getQuery("Cell411Alert");
      query.whereEqualTo("issuedBy", ParseUser.getCurrentUser());
      query.whereEqualTo("to", userCellOwner.getUsername());
      query.whereEqualTo("status", "PENDING");
      query.whereEqualTo("cellId", cell.cell.getObjectId());
      query.whereEqualTo("entryFor", "CR");
      query.findInBackground(new FindCallback<ParseObject>() {
        @Override
        public void done(List<ParseObject> parseObjects, ParseException e) {
          if(e == null) {
            //if (parseObjects != null && parseObjects.size() > 0) {
            // An approval request has already been sent which is pending, so display an alert to the current user
            //showAlertDialog("A friend invite was already sent to " + email + " for approval");
            //} else {
            ParseRelation relation = ParseUser.getCurrentUser().getRelation("spammedBy");
            ParseQuery parseQuery = relation.getQuery();
            parseQuery.findInBackground(new FindCallback<ParseObject>() {
              @Override
              public void done(List list, ParseException e) {
                if(e == null) {
                  if(list != null) {
                    boolean userHasSpammedCurrentUser = false;
                    for(int i = 0; i < list.size(); i++) {
                      if(((ParseUser) list.get(i)).getObjectId().equals(
                        userCellOwner.getObjectId())) {
                        userHasSpammedCurrentUser = true;
                        break;
                      }
                    }
                    if(userHasSpammedCurrentUser) {
                      showAlertDialog(getString(R.string.cannot_send_cell_join_request));
                    } else {
                      // Retrieve the new object from parse to check if it still
                      // exists or the owner has deleted
                      ParseQuery parseQuery = ParseQuery.getQuery("PublicCell");
                      parseQuery.whereEqualTo("objectId", cell.cell.getObjectId());
                      parseQuery.getFirstInBackground(new GetCallback<ParseObject>() {
                        @Override
                        public void done(ParseObject parseObject, ParseException e) {
                          if(e == null) {
                            final String name = ParseUser.getCurrentUser().get("firstName") + " " +
                                                  ParseUser.getCurrentUser().get("lastName");
                            final ParseObject cellRequestObject = new ParseObject("Cell411Alert");
                            cellRequestObject.put("issuedBy", ParseUser.getCurrentUser());
                            cellRequestObject.put("issuerFirstName", name);
                            cellRequestObject.put("to", userCellOwner.getUsername());
                            cellRequestObject.put("status", "PENDING");
                            cellRequestObject.put("entryFor", "CR");
                            cellRequestObject.put("cellId", cell.cell.getObjectId());
                            cellRequestObject.put("cellName", cell.name);
                            cellRequestObject.saveInBackground(new SaveCallback() {
                              @Override
                              public void done(ParseException e) {
                                if(e == null) {
                                  // Send notification for friend request here
                                  ParseQuery pushQuery = ParseInstallation.getQuery();
                                  pushQuery.whereEqualTo("user", userCellOwner);
                                  ParsePush push = new ParsePush();
                                  push.setQuery(pushQuery);
                                  JSONObject data = new JSONObject();
                                  try {
                                    data.put("alert",
                                      getString(R.string.notif_cell_join_request_received, name,
                                        cell.name));
                                    data.put("userId", ParseUser.getCurrentUser().
                                                                                   getObjectId());
                                    data.put("sound", "default");
                                    data.put("cellRequestObjectId",
                                      cellRequestObject.getObjectId());
                                    data.put("name", name);
                                    data.put("cellId", cell.cell.getObjectId());
                                    data.put("cellName", cell.name);
                                    data.put("alertType", "CELL_REQUEST");
                                    data.put("badge", "Increment");
                                  } catch(JSONException e1) {
                                    e1.printStackTrace();
                                  }
                                  push.setData(data);
                                  push.sendInBackground();
                                  cell.inProcessing = false;
                                  cell.status = Cell.Status.PENDING;
                                  notifyDataSetChanged();
                                  if(weakRef.get() != null && !weakRef.get().isFinishing()) {
                                    getApplicationContext();
                                    Singleton.sendToast(
                                      getString(R.string.notif_cell_join_request_sent, cell.name));
                                  }
                                } else {
                                  cell.inProcessing = false;
                                  notifyDataSetChanged();
                                  if(weakRef.get() != null && !weakRef.get().isFinishing()) {
                                    Singleton.sendToast(e);
                                  }
                                }
                              }
                            });
                          } else {
                            if(e.getCode() == 101) {
                              // Cell does not exists anymore, so show appropriate message
                              // to the user and then remove this newPrivateCell from list
                              publicCellsList.remove(index);
                              notifyDataSetChanged();
                              showCellDeletedAlertDialog();
                            } else {
                              // Seems like the list is empty
                              cell.inProcessing = false;
                              notifyDataSetChanged();
                              if(weakRef.get() != null && !weakRef.get().isFinishing()) {
                                Singleton.sendToast(e);
                              }
                            }
                          }
                        }
                      });
                    }
                  } else {
                    // Seems like the list is empty
                    cell.inProcessing = false;
                    notifyDataSetChanged();
                    LogEvent.Log("SettingsActivity", "Spam users list is null");
                  }
                } else {
                  cell.inProcessing = false;
                  notifyDataSetChanged();
                  if(weakRef.get() != null && !weakRef.get().isFinishing()) {
                    Singleton.sendToast(e);
                  }
                }
              }
            });
          } else {
            cell.inProcessing = false;
            notifyDataSetChanged();
            if(weakRef.get() != null && !weakRef.get().isFinishing()) {
              Singleton.sendToast(e);
            }
          }
        }
      });
    }

    private void showAlertDialog(String alertMessage) {
      AlertDialog.Builder alert = new AlertDialog.Builder(ExploreCellActivity.this);
      alert.setMessage(alertMessage);
      alert.setPositiveButton(R.string.dialog_btn_ok, new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialogInterface, int i) {
        }
      });
      AlertDialog dialog = alert.create();
      dialog.show();
    }

    private void unJoinCell(final Cell cell, final int index) {
      ParseQuery<ParseObject> query = ParseQuery.getQuery("Cell411Alert");
      query.whereEqualTo("objectId", cell.cell411AlertId);
      query.getFirstInBackground(new GetCallback<ParseObject>() {
        @Override
        public void done(ParseObject parseObject, ParseException e) {
          if(e == null) {
            parseObject.put("status", "LEFT");
            parseObject.saveInBackground(new SaveCallback() {
              @Override
              public void done(ParseException e) {
                if(e == null) {
                  // Retrieve the new object from parse to check if it still exists or the owner has deleted
                  ParseQuery parseQuery = ParseQuery.getQuery("PublicCell");
                  parseQuery.whereEqualTo("objectId", cell.cell.getObjectId());
                  parseQuery.getFirstInBackground(new GetCallback<ParseObject>() {
                    @Override
                    public void done(ParseObject parseObject, ParseException e) {
                      if(e == null) {
                        ParseRelation relation = parseObject.getRelation("members");
                        relation.remove(ParseUser.getCurrentUser());
                        parseObject.increment("totalMembers", -1);
                        parseObject.saveInBackground(new SaveCallback() {
                          @Override
                          public void done(ParseException e) {
                            if(e == null) {
                              cell.totalMembers--;
                              cell.inProcessing = false;
                              cell.status = Cell.Status.NOT_JOINED;
                              DataSource ds = new DataSource(ExploreCellActivity.this);
                              ds.open();
                              ds.updateIsRemovedFromChatRoom(cell.cell411AlertId, true);
                              ds.close();
                              // Stop listening to the public newPrivateCell for chat messages
                                                            /*Intent chatServiceIntent = new Intent(getActivity(), ChatService.class);
                                                            chatServiceIntent.putExtra("ChatServiceTask", ChatService.ChatServiceTask.DETACH_LISTENER);
                                                            chatServiceIntent.putExtra("entityType", ChatRoom.EntityType.PUBLIC_CELL);
                                                            chatServiceIntent.putExtra("entityObjectId", newPrivateCell.newPrivateCell.getObjectId());
                                                            chatServiceIntent.putExtra("entityCreatedAt", String.valueOf(newPrivateCell.newPrivateCell.getCreatedAt().getTime()));
                                                            chatServiceIntent.putExtra("entityName", newPrivateCell.name);
                                                            getActivity().startService(chatServiceIntent);*/
                            } else {
                              cell.inProcessing = false;
                            }
                            notifyDataSetChanged();
                          }
                        });
                      } else {
                        if(e.getCode() == 101) {
                          // Cell does not exists anymore, remove the newPrivateCell from the list
                          publicCellsList.remove(index);
                          notifyDataSetChanged();
                          DataSource ds = new DataSource(ExploreCellActivity.this);
                          ds.open();
                          ds.updateIsRemovedFromChatRoom(cell.cell411AlertId, true);
                          ds.close();
                          // Stop listening to the public newPrivateCell for chat messages
                                                    /*Intent chatServiceIntent = new Intent(getActivity(), ChatService.class);
                                                    chatServiceIntent.putExtra("ChatServiceTask", ChatService.ChatServiceTask.DETACH_LISTENER);
                                                    chatServiceIntent.putExtra("entityType", ChatRoom.EntityType.PUBLIC_CELL);
                                                    chatServiceIntent.putExtra("entityObjectId", newPrivateCell.newPrivateCell.getObjectId());
                                                    chatServiceIntent.putExtra("entityCreatedAt", String.valueOf(newPrivateCell.newPrivateCell.getCreatedAt().getTime()));
                                                    chatServiceIntent.putExtra("entityName", newPrivateCell.name);
                                                    getActivity().startService(chatServiceIntent);*/
                        } else {
                          // Seems like the list is empty
                          cell.inProcessing = false;
                          notifyDataSetChanged();
                          if(weakRef.get() != null && !weakRef.get().isFinishing()) {
                            Singleton.sendToast(e);
                          }
                        }
                      }
                    }
                  });
                } else {
                  cell.inProcessing = false;
                  notifyDataSetChanged();
                  if(weakRef.get() != null && !weakRef.get().isFinishing()) {
                    Singleton.sendToast(e);
                  }
                }
              }
            });
          } else {
            cell.inProcessing = false;
            notifyDataSetChanged();
            if(weakRef.get() != null && !weakRef.get().isFinishing()) {
              Singleton.sendToast(e);
            }
          }
        }
      });
    }

    private void showCellDeletedAlertDialog() {
      AlertDialog.Builder alert = new AlertDialog.Builder(ExploreCellActivity.this);
      alert.setMessage(R.string.dialog_message_cell_no_longer_exists);
      alert.setPositiveButton(R.string.dialog_btn_ok, new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialogInterface, int i) {
        }
      });
      AlertDialog dialog = alert.create();
      dialog.show();
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
      return arrayList.size();
    }

    @Override
    public int getItemViewType(int position) {
      super.getItemViewType(position);
      if(arrayList.get(position) instanceof Cell) {
        return VIEW_TYPE_CELL;
      } else {
        return VIEW_TYPE_FOOTER;
      }
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public class ViewHolder extends RecyclerView.ViewHolder {
      // each data item is just a string in this case
      private TextView txtCellName;
      private TextView txtBtnAction;
      private CircularImageView imgCell;
      private TextView txtTitle;
      private ImageView imgVerified;
      private ImageView imgChat;
      private TextView txtInfo;
      private ProgressBar pb;

      public ViewHolder(View view, int type) {
        super(view);
        if(type == VIEW_TYPE_CELL) {
          txtCellName = (TextView) view.findViewById(R.id.txt_cell_name);
          txtBtnAction = (TextView) view.findViewById(R.id.txt_btn_action);
          imgCell = (CircularImageView) view.findViewById(R.id.img_cell);
          imgChat = (ImageView) view.findViewById(R.id.img_chat);
          imgVerified = (ImageView) view.findViewById(R.id.img_verified);
        } else if(type == VIEW_TYPE_FOOTER) {
          txtInfo = (TextView) view.findViewById(R.id.txt_info);
          pb = (ProgressBar) view.findViewById(R.id.pb);
        }
      }
    }
  }
}