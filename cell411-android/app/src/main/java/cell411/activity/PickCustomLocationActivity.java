package cell411.activity;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.ActionBar;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.safearx.cell411.R;

import java.lang.ref.WeakReference;

import cell411.Singleton;
import cell411.utils.LogEvent;

/**
 * Created by Sachin on 7/10/2015.
 */
public class PickCustomLocationActivity extends BaseActivity implements OnMapReadyCallback {
  public static WeakReference<PickCustomLocationActivity> weakRef;
  private final String TAG = "PickCustomLocationActivity";
  private GoogleMap mMap; // Might be null if Google Play services APK is not available.

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_pic_custom_location);
    weakRef = new WeakReference<>(this);
    Singleton.INSTANCE.setCurrentActivity(this);
    // Set up the action bar.
    final ActionBar actionBar = getSupportActionBar();
    //actionBar.setDisplayHomeAsUpEnabled(true);
    actionBar.setDisplayShowHomeEnabled(true);
    TextView txtBtnPickLocation = (TextView) findViewById(R.id.txt_btn_pick_location);
    txtBtnPickLocation.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        if(mMap != null) {
          LatLng latLng = mMap.getCameraPosition().target;
          LogEvent.Log(TAG, "latlng: " + latLng.toString());
          Singleton.INSTANCE.setCustomLocation(latLng);
          finish();
        } else {
          getApplicationContext();
          Singleton.sendToast(R.string.initializing_map);
        }
      }
    });
    // Obtain the SupportMapFragment and get notified when the map is ready to be used.
    SupportMapFragment mapFragment =
      (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
    mapFragment.getMapAsync(this);
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    switch(item.getItemId()) {
      case android.R.id.home:
        finish();
        return true;
      default:
        return super.onOptionsItemSelected(item);
    }
  }

  private void setUpMap(GoogleMap googleMap) {
    googleMap.getUiSettings().setAllGesturesEnabled(true);
    googleMap.getUiSettings().setCompassEnabled(true);
    googleMap.getUiSettings().setMyLocationButtonEnabled(false);
    googleMap.getUiSettings().setRotateGesturesEnabled(true);
    googleMap.getUiSettings().setScrollGesturesEnabled(true);
    googleMap.getUiSettings().setTiltGesturesEnabled(true);
    googleMap.getUiSettings().setZoomControlsEnabled(false);
    googleMap.getUiSettings().setZoomGesturesEnabled(true);
  }

  @Override
  public void onBackPressed() {
    //super.onBackPressed();
  }

  @Override
  public void onMapReady(GoogleMap googleMap) {
    mMap = googleMap;
    setUpMap(googleMap);
    googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
      new LatLng(Singleton.INSTANCE.getLatitude(), Singleton.INSTANCE.getLongitude()),
      16f));
  }
}