package cell411.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.appcompat.app.ActionBar;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.safearx.cell411.R;

import java.lang.ref.WeakReference;

import cell411.Singleton;

import static cell411.SingletonConfig.DEFAULT_VIDEO_SIZE_HEIGHT;
import static cell411.SingletonConfig.DEFAULT_VIDEO_SIZE_WIDTH;

/**
 * Created by Sachin on 19-04-2016.
 */
public class VideoSettingsActivity extends BaseActivity {
  // FIXME:  there are resources with _fb_ in the names used for other things.
  public static WeakReference<VideoSettingsActivity> weakRef;
  private final String COLOR_ACCENT = "#69f0ae";
  private final String COLOR_GRAY_CCC = "#cccccc";
  private boolean isStreamingVideoToYouTubeChannelEnabled;
  private boolean isStreamingVideoToMyYouTubeChannelEnabled;
  private boolean isSaveVideoToLocalStorageEnabled;
  private FloatingActionButton fabStreamVideoToYoutubeChannel;
  private FloatingActionButton fabStreamVideoToMyYoutubeChannel;
  private FloatingActionButton fabSaveVideoToLocalStorage;
  private EditText etServerURL;
  private EditText etStreamNameOrKey;
  private SharedPreferences prefs;
  private String videoSizeWidth;
  private String videoSizeHeight;
  private Spinner spVideoSize;
  private String[] videoSizeWidthArray;
  private String[] videoSizeHeightArray;

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    switch(item.getItemId()) {
      case android.R.id.home:
        finish();
        return true;
      default:
        return super.onOptionsItemSelected(item);
    }
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_video_settings);
    // Set up the action bar.
    final ActionBar actionBar = getSupportActionBar();
    actionBar.setDisplayHomeAsUpEnabled(true);
    Singleton.INSTANCE.setCurrentActivity(this);
    weakRef = new WeakReference<>(this);
    prefs = Singleton.INSTANCE.getAppPrefs();
    fabStreamVideoToYoutubeChannel =
      (FloatingActionButton) findViewById(R.id.fab_stream_videos_to_youtube_channel);
    fabStreamVideoToMyYoutubeChannel =
      (FloatingActionButton) findViewById(R.id.fab_stream_videos_to_my_youtube_channel);
    fabSaveVideoToLocalStorage =
      (FloatingActionButton) findViewById(R.id.fab_save_video_to_local_storage);
    isStreamingVideoToYouTubeChannelEnabled =
      prefs.getBoolean("StreamVideoToYouTubeChannel", false);
    isStreamingVideoToMyYouTubeChannelEnabled =
      prefs.getBoolean("StreamVideoToMyYouTubeChannel", false);
    isSaveVideoToLocalStorageEnabled = prefs.getBoolean("SaveVideoToLocalStorage", false);
    String serverURL =
      prefs.getString("YTServerURL", getString(R.string.default_youtube_server_url));
    String streamName = prefs.getString("StreamName", null);
    etServerURL = (EditText) findViewById(R.id.et_server_url);
    etStreamNameOrKey = (EditText) findViewById(R.id.et_stream_key);
    etServerURL.setText(serverURL);
    if(etStreamNameOrKey != null) {
      etStreamNameOrKey.setText(streamName);
    }
    TextView txtBtnReadMore = (TextView) findViewById(R.id.txt_btn_read_more);
    txtBtnReadMore.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        showHelpForYTLiveChannelSetupAlertDialog();
      }
    });
    spVideoSize = (android.widget.Spinner) findViewById(R.id.spinner_video_size);
    String[] videoSizeArray = getResources().getStringArray(R.array.video_size);
    videoSizeWidthArray = getResources().getStringArray(R.array.video_size_width);
    videoSizeHeightArray = getResources().getStringArray(R.array.video_size_height);
    ArrayAdapter<String> adapterVideoSize =
      new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, videoSizeArray);
    adapterVideoSize.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
    spVideoSize.setAdapter(adapterVideoSize);
    videoSizeWidth = prefs.getString("VideoSizeWidth", DEFAULT_VIDEO_SIZE_WIDTH);
    videoSizeHeight = prefs.getString("VideoSizeHeight", DEFAULT_VIDEO_SIZE_HEIGHT);
    for(int i = 0; i < videoSizeArray.length; i++) {
      if(videoSizeWidthArray[i].equals(videoSizeWidth) &&
           videoSizeHeightArray[i].equals(videoSizeHeight)) {
        spVideoSize.setSelection(i);
        break;
      }
    }
    if(isStreamingVideoToYouTubeChannelEnabled) {
      fabStreamVideoToYoutubeChannel.setBackgroundTintList(
        ColorStateList.valueOf(Color.parseColor(COLOR_ACCENT)));
      fabStreamVideoToYoutubeChannel.setImageResource(R.drawable.fab_post_video_to_fb_enabled);
    } else {
      fabStreamVideoToYoutubeChannel.setBackgroundTintList(
        ColorStateList.valueOf(Color.parseColor(COLOR_GRAY_CCC)));
      fabStreamVideoToYoutubeChannel.setImageResource(R.drawable.fab_post_video_to_fb_disabled);
    }
    if(isStreamingVideoToMyYouTubeChannelEnabled) {
      fabStreamVideoToMyYoutubeChannel.setBackgroundTintList(
        ColorStateList.valueOf(Color.parseColor(COLOR_ACCENT)));
      fabStreamVideoToMyYoutubeChannel.setImageResource(R.drawable.fab_post_video_to_fb_enabled);
    } else {
      fabStreamVideoToMyYoutubeChannel.setBackgroundTintList(
        ColorStateList.valueOf(Color.parseColor(COLOR_GRAY_CCC)));
      fabStreamVideoToMyYoutubeChannel.setImageResource(R.drawable.fab_post_video_to_fb_disabled);
    }
    if(isSaveVideoToLocalStorageEnabled) {
      fabSaveVideoToLocalStorage.setBackgroundTintList(
        ColorStateList.valueOf(Color.parseColor(COLOR_ACCENT)));
      fabSaveVideoToLocalStorage.setImageResource(R.drawable.fab_post_video_to_fb_enabled);
    } else {
      fabSaveVideoToLocalStorage.setBackgroundTintList(
        ColorStateList.valueOf(Color.parseColor(COLOR_GRAY_CCC)));
      fabSaveVideoToLocalStorage.setImageResource(R.drawable.fab_post_video_to_fb_disabled);
    }
    fabStreamVideoToYoutubeChannel.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        if(isStreamingVideoToYouTubeChannelEnabled) {
          isStreamingVideoToYouTubeChannelEnabled = false;
          fabStreamVideoToYoutubeChannel.setBackgroundTintList(
            ColorStateList.valueOf(Color.parseColor(COLOR_GRAY_CCC)));
          fabStreamVideoToYoutubeChannel.setImageResource(R.drawable.fab_post_video_to_fb_disabled);
        } else {
          isStreamingVideoToYouTubeChannelEnabled = true;
          fabStreamVideoToYoutubeChannel.setBackgroundTintList(
            ColorStateList.valueOf(Color.parseColor(COLOR_ACCENT)));
          fabStreamVideoToYoutubeChannel.setImageResource(R.drawable.fab_post_video_to_fb_enabled);
        }
      }
    });
    fabStreamVideoToMyYoutubeChannel.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        if(isStreamingVideoToMyYouTubeChannelEnabled) {
          isStreamingVideoToMyYouTubeChannelEnabled = false;
          fabStreamVideoToMyYoutubeChannel.setBackgroundTintList(
            ColorStateList.valueOf(Color.parseColor(COLOR_GRAY_CCC)));
          fabStreamVideoToMyYoutubeChannel.setImageResource(
            R.drawable.fab_post_video_to_fb_disabled);
        } else {
          String serverURL = etServerURL.getText().toString().trim();
          String streamName = etStreamNameOrKey.getText().toString().trim();
          if(serverURL.isEmpty()) {
            Singleton.sendToast(R.string.please_enter_server_url);
          } else if(streamName.isEmpty()) {
            Singleton.sendToast(R.string.please_enter_stream_name_or_key);
          } else {
            isStreamingVideoToMyYouTubeChannelEnabled = true;
            fabStreamVideoToMyYoutubeChannel.setBackgroundTintList(
              ColorStateList.valueOf(Color.parseColor(COLOR_ACCENT)));
            fabStreamVideoToMyYoutubeChannel.setImageResource(
              R.drawable.fab_post_video_to_fb_enabled);
          }
        }
      }
    });
    fabSaveVideoToLocalStorage.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        if(isSaveVideoToLocalStorageEnabled) {
          isSaveVideoToLocalStorageEnabled = false;
          fabSaveVideoToLocalStorage.setBackgroundTintList(
            ColorStateList.valueOf(Color.parseColor(COLOR_GRAY_CCC)));
          fabSaveVideoToLocalStorage.setImageResource(R.drawable.fab_post_video_to_fb_disabled);
        } else {
          isSaveVideoToLocalStorageEnabled = true;
          fabSaveVideoToLocalStorage.setBackgroundTintList(
            ColorStateList.valueOf(Color.parseColor(COLOR_ACCENT)));
          fabSaveVideoToLocalStorage.setImageResource(R.drawable.fab_post_video_to_fb_enabled);
        }
      }
    });
  }

  @Override
  public void onPause() {
    super.onPause();
    SharedPreferences.Editor editor = Singleton.INSTANCE.getAppPrefs().edit();
    editor.putBoolean("StreamVideoToYouTubeChannel", isStreamingVideoToYouTubeChannelEnabled);
    editor.putBoolean("StreamVideoToMyYouTubeChannel", isStreamingVideoToMyYouTubeChannelEnabled);
    editor.putBoolean("SaveVideoToLocalStorage", isSaveVideoToLocalStorageEnabled);
    String serverURL = etServerURL.getText().toString().trim();
    String streamName = etStreamNameOrKey.getText().toString().trim();
    editor.putString("YTServerURL", serverURL);
    if(!streamName.isEmpty()) {
      editor.putString("StreamName", streamName);
    }
    int position = spVideoSize.getSelectedItemPosition();
    editor.putString("VideoSizeWidth", videoSizeWidthArray[position]);
    editor.putString("VideoSizeHeight", videoSizeHeightArray[position]);
    editor.commit();
  }

  private void showHelpForYTLiveChannelSetupAlertDialog() {
    AlertDialog.Builder alert = new AlertDialog.Builder(this);
    alert.setMessage(R.string.dialog_message_help_for_yt_live_channel_setup);
    alert.setNegativeButton(R.string.dialog_btn_done, new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int which) {
      }
    });
    alert.setPositiveButton(R.string.dialog_btn_faq, new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int which) {
        Intent intentWeb = new Intent(Intent.ACTION_VIEW);
        intentWeb.setData(Uri.parse(getString(R.string.faq_url)));
        startActivity(intentWeb);
      }
    });
    AlertDialog dialog = alert.create();
    dialog.show();
  }
}
