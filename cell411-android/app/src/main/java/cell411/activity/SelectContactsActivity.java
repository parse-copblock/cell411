package cell411.activity;

import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.SearchView;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.callback.FunctionCallback;
import com.parse.callback.SaveCallback;
import com.safearx.cell411.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Locale;

import cell411.Singleton;
import cell411.SingletonImageFactory;
import cell411.constants.LMA;
import cell411.constants.ParseKeys;
import cell411.methods.Modules;
import cell411.methods.UtilityMethods;
import cell411.models.CountryInfo;
import cell411.models.NewPrivateCell;
import cell411.models.SelectContact;
import cell411.utils.LogEvent;
import cell411.utils.MD5Util;
import cell411.widgets.CircularImageView;
import cell411.widgets.Spinner;

public class SelectContactsActivity extends BaseActivity
  implements SearchView.OnQueryTextListener, SearchView.OnCloseListener {
  private static final String TAG = "SelectContactsActivity";
  public static WeakReference<SelectContactsActivity> weakRef;
  public static NewPrivateCell newPrivateCell;
  private String countryCode;
  private ArrayList<CountryInfo> list;
  private boolean isSMSInviteEnabled;
  private SearchView searchView;
  private RecyclerView recyclerView;
  private LinearLayoutManager linearLayoutManager;
  private SelectContactListAdapter selectContactListAdapter;
  private ArrayList<SelectContact> selectContactArrayList;
  private LinearLayout llLoading;
  private TextView txtLoading;
  private Spinner spinner;
  private String cellObjectId;
  private String title;
  private JSONArray arraySelectedContacts = null;
  private MenuItem miAdd;
  private MenuItem miSend;
  private MenuItem miDone;

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    switch(item.getItemId()) {
      case android.R.id.home:
        if(cellObjectId != null) {
          finish();
        } else {
          saveSelection();
        }
        return true;
      case R.id.action_add:
        if(spinner.getVisibility() != View.VISIBLE) {
          saveNAUMembers();
        } else {
          getApplicationContext();
          Singleton.sendToast(R.string.please_wait);
        }
        return true;
      case R.id.action_send:
        if(spinner.getVisibility() != View.VISIBLE) {
          sendAlertToSelectedContact();
        } else {
          getApplicationContext();
          Singleton.sendToast(R.string.please_wait);
        }
        return true;
      case R.id.action_done:
        if(spinner.getVisibility() != View.VISIBLE) {
          saveSelection();
        } else {
          getApplicationContext();
          Singleton.sendToast(R.string.please_wait);
        }
        return true;
      default:
        return super.onOptionsItemSelected(item);
    }
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_select_contacts);
    Singleton.INSTANCE.setCurrentActivity(this);
    weakRef = new WeakReference<>(this);
    getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    spinner = (Spinner) findViewById(R.id.spinner);
    cellObjectId = getIntent().getStringExtra("cellObjectId");
    title = getIntent().getStringExtra("title");
    if(cellObjectId != null) {
      ArrayList<NewPrivateCell> newPrivateCellArrayList =
        Singleton.INSTANCE.getNewPrivateCells();
      for(int i = 0; i < newPrivateCellArrayList.size(); i++) {
        if(newPrivateCellArrayList.get(i).parseObject.getObjectId().equals(cellObjectId)) {
          newPrivateCell = newPrivateCellArrayList.get(i);
          break;
        }
      }
    } else {
      String contactArray = getIntent().getStringExtra("contactArray");
      if(contactArray != null && !contactArray.isEmpty()) {
        try {
          arraySelectedContacts = new JSONArray(contactArray);
        } catch(JSONException e) {
          e.printStackTrace();
        }
      }
    }
    recyclerView = (RecyclerView) findViewById(R.id.rv_invite);
    // use this setting to improve performance if you know that changes
    // in content do not change the layout size of the RecyclerView
    recyclerView.setHasFixedSize(true);
    // use a linear layout manager
    linearLayoutManager = new LinearLayoutManager(this);
    recyclerView.setLayoutManager(linearLayoutManager);
    llLoading = (LinearLayout) findViewById(R.id.ll_loading_text);
    txtLoading = (TextView) findViewById(R.id.txt_loading);
    searchView = (SearchView) findViewById(R.id.searchview);
    int id =
      searchView.getContext().getResources().getIdentifier("android:id/search_src_text", null,
        null);
    TextView textView = (TextView) searchView.findViewById(id);
    textView.setTextColor(Color.BLACK);
    searchView.setIconifiedByDefault(false);
    searchView.setOnQueryTextListener(this);
    searchView.setOnCloseListener(this);
    list = new ArrayList<CountryInfo>();
    UtilityMethods.initializeCountryCodeList(list,
      1,
      getResources().getBoolean(R.bool.enable_publish));
    int defaultCountryCodeIndex;
    defaultCountryCodeIndex = UtilityMethods.getDefaultCountryCodeIndex(list);
    // Use dialing code selected by the user in their profile
    countryCode = UtilityMethods.getUserCountryCode(
      ParseUser.getCurrentUser().getString(ParseKeys.MOBILE_NUMBER), list);
    if(countryCode == null) { // This might not ever be True
      countryCode = list.get(defaultCountryCodeIndex).dialingCode;
    }
    // FIXME:  We may be able to get rid of this.
    android.os.Handler handler = new android.os.Handler();
    handler.postDelayed(new Runnable() {
      @Override
      public void run() {
        retrieveContacts();
      }
    }, 200);
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    MenuInflater inflater = getMenuInflater();
    inflater.inflate(R.menu.menu_select_contact, menu);
    miAdd = menu.findItem(R.id.action_add);
    miSend = menu.findItem(R.id.action_send);
    miDone = menu.findItem(R.id.action_done);
    return super.onCreateOptionsMenu(menu);
  }

  @Override
  public boolean onPrepareOptionsMenu(Menu menu) {
    miSend.setVisible(false);
    miAdd.setVisible(false);
    miDone.setVisible(cellObjectId == null);
    return super.onPrepareOptionsMenu(menu);
  }

  private void saveNAUMembers() {
    final JSONArray jsonArray = new JSONArray();
    for(int i = 0; i < selectContactArrayList.size(); i++) {
      if(selectContactArrayList.get(i).isChecked) {
        JSONObject objNAU = new JSONObject();
        try {
          objNAU.put("name", selectContactArrayList.get(i).displayName);
          if(selectContactArrayList.get(i).phone != null &&
               !selectContactArrayList.get(i).phone.isEmpty()) {
            objNAU.put("phone", selectContactArrayList.get(i).phone);
            objNAU.put("type", 1);
          } else {
            objNAU.put("email", selectContactArrayList.get(i).email);
            objNAU.put("type", 2);
          }
        } catch(JSONException e) {
          e.printStackTrace();
        }
        jsonArray.put(objNAU);
      }
    }
    spinner.setVisibility(View.VISIBLE);
    newPrivateCell.parseObject.put("nauMembers", jsonArray);
    newPrivateCell.parseObject.saveInBackground(new SaveCallback() {
      @Override
      public void done(ParseException e) {
        spinner.setVisibility(View.GONE);
        if(e == null) {
          newPrivateCell.nauMembers = jsonArray;
          getApplicationContext();
          Singleton.sendToast(R.string.contacts_saved);
        } else {
          if(weakRef != null && !weakRef.get().isFinishing()) {
            Singleton.sendToast(e);
          }
        }
      }
    });
  }

  @Override
  protected void onPause() {
    super.onPause();
    if(cellObjectId != null) {
      final JSONArray jsonArray = new JSONArray();
      for(int i = 0; i < selectContactArrayList.size(); i++) {
        if(selectContactArrayList.get(i).isChecked) {
          JSONObject objNAU = new JSONObject();
          try {
            objNAU.put("name", selectContactArrayList.get(i).displayName);
            if(selectContactArrayList.get(i).phone != null &&
                 !selectContactArrayList.get(i).phone.isEmpty()) {
              objNAU.put("phone", selectContactArrayList.get(i).phone);
              objNAU.put("type", 1);
            } else {
              objNAU.put("email", selectContactArrayList.get(i).email);
              objNAU.put("type", 2);
            }
          } catch(JSONException e) {
            e.printStackTrace();
          }
          jsonArray.put(objNAU);
        }
      }
      newPrivateCell.nauMembers = jsonArray;
      newPrivateCell.parseObject.put("nauMembers", jsonArray);
      newPrivateCell.parseObject.saveInBackground();
    }
  }

  private void saveSelection() {
    JSONArray contactArray = new JSONArray();
    if(selectContactArrayList == null) {
      finish();
      return;
    }
    for(int i = 0; i < selectContactArrayList.size(); i++) {
      if(selectContactArrayList.get(i).isChecked) {
        JSONObject objNAU = new JSONObject();
        try {
          objNAU.put("name", selectContactArrayList.get(i).displayName);
          if(selectContactArrayList.get(i).phone != null &&
               !selectContactArrayList.get(i).phone.isEmpty()) {
            objNAU.put("phone", selectContactArrayList.get(i).phone);
            objNAU.put("type", 1);
          } else {
            objNAU.put("email", selectContactArrayList.get(i).email);
            objNAU.put("type", 2);
          }
        } catch(JSONException e) {
          e.printStackTrace();
        }
        contactArray.put(objNAU);
      }
    }
    Intent result = new Intent();
    result.putExtra("contactArray", contactArray.toString());
    setResult(RESULT_OK, result);
    finish();
  }

  private void sendAlertToSelectedContact() {
    JSONArray contactArray = new JSONArray();
    for(int i = 0; i < selectContactArrayList.size(); i++) {
      if(selectContactArrayList.get(i).isChecked) {
        JSONObject objNAU = new JSONObject();
        try {
          objNAU.put("name", selectContactArrayList.get(i).displayName);
          if(selectContactArrayList.get(i).phone != null &&
               !selectContactArrayList.get(i).phone.isEmpty()) {
            objNAU.put("phone", selectContactArrayList.get(i).phone);
            objNAU.put("type", 1);
          } else {
            objNAU.put("email", selectContactArrayList.get(i).email);
            objNAU.put("type", 2);
          }
        } catch(JSONException e) {
          e.printStackTrace();
        }
        contactArray.put(objNAU);
      }
    }
    final boolean isDispatchModeEnabled =
      Singleton.INSTANCE.getAppPrefs().getBoolean("DispatchMode", false);
    byte[] b = new byte[0];
    if(title.equals("Photo")) {
      String path = getExternalFilesDir(null).getAbsolutePath() + "/Photo/photo_alert.png";
      Bitmap bmp = BitmapFactory.decodeFile(path);
      ByteArrayOutputStream baos = new ByteArrayOutputStream();
      bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
      b = baos.toByteArray();
    }
    // send alert here
    HashMap<String, Object> params = new HashMap<>();
    params.put("contactArray", contactArray.toString());
    params.put("title", title);
    if(title.equals("Photo")) {
      params.put("isPhotoAlert", true);
      params.put("imageBytes", b);
    } else {
      params.put("isPhotoAlert", false);
    }
    if(isDispatchModeEnabled && !title.equals("Photo")) {
      params.put("dispatchMode", 1);
      params.put("lat", Singleton.INSTANCE.getCustomLocation().latitude);
      params.put("lng", Singleton.INSTANCE.getCustomLocation().longitude);
    } else {
      params.put("dispatchMode", 2);
      params.put("lat", Singleton.INSTANCE.getLatitude());
      params.put("lng", Singleton.INSTANCE.getLongitude());
    }
    params.put("clientFirmId", 1);
    params.put("isLive", getResources().getBoolean(R.bool.enable_publish));
    params.put("languageCode", getString(R.string.language_code));
    params.put("platform", "android");
    final JSONObject objLMA = new JSONObject();
    try {
      objLMA.put(LMA.PARAM_USER_ID, ParseUser.getCurrentUser().getObjectId());
      objLMA.put(LMA.PARAM_ALERT_TYPE, title);
      objLMA.put(LMA.PARAM_NOTE, ""); // not asking for additional text when sending alert to
      // Non-app user newPrivateCell
      objLMA.put(LMA.PARAM_GEO_LOCATION,
        Singleton.INSTANCE.getLatitude() + "," + Singleton.INSTANCE.getLongitude());
    } catch(JSONException e1) {
      e1.printStackTrace();
    }
    ParseCloud.callFunctionInBackground("sendSMSAndEmailAlert", params,
      new FunctionCallback<String>() {
        public void done(String result, ParseException e) {
          if(e == null) {
            LogEvent.Log(TAG, "result: " + result);
            // Alert sent successfully
            if(Modules.isWeakReferenceValid()) {
              getApplicationContext();
              Singleton.sendToast(R.string.alert_sent_successfully);
            }
          } else {
            if(Modules.isWeakReferenceValid()) {
              Singleton.sendToast(e);
            }
          }
        }
      });
    finish();
  }

  /**
   * Loads contacts list
   */
  private void retrieveContacts() {
    JSONArray arrMembers = null;
    if(cellObjectId != null) {
      arrMembers = newPrivateCell.nauMembers;
    }
    selectContactArrayList = new ArrayList<>();
    ContentResolver cr = getContentResolver();
    Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
    final ArrayList<SelectContact> contactArrayList = new ArrayList<>();
    final ArrayList<String> idList = new ArrayList<>();
    if(cur.getCount() > 0) {
      while(cur.moveToNext()) {
        //String accountName = cur.getString(cur.getColumnIndex(ContactsContract.PRIMARY_ACCOUNT_NAME));
        //String accountType = cur.getString(cur.getColumnIndex(ContactsContract.PRIMARY_ACCOUNT_TYPE));
        String contactId = cur.getString(cur.getColumnIndex(ContactsContract.Contacts._ID));
        String displayName =
          cur.getString(cur.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
        idList.add(contactId);
        contactArrayList.add(new SelectContact(contactId, displayName));
        //LogEvent.Log(TAG, "accountName: " + accountName);
        //LogEvent.Log(TAG, "accountType: " + accountType);
      }
      cur.close();
      Cursor emailCur = cr.query(ContactsContract.CommonDataKinds.Email.CONTENT_URI, null,
        ContactsContract.CommonDataKinds.Email.CONTACT_ID + " IN (" +
          TextUtils.join(",", idList) + ")", null, null);
      while(emailCur.moveToNext()) {
        // This would allow you get several email addresses
        // if the email addresses were stored in an array
        String contactId = emailCur.getString(
          emailCur.getColumnIndex(ContactsContract.CommonDataKinds.Email.CONTACT_ID));
        String email = emailCur.getString(
          emailCur.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));
        String emailType = emailCur.getString(
          emailCur.getColumnIndex(ContactsContract.CommonDataKinds.Email.TYPE));
        // check if the email does not belongs to current user
        if(!email.equalsIgnoreCase(ParseUser.getCurrentUser().getUsername()) &&
             (ParseUser.getCurrentUser().getEmail() == null ||
                !email.equalsIgnoreCase(ParseUser.getCurrentUser().getEmail()))) {
          // this email does not belongs to current user
          for(int i = 0; i < contactArrayList.size(); i++) {
            if(contactArrayList.get(i).contactId.equals(contactId)) {
              contactArrayList.get(i).email = email.toLowerCase();
              break;
            }
          }
        }
        LogEvent.Log(TAG, "Email " + email);
      }
      emailCur.close();
      if(isSMSInviteEnabled) {
        Cursor phoneCur = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
          ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " IN (" +
            TextUtils.join(",", idList) + ")", null, null);
        while(phoneCur.moveToNext()) {
          String contactId = phoneCur.getString(
            phoneCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.CONTACT_ID));
          String phone = phoneCur.getString(
            phoneCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
          phone = phone.replaceAll("[\\D]+", "");
          if(phone.length() < 7 || phone.length() > 15) { // ignore phone numbers less than 7
            // digits and greater than 15 digits
            continue;
          }
          if(phone.startsWith("00")) {
            phone = phone.substring(2, phone.length());
          } else if(phone.startsWith("0")) {
            phone = phone.substring(1, phone.length());
            phone = countryCode + phone;
          } /*else if (!phone.startsWith(countryCode)
                            && !UtilityMethods.isPhonePrecededByCountryCode(list, phone)) {
                        phone = countryCode + phone;
                    }*/ else if(!phone.startsWith(countryCode)) {
            phone = countryCode + phone;
          }
          // Above we are handling case where one can have their friends/family members
          // contact in their phone book without preceding with their country code but belongs
          // to some other country code, so we are going to append/precede the phone with the
          // current code all the time. This logic presents two bugs:
          // 1. if number starts with digits that matches with country code, in such case, app
          // will not append/precede the phone with country code.
          // 2. if number starts with the country code of some other country (of an overseas
          // friend, family or other associate), in which case since the app logic will
          // append/precede the phone with current country code making it an invalid number.
          // The second case, might not have much impact on the usage as this is an Emergency
          // app and users wont be much interested to ask help from someone sitting overseas.
          if(ParseUser.getCurrentUser().getString("mobileNumber") != null &&
               phone.equals(ParseUser.getCurrentUser().getString("mobileNumber"))) {
            // the number belongs to the current user
            continue;
          }
          for(int i = 0; i < contactArrayList.size(); i++) {
            if(contactArrayList.get(i).contactId.equals(contactId)) {
              if(contactArrayList.get(i).email == null) { // check if email is null,
                // then only put the phone in the same object
                contactArrayList.get(i).phone = phone;
              } else {
                // email is already in the matched contact, so create new object
                // with the same if but with phone instead of email
                SelectContact contact =
                  new SelectContact(contactId, contactArrayList.get(i).displayName);
                contact.phone = phone;
                contactArrayList.add(contact);
              }
              break;
            }
          }
          LogEvent.Log(TAG, "phone " + phone);
        }
        phoneCur.close();
      }
      LogEvent.Log(TAG, "contactArrayList.size(): " + contactArrayList.size());
      for(int i = 0; i < contactArrayList.size(); i++) {
        LogEvent.Log(TAG,
          "contactArrayList.get(i).displayName: " + contactArrayList.get(i).displayName);
        // if ((contactArrayList.get(i).email == null && contactArrayList.get(i).phone == null) ||
        //  (contactArrayList.get(i).displayName == null))
        // continue;
        if((contactArrayList.get(i).email == null && contactArrayList.get(i).phone == null)) {
          continue;
        } else {
          boolean duplicate = false;
          for(int j = 0; j < selectContactArrayList.size(); j++) {
            if((selectContactArrayList.get(j).email != null &&
                  selectContactArrayList.get(j).email.equalsIgnoreCase(
                    contactArrayList.get(i).email)) ||
                 (selectContactArrayList.get(j).phone != null &&
                    selectContactArrayList.get(j).phone.equalsIgnoreCase(
                      contactArrayList.get(i).phone))) {
              duplicate = true;
              break;
            }
                        /*if (contactsList.get(j).email.equalsIgnoreCase(contactArrayList.get(i).email)
                                || contactsList.get(j).phone.equals(contactArrayList.get(i).phone)) {
                            duplicate = true;
                            break;
                        }*/
          }
          if(duplicate) {
            continue;
          }
          if(arrMembers != null) {
            for(int k = 0; k < arrMembers.length(); k++) {
              try {
                if(contactArrayList.get(i).displayName.equalsIgnoreCase(
                  arrMembers.getJSONObject(k).getString("name")) &&
                     ((contactArrayList.get(i).phone != null &&
                         arrMembers.getJSONObject(k).getInt("type") == 1 &&
                         contactArrayList.get(i).phone.equals(
                           arrMembers.getJSONObject(k).getString("phone"))) ||
                        (contactArrayList.get(i).email != null &&
                           arrMembers.getJSONObject(k).getInt("type") == 2 &&
                           contactArrayList.get(i).email.equals(
                             arrMembers.getJSONObject(k).getString("email"))))) {
                  contactArrayList.get(i).isChecked = true;
                }
              } catch(JSONException e) {
                e.printStackTrace();
              }
            }
          } else if(arraySelectedContacts != null) { // If coming from AlertIssuingActivity
            for(int k = 0; k < arraySelectedContacts.length(); k++) {
              try {
                if(contactArrayList.get(i).displayName.equalsIgnoreCase(
                  arraySelectedContacts.getJSONObject(k).getString("name")) &&
                     ((contactArrayList.get(i).phone != null &&
                         arraySelectedContacts.getJSONObject(k).getInt("type") == 1 &&
                         contactArrayList.get(i).phone.equals(
                           arraySelectedContacts.getJSONObject(k).getString("phone"))) ||
                        (contactArrayList.get(i).email != null &&
                           arraySelectedContacts.getJSONObject(k).getInt("type") == 2 &&
                           contactArrayList.get(i).email.equals(
                             arraySelectedContacts.getJSONObject(k).getString("email"))))) {
                  contactArrayList.get(i).isChecked = true;
                }
              } catch(JSONException e) {
                e.printStackTrace();
              }
            }
          }
        }
        selectContactArrayList.add(contactArrayList.get(i));
      }
      LogEvent.Log(TAG, "contactsList.size(): " + contactArrayList.size());
      Collections.sort(selectContactArrayList, new Comparator<SelectContact>() {
        @Override
        public int compare(SelectContact lhs, SelectContact rhs) {
          return lhs.displayName.compareToIgnoreCase(rhs.displayName);
        }
      });
    }
    llLoading.setVisibility(View.GONE);
    displayAllData();
  }

  @Override
  public boolean onQueryTextSubmit(String query) {
    LogEvent.Log(TAG, query);
    hideSoftKeyboard();
    return false;
  }

  @Override
  public boolean onQueryTextChange(String newText) {
    LogEvent.Log(TAG, newText);
    if(selectContactArrayList != null && selectContactArrayList.size() > 0) {
      displayQueryData(newText);
    }
    return false;
  }

  @Override
  public boolean onClose() {
    LogEvent.Log(TAG, "onClose invoked");
    return false;
  }

  private void displayAllData() {
    selectContactListAdapter = new SelectContactListAdapter(selectContactArrayList, null);
    recyclerView.setAdapter(selectContactListAdapter);
  }

  private void displayQueryData(String queryText) {
    if(queryText.equals("")) {
      displayAllData();
      return;
    }
    final ArrayList<SelectContact> mList = new ArrayList<>();
    for(int i = 0; i < selectContactArrayList.size(); i++) {
      if(((SelectContact) selectContactArrayList.get(i)).displayName.
                                                                      toLowerCase(
                                                                        Locale.getDefault())
           .contains(queryText.toLowerCase())) {
        mList.add(selectContactArrayList.get(i));
      }
    }
    selectContactListAdapter = new SelectContactListAdapter(mList, queryText);
    recyclerView.setAdapter(selectContactListAdapter);
  }

  /**
   * Used to hide the keyboard
   */
  public void hideSoftKeyboard() {
    if(getCurrentFocus() != null) {
      InputMethodManager inputMethodManager =
        (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
      inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
    }
  }

  @Override
  public void onBackPressed() {
    if(cellObjectId != null) {
      super.onBackPressed();
      finish();
    } else {
      saveSelection();
    }
  }

  public class SelectContactListAdapter
    extends RecyclerView.Adapter<SelectContactListAdapter.ViewHolder> {
    private final View.OnClickListener mOnClickListener = new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        int position = recyclerView.getChildAdapterPosition(v);
      }
    };
    public ArrayList<SelectContact> arrayList;
    private String query;

    // Provide a suitable constructor (depends on the kind of data set)
    public SelectContactListAdapter(ArrayList<SelectContact> arrayList, String query) {
      this.arrayList = arrayList;
      this.query = query;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public SelectContactListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
      // create a new view
      View v =
        LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_select_contact, parent,
          false);
      SelectContactListAdapter.ViewHolder vh = new SelectContactListAdapter.ViewHolder(v, viewType);
      v.setOnClickListener(mOnClickListener);
      return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final SelectContactListAdapter.ViewHolder viewHolder,
                                 final int position) {
      // - get element from your data set at this position
      // - replace the contents of the view with that element
      final SelectContact selectContact = (SelectContact) arrayList.get(position);
      if(query == null) {
        viewHolder.txtDisplayName.setText(selectContact.displayName);
      } else {
        String name = selectContact.displayName;
        name = name.replaceAll(query, "<b>" + query + "</b>");
        viewHolder.txtDisplayName.setText(Html.fromHtml(name));
      }
      viewHolder.imgUser.setImageResource(R.drawable.ic_placeholder_user);
      if(selectContact.email != null) {
        if(query == null) {
          viewHolder.txtEmail.setText(selectContact.email);
        } else {
          String name = selectContact.email;
          name = name.replaceAll(query, "<b>" + query + "</b>");
          viewHolder.txtEmail.setText(Html.fromHtml(name));
        }
        final SelectContactListAdapter.ViewHolder finalHolder = viewHolder;
        String email = selectContact.email;
        String url = "http://www.gravatar.com/avatar/%s?d=".replace("HASH", MD5Util.md5Hex(email));
        SingletonImageFactory.getImageLoader().loadImage(url,
          new ImageLoadingListener() {
            @Override
            public void onLoadingStarted(String s, View view) {
            }

            @Override
            public void onLoadingFailed(String s, View view, FailReason failReason) {
              finalHolder.imgUser.setImageResource(R.drawable.ic_placeholder_user);
            }

            @Override
            public void onLoadingComplete(String s, View view, Bitmap bitmap) {
              finalHolder.imgUser.setImageBitmap(Singleton.getCroppedBitmap(bitmap));
            }

            @Override
            public void onLoadingCancelled(String s, View view) {
            }
          });
      } else {
        if(query == null) {
          viewHolder.txtEmail.setText(selectContact.phone);
        } else {
          String name = selectContact.phone;
          name = name.replaceAll(query, "<b>" + query + "</b>");
          viewHolder.txtEmail.setText(Html.fromHtml(name));
        }
      }
      viewHolder.cbContact.setOnCheckedChangeListener(null);
      viewHolder.cbContact.setChecked(selectContact.isChecked);
      viewHolder.cbContact.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
          selectContact.isChecked = isChecked;
        }
      });
    }

    // Return the size of your data set (invoked by the layout manager)
    @Override
    public int getItemCount() {
      return arrayList.size();
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public class ViewHolder extends RecyclerView.ViewHolder {
      // each data item is just a string in this case
      private TextView txtDisplayName;
      private CircularImageView imgUser;
      private TextView txtEmail;
      private CheckBox cbContact;

      public ViewHolder(View view, int type) {
        super(view);
        txtDisplayName = (TextView) view.findViewById(R.id.txt_display_name);
        imgUser = (CircularImageView) view.findViewById(R.id.img_user);
        txtEmail = (TextView) view.findViewById(R.id.txt_email);
        cbContact = (CheckBox) view.findViewById(R.id.cb_contact);
      }
    }
  }
}