package cell411.activity;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.SurfaceTexture;
import android.hardware.Camera;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.ResultReceiver;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.URLSpan;
import android.util.Log;
import android.view.MenuItem;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.firebase.jobdispatcher.Constraint;
import com.firebase.jobdispatcher.FirebaseJobDispatcher;
import com.firebase.jobdispatcher.GooglePlayDriver;
import com.firebase.jobdispatcher.Job;
import com.firebase.jobdispatcher.Lifetime;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseRelation;
import com.parse.ParseUser;
import com.parse.callback.GetCallback;
import com.parse.callback.GetDataCallback;
import com.parse.callback.SaveCallback;
import com.safearx.cell411.R;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import cell411.Singleton;
import cell411.constants.Constants;
import cell411.models.Cell411Alert;
import cell411.models.Cell411AlertDetail;
import cell411.models.ChatRoom;
import cell411.services.DownloadJobService;
import cell411.services.FetchAddressIntentService;
import cell411.utils.LogEvent;
import cell411.widgets.CircularImageView;

import static cell411.Singleton.sendToast;
import static cell411.SingletonConfig.DOWNLOAD_VIDEO_URL;
import static cell411.SingletonConfig.TIME_TO_LIVE_FOR_CHAT_ON_ALERTS;
import static cell411.SingletonConfig.WOWZA_BASE_URL_DVR;
import static cell411.SingletonConfig.WOWZA_BASE_URL_DVR_POSTFIX;

/**
 * Created by Sachin on 06-04-2016.
 */
public class AlertDetailActivity2 extends BaseActivity
  implements View.OnClickListener, OnMapReadyCallback {
  public static final String BROADCAST_ACTION_VIDEO_DOWNLOAD_PROGRESS =
    "com.safearx.cell411.VIDEO_DOWNLOAD_PROGRESS";
  private static final int PERMISSION_WRITE_EXTERNAL_STORAGE = 2;
  public static String TAG = "AlertDetailActivity2.java";
  public static WeakReference<AlertDetailActivity2> weakRef;
  private static long previewStartTime = 0;
  private final String COLOR_GRAY_CCC = "#cccccc";
  private Cell411AlertDetail cell411AlertDetail;
  private LinearLayout llBtnFlag;
  private boolean userFlaggingInProgress;
  private boolean userFlagged;
  private String userObjectId;
  private String firstName;
  private String lastName;
  private String username;
  private String email;
  private double lat;
  private double lng;
  private FloatingActionButton fabSaveOrDownloadOrDownloaded;
  private FloatingActionButton fabViewOrPlay;
  private FloatingActionButton fabChat;
  private FloatingActionButton fabNavigate;
  private FloatingActionButton fabDeleteVideo;
  private SurfaceHolder previewHolder = null;
  private Camera camera = null;
  private boolean inPreview = false;
  private boolean cameraConfigured = false;
  private boolean frontCameraPresent = false;
  private boolean processingCapture = false;
  private SurfaceTexture surfaceTexture;
  private AddressResultReceiver mResultReceiver;
  private TextView txtAddress;
  private TextView txtLblDownloadProgress;
  private boolean isLiveStreamingEnabled;
  private boolean isDeleteVideoEnabled;
  private Handler handler = new Handler();
  private Runnable runnable = new Runnable() {
    @Override
    public void run() {
      int camId = Camera.CameraInfo.CAMERA_FACING_FRONT;
      camera = Camera.open(camId);
      setCameraDisplayOrientation(AlertDetailActivity2.this, camId, camera);
      try {
        //camera.setPreviewDisplay(previewHolder);
        camera.setPreviewTexture(surfaceTexture);
        camera.startPreview();
        inPreview = true;
        //previewStartTime = System.currentTimeMillis();
      } catch(IOException e) {
        e.printStackTrace();
      }
      //startPreview();
    }
  };
  /**
   * Callback for when picture is taken
   */
  private Camera.PictureCallback mPicture = new Camera.PictureCallback() {
    @Override
    public void onPictureTaken(byte[] data, Camera camera) {
      LogEvent.Log("Camera", "onPictureTaken() invoked..");
      Bitmap photo = BitmapFactory.decodeByteArray(data, 0, data.length);
      Matrix matrix2 = new Matrix();
      Camera.CameraInfo info = new Camera.CameraInfo();
      Camera.getCameraInfo(0, info);
      // Perform matrix rotations/mirrors depending on camera that took the photo
      float[] mirrorY = {-1, 0, 0, 0, 1, 0, 0, 0, 1};
      Matrix matrixMirrorY = new Matrix();
      matrixMirrorY.setValues(mirrorY);
      matrix2.postConcat(matrixMirrorY);
      photo = Bitmap.createBitmap(photo, 0, 0, photo.getWidth(), photo.getHeight(), matrix2, true);
      Matrix matrix = new Matrix();
      matrix.preRotate(90, (float) photo.getWidth() / 2, (float) photo.getHeight() / 2);
      photo = Bitmap.createBitmap(photo, 0, 0, photo.getWidth(), photo.getHeight(), matrix, true);
      // save captured image
      saveImage(photo);
      restartPreview();
    }
  };

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    //setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    setContentView(R.layout.activity_alert_detail2);
    weakRef = new WeakReference<>(this);
    Singleton.INSTANCE.setCurrentActivity(this);
    cell411AlertDetail = (Cell411AlertDetail) getIntent().
                                                           getSerializableExtra(
                                                             "cell411AlertDetail");
    userObjectId = getIntent().getStringExtra("userObjectId");
    firstName = getIntent().getStringExtra("firstName");
    lastName = getIntent().getStringExtra("lastName");
    username = getIntent().getStringExtra("username");
    email = getIntent().getStringExtra("email");
    lat = getIntent().getDoubleExtra("lat", 0);
    lng = getIntent().getDoubleExtra("lng", 0);
    final String imageName = getIntent().getStringExtra("imageName");
    final CircularImageView imgUser = (CircularImageView) findViewById(R.id.img_user);
    TextView txtUserName = (TextView) findViewById(R.id.txt_user_name);
    TextView txtAlertTime = (TextView) findViewById(R.id.txt_alert_time);
    RelativeLayout rlAdditionalNote = (RelativeLayout) findViewById(R.id.rl_additional_note);
    TextView txtAdditionalNote = (TextView) findViewById(R.id.txt_additional_note);
    final TextView txtMedical = (TextView) findViewById(R.id.txt_medical);
    llBtnFlag = (LinearLayout) findViewById(R.id.rl_btn_flag);
    ImageView imgAlertType = (ImageView) findViewById(R.id.img_alert_type);
    txtAddress = (TextView) findViewById(R.id.txt_address);
    txtLblDownloadProgress = (TextView) findViewById(R.id.txt_lbl_download_progress);
    //TextView txtBtnDelete = (TextView) findViewById(R.id.txt_btn_delete);
    isDeleteVideoEnabled = Singleton.INSTANCE.getAppPrefs().getBoolean("DeleteVideo", false);
    RelativeLayout rlLive = (RelativeLayout) findViewById(R.id.rl_live);
    View viewAlertConnector = (View) findViewById(R.id.view_alert_connector);
    ImageView imgAlertHead = (ImageView) findViewById(R.id.img_alert_head);
    ImageView imgClose = (ImageView) findViewById(R.id.img_close);
    imgClose.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        finish();
      }
    });
    fabSaveOrDownloadOrDownloaded =
      (FloatingActionButton) findViewById(R.id.fab_save_or_download_or_downloaded);
    fabViewOrPlay = (FloatingActionButton) findViewById(R.id.fab_view_or_play);
    fabChat = (FloatingActionButton) findViewById(R.id.fab_chat);
    fabNavigate = (FloatingActionButton) findViewById(R.id.fab_navigate);
    fabDeleteVideo = (FloatingActionButton) findViewById(R.id.fab_delete_video);
    // fabChat.hide();
    if(!getResources().getBoolean(R.bool.is_chat_enabled)) {
      fabChat.hide();
    } else if(System.currentTimeMillis() >=
                cell411AlertDetail.millis + TIME_TO_LIVE_FOR_CHAT_ON_ALERTS) {
      // the chat is expired
      fabChat.hide();
    }
    fabSaveOrDownloadOrDownloaded.setOnClickListener(this);
    fabViewOrPlay.setOnClickListener(this);
    fabChat.setOnClickListener(this);
    fabNavigate.setOnClickListener(this);
    fabDeleteVideo.setOnClickListener(this);
    llBtnFlag.setOnClickListener(this);
    //txtBtnDelete.setOnClickListener(this);
    if(Singleton.INSTANCE.getSpammedUsersIdList().contains(userObjectId)) {
      llBtnFlag.setVisibility(View.GONE);
    } else {
      llBtnFlag.setVisibility(View.VISIBLE);
      if(Singleton.INSTANCE.getSpammingUsersIdList().contains(userObjectId)) {
        llBtnFlag.setBackgroundResource(R.drawable.bg_user_flagging);
      } else {
        llBtnFlag.setBackgroundResource(R.drawable.bg_user_flag);
      }
    }
    if(cell411AlertDetail.isDeleted != 1) {
      imgUser.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
          Intent profileImageIntent =
            new Intent(AlertDetailActivity2.this, ProfileImageActivity.class);
          profileImageIntent.putExtra("userId", userObjectId);
          profileImageIntent.putExtra("imageName", imageName);
          profileImageIntent.putExtra("email", email);
          startActivity(profileImageIntent);
        }
      });
      Singleton.INSTANCE.setImage(imgUser, userObjectId + imageName,
        cell411AlertDetail.issuerEmail);
    }
    String issuerEntity;
    if(cell411AlertDetail.selfAlert) {
      issuerEntity = getString(R.string.i);
    } else {
      if(cell411AlertDetail.isDeleted == 1) {
        issuerEntity = cell411AlertDetail.issuerFirstName;
      } else {
        issuerEntity = "<a href='profile'>" + cell411AlertDetail.issuerFirstName + "</a>";
      }
    }
    if(cell411AlertDetail.forwardedBy != null) {
      String description = getString(R.string.alert_message_forwarded, issuerEntity,
        convertEnumToString(cell411AlertDetail.alertType), cell411AlertDetail.forwardedBy);
      setTextViewHTML(txtUserName, description, imageName);
    } else if(cell411AlertDetail.cellName != null && !cell411AlertDetail.cellName.equals("")) {
      String description =
        "<b>" + issuerEntity + "</b>, " + getString(R.string.a_member_of) + " <b>" +
          cell411AlertDetail.cellName + "</b> " + getString(R.string.issued_a) + " <b>" +
          convertEnumToString(cell411AlertDetail.alertType) + "</b> " +
          Singleton.getAlertNotificationTrail();
      setTextViewHTML(txtUserName, description, imageName);
    } else {
      String description;
      if(cell411AlertDetail.selfAlert) {
        description = getString(R.string.alert_message_self,
          convertEnumToString(cell411AlertDetail.alertType));
      } else {
        description = getString(R.string.alert_message, issuerEntity,
          convertEnumToString(cell411AlertDetail.alertType));
      }
      setTextViewHTML(txtUserName, description, imageName);
    }
    txtAlertTime.setText(cell411AlertDetail.time);
    switch(cell411AlertDetail.alertType) {
      case BROKEN_CAR:
        imgAlertType.setImageResource(R.drawable.alert_broken_car);
        imgAlertType.setBackgroundResource(R.drawable.bg_alert_broken_car_icon);
        viewAlertConnector.setBackgroundColor(getResources().getColor(R.color.alert_broken_car));
        imgAlertHead.setImageResource(R.drawable.alert_head_broken_car);
        //imgUser.setBorderColor(getResources().getColor(R.color.alert_broken_car));
        break;
      case BULLIED:
        imgAlertType.setImageResource(R.drawable.alert_bullied);
        imgAlertType.setBackgroundResource(R.drawable.bg_alert_bullied_icon);
        viewAlertConnector.setBackgroundColor(getResources().getColor(R.color.alert_bullied));
        imgAlertHead.setImageResource(R.drawable.alert_head_bullied);
        //imgUser.setBorderColor(getResources().getColor(R.color.alert_bullied));
        break;
      case CRIMINAL:
        imgAlertType.setImageResource(R.drawable.alert_criminal);
        imgAlertType.setBackgroundResource(R.drawable.bg_alert_criminal_icon);
        viewAlertConnector.setBackgroundColor(getResources().getColor(R.color.alert_criminal));
        imgAlertHead.setImageResource(R.drawable.alert_head_criminal);
        //imgUser.setBorderColor(getResources().getColor(R.color.alert_criminal));
        break;
      case DANGER:
        imgAlertType.setImageResource(R.drawable.alert_danger);
        imgAlertType.setBackgroundResource(R.drawable.bg_alert_danger_icon);
        viewAlertConnector.setBackgroundColor(getResources().getColor(R.color.alert_danger));
        imgAlertHead.setImageResource(R.drawable.alert_head_danger);
        //imgUser.setBorderColor(getResources().getColor(R.color.alert_danger));
        break;
      case FIRE:
        imgAlertType.setImageResource(R.drawable.alert_fire);
        imgAlertType.setBackgroundResource(R.drawable.bg_alert_fire_icon);
        viewAlertConnector.setBackgroundColor(getResources().getColor(R.color.alert_fire));
        imgAlertHead.setImageResource(R.drawable.alert_head_fire);
        //imgUser.setBorderColor(getResources().getColor(R.color.alert_fire));
        break;
      case GENERAL:
        imgAlertType.setImageResource(R.drawable.alert_general);
        imgAlertType.setBackgroundResource(R.drawable.bg_alert_general_icon);
        viewAlertConnector.setBackgroundColor(getResources().getColor(R.color.alert_general));
        imgAlertHead.setImageResource(R.drawable.alert_head_general);
        //imgUser.setBorderColor(getResources().getColor(R.color.alert_general));
        break;
      case MEDICAL:
        imgAlertType.setImageResource(R.drawable.alert_medical);
        imgAlertType.setBackgroundResource(R.drawable.bg_alert_medical_icon);
        viewAlertConnector.setBackgroundColor(getResources().getColor(R.color.alert_medical));
        imgAlertHead.setImageResource(R.drawable.alert_head_medical);
        //imgUser.setBorderColor(getResources().getColor(R.color.alert_medical));
        break;
      case PHOTO:
        imgAlertType.setImageResource(R.drawable.alert_photo);
        imgAlertType.setBackgroundResource(R.drawable.bg_alert_photo_icon);
        viewAlertConnector.setBackgroundColor(getResources().getColor(R.color.alert_photo));
        imgAlertHead.setImageResource(R.drawable.alert_head_photo);
        //imgUser.setBorderColor(getResources().getColor(R.color.alert_photo));
        break;
      case POLICE_ARREST:
        imgAlertType.setImageResource(R.drawable.alert_police_arrest);
        imgAlertType.setBackgroundResource(R.drawable.bg_alert_police_arrest_icon);
        viewAlertConnector.setBackgroundColor(getResources().getColor(R.color.alert_police_arrest));
        imgAlertHead.setImageResource(R.drawable.alert_head_police_arrest);
        //imgUser.setBorderColor(getResources().getColor(R.color.alert_police_arrest));
        break;
      case POLICE_INTERACTION:
        imgAlertType.setImageResource(R.drawable.alert_police_interaction);
        imgAlertType.setBackgroundResource(R.drawable.bg_alert_police_interaction_icon);
        viewAlertConnector.setBackgroundColor(
          getResources().getColor(R.color.alert_police_interaction));
        imgAlertHead.setImageResource(R.drawable.alert_head_police_interaction);
        //imgUser.setBorderColor(getResources().getColor(R.color.alert_police_interaction));
        break;
      case PULLED_OVER:
        imgAlertType.setImageResource(R.drawable.alert_pulled_over);
        imgAlertType.setBackgroundResource(R.drawable.bg_alert_pulled_over_icon);
        viewAlertConnector.setBackgroundColor(getResources().getColor(R.color.alert_pulled_over));
        imgAlertHead.setImageResource(R.drawable.alert_head_pulled_over);
        //imgUser.setBorderColor(getResources().getColor(R.color.alert_pulled_over));
        break;
      case VIDEO:
        imgAlertType.setImageResource(R.drawable.alert_video);
        imgAlertType.setBackgroundResource(R.drawable.bg_alert_video_icon);
        viewAlertConnector.setBackgroundColor(getResources().getColor(R.color.alert_video));
        imgAlertHead.setImageResource(R.drawable.alert_head_video);
        //imgUser.setBorderColor(getResources().getColor(R.color.alert_video));
        break;
      case HIJACK:
        imgAlertType.setImageResource(R.drawable.alert_hijack);
        imgAlertType.setBackgroundResource(R.drawable.bg_alert_hijack_icon);
        viewAlertConnector.setBackgroundColor(getResources().getColor(R.color.alert_hijack));
        imgAlertHead.setImageResource(R.drawable.alert_head_hijack);
        //imgUser.setBorderColor(getResources().getColor(R.color.alert_hijack));
        break;
      case PANIC:
        imgAlertType.setImageResource(R.drawable.alert_panic);
        imgAlertType.setBackgroundResource(R.drawable.bg_alert_panic_icon);
        viewAlertConnector.setBackgroundColor(getResources().getColor(R.color.alert_panic));
        imgAlertHead.setImageResource(R.drawable.alert_head_panic);
        //imgUser.setBorderColor(getResources().getColor(R.color.alert_panic));
        break;
      case FALLEN:
        imgAlertType.setImageResource(R.drawable.alert_fallen);
        imgAlertType.setBackgroundResource(R.drawable.bg_alert_fallen_icon);
        viewAlertConnector.setBackgroundColor(getResources().getColor(R.color.alert_fallen));
        imgAlertHead.setImageResource(R.drawable.alert_head_fallen);
        //imgUser.setBorderColor(getResources().getColor(R.color.alert_fallen));
        break;
      case PHYSICAL_ABUSE:
        imgAlertType.setImageResource(R.drawable.alert_physical_abuse);
        imgAlertType.setBackgroundResource(R.drawable.bg_alert_physical_abuse_icon);
        viewAlertConnector.setBackgroundColor(
          getResources().getColor(R.color.alert_physical_abuse));
        imgAlertHead.setImageResource(R.drawable.alert_head_physical_abuse);
        //imgUser.setBorderColor(getResources().getColor(R.color.alert_fallen));
        break;
      case TRAPPED:
        imgAlertType.setImageResource(R.drawable.alert_trapped);
        imgAlertType.setBackgroundResource(R.drawable.bg_alert_trapped_icon);
        viewAlertConnector.setBackgroundColor(getResources().getColor(R.color.alert_trapped));
        imgAlertHead.setImageResource(R.drawable.alert_head_trapped);
        //imgUser.setBorderColor(getResources().getColor(R.color.alert_fallen));
        break;
      case CAR_ACCIDENT:
        imgAlertType.setImageResource(R.drawable.alert_car_accident);
        imgAlertType.setBackgroundResource(R.drawable.bg_alert_car_accident_icon);
        viewAlertConnector.setBackgroundColor(getResources().getColor(R.color.alert_car_accident));
        imgAlertHead.setImageResource(R.drawable.alert_head_car_accident);
        //imgUser.setBorderColor(getResources().getColor(R.color.alert_fallen));
        break;
      case NATURAL_DISASTER:
        imgAlertType.setImageResource(R.drawable.alert_natural_disaster);
        imgAlertType.setBackgroundResource(R.drawable.bg_alert_natural_disaster_icon);
        viewAlertConnector.setBackgroundColor(
          getResources().getColor(R.color.alert_natural_disaster));
        imgAlertHead.setImageResource(R.drawable.alert_head_natural_disaster);
        //imgUser.setBorderColor(getResources().getColor(R.color.alert_fallen));
        break;
      case PRE_AUTHORISATION:
        imgAlertType.setImageResource(R.drawable.alert_pre_authorisation);
        imgAlertType.setBackgroundResource(R.drawable.bg_alert_pre_authorisation_icon);
        viewAlertConnector.setBackgroundColor(
          getResources().getColor(R.color.alert_pre_authorisation));
        imgAlertHead.setImageResource(R.drawable.alert_head_pre_authorisation);
        //imgUser.setBorderColor(getResources().getColor(R.color.alert_police_interaction));
        break;
      default:
        imgAlertType.setImageResource(R.drawable.alert_un_recognized);
        imgAlertType.setBackgroundResource(R.drawable.bg_alert_un_recognized_icon);
        viewAlertConnector.setBackgroundColor(getResources().getColor(R.color.alert_un_recognized));
        imgAlertHead.setImageResource(R.drawable.alert_head_un_recognized);
        //imgUser.setBorderColor(getResources().getColor(R.color.alert_un_recognized));
        break;
    }
    if(cell411AlertDetail.additionalNote != null &&
         !cell411AlertDetail.additionalNote.equals("")) {
      txtAdditionalNote.setText(cell411AlertDetail.additionalNote);
    } else {
      rlAdditionalNote.setVisibility(View.GONE);
    }
    if(cell411AlertDetail.selfAlert) {
      llBtnFlag.setVisibility(View.GONE);
    }
    if(cell411AlertDetail.isVideo) {
      LogEvent.Log(TAG, "VIDEO ALERT");
      if(!cell411AlertDetail.status.equals("VOD")) {
        LogEvent.Log(TAG, "VOD");
        fabSaveOrDownloadOrDownloaded.setImageResource(R.drawable.fab_download_disabled);
        fabSaveOrDownloadOrDownloaded.setBackgroundTintList(ColorStateList.
                                                                            valueOf(Color
                                                                                      .parseColor(
                                                                                        COLOR_GRAY_CCC)));
        rlLive.setVisibility(View.VISIBLE);
      } else {
        fabSaveOrDownloadOrDownloaded.setImageResource(R.drawable.fab_download_enabled);
      }
      fabViewOrPlay.setImageResource(R.drawable.fab_play);
      if(!isDeleteVideoEnabled) {
        fabDeleteVideo.hide();
      }
    } else if(cell411AlertDetail.isPhoto) {
      LogEvent.Log(TAG, "PHOTO ALERT");
      fabSaveOrDownloadOrDownloaded.setImageResource(R.drawable.fab_save);
      fabViewOrPlay.setImageResource(R.drawable.fab_view);
      fabDeleteVideo.hide();
    } else {
      LogEvent.Log(TAG, "OTHER ALERT");
      fabSaveOrDownloadOrDownloaded.hide();
      fabViewOrPlay.hide();
      fabDeleteVideo.hide();
    }
    if(cell411AlertDetail.isMedical) {
      ParseQuery<ParseUser> parseQuery = ParseUser.getQuery();
      parseQuery.whereEqualTo("objectId", userObjectId);
      parseQuery.getFirstInBackground(new GetCallback<ParseUser>() {
        @Override
        public void done(ParseUser parseUser, ParseException e) {
          String medical = "";
          if(e == null) {
            String bloodType = (String) parseUser.get("bloodType");
            String allergies = (String) parseUser.get("allergies");
            String otherMedicalConditions = (String) parseUser.get("otherMedicalConditions");
            if(bloodType != null && !bloodType.isEmpty()) {
              medical += "\n" + getString(R.string.blood_type) + ": " + bloodType;
            }
            if(allergies != null && !allergies.isEmpty()) {
              medical += "\n" + getString(R.string.allergies) + ": " + allergies;
            }
            if(otherMedicalConditions != null && !otherMedicalConditions.isEmpty()) {
              medical += "\n" + getString(R.string.other_medical_conditions) + ": " +
                           otherMedicalConditions;
            }
            if(medical != null && !medical.equals("")) {
              txtMedical.setText(medical);
            } else {
              txtMedical.setVisibility(View.GONE);
            }
          } else {
            txtMedical.setVisibility(View.GONE);
          }
        }
      });
    } else {
      txtMedical.setVisibility(View.GONE);
    }
    // Obtain the SupportMapFragment and get notified when the map is ready to be used.
    SupportMapFragment mapFragment =
      (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
    mapFragment.getMapAsync(this);
    IntentFilter mDownloadReceiverIntentFilter =
      new IntentFilter(BROADCAST_ACTION_VIDEO_DOWNLOAD_PROGRESS);
    DownloadReceiver downloadReceiver = new DownloadReceiver();
    LocalBroadcastManager.getInstance(this).registerReceiver(downloadReceiver,
      mDownloadReceiverIntentFilter);
    isLiveStreamingEnabled = getResources().getBoolean(R.bool.is_live_streaming_enabled);
    if(isLiveStreamingEnabled && isDeleteVideoEnabled && cell411AlertDetail.isVideo) {
      surfaceTexture = new SurfaceTexture(10);
      //initializeCameraParams();
    }
    if(cell411AlertDetail.city != null && !cell411AlertDetail.city.equals("")) {
      txtAddress.setText(cell411AlertDetail.city);
    } else {
      mResultReceiver = new AddressResultReceiver(new Handler());
      startIntentService();
    }
  }

  protected void setTextViewHTML(TextView text, String html, String imageName) {
    CharSequence sequence = Html.fromHtml(html);
    SpannableStringBuilder strBuilder = new SpannableStringBuilder(sequence);
    URLSpan[] urls = strBuilder.getSpans(0, sequence.length(), URLSpan.class);
    for(URLSpan span : urls) {
      makeLinkClickable(strBuilder, span, imageName);
    }
    text.setText(strBuilder);
    text.setMovementMethod(LinkMovementMethod.getInstance());
  }

  protected void makeLinkClickable(SpannableStringBuilder strBuilder, final URLSpan span,
                                   final String imageName) {
    int start = strBuilder.getSpanStart(span);
    int end = strBuilder.getSpanEnd(span);
    int flags = strBuilder.getSpanFlags(span);
    ClickableSpan clickable = new ClickableSpan() {
      public void onClick(View view) {
        Intent intentUser = new Intent(AlertDetailActivity2.this, UserActivity.class);
        intentUser.putExtra("userId", userObjectId);
        intentUser.putExtra("imageName", imageName);
        intentUser.putExtra("firstName", firstName);
        intentUser.putExtra("lastName", lastName);
        intentUser.putExtra("username", username);
        intentUser.putExtra("email", email);
        intentUser.putExtra("lat", lat);
        intentUser.putExtra("lng", lng);
        startActivity(intentUser);
      }
    };
    strBuilder.setSpan(clickable, start, end, flags);
    strBuilder.removeSpan(span);
  }

  private String convertEnumToString(Cell411Alert.AlertType alertType) {
    switch(alertType) {
      case BROKEN_CAR:
        return getString(R.string.alert_broken_car);
      case BULLIED:
        return getString(R.string.alert_bullied);
      case CRIMINAL:
        return getString(R.string.alert_criminal);
      case DANGER:
        return getString(R.string.alert_danger);
      case FIRE:
        return getString(R.string.alert_fire);
      case GENERAL:
        return getString(R.string.alert_general);
      case MEDICAL:
        return getString(R.string.alert_medical);
      case PHOTO:
        return getString(R.string.alert_photo);
      case POLICE_ARREST:
        return getString(R.string.alert_police_arrest);
      case POLICE_INTERACTION:
        return getString(R.string.alert_police_interaction);
      case PULLED_OVER:
        return getString(R.string.alert_pulled_over);
      case VIDEO:
        return getString(R.string.alert_video);
      case HIJACK:
        return getString(R.string.alert_hijack);
      case PANIC:
        return getString(R.string.alert_panic);
      case FALLEN:
        return getString(R.string.alert_fallen);
      case PHYSICAL_ABUSE:
        return getString(R.string.alert_physical_abuse);
      case TRAPPED:
        return getString(R.string.alert_trapped);
      case CAR_ACCIDENT:
        return getString(R.string.alert_car_accident);
      case NATURAL_DISASTER:
        return getString(R.string.alert_natural_disaster);
      case PRE_AUTHORISATION:
        return getString(R.string.alert_pre_authorisation);
      default:
        return getString(R.string.alert_un_recognized);
    }
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    switch(item.getItemId()) {
      case android.R.id.home:
        finish();
        return true;
      default:
        return super.onOptionsItemSelected(item);
    }
  }

  private void initializeCameraParams() {
    if(ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) !=
         PackageManager.PERMISSION_GRANTED) {
      return;
    }
    int numberOfCameras = Camera.getNumberOfCameras();
    for(int i = 0; i < numberOfCameras; i++) {
      Camera.CameraInfo info = new Camera.CameraInfo();
      Camera.getCameraInfo(i, info);
      // check if front camera is available
      if(info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
        LogEvent.Log("Camera", "Front Camera found: " + i);
        frontCameraPresent = true;
        break;
      }
    }
    // check if front camera is available then show the toggle camera icon
    if(!frontCameraPresent) {
      LogEvent.Log("Camera", "Front camera not present");
    } else {
      LogEvent.Log("Camera", "Front camera present");
    }
    if(frontCameraPresent) {
      handler.postDelayed(runnable, 3000);
    }
  }

  @Override
  protected void onResume() {
    super.onResume();
    if(isLiveStreamingEnabled && isDeleteVideoEnabled && cell411AlertDetail.isVideo) {
      initializeCameraParams();
    }
  }

  @Override
  protected void onPause() {
    super.onPause();
    if(isLiveStreamingEnabled && isDeleteVideoEnabled && cell411AlertDetail.isVideo) {
      if(handler != null && runnable != null) {
        handler.removeCallbacks(runnable);
      }
      if(frontCameraPresent) {
        // stop and release camera when going to paused state
        if(inPreview) {
          camera.stopPreview();
          inPreview = false;
          LogEvent.Log("onPause", "onPause: preview stopped");
        } else {
          LogEvent.Log("onPause", "onPause: not in preview");
        }
        if(camera != null) {
          camera.release();
          LogEvent.Log("onPause", "onPause: camera released");
          camera = null;
        } else {
          LogEvent.Log("onPause", "onPause: camera is null");
        }
      }
    }
  }

  private Camera.Size getOptimalPreviewSize(int w, int h, Camera.Parameters parameters) {
    List<Camera.Size> sizes = parameters.getSupportedPreviewSizes();
    final double ASPECT_TOLERANCE = 0.1;
    double targetRatio = (double) h / w;
    if(sizes == null) {
      return null;
    }
    Camera.Size optimalSize = null;
    double minDiff = Double.MAX_VALUE;
    int targetHeight = h;
    for(Camera.Size size : sizes) {
      double ratio = (double) size.width / size.height;
      if(Math.abs(ratio - targetRatio) > ASPECT_TOLERANCE) {
        continue;
      }
      if(Math.abs(size.height - targetHeight) < minDiff) {
        optimalSize = size;
        minDiff = Math.abs(size.height - targetHeight);
      }
    }
    if(optimalSize == null) {
      minDiff = Double.MAX_VALUE;
      for(Camera.Size size : sizes) {
        if(Math.abs(size.height - targetHeight) < minDiff) {
          optimalSize = size;
          minDiff = Math.abs(size.height - targetHeight);
        }
      }
    }
    return optimalSize;
  }

  private void initPreview(int width, int height) {
    if(camera != null && previewHolder.getSurface() != null) {
      LogEvent.Log("initPreview", "initPreview: surface and camera is not null");
      try {
        camera.setPreviewDisplay(previewHolder);
      } catch(Throwable t) {
        LogEvent.Log("PrvwDemo-surfaceCallbk", "Excpton setPreviewDisplay");
      }
      if(!cameraConfigured) {
        Camera.Parameters parameters = camera.getParameters();
        Camera.Size size = getOptimalPreviewSize(width, height, parameters);
        if(size != null) {
          parameters.setPreviewSize(size.width, size.height);
          camera.setParameters(parameters);
          cameraConfigured = true;
        }
      }
    } else {
      LogEvent.Log("initPreview", "initPreview: surface or camera is null");
    }
  }

  private void startPreview() {
    if(cameraConfigured && camera != null) {
      LogEvent.Log("startPreview", "startPreview: cameraConfigured is true and camera is not null");
      camera.startPreview();
      inPreview = true;
    } else {
      LogEvent.Log("startPreview", "startPreview: cameraConfigured is false or camera is null");
    }
  }

  private void restartPreview() {
    if(inPreview) {
      camera.stopPreview();
      inPreview = false;
    }
    if(camera != null) {
      camera.release();
      camera = null;
    }
    if(frontCameraPresent) {
      int camId = Camera.CameraInfo.CAMERA_FACING_FRONT;
      camera = Camera.open(camId);
      setCameraDisplayOrientation(this, camId, camera);
      try {
        //camera.setPreviewDisplay(previewHolder);
        camera.setPreviewTexture(surfaceTexture);
        camera.startPreview();
        inPreview = true;
        processingCapture = false;
        previewStartTime = System.currentTimeMillis();
      } catch(IOException e) {
        e.printStackTrace();
      }
      //startPreview();
    }
  }

  private void setCameraDisplayOrientation(Activity activity, int cameraId, Camera camera) {
    Camera.CameraInfo info = new Camera.CameraInfo();
    Camera.getCameraInfo(cameraId, info);
    int rotation = activity.getWindowManager().getDefaultDisplay().getRotation();
    int degrees = 0;
    switch(rotation) {
      case Surface.ROTATION_0:
        degrees = 0;
        break;
      case Surface.ROTATION_90:
        degrees = 90;
        break;
      case Surface.ROTATION_180:
        degrees = 180;
        break;
      case Surface.ROTATION_270:
        degrees = 270;
        break;
    }
    int result;
    if(info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
      result = (info.orientation + degrees) % 360;
      result = (360 - result) % 360; // compensate the mirror
    } else { // back-facing
      result = (info.orientation - degrees + 360) % 360;
    }
    LogEvent.Log("Camera", "info.facing: " + info.facing);
    LogEvent.Log("Camera", "info.orientation: " + info.orientation);
    LogEvent.Log("Camera", "degrees: " + degrees + " & result: " + result);
    camera.setDisplayOrientation(result);
  }

  // save image locally
  public void saveImage(Bitmap photo) {
    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    photo.compress(Bitmap.CompressFormat.JPEG, 100, baos);
    byte[] b = baos.toByteArray();
    File pictureFile = getOutputMediaFile(getString(R.string.app_name));
    FileOutputStream fos;
    try {
      fos = new FileOutputStream(pictureFile);
      fos.write(b);
      fos.close();
    } catch(FileNotFoundException e) {
      e.printStackTrace();
    } catch(IOException e) {
      e.printStackTrace();
    }
    galleryAddPic(pictureFile);
    LogEvent.Log("Camera", "Image Saved");
    if(cell411AlertDetail.isPhoto) {
      txtLblDownloadProgress.setText(R.string.saved);
    }
  }

  private File getOutputMediaFile(String folderName) {
    // To be safe, you should check that the SDCard is mounted
    // using Environment.getExternalStorageState() before doing this.
    File mediaStorageDir =
      new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
        folderName);
    // This location works best if you want the created images to be shared
    // between applications and persist after your app has been uninstalled.
    // Create the storage directory if it does not exist
    if(!mediaStorageDir.exists()) {
      if(!mediaStorageDir.mkdirs()) {
        Log.d("Cell411", "failed to create directory");
        return null;
      }
    }
    // Create a media file name
    String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
    File mediaFile =
      new File(mediaStorageDir.getPath() + File.separator + "IMG_" + timeStamp + ".jpg");
    return mediaFile;
  }

  // add picture to the gallery
  private void galleryAddPic(File f2) {
    Intent mediaScanIntent = new Intent("android.intent.action.MEDIA_SCANNER_SCAN_FILE");
    Uri contentUri = Uri.fromFile(f2);
    mediaScanIntent.setData(contentUri);
    sendBroadcast(mediaScanIntent);
  }

  @Override
  public void onClick(View v) {
    int id = v.getId();
    switch(id) {
      case R.id.fab_view_or_play:
        if(cell411AlertDetail.isVideo) {
          openVideo();
        } else {
          openPhoto();
        }
        break;
      case R.id.fab_chat:
        openChat();
        break;
      case R.id.fab_navigate:
        openMapForNavigation();
        break;
      case R.id.rl_btn_flag:
        showFlagAlertDialog();
        break;
      case R.id.fab_save_or_download_or_downloaded:
        checkPermissionAndDownload();
        break;
      case R.id.fab_delete_video:
        deleteVideo();
        break;
    }
  }

  private void checkPermissionAndDownload() {
    if(ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) !=
         PackageManager.PERMISSION_GRANTED) {
      if(ActivityCompat.shouldShowRequestPermissionRationale(this,
        Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
        ActivityCompat.requestPermissions(this,
          new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
          PERMISSION_WRITE_EXTERNAL_STORAGE);
      } else {
        ActivityCompat.requestPermissions(this,
          new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
          PERMISSION_WRITE_EXTERNAL_STORAGE);
      }
    } else {
      if(cell411AlertDetail.isVideo) {
        downloadVideo();
      } else {
        downloadAndSaveImage();
      }
    }
  }

  @Override
  public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                         @NonNull int[] grantResults) {
    if(requestCode == PERMISSION_WRITE_EXTERNAL_STORAGE) {
      if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
        if(cell411AlertDetail.isVideo) {
          downloadVideo();
        } else {
          downloadAndSaveImage();
        }
      }
    } else {
      super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }
  }

  private void openVideo() {
    String videoLink;
        /*if (cell411AlertDetail.status.equals("VOD"))
            videoLink = Singleton.WOWZA_BASE_URL_VOD + userObjectId + "_" + cell411AlertDetail.millis;
        else
            videoLink = Singleton.WOWZA_BASE_URL_LIVE + userObjectId + "_" + cell411AlertDetail.millis;*/
    videoLink = WOWZA_BASE_URL_DVR + userObjectId + "_" + cell411AlertDetail.millis +
                  WOWZA_BASE_URL_DVR_POSTFIX;
    try {
      Intent myIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(videoLink));
      startActivity(myIntent);
    } catch(ActivityNotFoundException e) {
      sendToast(getString(R.string.video_app_not_found));
      e.printStackTrace();
    }
  }

  private void openPhoto() {
    Intent myIntent = new Intent(this, ImageScreenActivity.class);
    myIntent.putExtra("cell411AlertId", cell411AlertDetail.cell411AlertId);
    startActivity(myIntent);
  }

  private void openChat() {
    Intent intentChat = new Intent(AlertDetailActivity2.this, ChatActivity.class);
    intentChat.putExtra("entityType", ChatRoom.EntityType.ALERT.toString());
    intentChat.putExtra("entityObjectId", cell411AlertDetail.cell411AlertId);
    intentChat.putExtra("entityName", convertEnumToString(cell411AlertDetail.alertType));
    intentChat.putExtra("alertType", convertEnumToString(cell411AlertDetail.alertType));
    intentChat.putExtra("issuedBy", cell411AlertDetail.issuerFirstName);
    intentChat.putExtra("time", String.valueOf(cell411AlertDetail.millis));
    startActivity(intentChat);
  }

  private void openMapForNavigation() {
    try {
      double latitude = cell411AlertDetail.lat;
      double longitude = cell411AlertDetail.lng;
      String label = cell411AlertDetail.issuerFirstName;
      String uriBegin = "geo:" + latitude + "," + longitude;
      String query = latitude + "," + longitude + "(" + label + ")";
      String encodedQuery = Uri.encode(query);
      String uriString = uriBegin + "?q=" + encodedQuery + "&z=16";
      Uri uri = Uri.parse(uriString);
      Intent intent = new Intent(Intent.ACTION_VIEW, uri);
      startActivity(intent);
    } catch(ActivityNotFoundException e) {
      e.printStackTrace();
      sendToast(R.string.maps_app_not_installed);
    }
  }

  private void showFlagAlertDialog() {
    AlertDialog.Builder alert = new AlertDialog.Builder(this);
    alert.setMessage(getString(R.string.dialog_msg_flag_user, cell411AlertDetail.issuerFirstName));
    alert.setPositiveButton(R.string.dialog_btn_yes, new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialogInterface, int i) {
        flagUser();
      }
    });
    alert.setNegativeButton(R.string.dialog_btn_no, new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialogInterface, int i) {
      }
    });
    AlertDialog dialog = alert.create();
    dialog.show();
  }

  private void flagUser() {
    if(userFlagged || userFlaggingInProgress) {
      return;
    }
    userFlaggingInProgress = true;
    // Make an entry on both end
    llBtnFlag.setBackgroundResource(R.drawable.bg_user_flagging);
    ParseQuery<ParseUser> userQuery = ParseUser.getQuery();
    userQuery.whereEqualTo("objectId", userObjectId);
    userQuery.getFirstInBackground(new GetCallback<ParseUser>() {
      @Override
      public void done(ParseUser parseUser, ParseException e) {
        if(e == null) {
          ParseRelation<ParseUser> relation = ParseUser.getCurrentUser().getRelation("spamUsers");
          relation.add(parseUser);
          ParseUser.getCurrentUser().saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
              if(e == null) {
                ParseObject taskObject = new ParseObject("Task");
                taskObject.put("userId", userObjectId);
                taskObject.put("assigneeUserId", ParseUser.getCurrentUser().getObjectId());
                taskObject.put("task", "SPAM_ADD");
                taskObject.put("status", "PENDING");
                taskObject.saveInBackground(new SaveCallback() {
                  @Override
                  public void done(ParseException e) {
                    if(e == null) {
                      userFlaggingInProgress = false;
                      userFlagged = true;
                      Singleton.INSTANCE.getSpammedUsersIdList().add(userObjectId);
                      llBtnFlag.setVisibility(View.GONE);
                      final String text = cell411AlertDetail.issuerFirstName + " " +
                                            getString(R.string.blocked_successfully);
                      sendToast(text);
                    } else {
                      userFlaggingInProgress = false;
                      userFlagged = false;
                      llBtnFlag.setBackgroundResource(R.drawable.bg_user_flag);
                      sendToast(e);
                    }
                    Singleton.INSTANCE.getSpammingUsersIdList().remove(userObjectId);
                  }
                });
              } else {
                userFlaggingInProgress = false;
                userFlagged = false;
                Singleton.INSTANCE.getSpammingUsersIdList().remove(userObjectId);
                llBtnFlag.setBackgroundResource(R.drawable.bg_user_flag);
                sendToast(e);
              }
            }
          });
        } else {
          userFlaggingInProgress = false;
          userFlagged = false;
          Singleton.INSTANCE.getSpammingUsersIdList().remove(userObjectId);
          llBtnFlag.setBackgroundResource(R.drawable.bg_user_flag);
          sendToast(e);
        }
      }
    });
  }

  private void downloadVideo() {
    if(!cell411AlertDetail.isDownloading) {
      cell411AlertDetail.isDownloading = true;
      txtLblDownloadProgress.setVisibility(View.VISIBLE);
      dispatchJob();
    }
  }

  private void dispatchJob() {
    String videoLink =
      DOWNLOAD_VIDEO_URL + userObjectId + "_" + cell411AlertDetail.millis + ".mp4";
    // Create a new dispatcher using the Google Play driver.
    FirebaseJobDispatcher dispatcher = new FirebaseJobDispatcher(new GooglePlayDriver(this));
    Bundle myExtrasBundle = new Bundle();
    myExtrasBundle.putString("videoLink", videoLink);
    myExtrasBundle.putString("cell411AlertId", cell411AlertDetail.cell411AlertId);
    Job myJob = dispatcher.newJobBuilder().setService(
      DownloadJobService.class) // the JobService that will be called
                  .setTag("download-job-service") // uniquely identifies the job
                  .setLifetime(Lifetime.FOREVER) // persist the task across boots
                  .setConstraints(
                    Constraint.ON_ANY_NETWORK) // run this job only when the network is available
                  .setExtras(myExtrasBundle).build();
    dispatcher.mustSchedule(myJob);
  }

  private void downloadAndSaveImage() {
    if(!cell411AlertDetail.isDownloading) {
      cell411AlertDetail.isDownloading = true;
      txtLblDownloadProgress.setVisibility(View.VISIBLE);
      txtLblDownloadProgress.setText(R.string.downloading);
      ParseQuery<ParseObject> query = ParseQuery.getQuery("Cell411Alert");
      query.whereEqualTo("objectId", cell411AlertDetail.cell411AlertId);
      query.getFirstInBackground(new GetCallback<ParseObject>() {
        @Override
        public void done(ParseObject cell411AlertObject, ParseException e) {
          if(e == null) {
            ParseFile applicantResume = (ParseFile) cell411AlertObject.get("photo");
            applicantResume.getDataInBackground(new GetDataCallback() {
              public void done(byte[] data, ParseException e) {
                cell411AlertDetail.isDownloading = false;
                if(e == null) {
                  // data has the bytes for the photo
                  BitmapFactory.Options options = new BitmapFactory.Options();
                  Bitmap bmp = BitmapFactory.decodeByteArray(data, 0, data.length, options);
                  txtLblDownloadProgress.setText(R.string.saving);
                  saveImage(bmp);
                } else {
                  txtLblDownloadProgress.setText(R.string.unable_to_save);
                  sendToast(e);
                }
              }
            });
          } else {
            sendToast(e);
          }
        }
      });
    }
  }

  private void deleteVideo() {
    if(frontCameraPresent) {
      if(inPreview && ((System.currentTimeMillis() - previewStartTime) > 4000) &&
           !processingCapture) {
        processingCapture = true;
        camera.takePicture(null, null, mPicture);
        Singleton.INSTANCE.setVideoDeleted(true);
        finish();
      } else {
        sendToast(R.string.please_wait);
      }
    } else {
      LogEvent.Log("Camera", "Front camera not found");
      Singleton.INSTANCE.setVideoDeleted(true);
      finish();
    }
  }

  private void setUpMap(GoogleMap googleMap) {
    googleMap.getUiSettings().setAllGesturesEnabled(true);
    googleMap.getUiSettings().setCompassEnabled(true);
    googleMap.getUiSettings().setMyLocationButtonEnabled(false);
    googleMap.getUiSettings().setRotateGesturesEnabled(true);
    googleMap.getUiSettings().setScrollGesturesEnabled(true);
    googleMap.getUiSettings().setTiltGesturesEnabled(true);
    googleMap.getUiSettings().setZoomControlsEnabled(false);
    googleMap.getUiSettings().setZoomGesturesEnabled(true);
  }

  @Override
  public void onMapReady(GoogleMap googleMap) {
    setUpMap(googleMap);
    googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
      new LatLng(cell411AlertDetail.lat, cell411AlertDetail.lng), 16f));
    googleMap.addMarker(new MarkerOptions().position(
      new LatLng(cell411AlertDetail.lat, cell411AlertDetail.lng)).title(
      cell411AlertDetail.issuerFirstName).icon(
      BitmapDescriptorFactory.fromResource(R.drawable.ic_map_marker)));
  }

  protected void startIntentService() {
    Intent intent = new Intent(this, FetchAddressIntentService.class);
    intent.putExtra(Constants.RECEIVER, mResultReceiver);
    intent.putExtra(Constants.LOCATION_DATA_LAT, cell411AlertDetail.lat);
    intent.putExtra(Constants.LOCATION_DATA_LNG, cell411AlertDetail.lng);
    FetchAddressIntentService.enqueueWork(this, intent);
  }

  public class DownloadReceiver extends BroadcastReceiver {
    private final String TAG = "DownloadReceiver";

    public DownloadReceiver() {
      super();
    }

    @Override
    public void onReceive(Context context, Intent intent) {
      LogEvent.Log(TAG, "onReceive invoked..");
      String cell411AlertId = intent.getStringExtra("cell411AlertId");
      int progress = intent.getIntExtra("progress", 0);
      String status = intent.getStringExtra("status");
      LogEvent.Log(TAG, "status: " + status);
      if(txtLblDownloadProgress != null &&
           (txtLblDownloadProgress.getVisibility() == View.VISIBLE)) {
        if(cell411AlertDetail.cell411AlertId.equals(cell411AlertId)) {
          if(status == null) {
            cell411AlertDetail.progress = progress;
            cell411AlertDetail.isDownloading = true;
            txtLblDownloadProgress.setText(progress + "%");
          } else {
            cell411AlertDetail.isDownloading = false;
            if(status.equals("Downloaded")) {
              cell411AlertDetail.isDownloaded = true;
              txtLblDownloadProgress.setText(R.string.downloaded);
              fabSaveOrDownloadOrDownloaded.setBackgroundTintList(
                ColorStateList.valueOf(Color.parseColor(COLOR_GRAY_CCC)));
              fabSaveOrDownloadOrDownloaded.setImageResource(R.drawable.fab_download_disabled);
              Singleton.INSTANCE.addDownloadedCell411VideoAlertIdToList(cell411AlertId);
            }
          }
        }
      }
    }
  }

  class AddressResultReceiver extends ResultReceiver {
    public AddressResultReceiver(Handler handler) {
      super(handler);
    }

    @Override
    protected void onReceiveResult(int resultCode, Bundle resultData) {
      // Display the city
      if(resultCode == Constants.SUCCESS_RESULT) {
        String city = resultData.getString(Constants.RESULT_DATA_CITY);
        txtAddress.setText(city);
      } else {
        txtAddress.setText(R.string.city_not_found);
      }
    }
  }
}
