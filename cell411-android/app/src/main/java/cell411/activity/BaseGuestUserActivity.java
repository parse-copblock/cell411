package cell411.activity;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import cell411.Singleton;
import cell411.utils.LogEvent;

/**
 * Created by Sachin on 01-06-2016.
 */
public class BaseGuestUserActivity extends AppCompatActivity {
  private static final String TAG = "BaseGuestUserActivity";

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
  }

  @Override
  protected void onResume() {
    super.onResume();
    LogEvent.Log(TAG, "onResume() invoked");
    LogEvent.Log(TAG, Singleton.INSTANCE.getCurrentActivity().getClass().getName());
    if(Singleton.isWasInBackground()) {
      Singleton.setWasInBackground(false);
    }
    Singleton.setIsInForeground(true);
  }

  @Override
  protected void onPause() {
    super.onPause();
    LogEvent.Log(TAG, "onPause() invoked");
    Singleton.setIsInForeground(false);
  }
}