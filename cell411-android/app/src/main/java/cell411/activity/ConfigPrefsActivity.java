/**
 * ConfigPrefsActivity.java
 * gocoder-sdk-sampleapp
 * <p/>
 * This is sample code provided by Wowza Media Systems, LLC.  All sample code is intended to be a reference for the
 * purpose of educating developers, and is not intended to be used in any production environment.
 * <p/>
 * IN NO EVENT SHALL WOWZA MEDIA SYSTEMS, LLC BE LIABLE TO YOU OR ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
 * OR CONSEQUENTIAL DAMAGES, INCLUDING LOST PROFITS, ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION,
 * EVEN IF WOWZA MEDIA SYSTEMS, LLC HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * <p/>
 * WOWZA MEDIA SYSTEMS, LLC SPECIFICALLY DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. ALL CODE PROVIDED HEREUNDER IS PROVIDED "AS IS".
 * WOWZA MEDIA SYSTEMS, LLC HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
 * <p/>
 * Copyright © 2015 Wowza Media Systems, LLC. All rights reserved.
 */
package cell411.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;

import com.safearx.cell411.R;
import com.wowza.gocoder.sdk.api.configuration.WZMediaConfig;
import com.wowza.gocoder.sdk.api.geometry.WZSize;

import java.util.Arrays;
import java.util.List;

/**
 * Display and persist a set of application preferences
 */
public class ConfigPrefsActivity extends PreferenceActivity {
  public final static int ALL_PREFS = 0x1;
  public final static int CONNECTION_ONLY_PREFS = 0x2;
  public final static int VIDEO_AND_CONNECTION = 0x3;
  protected static boolean sFixedFrameSize = false;
  protected static boolean sFixedFrameRate = false;
  private static WZMediaConfig[] sVideoConfigs = null;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
  }

  /**
   * Populate the activity with the top-level headers.
   */
  @Override
  public void onBuildHeaders(List<Header> target) {
    Intent intent = getIntent();
    // workaround for ClassCastException bug in older Android versions
    // http://stackoverflow.com/questions/28720062/class-cast-exception-when-passing-array-of-serializables-from-one-activity-to-an
    Object[] objectArray = (Object[]) intent.getSerializableExtra("configs");
    if(objectArray != null) {
      sVideoConfigs = Arrays.copyOf(objectArray, objectArray.length, WZMediaConfig[].class);
    }
    sFixedFrameSize = intent.getBooleanExtra("fixed_frame_size", false);
    sFixedFrameRate = intent.getBooleanExtra("fixed_frame_rate", false);
    int header_resource = getIntent().getIntExtra("header_resource", -1);
    if(header_resource != -1) {
      switch(header_resource) {
        case ALL_PREFS:
          loadHeadersFromResource(R.xml.capture_pref_headers, target);
          break;
        case CONNECTION_ONLY_PREFS:
          loadHeadersFromResource(R.xml.connection_only_pref_headers, target);
          break;
        case VIDEO_AND_CONNECTION:
          loadHeadersFromResource(R.xml.video_and_connection, target);
          break;
      }
    }
  }

  @Override
  protected boolean isValidFragment(String fragmentName) {
    return true;
  }

  public static class ConnectionSettingsFragment extends PreferenceFragment {
    @Override
    public void onCreate(final Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);
      addPreferencesFromResource(R.xml.connection_preferences);
    }
  }

  public static class VideoSettingsFragment extends PreferenceFragment {
    @Override
    public void onCreate(final Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);
      addPreferencesFromResource(R.xml.video_preferences);
      ListPreference videoSizes = (ListPreference) findPreference("wz_video_size");
      EditTextPreference bitRate = (EditTextPreference) findPreference("wz_video_bitrate");
      if(sVideoConfigs == null || sVideoConfigs.length == 0 || sFixedFrameSize) {
        getPreferenceScreen().removePreference(videoSizes);
      } else {
        getPreferenceScreen().removePreference(bitRate);
        SharedPreferences sharedPreferences =
          PreferenceManager.getDefaultSharedPreferences(getActivity());
        int prefWidth = sharedPreferences.getInt("wz_video_frame_width",
          WZMediaConfig.DEFAULT_VIDEO_FRAME_WIDTH);
        int prefHeight = sharedPreferences.getInt("wz_video_frame_height",
          WZMediaConfig.DEFAULT_VIDEO_FRAME_HEIGHT);
        WZSize prefSize = new WZSize(prefWidth, prefHeight);
        int prefIndex = sVideoConfigs.length - 1;
        String[] entries = new String[sVideoConfigs.length];
        String[] entryValues = new String[sVideoConfigs.length];
        for(int i = 0; i < sVideoConfigs.length; i++) {
          entries[i] = sVideoConfigs[i].toString();
          entryValues[i] = String.valueOf(i);
          if(sVideoConfigs[i].getVideoFrameSize().equals(prefSize)) {
            prefIndex = i;
          }
        }
        videoSizes.setEntries(entries);
        videoSizes.setEntryValues(entryValues);
        videoSizes.setValueIndex(prefIndex);
        videoSizes.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
          @Override
          public boolean onPreferenceChange(Preference preference, Object o) {
            if(o instanceof String) {
              try {
                int selectedIndex = Integer.parseInt((String) o);
                if(selectedIndex >= 0 && selectedIndex < sVideoConfigs.length) {
                  WZMediaConfig selectedConfig = sVideoConfigs[selectedIndex];
                  SharedPreferences sharedPreferences =
                    PreferenceManager.getDefaultSharedPreferences(getActivity());
                  SharedPreferences.Editor prefsEditor = sharedPreferences.edit();
                  prefsEditor.putInt("wz_video_frame_width", selectedConfig.getVideoFrameWidth());
                  prefsEditor.putInt("wz_video_frame_height", selectedConfig.getVideoFrameHeight());
                  prefsEditor.putString("wz_video_bitrate",
                    String.valueOf(selectedConfig.getVideoBitRate()));
                  prefsEditor.apply();
                  return true;
                }
              } catch(NumberFormatException e) {
                // bad no. returned
              }
            }
            return false;
          }
        });
      }
      if(sFixedFrameRate) {
        EditTextPreference frameRate = (EditTextPreference) findPreference("wz_video_framerate");
        getPreferenceScreen().removePreference(frameRate);
      }
    }
  }

  public static class AudioSettingsFragment extends PreferenceFragment {
    @Override
    public void onCreate(final Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);
      addPreferencesFromResource(R.xml.audio_preferences);
    }
  }
}