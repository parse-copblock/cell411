package cell411.interfaces;

import android.location.Location;

/**
 * Created by Sachin on 24-05-2018.
 */
public interface CurrentLocationFromLocationUpdatesCallBack {
  void onCurrentLocationRetrievedFromLocationUpdates(Location location,
                                                     LastLocationCallBack.Feature feature);
}