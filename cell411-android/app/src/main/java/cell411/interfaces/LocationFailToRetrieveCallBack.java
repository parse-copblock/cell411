package cell411.interfaces;

/**
 * Created by Sachin on 24-05-2018.
 */
public interface LocationFailToRetrieveCallBack {
  void onLocationFailToRetrieve(FailureReason failureReason, LastLocationCallBack.Feature feature);

  public enum FailureReason {
    PERMISSION_DENIED, SETTINGS_REQUEST_DENIED, UNKNOWN
  }
}