package cell411.interfaces;

/**
 * Created by Sachin on 21-05-2018.
 */
public interface LocationStateChangeCallBack {
  /**
   * on Location switch triggered
   */
  void onLocationStateChanged(boolean isEnabled);
}