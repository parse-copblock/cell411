package cell411.interfaces;

/**
 * Created by Sachin on 24-05-2018.
 */
public interface LocationPermissionDeniedCallBack {
  void onLocationPermissionDenied();
}