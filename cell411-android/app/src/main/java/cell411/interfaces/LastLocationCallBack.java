package cell411.interfaces;

import android.location.Location;

/**
 * Created by Sachin on 24-05-2018.
 */
public interface LastLocationCallBack {
  void onLastLocationRetrieved(Location mLastLocation, Feature feature);

  public static enum Feature {
    PATROL_MODE, NEW_PUBLIC_CELL_ALERT, RIDE_ALERT, ISSUE_ALERT, SHOW_LOCATION_ON_MAP,
    CALCULATE_DISTANCE, SHARE_CURRENT_LOCATION, RETRIEVE_NEAR_BY_CELLS
  }
}