package cell411;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Shader;
import android.view.View;
import android.widget.ImageView;

import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Marker;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.parse.ParseUser;

import cell411.utils.LogEvent;
import cell411.utils.MD5Util;

public class SingletonImageFactory {
  final public static String TAG = SingletonImageFactory.class.getSimpleName();

  public static int GRAVATAR_SIZE = (int) (70 * Singleton.getDensity() + 0.5f);
  public static int GRAVATAR_SIZE_FULL_SCREEN = (int) (400 * Singleton.getDensity() + 0.5f);

  static ImageLoader xImageLoader;
  public static ImageLoader getImageLoader() {
    if(xImageLoader != null)
      return xImageLoader;

      if(Singleton.INSTANCE == null) {
        throw new Error("getImageLoader() called too soon!");
      }
      xImageLoader = ImageLoader.getInstance();
      if(!xImageLoader.isInited()) {
        xImageLoader.init(ImageLoaderConfiguration.createDefault(Singleton.INSTANCE));
      }
      return xImageLoader;

  }

  static public String getEmail() {
    ParseUser user = ParseUser.getCurrentUser();
    String res = user.getEmail();
    if(res == null || res.isEmpty()) {
      res = user.getUsername();
    }
    return res;
  }


  public static String getProfilePicUrl(ParseUser user) {
    return "https://s3.amazonaws.com/cell411/profile_pic/" +
             user.getObjectId() +
             user.get("imageName") +
             ".png";
  }

  ;

/*
  ParseUser user = ;
    +objectId+imageName+".png"
  String objectId = user.getObjectId();
  String imageName = user.getString("imageName");
*/

  public static void setImage(final ImageView imgUser) {
    LogEvent.Log(TAG, "setImage() invoked");
    ParseUser user1 = ParseUser.getCurrentUser();
    String url = getProfilePicUrl(user1);
    LogEvent.Log(TAG, "Loading image: " + url);
    getImageLoader().loadImage(url, new ImageLoadingListener() {
      @Override
      public void onLoadingStarted(String s, View view) {
        LogEvent.Log(TAG, "onLoadingStarted() invoked");
      }

      @Override
      public void onLoadingFailed(String s, View view, FailReason failReason) {
        LogEvent.Log(TAG, "onLoadingFailed() invoked");
        String res = user1.getEmail();
        if(res == null || res.isEmpty()) {
          res = user1.getUsername();
        }
        String email = res;
        String gravatarImageUrl = getGravatarImageUrl(email);
        LogEvent.Log(TAG, "Loading image: " + gravatarImageUrl);
        LogEvent.Log(TAG, "setImage() invoked");
        LogEvent.Log(TAG, "Loading image: " + url);
        getImageLoader().loadImage(url, new ImageLoadingListener() {
          @Override
          public void onLoadingStarted(String s1, View view1) {
            LogEvent.Log(TAG, "onLoadingStarted() invoked");
          }

          @Override
          public void onLoadingFailed(String s1, View view1, FailReason failReason1) {
            LogEvent.Log(TAG, "onLoadingFailed() invoked");
          }

          @Override
          public void onLoadingComplete(String s1, View view1, Bitmap bitmap) {
            LogEvent.Log(TAG, "onLoadingComplete() invoked");
            imgUser.setImageBitmap(Singleton.getCroppedBitmapFromGravatar(bitmap));
          }

          @Override
          public void onLoadingCancelled(String s1, View view1) {
            LogEvent.Log(TAG, "onLoadingCancelled() invoked");
          }
        });
      }

      @Override
      public void onLoadingComplete(String s, View view, Bitmap bitmap) {
        LogEvent.Log(TAG, "onLoadingComplete() invoked");
        imgUser.setImageBitmap(getCroppedBitmap(bitmap));
      }

      @Override
      public void onLoadingCancelled(String s, View view) {
        LogEvent.Log(TAG, "onLoadingCancelled() invoked");
      }
    });
  }

  
  public static void setImageOnMap(final Marker marker) {
    getImageLoader().loadImage(getProfilePicUrl(ParseUser.getCurrentUser()), new ImageLoadingListener() {
      @Override
      public void onLoadingStarted(String s, View view) {
      }

      @Override
      public void onLoadingFailed(String s, View view, FailReason failReason) {
        String email = null;
        if(ParseUser.getCurrentUser().getEmail() != null &&
             !ParseUser.getCurrentUser().getEmail().isEmpty()) {
          email = ParseUser.getCurrentUser().getEmail();
        } else {
          email = ParseUser.getCurrentUser().getUsername();
        }
        String gravatarImageUrl4Map = getGravatarUrl4Map(email);
        getImageLoader().loadImage(gravatarImageUrl4Map, new ImageLoadingListener() {
          @Override
          public void onLoadingStarted(String s, View view) {
          }

          @Override
          public void onLoadingFailed(String s, View view, FailReason failReason) {
          }

          @Override
          public void onLoadingComplete(String s, View view, Bitmap bitmap) {
            marker.setIcon(BitmapDescriptorFactory.fromBitmap(
              Singleton.getCroppedBitmapFromGravatar(bitmap)));
            //marker.setIcon(BitmapDescriptorFactory
            //        .fromBitmap(bitmap));
            marker.setInfoWindowAnchor(0.5f, 0.01f);
            marker.setAnchor(0.5f, 0.5f);
            marker.showInfoWindow();
          }

          @Override
          public void onLoadingCancelled(String s, View view) {
          }
        });
      }

      @Override
      public void onLoadingComplete(String s, View view, Bitmap bitmap) {
        marker
          .setIcon(BitmapDescriptorFactory.fromBitmap(Singleton.getCroppedBitmap4Map(bitmap)));
        marker.setInfoWindowAnchor(0.5f, 0.01f);
        marker.setAnchor(0.5f, 0.5f);
        marker.showInfoWindow();
      }

      @Override
      public void onLoadingCancelled(String s, View view) {
      }
    });
  }

  public static String getGravatarUrl4Map(String email) {
    return "http://www.gravatar.com/avatar/%s?d=" + "?d=404&s=" + GRAVATAR_SIZE;
  }

  public static void setImage(final ImageView imgUser, final String imageName, final String email) {
    String profilePicUrl = getProfilePicUrl(imageName);
    LogEvent.Log(TAG, "setImage(" + profilePicUrl + ")");
    getImageLoader().loadImage(profilePicUrl, new ImageLoadingListener() {
      @Override
      public void onLoadingStarted(String s, View view) {
      }

      @Override
      public void onLoadingFailed(String s, View view, FailReason failReason) {
        String gravatarImageUrl = getGravatarImageUrl(email);
        LogEvent.Log(TAG, "setImage(" + gravatarImageUrl + ")");
        getImageLoader().loadImage(gravatarImageUrl, new ImageLoadingListener() {
          @Override
          public void onLoadingStarted(String s, View view) {
          }

          @Override
          public void onLoadingFailed(String s, View view, FailReason failReason) {
          }

          @Override
          public void onLoadingComplete(String s, View view, Bitmap bitmap) {
            imgUser.setImageBitmap(Singleton.getCroppedBitmapFromGravatar(bitmap));
          }

          @Override
          public void onLoadingCancelled(String s, View view) {
          }
        });
      }

      @Override
      public void onLoadingComplete(String s, View view, Bitmap bitmap) {
        imgUser.setImageBitmap(Singleton.getCroppedBitmap(bitmap));
      }

      @Override
      public void onLoadingCancelled(String s, View view) {
      }
    });
  }

  public static String getGravatarImageUrl(String email) {
    String res = "http://www.gravatar.com/avatar/%s?d=";
    res = res.replace("HASH", MD5Util.md5Hex(email));
    return res;
  }

  public static String getProfilePicUrl(String imageName) {
    String res = "http://www.gravatar.com/avatar/%s?d=";
    res = res + imageName + ".png";
    return res;
  }

  public static Bitmap getCroppedBitmap4Map2(Bitmap bitmap) {
    Bitmap output;
    if(bitmap.getWidth() < bitmap.getHeight()) {
      output = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getWidth(), Bitmap.Config.ARGB_8888);
    } else {
      output = Bitmap.createBitmap(bitmap.getHeight(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
    }
    Canvas canvas = new Canvas(output);
    final int color = 0xff424242;
    final Paint paint = new Paint();
    paint.setAntiAlias(true);
    canvas.drawARGB(0, 0, 0, 0);
    paint.setColor(color);
    Rect rectDest;
    Rect rectSrc;
    if(bitmap.getWidth() < bitmap.getHeight()) {
      canvas.drawCircle(bitmap.getWidth() / 2, bitmap.getHeight() / 2, bitmap.getWidth() / 2,
        paint);
      double diff = bitmap.getHeight() - bitmap.getWidth();
      rectSrc =
        new Rect(0, (int) diff / 2, bitmap.getWidth(), bitmap.getHeight() - ((int) diff / 2));
      rectDest = new Rect(0, 0, bitmap.getWidth(), bitmap.getWidth());
    } else {
      canvas.drawCircle(bitmap.getWidth() / 2, bitmap.getHeight() / 2, bitmap.getHeight() / 2,
        paint);
      double diff = bitmap.getWidth() - bitmap.getHeight();
      rectSrc =
        new Rect((int) diff / 2, 0, bitmap.getWidth() - ((int) diff / 2), bitmap.getHeight());
      rectDest = new Rect(0, 0, bitmap.getHeight(), bitmap.getHeight());
      rectDest = new Rect(0, 0, GRAVATAR_SIZE, GRAVATAR_SIZE);
    }
    //paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_ATOP));
    canvas.drawBitmap(bitmap, rectSrc, rectDest, paint);
    BitmapShader shader;
    shader = new BitmapShader(output, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP);
    Paint paint2 = new Paint();
    paint2.setAntiAlias(true);
    paint2.setShader(shader);
    RectF rect = new RectF(0.0f, 0.0f, GRAVATAR_SIZE, GRAVATAR_SIZE);
    canvas.drawRoundRect(rect, GRAVATAR_SIZE / 2, GRAVATAR_SIZE / 2, paint2);
    return output;
  }

  public static Bitmap getCroppedCircleBitmap4Map(Bitmap bitmap) {
    Bitmap output =
      Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
    Canvas canvas = new Canvas(output);
    final int color = 0xff424242;
    final Paint paint = new Paint();
    final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
    paint.setAntiAlias(true);
    canvas.drawARGB(0, 0, 0, 0);
    paint.setColor(color);
    // canvas.drawRoundRect(rectF, roundPx, roundPx, paint);
    canvas.drawCircle(bitmap.getWidth() / 2, bitmap.getHeight() / 2, bitmap.getWidth() / 2, paint);
    paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
    canvas.drawBitmap(bitmap, rect, rect, paint);
    Bitmap _bmp = Bitmap.createScaledBitmap(output, GRAVATAR_SIZE, GRAVATAR_SIZE, false);
    return _bmp;
  }

  public static Bitmap getCroppedBitmap(Bitmap bitmap) {
    Bitmap output;
    if(bitmap.getWidth() < bitmap.getHeight()) {
      output = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getWidth(), Bitmap.Config.ARGB_8888);
    } else {
      output = Bitmap.createBitmap(bitmap.getHeight(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
    }
    Canvas canvas = new Canvas(output);
    final int color = 0xff424242;
    final Paint paint = new Paint();
    paint.setAntiAlias(true);
    canvas.drawARGB(0, 0, 0, 0);
    paint.setColor(color);
    Rect rectDest;
    Rect rectSrc;
    if(bitmap.getWidth() < bitmap.getHeight()) {
      canvas.drawCircle(bitmap.getWidth() / 2, bitmap.getHeight() / 2, bitmap.getWidth() / 2,
        paint);
      double diff = bitmap.getHeight() - bitmap.getWidth();
      rectSrc =
        new Rect(0, (int) diff / 2, bitmap.getWidth(), bitmap.getHeight() - ((int) diff / 2));
      rectDest = new Rect(0, 0, bitmap.getWidth(), bitmap.getWidth());
    } else {
      canvas.drawCircle(bitmap.getWidth() / 2, bitmap.getHeight() / 2, bitmap.getHeight() / 2,
        paint);
      double diff = bitmap.getWidth() - bitmap.getHeight();
      rectSrc =
        new Rect((int) diff / 2, 0, bitmap.getWidth() - ((int) diff / 2), bitmap.getHeight());
      rectDest = new Rect(0, 0, bitmap.getHeight(), bitmap.getHeight());
    }
    canvas.drawBitmap(bitmap, rectSrc, rectDest, null);
    return output;
  }

  public static Bitmap getCroppedBitmap4Map(Bitmap bitmap) {
    Bitmap output;
    if(bitmap.getWidth() < bitmap.getHeight()) {
      output = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getWidth(), Bitmap.Config.ARGB_8888);
    } else {
      output = Bitmap.createBitmap(bitmap.getHeight(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
    }
    Canvas canvas = new Canvas(output);
    final int color = 0xff424242;
    final Paint paint = new Paint();
    paint.setAntiAlias(true);
    canvas.drawARGB(0, 0, 0, 0);
    paint.setColor(color);
    Rect rectDest;
    Rect rectSrc;
    if(bitmap.getWidth() < bitmap.getHeight()) {
      canvas.drawCircle(bitmap.getWidth() / 2, bitmap.getHeight() / 2, bitmap.getWidth() / 2,
        paint);
      double diff = bitmap.getHeight() - bitmap.getWidth();
      rectSrc =
        new Rect(0, (int) diff / 2, bitmap.getWidth(), bitmap.getHeight() - ((int) diff / 2));
      rectDest = new Rect(0, 0, bitmap.getWidth(), bitmap.getWidth());
    } else {
      canvas.drawCircle(bitmap.getWidth() / 2, bitmap.getHeight() / 2, bitmap.getHeight() / 2,
        paint);
      double diff = bitmap.getWidth() - bitmap.getHeight();
      rectSrc =
        new Rect((int) diff / 2, 0, bitmap.getWidth() - ((int) diff / 2), bitmap.getHeight());
      rectDest = new Rect(0, 0, bitmap.getHeight(), bitmap.getHeight());
    }
    canvas.drawBitmap(bitmap, rectSrc, rectDest, null);
    return getCroppedCircleBitmap4Map(output);
  }

  public static Bitmap getCroppedBitmapFromGravatar(Bitmap bitmap) {
    Bitmap output =
      Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
    Canvas canvas = new Canvas(output);
    final int color = 0xff424242;
    final Paint paint = new Paint();
    final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
    paint.setAntiAlias(true);
    canvas.drawARGB(0, 0, 0, 0);
    paint.setColor(color);
    canvas.drawCircle(bitmap.getWidth() / 2, bitmap.getHeight() / 2, bitmap.getWidth() / 2, paint);
    paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
    canvas.drawBitmap(bitmap, rect, rect, paint);
    return output;
  }

  ;

  public String getString(int resId) {
    return Singleton.INSTANCE.getString(resId);
  }

  public Resources getResources() {
    return Singleton.INSTANCE.getResources();
  }
}
