package cell411.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.safearx.cell411.R;

import java.util.ArrayList;

import cell411.models.Cell;

/**
 * Created by Sachin on 24-05-2016.
 */
public class PublicCellListAdapter extends ArrayAdapter<Cell> {
  public ArrayList<Cell> cellsList;
  private int resource;
  private LayoutInflater inflater;
  private Context context;

  public PublicCellListAdapter(Context context, int resource, ArrayList<Cell> objects) {
    super(context, resource, objects);
    this.resource = resource;
    inflater = ((Activity) context).getLayoutInflater();
    this.context = context;
    cellsList = objects;
  }

  public int getCount() {
    return cellsList.size();
  }

  public View getView(final int position, View convertView1, ViewGroup parent) {
    View cellView = convertView1;
    ViewHolder holder = null;
    if(cellView == null) {
      holder = new ViewHolder();
      cellView = inflater.inflate(resource, null);
      holder.txtUserName = (TextView) cellView.findViewById(R.id.txt_user_name);
      holder.imgUser = (ImageView) cellView.findViewById(R.id.img_user);
      holder.imgRadio = (ImageView) cellView.findViewById(R.id.img_radio);
      holder.imgChevron = (ImageView) cellView.findViewById(R.id.img_chevron);
      cellView.setTag(holder);
    } else {
      holder = (ViewHolder) cellView.getTag();
    }
    holder.imgRadio.setVisibility(View.VISIBLE);
    holder.imgUser.setVisibility(View.GONE);
    if(cellsList.get(position).name.equals("PUBLIC CELLS")) {
      holder.txtUserName.setText(R.string.option_public_cells);
    } else if(cellsList.get(position).name.equals("GLOBAL ALERT")) {
      holder.txtUserName.setText(R.string.global_alert);
    } else if(cellsList.get(position).name.equals("ALL FRIENDS")) {
      holder.txtUserName.setText(R.string.option_all_friends);
    } else {
      holder.txtUserName.setText(cellsList.get(position).name);
    }
    holder.imgRadio.setImageResource(R.drawable.ic_radio);
    if(cellsList.get(position).cell == null) {
      if(cellsList.get(position).name.equals("PUBLIC CELLS")) {
        holder.imgChevron.setVisibility(View.VISIBLE);
        holder.imgRadio.setVisibility(View.GONE);
      } else if(cellsList.get(position).name.equals("NON APP USERS")) {
        holder.imgChevron.setVisibility(View.VISIBLE);
        holder.imgRadio.setVisibility(View.GONE);
      } else {
        holder.imgChevron.setVisibility(View.GONE);
        holder.imgRadio.setVisibility(View.VISIBLE);
      }
    } else {
      holder.imgChevron.setVisibility(View.GONE);
      holder.imgRadio.setVisibility(View.VISIBLE);
    }
    if(cellsList.get(position).selected) {
      holder.imgRadio.setImageResource(R.drawable.ic_radio_selected);
    }
    return cellView;
  }

  private static class ViewHolder {
    private TextView txtUserName;
    private ImageView imgUser;
    private ImageView imgRadio;
    private ImageView imgChevron;
  }
}