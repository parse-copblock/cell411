package cell411.db;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;

import cell411.models.ChatRoom;
import cell411.models.ChatRoomSettings;
import cell411.utils.LogEvent;

public class DataSource {
  public static final String databaseName = "cell411";
  private static String TAG = "Datasource";
  private SQLiteDatabase mydatabase;
  private MySQLHelper helper;

  public DataSource(Context context) {
    SharedPreferences preferences = context.getSharedPreferences("AppPrefs", Context.MODE_PRIVATE);
    String db = preferences.getString("UID", databaseName) + ".sqlite";
    helper = new MySQLHelper(context, db);
  }

  public void open() {
    mydatabase = helper.getWritableDatabase();
  }

  public void close() {
    helper.close();
  }

  public void insertOrUpdateChatRoom(ChatRoom chatRoom, boolean incrementMsgCount) {
    if(incrementMsgCount) {
      int count = 0;
      String[] columns = new String[]{MySQLHelper.TBL_CHAT_ROOM_MESSAGES_COUNT};
      String selection =
        MySQLHelper.TBL_CHAT_ROOM_ENTITY_OBJECT_ID + " = '" + chatRoom.entityObjectId + "'";
      Cursor cursor =
        mydatabase.query(MySQLHelper.TABLE_CHAT_ROOM, columns, selection, null, null, null, null);
      if(cursor.getCount() != 0) {
        cursor.moveToFirst();
        count =
          cursor.getInt(cursor.getColumnIndexOrThrow(MySQLHelper.TBL_CHAT_ROOM_MESSAGES_COUNT));
      }
      cursor.close();
      chatRoom.msgCount = count + 1;
    } else {
      chatRoom.msgCount = 0;
    }
    String selection2 =
      MySQLHelper.TBL_CHAT_ROOM_ENTITY_OBJECT_ID + " = '" + chatRoom.entityObjectId + "'";
    ContentValues valueChatRoom = new ContentValues();
    valueChatRoom.put(MySQLHelper.TBL_CHAT_ROOM_MESSAGE, chatRoom.msg);
    if(chatRoom.isReceived) {
      valueChatRoom.put(MySQLHelper.TBL_CHAT_ROOM_MESSAGE_INCOMING, 1);
    } else {
      valueChatRoom.put(MySQLHelper.TBL_CHAT_ROOM_MESSAGE_INCOMING, 0);
    }
    valueChatRoom.put(MySQLHelper.TBL_CHAT_ROOM_MESSAGES_COUNT, chatRoom.msgCount);
    valueChatRoom.put(MySQLHelper.TBL_CHAT_ROOM_MESSAGE_TIME, String.valueOf(chatRoom.msgTime));
    valueChatRoom.put(MySQLHelper.TBL_CHAT_ROOM_SENDER_FIRST_NAME, chatRoom.senderFirstName);
    valueChatRoom.put(MySQLHelper.TBL_CHAT_ROOM_SENDER_LAST_NAME, chatRoom.senderLastName);
    valueChatRoom.put(MySQLHelper.TBL_CHAT_ROOM_SENDER_ID, chatRoom.senderId);
    valueChatRoom.put(MySQLHelper.TBL_CHAT_ROOM_MESSAGE_TYPE, chatRoom.msgType);
    int rowsUpdated =
      mydatabase.update(MySQLHelper.TABLE_CHAT_ROOM, valueChatRoom, selection2, null);
    if(rowsUpdated == 0) {
      insertIntoChatRoom(chatRoom);
    }
  }

  public int insertIntoChatRoom(ChatRoom chatRoom) {
    ContentValues value = new ContentValues();
    value.put(MySQLHelper.TBL_CHAT_ROOM_ENTITY_TYPE_ID, chatRoom.entityTypeId);
    value.put(MySQLHelper.TBL_CHAT_ROOM_ENTITY_OBJECT_ID, chatRoom.entityObjectId);
    value.put(MySQLHelper.TBL_CHAT_ROOM_ENTITY_NAME, chatRoom.entityName);
    value.put(MySQLHelper.TBL_CHAT_ROOM_ENTITY_CREATED_AT,
      String.valueOf(chatRoom.entityCreatedAt));
    value.put(MySQLHelper.TBL_CHAT_ROOM_SENDER_ID, chatRoom.senderId);
    value.put(MySQLHelper.TBL_CHAT_ROOM_SENDER_FIRST_NAME, chatRoom.senderFirstName);
    value.put(MySQLHelper.TBL_CHAT_ROOM_SENDER_LAST_NAME, chatRoom.senderLastName);
    value.put(MySQLHelper.TBL_CHAT_ROOM_MESSAGE, chatRoom.msg);
    value.put(MySQLHelper.TBL_CHAT_ROOM_MESSAGE_TYPE, chatRoom.msgType);
    value.put(MySQLHelper.TBL_CHAT_ROOM_MESSAGES_COUNT, chatRoom.msgCount);
    value.put(MySQLHelper.TBL_CHAT_ROOM_MESSAGE_TIME, String.valueOf(chatRoom.msgTime));
    if(chatRoom.isReceived) {
      value.put(MySQLHelper.TBL_CHAT_ROOM_MESSAGE_INCOMING, 1);
    } else {
      value.put(MySQLHelper.TBL_CHAT_ROOM_MESSAGE_INCOMING, 0);
    }
    long insert_id = mydatabase.insertWithOnConflict(MySQLHelper.TABLE_CHAT_ROOM, null, value,
      SQLiteDatabase.CONFLICT_IGNORE);
    Log.i(TAG, "Added to chat_room " + insert_id);
    return (int) insert_id;
  }

  public void updateEntityCreatedAt(String entityObjectId, String entityCreatedAt) {
    LogEvent.Log(TAG, "updateEntityCreatedAt() invoked");
    mydatabase.execSQL("UPDATE " + MySQLHelper.TABLE_CHAT_ROOM + " SET " +
                         MySQLHelper.TBL_CHAT_ROOM_ENTITY_CREATED_AT + " = " + entityCreatedAt +
                         " WHERE " +
                         MySQLHelper.TBL_CHAT_ROOM_ENTITY_OBJECT_ID + " = '" + entityObjectId +
                         "'");
  }

  public int getChatRoomId(String entityObjectId) {
    String selection = MySQLHelper.TBL_CHAT_ROOM_ENTITY_OBJECT_ID + " = '" + entityObjectId + "'";
    String[] columns = new String[]{MySQLHelper.PK};
    Cursor cursor =
      mydatabase.query(MySQLHelper.TABLE_CHAT_ROOM, columns, selection, null, null, null, null);
    int personId = 0;
    if(cursor.getCount() != 0) {
      cursor.moveToFirst();
      personId = cursor.getInt(cursor.getColumnIndexOrThrow(MySQLHelper.PK));
    }
    cursor.close();
    return personId;
  }

  public void updateUnreadMessages(String entityObjectId) {
    mydatabase.execSQL("UPDATE " + MySQLHelper.TABLE_CHAT_ROOM + " SET " +
                         MySQLHelper.TBL_CHAT_ROOM_MESSAGES_COUNT + " = " +
                         MySQLHelper.TBL_CHAT_ROOM_MESSAGES_COUNT + " + 1 WHERE " +
                         MySQLHelper.TBL_CHAT_ROOM_ENTITY_OBJECT_ID + " = '" + entityObjectId +
                         "'");
  }

  public void setUnreadMessagesToZero(String entityObjectId) {
    mydatabase.execSQL("UPDATE " + MySQLHelper.TABLE_CHAT_ROOM + " SET " +
                         MySQLHelper.TBL_CHAT_ROOM_MESSAGES_COUNT + " = 0 WHERE " +
                         MySQLHelper.TBL_CHAT_ROOM_ENTITY_OBJECT_ID + " = '" + entityObjectId +
                         "'");
    LogEvent.Log(TAG, "Count set to 0");
  }

  public int getUnreadMessagesCount() {
    Cursor cursor =
      mydatabase.query(MySQLHelper.TABLE_CHAT_ROOM, null, null, null, null, null, null);
    if(cursor.getCount() != 0) {
      cursor.moveToFirst();
      int count = 0;
      while(!cursor.isAfterLast()) {
        int lastMsgStatus =
          cursor.getInt(cursor.getColumnIndexOrThrow(MySQLHelper.TBL_CHAT_ROOM_MESSAGES_COUNT));
        LogEvent.Log(TAG, "newMessagesCount: " + lastMsgStatus);
        if(lastMsgStatus > 0) {
          count++;
        }
        cursor.moveToNext();
      }
      cursor.close();
      return count;
    } else {
      cursor.close();
      return 0;
    }
  }

  public ArrayList<ChatRoom> getChatRooms() {
    Cursor cursor =
      mydatabase.query(MySQLHelper.TABLE_CHAT_ROOM, null, null, null, null, null, null);
    if(cursor.getCount() != 0) {
      cursor.moveToFirst();
      ArrayList<ChatRoom> chatRoomsArrayList = new ArrayList<ChatRoom>();
      while(!cursor.isAfterLast()) {
        int _id = cursor.getInt(cursor.getColumnIndexOrThrow(MySQLHelper.PK));
        int entityTypeId =
          cursor.getInt(cursor.getColumnIndexOrThrow(MySQLHelper.TBL_CHAT_ROOM_ENTITY_TYPE_ID));
        String entityObjectId = cursor.getString(
          cursor.getColumnIndexOrThrow(MySQLHelper.TBL_CHAT_ROOM_ENTITY_OBJECT_ID));
        String entityName =
          cursor.getString(cursor.getColumnIndexOrThrow(MySQLHelper.TBL_CHAT_ROOM_ENTITY_NAME));
        String entityCreatedAt = cursor.getString(
          cursor.getColumnIndexOrThrow(MySQLHelper.TBL_CHAT_ROOM_ENTITY_CREATED_AT));
        String senderId =
          cursor.getString(cursor.getColumnIndexOrThrow(MySQLHelper.TBL_CHAT_ROOM_SENDER_ID));
        String senderFirstName = cursor.getString(
          cursor.getColumnIndexOrThrow(MySQLHelper.TBL_CHAT_ROOM_SENDER_FIRST_NAME));
        String senderLastName = cursor.getString(
          cursor.getColumnIndexOrThrow(MySQLHelper.TBL_CHAT_ROOM_SENDER_LAST_NAME));
        String msg =
          cursor.getString(cursor.getColumnIndexOrThrow(MySQLHelper.TBL_CHAT_ROOM_MESSAGE));
        String msgTime =
          cursor.getString(cursor.getColumnIndexOrThrow(MySQLHelper.TBL_CHAT_ROOM_MESSAGE_TIME));
        String msgType =
          cursor.getString(cursor.getColumnIndexOrThrow(MySQLHelper.TBL_CHAT_ROOM_MESSAGE_TYPE));
        int msgCount =
          cursor.getInt(cursor.getColumnIndexOrThrow(MySQLHelper.TBL_CHAT_ROOM_MESSAGES_COUNT));
        int msgIncoming =
          cursor.getInt(cursor.getColumnIndexOrThrow(MySQLHelper.TBL_CHAT_ROOM_MESSAGE_INCOMING));
        int isRemovedFromChatRoom =
          cursor.getInt(cursor.getColumnIndexOrThrow(MySQLHelper.TBL_CHAT_ROOM_IS_REMOVED));
        boolean isReceived = false;
        if(msgIncoming == 1) {
          isReceived = true;
        }
        boolean isRemoved = false;
        if(isRemovedFromChatRoom == 1) {
          isRemoved = true;
        }
        ChatRoom chatRoom =
          new ChatRoom(_id, entityTypeId, entityObjectId, entityName, entityCreatedAt, senderId,
            senderFirstName, senderLastName, msg, msgTime, msgType, msgCount, isReceived,
            isRemoved);
        LogEvent.Log(TAG, entityName + ": " + entityCreatedAt);
        chatRoomsArrayList.add(chatRoom);
        cursor.moveToNext();
      }
      cursor.close();
      return chatRoomsArrayList;
    } else {
      cursor.close();
      return null;
    }
  }

  public void updateIsRemovedFromChatRoom(String entityObjectId, boolean isRemoved) {
    LogEvent.Log(TAG, "updateIsRemovedFromChatRoom() invoked");
    int isRemovedFromChatRoom = 0;
    if(isRemoved) {
      isRemovedFromChatRoom = 1;
    }
    String selection = MySQLHelper.TBL_CHAT_ROOM_ENTITY_OBJECT_ID + " = '" + entityObjectId + "'";
    ContentValues valueChatRoom = new ContentValues();
    valueChatRoom.put(MySQLHelper.TBL_CHAT_ROOM_IS_REMOVED, isRemovedFromChatRoom);
    int rowsUpdated =
      mydatabase.update(MySQLHelper.TABLE_CHAT_ROOM, valueChatRoom, selection, null);
    if(rowsUpdated == 0) {
      LogEvent.Log(TAG, "not updated");
    } else {
      LogEvent.Log(TAG, "updated");
    }
  }

  public boolean isRemovedFromChatRoom(String entityObjectId) {
    LogEvent.Log(TAG, "isRemovedFromChatRoom() invoked");
    String[] columns = new String[]{MySQLHelper.TBL_CHAT_ROOM_IS_REMOVED};
    String selection = MySQLHelper.TBL_CHAT_ROOM_ENTITY_OBJECT_ID + " = '" + entityObjectId + "'";
    Cursor cursor =
      mydatabase.query(MySQLHelper.TABLE_CHAT_ROOM, columns, selection, null, null, null, null);
    boolean isRemoved = false;
    if(cursor.getCount() != 0) {
      cursor.moveToFirst();
      int isRemovedFromChatRoom =
        cursor.getInt(cursor.getColumnIndexOrThrow(MySQLHelper.TBL_CHAT_ROOM_IS_REMOVED));
      if(isRemovedFromChatRoom == 1) {
        isRemoved = true;
      }
    }
    cursor.close();
    LogEvent.Log(TAG, "isRemoved: " + isRemoved);
    return isRemoved;
  }

  public void insertOrUpdateSettings(ChatRoomSettings chatRoomSettings, String entityObjectId) {
    LogEvent.Log(TAG, "insertOrUpdateSettings() invoked");
    LogEvent.Log(TAG, "chatRoomSettings.entityTypeId: " + chatRoomSettings.entityTypeId);
    LogEvent.Log(TAG, "chatRoomSettings.entityObjectId: " + chatRoomSettings.entityObjectId);
    LogEvent.Log(TAG, "chatRoomSettings.entityName: " + chatRoomSettings.entityName);
    LogEvent.Log(TAG, "chatRoomSettings.muteEnabled: " + chatRoomSettings.muteEnabled);
    LogEvent.Log(TAG, "chatRoomSettings.muteFor: " + chatRoomSettings.muteFor);
    LogEvent.Log(TAG, "chatRoomSettings.muteTime: " + chatRoomSettings.muteTime);
    LogEvent.Log(TAG, "chatRoomSettings.muteUntil: " + chatRoomSettings.muteUntil);
    LogEvent.Log(TAG,
      "chatRoomSettings.notificationDisabled: " + chatRoomSettings.notificationDisabled);
    String selection =
      MySQLHelper.TBL_CHAT_ROOM_SETTINGS_ENTITY_OBJECT_ID + " = '" + entityObjectId + "'";
    ContentValues valueChatRoomSettings = new ContentValues();
    if(chatRoomSettings.muteEnabled) {
      valueChatRoomSettings.put(MySQLHelper.TBL_CHAT_ROOM_SETTINGS_MUTE_ENABLED, 1);
    } else {
      valueChatRoomSettings.put(MySQLHelper.TBL_CHAT_ROOM_SETTINGS_MUTE_ENABLED, 0);
    }
    if(chatRoomSettings.notificationDisabled) {
      valueChatRoomSettings.put(MySQLHelper.TBL_CHAT_ROOM_SETTINGS_NOTIFICATION_DISABLED, 1);
    } else {
      valueChatRoomSettings.put(MySQLHelper.TBL_CHAT_ROOM_SETTINGS_NOTIFICATION_DISABLED, 0);
    }
    if(chatRoomSettings.muteFor != null) {
      valueChatRoomSettings.put(MySQLHelper.TBL_CHAT_ROOM_SETTINGS_MUTE_FOR,
        chatRoomSettings.muteFor);
    }
    valueChatRoomSettings.put(MySQLHelper.TBL_CHAT_ROOM_SETTINGS_MUTE_TIME,
      String.valueOf(chatRoomSettings.muteTime));
    valueChatRoomSettings.put(MySQLHelper.TBL_CHAT_ROOM_SETTINGS_MUTE_UNTIL,
      String.valueOf(chatRoomSettings.muteUntil));
    int rowsUpdated =
      mydatabase.update(MySQLHelper.TABLE_CHAT_ROOM_SETTINGS, valueChatRoomSettings, selection,
        null);
    if(rowsUpdated == 0) {
      insertIntoChatRoomSettings(chatRoomSettings, valueChatRoomSettings);
    } else {
      LogEvent.Log(TAG, "updated");
    }
  }

  public int insertIntoChatRoomSettings(ChatRoomSettings chatRoomSettings,
                                        ContentValues valueChatRoomSettings) {
    LogEvent.Log(TAG, "insertIntoChatRoomSettings() invoked");
    LogEvent.Log(TAG, "chatRoomSettings.entityTypeId: " + chatRoomSettings.entityTypeId);
    LogEvent.Log(TAG, "chatRoomSettings.entityObjectId: " + chatRoomSettings.entityObjectId);
    LogEvent.Log(TAG, "chatRoomSettings.entityName: " + chatRoomSettings.entityName);
    LogEvent.Log(TAG, "chatRoomSettings.muteEnabled: " + chatRoomSettings.muteEnabled);
    LogEvent.Log(TAG, "chatRoomSettings.muteFor: " + chatRoomSettings.muteFor);
    LogEvent.Log(TAG, "chatRoomSettings.muteTime: " + chatRoomSettings.muteTime);
    LogEvent.Log(TAG, "chatRoomSettings.muteUntil: " + chatRoomSettings.muteUntil);
    LogEvent.Log(TAG,
      "chatRoomSettings.notificationDisabled: " + chatRoomSettings.notificationDisabled);
    valueChatRoomSettings.put(MySQLHelper.TBL_CHAT_ROOM_SETTINGS_ENTITY_TYPE_ID,
      chatRoomSettings.entityTypeId);
    valueChatRoomSettings.put(MySQLHelper.TBL_CHAT_ROOM_SETTINGS_ENTITY_OBJECT_ID,
      chatRoomSettings.entityObjectId);
    valueChatRoomSettings.put(MySQLHelper.TBL_CHAT_ROOM_SETTINGS_ENTITY_NAME,
      chatRoomSettings.entityName);
    long insert_id = mydatabase.insertWithOnConflict(MySQLHelper.TABLE_CHAT_ROOM_SETTINGS, null,
      valueChatRoomSettings, SQLiteDatabase.CONFLICT_IGNORE);
    Log.i(TAG, "Added to chat_room_settings " + insert_id);
    return (int) insert_id;
  }

  public ChatRoomSettings getSettings(String entityObjectId) {
    LogEvent.Log(TAG, "getSettings() invoked");
    String selection =
      MySQLHelper.TBL_CHAT_ROOM_SETTINGS_ENTITY_OBJECT_ID + " = '" + entityObjectId + "'";
    String[] columns = new String[]{MySQLHelper.PK, MySQLHelper.TBL_CHAT_ROOM_SETTINGS_MUTE_ENABLED,
      MySQLHelper.TBL_CHAT_ROOM_SETTINGS_NOTIFICATION_DISABLED,
      MySQLHelper.TBL_CHAT_ROOM_SETTINGS_MUTE_UNTIL};
    Cursor cursor =
      mydatabase.query(MySQLHelper.TABLE_CHAT_ROOM_SETTINGS, null, selection, null, null, null,
        null);
    ChatRoomSettings chatRoomSettings = null;
    if(cursor.getCount() != 0) {
      cursor.moveToFirst();
      int id = cursor.getInt(cursor.getColumnIndexOrThrow(MySQLHelper.PK));
      int entityTypeId = cursor.getInt(
        cursor.getColumnIndexOrThrow(MySQLHelper.TBL_CHAT_ROOM_SETTINGS_ENTITY_TYPE_ID));
      String entityName = cursor.getString(
        cursor.getColumnIndexOrThrow(MySQLHelper.TBL_CHAT_ROOM_SETTINGS_ENTITY_NAME));
      int isMuteEnabled = cursor.getInt(
        cursor.getColumnIndexOrThrow(MySQLHelper.TBL_CHAT_ROOM_SETTINGS_MUTE_ENABLED));
      int isNotificationDisabled = cursor.getInt(
        cursor.getColumnIndexOrThrow(MySQLHelper.TBL_CHAT_ROOM_SETTINGS_NOTIFICATION_DISABLED));
      String muteUntilStr = cursor.getString(
        cursor.getColumnIndexOrThrow(MySQLHelper.TBL_CHAT_ROOM_SETTINGS_MUTE_UNTIL));
      String muteFor = cursor.getString(
        cursor.getColumnIndexOrThrow(MySQLHelper.TBL_CHAT_ROOM_SETTINGS_MUTE_FOR));
      String muteTimeStr = cursor.getString(
        cursor.getColumnIndexOrThrow(MySQLHelper.TBL_CHAT_ROOM_SETTINGS_MUTE_TIME));
      boolean muteEnabled = false;
      if(isMuteEnabled == 1) {
        muteEnabled = true;
      }
      boolean notificationDisabled = false;
      if(isNotificationDisabled == 1) {
        notificationDisabled = true;
      }
      ChatRoom.EntityType entityType;
      if(entityTypeId == 0) {
        entityType = ChatRoom.EntityType.PUBLIC_CELL;
      } else {
        entityType = ChatRoom.EntityType.ALERT;
      }
      long muteUntil = 0;
      if(muteUntilStr != null && !muteUntilStr.isEmpty()) {
        muteUntil = Long.parseLong(muteUntilStr);
      }
      long muteTime = 0;
      if(muteTimeStr != null && !muteTimeStr.isEmpty()) {
        muteTime = Long.parseLong(muteTimeStr);
      }
      chatRoomSettings = new ChatRoomSettings(entityType, entityObjectId, entityName, muteEnabled,
        notificationDisabled, muteFor, muteTime, muteUntil);
    }
    cursor.close();
    if(chatRoomSettings != null) {
      LogEvent.Log(TAG, "chatRoomSettings.entityTypeId: " + chatRoomSettings.entityTypeId);
      LogEvent.Log(TAG, "chatRoomSettings.entityObjectId: " + chatRoomSettings.entityObjectId);
      LogEvent.Log(TAG, "chatRoomSettings.entityName: " + chatRoomSettings.entityName);
      LogEvent.Log(TAG, "chatRoomSettings.muteEnabled: " + chatRoomSettings.muteEnabled);
      LogEvent.Log(TAG, "chatRoomSettings.muteTime: " + chatRoomSettings.muteTime);
      LogEvent.Log(TAG, "chatRoomSettings.muteUntil: " + chatRoomSettings.muteUntil);
      LogEvent.Log(TAG, "chatRoomSettings.muteFor: " + chatRoomSettings.muteFor);
      LogEvent.Log(TAG,
        "chatRoomSettings.notificationDisabled: " + chatRoomSettings.notificationDisabled);
    } else {
      LogEvent.Log(TAG, "chatRoomSettings is null");
    }
    return chatRoomSettings;
  }

  public void deleteChatRoomId(String entityObjectId) {
    String whereClause = MySQLHelper.TBL_CHAT_ROOM_ENTITY_OBJECT_ID + " = '" + entityObjectId + "'";
    mydatabase.delete(MySQLHelper.TABLE_CHAT_ROOM, whereClause, null);
  }

  public void clearDatabase() {
    mydatabase.delete(MySQLHelper.TABLE_CHAT_ROOM, null, null);
    mydatabase.delete(MySQLHelper.TABLE_CHAT_ROOM_SETTINGS, null, null);
  }
}