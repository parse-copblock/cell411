package cell411.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class MySQLHelper extends SQLiteOpenHelper {
  public static final int databaseVersion = 5;
  //public static final String TABLE_CHAT_ROOM_ENTITY = "chat_room_entity";
  public static final String TABLE_CHAT_ROOM = "chat_room";
  public static final String TABLE_CHAT_ROOM_SETTINGS = "chat_room_settings";
  public static final String PK = "_id";
  //public static final String TBL_CHAT_ROOM_ENTITY_TYPE_NAME = "entity_type_name";
  public static final String TBL_CHAT_ROOM_ENTITY_TYPE_ID = "entity_type_id";
  public static final String TBL_CHAT_ROOM_ENTITY_OBJECT_ID = "entity_object_id";
  public static final String TBL_CHAT_ROOM_ENTITY_NAME = "entity_name";
  public static final String TBL_CHAT_ROOM_ENTITY_CREATED_AT = "entity_created_at";
  public static final String TBL_CHAT_ROOM_SENDER_ID = "sender_id";
  public static final String TBL_CHAT_ROOM_SENDER_FIRST_NAME = "sender_first_name";
  public static final String TBL_CHAT_ROOM_SENDER_LAST_NAME = "sender_last_name";
  public static final String TBL_CHAT_ROOM_MESSAGE = "msg";
  public static final String TBL_CHAT_ROOM_MESSAGE_TYPE = "msg_type";
  public static final String TBL_CHAT_ROOM_MESSAGE_INCOMING = "msg_incoming";
  public static final String TBL_CHAT_ROOM_MESSAGES_COUNT = "msg_count";
  public static final String TBL_CHAT_ROOM_MESSAGE_TIME = "msg_time";
  public static final String TBL_CHAT_ROOM_IS_REMOVED = "is_removed";
  public static final String TBL_CHAT_ROOM_SETTINGS_ENTITY_TYPE_ID = "entity_type_id";
  public static final String TBL_CHAT_ROOM_SETTINGS_ENTITY_OBJECT_ID = "entity_object_id";
  public static final String TBL_CHAT_ROOM_SETTINGS_ENTITY_NAME = "entity_name";
  public static final String TBL_CHAT_ROOM_SETTINGS_MUTE_ENABLED = "mute_enabled";
  public static final String TBL_CHAT_ROOM_SETTINGS_MUTE_FOR = "mute_for";
  public static final String TBL_CHAT_ROOM_SETTINGS_MUTE_TIME = "mute_time";
  public static final String TBL_CHAT_ROOM_SETTINGS_MUTE_UNTIL = "mute_until";
  public static final String TBL_CHAT_ROOM_SETTINGS_NOTIFICATION_DISABLED = "notification_disabled";
  public static final String CHAT_ROOM_TABLE_CREATE =
    "create table " + TABLE_CHAT_ROOM + "(" + PK + " integer primary key autoincrement, " +
      TBL_CHAT_ROOM_ENTITY_TYPE_ID + " integer not null ," + TBL_CHAT_ROOM_ENTITY_OBJECT_ID +
      " text not null ," + TBL_CHAT_ROOM_ENTITY_NAME + " text not null ," +
      TBL_CHAT_ROOM_ENTITY_CREATED_AT + " text not null ," + TBL_CHAT_ROOM_SENDER_ID +
      " text not null ," + TBL_CHAT_ROOM_SENDER_FIRST_NAME + " text not null ," +
      TBL_CHAT_ROOM_SENDER_LAST_NAME + " text not null ," + TBL_CHAT_ROOM_MESSAGE +
      " text not null ," + TBL_CHAT_ROOM_MESSAGE_TYPE + " text not null ," +
      TBL_CHAT_ROOM_MESSAGE_TIME + " text not null ," + TBL_CHAT_ROOM_MESSAGES_COUNT +
      " integer ," + TBL_CHAT_ROOM_IS_REMOVED + " integer ," + TBL_CHAT_ROOM_MESSAGE_INCOMING +
      " integer not null );";
  public static final String CHAT_ROOM_SETTING_TABLE_CREATE =
    "create table " + TABLE_CHAT_ROOM_SETTINGS + "(" + PK +
      " integer primary key autoincrement, " + TBL_CHAT_ROOM_SETTINGS_ENTITY_TYPE_ID +
      " integer not null ," + TBL_CHAT_ROOM_SETTINGS_ENTITY_OBJECT_ID + " text not null ," +
      TBL_CHAT_ROOM_SETTINGS_ENTITY_NAME + " text not null ," +
      TBL_CHAT_ROOM_SETTINGS_MUTE_FOR + " text ," + TBL_CHAT_ROOM_SETTINGS_MUTE_TIME +
      " text ," + TBL_CHAT_ROOM_SETTINGS_MUTE_UNTIL + " text ," +
      TBL_CHAT_ROOM_SETTINGS_MUTE_ENABLED + " integer ," +
      TBL_CHAT_ROOM_SETTINGS_NOTIFICATION_DISABLED + " integer );";

  public MySQLHelper(Context context, String db) {
    super(context, db, null, databaseVersion);
  }

  @Override
  public void onCreate(SQLiteDatabase db) {
    //db.execSQL(CHAT_ROOM_ENTITY_TABLE_CREATE);
    db.execSQL(CHAT_ROOM_TABLE_CREATE);
    db.execSQL(CHAT_ROOM_SETTING_TABLE_CREATE);
  }

  @Override
  public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    // "Create table if not exists", use this SQL command to create a new table without deleting old tables
    // "Drop table if exists", use this SQl command to drop existing table and then create new
    //db.execSQL("Drop table if exists " + TABLE_CHAT_ROOM_ENTITY);
    db.execSQL("Drop table if exists " + TABLE_CHAT_ROOM);
    db.execSQL("Drop table if exists " + TABLE_CHAT_ROOM_SETTINGS);
    onCreate(db);
  }
}