package cell411.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.util.Log;

/**
 * Created by Sachin on 10/14/2015.
 */
public class SmsListener extends BroadcastReceiver {
  final Listener mListener;
  //  protected abstract void onVerificationCodeReceived(String verificationCode);
  private final String TAG = SmsListener.class.getSimpleName();
  ;

  public SmsListener(Listener listener) {
    mListener = listener;
  }

  @Override
  public void onReceive(Context context, Intent intent) {
    if(intent.getAction().equals("android.provider.Telephony.SMS_RECEIVED")) {
      Bundle bundle = intent.getExtras(); // --- get the SMS message passed in ---
      SmsMessage[] msgs = null;
      String msgFrom;
      if(bundle != null) {
        //---retrieve the SMS message received---
        try {
          Object[] pdus = (Object[]) bundle.get("pdus");
          msgs = new SmsMessage[pdus.length];
          for(int i = 0; i < msgs.length; i++) {
            msgs[i] = SmsMessage.createFromPdu((byte[]) pdus[i]);
            msgFrom = msgs[i].getOriginatingAddress();
            String msgBody = msgs[i].getMessageBody();
            mListener.messageReceived(msgFrom, msgBody);
          }
        } catch(Exception e) {
          Log.d("Exception caught", e.getMessage());
        }
      }
    }
  }

  ;

  public interface Listener {
    void messageReceived(String sender, String body);
  }
}
