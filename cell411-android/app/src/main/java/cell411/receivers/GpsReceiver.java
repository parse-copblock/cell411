package cell411.receivers;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.LocationManager;
import android.os.Handler;
import android.provider.Settings;

import androidx.core.app.NotificationCompat;
import androidx.core.app.TaskStackBuilder;

import com.firebase.jobdispatcher.Constraint;
import com.firebase.jobdispatcher.FirebaseJobDispatcher;
import com.firebase.jobdispatcher.GooglePlayDriver;
import com.firebase.jobdispatcher.Job;
import com.firebase.jobdispatcher.Lifetime;
import com.parse.ParseUser;
import com.safearx.cell411.R;

import cell411.interfaces.LocationStateChangeCallBack;
import cell411.services.LocationJobService;
import cell411.utils.LogEvent;

import static cell411.SingletonConfig.CHANNEL_ID_LOCATION_DISABLED;

/**
 * Created by Sachin on 21-05-2018.
 */
public class GpsReceiver extends BroadcastReceiver {
  private static final String TAG = GpsReceiver.class.getSimpleName();
  private LocationStateChangeCallBack locationStateChangeCallBack = null;
  private Handler handler;
  private boolean isHandlerInProgress;

  public GpsReceiver() {
    init();
  }

  /**
   * initializes receiver with callback
   *
   * @param iLocationStateChangeCallBack Location callback
   */
  public GpsReceiver(LocationStateChangeCallBack iLocationStateChangeCallBack) {
    this.locationStateChangeCallBack = iLocationStateChangeCallBack;
    init();
  }

  public static void cancelJob(final Context context) {
    FirebaseJobDispatcher dispatcher = new FirebaseJobDispatcher(new GooglePlayDriver(context));
    dispatcher.cancel("location-job-service");
  }

  public static void dispatchJob(final Context context) {
    // Create a new dispatcher using the Google Play driver.
    FirebaseJobDispatcher dispatcher = new FirebaseJobDispatcher(new GooglePlayDriver(context));
    //Bundle myExtrasBundle = new Bundle();
    //myExtrasBundle.putString("some_key", "some_value");
    Job myJob = dispatcher.newJobBuilder().setService(
      LocationJobService.class) // the JobService that will be called
                  .setTag("location-job-service") // uniquely identifies the job
                  .setLifetime(Lifetime.FOREVER) // persist the task across boots
                  .setConstraints(
                    Constraint.ON_ANY_NETWORK) // run this job only when the network is available
                  //.setExtras(myExtrasBundle)
                  .build();
    dispatcher.mustSchedule(myJob);
  }

  private void init() {
    handler = new Handler();
  }

  /**
   * triggers on receiving external broadcast
   *
   * @param context Context
   * @param intent  Intent
   */
  @Override
  public void onReceive(final Context context, Intent intent) {
    if(intent.getAction().matches("android.location.PROVIDERS_CHANGED")) {
      LogEvent.Log(TAG, "android.location.PROVIDERS_CHANGED");
      if(ParseUser.getCurrentUser() == null) {
        // return if user is not logged in
        return;
      }
      if(!isHandlerInProgress) {
        isHandlerInProgress = true;
        handler.postDelayed(new Runnable() {
          @Override
          public void run() {
            final SharedPreferences prefs =
              context.getSharedPreferences("AppPrefs", Context.MODE_PRIVATE);
            LocationManager lm =
              (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
            boolean networkProviderEnabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
            boolean gpsProviderEnabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
            if(!networkProviderEnabled && !gpsProviderEnabled) {
              if(prefs.getBoolean("LocationUpdate", true)) {
                LogEvent.Log(TAG, "LocationUpdate is true");
                final boolean isPatrolModeFeatureEnabled =
                  context.getResources().getBoolean(R.bool.is_patrol_mode_feature_enabled);
                int patrolMode = 0, newPublicCellAlert = 0;
                boolean rideRequestAlert = false;
                String str = "";
                if(isPatrolModeFeatureEnabled) {
                  if(ParseUser.getCurrentUser().get("PatrolMode") != null) {
                    patrolMode = (int) ParseUser.getCurrentUser().get("PatrolMode");
                    if(patrolMode == 1) {
                      str += "\n- " + "Patrol mode";
                    }
                  }
                }
                if(ParseUser.getCurrentUser().get("newPublicCellAlert") != null) {
                  newPublicCellAlert = (int) ParseUser.getCurrentUser().get("newPublicCellAlert");
                  if(newPublicCellAlert == 1) {
                    str += "\n- " + "New Public Cell alert";
                  }
                }
                if(ParseUser.getCurrentUser().get("rideRequestAlert") != null) {
                  rideRequestAlert = ParseUser.getCurrentUser().getBoolean("rideRequestAlert");
                  if(rideRequestAlert) {
                    str += "\n- " + "Ride requests";
                  }
                }
                                /*if ((isPatrolModeFeatureEnabled && finalPatrolMode == 1) && finalNewPublicCellAlert == 1) {
                                    displayLocationOffNotification(context, 1, 1);
                                } else if ((isPatrolModeFeatureEnabled && finalPatrolMode == 1)) {
                                    displayLocationOffNotification(context, 1, 0);
                                } else if (finalNewPublicCellAlert == 1) {
                                    displayLocationOffNotification(context, 0, 1);
                                } else {
                                    // No need to display any notification
                                }*/
                LogEvent.Log(TAG, "str: " + str);
                if(!str.equals("")) {
                  displayLocationOffNotification(context, str, patrolMode, newPublicCellAlert,
                    rideRequestAlert);
                } else {
                  // No need to display any notification
                }
                if(locationStateChangeCallBack != null) {
                  //locationStateChangeCallBack.onLocationStateChanged(false);
                }
                SharedPreferences.Editor editor = prefs.edit();
                boolean somethingChanged = false;
                if(isPatrolModeFeatureEnabled && patrolMode == 1) {
                  somethingChanged = true;
                  editor.putBoolean("patrolModeDisabledByApp", true);
                  ParseUser.getCurrentUser().put("PatrolMode", 0);
                }
                if(newPublicCellAlert == 1) {
                  somethingChanged = true;
                  editor.putBoolean("newPublicCellAlertDisabledByApp", true);
                  ParseUser.getCurrentUser().put("newPublicCellAlert", 0);
                }
                if(rideRequestAlert) {
                  somethingChanged = true;
                  editor.putBoolean("rideRequestAlertDisabledByApp", true);
                  ParseUser.getCurrentUser().put("rideRequestAlert", false);
                }
                if(somethingChanged) {
                  editor.commit();
                  ParseUser.getCurrentUser().saveEventually();
                }
                // Stop the location service
                //Intent intentLocationService = new Intent(context, LocationService.class);
                //context.stopService(intentLocationService);
                // Stop the location service
                cancelJob(context);
              }
            } else {
              if(locationStateChangeCallBack != null) {
                //locationStateChangeCallBack.onLocationStateChanged(true);
              }
              if(prefs.getBoolean("LocationUpdate", true)) {
                boolean somethingChanged = false;
                if(prefs.getBoolean("patrolModeDisabledByApp", false)) {
                  somethingChanged = true;
                  ParseUser.getCurrentUser().put("PatrolMode", 1);
                }
                if(prefs.getBoolean("newPublicCellAlertDisabledByApp", false)) {
                  somethingChanged = true;
                  ParseUser.getCurrentUser().put("newPublicCellAlert", 1);
                }
                if(prefs.getBoolean("rideRequestAlertDisabledByApp", false)) {
                  somethingChanged = true;
                  ParseUser.getCurrentUser().put("rideRequestAlert", true);
                }
                if(somethingChanged) {
                  ParseUser.getCurrentUser().saveEventually();
                }
                                /*if (Build.VERSION.SDK_INT >= 26) {
                                    scheduleJob(context, true);
                                } else {
                                    // Start the location service to update user's current location indefinitely
                                    Intent intentLocationService = new Intent(context, LocationService.class);
                                    context.startService(intentLocationService);
                                }*/
                dispatchJob(context);
              }
            }
            isHandlerInProgress = false;
          }
        }, 5000);
      }
    }
  }

  public void displayLocationOffNotification(Context context, final String str, int patrolMode,
                                             int newPublicCellAlert, boolean rideRequestAlert) {
    LogEvent.Log(TAG, "displayLocationOffNotification invoked");
    String message;
    if(patrolMode == 1 && newPublicCellAlert == 1 && rideRequestAlert) {
      message = context.getString(R.string.msg_location_three_features_disabled,
        context.getString(R.string.app_name), context.getString(R.string.feature_patrol_mode),
        context.getString(R.string.feature_new_public_cell_alert),
        context.getString(R.string.feature_ride_request_alert));
    } else if(patrolMode == 1 && newPublicCellAlert == 1) {
      message = context.getString(R.string.msg_location_two_features_disabled,
        context.getString(R.string.app_name), context.getString(R.string.feature_patrol_mode),
        context.getString(R.string.feature_new_public_cell_alert));
    } else if(newPublicCellAlert == 1 && rideRequestAlert) {
      message = context.getString(R.string.msg_location_two_features_disabled,
        context.getString(R.string.app_name),
        context.getString(R.string.feature_new_public_cell_alert),
        context.getString(R.string.feature_ride_request_alert));
    } else if(patrolMode == 1 && rideRequestAlert) {
      message = context.getString(R.string.msg_location_two_features_disabled,
        context.getString(R.string.app_name), context.getString(R.string.feature_patrol_mode),
        context.getString(R.string.feature_ride_request_alert));
    } else if(patrolMode == 1) {
      message = context.getString(R.string.msg_location_one_feature_disabled,
        context.getString(R.string.app_name), context.getString(R.string.feature_patrol_mode));
    } else if(newPublicCellAlert == 1) {
      message = context.getString(R.string.msg_location_one_feature_disabled,
        context.getString(R.string.app_name),
        context.getString(R.string.feature_new_public_cell_alert));
    } else {
      message = context.getString(R.string.msg_location_one_feature_disabled,
        context.getString(R.string.app_name),
        context.getString(R.string.feature_ride_request_alert));
    }
    //message = "You've turned off location services, which has disabled following " + appName + " features:" + str;
    NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context,
      CHANNEL_ID_LOCATION_DISABLED).setSmallIcon(R.drawable.logo).setContentTitle(
      context.getString(R.string.app_name)).setContentText(message).setAutoCancel(true);
    mBuilder.setPriority(Notification.PRIORITY_HIGH);
    // Creates an explicit intent for an Activity in your app
    Intent resultIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
    //resultIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);
    // The stack builder object will contain an artificial back stack for the
    // started Activity.
    // This ensures that navigating backward from the Activity leads out of
    // your application to the Home screen.
    TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
    // Adds the back stack for the Intent (but not the Intent itself)
    //stackBuilder.addParentStack(MainActivity.class);
    // Adds the Intent that starts the Activity to the top of the stack
    stackBuilder.addNextIntent(resultIntent);
    int id = 100001;
    PendingIntent resultPendingIntent =
      stackBuilder.getPendingIntent(id, PendingIntent.FLAG_UPDATE_CURRENT);
    mBuilder.setContentIntent(resultPendingIntent);
    NotificationManager mNotificationManager =
      (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
    // mId allows you to update the notification later on.
    NotificationCompat.BigTextStyle notificationStyle = new NotificationCompat.BigTextStyle();
    notificationStyle.bigText(message);
    mBuilder.setStyle(notificationStyle);
    Notification notification = mBuilder.build();
    //notification.sound = Uri.parse("android.resource://" + context.getPackageName() + "/" + R.raw.cell411alert);
    notification.defaults =
      Notification.DEFAULT_SOUND | Notification.DEFAULT_LIGHTS | Notification.DEFAULT_VIBRATE;
    mNotificationManager.notify(id, notification);
  }
}