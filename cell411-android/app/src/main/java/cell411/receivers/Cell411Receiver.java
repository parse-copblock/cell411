package cell411.receivers;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.widget.RemoteViews;

import androidx.core.app.NotificationCompat;
import androidx.core.app.TaskStackBuilder;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParsePushBroadcastReceiver;
import com.parse.ParseQuery;
import com.parse.ParseRelation;
import com.parse.ParseUser;
import com.parse.callback.FindCallback;
import com.parse.callback.GetCallback;
import com.parse.callback.SaveCallback;
import com.safearx.cell411.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

import cell411.Singleton;
import cell411.activity.AlertActivity;
import cell411.activity.ChatActivity;
import cell411.activity.MainActivity;
import cell411.activity.NewMemberJoinedActivity;
import cell411.activity.SplashActivity;
import cell411.db.DataSource;
import cell411.fragments.FriendsFragment;
import cell411.methods.DialogModules4ReceivedAlerts;
import cell411.methods.Dialogs;
import cell411.methods.Modules;
import cell411.models.AlertFromHelper;
import cell411.models.AlertFromNeedy;
import cell411.models.AlertFromRejector;
import cell411.models.Cell411Alert;
import cell411.models.CellRequest;
import cell411.models.Chat;
import cell411.models.ChatRoom;
import cell411.models.ChatRoomSettings;
import cell411.models.FriendApproved;
import cell411.models.FriendRequest;
import cell411.models.NewPublicCell;
import cell411.models.ReceivedCell411Alert;
import cell411.models.User;
import cell411.models.Video;
import cell411.utils.LogEvent;
import cell411.utils.StorageOperations;

import static cell411.Singleton.isIsInForeground;
import static cell411.SingletonConfig.ACK_ALERT;
import static cell411.SingletonConfig.CHANNEL_ID_EMERGENCY_ALERT;
import static cell411.SingletonConfig.CHANNEL_ID_MESSAGE;
import static cell411.SingletonConfig.CHANNEL_ID_MISCELLANEOUS;

/**
 * Created by Sachin on 27-03-2016.
 */
public class Cell411Receiver extends ParsePushBroadcastReceiver {
  private static final String TAG = "Cell411Receiver";
  private SharedPreferences prefs;

  public Cell411Receiver() {
    super();
    prefs = Singleton.INSTANCE.getAppPrefs();
    LogEvent.Log(TAG, "Cell411Receiver() created");
  }

  @Override
  public void onReceive(final Context context, final Intent intent) {
    LogEvent.Log(TAG, "onReceive() invoked: " + intent.getAction());
    final String action = intent.getAction();
    try {
      if(intent.getExtras() == null) {
        return;
      }
      JSONObject json = new JSONObject(intent.getExtras().getString("com.parse.Data"));
      LogEvent.Log(TAG, json.toString());
      if(json == null || action == null) {
        LogEvent.Log(TAG, "Either json or action is null");
        return;
      }
      // New logic to prevent duplicate alerts
      if(json.has("badge") && !action.equals("com.parse.push.intent.OPEN")) {
        json.remove("badge");
        String previousParsePushData = prefs.getString("pushData", null);
        String parsePushData = json.toString();
        LogEvent.Log(TAG, "parsePushData: " + parsePushData);
        if(previousParsePushData != null && parsePushData != null) {
          if(previousParsePushData.equals(parsePushData)) {
            LogEvent.Log(TAG, "Ignoring duplicate push");
            return;
          }
        } else {
          LogEvent.Log(TAG, "previousParsePushData is null");
        }
        prefs.edit().putString("pushData", parsePushData).commit();
      }
      // Logic to prevent duplicate alerts
      if(json.has("parsePushId") && !action.equals("com.parse.push.intent.OPEN")) {
        String previousParsePushId = prefs.getString("parsePushId", null);
        String parsePushId = json.getString("parsePushId");
        if(previousParsePushId != null && parsePushId != null) {
          if(previousParsePushId.equals(parsePushId)) {
            LogEvent.Log(TAG, "Ignoring duplicate push");
            return;
          }
        } else {
          LogEvent.Log(TAG, "previousParsePushId is null");
        }
        prefs.edit().putString("parsePushId", parsePushId).commit();
      }
      String alertType = json.getString("alertType");
      LogEvent.Log(TAG, "alertType: " + alertType);
      if(alertType.equalsIgnoreCase("NEEDY")) {
        // Show alert to the user
        final String name = json.getString("firstName");
        final String userId = json.getString("userId");
        final String alert = json.getString("alert");
        final double lat = json.getDouble("lat");
        final double lon = json.getDouble("lon");
        if(ParseUser.getCurrentUser() == null) {
          return;
        } else if(ParseUser.getCurrentUser().getObjectId().equals(userId)) {
          LogEvent.Log(TAG, "Ignoring self notification");
          return;
        }
        if(json.has("key")) {
          final String key = json.getString("key");
          new SendAck4ReceivedAlertApiCall(context, ParseUser.getCurrentUser().getObjectId(),
            key).execute();
        }
        Cell411Alert.AlertType alertTypeEnum;
        int alertId = -1;
        String alertRegarding = null;
        if(json.has("alertId")) {
          alertId = json.getInt("alertId");
          alertTypeEnum = Cell411Alert.AlertType.values()[alertId];
        } else {
          alertRegarding = json.getString("alertRegarding");
          alertTypeEnum = Modules.convertStringToEnum(alertRegarding);
        }
        int isGlobal = 0;
        if(json.has("isGlobal")) {
          isGlobal = json.getInt("isGlobal");
        }
        int dispatchMode = 0;
        if(json.has("dispatchMode")) {
          dispatchMode = json.getInt("dispatchMode");
        }
        String additionalNote = null;
        if(json.has("additionalNote")) {
          additionalNote = json.getString("additionalNote");
        }
        final String cell411AlertId = json.getString("cell411AlertId");
        final long createdAt = json.getLong("createdAt");
        String fullAddress = null;
        String city = null;
        String country = null;
        if(json.has("fullAddress")) {
          fullAddress = json.getString("fullAddress");
        }
        if(json.has("city")) {
          fullAddress = json.getString("city");
        }
        if(json.has("country")) {
          fullAddress = json.getString("country");
        }
        //updateReceivedBy(cell411AlertId);
        // New Code
        AlertFromNeedy alertFromNeedy =
          new AlertFromNeedy(name, userId, lat, lon, createdAt, null, cell411AlertId,
            alertRegarding, additionalNote, dispatchMode, isGlobal, null, null, null, alertId,
            null, fullAddress, city, country);
        String[] nameArr = name.split(" ");
        ReceivedCell411Alert receivedCell411Alert =
          new ReceivedCell411Alert(nameArr[0], nameArr[1], userId, lat, lon, createdAt, null,
            cell411AlertId, additionalNote, dispatchMode, isGlobal, null, null, null,
            alertTypeEnum, null, fullAddress, city, country);
        if(isIsInForeground()) {
          LogEvent.Log(TAG, "App is in Foreground");
          Intent alertIntent =
            new Intent(Singleton.INSTANCE.getCurrentActivity(), AlertActivity.class);
          alertIntent.putExtra("receivedCell411Alert", receivedCell411Alert);
          Singleton.INSTANCE.getCurrentActivity().startActivity(alertIntent);
        } else {
          LogEvent.Log(TAG, "App is in Background");
          //super.onReceive(context, intent);
          displayNotification(context, alert);
          if(action.equals("com.parse.push.intent.RECEIVE")) {
            ArrayList<AlertFromNeedy> alertArrayList =
              StorageOperations.getAlertsFromNeedy(context);
            if(alertArrayList == null) {
              alertArrayList = new ArrayList<>();
            }
            alertArrayList.add(alertFromNeedy);
            StorageOperations.storeAlertsFromNeedy(context, alertArrayList);
          }
        }
      } else if(alertType.equalsIgnoreCase("NEEDY_FORWARDED")) {
        // Show alert to the user
        final String forwardedBy = json.getString("forwardedBy");
        final String name = json.getString("firstName");
        final String alert = json.getString("alert");
        final String userId = json.getString("userId");
        final double lat = json.getDouble("lat");
        final double lon = json.getDouble("lon");
        if(ParseUser.getCurrentUser() == null) {
          return;
        } else if(ParseUser.getCurrentUser().getObjectId().equals(userId)) {
          LogEvent.Log(TAG, "Ignoring self notification");
          return;
        }
        if(json.has("key")) {
          final String key = json.getString("key");
          new SendAck4ReceivedAlertApiCall(context, ParseUser.getCurrentUser().getObjectId(),
            key).execute();
        }
        Cell411Alert.AlertType alertTypeEnum;
        int alertId = -1;
        String alertRegarding = null;
        if(json.has("alertId")) {
          alertId = json.getInt("alertId");
          alertTypeEnum = Cell411Alert.AlertType.values()[alertId];
        } else {
          alertRegarding = json.getString("alertRegarding");
          alertTypeEnum = Modules.convertStringToEnum(alertRegarding);
        }
        int isGlobal = 0;
        if(json.has("isGlobal")) {
          isGlobal = json.getInt("isGlobal");
        }
        int dispatchMode = 0;
        if(json.has("dispatchMode")) {
          dispatchMode = json.getInt("dispatchMode");
        }
        String additionalNote = null;
        if(json.has("additionalNote")) {
          additionalNote = json.getString("additionalNote");
        }
        final String cell411AlertId = json.getString("cell411AlertId");
        final long createdAt = json.getLong("createdAt");
        String fullAddress = null;
        String city = null;
        String country = null;
        if(json.has("fullAddress")) {
          fullAddress = json.getString("fullAddress");
        }
        if(json.has("city")) {
          fullAddress = json.getString("city");
        }
        if(json.has("country")) {
          fullAddress = json.getString("country");
        }
        final String forwardingAlertId = json.getString("forwardingAlertId");
        //updateReceivedBy(forwardingAlertId);
        AlertFromNeedy alertFromNeedy =
          new AlertFromNeedy(name, userId, lat, lon, createdAt, null, cell411AlertId,
            alertRegarding, additionalNote, dispatchMode, isGlobal, forwardedBy, null, null,
            alertId, forwardingAlertId, fullAddress, city, country);
        String[] nameArr = name.split(" ");
        ReceivedCell411Alert receivedCell411Alert =
          new ReceivedCell411Alert(nameArr[0], nameArr[1], userId, lat, lon, createdAt, null,
            cell411AlertId, additionalNote, dispatchMode, isGlobal, forwardedBy, null, null,
            alertTypeEnum, forwardingAlertId, fullAddress, city, country);
        if(isIsInForeground()) {
          LogEvent.Log(TAG, "App is in Foreground");
          Intent alertIntent =
            new Intent(Singleton.INSTANCE.getCurrentActivity(), AlertActivity.class);
          alertIntent.putExtra("receivedCell411Alert", receivedCell411Alert);
          Singleton.INSTANCE.getCurrentActivity().startActivity(alertIntent);
        } else {
          LogEvent.Log(TAG, "App is in Background");
          //super.onReceive(context, intent);
          displayNotification(context, alert);
          if(action.equals("com.parse.push.intent.RECEIVE")) {
            ArrayList<AlertFromNeedy> alertArrayList =
              StorageOperations.getAlertsFromNeedy(context);
            if(alertArrayList == null) {
              alertArrayList = new ArrayList<>();
            }
            alertArrayList.add(alertFromNeedy);
            StorageOperations.storeAlertsFromNeedy(context, alertArrayList);
          }
        }
      } else if(alertType.equalsIgnoreCase("NEEDY_CELL")) {
        // Show alert to the user
        final String cellName = json.getString("cellName");
        final String cellId = json.getString("cellId");
        final String alert = json.getString("alert");
        final String name = json.getString("firstName");
        final String userId = json.getString("userId");
        final double lat = json.getDouble("lat");
        final double lon = json.getDouble("lon");
        if(ParseUser.getCurrentUser() == null) {
          return;
        } else if(ParseUser.getCurrentUser().getObjectId().equals(userId)) {
          LogEvent.Log(TAG, "Ignoring self notification");
          return;
        }
        if(json.has("key")) {
          final String key = json.getString("key");
          new SendAck4ReceivedAlertApiCall(context, ParseUser.getCurrentUser().getObjectId(),
            key).execute();
        }
        Cell411Alert.AlertType alertTypeEnum;
        int alertId = -1;
        String alertRegarding = null;
        if(json.has("alertId")) {
          alertId = json.getInt("alertId");
          alertTypeEnum = Cell411Alert.AlertType.values()[alertId];
        } else {
          alertRegarding = json.getString("alertRegarding");
          alertTypeEnum = Modules.convertStringToEnum(alertRegarding);
        }
        int isGlobal = 0;
        if(json.has("isGlobal")) {
          isGlobal = json.getInt("isGlobal");
        }
        int dispatchMode = 0;
        if(json.has("dispatchMode")) {
          dispatchMode = json.getInt("dispatchMode");
        }
        String additionalNote = null;
        if(json.has("additionalNote")) {
          additionalNote = json.getString("additionalNote");
        }
        final String cell411AlertId = json.getString("cell411AlertId");
        final long createdAt = json.getLong("createdAt");
        String fullAddress = null;
        String city = null;
        String country = null;
        if(json.has("fullAddress")) {
          fullAddress = json.getString("fullAddress");
        }
        if(json.has("city")) {
          fullAddress = json.getString("city");
        }
        if(json.has("country")) {
          fullAddress = json.getString("country");
        }
        //updateReceivedBy(cell411AlertId);
        AlertFromNeedy alertFromNeedy =
          new AlertFromNeedy(name, userId, lat, lon, createdAt, null, cell411AlertId,
            alertRegarding, additionalNote, dispatchMode, isGlobal, null, cellName, cellId,
            alertId, null, fullAddress, city, country);
        String[] nameArr = name.split(" ");
        ReceivedCell411Alert receivedCell411Alert =
          new ReceivedCell411Alert(nameArr[0], nameArr[1], userId, lat, lon, createdAt, null,
            cell411AlertId, additionalNote, dispatchMode, isGlobal, null, cellName, cellId,
            alertTypeEnum, null, fullAddress, city, country);
        if(isIsInForeground()) {
          LogEvent.Log(TAG, "App is in Foreground");
          Intent alertIntent =
            new Intent(Singleton.INSTANCE.getCurrentActivity(), AlertActivity.class);
          alertIntent.putExtra("receivedCell411Alert", receivedCell411Alert);
          Singleton.INSTANCE.getCurrentActivity().startActivity(alertIntent);
        } else {
          LogEvent.Log(TAG, "App is in Background");
          //super.onReceive(context, intent);
          displayNotification(context, alert);
          if(action.equals("com.parse.push.intent.RECEIVE")) {
            ArrayList<AlertFromNeedy> alertArrayList =
              StorageOperations.getAlertsFromNeedy(context);
            if(alertArrayList == null) {
              alertArrayList = new ArrayList<>();
            }
            alertArrayList.add(alertFromNeedy);
            StorageOperations.storeAlertsFromNeedy(context, alertArrayList);
          }
        }
      } else if(alertType.equalsIgnoreCase("HELPER")) {
        String name = json.getString("name");
        String forwardedBy = null;
        final String alert = json.getString("alert");
        if(json.has("forwardedBy")) {
          forwardedBy = json.getString("forwardedBy");
        }
        String cellName = null;
        if(json.has("cellName")) {
          cellName = json.getString("cellName");
        }
        String cellId = null;
        if(json.has("cellId")) {
          cellId = json.getString("cellId");
        }
        String userId = json.getString("userId");
        if(ParseUser.getCurrentUser() == null) {
          return;
        } else if(ParseUser.getCurrentUser().getObjectId().equals(userId)) {
          LogEvent.Log(TAG, "Ignoring self notification");
          return;
        }
        String duration = json.getString("duration");
        String additionalNote = null;
        if(json.has("additionalNote")) {
          additionalNote = json.getString("additionalNote");
        }
        String additionalNoteId = null;
        if(json.has("additionalNoteId")) {
          additionalNoteId = json.getString("additionalNoteId");
        }
        String userType = null;
        if(json.has("userType")) {
          userType = json.getString("userType");
        }
        long createdAt = json.getLong("createdAt");
        String message;
        if(forwardedBy != null && !forwardedBy.isEmpty()) {
          message = context.getString(R.string.alert_helper_varient1, name, forwardedBy, duration);
        } else if(cellName != null && !cellName.isEmpty()) {
          message = context.getString(R.string.alert_helper_varient2, name, cellName, duration);
        } else if(userType != null && userType.equalsIgnoreCase("FB")) {
          message = context.getString(R.string.alert_helper_varient3, name, duration);
        } else {
          message = context.getString(R.string.alert_helper_varient4, name, duration);
        }
        if(additionalNote != null) {
          message += ": " + additionalNote;
        } else {
          message += ".";
        }
        AlertFromHelper alertFromHelper =
          new AlertFromHelper(name, userId, createdAt, message, duration, additionalNoteId);
        if(isIsInForeground()) {
          LogEvent.Log(TAG, "App is in Foreground");
          ParseQuery<ParseObject> parseQuery = new ParseQuery<>("AdditionalNote");
          parseQuery.whereEqualTo("objectId", additionalNoteId);
          parseQuery.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> parseObjects, ParseException e) {
              if(e == null) {
                if(parseObjects != null && parseObjects.size() > 0) {
                  parseObjects.get(0).put("seen", 1);
                  parseObjects.get(0).saveEventually();
                }
              }
            }
          });
          Dialogs.showAlertDialog(Singleton.INSTANCE.getCurrentActivity(), message);
        } else {
          LogEvent.Log(TAG, "App is in Background");
          //super.onReceive(context, intent);
          displayNotification(context, alert);
          if(action.equals("com.parse.push.intent.RECEIVE")) {
            ArrayList<AlertFromHelper> alertArrayList =
              StorageOperations.getAlertsFromHelper(context);
            if(alertArrayList == null) {
              alertArrayList = new ArrayList<>();
            }
            alertArrayList.add(alertFromHelper);
            StorageOperations.storeAlertsFromHelper(context, alertArrayList);
          }
        }
      } else if(alertType.equalsIgnoreCase("REJECTOR")) {
        String name = json.getString("name");
        String userId = json.getString("userId");
        long createdAt = json.getLong("createdAt");
        final String alert = json.getString("alert");
        String additionalNote = null;
        if(ParseUser.getCurrentUser() == null) {
          return;
        } else if(ParseUser.getCurrentUser().getObjectId().equals(userId)) {
          LogEvent.Log(TAG, "Ignoring self notification");
          return;
        }
        if(json.has("additionalNote")) {
          additionalNote = json.getString("additionalNote");
        }
        String additionalNoteId = null;
        if(json.has("additionalNoteId")) {
          additionalNoteId = json.getString("additionalNoteId");
        }
        String message = context.getString(R.string.notif_rejector, name);
        if(additionalNote != null) {
          message += ": " + additionalNote;
        } else {
          message += ".";
        }
        AlertFromRejector alertFromRejector =
          new AlertFromRejector(name, userId, createdAt, message, additionalNoteId);
        if(isIsInForeground()) {
          LogEvent.Log(TAG, "App is in Foreground");
          ParseQuery<ParseObject> parseQuery = new ParseQuery<>("AdditionalNote");
          parseQuery.whereEqualTo("objectId", additionalNoteId);
          parseQuery.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> parseObjects, ParseException e) {
              if(e == null) {
                if(parseObjects != null && parseObjects.size() > 0) {
                  parseObjects.get(0).put("seen", 1);
                  parseObjects.get(0).saveEventually();
                }
              }
            }
          });
          Dialogs.showAlertDialog(Singleton.INSTANCE.getCurrentActivity(), message);
        } else {
          LogEvent.Log(TAG, "App is in Background");
          //super.onReceive(context, intent);
          displayNotification(context, alert);
          if(action.equals("com.parse.push.intent.RECEIVE")) {
            ArrayList<AlertFromRejector> alertArrayList =
              StorageOperations.getAlertsFromRejector(context);
            if(alertArrayList == null) {
              alertArrayList = new ArrayList<>();
            }
            alertArrayList.add(alertFromRejector);
            StorageOperations.storeAlertsFromRejector(context, alertArrayList);
          }
        }
      } else if(alertType.equalsIgnoreCase("FRIEND_REQUEST")) {
        String name = json.getString("name");
        String userId = json.getString("userId");
        final String alert = json.getString("alert");
        String friendRequestObjectId = json.getString("friendRequestObjectId");
        if(ParseUser.getCurrentUser() == null) {
          return;
        } else if(ParseUser.getCurrentUser().getObjectId().equals(userId)) {
          LogEvent.Log(TAG, "Ignoring self notification");
          return;
        }
        // Handling alerts from manually deleted users
        String[] nameArr = name.split(" ");
        for(int i = 0; i < nameArr.length; i++) {
          if(nameArr[i] == null || nameArr[i].equals("null")) {
            return;
          }
        }
        FriendRequest friendRequest = new FriendRequest(name, userId, friendRequestObjectId);
        if(isIsInForeground()) {
          LogEvent.Log(TAG, "App is in Foreground");
          DialogModules4ReceivedAlerts.showFriendRequestDialog(
            Singleton.INSTANCE.getCurrentActivity(), friendRequest);
        } else {
          LogEvent.Log(TAG, "App is in Background");
          //super.onReceive(context, intent);
          displayNotification(context, alert);
        }
      } else if(alertType.equalsIgnoreCase("CELL_REQUEST")) {
        String name = json.getString("name");
        String userId = json.getString("userId");
        final String alert = json.getString("alert");
        String cellRequestObjectId = json.getString("cellRequestObjectId");
        String cellId = json.getString("cellId");
        String cellName = json.getString("cellName");
        if(ParseUser.getCurrentUser() == null) {
          return;
        } else if(ParseUser.getCurrentUser().getObjectId().equals(userId)) {
          LogEvent.Log(TAG, "Ignoring self notification");
          return;
        }
        String message = context.getString(R.string.notif_cell_join_request, name, cellName);
        // Handling alerts from manually deleted users
        String[] nameArr = name.split(" ");
        for(int i = 0; i < nameArr.length; i++) {
          if(nameArr[i] == null || nameArr[i].equals("null")) {
            return;
          }
        }
        CellRequest cellRequest =
          new CellRequest(name, userId, message, cellRequestObjectId, cellName, cellId);
        if(isIsInForeground()) {
          LogEvent.Log(TAG, "App is in Foreground");
          DialogModules4ReceivedAlerts.showCellRequestDialog(
            Singleton.INSTANCE.getCurrentActivity(), cellRequest);
        } else {
          LogEvent.Log(TAG, "App is in Background");
          //super.onReceive(context, intent);
          displayNotification(context, alert);
        }
      } else if(alertType.equalsIgnoreCase("NEW_PUBLIC_CELL")) {
        final String cellName = json.getString("cellName");
        String cellId = json.getString("cellId");
        String userId = json.getString("userId");
        //final String alert = json.getString("alert");
        if(ParseUser.getCurrentUser() == null) {
          return;
        } else if(ParseUser.getCurrentUser().getObjectId().equals(userId)) {
          LogEvent.Log(TAG, "Ignoring self notification");
          return;
        }
        final String alert = context.getString(R.string.notif_new_public_cell_alert, cellName);
        final NewPublicCell newPublicCell = new NewPublicCell(cellName, cellId, userId);
        // Retrieve the new object from parse to check if it still exists or the owner has deleted
        ParseQuery parseQuery = ParseQuery.getQuery("PublicCell");
        parseQuery.whereEqualTo("objectId", cellId);
        parseQuery.getFirstInBackground(new GetCallback<ParseObject>() {
          @Override
          public void done(ParseObject parseObject, ParseException e) {
            if(e == null) {
              if(isIsInForeground()) {
                LogEvent.Log(TAG, "App is in Foreground");
                if(Modules.isWeakReferenceValid()) {
                  DialogModules4ReceivedAlerts.showCellJoinDialog(
                    Singleton.INSTANCE.getCurrentActivity(), newPublicCell);
                }
              } else {
                LogEvent.Log(TAG, "App is in Background");
                //Cell411Receiver.super.onReceive(context, intent);
                displayNotification(context, alert);
                if(action.equals("com.parse.push.intent.RECEIVE")) {
                  ArrayList<NewPublicCell> newPublicCellArrayList =
                    StorageOperations.getNewPublicCells(context);
                  if(newPublicCellArrayList == null) {
                    newPublicCellArrayList = new ArrayList<>();
                  }
                  newPublicCellArrayList.add(newPublicCell);
                  StorageOperations.storeNewPublicCells(context, newPublicCellArrayList);
                }
              }
            } else {
              if(e.getCode() == 101) {
                // This newPrivateCell no longer exists, so mute this alert
                LogEvent.Log(TAG,
                  cellName + " newPrivateCell no longer exists, so this alert is muted");
              } else {
                if(isIsInForeground()) {
                  LogEvent.Log(TAG, "App is in Foreground");
                  if(Modules.isWeakReferenceValid()) {
                    DialogModules4ReceivedAlerts.showCellJoinDialog(
                      Singleton.INSTANCE.getCurrentActivity(), newPublicCell);
                  }
                } else {
                  LogEvent.Log(TAG, "App is in Background");
                  //Cell411Receiver.super.onReceive(context, intent);
                  displayNotification(context, alert);
                  if(action.equals("com.parse.push.intent.RECEIVE")) {
                    ArrayList<NewPublicCell> newPublicCellArrayList =
                      StorageOperations.getNewPublicCells(context);
                    if(newPublicCellArrayList == null) {
                      newPublicCellArrayList = new ArrayList<>();
                    }
                    newPublicCellArrayList.add(newPublicCell);
                    StorageOperations.storeNewPublicCells(context, newPublicCellArrayList);
                  }
                }
              }
            }
          }
        });
      } else if(alertType.equalsIgnoreCase("FRIEND_APPROVED")) {
        String name = json.getString("name");
        final String alert = json.getString("alert");
        String assigneeUserId = null;
        if(json.has("assigneeUserId")) {
          assigneeUserId = json.getString("assigneeUserId");
        }
        String userId = null;
        if(json.has("userId")) {
          userId = json.getString("userId");
          if(ParseUser.getCurrentUser() == null) {
            return;
          } else if(ParseUser.getCurrentUser().getObjectId().equals(userId)) {
            LogEvent.Log(TAG, "Ignoring self notification");
            return;
          }
        }
        String taskId = json.getString("taskId");
        String message = context.getString(R.string.notif_friend_approved, name);
        String id = null;
        if(assigneeUserId == null) {
          id = userId;
        }
        final FriendApproved friendApproved = new FriendApproved(name, id, message);
        if(isIsInForeground()) {
          LogEvent.Log(TAG, "App is in Foreground");
          ParseQuery<ParseObject> parseQuery = ParseQuery.getQuery("Task");
          parseQuery.whereEqualTo("objectId", taskId);
          final String finalId = id;
          parseQuery.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> parseObjects, ParseException e) {
              if(e == null) {
                if(parseObjects != null && parseObjects.size() > 0) {
                  final ParseObject taskObject = parseObjects.get(0);
                  ParseQuery<ParseUser> userQuery = ParseUser.getQuery();
                  userQuery.whereEqualTo("objectId", finalId);
                  userQuery.findInBackground(new FindCallback<ParseUser>() {
                    @Override
                    public void done(final List<ParseUser> parseUsers, ParseException e) {
                      if(e == null) {
                        if(parseUsers != null && parseUsers.size() > 0) {
                          ParseRelation<ParseUser> relation = ParseUser.getCurrentUser().
                                                                                          getRelation(
                                                                                            "friends");
                          relation.add(parseUsers.get(0));
                          parseUsers.get(0).saveEventually(new SaveCallback() {
                            @Override
                            public void done(ParseException e) {
                              if(e == null) {
                                // Friend added successfully
                                // taskObject.put("status", "COMPLETE");
                                taskObject.deleteEventually();
                                String email = null;
                                if(parseUsers.get(0).getEmail() != null &&
                                     !parseUsers.get(0).getEmail().isEmpty()) {
                                  email = parseUsers.get(0).getEmail();
                                } else {
                                  email = parseUsers.get(0).getUsername();
                                }
                                User friend =
                                  new User(email, (String) parseUsers.get(0).get("firstName"),
                                    (String) parseUsers.get(0).get("lastName"),
                                    parseUsers.get(0));
                                Singleton.INSTANCE.addFriendToList(friend);
                                if(Modules.isWeakReferenceValid()) {
                                  // update the UI here
                                  FriendsFragment.refreshFriendsList();
                                  DialogModules4ReceivedAlerts.showFriendApprovedDialog(
                                    Singleton.INSTANCE.getCurrentActivity(),
                                    friendApproved.message, friend);
                                }
                              } else {
                                LogEvent.Log(TAG, "Unable to add friend");
                              }
                            }
                          });
                        }
                      } else {
                        LogEvent.Log(TAG, "Unable to retrieve task assignee");
                      }
                    }
                  });
                }
              } else {
              }
            }
          });
        } else {
          LogEvent.Log(TAG, "App is in Background");
          //super.onReceive(context, intent);
          displayNotification(context, alert);
        }
      } else if(alertType.equalsIgnoreCase("SAFE")) {
        final String alert = json.getString("alert");
        if(isIsInForeground()) {
          //String alert = json.getString("alert");
          Dialogs.showAlertDialog(Singleton.INSTANCE.getCurrentActivity(), alert);
        } else {
          LogEvent.Log(TAG, "App is in Background");
          //super.onReceive(context, intent);
          displayNotification(context, alert);
        }
      } else if(alertType.equalsIgnoreCase("CELL_APPROVED")) {
        final String alert = json.getString("alert");
        String cellId = null;
        if(json.has("cellId")) {
          cellId = json.getString("cellId");
        }
        String userId = json.getString("userId");
        if(ParseUser.getCurrentUser() == null) {
          return;
        } else if(ParseUser.getCurrentUser().getObjectId().equals(userId)) {
          LogEvent.Log(TAG, "Ignoring self notification");
          return;
        }
        String cellName = json.getString("cellName");
        if(cellId != null) {
          DataSource ds = new DataSource(context);
          ds.open();
          ds.updateIsRemovedFromChatRoom(cellId, false);
          ds.close();
        }
                /*// Start listening to the newly created newPrivateCell for chat messages
                if (cellId != null) { // the older version until 6.0.2 (iER: 2.0.2) don't have cellId
                    Intent chatServiceIntent = new Intent(context, ChatService.class);
                    chatServiceIntent.putExtra("ChatServiceTask", ChatService.ChatServiceTask.ATTACH_LISTENER);
                    chatServiceIntent.putExtra("entityType", ChatRoom.EntityType.PUBLIC_CELL);
                    chatServiceIntent.putExtra("entityObjectId", cellId);
                    chatServiceIntent.putExtra("entityName", cellName);
                    context.startService(chatServiceIntent);
                }*/
        if(isIsInForeground()) {
          //String alert = json.getString("alert");
          Dialogs.showAlertDialog(Singleton.INSTANCE.getCurrentActivity(), alert);
        } else {
          LogEvent.Log(TAG, "App is in Background");
          //super.onReceive(context, intent);
          displayNotification(context, alert);
        }
      } else if(alertType.equalsIgnoreCase("CELL_DENIED")) {
        final String alert = json.getString("alert");
        String userId = json.getString("userId");
        if(ParseUser.getCurrentUser() == null) {
          return;
        } else if(ParseUser.getCurrentUser().getObjectId().equals(userId)) {
          LogEvent.Log(TAG, "Ignoring self notification");
          return;
        }
        if(isIsInForeground()) {
          //String alert = json.getString("alert");
          Dialogs.showAlertDialog(Singleton.INSTANCE.getCurrentActivity(), alert);
        } else {
          LogEvent.Log(TAG, "App is in Background");
          //super.onReceive(context, intent);
          displayNotification(context, alert);
        }
      } else if(alertType.equalsIgnoreCase("CELL_REMOVED")) {
        final String alert = json.getString("alert");
        String userId = json.getString("userId");
        if(ParseUser.getCurrentUser() == null) {
          return;
        } else if(ParseUser.getCurrentUser().getObjectId().equals(userId)) {
          LogEvent.Log(TAG, "Ignoring self notification");
          return;
        }
        String cellId = json.getString("cellId");
        String cellName = json.getString("cellName");
        DataSource ds = new DataSource(context);
        ds.open();
        ds.updateIsRemovedFromChatRoom(cellId, true);
        ds.close();
        // Stop listening to the public newPrivateCell for chat messages
                /*Intent chatServiceIntent = new Intent(context, ChatService.class);
                chatServiceIntent.putExtra("ChatServiceTask", ChatService.ChatServiceTask.DETACH_LISTENER);
                chatServiceIntent.putExtra("entityType", ChatRoom.EntityType.PUBLIC_CELL);
                chatServiceIntent.putExtra("entityObjectId", cellId);
                chatServiceIntent.putExtra("entityName", cellName);
                context.startService(chatServiceIntent);*/
        if(isIsInForeground()) {
          //String alert = json.getString("alert");
          Dialogs.showAlertDialog(Singleton.INSTANCE.getCurrentActivity(), alert);
        } else {
          LogEvent.Log(TAG, "App is in Background");
          //super.onReceive(context, intent);
          displayNotification(context, alert);
        }
      } else if(alertType.equalsIgnoreCase("CELL_CHANGED")) {
        String alert = json.getString("alert");
        String ownerId = json.getString("userId");
        if(ParseUser.getCurrentUser() == null) {
          return;
        } else if(ParseUser.getCurrentUser().getObjectId().equals(ownerId)) {
          LogEvent.Log(TAG, "Ignoring self notification");
          return;
        }
        String cellId = json.getString("cellId");
        String cellName = json.getString("cellName");
        if(isIsInForeground()) {
          LogEvent.Log(TAG, "App is in Foreground");
          Dialogs.showAlertDialog(Singleton.INSTANCE.getCurrentActivity(), alert);
        } else {
          LogEvent.Log(TAG, "App is in Background");
          //super.onReceive(context, intent);
          displayNotification(context, alert);
          if(action.equals("com.parse.push.intent.RECEIVE")) {
            // update the newPrivateCell with the changes that are made by the owner
          }
        }
      } else if(alertType.equalsIgnoreCase("CELL_DELETED")) {
        //final String alert = json.getString("alert");
        String ownerId = json.getString("userId");
        if(ParseUser.getCurrentUser() == null) {
          return;
        } else if(ParseUser.getCurrentUser().getObjectId().equals(ownerId)) {
          LogEvent.Log(TAG, "Ignoring self notification");
          return;
        }
        String cellId = json.getString("cellId");
        String cellName = json.getString("cellName");
        final String alert = context.getString(R.string.msg_cell_deleted, cellName);
        DataSource ds = new DataSource(context);
        ds.open();
        ds.updateIsRemovedFromChatRoom(cellId, true);
        ds.close();
        if(isIsInForeground()) {
          Dialogs.showAlertDialog(Singleton.INSTANCE.getCurrentActivity(), alert);
        } else {
          LogEvent.Log(TAG, "App is in Background");
          displayNotification(context, alert);
        }
      } else if(alertType.equalsIgnoreCase("CELL_VERIFIED")) {
        String userId = json.getString("userId");
        if(ParseUser.getCurrentUser() == null) {
          return;
        } else if(ParseUser.getCurrentUser().getObjectId().equals(userId)) {
          LogEvent.Log(TAG, "Ignoring self notification");
          return;
        }
        String cellId = json.getString("cellId");
        String cellName = json.getString("cellName");
        final String alert = context.getString(R.string.msg_cell_verified, cellName);
        if(isIsInForeground()) {
          Dialogs.showAlertDialog(Singleton.INSTANCE.getCurrentActivity(), alert);
        } else {
          LogEvent.Log(TAG, "App is in Background");
          displayNotification(context, alert);
        }
      } else if(alertType.equalsIgnoreCase("CELL_REJECTED")) {
        String userId = json.getString("userId");
        if(ParseUser.getCurrentUser() == null) {
          return;
        } else if(ParseUser.getCurrentUser().getObjectId().equals(userId)) {
          LogEvent.Log(TAG, "Ignoring self notification");
          return;
        }
        String cellId = json.getString("cellId");
        String cellName = json.getString("cellName");
        final String alert = context.getString(R.string.msg_cell_rejected, cellName);
        if(isIsInForeground()) {
          Dialogs.showAlertDialog(Singleton.INSTANCE.getCurrentActivity(), alert);
        } else {
          LogEvent.Log(TAG, "App is in Background");
          displayNotification(context, alert);
        }
      } else if(alertType.equalsIgnoreCase("VIDEO")) {
        String name = json.getString("firstName");
        String userId = json.getString("userId");
        final String alert = json.getString("alert");
        long createdAt = json.getLong("createdAt");
        String cell411AlertId = json.getString("cell411AlertId");
        if(ParseUser.getCurrentUser() == null) {
          return;
        } else if(ParseUser.getCurrentUser().getObjectId().equals(userId)) {
          LogEvent.Log(TAG, "Ignoring self notification");
          return;
        }
        int isGlobal = 0;
        if(json.has("isGlobal")) {
          isGlobal = json.getInt("isGlobal");
        }
        Video video = new Video(userId, name, createdAt, true, cell411AlertId, isGlobal);
        if(isIsInForeground()) {
          LogEvent.Log(TAG, "App is in Foreground");
          DialogModules4ReceivedAlerts.showVideoStreamDialog(
            Singleton.INSTANCE.getCurrentActivity(), video);
        } else {
          LogEvent.Log(TAG, "App is in Background");
          //super.onReceive(context, intent);
          displayNotification(context, alert);
        }
                /*Intent chatServiceIntent = new Intent(context, ChatService.class);
                chatServiceIntent.putExtra("ChatServiceTask", ChatService.ChatServiceTask.ATTACH_LISTENER);
                chatServiceIntent.putExtra("entityType", ChatRoom.EntityType.ALERT);
                chatServiceIntent.putExtra("entityObjectId", cell411AlertId);
                chatServiceIntent.putExtra("entityCreatedAt", String.valueOf(createdAt));
                chatServiceIntent.putExtra("entityName", "Video");
                context.startService(chatServiceIntent);*/
      } else if(alertType.equalsIgnoreCase("PHOTO")) {
        String name = json.getString("firstName");
        final String alert = json.getString("alert");
        String userId = json.getString("userId");
        long createdAt = json.getLong("createdAt");
        String cell411AlertId = json.getString("cell411AlertId");
        if(ParseUser.getCurrentUser() == null) {
          return;
        } else if(ParseUser.getCurrentUser().getObjectId().equals(userId)) {
          LogEvent.Log(TAG, "Ignoring self notification");
          return;
        }
        if(json.has("key")) {
          final String key = json.getString("key");
          new SendAck4ReceivedAlertApiCall(context, ParseUser.getCurrentUser().getObjectId(),
            key).execute();
        }
        int isGlobal = 0;
        if(json.has("isGlobal")) {
          isGlobal = json.getInt("isGlobal");
        }
        Video video = new Video(userId, name, createdAt, true, cell411AlertId, isGlobal);
        if(isIsInForeground()) {
          LogEvent.Log(TAG, "App is in Foreground");
          DialogModules4ReceivedAlerts.showPhotoAlertDialog(Singleton.INSTANCE.getCurrentActivity(),
            video, null);
        } else {
          LogEvent.Log(TAG, "App is in Background");
          //super.onReceive(context, intent);
          displayNotification(context, alert);
        }
                /*Intent chatServiceIntent = new Intent(context, ChatService.class);
                chatServiceIntent.putExtra("ChatServiceTask", ChatService.ChatServiceTask.ATTACH_LISTENER);
                chatServiceIntent.putExtra("entityType", ChatRoom.EntityType.ALERT);
                chatServiceIntent.putExtra("entityObjectId", cell411AlertId);
                chatServiceIntent.putExtra("entityCreatedAt", String.valueOf(createdAt));
                chatServiceIntent.putExtra("entityName", "Photo");
                context.startService(chatServiceIntent);*/
      } else if(alertType.equalsIgnoreCase("PHOTO_CELL")) {
        String name = json.getString("firstName");
        String userId = json.getString("userId");
        final String alert = json.getString("alert");
        String cellName = json.getString("cellName");
        long createdAt = json.getLong("createdAt");
        String cell411AlertId = json.getString("cell411AlertId");
        if(ParseUser.getCurrentUser() == null) {
          return;
        } else if(ParseUser.getCurrentUser().getObjectId().equals(userId)) {
          LogEvent.Log(TAG, "Ignoring self notification");
          return;
        }
        if(json.has("key")) {
          final String key = json.getString("key");
          new SendAck4ReceivedAlertApiCall(context, ParseUser.getCurrentUser().getObjectId(),
            key).execute();
        }
        int isGlobal = 0;
        if(json.has("isGlobal")) {
          isGlobal = json.getInt("isGlobal");
        }
        Video video = new Video(userId, name, createdAt, true, cell411AlertId, isGlobal);
        if(isIsInForeground()) {
          LogEvent.Log(TAG, "App is in Foreground");
          DialogModules4ReceivedAlerts.showPhotoAlertDialog(Singleton.INSTANCE.getCurrentActivity(),
            video, cellName);
        } else {
          LogEvent.Log(TAG, "App is in Background");
          //super.onReceive(context, intent);
          displayNotification(context, alert);
        }
                /*Intent chatServiceIntent = new Intent(context, ChatService.class);
                chatServiceIntent.putExtra("ChatServiceTask", ChatService.ChatServiceTask.ATTACH_LISTENER);
                chatServiceIntent.putExtra("entityType", ChatRoom.EntityType.ALERT);
                chatServiceIntent.putExtra("entityObjectId", cell411AlertId);
                chatServiceIntent.putExtra("entityCreatedAt", String.valueOf(createdAt));
                chatServiceIntent.putExtra("entityName", "Photo");
                context.startService(chatServiceIntent);*/
      } else if(alertType.equalsIgnoreCase("MESSAGE")) {
        String senderId = json.getString("senderId");
        String senderFirstName = json.getString("senderFirstName");
        String senderLastName = json.getString("senderLastName");
        String messageType = json.getString("msgType");
        String message = json.getString("msg");
        long time = json.getLong("time");
        long entityCreatedAtMillis = json.getLong("entityCreatedAt");
        ChatRoom.EntityType entityType = ChatRoom.EntityType.valueOf(json.getString("entityType"));
        String entityObjectId = json.getString("entityObjectId");
        String entityName = json.getString("entityName");
        if(ParseUser.getCurrentUser() == null) {
          return;
        } else if(ParseUser.getCurrentUser().getObjectId().equals(senderId)) {
          LogEvent.Log(TAG, "Ignoring self notification");
          return;
        }
        String entityCreatedAt = String.valueOf(entityCreatedAtMillis);
        Chat chat = new Chat(senderId, senderFirstName, senderLastName, message, messageType, time);
        chat.entityType = entityType.toString();
        chat.entityName = entityName;
        chat.entityObjectId = entityObjectId;
        chat.entityCreatedAt = entityCreatedAt;
        chat.isReceived = true;
        if(MainActivity.INSTANCE != null) {
          if(ChatActivity.INSTANCE != null && Singleton.INSTANCE.getCurrentActivity() != null &&
               Singleton.INSTANCE.getCurrentActivity() instanceof ChatActivity &&
               ((ChatActivity) Singleton.INSTANCE.getCurrentActivity()).entityObjectId.equals(
                 entityObjectId)) {
                        /*ChatRoom chatRoom = new ChatRoom(entityType, entityObjectId, entityName,
                                entityCreatedAt, senderId, senderFirstName, senderLastName, message,
                                time, messageType, true);
                        DataSource ds = new DataSource(context);
                        ds.open();
                        ds.insertOrUpdateChatRoom(chatRoom, false);
                        ds.close();
                        ((ChatActivity) INSTANCE.getCurrentActivity())
                                .newMessageArrived(chat);*/
          } else {
            ChatRoom chatRoom =
              new ChatRoom(entityType, entityObjectId, entityName, entityCreatedAt, senderId,
                senderFirstName, senderLastName, message, time, messageType, true);
            DataSource ds = new DataSource(context);
            ds.open();
            ds.insertOrUpdateChatRoom(chatRoom, true);
            ChatRoomSettings chatRoomSettings = ds.getSettings(entityObjectId);
            ds.close();
            if(chatRoomSettings != null) {
              if(System.currentTimeMillis() >= chatRoomSettings.muteUntil) {
                DataSource dataSource = new DataSource(context);
                dataSource.open();
                chatRoomSettings.muteUntil = 0;
                chatRoomSettings.muteFor = null;
                chatRoomSettings.muteTime = 0;
                chatRoomSettings.notificationDisabled = false;
                chatRoomSettings.muteEnabled = false;
                dataSource.insertOrUpdateSettings(chatRoomSettings, entityObjectId);
                dataSource.close();
              }
              chat.notificationDisabled = chatRoomSettings.notificationDisabled;
              chat.muteEnabled = chatRoomSettings.muteEnabled;
            }
            ArrayList<Chat> chats = StorageOperations.getUnseenChats(context);
            if(chats == null) {
              chats = new ArrayList<>();
            }
            chats.add(chat);
            StorageOperations.storeUnseenChats(Singleton.INSTANCE, chats);
            // Display notification
            if(!chat.notificationDisabled) {
              displayMessageNotification(chats, chat.muteEnabled, context);
            }
            // Since the app is in foreground, send the data to the Activity directly
            Intent localIntent = new Intent(MainActivity.BROADCAST_ACTION_MESSAGE)
                                   // Puts the status into the Intent
                                   .putExtra("chat", chat);
            // Broadcasts the Intent to receivers in this app.
            LocalBroadcastManager.getInstance(context).sendBroadcast(localIntent);
          }
        } else {
          // Save the request location data, so that we can process it when user launches
          // the app, either by tapping on notification or by tapping on app icon
          ChatRoom chatRoom =
            new ChatRoom(entityType, entityObjectId, entityName, entityCreatedAt, senderId,
              senderFirstName, senderLastName, message, time, messageType, true);
          DataSource ds = new DataSource(context);
          ds.open();
          ds.insertOrUpdateChatRoom(chatRoom, true);
          ChatRoomSettings chatRoomSettings = ds.getSettings(entityObjectId);
          ds.close();
          if(chatRoomSettings != null) {
            if(System.currentTimeMillis() >= chatRoomSettings.muteUntil) {
              DataSource dataSource = new DataSource(context);
              dataSource.open();
              chatRoomSettings.muteUntil = 0;
              chatRoomSettings.muteFor = null;
              chatRoomSettings.muteTime = 0;
              chatRoomSettings.notificationDisabled = false;
              chatRoomSettings.muteEnabled = false;
              dataSource.insertOrUpdateSettings(chatRoomSettings, entityObjectId);
              dataSource.close();
            }
            chat.notificationDisabled = chatRoomSettings.notificationDisabled;
            chat.muteEnabled = chatRoomSettings.muteEnabled;
          }
          ArrayList<Chat> chats = StorageOperations.getUnseenChats(context);
          if(chats == null) {
            chats = new ArrayList<>();
          }
          chats.add(chat);
          StorageOperations.storeUnseenChats(Singleton.INSTANCE, chats);
          // Display notification
          if(!chat.notificationDisabled) {
            displayMessageNotification(chats, chat.muteEnabled, context);
          }
        }
                /*if (isInForeground) {
                    LogEvent.Log(TAG, "App is in Foreground");
                    Dialogs.showAlertDialog(INSTANCE.getCurrentActivity(), alert);
                } else {
                    LogEvent.Log(TAG, "App is in Background");
                    //super.onReceive(context, intent);
                    displayNotification(context, alert);
                    if (action.equals("com.parse.push.intent.RECEIVE")) {
                        ArrayList<String> otherAlerts = StorageOperations.getOtherAlerts(context);
                        if (otherAlerts == null)
                            otherAlerts = new ArrayList<>();
                        otherAlerts.add(new String(alert));
                        StorageOperations.storeOtherAlerts(context, otherAlerts);
                    }
                }*/
      } else if(alertType.equalsIgnoreCase("CUSTOM")) {
        String alert = json.getString("alert");
        if(isIsInForeground()) {
          LogEvent.Log(TAG, "App is in Foreground");
          Dialogs.showAlertDialog(Singleton.INSTANCE.getCurrentActivity(), alert);
        } else {
          LogEvent.Log(TAG, "App is in Background");
          //super.onReceive(context, intent);
          displayNotification(context, alert);
          if(action.equals("com.parse.push.intent.RECEIVE")) {
            ArrayList<String> otherAlerts = StorageOperations.getOtherAlerts(context);
            if(otherAlerts == null) {
              otherAlerts = new ArrayList<>();
            }
            otherAlerts.add(new String(alert));
            StorageOperations.storeOtherAlerts(context, otherAlerts);
          }
        }
      } else if(alertType.equalsIgnoreCase("BG")) {
      } else if(alertType.equalsIgnoreCase("IGNORE")) {
        String alert = json.getString("alert");
        LogEvent.Log(TAG, "Received IGNORE alert: " + alert);
        if(isIsInForeground()) {
          LogEvent.Log(TAG, "App is in Foreground");
        } else {
          LogEvent.Log(TAG, "App is in Background");
        }
      } else {
        if(isIsInForeground()) {
          Dialogs.showAlertDialog(Singleton.INSTANCE.getCurrentActivity(),
            context.getString(R.string.dialog_message_install_latest_version));
        } else {
          final String alert = json.getString("alert");
          LogEvent.Log(TAG, "App is in Background");
          //super.onReceive(context, intent);
          displayNotification(context,
            context.getString(R.string.dialog_message_install_latest_version));
          if(action.equals("com.parse.push.intent.RECEIVE")) {
            ArrayList<String> otherAlerts = StorageOperations.getOtherAlerts(context);
            if(otherAlerts == null) {
              otherAlerts = new ArrayList<>();
            }
            otherAlerts.add(
              new String(context.getString(R.string.dialog_message_install_latest_version)));
            StorageOperations.storeOtherAlerts(context, otherAlerts);
          }
        }
      }
    } catch(JSONException e) {
      e.printStackTrace();
    }
  }

  public void displayNotification(Context context, String content) {
    NotificationCompat.Builder mBuilder =
      new NotificationCompat.Builder(context, CHANNEL_ID_EMERGENCY_ALERT).setSmallIcon(
        R.drawable.logo).setContentTitle(context.getString(R.string.app_name)).setContentText(
        content).setAutoCancel(true);
    mBuilder.setPriority(Notification.PRIORITY_HIGH);
    // Creates an explicit intent for an Activity in your app
    Intent resultIntent = new Intent(context, MainActivity.class);
    resultIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);
    // The stack builder object will contain an artificial back stack for the
    // started Activity.
    // This ensures that navigating backward from the Activity leads out of
    // your application to the Home screen.
    TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
    // Adds the back stack for the Intent (but not the Intent itself)
    stackBuilder.addParentStack(MainActivity.class);
    // Adds the Intent that starts the Activity to the top of the stack
    stackBuilder.addNextIntent(resultIntent);
    int id = (int) (System.currentTimeMillis() / 1000);
    PendingIntent resultPendingIntent =
      stackBuilder.getPendingIntent(id, PendingIntent.FLAG_UPDATE_CURRENT);
    mBuilder.setContentIntent(resultPendingIntent);
    NotificationManager mNotificationManager =
      (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
    // mId allows you to update the notification later on.
    NotificationCompat.BigTextStyle notificationStyle = new NotificationCompat.BigTextStyle();
    notificationStyle.bigText(content);
    mBuilder.setStyle(notificationStyle);
    Notification notification = mBuilder.build();
    //notification.sound = Uri.parse("android.resource://" + context.getPackageName() + "/" + R.raw.cell411alert);
    notification.defaults =
      Notification.DEFAULT_SOUND | Notification.DEFAULT_LIGHTS | Notification.DEFAULT_VIBRATE;
    mNotificationManager.notify(id, notification);
  }

  public void displayNewMemberJoinedNotification(Context context, String content,
                                                 String userObjectId, String username,
                                                 String name) {
    NotificationCompat.Builder mBuilder =
      new NotificationCompat.Builder(context, CHANNEL_ID_MISCELLANEOUS).setSmallIcon(
        R.drawable.logo).setContentTitle(context.getString(R.string.app_name)).setContentText(
        content).setAutoCancel(true);
    mBuilder.setPriority(Notification.PRIORITY_HIGH);
    // Creates an explicit intent for an Activity in your app
    Intent resultIntent = new Intent(context, NewMemberJoinedActivity.class);
    resultIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);
    resultIntent.putExtra("userObjectId", userObjectId);
    resultIntent.putExtra("username", username);
    resultIntent.putExtra("name", name);
    // The stack builder object will contain an artificial back stack for the
    // started Activity.
    // This ensures that navigating backward from the Activity leads out of
    // your application to the Home screen.
    TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
    // Adds the back stack for the Intent (but not the Intent itself)
    stackBuilder.addParentStack(MainActivity.class);
    // Adds the Intent that starts the Activity to the top of the stack
    stackBuilder.addNextIntent(resultIntent);
    int id = (int) (System.currentTimeMillis() / 1000);
    PendingIntent resultPendingIntent =
      stackBuilder.getPendingIntent(id, PendingIntent.FLAG_UPDATE_CURRENT);
    mBuilder.setContentIntent(resultPendingIntent);
    NotificationManager mNotificationManager =
      (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
    // mId allows you to update the notification later on.
    Notification notification = mBuilder.build();
    //notification.sound = Uri.parse("android.resource://" + context.getPackageName() + "/" + R.raw.cell411alert);
    notification.defaults =
      Notification.DEFAULT_SOUND | Notification.DEFAULT_LIGHTS | Notification.DEFAULT_VIBRATE;
    mNotificationManager.notify(id, notification);
  }

  /**
   * Method to display the notification for the messages received on Public Cell or Alerts
   *
   * @param chats array of chats that are not seen by the current user yet
   */
  private void displayMessageNotification(ArrayList<Chat> chats, boolean muteEnabled,
                                          Context context) {
    LogEvent.Log(TAG, "displayMessageNotification invoked");
    int[] textViewIds = new int[7];
    textViewIds[0] = R.id.message_1;
    textViewIds[1] = R.id.message_2;
    textViewIds[2] = R.id.message_3;
    textViewIds[3] = R.id.message_4;
    textViewIds[4] = R.id.message_5;
    textViewIds[5] = R.id.message_6;
    textViewIds[6] = R.id.message_7;
    // Create custom layout using RemoteViews for notification's normal view (64dp)
    RemoteViews normalView =
      new RemoteViews(context.getPackageName(), R.layout.layout_notification);
    normalView.setTextViewText(R.id.title_notification, context.getString(R.string.app_name));
    // Create custom layout using RemoteViews for notification's big/expanded view (120dp)
    RemoteViews expandedView;
    // Logic to calculate total number of cells
    Map<String, Integer> cells = new HashMap<>();
    for(int i = 0; i < chats.size(); i++) {
      if(!chats.get(
        i).notificationDisabled) // remove the count of the chats for chat rooms those are having notification disabled
      {
        cells.put(chats.get(i).entityName, 0);
      }
    }
    int cellCount = cells.size();
    int totalChats = 0;
    // calculate total chats
    for(int i = 0; i < chats.size(); i++) {
      if(!chats.get(
        i).notificationDisabled) // remove the count of the chats those are having notification disabled
      {
        totalChats++;
      }
    }
    String messageCount;
    if(totalChats == 1) {
      String msg = chats.get(0).senderFirstName + " " + chats.get(0).senderLastName + " @ " +
                     chats.get(0).entityName + " : " + chats.get(0).msg;
      // set normal view
      normalView.setTextViewText(R.id.message_1, msg);
      // set expanded view
      expandedView =
        new RemoteViews(context.getPackageName(), R.layout.layout_notification_expanded);
      expandedView.setTextViewText(R.id.message_1, msg);
      expandedView.setTextViewText(R.id.messages_count, "1 new message");
      messageCount = "1 new message";
    } else {
      // set normal view
      if(cellCount == 1) {
        normalView.setTextViewText(R.id.message_1, totalChats + " new messages");
      } else {
        normalView.setTextViewText(R.id.message_1,
          totalChats + " messages from " + cellCount + " cells");
      }
      // set expanded view
      if(totalChats == 2) {
        expandedView =
          new RemoteViews(context.getPackageName(), R.layout.layout_notification_expanded_2);
      } else if(totalChats == 3) {
        expandedView =
          new RemoteViews(context.getPackageName(), R.layout.layout_notification_expanded_3);
      } else if(totalChats == 4) {
        expandedView =
          new RemoteViews(context.getPackageName(), R.layout.layout_notification_expanded_4);
      } else if(totalChats == 5) {
        expandedView =
          new RemoteViews(context.getPackageName(), R.layout.layout_notification_expanded_5);
      } else if(totalChats == 6) {
        expandedView =
          new RemoteViews(context.getPackageName(), R.layout.layout_notification_expanded_6);
      } else {
        expandedView =
          new RemoteViews(context.getPackageName(), R.layout.layout_notification_expanded_7);
      }
      for(int i = 0; i < chats.size(); i++) {
        if(i == 7) {
          break;
        }
        if(chats.get(
          i).notificationDisabled) // remove the count of the chats those are having notification disabled
        {
          continue;
        }
        String msg = chats.get(i).senderFirstName + " " + chats.get(i).senderLastName + " @ " +
                       chats.get(i).entityName + " : " + chats.get(i).msg;
        expandedView.setTextViewText(textViewIds[i], msg);
      }
      //String msg = chats.get(0).senderFirstName + " " + chats.get(0).senderLastName + " @ " + chats.get(0).circleTag + " : " + chats.get(0).msg;
      //expandedView.setTextViewText(R.id.message_1, msg);
      if(cellCount == 1) {
        messageCount = totalChats + " messages from 1 cell";
        expandedView.setTextViewText(R.id.messages_count, messageCount);
      } else {
        messageCount = totalChats + " messages from " + cellCount + " cells";
        expandedView.setTextViewText(R.id.messages_count, messageCount);
      }
    }
    expandedView.setTextViewText(R.id.title_notification, context.getString(R.string.app_name));
    NotificationCompat.Builder mBuilder =
      new NotificationCompat.Builder(context, CHANNEL_ID_MESSAGE).setContent(
        normalView).setSmallIcon(R.drawable.logo).setContentTitle(
        context.getString(R.string.app_name)).setContentText(messageCount).setAutoCancel(true);
    Intent resultIntent;
    // Check if the new message is on single newPrivateCell or chat room
    if(cellCount == 1) {
      // Check if app is open
      if(Singleton.INSTANCE != null && Singleton.INSTANCE.getCurrentActivity() != null) {
        // we can directly go to the chat activity
        resultIntent = new Intent(context, ChatActivity.class);
        resultIntent.putExtra("entityType", chats.get(0).entityType);
        resultIntent.putExtra("entityObjectId", chats.get(0).entityObjectId);
        resultIntent.putExtra("entityName", chats.get(0).entityName);
        resultIntent.putExtra("time", chats.get(0).entityCreatedAt);
      } else {
        resultIntent = new Intent(context, SplashActivity.class);
        resultIntent.putExtra("entityType", chats.get(0).entityType);
        resultIntent.putExtra("entityObjectId", chats.get(0).entityObjectId);
        resultIntent.putExtra("entityName", chats.get(0).entityName);
        resultIntent.putExtra("time", chats.get(0).entityCreatedAt);
        resultIntent.putExtra("gotoChatScreen", true);
      }
    } else {
      // Check if the app is already open, then we should go to the chat tab in the MainActivity
      if(Singleton.INSTANCE != null && Singleton.INSTANCE.getCurrentActivity() != null) {
        resultIntent = new Intent(context, MainActivity.class);
      } else {
        // We need to goto the activity from Splash Screen so that app can make the app
        // version api check
        resultIntent = new Intent(context, SplashActivity.class);
      }
      // user should go to the chats tab
      resultIntent.putExtra("gotoChatTab", true);
    }
    // The stack builder object will contain an artificial back stack for the
    // started Activity.
    // This ensures that navigating backward from the Activity leads out of
    // your application to the Home screen.
    TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
    // Adds the back stack for the Intent (but not the Intent itself)
    stackBuilder.addParentStack(MainActivity.class);
    // Adds the Intent that starts the Activity to the top of the stack
    stackBuilder.addNextIntent(resultIntent);
    PendingIntent resultPendingIntent =
      stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
    mBuilder.setContentIntent(resultPendingIntent);
    NotificationManager mNotificationManager =
      (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
    // mId allows you to update the notification later on.
    Notification notification = mBuilder.build();
    notification.priority = Notification.PRIORITY_HIGH;
    if(!muteEnabled) {
      notification.defaults =
        Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE | Notification.DEFAULT_LIGHTS;
    } else {
      notification.defaults = Notification.DEFAULT_LIGHTS;
    }
    // check if version is 4.1 as expanded views can not appear on earlier versions
    if(Build.VERSION.SDK_INT >= 16) {
      notification.bigContentView = expandedView;
    }
    mNotificationManager.notify(1010, notification);
  }

  /*private static void updateReceivedBy(String cell411AlertId) {
      ParseQuery<ParseObject> query = ParseQuery.getQuery("Cell411Alert");
      query.whereEqualTo("objectId", cell411AlertId);
      query.findInBackground(new FindCallback<ParseObject>() {
          @Override
          public void done(List<ParseObject> parseObjects, ParseException e) {
              if (e == null) {
                  final ParseObject cell411AlertObject = parseObjects.get(0);
                  ParseRelation relation = cell411AlertObject
                          .getRelation("receivedBy");
                  relation.add(ParseUser.getCurrentUser());
                  cell411AlertObject.saveInBackground(new SaveCallback() {
                      @Override
                      public void done(ParseException e) {
                          if (e != null) {
                              cell411AlertObject.saveEventually();
                          }
                      }
                  });
              }
          }
      });
  }*/
  private static class SendAck4ReceivedAlertApiCall extends AsyncTask<String, Void, Boolean> {
    private Context context;
    private String key;
    private String userId;
    private int isLive;

    public SendAck4ReceivedAlertApiCall(final Context context, final String userId,
                                        final String key) {
      this.context = context;
      this.userId = userId;
      this.key = key;

      isLive = (context.getResources().getBoolean(R.bool.enable_publish) ? 1 : 0);
    }

    @Override
    protected Boolean doInBackground(String... params) {
      String urlBroadcastAlert;
      urlBroadcastAlert = ACK_ALERT;
      try {
        JSONObject jsonObject = new JSONObject();
        jsonObject.accumulate("key", key);
        jsonObject.accumulate("userId", userId);
        jsonObject.accumulate("clientFirmId", 1);
        jsonObject.accumulate("isLive", isLive);
        String json = jsonObject.toString();
        LogEvent.Log(TAG, "json: " + json);
        URL url = new URL(urlBroadcastAlert);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setReadTimeout(context.getResources().getInteger(R.integer.maximum_timeout_to_server));
        conn.setConnectTimeout(
          context.getResources().getInteger(R.integer.maximum_timeout_to_server));
        conn.setRequestMethod("POST");
        conn.setDoInput(true);
        conn.setDoOutput(true);
        OutputStream os = conn.getOutputStream();
        byte[] outputBytes = json.getBytes("UTF-8");
        os.write(outputBytes);
        int responseCode = conn.getResponseCode();
        LogEvent.Log(TAG, "responseCode : " + responseCode);
        String response = "";
        if(responseCode == HttpsURLConnection.HTTP_OK) {
          LogEvent.Log(TAG, "HTTP_OK");
          String line;
          BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
          while((line = br.readLine()) != null) {
            response += line;
          }
        } else {
          LogEvent.Log(TAG, "HTTP_NOT_OK");
          response = "";
        }
        LogEvent.Log("json response: ", response);
        return true;
      } catch(Exception e) {
        return false;
      }
    }
  }
}
