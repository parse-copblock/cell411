package cell411.services;

import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.firebase.jobdispatcher.JobParameters;
import com.firebase.jobdispatcher.JobService;
import com.safearx.cell411.R;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import cell411.Singleton;
import cell411.activity.AlertDetailActivity2;
import cell411.models.DownloadVideoTask;
import cell411.utils.LogEvent;
import cell411.utils.StorageOperations;

public class DownloadJobService extends JobService {
  private static final String TAG = DownloadJobService.class.getSimpleName();

  @Override
  public boolean onStartJob(JobParameters job) {
    if(job.getExtras() != null) {
      String videoLink = job.getExtras().getString("videoLink");
      String cell411AlertId = job.getExtras().getString("cell411AlertId");
      ArrayList<DownloadVideoTask> downloadVideoTasks =
        StorageOperations.getDownloadVideoTask(this);
      if(downloadVideoTasks == null) {
        downloadVideoTasks = new ArrayList<>();
      }
      downloadVideoTasks.add(new DownloadVideoTask(videoLink, cell411AlertId));
      StorageOperations.storeDownloadVideoTask(this, downloadVideoTasks);
      new DownloadVideoAsyncTask(job, cell411AlertId).execute(videoLink);
    } else {
      ArrayList<DownloadVideoTask> downloadVideoTasks =
        StorageOperations.getDownloadVideoTask(this);
      if(downloadVideoTasks != null && downloadVideoTasks.size() != 0) {
        for(int i = 0; i < downloadVideoTasks.size(); i++) {
          new DownloadVideoAsyncTask(job, downloadVideoTasks.get(i).cell411AlertId).execute(
            downloadVideoTasks.get(i).videoLink);
        }
      } else {
        //stopSelf();
        jobFinished(job, false);
      }
    }
    return true; // Answers the question: "Is there still work going on?"
  }

  @Override
  public boolean onStopJob(JobParameters job) {
    return false; // Answers the question: "Should this job be retried?"
  }

  private File getOutputMediaFile(String folderName) {
    // To be safe, you should check that the SDCard is mounted
    // using Environment.getExternalStorageState() before doing this.
    File mediaStorageDir =
      new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MOVIES),
        folderName);
    // This location works best if you want the created images to be shared
    // between applications and persist after your app has been uninstalled.
    // Create the storage directory if it does not exist
    if(!mediaStorageDir.exists()) {
      if(!mediaStorageDir.mkdirs()) {
        Log.d("Cell411", "failed to create directory");
        return null;
      }
    }
    // Create a media file name
    String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
    File mediaFile =
      new File(mediaStorageDir.getPath() + File.separator + "VID" + timeStamp + ".mp4");
    return mediaFile;
  }

  // add picture to the gallery
  private void galleryAddVideo(File f2) {
    Intent mediaScanIntent = new Intent("android.intent.action.MEDIA_SCANNER_SCAN_FILE");
    Uri contentUri = Uri.fromFile(f2);
    mediaScanIntent.setData(contentUri);
    sendBroadcast(mediaScanIntent);
  }

  private class DownloadVideoAsyncTask extends AsyncTask<String, Integer, Boolean> {
    private JobParameters job;
    private String cell411AlertId;

    public DownloadVideoAsyncTask(JobParameters job, String cell411AlertId) {
      this.job = job;
      this.cell411AlertId = cell411AlertId;
    }

    protected Boolean doInBackground(String... params) {
      try {
        URL u = new URL(params[0]);
        LogEvent.Log("Video Download status", "URL: " + u.toString());
        URLConnection conn = u.openConnection();
        int contentLength = conn.getContentLength();
        LogEvent.Log("Video Download status", "Content Length: " + contentLength);
        DataInputStream stream = new DataInputStream(u.openStream());
        File videoFile = getOutputMediaFile(getString(R.string.app_name));
        DataOutputStream fos = new DataOutputStream(new FileOutputStream(videoFile));
        byte data[] = new byte[1024];
        long total = 0;
        int count;
        int totalBytesToRead = (contentLength > 1024) ? 1024 : contentLength;
        LogEvent.Log(TAG, "totalBytesToRead: " + totalBytesToRead);
        while((count = stream.read(data, 0, totalBytesToRead)) != -1) {
          total += count;
          // publishing the progress...
          publishProgress((int) ((total * 100) / contentLength));
          // writing data to file
          fos.write(data, 0, count);
          totalBytesToRead =
            ((contentLength - total) > 1024) ? 1024 : (int) (contentLength - total);
          LogEvent.Log(TAG, "totalBytesToRead: " + totalBytesToRead);
        }
        //byte[] buffer;
        //if (contentLength != -1)
        //buffer = new byte[contentLength];
        //else
        //buffer = new byte[200000];
        //stream.readFully(buffer);
        stream.close();
        //fos.write(buffer);
        fos.flush();
        fos.close();
        LogEvent.Log("Video Download status", "Video Saved");
        galleryAddVideo(videoFile);
        ArrayList<DownloadVideoTask> downloadVideoTasks =
          StorageOperations.getDownloadVideoTask(DownloadJobService.this);
        if(downloadVideoTasks == null) {
          downloadVideoTasks = new ArrayList<>();
        }
        for(int i = 0; i < downloadVideoTasks.size(); i++) {
          if(downloadVideoTasks.get(i).cell411AlertId.equals(cell411AlertId)) {
            downloadVideoTasks.remove(i);
            StorageOperations.storeDownloadVideoTask(DownloadJobService.this, downloadVideoTasks);
            break;
          }
        }
        return true;
      } catch(FileNotFoundException e) {
        e.printStackTrace();
        LogEvent.Log("Video Download status", "FileNotFoundException");
        return null;
        // swallow a 404
      } catch(IOException e) {
        e.printStackTrace();
        LogEvent.Log("Video Download status", "IOException");
        return null;
        // swallow a 404
      }
    }

    @Override
    protected void onProgressUpdate(Integer... progress) {
      super.onProgressUpdate(progress);
      LogEvent.Log(TAG, "progress: " + progress[0]);
      Intent localIntent2 =
        new Intent(AlertDetailActivity2.BROADCAST_ACTION_VIDEO_DOWNLOAD_PROGRESS)
          // Puts the status into the Intent
          .putExtra("progress", (int) progress[0]).putExtra("cell411AlertId", cell411AlertId);
      // Broadcasts the Intent to receivers in this app.
      LocalBroadcastManager.getInstance(DownloadJobService.this).sendBroadcast(localIntent2);
    }

    protected void onPostExecute(Boolean result) {
      if(result != null) {
        Intent localIntent2 =
          new Intent(AlertDetailActivity2.BROADCAST_ACTION_VIDEO_DOWNLOAD_PROGRESS)
            // Puts the status into the Intent
            .putExtra("cell411AlertId", cell411AlertId).putExtra("status", "Downloaded");
        // Broadcasts the Intent to receivers in this app.
        LocalBroadcastManager.getInstance(DownloadJobService.this).sendBroadcast(localIntent2);
        Singleton.sendToast(R.string.video_downloaded_successfully);
      } else {
        Intent localIntent2 =
          new Intent(AlertDetailActivity2.BROADCAST_ACTION_VIDEO_DOWNLOAD_PROGRESS)
            // Puts the status into the Intent
            .putExtra("cell411AlertId", cell411AlertId).putExtra("status", "Failed");
        // Broadcasts the Intent to receivers in this app.
        LocalBroadcastManager.getInstance(DownloadJobService.this).sendBroadcast(localIntent2);
        Singleton.sendToast(R.string.video_download_failed);
      }
      ArrayList<DownloadVideoTask> downloadVideoTasks =
        StorageOperations.getDownloadVideoTask(DownloadJobService.this);
      if(downloadVideoTasks == null || downloadVideoTasks.size() == 0) {
        //stopSelf();
        jobFinished(job, false);
      }
    }
  }
}