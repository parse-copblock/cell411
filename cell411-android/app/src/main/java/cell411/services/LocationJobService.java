package cell411.services;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Looper;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.firebase.jobdispatcher.JobParameters;
import com.firebase.jobdispatcher.JobService;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.parse.ParseGeoPoint;
import com.parse.ParseUser;

import cell411.Singleton;
import cell411.utils.LogEvent;

public class LocationJobService extends JobService {
  public static final String BROADCAST_ACTION_LOCATION_RECEIVED =
    "com.safearx.cell411.LocationReceiver";
  private final String TAG = LocationJobService.class.getSimpleName();
  private FusedLocationProviderClient mFusedLocationClient;
  private Location mLastLocation;
  private LocationRequest mLocationRequest;
  private LocationCallback mLocationCallback;

  @Override
  public void onCreate() {
    super.onCreate();
    LogEvent.Log(TAG, "onCreate() invoked");
    mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
    createLocationRequest();
  }

  protected void createLocationRequest() {
    LogEvent.Log(TAG, "createLocationRequest() invoked");
    mLocationRequest = new LocationRequest();
    mLocationRequest.setSmallestDisplacement(150); // 150 meters
    mLocationRequest.setFastestInterval(5000); // 5 seconds
    mLocationRequest.setInterval(300000); // 5 minutes
    mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
    LogEvent.Log(TAG, "Location request created");
  }

  /**
   * Creates a callback for receiving location events.
   */
  private void createLocationCallback(final JobParameters job) {
    LogEvent.Log(TAG, "createLocationCallback() invoked");
    mLocationCallback = new LocationCallback() {
      @Override
      public void onLocationResult(LocationResult locationResult) {
        super.onLocationResult(locationResult);
        LogEvent.Log(TAG, "onLocationResult() invoked");
        mLastLocation = locationResult.getLastLocation();
        if(mLastLocation != null) {
          LogEvent.Log(TAG, "location is available");
          Singleton.INSTANCE.setLatitude(mLastLocation.getLatitude());
          Singleton.INSTANCE.setLongitude(mLastLocation.getLongitude());
          if(ParseUser.getCurrentUser() != null) {
            // Update user's current location
            ParseGeoPoint currentUserLocation =
              new ParseGeoPoint(mLastLocation.getLatitude(), mLastLocation.getLongitude());
            ParseUser.getCurrentUser().put("location", currentUserLocation);
            ParseUser.getCurrentUser().saveEventually();
            Intent localIntent = new Intent(BROADCAST_ACTION_LOCATION_RECEIVED)
                                   // Puts the status into the Intent
                                   .putExtra("location", mLastLocation);
            // Broadcasts the Intent to receivers in this app.
            LocalBroadcastManager.getInstance(LocationJobService.this).sendBroadcast(localIntent);
          } else {
            //stopSelf();
            jobFinished(job, false);
          }
        } else {
          LogEvent.Log(TAG, "location is null");
        }
      }
    };
  }

  /**
   * Removes location updates from the FusedLocationApi.
   */
  private void stopLocationUpdates() {
    LogEvent.Log(TAG, "stopLocationUpdates() invoked");
    if(mLocationCallback == null || mFusedLocationClient == null) {
      return;
    }
    // It is a good practice to remove location requests when the activity is in a paused or
    // stopped state. Doing so helps battery performance and is especially
    // recommended in applications that request frequent location updates.
    mFusedLocationClient.removeLocationUpdates(mLocationCallback).addOnCompleteListener(
      new OnCompleteListener<Void>() {
        @Override
        public void onComplete(@NonNull Task<Void> task) {
          LogEvent.Log(TAG, "Location update stopped");
        }
      });
  }

  @Override
  public boolean onStartJob(final JobParameters job) {
    LogEvent.Log(TAG, "onStartJob() invoked");
    LogEvent.Log(TAG, "job.getTag(): " + job.getTag());
    LogEvent.Log(TAG, "job.getService(): " + job.getService());
    LogEvent.Log(TAG, "job.getExtras().toString(): " + job.getExtras().toString());
    LogEvent.Log(TAG,
      "job.getExtras().getString(\"some_key\"): " + job.getExtras().getString("some_key"));
    if(ActivityCompat.checkSelfPermission(getApplicationContext(),
      Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
      LogEvent.Log(TAG, "PERMISSION_GRANTED");
      createLocationCallback(job);
      mFusedLocationClient.getLastLocation().addOnSuccessListener(
        new OnSuccessListener<Location>() {
          @Override
          public void onSuccess(Location location) {
            LogEvent.Log(TAG, "getLastLocation onSuccess listener invoked");
            if(location != null) {
              LogEvent.Log(TAG, location.getLatitude() + "," + location.getLongitude());
              Singleton.INSTANCE.setLatitude(location.getLatitude());
              Singleton.INSTANCE.setLongitude(location.getLongitude());
              if(ParseUser.getCurrentUser() != null) {
                ParseGeoPoint currentUserLocation =
                  new ParseGeoPoint(location.getLatitude(), location.getLongitude());
                ParseUser.getCurrentUser().put("location", currentUserLocation);
                ParseUser.getCurrentUser().saveEventually();
              } else {
                //stopSelf();
                jobFinished(job, false);
              }
            } else {
              LogEvent.Log(TAG, "Failed to retrieve last location");
            }
          }
        });
      mFusedLocationClient.requestLocationUpdates(mLocationRequest, mLocationCallback,
        Looper.myLooper());
    } else {
      LogEvent.Log(TAG, "PERMISSION_DENIED");
    }
    return true; // Answers the question: "Is there still work going on?"
  }

  @Override
  public boolean onStopJob(JobParameters job) {
    LogEvent.Log(TAG, "onStartJob() invoked");
    LogEvent.Log(TAG, "job.getTag(): " + job.getTag());
    LogEvent.Log(TAG, "job.getService(): " + job.getService());
    LogEvent.Log(TAG, "job.getExtras().toString(): " + job.getExtras().toString());
    LogEvent.Log(TAG,
      "job.getExtras().getString(\"some_key\"): " + job.getExtras().getString("some_key"));
    return false; // Answers the question: "Should this job be retried?"
  }

  @Override
  public void onDestroy() {
    super.onDestroy();
    LogEvent.Log(TAG, "onDestroy() invoked");
    stopLocationUpdates();
  }
}