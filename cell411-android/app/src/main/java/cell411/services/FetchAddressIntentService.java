package cell411.services;

import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.text.TextUtils;

import androidx.core.app.JobIntentService;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import cell411.constants.Constants;
import cell411.utils.LogEvent;

/**
 * Created by Sachin on 15-01-2016.
 */
public class FetchAddressIntentService extends JobIntentService {
  private static final int JOB_ID = 1000;
  private final String TAG = "FetchAddressIntentService";
  protected ResultReceiver mReceiver;
    /*public FetchAddressIntentService() {
        super("Geocoder101");
    }*/

  /**
   * Creates an IntentService.  Invoked by your subclass's constructor.
   *
   * @param name Used to name the worker thread, important only for debugging.
   */
    /*public FetchAddressIntentService(String name) {
        super(name);
    }*/
  public static void enqueueWork(Context context, Intent work) {
    enqueueWork(context, FetchAddressIntentService.class, JOB_ID, work);
  }

  @Override
  protected void onHandleWork(Intent intent) {
    LogEvent.Log(TAG, "onHandleWork invoked");
    Geocoder geocoder = new Geocoder(this, Locale.getDefault());
    String errorMessage = "";
    // Get the location passed to this service through an extra.
    double lat = intent.getDoubleExtra(Constants.LOCATION_DATA_LAT, 0);
    double lng = intent.getDoubleExtra(Constants.LOCATION_DATA_LNG, 0);
    int id = intent.getIntExtra(Constants.LOCATION_DATA_ID, -1);
    String requestId = intent.getStringExtra(Constants.LOCATION_DATA_REQUEST_ID);
    mReceiver = intent.getParcelableExtra(Constants.RECEIVER);
    List<Address> addresses = null;
    try {
      addresses = geocoder.getFromLocation(lat, lng,
        // In this sample, get just a single address.
        1);
    } catch(IOException ioException) {
      // Catch network or other I/O problems.
      errorMessage = "Service not available";
      LogEvent.Log(TAG, errorMessage);
      LogEvent.Log(TAG, ioException.toString());
    } catch(IllegalArgumentException illegalArgumentException) {
      // Catch invalid latitude or longitude values.
      errorMessage = "Invalid lat lng used";
      LogEvent.Log(TAG, errorMessage + ". " + "Latitude = " + lat + ", Longitude = " + lng);
      LogEvent.Log(TAG, illegalArgumentException.toString());
    }
    // Handle case where no address was found.
    if(addresses == null || addresses.size() == 0) {
      if(errorMessage.isEmpty()) {
        errorMessage = "No address found";
        LogEvent.Log(TAG, errorMessage);
      }
      deliverResultToReceiver(Constants.FAILURE_RESULT, errorMessage, id, requestId);
    } else {
      Address address = addresses.get(0);
      String city = address.getLocality();
      String country = address.getCountryName();
      LogEvent.Log(TAG, "Locale: " + address.getLocale());
      LogEvent.Log(TAG, "Feature Name: " + address.getFeatureName());
      LogEvent.Log(TAG, "Sub Locality: " + address.getSubLocality());
      LogEvent.Log(TAG, "Sub Admin Area: " + address.getSubAdminArea());
      LogEvent.Log(TAG, "Locality: " + city);
      LogEvent.Log(TAG, "Admin Area: " + address.getAdminArea());
      LogEvent.Log(TAG, "Premises: " + address.getPremises());
      LogEvent.Log(TAG, "Postal Code: " + address.getPostalCode());
      LogEvent.Log(TAG, "Country Name: " + country);
      LogEvent.Log(TAG, "address.getMaxAddressLineIndex(): " + address.getMaxAddressLineIndex());
      // Fetch the address lines using getAddressLine,
      // join them, and send them to the thread.
      ArrayList<String> addressArrayList = new ArrayList<>();
      for(int i = 0; i <= address.getMaxAddressLineIndex(); i++) {
        addressArrayList.add(address.getAddressLine(i));
      }
      // System.getProperty("line.separator")
      String detailAddress = TextUtils.join(", ", addressArrayList);
      LogEvent.Log(TAG, "detailAddress: " + detailAddress);
      deliverResultToReceiver(Constants.SUCCESS_RESULT, address.getLocality(),
        address.getCountryName(), detailAddress, id, requestId);
    }
  }

  private void deliverResultToReceiver(int resultCode, String city, String country,
                                       String detailAddress, int id, String requestId) {
    LogEvent.Log(TAG, "Address Found");
    Bundle bundle = new Bundle();
    bundle.putString(Constants.RESULT_DATA_CITY, city);
    bundle.putString(Constants.RESULT_DATA_COUNTRY, country);
    bundle.putString(Constants.RESULT_DATA_DETAIL_ADDRESS, detailAddress);
    bundle.putInt(Constants.LOCATION_DATA_ID, id);
    bundle.putString(Constants.LOCATION_DATA_REQUEST_ID, requestId);
    mReceiver.send(resultCode, bundle);
  }

  private void deliverResultToReceiver(int resultCode, String message, int id, String requestId) {
    LogEvent.Log(TAG, "Address Not Found");
    Bundle bundle = new Bundle();
    bundle.putString(Constants.RESULT_DATA_ERROR, message);
    bundle.putInt(Constants.LOCATION_DATA_ID, id);
    bundle.putString(Constants.LOCATION_DATA_REQUEST_ID, requestId);
    mReceiver.send(resultCode, bundle);
  }
}