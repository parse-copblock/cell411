package cell411.utils;

import android.content.DialogInterface;

public class ExitListener implements DialogInterface.OnClickListener {
  @Override
  public void onClick(DialogInterface dialog, int which) {
    System.exit(0);
  }
}
