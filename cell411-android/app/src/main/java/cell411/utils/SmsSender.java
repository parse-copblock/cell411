package cell411.utils;

import android.os.AsyncTask;
import android.telephony.SmsManager;

public class SmsSender extends AsyncTask<Void, Void, Void> {
  final String mPhoneNumber;
  final String mMessage;

  public SmsSender(String phoneNumber, String message) {
    mPhoneNumber = phoneNumber;
    mMessage = message;
  }

  protected Void doInBackground(Void... voids) {
    SmsManager sms = SmsManager.getDefault();
    sms.sendTextMessage(mPhoneNumber, null, mMessage, null, null);
    return null;
  }
}
