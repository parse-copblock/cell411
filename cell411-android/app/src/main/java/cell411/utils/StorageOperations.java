package cell411.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;

import cell411.models.AlertFromHelper;
import cell411.models.AlertFromNeedy;
import cell411.models.AlertFromRejector;
import cell411.models.Chat;
import cell411.models.DownloadVideoTask;
import cell411.models.NewPublicCell;
import cell411.models.PrivateCell;
import cell411.models.PublicCell;

public class StorageOperations {
  private static String TAG = "java";
  private static File cacheDir;

  /**
   * Call this method to retrieve the saved CirclePlace object in order to display data in context of selected Circle.
   *
   * @param context context of the Activity
   * @return Circle list
   */
  public static HashMap<String, Object> getAudience(Context context) {
    // Make sure external shared storage is available
    if(Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
      // We can read and write the media
      cacheDir = context.getExternalFilesDir(null);
    } else {
      // Load another directory, probably local memory
      cacheDir = context.getFilesDir();
    }
    if(cacheDir == null) {
      return null;
    }
    HashMap<String, Object> audienceMap = null;
    File placeFile = new File(cacheDir.getAbsolutePath() + "/" + "SelectedAudience/data");
    LogEvent.Log(TAG, "Reading SelectedAudience Directory");
    try {
      ObjectInputStream inputStream = new ObjectInputStream(new FileInputStream(placeFile));
      audienceMap = (HashMap<String, Object>) inputStream.readObject();
      LogEvent.Log(TAG, "Object Read");
      inputStream.close();
    } catch(IOException e) {
      e.printStackTrace();
    } catch(ClassNotFoundException e) {
      e.printStackTrace();
    }
    return audienceMap;
  }

  /**
   * Call this method to store the NotifyUser objects.
   *
   * @param context     context of the Activity
   * @param audienceMap Selected audience
   */
  public static void storeAudience(Context context, HashMap<String, Object> audienceMap) {
    LogEvent.Log(TAG, "storeAudience invoked");
    // Make sure external shared storage is available
    if(Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
      // We can read and write the media
      cacheDir = context.getExternalFilesDir(null);
    } else {
      // Load another directory, probably local memory
      cacheDir = context.getFilesDir();
    }
    if(cacheDir == null) {
      return;
    }
    File circleFile = new File(cacheDir.getAbsolutePath() + "/" + "SelectedAudience");
    if(!circleFile.isDirectory()) {
      circleFile.mkdir();
      LogEvent.Log(TAG, "SelectedAudience Directory Created");
    }
    ObjectOutputStream out = null;
    File file = null;
    try {
      file = new File(circleFile.getAbsolutePath() + "/data");
      LogEvent.Log(TAG, "saved to " + file.toString());
      out = new ObjectOutputStream(new FileOutputStream(file));
      file = null;
      out.writeObject(audienceMap);
      LogEvent.Log(TAG, "Object Written");
      out.close();
    } catch(IOException e) {
      e.printStackTrace();
    }
  }

  public static void deleteAudience(Context context) {
    LogEvent.Log(TAG, "deleteAudience() invoked");
    // Make sure external shared storage is available
    if(Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
      // We can read and write the media
      cacheDir = context.getExternalFilesDir(null);
    } else {
      // Load another directory, probably local memory
      cacheDir = context.getFilesDir();
    }
    if(cacheDir == null) {
      return;
    }
    //cacheDir = context.getExternalFilesDir(null);
    File dir = new File(cacheDir.getAbsolutePath() + "/" + "SelectedAudience");
    if(dir.exists()) {
      File file = new File(dir.getAbsolutePath() + "/data");
      if(file.exists()) {
        file.delete();
      }
      dir.delete();
    }
  }

  /**
   * Call this method to retrieve the saved CirclePlace object in order to display data in context of selected Circle.
   *
   * @param context context of the Activity
   * @return Circle list
   */
  public static ArrayList<Chat> getUnseenChats(Context context) {
    // Make sure external shared storage is available
    if(Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
      // We can read and write the media
      cacheDir = context.getExternalFilesDir(null);
    } else {
      // Load another directory, probably local memory
      cacheDir = context.getFilesDir();
    }
    if(cacheDir == null) {
      return null;
    }
    //cacheDir = context.getExternalFilesDir(null);
    ArrayList<Chat> chatArrayList = null;
    File placeFile = new File(cacheDir.getAbsolutePath() + "/" + "Chats/data");
    LogEvent.Log(TAG, "Reading Chats Directory");
    try {
      ObjectInputStream inputStream = new ObjectInputStream(new FileInputStream(placeFile));
      chatArrayList = (ArrayList<Chat>) inputStream.readObject();
      LogEvent.Log(TAG, "Object Read");
      inputStream.close();
    } catch(IOException e) {
      e.printStackTrace();
    } catch(ClassNotFoundException e) {
      e.printStackTrace();
    }
    return chatArrayList;
  }

  /**
   * Call this method to store the NotifyUser objects.
   *
   * @param context       context of the Activity
   * @param chatArrayList Unseen Chat objects list
   */
  public static void storeUnseenChats(Context context, ArrayList<Chat> chatArrayList) {
    LogEvent.Log(TAG, "storeUnseenChats invoked");
    // Make sure external shared storage is available
    if(Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
      // We can read and write the media
      cacheDir = context.getExternalFilesDir(null);
    } else {
      // Load another directory, probably local memory
      cacheDir = context.getFilesDir();
    }
    if(cacheDir == null) {
      return;
    }
    //cacheDir = context.getExternalFilesDir(null);
    File circleFile = new File(cacheDir.getAbsolutePath() + "/" + "Chats");
    if(!circleFile.isDirectory()) {
      circleFile.mkdir();
      LogEvent.Log(TAG, "Chats Directory Created");
    }
    ObjectOutputStream out = null;
    File file = null;
    try {
      file = new File(circleFile.getAbsolutePath() + "/data");
      LogEvent.Log(TAG, "saved to " + file.toString());
      out = new ObjectOutputStream(new FileOutputStream(file));
      file = null;
      out.writeObject(chatArrayList);
      LogEvent.Log(TAG, "Object Written");
      out.close();
    } catch(IOException e) {
      e.printStackTrace();
    }
  }

  public static void deleteRecursive(File fileOrDirectory) {
    if(fileOrDirectory.isDirectory()) {
      for(File child : fileOrDirectory.listFiles()) {
        deleteRecursive(child);
      }
    }
    fileOrDirectory.delete();
  }

  public static void deletePhotos(Context context) {
    LogEvent.Log(TAG, "deleteUnseenChatsIfExists invoked");
    // Make sure external shared storage is available
    if(Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
      // We can read and write the media
      cacheDir = context.getExternalFilesDir(null);
    } else {
      // Load another directory, probably local memory
      cacheDir = context.getFilesDir();
    }
    if(cacheDir == null) {
      return;
    }
    //cacheDir = context.getExternalFilesDir(null);
    File dirPhoto = new File(cacheDir.getAbsolutePath() + "/" + "Photo");
    deleteRecursive(dirPhoto);
  }

  public static void deleteUnseenChatsIfExists(Context context) {
    LogEvent.Log(TAG, "deleteUnseenChatsIfExists invoked");
    // Make sure external shared storage is available
    if(Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
      // We can read and write the media
      cacheDir = context.getExternalFilesDir(null);
    } else {
      // Load another directory, probably local memory
      cacheDir = context.getFilesDir();
    }
    if(cacheDir == null) {
      return;
    }
    //cacheDir = context.getExternalFilesDir(null);
    File dir = new File(cacheDir.getAbsolutePath() + "/" + "Chats");
    if(dir.exists()) {
      File file = new File(dir.getAbsolutePath() + "/data");
      if(file.exists()) {
        file.delete();
      }
      dir.delete();
    }
  }

  public static void storePrivateCellsForPanicAlert(Context context,
                                                    ArrayList<PrivateCell> privateCells) {
    // Make sure external shared storage is available
    if(Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
      // We can read and write the media
      cacheDir = context.getExternalFilesDir(null);
    } else {
      // Load another directory, probably local memory
      cacheDir = context.getFilesDir();
    }
    if(cacheDir == null) {
      return;
    }
    //cacheDir = context.getExternalFilesDir(null);
    File alertFile = new File(cacheDir.getAbsolutePath() + "/" + "PrivateCellsForPanicAlert");
    if(!alertFile.isDirectory()) {
      alertFile.mkdir();
      LogEvent.Log(TAG, "PrivateCellsForPanicAlert Directory Created");
    }
    ObjectOutputStream out = null;
    File file = null;
    try {
      file = new File(alertFile.getAbsolutePath() + "/data");
      LogEvent.Log(TAG, "saved to " + file.toString());
      out = new ObjectOutputStream(new FileOutputStream(file));
      file = null;
      out.writeObject(privateCells);
      LogEvent.Log(TAG, "Object Written");
      out.close();
    } catch(IOException e) {
      e.printStackTrace();
    }
  }

  public static ArrayList<PrivateCell> getPrivateCellsForPanicAlert(Context context) {
    // Make sure external shared storage is available
    if(Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
      // We can read and write the media
      cacheDir = context.getExternalFilesDir(null);
    } else {
      // Load another directory, probably local memory
      cacheDir = context.getFilesDir();
    }
    if(cacheDir == null) {
      return null;
    }
    //cacheDir = context.getExternalFilesDir(null);
    ArrayList<PrivateCell> privateCells = null;
    File alertFile = new File(cacheDir.getAbsolutePath() + "/" + "PrivateCellsForPanicAlert/data");
    LogEvent.Log(TAG, "Reading PrivateCellsForPanicAlert Directory");
    try {
      ObjectInputStream inputStream = new ObjectInputStream(new FileInputStream(alertFile));
      privateCells = (ArrayList<PrivateCell>) inputStream.readObject();
      LogEvent.Log(TAG, "Object Read");
      inputStream.close();
    } catch(IOException e) {
      e.printStackTrace();
    } catch(ClassNotFoundException e) {
      e.printStackTrace();
    }
    return privateCells;
  }

  public static void deletePrivateCellsForPanicAlert(Context context) {
    LogEvent.Log(TAG, "deletePrivateCellsForPanicAlert() invoked");
    // Make sure external shared storage is available
    if(Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
      // We can read and write the media
      cacheDir = context.getExternalFilesDir(null);
    } else {
      // Load another directory, probably local memory
      cacheDir = context.getFilesDir();
    }
    if(cacheDir == null) {
      return;
    }
    //cacheDir = context.getExternalFilesDir(null);
    File dir = new File(cacheDir.getAbsolutePath() + "/" + "PrivateCellsForPanicAlert");
    if(dir.exists()) {
      File file = new File(dir.getAbsolutePath() + "/data");
      if(file.exists()) {
        file.delete();
      }
      dir.delete();
    }
  }

  public static void storePublicCellsForPanicAlert(Context context,
                                                   ArrayList<PublicCell> publicCells) {
    // Make sure external shared storage is available
    if(Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
      // We can read and write the media
      cacheDir = context.getExternalFilesDir(null);
    } else {
      // Load another directory, probably local memory
      cacheDir = context.getFilesDir();
    }
    if(cacheDir == null) {
      return;
    }
    //cacheDir = context.getExternalFilesDir(null);
    File alertFile = new File(cacheDir.getAbsolutePath() + "/" + "PublicCellsForPanicAlert");
    if(!alertFile.isDirectory()) {
      alertFile.mkdir();
      LogEvent.Log(TAG, "PublicCellsForPanicAlert Directory Created");
    }
    ObjectOutputStream out = null;
    File file = null;
    try {
      file = new File(alertFile.getAbsolutePath() + "/data");
      LogEvent.Log(TAG, "saved to " + file.toString());
      out = new ObjectOutputStream(new FileOutputStream(file));
      file = null;
      out.writeObject(publicCells);
      LogEvent.Log(TAG, "Object Written");
      out.close();
    } catch(IOException e) {
      e.printStackTrace();
    }
  }

  public static ArrayList<PublicCell> getPublicCellsForPanicAlert(Context context) {
    // Make sure external shared storage is available
    if(Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
      // We can read and write the media
      cacheDir = context.getExternalFilesDir(null);
    } else {
      // Load another directory, probably local memory
      cacheDir = context.getFilesDir();
    }
    if(cacheDir == null) {
      return null;
    }
    //cacheDir = context.getExternalFilesDir(null);
    ArrayList<PublicCell> publicCells = null;
    File alertFile = new File(cacheDir.getAbsolutePath() + "/" + "PublicCellsForPanicAlert/data");
    LogEvent.Log(TAG, "Reading PublicCellsForPanicAlert Directory");
    try {
      ObjectInputStream inputStream = new ObjectInputStream(new FileInputStream(alertFile));
      publicCells = (ArrayList<PublicCell>) inputStream.readObject();
      LogEvent.Log(TAG, "Object Read");
      inputStream.close();
    } catch(IOException e) {
      e.printStackTrace();
    } catch(ClassNotFoundException e) {
      e.printStackTrace();
    }
    return publicCells;
  }

  public static void deletePublicCellsForPanicAlert(Context context) {
    LogEvent.Log(TAG, "deletePublicCellsForPanicAlert() invoked");
    // Make sure external shared storage is available
    if(Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
      // We can read and write the media
      cacheDir = context.getExternalFilesDir(null);
    } else {
      // Load another directory, probably local memory
      cacheDir = context.getFilesDir();
    }
    if(cacheDir == null) {
      return;
    }
    //cacheDir = context.getExternalFilesDir(null);
    File dir = new File(cacheDir.getAbsolutePath() + "/" + "PublicCellsForPanicAlert");
    if(dir.exists()) {
      File file = new File(dir.getAbsolutePath() + "/data");
      if(file.exists()) {
        file.delete();
      }
      dir.delete();
    }
  }

  public static void storeAlertsFromNeedy(Context context,
                                          ArrayList<AlertFromNeedy> alertArrayList) {
    // Make sure external shared storage is available
    if(Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
      // We can read and write the media
      cacheDir = context.getExternalFilesDir(null);
    } else {
      // Load another directory, probably local memory
      cacheDir = context.getFilesDir();
    }
    if(cacheDir == null) {
      return;
    }
    //cacheDir = context.getExternalFilesDir(null);
    File alertFile = new File(cacheDir.getAbsolutePath() + "/" + "AlertFromNeedy");
    if(!alertFile.isDirectory()) {
      alertFile.mkdir();
      LogEvent.Log(TAG, "AlertFromNeedy Directory Created");
    }
    ObjectOutputStream out = null;
    File file = null;
    try {
      file = new File(alertFile.getAbsolutePath() + "/data");
      LogEvent.Log(TAG, "saved to " + file.toString());
      out = new ObjectOutputStream(new FileOutputStream(file));
      file = null;
      out.writeObject(alertArrayList);
      LogEvent.Log(TAG, "Object Written");
      out.close();
    } catch(IOException e) {
      e.printStackTrace();
    }
  }

  public static ArrayList<AlertFromNeedy> getAlertsFromNeedy(Context context) {
    // Make sure external shared storage is available
    if(Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
      // We can read and write the media
      cacheDir = context.getExternalFilesDir(null);
    } else {
      // Load another directory, probably local memory
      cacheDir = context.getFilesDir();
    }
    if(cacheDir == null) {
      return null;
    }
    //cacheDir = context.getExternalFilesDir(null);
    ArrayList<AlertFromNeedy> alertArrayList = null;
    File alertFile = new File(cacheDir.getAbsolutePath() + "/" + "AlertFromNeedy/data");
    LogEvent.Log(TAG, "Reading AlertFromNeedy Directory");
    try {
      ObjectInputStream inputStream = new ObjectInputStream(new FileInputStream(alertFile));
      alertArrayList = (ArrayList<AlertFromNeedy>) inputStream.readObject();
      LogEvent.Log(TAG, "Object Read");
      inputStream.close();
    } catch(IOException e) {
      e.printStackTrace();
    } catch(ClassNotFoundException e) {
      e.printStackTrace();
    }
    return alertArrayList;
  }

  public static void deleteAlertsFromNeedy(Context context) {
    LogEvent.Log(TAG, "deleteAlertsFromNeedy() invoked");
    // Make sure external shared storage is available
    if(Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
      // We can read and write the media
      cacheDir = context.getExternalFilesDir(null);
    } else {
      // Load another directory, probably local memory
      cacheDir = context.getFilesDir();
    }
    if(cacheDir == null) {
      return;
    }
    //cacheDir = context.getExternalFilesDir(null);
    File dir = new File(cacheDir.getAbsolutePath() + "/" + "AlertFromNeedy");
    if(dir.exists()) {
      File file = new File(dir.getAbsolutePath() + "/data");
      if(file.exists()) {
        file.delete();
      }
      dir.delete();
    }
  }

  public static void storeNewPublicCells(Context context, ArrayList<NewPublicCell> newPublicCells) {
    // Make sure external shared storage is available
    if(Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
      // We can read and write the media
      cacheDir = context.getExternalFilesDir(null);
    } else {
      // Load another directory, probably local memory
      cacheDir = context.getFilesDir();
    }
    if(cacheDir == null) {
      return;
    }
    //cacheDir = context.getExternalFilesDir(null);
    File alertFile = new File(cacheDir.getAbsolutePath() + "/" + "NewPublicCell");
    if(!alertFile.isDirectory()) {
      alertFile.mkdir();
      LogEvent.Log(TAG, "NewPublicCell Directory Created");
    }
    ObjectOutputStream out = null;
    File file = null;
    try {
      file = new File(alertFile.getAbsolutePath() + "/data");
      LogEvent.Log(TAG, "saved to " + file.toString());
      out = new ObjectOutputStream(new FileOutputStream(file));
      file = null;
      out.writeObject(newPublicCells);
      LogEvent.Log(TAG, "Object Written");
      out.close();
    } catch(IOException e) {
      e.printStackTrace();
    }
  }

  public static ArrayList<NewPublicCell> getNewPublicCells(Context context) {
    // Make sure external shared storage is available
    if(Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
      // We can read and write the media
      cacheDir = context.getExternalFilesDir(null);
    } else {
      // Load another directory, probably local memory
      cacheDir = context.getFilesDir();
    }
    if(cacheDir == null) {
      return null;
    }
    //cacheDir = context.getExternalFilesDir(null);
    ArrayList<NewPublicCell> newPublicCellArrayList = null;
    File alertFile = new File(cacheDir.getAbsolutePath() + "/" + "NewPublicCell/data");
    LogEvent.Log(TAG, "Reading NewPublicCell Directory");
    try {
      ObjectInputStream inputStream = new ObjectInputStream(new FileInputStream(alertFile));
      newPublicCellArrayList = (ArrayList<NewPublicCell>) inputStream.readObject();
      LogEvent.Log(TAG, "Object Read");
      inputStream.close();
    } catch(IOException e) {
      e.printStackTrace();
    } catch(ClassNotFoundException e) {
      e.printStackTrace();
    }
    return newPublicCellArrayList;
  }

  public static void deleteNewPublicCells(Context context) {
    LogEvent.Log(TAG, "deleteNewPublicCells() invoked");
    // Make sure external shared storage is available
    if(Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
      // We can read and write the media
      cacheDir = context.getExternalFilesDir(null);
    } else {
      // Load another directory, probably local memory
      cacheDir = context.getFilesDir();
    }
    if(cacheDir == null) {
      return;
    }
    //cacheDir = context.getExternalFilesDir(null);
    File dir = new File(cacheDir.getAbsolutePath() + "/" + "NewPublicCell");
    if(dir.exists()) {
      File file = new File(dir.getAbsolutePath() + "/data");
      if(file.exists()) {
        file.delete();
      }
      dir.delete();
    }
  }

  public static void storeAlertsFromRejector(Context context,
                                             ArrayList<AlertFromRejector> alertArrayList) {
    // Make sure external shared storage is available
    if(Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
      // We can read and write the media
      cacheDir = context.getExternalFilesDir(null);
    } else {
      // Load another directory, probably local memory
      cacheDir = context.getFilesDir();
    }
    if(cacheDir == null) {
      return;
    }
    //cacheDir = context.getExternalFilesDir(null);
    File alertFile = new File(cacheDir.getAbsolutePath() + "/" + "AlertFromRejector");
    if(!alertFile.isDirectory()) {
      alertFile.mkdir();
      LogEvent.Log(TAG, "AlertFromRejector Directory Created");
    }
    ObjectOutputStream out = null;
    File file = null;
    try {
      file = new File(alertFile.getAbsolutePath() + "/data");
      LogEvent.Log(TAG, "saved to " + file.toString());
      out = new ObjectOutputStream(new FileOutputStream(file));
      file = null;
      out.writeObject(alertArrayList);
      LogEvent.Log(TAG, "Object Written");
      out.close();
    } catch(IOException e) {
      e.printStackTrace();
    }
  }

  public static ArrayList<AlertFromRejector> getAlertsFromRejector(Context context) {
    // Make sure external shared storage is available
    if(Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
      // We can read and write the media
      cacheDir = context.getExternalFilesDir(null);
    } else {
      // Load another directory, probably local memory
      cacheDir = context.getFilesDir();
    }
    if(cacheDir == null) {
      return null;
    }
    //cacheDir = context.getExternalFilesDir(null);
    ArrayList<AlertFromRejector> alertArrayList = null;
    File alertFile = new File(cacheDir.getAbsolutePath() + "/" + "AlertFromRejector/data");
    LogEvent.Log(TAG, "Reading AlertFromRejector Directory");
    try {
      ObjectInputStream inputStream = new ObjectInputStream(new FileInputStream(alertFile));
      alertArrayList = (ArrayList<AlertFromRejector>) inputStream.readObject();
      LogEvent.Log(TAG, "Object Read");
      inputStream.close();
    } catch(IOException e) {
      e.printStackTrace();
    } catch(ClassNotFoundException e) {
      e.printStackTrace();
    }
    return alertArrayList;
  }

  public static void deleteAlertsFromRejector(Context context) {
    LogEvent.Log(TAG, "deleteAlertsFromRejector() invoked");
    // Make sure external shared storage is available
    if(Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
      // We can read and write the media
      cacheDir = context.getExternalFilesDir(null);
    } else {
      // Load another directory, probably local memory
      cacheDir = context.getFilesDir();
    }
    if(cacheDir == null) {
      return;
    }
    //cacheDir = context.getExternalFilesDir(null);
    File dir = new File(cacheDir.getAbsolutePath() + "/" + "AlertFromRejector");
    if(dir.exists()) {
      File file = new File(dir.getAbsolutePath() + "/data");
      if(file.exists()) {
        file.delete();
      }
      dir.delete();
    }
  }

  public static void storeOtherAlerts(Context context, ArrayList<String> otherAlerts) {
    // Make sure external shared storage is available
    if(Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
      // We can read and write the media
      cacheDir = context.getExternalFilesDir(null);
    } else {
      // Load another directory, probably local memory
      cacheDir = context.getFilesDir();
    }
    if(cacheDir == null) {
      return;
    }
    //cacheDir = context.getExternalFilesDir(null);
    File alertFile = new File(cacheDir.getAbsolutePath() + "/" + "OtherAlerts");
    if(!alertFile.isDirectory()) {
      alertFile.mkdir();
      LogEvent.Log(TAG, "OtherAlerts Directory Created");
    }
    ObjectOutputStream out = null;
    File file = null;
    try {
      file = new File(alertFile.getAbsolutePath() + "/data");
      LogEvent.Log(TAG, "saved to " + file.toString());
      out = new ObjectOutputStream(new FileOutputStream(file));
      file = null;
      out.writeObject(otherAlerts);
      LogEvent.Log(TAG, "Object Written");
      out.close();
    } catch(IOException e) {
      e.printStackTrace();
    }
  }

  public static ArrayList<String> getOtherAlerts(Context context) {
    // Make sure external shared storage is available
    if(Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
      // We can read and write the media
      cacheDir = context.getExternalFilesDir(null);
    } else {
      // Load another directory, probably local memory
      cacheDir = context.getFilesDir();
    }
    if(cacheDir == null) {
      return null;
    }
    //cacheDir = context.getExternalFilesDir(null);
    ArrayList<String> otherAlerts = null;
    File alertFile = new File(cacheDir.getAbsolutePath() + "/" + "OtherAlerts/data");
    LogEvent.Log(TAG, "Reading OtherAlerts Directory");
    try {
      ObjectInputStream inputStream = new ObjectInputStream(new FileInputStream(alertFile));
      otherAlerts = (ArrayList<String>) inputStream.readObject();
      LogEvent.Log(TAG, "Object Read");
      inputStream.close();
    } catch(IOException e) {
      e.printStackTrace();
    } catch(ClassNotFoundException e) {
      e.printStackTrace();
    }
    return otherAlerts;
  }

  public static void deleteOtherAlerts(Context context) {
    LogEvent.Log(TAG, "deleteOtherAlerts() invoked");
    // Make sure external shared storage is available
    if(Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
      // We can read and write the media
      cacheDir = context.getExternalFilesDir(null);
    } else {
      // Load another directory, probably local memory
      cacheDir = context.getFilesDir();
    }
    if(cacheDir == null) {
      return;
    }
    //cacheDir = context.getExternalFilesDir(null);
    File dir = new File(cacheDir.getAbsolutePath() + "/" + "OtherAlerts");
    if(dir.exists()) {
      File file = new File(dir.getAbsolutePath() + "/data");
      if(file.exists()) {
        file.delete();
      }
      dir.delete();
    }
  }

  public static void storeDownloadVideoTask(Context context,
                                            ArrayList<DownloadVideoTask> downloadVideoTasks) {
    // Make sure external shared storage is available
    if(Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
      // We can read and write the media
      cacheDir = context.getExternalFilesDir(null);
    } else {
      // Load another directory, probably local memory
      cacheDir = context.getFilesDir();
    }
    if(cacheDir == null) {
      return;
    }
    //cacheDir = context.getExternalFilesDir(null);
    File alertFile = new File(cacheDir.getAbsolutePath() + "/" + "DownloadVideoTask");
    if(!alertFile.isDirectory()) {
      alertFile.mkdir();
      LogEvent.Log(TAG, "DownloadVideoTask Directory Created");
    }
    ObjectOutputStream out = null;
    File file = null;
    try {
      file = new File(alertFile.getAbsolutePath() + "/data");
      LogEvent.Log(TAG, "saved to " + file.toString());
      out = new ObjectOutputStream(new FileOutputStream(file));
      file = null;
      out.writeObject(downloadVideoTasks);
      LogEvent.Log(TAG, "Object Written");
      out.close();
    } catch(IOException e) {
      e.printStackTrace();
    }
  }

  public static ArrayList<DownloadVideoTask> getDownloadVideoTask(Context context) {
    // Make sure external shared storage is available
    if(Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
      // We can read and write the media
      cacheDir = context.getExternalFilesDir(null);
    } else {
      // Load another directory, probably local memory
      cacheDir = context.getFilesDir();
    }
    if(cacheDir == null) {
      return null;
    }
    //cacheDir = context.getExternalFilesDir(null);
    ArrayList<DownloadVideoTask> downloadVideoTasks = null;
    File alertFile = new File(cacheDir.getAbsolutePath() + "/" + "DownloadVideoTask/data");
    LogEvent.Log(TAG, "Reading DownloadVideoTask Directory");
    try {
      ObjectInputStream inputStream = new ObjectInputStream(new FileInputStream(alertFile));
      downloadVideoTasks = (ArrayList<DownloadVideoTask>) inputStream.readObject();
      LogEvent.Log(TAG, "Object Read");
      inputStream.close();
    } catch(IOException e) {
      e.printStackTrace();
    } catch(ClassNotFoundException e) {
      e.printStackTrace();
    }
    return downloadVideoTasks;
  }

  public static void deleteDownloadVideoTask(Context context) {
    LogEvent.Log(TAG, "deleteDownloadVideoTask() invoked");
    // Make sure external shared storage is available
    if(Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
      // We can read and write the media
      cacheDir = context.getExternalFilesDir(null);
    } else {
      // Load another directory, probably local memory
      cacheDir = context.getFilesDir();
    }
    if(cacheDir == null) {
      return;
    }
    //cacheDir = context.getExternalFilesDir(null);
    File dir = new File(cacheDir.getAbsolutePath() + "/" + "DownloadVideoTask");
    if(dir.exists()) {
      File file = new File(dir.getAbsolutePath() + "/data");
      if(file.exists()) {
        file.delete();
      }
      dir.delete();
    }
  }

  public static Bitmap decodeSampledBitmapFromResource(String imageFile, int reqWidth,
                                                       int reqHeight) {
    LogEvent.Log(TAG, "Decoding Bitmap to " + reqWidth + " * " + reqHeight);
    // First decode with inJustDecodeBounds=true to check dimensions
    final BitmapFactory.Options options = new BitmapFactory.Options();
    options.inJustDecodeBounds = true;
    BitmapFactory.decodeFile(imageFile, options);
    // Calculate inSampleSize
    options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);
    // Decode bitmap with inSampleSize set
    options.inJustDecodeBounds = false;
    options.inPreferQualityOverSpeed = true;
    LogEvent.Log(TAG, "Decoding Bitmap to smaple size = " + options.inSampleSize);
    Bitmap bmp = BitmapFactory.decodeFile(imageFile, options);
    return bmp;
  }

  public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth,
                                          int reqHeight) {
    // Raw height and width of image
    final int height = options.outHeight;
    final int width = options.outWidth;
    int inSampleSize = 1;
    if(height > reqHeight || width > reqWidth) {
      final int halfHeight = height / 2;
      final int halfWidth = width / 2;
      // Calculate the largest inSampleSize value that is a power of 2 and
      // keeps both
      // height and width larger than the requested height and width.
      while((halfHeight / inSampleSize) > reqHeight && (halfWidth / inSampleSize) > reqWidth) {
        inSampleSize *= 2;
      }
    }
    return inSampleSize;
  }

  public static void saveImage(Context context, String filename, Bitmap bitmap) {
    // Make sure external shared storage is available
    if(Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
      // We can read and write the media
      cacheDir = context.getExternalFilesDir(null);
    } else {
      // Load another directory, probably local memory
      cacheDir = context.getFilesDir();
    }
    if(cacheDir == null) {
      return;
    }
    //cacheDir = context.getExternalFilesDir(null);
    File backgroundImagesDir = new File(cacheDir.getAbsolutePath() + "/" + "Photo");
    if(!backgroundImagesDir.isDirectory()) {
      backgroundImagesDir.mkdir();
      LogEvent.Log(TAG, "Photo Directory Created");
    }
    OutputStream out = null;
    File file = null;
    try {
      file = new File(backgroundImagesDir.getAbsolutePath() + "/" + filename);
      LogEvent.Log(TAG, "saved to " + file.toString());
      out = new FileOutputStream(file);
      bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
      out.flush();
      file = null;
      LogEvent.Log(TAG, "File Written");
      bitmap = null;
      out.close();
    } catch(IOException e) {
      e.printStackTrace();
    }
  }

  public static void deleteAlertsFromHelper(Context context) {
    LogEvent.Log(TAG, "deleteAlertsFromHelper() invoked");
    // Make sure external shared storage is available
    if(Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
      // We can read and write the media
      cacheDir = context.getExternalFilesDir(null);
    } else {
      // Load another directory, probably local memory
      cacheDir = context.getFilesDir();
    }
    if(cacheDir == null) {
      return;
    }
    //cacheDir = context.getExternalFilesDir(null);
    File dir = new File(cacheDir.getAbsolutePath() + "/" + "AlertFromHelper");
    if(dir.exists()) {
      File file = new File(dir.getAbsolutePath() + "/data");
      if(file.exists()) {
        file.delete();
      }
      dir.delete();
    }
  }

  public static void storeAlertsFromHelper(Context context,
                                           ArrayList<AlertFromHelper> alertArrayList) {
    // Make sure external shared storage is available
    if(Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
      // We can read and write the media
      cacheDir = context.getExternalFilesDir(null);
    } else {
      // Load another directory, probably local memory
      cacheDir = context.getFilesDir();
    }
    if(cacheDir == null) {
      return;
    }
    //cacheDir = context.getExternalFilesDir(null);
    File alertFile = new File(cacheDir.getAbsolutePath() + "/" + "AlertFromHelper");
    if(!alertFile.isDirectory()) {
      alertFile.mkdir();
      LogEvent.Log(TAG, "AlertFromHelper Directory Created");
    }
    ObjectOutputStream out = null;
    File file = null;
    try {
      file = new File(alertFile.getAbsolutePath() + "/data");
      LogEvent.Log(TAG, "saved to " + file.toString());
      out = new ObjectOutputStream(new FileOutputStream(file));
      file = null;
      out.writeObject(alertArrayList);
      LogEvent.Log(TAG, "Object Written");
      out.close();
    } catch(IOException e) {
      e.printStackTrace();
    }
  }

  public static ArrayList<AlertFromHelper> getAlertsFromHelper(Context context) {
    // Make sure external shared storage is available
    if(Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
      // We can read and write the media
      cacheDir = context.getExternalFilesDir(null);
    } else {
      // Load another directory, probably local memory
      cacheDir = context.getFilesDir();
    }
    if(cacheDir == null) {
      return null;
    }
    //cacheDir = context.getExternalFilesDir(null);
    ArrayList<AlertFromHelper> alertArrayList = null;
    File alertFile = new File(cacheDir.getAbsolutePath() + "/" + "AlertFromHelper/data");
    LogEvent.Log(TAG, "Reading AlertFromHelper Directory");
    try {
      ObjectInputStream inputStream = new ObjectInputStream(new FileInputStream(alertFile));
      alertArrayList = (ArrayList<AlertFromHelper>) inputStream.readObject();
      LogEvent.Log(TAG, "Object Read");
      inputStream.close();
    } catch(IOException e) {
      e.printStackTrace();
    } catch(ClassNotFoundException e) {
      e.printStackTrace();
    }
    return alertArrayList;
  }

  public static void clearData(Context context) {
    deleteAlertsFromHelper(context);
    deleteUnseenChatsIfExists(context);
    deletePrivateCellsForPanicAlert(context);
    deletePublicCellsForPanicAlert(context);
    deleteAlertsFromNeedy(context);
    deleteNewPublicCells(context);
    deleteAlertsFromRejector(context);
    deleteOtherAlerts(context);
    deleteDownloadVideoTask(context);
    deletePhotos(context);
    deleteAudience(context);
  }
}
