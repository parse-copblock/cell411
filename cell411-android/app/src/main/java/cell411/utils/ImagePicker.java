package cell411.utils;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;

import com.safearx.cell411.BuildConfig;
import com.safearx.cell411.R;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import cell411.Singleton;
import cell411.fragment_tabs.TabMapFragment;

/**
 * Created by Sachin on 11/1/2015.
 */
public class ImagePicker {
  public static final int REQUEST_IMAGE_CAPTURE = 25001;
  public static final int REQUEST_LOAD_IMAGE = 10001;
  public static final int PERMISSION_CAMERA = 10;
  private static final String TAG = "ImagePicker";
  private static final String JPEG_FILE_PREFIX = "IMG_";
  private static final String JPEG_FILE_SUFFIX = ".jpg";
  private static final float BYTES_PER_PX = 4.0f;
  private static String mCurrentPhotoPath;
  public boolean isProfileImage;
  public boolean isCarImage;
  public Uri uri;
  private Activity activity;
  private TabMapFragment fragment;
  private boolean startActivityFromFragment;
  private String path;

  public ImagePicker(Activity activity, TabMapFragment fragment,
                     boolean startActivityFromFragment) {
    this.activity = activity;
    this.fragment = fragment;
    this.startActivityFromFragment = startActivityFromFragment;
  }

  public static int getOrientation(final String filePath) {
    if(TextUtils.isEmpty(filePath)) {
      return 0;
    }
    try {
      final ExifInterface exifInterface = new ExifInterface(filePath);
      final int orientation = exifInterface.getAttributeInt(ExifInterface.TAG_ORIENTATION,
        ExifInterface.ORIENTATION_NORMAL);
      int rotate = 0;
      switch(orientation) {
        case ExifInterface.ORIENTATION_ROTATE_270:
          rotate = 270;
          break;
        case ExifInterface.ORIENTATION_ROTATE_180:
          rotate = 180;
          break;
        case ExifInterface.ORIENTATION_ROTATE_90:
          rotate = 90;
          break;
      }
      return rotate;
    } catch(final IOException ignored) {
    }
    return 0;
  }

  /**
   * Returns how much we have to rotate
   */
  public static float rotationForImage(Uri uri) {
    try {
      if(uri.getScheme().equals("content")) {
        //From the media gallery
        String[] projection = {MediaStore.Images.ImageColumns.ORIENTATION};
        Cursor c = Singleton.INSTANCE
                     .getContentResolver().query(uri, projection, null, null, null);
        if(c.moveToFirst()) {
          return c.getInt(0);
        }
      } else if(uri.getScheme().equals("file")) {
        //From a file saved by the camera
        ExifInterface exif = new ExifInterface(uri.getPath());
        int rotation = (int) exifOrientationToDegrees(
          exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL));
        exif.setAttribute(ExifInterface.TAG_ORIENTATION, "" + ExifInterface.ORIENTATION_NORMAL);
        exif.saveAttributes();
        return rotation;
      }
      return 0;
    } catch(IOException e) {
      Log.e(TAG, "Error checking exif", e);
      return 0;
    }
  }

  /**
   * Get rotation in degrees
   */
  private static float exifOrientationToDegrees(int exifOrientation) {
    if(exifOrientation == ExifInterface.ORIENTATION_ROTATE_90) {
      return 90;
    } else if(exifOrientation == ExifInterface.ORIENTATION_ROTATE_180) {
      return 180;
    } else if(exifOrientation == ExifInterface.ORIENTATION_ROTATE_270) {
      return 270;
    }
    return 0;
  }

  public static Bitmap decodeSampledBitmapFromResource(String imageFile, int reqWidth,
                                                       int reqHeight) {
    LogEvent.Log(TAG, "Decoding Bitmap to " + reqWidth + " * " + reqHeight);
    // First decode with inJustDecodeBounds=true to check dimensions
    final BitmapFactory.Options options = new BitmapFactory.Options();
    options.inJustDecodeBounds = true;
    BitmapFactory.decodeFile(imageFile.toString(), options);
    // Calculate inSampleSize
    options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);
    // Decode bitmap with inSampleSize set
    options.inJustDecodeBounds = false;
    options.inPreferQualityOverSpeed = true;
    LogEvent.Log(TAG, "Decoding Bitmap to smaple size = " + options.inSampleSize);
    Bitmap bmp = BitmapFactory.decodeFile(imageFile.toString(), options);
    return bmp;
  }

  public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth,
                                          int reqHeight) {
    // Raw height and width of image
    final int height = options.outHeight;
    final int width = options.outWidth;
    int inSampleSize = 1;
    if(height > reqHeight || width > reqWidth) {
      final int halfHeight = height / 2;
      final int halfWidth = width / 2;
      // Calculate the largest inSampleSize value that is a power of 2 and
      // keeps both
      // height and width larger than the requested height and width.
      while((halfHeight / inSampleSize) > reqHeight && (halfWidth / inSampleSize) > reqWidth) {
        inSampleSize *= 2;
      }
    }
    return inSampleSize;
  }

  public void displayAddImageChooserDialog() {
    AlertDialog.Builder dialog = new AlertDialog.Builder(activity);
    dialog.setMessage(R.string.dialog_message_upload_image);
    dialog.setNegativeButton(R.string.dialog_btn_camera, new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialog, int which) {
        if(startActivityFromFragment) {
          if(ContextCompat.checkSelfPermission(fragment.getActivity(),
            Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            if(ActivityCompat.shouldShowRequestPermissionRationale(fragment.getActivity(),
              Manifest.permission.CALL_PHONE)) {
              fragment.requestPermissions(new String[]{Manifest.permission.CAMERA},
                PERMISSION_CAMERA);
            } else {
              fragment.requestPermissions(new String[]{Manifest.permission.CAMERA},
                PERMISSION_CAMERA);
            }
          } else {
            dispatchTakePictureIntent();
          }
        } else {
          if(ContextCompat.checkSelfPermission(activity, Manifest.permission.CAMERA) !=
               PackageManager.PERMISSION_GRANTED) {
            if(ActivityCompat.shouldShowRequestPermissionRationale(activity,
              Manifest.permission.CALL_PHONE)) {
              ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.CAMERA},
                PERMISSION_CAMERA);
            } else {
              ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.CAMERA},
                PERMISSION_CAMERA);
            }
          } else {
            dispatchTakePictureIntent();
          }
        }
      }
    });
    dialog.setPositiveButton(R.string.dialog_btn_gallery, new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialog, int which) {
        Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        if(startActivityFromFragment) {
          fragment.startActivityForResult(i, REQUEST_LOAD_IMAGE);
        } else {
          activity.startActivityForResult(i, REQUEST_LOAD_IMAGE);
        }
      }
    });
    dialog.create().show();
  }

  private String getAlbumName() {
    if(startActivityFromFragment) {
      return fragment.getString(R.string.album_name);
    } else {
      return activity.getString(R.string.album_name);
    }
  }

  /**
   * This method was earlier kept in a separate Class called AlbumStorageDirFactory as an abstract method
   * This function was implemeted in two other classes called FroyoAlbumStoradeDirectory and BaseAlbumStorageDirectory
   *
   * @param albumName
   * @return
   */
  private File getAlbumStorageDir(String albumName) {
    return new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
      albumName);
  }

  private File getAlbumDir() {
    File storageDir = null;
    if(Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
      storageDir = getAlbumStorageDir(getAlbumName());
      if(storageDir != null) {
        if(!storageDir.mkdirs()) {
          if(!storageDir.exists()) {
            Log.d("CameraSample", "failed to create directory");
            return null;
          }
        }
      }
    } else {
      Log.v(activity.getString(R.string.app_name), "External storage is not mounted READ/WRITE.");
    }
    return storageDir;
  }

  private File createImageFile() throws IOException {
    // Create an image file name
    String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
    String imageFileName = JPEG_FILE_PREFIX + timeStamp + "_";
    //File storageDir = getAlbumDir();
    File storageDir =
      Singleton.INSTANCE.getCurrentActivity()
        .getExternalFilesDir(Environment.DIRECTORY_PICTURES);
    File imageF = File.createTempFile(imageFileName, JPEG_FILE_SUFFIX, storageDir);
    // Save a file: path for use with ACTION_VIEW intents
    mCurrentPhotoPath = imageF.getAbsolutePath();
    return imageF;
  }

  public void dispatchTakePictureIntent() {
    Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
    // Commented the below code to support Android Oreo
    // Now instead of taking image data from the Uri, accessing directly from the intent bundle
    // in onActivityResult() method in TabMapFragment
    try {
      File f = createImageFile();
      LogEvent.Log(TAG, "mCurrentPhotoPath: " + mCurrentPhotoPath);
      Uri photoURI = FileProvider.getUriForFile(Singleton.INSTANCE.getCurrentActivity(),
        BuildConfig.APPLICATION_ID + ".fileprovider", f);
      takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
    } catch(IOException e) {
      e.printStackTrace();
      LogEvent.Log(TAG, "IOException: " + e.getMessage());
    }
    if(startActivityFromFragment) {
      fragment.startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
    } else {
      activity.startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
    }
  }

  public void saveCameraImage(String imageName) {
    LogEvent.Log(TAG, "mCurrentPhotoPath: " + mCurrentPhotoPath);
    Bitmap bitmap =
      StorageOperations.decodeSampledBitmapFromResource(mCurrentPhotoPath,
        Singleton.getScreenWidth(),
        Singleton.getScreenHeight());
    float rotation = getOrientation(mCurrentPhotoPath);
    LogEvent.Log(TAG, "rotation: " + rotation);
    if(rotation != 0) {
      // New rotation matrix
      Matrix matrix2 = new Matrix();
      matrix2.preRotate(rotation);
      bitmap =
        Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix2, true);
    } else {
      // No need to rotate
      // loadImage();
      LogEvent.Log(TAG, "No need to rotate");
    }
    StorageOperations.saveImage(Singleton.INSTANCE, imageName, bitmap);
  }

  public void saveGalleryImage(Intent data) {
    Uri imageUri = data.getData();
    getImageUrlWithAuthority(imageUri);
  }

  public Uri getImageUrlWithAuthority(Uri uri) {
    InputStream is = null;
    if(uri.getAuthority() != null) {
      try {
        is = Singleton.INSTANCE.getContentResolver().openInputStream(uri);
        Bitmap bmp = BitmapFactory.decodeStream(is);
        float rotation;
        rotation = rotationForImage(uri);
        LogEvent.Log(TAG, "rotation: " + rotation);
        if(rotation != 0) {
          //New rotation matrix
          Matrix matrix = new Matrix();
          matrix.preRotate(rotation);
          bmp = Bitmap.createBitmap(bmp, 0, 0, bmp.getWidth(), bmp.getHeight(), matrix, true);
          if(bmp == null) {
            LogEvent.Log(TAG, "2: Bitmap is null");
          }
          //imgCamera.setImageBitmap(Singleton.getCroppedBitmap(bitmap));
        } else {
          //No need to rotate
          //imgCamera.setImageBitmap(Singleton.getCroppedBitmap(BitmapFactory.decodeFile(path, options)));
        }
        if(bmp == null) {
          LogEvent.Log(TAG, "bmp is null");
        }
        return writeToTempImageAndGetPathUri(bmp);
      } catch(FileNotFoundException e) {
        e.printStackTrace();
      } finally {
        try {
          is.close();
        } catch(IOException e) {
          e.printStackTrace();
        }
      }
    }
    return null;
  }

  public Uri writeToTempImageAndGetPathUri(Bitmap inImage) {
    if(inImage == null) {
      LogEvent.Log(TAG, "inImage is null");
    }
    StorageOperations.saveImage(Singleton.INSTANCE, "photo_alert.png", inImage);
    String path =
      Singleton.INSTANCE.getExternalFilesDir(null).getAbsolutePath() +
        "/Photo/photo_alert.png";
        /*ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);*/
    LogEvent.Log(TAG, "url: " + path);
    return Uri.parse(path);
  }

  public void saveGalleryImage(Intent data, String imageName) {
    Uri selectedImage = data.getData();
    getImageUrlWithAuthority(selectedImage, imageName);
  }

  public Uri getImageUrlWithAuthority(Uri uri, String imageName) {
    InputStream is = null;
    if(uri.getAuthority() != null) {
      try {
        is = Singleton.INSTANCE.getContentResolver().openInputStream(uri);
        Bitmap bmp = BitmapFactory.decodeStream(is);
        float rotation = rotationForImage(uri);
        LogEvent.Log(TAG, "rotation1: " + rotation);
        if(rotation != 0) {
          //New rotation matrix
          Matrix matrix = new Matrix();
          matrix.preRotate(rotation);
          bmp = Bitmap.createBitmap(bmp, 0, 0, bmp.getWidth(), bmp.getHeight(), matrix, true);
          if(bmp == null) {
            LogEvent.Log(TAG, "2: Bitmap is null");
          }
        }
        if(bmp == null) {
          LogEvent.Log(TAG, "bmp is null");
        }
        return writeToTempImageAndGetPathUri(bmp, imageName);
      } catch(FileNotFoundException e) {
        e.printStackTrace();
      } finally {
        try {
          is.close();
        } catch(IOException e) {
          e.printStackTrace();
        }
      }
    }
    return null;
  }

  public Uri writeToTempImageAndGetPathUri(Bitmap inImage, String imageName) {
    if(inImage == null) {
      LogEvent.Log(TAG, "inImage is null");
    }
    StorageOperations.saveImage(Singleton.INSTANCE, imageName, inImage);
    String path =
      Singleton.INSTANCE.getExternalFilesDir(null).getAbsolutePath() + "/Photo/" + imageName;
    LogEvent.Log(TAG, "url: " + path);
    return Uri.parse(path);
  }

  private void loadImage() {
    if(readBitmapInfo() > MemUtils.megabytesFree()) {
      subSampleImage(32);
    } else {
      subSampleImage((int) readBitmapInfo());
    }
  }

  private float readBitmapInfo() {
    final BitmapFactory.Options options = new BitmapFactory.Options();
    Bitmap bitmap = BitmapFactory.decodeFile(path, options);
    options.inJustDecodeBounds = true;
    BitmapFactory.decodeFile(path, options);
    final float imageHeight = options.outHeight;
    final float imageWidth = options.outWidth;
    final String imageMimeType = options.outMimeType;
    Log.d("ScaleBeforeLoad",
      "w, h, type: " + imageWidth + ", " + imageHeight + ", " + imageMimeType);
    Log.d("ScaleBeforeLoad", "estimated memory required in MB: " +
                               imageWidth * imageHeight * BYTES_PER_PX / MemUtils.BYTES_IN_MB);
    return imageWidth * imageHeight * BYTES_PER_PX / MemUtils.BYTES_IN_MB;
  }

  private void subSampleImage(int powerOf2) {
    if(powerOf2 < 1 || powerOf2 > 32) {
      Log.e("ScaleBeforeLoad", "trying to appply upscale or excessive downscale: " + powerOf2);
      if(powerOf2 > 40) {
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = false;
        options.inSampleSize = powerOf2 - 39;
        final Bitmap bmp = BitmapFactory.decodeFile(path, options);
        StorageOperations.saveImage(Singleton.INSTANCE, "photo_alert.png", bmp);
        Log.e(TAG, "ScaleAfterLoad  ,estimated memory required in MB: " + bmp.getWidth() + " " +
                     bmp.getHeight() + "size=" +
                     bmp.getWidth() * bmp.getHeight() * BYTES_PER_PX / MemUtils.BYTES_IN_MB);
      } else if(powerOf2 > 32) {
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = false;
        options.inSampleSize = powerOf2 - 32;
        final Bitmap bmp = BitmapFactory.decodeFile(path, options);
        StorageOperations.saveImage(Singleton.INSTANCE, "photo_alert.png", bmp);
        Log.e(TAG, "ScaleAfterLoad  ,estimated memory required in MB: " + bmp.getWidth() + " " +
                     bmp.getHeight() + "size=" +
                     bmp.getWidth() * bmp.getHeight() * BYTES_PER_PX / MemUtils.BYTES_IN_MB);
      } else {
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = false;
        options.inSampleSize = 1;
        final Bitmap bmp = BitmapFactory.decodeFile(path, options);
        StorageOperations.saveImage(Singleton.INSTANCE, "photo_alert.png", bmp);
        Log.e(TAG, "ScaleAfterLoad  ,estimated memory required in MB: " + bmp.getWidth() + " " +
                     bmp.getHeight() + "size=" +
                     bmp.getWidth() * bmp.getHeight() * BYTES_PER_PX / MemUtils.BYTES_IN_MB);
      }
      return;
    }
    final BitmapFactory.Options options = new BitmapFactory.Options();
    options.inJustDecodeBounds = false;
    options.inSampleSize = powerOf2;
    final Bitmap bmp = BitmapFactory.decodeFile(path, options);
    StorageOperations.saveImage(Singleton.INSTANCE, "photo_alert.png", bmp);
    Log.e(TAG, "ScaleAfterLoad  ,estimated memory required in MB: " + bmp.getWidth() + " " +
                 bmp.getHeight() + "size=" +
                 bmp.getWidth() * bmp.getHeight() * BYTES_PER_PX / MemUtils.BYTES_IN_MB);
    //image.setImageBitmap(bmp); }
  }
}