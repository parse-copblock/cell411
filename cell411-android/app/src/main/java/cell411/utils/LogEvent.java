package cell411.utils;

import android.util.Log;

/**
 * Created by Sachin Tibrewal on 1/14/2015.
 */
public class LogEvent {
  private static boolean DISPLAYLOGS = true;

  public static void Log(String TAG, String msg) {
    if(DISPLAYLOGS) {
      Log.i(TAG, msg);
    }
  }
}
