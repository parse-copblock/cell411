package cell411.fragment_tabs;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.AlertDialog;
import android.app.Service;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.OvershootInterpolator;
import android.widget.EditText;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.fragment.app.FragmentTransaction;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.viewpager.widget.PagerTabStrip;
import androidx.viewpager.widget.ViewPager;

import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.callback.FindCallback;
import com.parse.callback.SaveCallback;
import com.safearx.cell411.R;

import java.util.ArrayList;
import java.util.List;

import cell411.Singleton;
import cell411.activity.CreateOrEditPublicCellActivity;
import cell411.activity.ExploreCellActivity;
import cell411.activity.MainActivity;
import cell411.fragments.NewPrivateCellsFragment;
import cell411.fragments.PublicCellsFragment;
import cell411.models.Cell;
import cell411.models.NewPrivateCell;
import cell411.utils.LogEvent;
import cell411.widgets.Spinner;

/**
 * Created by Sachin on 18-04-2016.
 */
public class TabCellsFragment extends Fragment {
  private final String TAG = "TabCellsFragment";
  private ViewPager viewPager;
  private TabCellsAdapter tabCellsAdapter;
  private PagerTabStrip pagerTabStrip;
  private Spinner spinner;
  private FloatingActionMenu menuAddCell;
  private boolean isNonAppUserAlertEnabled;

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
                           Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.fragment_tab_cells, container, false);
    isNonAppUserAlertEnabled = false;
    viewPager = (ViewPager) view.findViewById(R.id.pager);
    pagerTabStrip = (PagerTabStrip) view.findViewById(R.id.pager_title_strip);
    tabCellsAdapter = new TabCellsAdapter(getChildFragmentManager(), 2);
    viewPager.setAdapter(tabCellsAdapter);
    pagerTabStrip.setTabIndicatorColorResource(R.color.transparent);
    spinner = (Spinner) view.findViewById(R.id.spinner);
    menuAddCell = (FloatingActionMenu) view.findViewById(R.id.menu_add_cell);
    menuAddCell.setClosedOnTouchOutside(true);
    FloatingActionButton fabExploreCells =
      (FloatingActionButton) view.findViewById(R.id.action_explore_cells);
    FloatingActionButton fabCreatePublicCell =
      (FloatingActionButton) view.findViewById(R.id.action_create_public_cell);
    FloatingActionButton fabCreatePrivateCell =
      (FloatingActionButton) view.findViewById(R.id.action_create_private_cell);
    boolean isCreatePublicCellEnabled =
      getResources().getBoolean(R.bool.is_create_public_cell_enabled);
    if(isCreatePublicCellEnabled) {
      fabCreatePublicCell.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
          if(menuAddCell.isOpened()) {
            menuAddCell.close(true);
          }
          Intent intent = new Intent(getActivity(), CreateOrEditPublicCellActivity.class);
          startActivity(intent);
        }
      });
    } else {
      fabCreatePublicCell.setVisibility(View.GONE);
    }
    fabExploreCells.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        if(menuAddCell.isOpened()) {
          menuAddCell.close(true);
        }
        Intent intent = new Intent(getActivity(), ExploreCellActivity.class);
        startActivity(intent);
      }
    });
    fabCreatePrivateCell.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        if(menuAddCell.isOpened()) {
          menuAddCell.close(true);
        }
        showCreateNewCellDialog();
      }
    });
    createCustomAnimation();
    return view;
  }

  private void createCustomAnimation() {
    AnimatorSet set = new AnimatorSet();
    ObjectAnimator scaleOutX =
      ObjectAnimator.ofFloat(menuAddCell.getMenuIconView(), "scaleX", 1.0f, 0.2f);
    ObjectAnimator scaleOutY =
      ObjectAnimator.ofFloat(menuAddCell.getMenuIconView(), "scaleY", 1.0f, 0.2f);
    ObjectAnimator scaleInX =
      ObjectAnimator.ofFloat(menuAddCell.getMenuIconView(), "scaleX", 0.2f, 1.0f);
    ObjectAnimator scaleInY =
      ObjectAnimator.ofFloat(menuAddCell.getMenuIconView(), "scaleY", 0.2f, 1.0f);
    scaleOutX.setDuration(50);
    scaleOutY.setDuration(50);
    scaleInX.setDuration(150);
    scaleInY.setDuration(150);
    scaleInX.addListener(new AnimatorListenerAdapter() {
      @Override
      public void onAnimationStart(Animator animation) {
        menuAddCell.getMenuIconView().setImageResource(
          menuAddCell.isOpened() ? R.drawable.fab_add_cell : R.drawable.fab_menu_close);
      }
    });
    set.play(scaleOutX).with(scaleOutY);
    set.play(scaleInX).with(scaleInY).after(scaleOutX);
    set.setInterpolator(new OvershootInterpolator(2));
    menuAddCell.setIconToggleAnimatorSet(set);
  }

  private void showCreateNewCellDialog() {
    AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
    alert.setMessage(R.string.dialog_message_create_new_cell);
    LayoutInflater inflater =
      (LayoutInflater) getActivity().getSystemService(Service.LAYOUT_INFLATER_SERVICE);
    View view = inflater.inflate(R.layout.layout_create_cell, null);
    final EditText etCellName = (EditText) view.findViewById(R.id.et_cell_name);
    alert.setView(view);
    alert.setNegativeButton(R.string.dialog_btn_cancel, new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int arg1) {
        dialog.dismiss();
      }
    });
    alert.setPositiveButton(R.string.dialog_btn_ok, new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int which) {
        final String cellName = etCellName.getText().toString().trim();
        if(cellName.isEmpty()) {
          MainActivity.INSTANCE.getApplicationContext();
          Singleton.sendToast(R.string.please_enter_cell_name);
        } else {
          if(Singleton.INSTANCE.getCells() != null) {
            ArrayList<Cell> cellList = (ArrayList<Cell>) Singleton.INSTANCE.getCells().clone();
            for(int i = 0; i < cellList.size(); i++) {
              if(cellList.get(i).cell != null) {
                if(cellList.get(i).name.equals(cellName)) {
                  MainActivity.INSTANCE.getApplicationContext();
                  Singleton.sendToast(cellName + " " + getString(R.string.already_created));
                  return;
                }
              }
            }
            createNewCell(cellName);
          } else {
            spinner.setVisibility(View.VISIBLE);
            ParseQuery<ParseObject> cellQuery = ParseQuery.getQuery("Cell");
            cellQuery.whereEqualTo("createdBy", ParseUser.getCurrentUser());
            cellQuery.whereNotEqualTo("type", 5);
            cellQuery.include("members");
            cellQuery.findInBackground(new FindCallback<ParseObject>() {
              @Override
              public void done(List<ParseObject> parseObjects, ParseException e) {
                if(e == null) {
                  ArrayList<Cell> cellList = new ArrayList<>();
                  if(parseObjects != null) {
                    for(int i = 0; i < parseObjects.size(); i++) {
                      ParseObject cell = parseObjects.get(i);
                      Cell cl = new Cell((String) cell.get("name"), cell);
                      if(cell.getList("members") != null) {
                        LogEvent.Log("Cell",
                          "There are some members in" + " " + cell.get("name") + " " +
                            "newPrivateCell");
                        List<ParseUser> members = cell.getList("members");
                        cl.members.addAll(members);
                      } else {
                        LogEvent.Log("Cell",
                          "There are no members in " + cell.get("name") + " newPrivateCell");
                      }
                      cellList.add(cl);
                    }
                  }
                  Singleton.INSTANCE.setCells((ArrayList<Cell>) cellList.clone());
                  for(int i = 0; i < cellList.size(); i++) {
                    if(cellList.get(i).cell != null) {
                      if(cellList.get(i).name.equals(cellName)) {
                        spinner.setVisibility(View.GONE);
                        MainActivity.INSTANCE.getApplicationContext();
                        Singleton.sendToast(cellName + " " + getString(R.string.already_created));
                        return;
                      }
                    }
                  }
                  createNewCell(cellName);
                } else {
                  spinner.setVisibility(View.GONE);
                  if(MainActivity.weakRef.get() != null &&
                       !MainActivity.weakRef.get().isFinishing()) {
                    MainActivity.INSTANCE.getApplicationContext();
                    Singleton.sendToast(e);
                  }
                }
              }
            });
          }
        }
      }
    });
    AlertDialog dialog = alert.create();
    dialog.show();
  }

  private void createNewCell(final String cellName) {
    spinner.setVisibility(View.VISIBLE);
    final ParseObject cellObject = new ParseObject("Cell");
    cellObject.put("createdBy", ParseUser.getCurrentUser());
    cellObject.put("name", cellName);
    cellObject.saveInBackground(new SaveCallback() {
      @Override
      public void done(ParseException e) {
        spinner.setVisibility(View.GONE);
        if(e == null) {
          NewPrivateCell newPrivateCell =
            new NewPrivateCell(cellName, cellObject, cellObject.getInt("type"),
              cellObject.getInt("totalMembers"));
          Singleton.INSTANCE.addNewPrivateCellToList(newPrivateCell);
          // Notify PrivateCellFragment for the newly created newPrivateCell
          Intent localIntent4NewPrivateCell =
            new Intent(NewPrivateCellsFragment.BROADCAST_ACTION_NEW_PRIVATE_CELL)
              // Puts the status into the Intent
              .putExtra("newPrivateCell", newPrivateCell);
          // Broadcasts the Intent to receivers in this app.
          LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(
            localIntent4NewPrivateCell);
          if(MainActivity.weakRef.get() != null && !MainActivity.weakRef.get().isFinishing()) {
            MainActivity.INSTANCE.getApplicationContext();
            Singleton.sendToast(R.string.cell_added_successfully);
          }
        } else {
          if(MainActivity.weakRef.get() != null && !MainActivity.weakRef.get().isFinishing()) {
            MainActivity.INSTANCE.getApplicationContext();
            Singleton.sendToast(e);
          }
        }
      }
    });
  }

  private class TabCellsAdapter extends FragmentPagerAdapter {
    private int size;

    public TabCellsAdapter(FragmentManager fm, int size) {
      super(fm);
      this.size = size;
    }

    @Override
    public Fragment getItem(int page) {
      if(page == 0) {
        return new PublicCellsFragment();
      } else {
        return new NewPrivateCellsFragment();
      }
    }

    @Override
    public int getCount() {
      return size;
    }

    @Override
    public CharSequence getPageTitle(int position) {
      switch(position) {
        case -1:
          return getString(R.string.strip_explore_cells);
        case 0:
          return getString(R.string.strip_my_public_cells);
        default:
          return getString(R.string.strip_my_private_cells);
      }
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
      super.destroyItem(container, position, object);
      if(position <= getCount()) {
        FragmentManager manager = ((Fragment) object).getFragmentManager();
        FragmentTransaction trans = manager.beginTransaction();
        trans.remove((Fragment) object);
        trans.commit();
      }
    }
  }
}