package cell411.fragment_tabs;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.parse.ParseUser;
import com.safearx.cell411.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import cell411.Singleton;
import cell411.activity.ChatActivity;
import cell411.activity.NewChatActivity;
import cell411.db.DataSource;
import cell411.models.ChatRoom;
import cell411.utils.LogEvent;
import cell411.widgets.CircularImageView;

/**
 * Created by Sachin on 06-02-2017.
 */
public class TabChatsFragment extends Fragment {
  private final String TAG = "TabChatsFragment";
  private RelativeLayout rlChatRoomEmpty;
  private RecyclerView mRecyclerView;
  private ChatRoomListAdapter chatRoomListAdapter;

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
                           Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.fragment_tab_chats, container, false);
    rlChatRoomEmpty = (RelativeLayout) view.findViewById(R.id.rl_chat_room);
    mRecyclerView = (RecyclerView) view.findViewById(R.id.rv_chat_room);
    mRecyclerView.setHasFixedSize(true);
    mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
    DataSource dataSource = new DataSource(getActivity());
    dataSource.open();
    ArrayList<ChatRoom> chatRoomArrayList = dataSource.getChatRooms();
    dataSource.close();
    if(chatRoomArrayList == null) {
      chatRoomArrayList = new ArrayList<>();
    }
    chatRoomListAdapter = new ChatRoomListAdapter(chatRoomArrayList);
    mRecyclerView.setAdapter(chatRoomListAdapter);
    FloatingActionButton fabNewChat = (FloatingActionButton) view.findViewById(R.id.fab_new_chat);
    fabNewChat.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        Intent intentNewChat = new Intent(getActivity(), NewChatActivity.class);
        startActivity(intentNewChat);
      }
    });
    return view;
  }

  @Override
  public void onResume() {
    super.onResume();
    Singleton.INSTANCE.setCurrentFragment(this);
    refreshChatRoomsList();
  }

  @Override
  public void onStop() {
    super.onStop();
    Singleton.INSTANCE.setCurrentFragment(null);
  }

  public void refreshChatRoomsList() {
    DataSource dataSource = new DataSource(getActivity());
    dataSource.open();
    ArrayList<ChatRoom> chatRoomArrayList = dataSource.getChatRooms();
    dataSource.close();
    if(chatRoomArrayList == null) {
      chatRoomArrayList = new ArrayList<>();
    }
    Collections.sort(chatRoomArrayList, new Comparator<ChatRoom>() {
      @Override
      public int compare(ChatRoom lhs, ChatRoom rhs) {
        return rhs.msgTime.compareTo(lhs.msgTime);
      }
    });
    chatRoomListAdapter.arrayList.clear();
    chatRoomListAdapter.arrayList.addAll(chatRoomArrayList);
    chatRoomListAdapter.notifyDataSetChanged();
    if(chatRoomArrayList.size() > 0) {
      rlChatRoomEmpty.setVisibility(View.GONE);
    }
  }

  public class ChatRoomListAdapter
    extends RecyclerView.Adapter<ChatRoomListAdapter.RequestViewHolder> {
    private final int VIEW_TYPE_PUBLIC_CELL = 0;
    private final int VIEW_TYPE_ALERT = 1;
    public ArrayList<ChatRoom> arrayList;
    private final View.OnClickListener mOnClickListener = new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        int position = mRecyclerView.getChildAdapterPosition(v);
        LogEvent.Log(TAG, "room tapped: " + arrayList.get(position).entityCreatedAt);
        Intent intentChat = new Intent(getActivity(), ChatActivity.class);
        intentChat.putExtra("entityType", arrayList.get(position).entityType.toString());
        intentChat.putExtra("entityObjectId", arrayList.get(position).entityObjectId);
        intentChat.putExtra("entityName", arrayList.get(position).entityName);
        intentChat.putExtra("time", String.valueOf(arrayList.get(position).entityCreatedAt));
        startActivity(intentChat);
      }
    };
    private final View.OnLongClickListener mOnLongClickListener = new View.OnLongClickListener() {
      @Override
      public boolean onLongClick(View v) {
        int position = mRecyclerView.getChildAdapterPosition(v);
        // Delete this friend
        showDeleteChatRoomDialog(arrayList.get(position), position);
        return false;
      }
    };
    private int colorAccent;
    private int colorGray;

    // Provide a suitable constructor (depends on the kind of data set)
    public ChatRoomListAdapter(ArrayList<ChatRoom> arrayList) {
      this.arrayList = arrayList;
      colorAccent = getResources().getColor(R.color.colorAccent);
      colorGray = getResources().getColor(R.color.gray_999);
    }

    // Create new views (invoked by the layout manager)
    @Override
    public RequestViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
      // create a new view
      View v = null;
      v = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_chat_room, parent, false);
      // set the view's size, margins, padding's and layout parameters
      RequestViewHolder vh = new RequestViewHolder(v, viewType);
      v.setOnClickListener(mOnClickListener);
      v.setOnLongClickListener(mOnLongClickListener);
      return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(RequestViewHolder requestViewHolder, int position) {
      // - get element from your data set at this position
      // - replace the contents of the view with that element
      final ChatRoom chatRoom = arrayList.get(position);
      LogEvent.Log(TAG, "msgType: " + chatRoom.msgType);
      if(chatRoom.msgType.equals("loc")) {
        if(chatRoom.senderId.equals(ParseUser.getCurrentUser().getObjectId())) {
          requestViewHolder.txtLastMessage.setText(R.string.chat_msg_you_location);
        } else {
          requestViewHolder.txtLastMessage.setText(
            getString(R.string.chat_msg_sender_location, chatRoom.senderFirstName));
        }
      } else if(chatRoom.msgType.equals("img")) {
        if(chatRoom.senderId.equals(ParseUser.getCurrentUser().getObjectId())) {
          requestViewHolder.txtLastMessage.setText(R.string.chat_msg_you_image);
        } else {
          requestViewHolder.txtLastMessage.setText(
            getString(R.string.chat_msg_sender_image, chatRoom.senderFirstName));
        }
      } else {
        if(chatRoom.senderId.equals(ParseUser.getCurrentUser().getObjectId())) {
          requestViewHolder.txtLastMessage.setText(getString(R.string.chat_msg_you, chatRoom.msg));
        } else {
          requestViewHolder.txtLastMessage.setText(chatRoom.senderFirstName + ": " + chatRoom.msg);
        }
      }
      requestViewHolder.txtTime.setText(Singleton.getFuzzyTimeOrDay(chatRoom.msgTime));
      if(chatRoom.msgCount > 0) {
        requestViewHolder.txtBadgeCount.setVisibility(View.VISIBLE);
        requestViewHolder.txtBadgeCount.setText("" + chatRoom.msgCount);
        requestViewHolder.txtTime.setTextColor(colorAccent);
      } else {
        requestViewHolder.txtBadgeCount.setVisibility(View.GONE);
        requestViewHolder.txtTime.setTextColor(colorGray);
      }
      if(getItemViewType(position) == VIEW_TYPE_PUBLIC_CELL) {
        requestViewHolder.txtChatRoomName.setText(chatRoom.entityName);
        requestViewHolder.img.setImageResource(R.drawable.ic_placeholder_cell);
      } else {
        requestViewHolder.txtChatRoomName.setText(
          getString(R.string.chat_title_alert, chatRoom.entityName));
        requestViewHolder.img.setImageResource(R.drawable.logo);
      }
      LogEvent.Log("ChatFragment", "time: " + chatRoom.msgTime);
      LogEvent.Log("ChatFragment", "count: " + chatRoom.msgCount);
    }

    // Return the size of your data set (invoked by the layout manager)
    @Override
    public int getItemCount() {
      return arrayList.size();
    }

    @Override
    public int getItemViewType(int position) {
      super.getItemViewType(position);
      if(arrayList.get(position).entityType == ChatRoom.EntityType.PUBLIC_CELL) {
        return VIEW_TYPE_PUBLIC_CELL;
      } else {
        return VIEW_TYPE_ALERT;
      }
    }

    private void showDeleteChatRoomDialog(final ChatRoom chatRoom, final int position) {
      AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
      alert.setMessage(R.string.dialog_msg_delete_chat);
      alert.setNegativeButton(R.string.dialog_btn_cancel, new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialogInterface, int i) {
        }
      });
      alert.setPositiveButton(R.string.dialog_btn_yes, new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialogInterface, int i) {
          DataSource ds = new DataSource(getActivity());
          ds.open();
          ds.deleteChatRoomId(chatRoom.entityObjectId);
          ds.close();
          chatRoomListAdapter.arrayList.remove(position);
          chatRoomListAdapter.notifyItemRemoved(position);
          refreshViews();
        }
      });
      AlertDialog dialog = alert.create();
      dialog.show();
    }

    private void refreshViews() {
      if(arrayList.size() > 0) {
        rlChatRoomEmpty.setVisibility(View.GONE);
      } else {
        rlChatRoomEmpty.setVisibility(View.VISIBLE);
      }
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public class RequestViewHolder extends RecyclerView.ViewHolder {
      // each data item is just a string in this case
      private CircularImageView img;
      private TextView txtChatRoomName;
      private TextView txtTime;
      private TextView txtLastMessage;
      private TextView txtBadgeCount;

      public RequestViewHolder(View view, int type) {
        super(view);
        img = (CircularImageView) view.findViewById(R.id.img);
        txtChatRoomName = (TextView) view.findViewById(R.id.txt_chat_room_name);
        txtTime = (TextView) view.findViewById(R.id.txt_time);
        txtLastMessage = (TextView) view.findViewById(R.id.txt_last_message);
        txtBadgeCount = (TextView) view.findViewById(R.id.txt_badge_count);
      }
    }
  }
}