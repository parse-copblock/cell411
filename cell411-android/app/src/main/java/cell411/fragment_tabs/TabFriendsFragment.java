package cell411.fragment_tabs;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.AlertDialog;
import android.app.Service;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.OvershootInterpolator;
import android.widget.EditText;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.fragment.app.FragmentTransaction;
import androidx.viewpager.widget.PagerTabStrip;
import androidx.viewpager.widget.ViewPager;

import com.github.clans.fab.FloatingActionMenu;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.safearx.cell411.R;

import cell411.activity.CustomScannerActivity;
import cell411.fragments.FriendRequestFragment;
import cell411.fragments.FriendsFragment;
import cell411.fragments.SearchFriendFragment;
import cell411.methods.AddFriendModules;
import cell411.utils.LogEvent;

/**
 * Created by Sachin on 18-04-2016.
 */
public class TabFriendsFragment extends Fragment {
  private final String TAG = "TabFriendsFragment";
  private ViewPager viewPager;
  private TabFriendsAdapter tabFriendsAdapter;
  private PagerTabStrip pagerTabStrip;
  private FloatingActionButton fab;
  private FloatingActionMenu menuAddFriend;

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
                           Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.fragment_tab_friends, container, false);
    viewPager = (ViewPager) view.findViewById(R.id.pager);
    pagerTabStrip = (PagerTabStrip) view.findViewById(R.id.pager_title_strip);
    tabFriendsAdapter = new TabFriendsAdapter(getChildFragmentManager(), 3);
    viewPager.setAdapter(tabFriendsAdapter);
    pagerTabStrip.setTabIndicatorColorResource(R.color.transparent);
        /*fab = (FloatingActionButton) view.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showAddFriendDialog();
            }
        });
        fab.hide();*/
        /*final FloatingActionsMenu floatingActionsMenu = (FloatingActionsMenu) view.findViewById(R.id.multiple_actions);
        final View viewOverlay = view.findViewById(R.id.view_overlay);
        viewOverlay.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (floatingActionsMenu.isExpanded())
                    floatingActionsMenu.collapse();
                return true;
            }
        });
        final com.getbase.floatingactionbutton.FloatingActionButton actionImportContact =
                (com.getbase.floatingactionbutton.FloatingActionButton) view.findViewById(R.id.action_import_contact);
        actionImportContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), UploadContactsActivity.class);
                startActivity(intent);
                if (floatingActionsMenu.isExpanded())
                    floatingActionsMenu.collapse();
            }
        });
        final com.getbase.floatingactionbutton.FloatingActionButton actionScanQRCode =
                (com.getbase.floatingactionbutton.FloatingActionButton) view.findViewById(R.id.action_scar_qr_code);
        actionScanQRCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    //IntentIntegrator.forSupportFragment(this).setCaptureActivity(CustomScannerActivity.class).initiateScan();
                    IntentIntegrator integrator = new IntentIntegrator(getActivity());
                    integrator.setCaptureActivity(CustomScannerActivity.class);
                    integrator.initiateScan();
                } catch (Exception e) {
                    Uri marketUri = Uri.parse("market://details?id=com.google.zxing.client.android");
                    Intent marketIntent = new Intent(Intent.ACTION_VIEW, marketUri);
                    startActivity(marketIntent);
                }
                if (floatingActionsMenu.isExpanded())
                    floatingActionsMenu.collapse();
            }
        });
        floatingActionsMenu.setOnFloatingActionsMenuUpdateListener(new FloatingActionsMenu.OnFloatingActionsMenuUpdateListener() {
            @Override
            public void onMenuExpanded() {
                final Animation anim = AnimationUtils.loadAnimation(getActivity(), R.anim.show);
                viewOverlay.startAnimation(anim);
                anim.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {
                        viewOverlay.setVisibility(View.VISIBLE);
                    }
                    @Override
                    public void onAnimationEnd(Animation animation) {
                    }
                    @Override
                    public void onAnimationRepeat(Animation animation) {
                    }
                });
            }
            @Override
            public void onMenuCollapsed() {
                final Animation anim = AnimationUtils.loadAnimation(getActivity(), R.anim.hide);
                viewOverlay.startAnimation(anim);
                anim.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {
                    }
                    @Override
                    public void onAnimationEnd(Animation animation) {
                        viewOverlay.setVisibility(View.GONE);
                    }
                    @Override
                    public void onAnimationRepeat(Animation animation) {
                    }
                });
            }
        });*/
    menuAddFriend = (FloatingActionMenu) view.findViewById(R.id.menu_add_friend);
    menuAddFriend.setClosedOnTouchOutside(true);
    com.github.clans.fab.FloatingActionButton fabInviteFacebookFriends =
      (com.github.clans.fab.FloatingActionButton) view.findViewById(
        R.id.action_invite_facebook_friends);
    com.github.clans.fab.FloatingActionButton fabImportContact =
      (com.github.clans.fab.FloatingActionButton) view.findViewById(R.id.action_import_contact);
    com.github.clans.fab.FloatingActionButton fabScanQRCode =
      (com.github.clans.fab.FloatingActionButton) view.findViewById(R.id.action_scan_qr_code);
    fabInviteFacebookFriends.setVisibility(View.GONE);
    // FIXME:  There is more stuff from the ImportContactsActivity removeal that needs removed.
//    fabImportContact.setOnClickListener(new View.OnClickListener() {
//      @Override
//      public void onClick(View view) {
//        if(menuAddFriend.isOpened()) {
//          menuAddFriend.close(true);
//        }
//        Intent intent = new Intent(getActivity(), UploadContactsActivity.class);
//        startActivity(intent);
//      }
//    });
    fabScanQRCode.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        if(menuAddFriend.isOpened()) {
          menuAddFriend.close(true);
        }
        try {
          //IntentIntegrator.forSupportFragment(this).setCaptureActivity(CustomScannerActivity.class).initiateScan();
          IntentIntegrator integrator = new IntentIntegrator(getActivity());
          integrator.setCaptureActivity(CustomScannerActivity.class);
          integrator.initiateScan();
        } catch(Exception e) {
          Uri marketUri = Uri.parse("market://details?id=com.google.zxing.client.android");
          Intent marketIntent = new Intent(Intent.ACTION_VIEW, marketUri);
          startActivity(marketIntent);
        }
      }
    });
    createCustomAnimation();
    return view;
  }

  private void createCustomAnimation() {
    AnimatorSet set = new AnimatorSet();
    ObjectAnimator scaleOutX =
      ObjectAnimator.ofFloat(menuAddFriend.getMenuIconView(), "scaleX", 1.0f, 0.2f);
    ObjectAnimator scaleOutY =
      ObjectAnimator.ofFloat(menuAddFriend.getMenuIconView(), "scaleY", 1.0f, 0.2f);
    ObjectAnimator scaleInX =
      ObjectAnimator.ofFloat(menuAddFriend.getMenuIconView(), "scaleX", 0.2f, 1.0f);
    ObjectAnimator scaleInY =
      ObjectAnimator.ofFloat(menuAddFriend.getMenuIconView(), "scaleY", 0.2f, 1.0f);
    scaleOutX.setDuration(50);
    scaleOutY.setDuration(50);
    scaleInX.setDuration(150);
    scaleInY.setDuration(150);
    scaleInX.addListener(new AnimatorListenerAdapter() {
      @Override
      public void onAnimationStart(Animator animation) {
        menuAddFriend.getMenuIconView().setImageResource(
          menuAddFriend.isOpened() ? R.drawable.fab_add_friend : R.drawable.fab_menu_close);
      }
    });
    set.play(scaleOutX).with(scaleOutY);
    set.play(scaleInX).with(scaleInY).after(scaleOutX);
    set.setInterpolator(new OvershootInterpolator(2));
    menuAddFriend.setIconToggleAnimatorSet(set);
  }

  private void showAddFriendDialog() {
    AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
    alert.setMessage(R.string.dialog_title_search_for_friend);
    LayoutInflater inflater =
      (LayoutInflater) getActivity().getSystemService(Service.LAYOUT_INFLATER_SERVICE);
    View view = inflater.inflate(R.layout.layout_add_friend, null);
    final EditText etEmail = (EditText) view.findViewById(R.id.et_user_email);
    alert.setView(view);
    alert.setNegativeButton(getString(R.string.dialog_btn_cancel),
      new DialogInterface.OnClickListener() {
        public void onClick(DialogInterface dialog, int arg1) {
          dialog.dismiss();
        }
      });
    alert.setNeutralButton(getString(R.string.dialog_btn_scan_qr_code),
      new DialogInterface.OnClickListener() {
        public void onClick(DialogInterface dialog, int arg1) {
          try {
            //IntentIntegrator.forSupportFragment(this).setCaptureActivity(CustomScannerActivity.class).initiateScan();
            IntentIntegrator integrator = new IntentIntegrator(getActivity());
            integrator.setCaptureActivity(CustomScannerActivity.class);
            integrator.initiateScan();
          } catch(Exception e) {
            Uri marketUri = Uri.parse("market://details?id=com.google.zxing.client.android");
            Intent marketIntent = new Intent(Intent.ACTION_VIEW, marketUri);
            startActivity(marketIntent);
          }
          dialog.dismiss();
        }
      });
    alert.setPositiveButton(R.string.dialog_btn_ok, new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int which) {
        final String email = etEmail.getText().toString().toLowerCase().trim();
        AddFriendModules.addFriend(getActivity(), email);
      }
    });
    AlertDialog dialog = alert.create();
    dialog.show();
  }

  @Override
  public void onActivityResult(int requestCode, int resultCode, Intent intent) {
    super.onActivityResult(requestCode, resultCode, intent);
    LogEvent.Log("QRCode Scanner",
      "onActivityResult invoked, requestCode: " + requestCode + " resultCode: " + resultCode);
    IntentResult scanResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);
    String toast = null;
    if(scanResult != null) {
      if(scanResult.getContents() == null) {
        toast = "Cancelled";
      } else {
        toast = "Scanned: " + scanResult.getContents();
        AddFriendModules.addFriend(getActivity(), scanResult.getContents());
      }
    }
  }

  private class TabFriendsAdapter extends FragmentPagerAdapter {
    private int size;

    public TabFriendsAdapter(FragmentManager fm, int size) {
      super(fm);
      this.size = size;
    }

    @Override
    public Fragment getItem(int page) {
      if(page == 0) {
        return new SearchFriendFragment();
      } else if(page == 1) {
        return new FriendRequestFragment();
      } else {
        return new FriendsFragment();
      }
    }

    @Override
    public int getCount() {
      return size;
    }

    @Override
    public CharSequence getPageTitle(int position) {
      switch(position) {
        case 0:
          return getString(R.string.strip_search);
        case 1:
          return getString(R.string.strip_requests);
        default:
          return getString(R.string.strip_friends);
      }
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
      super.destroyItem(container, position, object);
      if(position <= getCount()) {
        FragmentManager manager = ((Fragment) object).getFragmentManager();
        FragmentTransaction trans = manager.beginTransaction();
        trans.remove((Fragment) object);
        trans.commit();
      }
    }
  }
}