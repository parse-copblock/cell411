package cell411.fragment_tabs;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.URLSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseRelation;
import com.parse.ParseUser;
import com.parse.callback.FindCallback;
import com.parse.callback.SaveCallback;
import com.safearx.cell411.R;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import cell411.Singleton;
import cell411.activity.AlertDetailActivity2;
import cell411.activity.ChatActivity;
import cell411.activity.MainActivity;
import cell411.activity.ProfileImageActivity;
import cell411.activity.UserActivity;
import cell411.constants.Alert;
import cell411.constants.ParseKeys;
import cell411.models.Cell411Alert;
import cell411.models.Cell411AlertDetail;
import cell411.models.ChatRoom;
import cell411.models.Footer;
import cell411.utils.LogEvent;
import cell411.widgets.CircularImageView;
import cell411.widgets.Spinner;

import static cell411.SingletonConfig.DEFAULT_ALERTS_COUNT;
import static cell411.SingletonConfig.TIME_TO_LIVE_FOR_CHAT_ON_ALERTS;

/**
 * Created by Sachin on 18-04-2016.
 */
public class TabAlertsFragment extends Fragment {
  private final String TAG = "TabAlertsFragment";
  private Spinner spinner;
  private RecyclerView recyclerView;
  private LinearLayoutManager linearLayoutManager;
  private AlertsListAdapter adapterAlerts;
  private ArrayList<Object> alertList;

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
                           Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.fragment_tab_alerts, container, false);
    spinner = (Spinner) view.findViewById(R.id.spinner);
    recyclerView = (RecyclerView) view.findViewById(R.id.rv_alerts);
    // use this setting to improve performance if you know that changes
    // in content do not change the layout size of the RecyclerView
    recyclerView.setHasFixedSize(true);
    // use a linear layout manager
    linearLayoutManager = new LinearLayoutManager(getActivity());
    recyclerView.setLayoutManager(linearLayoutManager);
    // Check if the user is already marked as spam
    ParseRelation relation4SpamUsers = ParseUser.getCurrentUser().getRelation("spamUsers");
    final ParseQuery<ParseUser> parseQuery4SpamUsers = relation4SpamUsers.getQuery();
    parseQuery4SpamUsers.findInBackground(new FindCallback<ParseUser>() {
      @Override
      public void done(List<ParseUser> list, ParseException e) {
        if(e == null) {
          if(list != null) {
            for(int i = 0; i < list.size(); i++) {
              Singleton.INSTANCE.getSpammedUsersIdList().
                                                          add(list.get(i).getObjectId());
            }
          }
          Singleton.INSTANCE.setIsSpamUserListRetrieved(true);
          retrieveAlerts();
        } else {
          if(MainActivity.weakRef.get() != null && !MainActivity.weakRef.get().isFinishing()) {
            MainActivity.INSTANCE.getApplicationContext();
            Singleton.sendToast(e);
          }
        }
      }
    });
    return view;
  }

  private void retrieveAlerts() {
    // Received alerts (old implementation)
    ParseQuery<ParseObject> parseQuery4Cell411Alerts = ParseQuery.getQuery("Cell411Alert");
    parseQuery4Cell411Alerts.whereEqualTo("targetMembers", ParseUser.getCurrentUser());
    parseQuery4Cell411Alerts.whereExists("issuedBy");
    // Received alerts which are forwarded (old implementation)
    ParseQuery<ParseObject> parseQuery4Cell411ForwardedAlerts = ParseQuery.getQuery("Cell411Alert");
    parseQuery4Cell411ForwardedAlerts.whereEqualTo("forwardedToMembers",
      ParseUser.getCurrentUser());
    parseQuery4Cell411ForwardedAlerts.whereExists("issuedBy");
    // Received alerts on Public Cells (old implementation)
    ParseQuery<ParseObject> parseQuery4Cell411PublicAlerts = ParseQuery.getQuery("Cell411Alert");
    parseQuery4Cell411PublicAlerts.whereEqualTo("cellMembers", ParseUser.getCurrentUser());
    parseQuery4Cell411PublicAlerts.whereExists("issuedBy");
    // Received alerts (new implementation)
    ParseQuery<ParseObject> parseQuery4Cell411Alerts2 = ParseQuery.getQuery("Cell411Alert");
    parseQuery4Cell411Alerts2.whereEqualTo("audienceAU", ParseUser.getCurrentUser());
    parseQuery4Cell411Alerts2.whereExists("issuedBy");
    // Issued alerts (old implementation)
    ParseQuery<ParseObject> parseQuery4SelfCell411Alerts = ParseQuery.getQuery("Cell411Alert");
    parseQuery4SelfCell411Alerts.whereEqualTo("issuedBy", ParseUser.getCurrentUser());
    parseQuery4SelfCell411Alerts.whereDoesNotExist("to");
    parseQuery4SelfCell411Alerts.whereExists("alertType");
    // Issued alerts (new implementation)
    ParseQuery<ParseObject> parseQuery4SelfCell411Alerts2 = ParseQuery.getQuery("Cell411Alert");
    parseQuery4SelfCell411Alerts2.whereEqualTo("issuedBy", ParseUser.getCurrentUser());
    parseQuery4SelfCell411Alerts2.whereDoesNotExist("to");
    parseQuery4SelfCell411Alerts2.whereExists("alertId");
    // Forwarded alerts (old implementation)
    ParseQuery<ParseObject> parseQuery4SelfForwardedCell411Alerts =
      ParseQuery.getQuery("Cell411Alert");
    parseQuery4SelfForwardedCell411Alerts.whereEqualTo("forwardedBy", ParseUser.getCurrentUser());
    parseQuery4SelfForwardedCell411Alerts.whereDoesNotExist("to");
    parseQuery4SelfForwardedCell411Alerts.whereExists("alertType");
    // Forwarded alerts (new implementation)
    ParseQuery<ParseObject> parseQuery4SelfForwardedCell411Alerts2 =
      ParseQuery.getQuery("Cell411Alert");
    parseQuery4SelfForwardedCell411Alerts2.whereEqualTo("forwardedBy", ParseUser.getCurrentUser());
    parseQuery4SelfForwardedCell411Alerts2.whereDoesNotExist("to");
    parseQuery4SelfForwardedCell411Alerts2.whereExists("alertId");
    //parseQuery4SelfCell411Alerts.whereEqualTo("alertType", "Video");
    //ParseQuery<ParseObject> parseQuery4FriendRequests = ParseQuery.getQuery("Cell411Alert");
    //parseQuery4FriendRequests.whereEqualTo("to", ParseUser.getCurrentUser().getUsername());
    //ParseQuery<ParseObject> parseQuery4CustomAlerts = ParseQuery.getQuery("Cell411Alert");
    //parseQuery4CustomAlerts.whereEqualTo("alertType", "Custom");
    List<ParseQuery<ParseObject>> queries = new ArrayList<>();
    queries.add(parseQuery4Cell411Alerts);
    queries.add(parseQuery4Cell411ForwardedAlerts);
    queries.add(parseQuery4Cell411PublicAlerts);
    queries.add(parseQuery4Cell411Alerts2);
    queries.add(parseQuery4SelfCell411Alerts);
    queries.add(parseQuery4SelfCell411Alerts2);
    queries.add(parseQuery4SelfForwardedCell411Alerts);
    queries.add(parseQuery4SelfForwardedCell411Alerts2);
    //queries.add(parseQuery4FriendRequests);
    //queries.add(parseQuery4CustomAlerts);
    ParseQuery<ParseObject> mainQuery = ParseQuery.or(queries);
    mainQuery.include("issuedBy");
    mainQuery.include("forwardedBy");
    mainQuery.orderByDescending("createdAt");
    mainQuery.setLimit(DEFAULT_ALERTS_COUNT);
    mainQuery.findInBackground(new FindCallback<ParseObject>() {
      public void done(List<ParseObject> results, ParseException e) {
        spinner.setVisibility(View.GONE);
        if(e == null) {
          if(results != null && results.size() > 0 && isAdded()) {
            alertList = new ArrayList<>();
            GregorianCalendar cal = new GregorianCalendar();
            GregorianCalendar calCurrentTime = new GregorianCalendar();
            calCurrentTime.setTimeInMillis(System.currentTimeMillis());
            calCurrentTime.set(Calendar.HOUR, 0);
            calCurrentTime.set(Calendar.MINUTE, 0);
            calCurrentTime.set(Calendar.SECOND, 0);
            calCurrentTime.set(Calendar.MILLISECOND, 0);
            for(int i = 0; i < results.size(); i++) {
              ParseObject cell411Obj = results.get(i);
              ParseUser issuer = (ParseUser) cell411Obj.get("issuedBy");
              if(issuer == null || issuer.getObjectId() == null) {
                continue;
              }
              if(issuer.getParseGeoPoint(ParseKeys.LOCATION) == null) {
                continue;
              }
              String additionalNote = (String) cell411Obj.get("additionalNote");
              String issuerEmail = null;
              if(cell411Obj.get("issuedBy") != null) {
                String email = null;
                if(((ParseUser) cell411Obj.get("issuedBy")).getEmail() != null &&
                     !((ParseUser) cell411Obj.get("issuedBy")).getEmail().isEmpty()) {
                  email = ((ParseUser) cell411Obj.get("issuedBy")).getEmail();
                } else {
                  email = ((ParseUser) cell411Obj.get("issuedBy")).getUsername();
                }
                issuerEmail = email;
              }
              String firstName = (String) issuer.get("firstName");
              String lastName = (String) issuer.get("lastName");
              String issuerName = firstName + " " + lastName;
              String forwarderEmail = null;
              if(cell411Obj.get("forwardedBy") != null) {
                String email = null;
                if(((ParseUser) cell411Obj.get("forwardedBy")).getEmail() != null &&
                     !((ParseUser) cell411Obj.get("forwardedBy")).getEmail().isEmpty()) {
                  email = ((ParseUser) cell411Obj.get("forwardedBy")).getEmail();
                } else {
                  email = ((ParseUser) cell411Obj.get("forwardedBy")).getUsername();
                }
                forwarderEmail = email;
              }
              ParseUser forwarder = (ParseUser) cell411Obj.get("forwardedBy");
              String forwardedBy = null;
              if(cell411Obj.get("forwardedBy") != null) {
                forwardedBy = forwarder.get("firstName") + " " + forwarder.get("lastName");
              }
              Cell411Alert.AlertType alertTypeEnum;
              if(cell411Obj.get("alertId") != null) {
                int alertId = cell411Obj.getInt("alertId");
                alertTypeEnum = Cell411Alert.AlertType.values()[alertId];
              } else {
                String alertType = (String) cell411Obj.get("alertType");
                alertTypeEnum = convertStringToEnum(alertType);
              }
              String cellName = (String) cell411Obj.get("cellName");
              String entryFor = (String) cell411Obj.get("entryFor");
              String status = (String) cell411Obj.get("status");
              long millis = cell411Obj.getCreatedAt().getTime();
              cal.setTimeInMillis(millis);
              int year = cal.get(Calendar.YEAR);
              int month = cal.get(Calendar.MONTH) + 1;
              int day = cal.get(Calendar.DAY_OF_MONTH);
              int hour = cal.get(Calendar.HOUR);
              int minute = cal.get(Calendar.MINUTE);
              int ampm = cal.get(Calendar.AM_PM);
              String ap;
              if(ampm == 0) {
                ap = getString(R.string.am);
              } else {
                ap = getString(R.string.pm);
              }
              if(hour == 0) {
                hour = 12;
              }
              String hourMinute = "" + hour;
              if(minute > 0 && minute < 10) {
                hourMinute += ":0" + minute;
              } else if(minute > 9) {
                hourMinute += ":" + minute;
              }
              String time;
              if(cal.getTimeInMillis() < calCurrentTime.getTimeInMillis()) {
                // Check if the alert was issued yesterday
                time = month + "/" + day + "/" + year + " " + getString(R.string.at) + " " +
                         hourMinute + " " + ap;
              } else {
                time = getString(R.string.today_at) + " " + hourMinute + " " + ap;
              }
              boolean isVideo = false;
              boolean isPhoto = false;
              boolean isMedical = false;
              boolean isFriendRequest = false;
              boolean isCellRequest = false;
              double lat = 0;
              double lng = 0;
              String fullAddress = null;
              String city = null;
              String country = null;
              if(entryFor == null || entryFor.equals("")) {
                if(cell411Obj.getParseGeoPoint("location") != null) {
                  lat = cell411Obj.getParseGeoPoint("location").getLatitude();
                  lng = cell411Obj.getParseGeoPoint("location").getLongitude();
                }
                fullAddress = cell411Obj.getString("fullAddress");
                city = cell411Obj.getString("city");
                country = cell411Obj.getString("country");
                if(alertTypeEnum == Cell411Alert.AlertType.VIDEO) {
                  isVideo = true;
                  if(!status.equals("VOD") &&
                       issuer.getObjectId().equals(ParseUser.getCurrentUser().getObjectId())) {
                    // The video is streamed by current user but is left LIVE due to low or no connectivity
                    status = "VOD";
                    cell411Obj.put("status", "VOD");
                    cell411Obj.saveEventually();
                  }
                }
                if(alertTypeEnum == Cell411Alert.AlertType.PHOTO) {
                  isPhoto = true;
                }
                if(alertTypeEnum == Cell411Alert.AlertType.MEDICAL) {
                  isMedical = true;
                }
              } else {
                if(entryFor.equals("CR")) {
                  isCellRequest = true;
                } else {
                  isFriendRequest = true;
                }
              }
              boolean selfAlert = false;
              if(issuer.getObjectId().equals(ParseUser.getCurrentUser().getObjectId())) {
                // This alert is issued by the current user
                selfAlert = true;
              }
              Cell411Alert cell411Alert =
                new Cell411Alert(issuerName, issuer, issuerEmail, forwarder, isVideo, status,
                  millis, selfAlert, isPhoto, cell411Obj.getObjectId(), additionalNote, lat,
                  lng, isFriendRequest, isCellRequest, isMedical, forwardedBy, alertTypeEnum,
                  time, cellName, fullAddress, city, country);
              if(issuer == null || issuer.equals("") || issuer.equals("null")) {
                cell411Alert.isCustomAlert = true;
              }
              alertList.add(cell411Alert);
            }
            if(MainActivity.weakRef.get() != null && !MainActivity.weakRef.get().isFinishing()) {
                            /*ArrayList<Cell411Alert> deletedVideoAlerts = Singleton.INSTANCE.getDeletedVideoAlerts();
                            if (deletedVideoAlerts != null) {
                                for (int i = 0; i < deletedVideoAlerts.size(); i++) {
                                    for (int j = 0; j < alertList.size(); j++) {
                                        if (alertList.get(j).cell411AlertId.equals(deletedVideoAlerts.get(i).cell411AlertId)) {
                                            alertList.remove(j);
                                            --j;
                                            break;
                                        }
                                    }
                                }
                            }
                            ArrayList<String> downloadedVideoAlertIds = Singleton.INSTANCE.getDownloadedCell411VideoAlertIds();
                            if (downloadedVideoAlertIds != null) {
                                for (int i = 0; i < downloadedVideoAlertIds.size(); i++) {
                                    for (int j = 0; j < alertList.size(); j++) {
                                        if (alertList.get(j).cell411AlertId.equals(downloadedVideoAlertIds.get(i))) {
                                            alertList.get(j).isDownloading = false;
                                            alertList.get(j).isDownloaded = true;
                                            break;
                                        }
                                    }
                                }
                            }*/
              adapterAlerts = new AlertsListAdapter(alertList);
              recyclerView.setAdapter(adapterAlerts);
            }
          }
        } else {
          if(MainActivity.weakRef.get() != null && !MainActivity.weakRef.get().isFinishing()) {
            MainActivity.INSTANCE.getApplicationContext();
            Singleton.sendToast(e);
          }
        }
      }
    });
  }

  @Override
  public void onResume() {
    super.onResume();
    if(adapterAlerts != null) {
      if(Singleton.INSTANCE.isVideoDeleted()) {
        Singleton.INSTANCE.setVideoDeleted(false);
        Singleton.INSTANCE
          .addDeletedVideoToList(Singleton.INSTANCE.getTappedVideoAlert());
        adapterAlerts.arrayList.remove(Singleton.INSTANCE.getTappedVideoAlert());
      }
      adapterAlerts.notifyDataSetChanged();
    }
  }

  private String convertEnumToString(Cell411Alert.AlertType alertType) {
    switch(alertType) {
      case BROKEN_CAR:
        return getString(R.string.alert_broken_car);
      case BULLIED:
        return getString(R.string.alert_bullied);
      case CRIMINAL:
        return getString(R.string.alert_criminal);
      case DANGER:
        return getString(R.string.alert_danger);
      case FIRE:
        return getString(R.string.alert_fire);
      case GENERAL:
        return getString(R.string.alert_general);
      case MEDICAL:
        return getString(R.string.alert_medical);
      case PHOTO:
        return getString(R.string.alert_photo);
      case POLICE_ARREST:
        return getString(R.string.alert_police_arrest);
      case POLICE_INTERACTION:
        return getString(R.string.alert_police_interaction);
      case PULLED_OVER:
        return getString(R.string.alert_pulled_over);
      case VIDEO:
        return getString(R.string.alert_video);
      case HIJACK:
        return getString(R.string.alert_hijack);
      case PANIC:
        return getString(R.string.alert_panic);
      case FALLEN:
        return getString(R.string.alert_fallen);
      case PHYSICAL_ABUSE:
        return getString(R.string.alert_physical_abuse);
      case TRAPPED:
        return getString(R.string.alert_trapped);
      case CAR_ACCIDENT:
        return getString(R.string.alert_car_accident);
      case NATURAL_DISASTER:
        return getString(R.string.alert_natural_disaster);
      case PRE_AUTHORISATION:
        return getString(R.string.alert_pre_authorisation);
      default:
        return getString(R.string.alert_un_recognized);
    }
  }

  private Cell411Alert.AlertType convertStringToEnum(String alertType) {
    if(alertType.equals(Alert.BROKEN_CAR)) {
      return Cell411Alert.AlertType.BROKEN_CAR;
    } else if(alertType.equals(Alert.BULLIED)) {
      return Cell411Alert.AlertType.BULLIED;
    } else if(alertType.equals(Alert.CRIMINAL)) {
      return Cell411Alert.AlertType.CRIMINAL;
    } else if(alertType.equals(Alert.DANGER)) {
      return Cell411Alert.AlertType.DANGER;
    } else if(alertType.equals(Alert.FIRE)) {
      return Cell411Alert.AlertType.FIRE;
    } else if(alertType.equals(Alert.GENERAL)) {
      return Cell411Alert.AlertType.GENERAL;
    } else if(alertType.equals(Alert.MEDICAL)) {
      return Cell411Alert.AlertType.MEDICAL;
    } else if(alertType.equals(Alert.PHOTO)) {
      return Cell411Alert.AlertType.PHOTO;
    } else if(alertType.equals(Alert.POLICE_ARREST)) {
      return Cell411Alert.AlertType.POLICE_ARREST;
    } else if(alertType.equals(Alert.POLICE_INTERACTION)) {
      return Cell411Alert.AlertType.POLICE_INTERACTION;
    } else if(alertType.equals(Alert.PULLED_OVER)) {
      return Cell411Alert.AlertType.PULLED_OVER;
    } else if(alertType.equals(Alert.VIDEO)) {
      return Cell411Alert.AlertType.VIDEO;
    } else if(alertType.equals(Alert.HIJACK)) {
      return Cell411Alert.AlertType.HIJACK;
    } else if(alertType.equals(Alert.PANIC)) {
      return Cell411Alert.AlertType.PANIC;
    } else if(alertType.equals(Alert.FALLEN)) {
      return Cell411Alert.AlertType.FALLEN;
    } else if(alertType.equals(Alert.PHYSICAL_ABUSE)) {
      return Cell411Alert.AlertType.PHYSICAL_ABUSE;
    } else if(alertType.equals(Alert.TRAPPED)) {
      return Cell411Alert.AlertType.TRAPPED;
    } else if(alertType.equals(Alert.CAR_ACCIDENT)) {
      return Cell411Alert.AlertType.CAR_ACCIDENT;
    } else if(alertType.equals(Alert.NATURAL_DISASTER)) {
      return Cell411Alert.AlertType.NATURAL_DISASTER;
    } else if(alertType.equals(Alert.PRE_AUTHORISATION)) {
      return Cell411Alert.AlertType.PRE_AUTHORISATION;
    } else {
      return Cell411Alert.AlertType.UN_RECOGNIZED;
    }
  }

  public class AlertsListAdapter extends RecyclerView.Adapter<AlertsListAdapter.ViewHolder> {
    private final int VIEW_TYPE_ALERT = 0;
    private final int VIEW_TYPE_FOOTER = 1;
    public ArrayList<Object> arrayList;
    private final View.OnClickListener mOnClickListener = new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        Cell411Alert alert = ((Cell411Alert) adapterAlerts.arrayList.
                                                                      get(recyclerView
                                                                            .getChildAdapterPosition(
                                                                              v)));
        if(alert.isCustomAlert) {
        } else if(alert.isFriendRequest || alert.isCellRequest) {
          //showFlagAlertDialog(alert);
        } else {
          if(alert.isVideo) {
            Singleton.INSTANCE.setTappedVideoAlert(alert);
          }
          Cell411AlertDetail cell411AlertDetail = new Cell411AlertDetail();
          cell411AlertDetail.additionalNote = alert.additionalNote;
          cell411AlertDetail.cell411AlertId = alert.cell411AlertId;
          cell411AlertDetail.isCustomAlert = alert.isCustomAlert;
          cell411AlertDetail.isDownloaded = alert.isDownloaded;
          cell411AlertDetail.isDownloading = alert.isDownloading;
          cell411AlertDetail.isPhoto = alert.isPhoto;
          cell411AlertDetail.issuerEmail = alert.issuerEmail;
          cell411AlertDetail.issuerFirstName = alert.issuerFirstName;
          cell411AlertDetail.isVideo = alert.isVideo;
          cell411AlertDetail.lat = alert.lat;
          cell411AlertDetail.lng = alert.lng;
          cell411AlertDetail.millis = alert.millis;
          cell411AlertDetail.progress = alert.progress;
          cell411AlertDetail.selfAlert = alert.selfAlert;
          cell411AlertDetail.status = alert.status;
          cell411AlertDetail.isMedical = alert.isMedical;
          cell411AlertDetail.forwardedBy = alert.forwardedBy;
          cell411AlertDetail.alertType = alert.alertType;
          cell411AlertDetail.time = alert.time;
          cell411AlertDetail.cellName = alert.cellName;
          cell411AlertDetail.fullAddress = alert.fullAddress;
          cell411AlertDetail.city = alert.city;
          cell411AlertDetail.country = alert.country;
          cell411AlertDetail.isDeleted = alert.user.getInt("isDeleted");
          LogEvent.Log(TAG, "firstName: " + alert.user.getString(ParseKeys.FIRST_NAME));
          LogEvent.Log(TAG, "lastName: " + alert.user.getString(ParseKeys.LAST_NAME));
          LogEvent.Log(TAG, "username: " + alert.user.getUsername());
          LogEvent.Log(TAG, "email: " + alert.user.getEmail());
          Intent intent = new Intent(MainActivity.INSTANCE, AlertDetailActivity2.class);
          intent.putExtra("cell411AlertDetail", cell411AlertDetail);
          intent.putExtra("userObjectId", alert.user.getObjectId());
          intent.putExtra("firstName", alert.user.getString(ParseKeys.FIRST_NAME));
          intent.putExtra("lastName", alert.user.getString(ParseKeys.LAST_NAME));
          intent.putExtra("username", alert.user.getUsername());
          intent.putExtra("email", alert.user.getEmail());
          intent.putExtra("lat", alert.user.getParseGeoPoint(ParseKeys.LOCATION).getLatitude());
          intent.putExtra("lng", alert.user.getParseGeoPoint(ParseKeys.LOCATION).getLongitude());
          if(alert.user.get("imageName") != null) {
            intent.putExtra("imageName", "" + alert.user.get("imageName"));
          }
          startActivity(intent);
        }
      }
    };
    private boolean isChatEnabled;

    // Provide a suitable constructor (depends on the kind of dataset)
    public AlertsListAdapter(ArrayList<Object> arrayList) {
      this.arrayList = arrayList;
      isChatEnabled = getResources().getBoolean(R.bool.is_chat_enabled);
    }

    // Create new views (invoked by the layout manager)
    @Override
    public AlertsListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
      // create a new view
      View v = null;
      if(viewType == VIEW_TYPE_ALERT) {
        v = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_alert, parent, false);
      } else if(viewType == VIEW_TYPE_FOOTER) {
        v = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_footer, parent, false);
      }
      // set the view's size, margins, padding(s) and layout parameters
      ViewHolder vh = new ViewHolder(v, viewType);
      v.setOnClickListener(mOnClickListener);
      return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int position) {
      // - get element from your data set at this position
      // - replace the contents of the view with that element
      if(getItemViewType(position) == VIEW_TYPE_ALERT) {
        final Cell411Alert cell411Alert = (Cell411Alert) arrayList.get(position);
        String issuerEntity;
        if(cell411Alert.selfAlert) {
          issuerEntity = getString(R.string.i);
        } else {
          if(cell411Alert.user.getInt("isDeleted") == 1 && cell411Alert.forwardedBy == null) {
            issuerEntity = cell411Alert.issuerFirstName;
          } else {
            issuerEntity = "<a href='profile'>" + cell411Alert.issuerFirstName + "</a>";
          }
        }
        String forwardedBy = null;
        if(cell411Alert.forwardedBy != null) {
          if(cell411Alert.forwarder.getInt("isDeleted") == 1) {
            forwardedBy = cell411Alert.forwardedBy;
          } else {
            forwardedBy = "<a href='profile'>" + cell411Alert.forwardedBy + "</a>";
          }
        }
        String description;
        if(cell411Alert.forwardedBy != null) {
          description = getString(R.string.alert_message_forwarded, issuerEntity,
            "<a href='alert'>" + convertEnumToString(cell411Alert.alertType) + "</a>",
            forwardedBy);
        } else if(cell411Alert.cellName != null && !cell411Alert.cellName.equals("")) {
          description = "<b>" + issuerEntity + "</b>, " + getString(R.string.a_member_of) + " <b>" +
                          cell411Alert.cellName + "</b> " + getString(R.string.issued_a) +
                          " <a href='alert'><b>" + convertEnumToString(cell411Alert.alertType) +
                          "</b></a> " +
                          Singleton.getAlertNotificationTrail();
        } else {
          if(cell411Alert.selfAlert) {
            description = getString(R.string.alert_message_self,
              "<a href='alert'>" + convertEnumToString(cell411Alert.alertType) + "</a>");
          } else {
            description = getString(R.string.alert_message, issuerEntity,
              "<a href='alert'>" + convertEnumToString(cell411Alert.alertType) + "</a>");
          }
        }
        setTextViewHTML(viewHolder.txtAlert, description, cell411Alert);
        viewHolder.txtAlertTime.setText(cell411Alert.time);
        if(cell411Alert.issuerEmail != null && cell411Alert.user.getInt("isDeleted") != 1) {
          Singleton.INSTANCE.setImage(viewHolder.imgUser,
            cell411Alert.user.getObjectId() + cell411Alert.user.get("imageName"),
            cell411Alert.issuerEmail);
          viewHolder.imgUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              Intent profileImageIntent = new Intent(getActivity(), ProfileImageActivity.class);
              profileImageIntent.putExtra("userId", cell411Alert.user.getObjectId());
              profileImageIntent.putExtra("imageName", "" + cell411Alert.user.get("imageName"));
              profileImageIntent.putExtra("email", "" + cell411Alert.issuerEmail);
              startActivity(profileImageIntent);
            }
          });
        } else {
          viewHolder.imgUser.setImageResource(R.drawable.ic_placeholder_user);
          viewHolder.imgUser.setOnClickListener(null);
        }
        switch(cell411Alert.alertType) {
          case BROKEN_CAR:
            viewHolder.imgAlertType.setImageResource(R.drawable.alert_broken_car);
            viewHolder.imgAlertType.setBackgroundResource(R.drawable.bg_alert_broken_car_icon);
            break;
          case BULLIED:
            viewHolder.imgAlertType.setImageResource(R.drawable.alert_bullied);
            viewHolder.imgAlertType.setBackgroundResource(R.drawable.bg_alert_bullied_icon);
            break;
          case CRIMINAL:
            viewHolder.imgAlertType.setImageResource(R.drawable.alert_criminal);
            viewHolder.imgAlertType.setBackgroundResource(R.drawable.bg_alert_criminal_icon);
            break;
          case DANGER:
            viewHolder.imgAlertType.setImageResource(R.drawable.alert_danger);
            viewHolder.imgAlertType.setBackgroundResource(R.drawable.bg_alert_danger_icon);
            break;
          case FIRE:
            viewHolder.imgAlertType.setImageResource(R.drawable.alert_fire);
            viewHolder.imgAlertType.setBackgroundResource(R.drawable.bg_alert_fire_icon);
            break;
          case GENERAL:
            viewHolder.imgAlertType.setImageResource(R.drawable.alert_general);
            viewHolder.imgAlertType.setBackgroundResource(R.drawable.bg_alert_general_icon);
            break;
          case MEDICAL:
            viewHolder.imgAlertType.setImageResource(R.drawable.alert_medical);
            viewHolder.imgAlertType.setBackgroundResource(R.drawable.bg_alert_medical_icon);
            break;
          case PHOTO:
            viewHolder.imgAlertType.setImageResource(R.drawable.alert_photo);
            viewHolder.imgAlertType.setBackgroundResource(R.drawable.bg_alert_photo_icon);
            break;
          case POLICE_ARREST:
            viewHolder.imgAlertType.setImageResource(R.drawable.alert_police_arrest);
            viewHolder.imgAlertType.setBackgroundResource(R.drawable.bg_alert_police_arrest_icon);
            break;
          case POLICE_INTERACTION:
            viewHolder.imgAlertType.setImageResource(R.drawable.alert_police_interaction);
            viewHolder.imgAlertType.setBackgroundResource(
              R.drawable.bg_alert_police_interaction_icon);
            break;
          case PULLED_OVER:
            viewHolder.imgAlertType.setImageResource(R.drawable.alert_pulled_over);
            viewHolder.imgAlertType.setBackgroundResource(R.drawable.bg_alert_pulled_over_icon);
            break;
          case VIDEO:
            viewHolder.imgAlertType.setImageResource(R.drawable.alert_video);
            viewHolder.imgAlertType.setBackgroundResource(R.drawable.bg_alert_video_icon);
            break;
          case HIJACK:
            viewHolder.imgAlertType.setImageResource(R.drawable.alert_hijack);
            viewHolder.imgAlertType.setBackgroundResource(R.drawable.bg_alert_hijack_icon);
            break;
          case PANIC:
            viewHolder.imgAlertType.setImageResource(R.drawable.alert_panic);
            viewHolder.imgAlertType.setBackgroundResource(R.drawable.bg_alert_panic_icon);
            break;
          case FALLEN:
            viewHolder.imgAlertType.setImageResource(R.drawable.alert_fallen);
            viewHolder.imgAlertType.setBackgroundResource(R.drawable.bg_alert_fallen_icon);
            break;
          case PHYSICAL_ABUSE:
            viewHolder.imgAlertType.setImageResource(R.drawable.alert_physical_abuse);
            viewHolder.imgAlertType.setBackgroundResource(R.drawable.bg_alert_physical_abuse_icon);
            break;
          case TRAPPED:
            viewHolder.imgAlertType.setImageResource(R.drawable.alert_trapped);
            viewHolder.imgAlertType.setBackgroundResource(R.drawable.bg_alert_trapped_icon);
            break;
          case CAR_ACCIDENT:
            viewHolder.imgAlertType.setImageResource(R.drawable.alert_car_accident);
            viewHolder.imgAlertType.setBackgroundResource(R.drawable.bg_alert_car_accident_icon);
            break;
          case NATURAL_DISASTER:
            viewHolder.imgAlertType.setImageResource(R.drawable.alert_natural_disaster);
            viewHolder.imgAlertType.setBackgroundResource(
              R.drawable.bg_alert_natural_disaster_icon);
            break;
          case PRE_AUTHORISATION:
            viewHolder.imgAlertType.setImageResource(R.drawable.alert_pre_authorisation);
            viewHolder.imgAlertType.setBackgroundResource(
              R.drawable.bg_alert_pre_authorisation_icon);
            break;
          default:
            viewHolder.imgAlertType.setImageResource(R.drawable.alert_un_recognized);
            viewHolder.imgAlertType.setBackgroundResource(R.drawable.bg_alert_un_recognized_icon);
            break;
        }
        if(Singleton.INSTANCE.getSpammedUsersIdList()
             .contains(cell411Alert.user.getObjectId()) ||
             cell411Alert.selfAlert || cell411Alert.user.getInt("isDeleted") == 1) {
          viewHolder.llBtnFlag.setVisibility(View.GONE);
        } else {
          viewHolder.llBtnFlag.setVisibility(View.VISIBLE);
          if(Singleton.INSTANCE.getSpammingUsersIdList()
               .contains(cell411Alert.user.getObjectId())) {
            viewHolder.llBtnFlag.setBackgroundResource(R.drawable.bg_user_flagging);
          } else {
            viewHolder.llBtnFlag.setBackgroundResource(R.drawable.bg_user_flag);
          }
        }
        viewHolder.llBtnFlag.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View view) {
            if(!Singleton.INSTANCE.getSpammingUsersIdList()
                  .contains(cell411Alert.user.getObjectId())) {
              showFlagAlertDialog(cell411Alert);
            }
          }
        });
        if(!isChatEnabled) {
          viewHolder.imgChat.setVisibility(View.GONE);
        } else {
          if(System.currentTimeMillis() >=
               cell411Alert.millis + TIME_TO_LIVE_FOR_CHAT_ON_ALERTS) {
            // the chat is expired
            viewHolder.imgChat.setVisibility(View.GONE);
          } else {
            viewHolder.imgChat.setVisibility(View.VISIBLE);
            viewHolder.imgChat.setOnClickListener(new View.OnClickListener() {
              @Override
              public void onClick(View view) {
                Intent intentChat = new Intent(getActivity(), ChatActivity.class);
                intentChat.putExtra("entityType", ChatRoom.EntityType.ALERT.toString());
                intentChat.putExtra("entityObjectId", cell411Alert.cell411AlertId);
                intentChat.putExtra("entityName", convertEnumToString(cell411Alert.alertType));
                intentChat.putExtra("alertType", convertEnumToString(cell411Alert.alertType));
                intentChat.putExtra("issuedBy", cell411Alert.issuerFirstName);
                intentChat.putExtra("time", String.valueOf(cell411Alert.millis));
                startActivity(intentChat);
              }
            });
          }
        }
      } else {
        final Footer footer = (Footer) arrayList.get(position);
        viewHolder.txtInfo.setText(footer.info);
        if(footer.spinnerVisibility) {
          viewHolder.spinner.setVisibility(View.VISIBLE);
        } else {
          viewHolder.spinner.setVisibility(View.GONE);
        }
      }
    }

    protected void setTextViewHTML(TextView text, String html, Cell411Alert alert) {
      CharSequence sequence = Html.fromHtml(html);
      SpannableStringBuilder strBuilder = new SpannableStringBuilder(sequence);
      URLSpan[] urls = strBuilder.getSpans(0, sequence.length(), URLSpan.class);
      for(URLSpan span : urls) {
        makeLinkClickable(strBuilder, span, alert);
      }
      text.setText(strBuilder);
      text.setMovementMethod(LinkMovementMethod.getInstance());
    }

    protected void makeLinkClickable(SpannableStringBuilder strBuilder, final URLSpan span,
                                     final Cell411Alert alert) {
      int start = strBuilder.getSpanStart(span);
      int end = strBuilder.getSpanEnd(span);
      int flags = strBuilder.getSpanFlags(span);
      ClickableSpan clickable = new ClickableSpan() {
        public void onClick(View view) {
          if(span.getURL().equals("profile")) {
            Intent intentUser = new Intent(getActivity(), UserActivity.class);
            intentUser.putExtra("userId", alert.user.getObjectId());
            intentUser.putExtra("imageName", "" + alert.user.get("imageName"));
            intentUser.putExtra("firstName", alert.user.getString(ParseKeys.FIRST_NAME));
            intentUser.putExtra("lastName", alert.user.getString(ParseKeys.LAST_NAME));
            intentUser.putExtra("username", alert.user.getUsername());
            intentUser.putExtra("email", alert.user.getEmail());
            if(alert.user.getParseGeoPoint(ParseKeys.LOCATION) != null) {
              intentUser.putExtra("lat",
                alert.user.getParseGeoPoint(ParseKeys.LOCATION).getLatitude());
              intentUser.putExtra("lng",
                alert.user.getParseGeoPoint(ParseKeys.LOCATION).getLongitude());
            }
            startActivity(intentUser);
          } else if(span.getURL().equals("alert")) {
            if(alert.isVideo) {
              Singleton.INSTANCE.setTappedVideoAlert(alert);
            }
            Cell411AlertDetail cell411AlertDetail = new Cell411AlertDetail();
            cell411AlertDetail.additionalNote = alert.additionalNote;
            cell411AlertDetail.cell411AlertId = alert.cell411AlertId;
            cell411AlertDetail.isCustomAlert = alert.isCustomAlert;
            cell411AlertDetail.isDownloaded = alert.isDownloaded;
            cell411AlertDetail.isDownloading = alert.isDownloading;
            cell411AlertDetail.isPhoto = alert.isPhoto;
            cell411AlertDetail.issuerEmail = alert.issuerEmail;
            cell411AlertDetail.issuerFirstName = alert.issuerFirstName;
            cell411AlertDetail.isVideo = alert.isVideo;
            cell411AlertDetail.lat = alert.lat;
            cell411AlertDetail.lng = alert.lng;
            cell411AlertDetail.millis = alert.millis;
            cell411AlertDetail.progress = alert.progress;
            cell411AlertDetail.selfAlert = alert.selfAlert;
            cell411AlertDetail.status = alert.status;
            cell411AlertDetail.isMedical = alert.isMedical;
            cell411AlertDetail.forwardedBy = alert.forwardedBy;
            cell411AlertDetail.alertType = alert.alertType;
            cell411AlertDetail.time = alert.time;
            cell411AlertDetail.cellName = alert.cellName;
            cell411AlertDetail.fullAddress = alert.fullAddress;
            cell411AlertDetail.city = alert.city;
            cell411AlertDetail.country = alert.country;
            cell411AlertDetail.isDeleted = alert.user.getInt("isDeleted");
            LogEvent.Log(TAG, "firstName: " + alert.user.getString(ParseKeys.FIRST_NAME));
            LogEvent.Log(TAG, "lastName: " + alert.user.getString(ParseKeys.LAST_NAME));
            LogEvent.Log(TAG, "username: " + alert.user.getUsername());
            LogEvent.Log(TAG, "email: " + alert.user.getEmail());
            Intent intent = new Intent(MainActivity.INSTANCE, AlertDetailActivity2.class);
            intent.putExtra("cell411AlertDetail", cell411AlertDetail);
            intent.putExtra("userObjectId", alert.user.getObjectId());
            intent.putExtra("firstName", alert.user.getString(ParseKeys.FIRST_NAME));
            intent.putExtra("lastName", alert.user.getString(ParseKeys.LAST_NAME));
            intent.putExtra("username", alert.user.getUsername());
            intent.putExtra("email", alert.user.getEmail());
            intent.putExtra("lat", alert.user.getParseGeoPoint(ParseKeys.LOCATION).getLatitude());
            intent.putExtra("lng", alert.user.getParseGeoPoint(ParseKeys.LOCATION).getLongitude());
            if(alert.user.get("imageName") != null) {
              intent.putExtra("imageName", "" + alert.user.get("imageName"));
            }
            startActivity(intent);
          }
        }
      };
      strBuilder.setSpan(clickable, start, end, flags);
      strBuilder.removeSpan(span);
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
      return arrayList.size();
    }

    @Override
    public int getItemViewType(int position) {
      super.getItemViewType(position);
      if(arrayList.get(position) instanceof Cell411Alert) {
        return VIEW_TYPE_ALERT;
      } else {
        return VIEW_TYPE_FOOTER;
      }
    }

    private void flagUser(final Cell411Alert cell411Alert) {
      if(!Singleton.INSTANCE.getSpammingUsersIdList()
            .contains(cell411Alert.user.getObjectId())) {
        Singleton.INSTANCE.getSpammingUsersIdList().add(cell411Alert.user.getObjectId());
        notifyDataSetChanged();
      }
      // Make an entry on both end
      ParseRelation<ParseUser> relation = ParseUser.getCurrentUser().getRelation("spamUsers");
      relation.add(cell411Alert.user);
      ParseUser.getCurrentUser().saveInBackground(new SaveCallback() {
        @Override
        public void done(ParseException e) {
          if(e == null) {
            ParseObject taskObject = new ParseObject("Task");
            taskObject.put("userId", cell411Alert.user.getObjectId());
            taskObject.put("assigneeUserId", ParseUser.getCurrentUser().getObjectId());
            taskObject.put("task", "SPAM_ADD");
            taskObject.put("status", "PENDING");
            taskObject.saveInBackground(new SaveCallback() {
              @Override
              public void done(ParseException e) {
                if(e == null) {
                  Singleton.INSTANCE.getSpammedUsersIdList()
                    .add(cell411Alert.user.getObjectId());
                  if(MainActivity.weakRef.get() != null &&
                       !MainActivity.weakRef.get().isFinishing()) {
                    MainActivity.INSTANCE.getApplicationContext();
                    Singleton.sendToast(cell411Alert.issuerFirstName + " " +
                                          getString(R.string.blocked_successfully));
                  }
                } else {
                  if(MainActivity.weakRef.get() != null &&
                       !MainActivity.weakRef.get().isFinishing()) {
                    MainActivity.INSTANCE.getApplicationContext();
                    Singleton.sendToast(e);
                  }
                }
                Singleton.INSTANCE.getSpammingUsersIdList()
                  .remove(cell411Alert.user.getObjectId());
                notifyDataSetChanged();
              }
            });
          } else {
            Singleton.INSTANCE.getSpammingUsersIdList()
              .remove(cell411Alert.user.getObjectId());
            notifyDataSetChanged();
            if(MainActivity.weakRef.get() != null && !MainActivity.weakRef.get().isFinishing()) {
              MainActivity.INSTANCE.getApplicationContext();
              Singleton.sendToast(e);
            }
          }
        }
      });
    }

    private void showFlagAlertDialog(final Cell411Alert cell411Alert) {
      AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
      alert.setMessage(getString(R.string.dialog_msg_flag_user, cell411Alert.issuerFirstName));
      alert.setPositiveButton(R.string.dialog_btn_yes, new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialogInterface, int i) {
          flagUser(cell411Alert);
        }
      });
      alert.setNegativeButton(R.string.dialog_btn_no, new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialogInterface, int i) {
        }
      });
      AlertDialog dialog = alert.create();
      dialog.show();
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public class ViewHolder extends RecyclerView.ViewHolder {
      // each data item is just a string in this case
      private TextView txtAlert;
      private TextView txtAlertTime;
      private LinearLayout llBtnFlag;
      private CircularImageView imgUser;
      private ImageView imgChat;
      private ImageView imgAlertType;
      private TextView txtInfo;
      private Spinner spinner;

      public ViewHolder(View view, int type) {
        super(view);
        if(type == VIEW_TYPE_ALERT) {
          txtAlert = (TextView) view.findViewById(R.id.txt_alert);
          txtAlertTime = (TextView) view.findViewById(R.id.txt_alert_time);
          llBtnFlag = (LinearLayout) view.findViewById(R.id.rl_btn_flag);
          imgUser = (CircularImageView) view.findViewById(R.id.img_user);
          imgChat = (ImageView) view.findViewById(R.id.img_chat);
          imgAlertType = (ImageView) view.findViewById(R.id.img_alert_type);
        } else if(type == VIEW_TYPE_FOOTER) {
          txtInfo = (TextView) view.findViewById(R.id.txt_info);
          spinner = (Spinner) view.findViewById(R.id.spinner);
        }
      }
    }
  }
}