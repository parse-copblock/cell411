package cell411.fragment_tabs;

import android.animation.AnimatorInflater;
import android.animation.AnimatorSet;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Service;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.model.PlaceLikelihood;
import com.google.android.libraries.places.api.net.FindCurrentPlaceRequest;
import com.google.android.libraries.places.api.net.FindCurrentPlaceResponse;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.parse.ParseException;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseRelation;
import com.parse.ParseUser;
import com.parse.callback.FindCallback;
import com.parse.callback.SaveCallback;
import com.safearx.cell411.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.math.RoundingMode;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

import cell411.Singleton;
import cell411.activity.AdvanceSettingsActivity;
import cell411.activity.AlertIssuingActivity;
import cell411.activity.IssuePanicAlertActivity;
import cell411.activity.MainActivity;
import cell411.activity.MapObjectiveActivity;
import cell411.activity.OSMObjectiveActivity;
import cell411.constants.Alert;
import cell411.constants.Constants;
import cell411.fragments.LocationFragment;
import cell411.interfaces.CurrentLocationFromLocationUpdatesCallBack;
import cell411.interfaces.LastLocationCallBack;
import cell411.interfaces.LocationFailToRetrieveCallBack;
import cell411.methods.DialogModules4IssuingAlert;
import cell411.methods.Modules;
import cell411.models.OSMObjective;
import cell411.models.User;
import cell411.utils.ImagePicker;
import cell411.utils.LogEvent;
import cell411.widgets.OpaqueAreaClickListener;
import cell411.widgets.PolygonImageView;

import static cell411.Singleton.sendToast;
import static cell411.SingletonImageFactory.GRAVATAR_SIZE;

/**
 * Created by Sachin on 18-04-2016.
 */
public class TabMapFragment extends Fragment
  implements OpaqueAreaClickListener, OnMapReadyCallback, View.OnClickListener,
               LastLocationCallBack, CurrentLocationFromLocationUpdatesCallBack,
               LocationFailToRetrieveCallBack {
  private static final String TAG = TabMapFragment.class.getSimpleName();
  private final static int PERMISSION_CALL_PHONE = 1;
  private Location mLastLocation;
  private GoogleMap mMap;
  private AnimatorSet setRadialOuterAnimator;
  private AnimatorSet setRadialInner4Animator;
  private RelativeLayout rlRadialMenu;
  private ImagePicker imagePicker;
  private FloatingActionButton fabPatrolMode;
  private FloatingActionButton fabLocationCenter;
  private ImageView imgLocationCenter;
  private int patrolMode = 1;
  private int COLOR_ACCENT;
  private int COLOR_PRIMARY;
  private int COLOR_GRAY_CCC;
  private boolean isLocationCenterEnabled = true;
  private Marker marker;
  private SharedPreferences prefs;
  private LocationFragment locationFragment;
  private boolean isLastLocationCallInvoked;

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
                           Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.fragment_tab_map, container, false);
    COLOR_ACCENT = getResources().getColor(R.color.colorAccent);
    COLOR_GRAY_CCC = getResources().getColor(R.color.gray_ccc);
    COLOR_PRIMARY = getResources().getColor(R.color.highlight_color);
    rlRadialMenu = (RelativeLayout) view.findViewById(R.id.rl_radial_menu);
    RelativeLayout rlRadialMenuOuterLayer = (RelativeLayout) view.findViewById(R.id.rl_outer);
    RelativeLayout rlRadialMenuInnerLayer = (RelativeLayout) view.findViewById(R.id.rl_inner);
    setRadialOuterAnimator = (AnimatorSet) AnimatorInflater.loadAnimator(MainActivity.INSTANCE,
      R.animator.radial_outer_animator);
    setRadialOuterAnimator.setTarget(rlRadialMenuOuterLayer);
    setRadialInner4Animator = (AnimatorSet) AnimatorInflater.loadAnimator(MainActivity.INSTANCE,
      R.animator.radial_inner_animator);
    setRadialInner4Animator.setTarget(rlRadialMenuInnerLayer);
    initSlices(view);
    prefs = Singleton.INSTANCE.getAppPrefs();
    locationFragment = new LocationFragment();
    FragmentManager fragmentManager = getChildFragmentManager();
    fragmentManager.beginTransaction().add(locationFragment, "locationFragment").commit();
    // Obtain the SupportMapFragment and get notified when the map is ready to be used.
    SupportMapFragment mapFragment =
      (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
    mapFragment.getMapAsync(this);
    return view;
  }

  @Override
  public void onStart() {
    super.onStart();
    if(!isLastLocationCallInvoked) {
      isLastLocationCallInvoked = true;
      locationFragment.getLastLocation(this, this, Feature.SHOW_LOCATION_ON_MAP);
      locationFragment.startLocationService();
    }
  }

  @Override
  public void onResume() {
    super.onResume();
    if(marker != null && ParseUser.getCurrentUser() != null) {
      Singleton.INSTANCE.setImageOnMap(marker);
    }
    if(mMap != null) {
      if(AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES) {
        mMap.setMapStyle(
          MapStyleOptions.loadRawResourceStyle(getActivity(), R.raw.map_style_aubergine));
      } else {
        mMap.setMapStyle(
          MapStyleOptions.loadRawResourceStyle(getActivity(), R.raw.map_style_standard));
      }
    }
    checkIfThereArePendingRequests();
  }

  @Override
  public void onPause() {
    super.onPause();
    SharedPreferences.Editor editor = prefs.edit();
    editor.putBoolean("LocationCenter", isLocationCenterEnabled);
    editor.commit();
  }

  @Override
  public void onLastLocationRetrieved(Location mLastLocation, Feature feature) {
    LogEvent.Log(TAG, "onLastLocationRetrieved() invoked");
    this.mLastLocation = mLastLocation;
    if(mLastLocation != null && mMap != null) {
      setMarkerAtCurrentLocation();
    }
  }

  @Override
  public void onCurrentLocationRetrievedFromLocationUpdates(Location location, Feature feature) {
    LogEvent.Log(TAG, "onCurrentLocationRetrievedFromLocationUpdates() invoked");
    mLastLocation = location;
    if(mLastLocation != null && mMap != null) {
      setMarkerAtCurrentLocation();
    }
  }

  @Override
  public void onLocationFailToRetrieve(FailureReason failureReason, Feature feature) {
    LogEvent.Log(TAG, "onLocationFailToRetrieve() invoked: " + failureReason.toString());
    switch(failureReason) {
      case PERMISSION_DENIED:
      case SETTINGS_REQUEST_DENIED:
        final SharedPreferences prefs = Singleton.INSTANCE.getAppPrefs();
        if(prefs.getBoolean("LocationUpdate", true)) {
          boolean isPatrolModeFeatureEnabled =
            getResources().getBoolean(R.bool.is_patrol_mode_feature_enabled);
          int patrolMode = 0, newPublicCellAlert = 0;
          boolean rideRequestAlert = false;
          String str = "";
          if(isPatrolModeFeatureEnabled) {
            if(ParseUser.getCurrentUser().get("PatrolMode") != null) {
              patrolMode = ParseUser.getCurrentUser().getInt("PatrolMode");
              if(patrolMode == 1) {
                str += "\n- " + "Patrol mode";
              }
            }
          }
          if(ParseUser.getCurrentUser().get("newPublicCellAlert") != null) {
            newPublicCellAlert = ParseUser.getCurrentUser().getInt("newPublicCellAlert");
            if(newPublicCellAlert == 1) {
              str += "\n- " + "New Public Cell alert";
            }
          }
          if(!str.equals("")) {
            showFeaturesDisabledDialog(patrolMode, newPublicCellAlert, rideRequestAlert);
          }
                    /*if ((isPatrolModeFeatureEnabled && patrolMode == 1) && newPublicCellAlert == 1) {
                        showPatrolModeAndNewPublicCellJoinRequestDisabledAlert(1, 1);
                    } else if ((isPatrolModeFeatureEnabled && patrolMode == 1)) {
                        showPatrolModeAndNewPublicCellJoinRequestDisabledAlert(1, 0);
                    } else if (newPublicCellAlert == 1) {
                        showPatrolModeAndNewPublicCellJoinRequestDisabledAlert(0, 1);
                    }*/
          SharedPreferences.Editor editor = prefs.edit();
          boolean somethingChanged = false;
          if(isPatrolModeFeatureEnabled && patrolMode == 1) {
            somethingChanged = true;
            editor.putBoolean("patrolModeDisabledByApp", true);
            ParseUser.getCurrentUser().put("PatrolMode", 0);
          }
          if(newPublicCellAlert == 1) {
            somethingChanged = true;
            editor.putBoolean("newPublicCellAlertDisabledByApp", true);
            ParseUser.getCurrentUser().put("newPublicCellAlert", 0);
          }
          if(rideRequestAlert) {
            somethingChanged = true;
            editor.putBoolean("rideRequestAlertDisabledByApp", true);
            ParseUser.getCurrentUser().put("rideRequestAlert", false);
          }
          if(somethingChanged) {
            editor.commit();
            ParseUser.getCurrentUser().saveEventually();
          }
        }
        break;
      default:
        locationFragment.getCurrentLocationFromLocationUpdates(this, feature);
    }
  }

  private void showFeaturesDisabledDialog(int patrolMode, int newPublicCellAlert,
                                          boolean rideRequestAlert) {
    AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
    alert.setTitle(R.string.dialog_title_location_alert);
    String message;
    if(patrolMode == 1 && newPublicCellAlert == 1 && rideRequestAlert) {
      message =
        getString(R.string.msg_location_three_features_disabled, getString(R.string.app_name),
          getString(R.string.feature_patrol_mode),
          getString(R.string.feature_new_public_cell_alert),
          getString(R.string.feature_ride_request_alert));
    } else if(patrolMode == 1 && newPublicCellAlert == 1) {
      message = getString(R.string.msg_location_two_features_disabled, getString(R.string.app_name),
        getString(R.string.feature_patrol_mode),
        getString(R.string.feature_new_public_cell_alert));
    } else if(newPublicCellAlert == 1 && rideRequestAlert) {
      message = getString(R.string.msg_location_two_features_disabled, getString(R.string.app_name),
        getString(R.string.feature_new_public_cell_alert),
        getString(R.string.feature_ride_request_alert));
    } else if(patrolMode == 1 && rideRequestAlert) {
      message = getString(R.string.msg_location_two_features_disabled, getString(R.string.app_name),
        getString(R.string.feature_patrol_mode), getString(R.string.feature_ride_request_alert));
    } else if(patrolMode == 1) {
      message = getString(R.string.msg_location_one_feature_disabled, getString(R.string.app_name),
        getString(R.string.feature_patrol_mode));
    } else if(newPublicCellAlert == 1) {
      message = getString(R.string.msg_location_one_feature_disabled, getString(R.string.app_name),
        getString(R.string.feature_new_public_cell_alert));
    } else {
      message = getString(R.string.msg_location_one_feature_disabled, getString(R.string.app_name),
        getString(R.string.feature_ride_request_alert));
    }
    alert.setMessage(message);
    alert.setPositiveButton(R.string.dialog_btn_ok, new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int which) {
      }
    });
    AlertDialog dialog = alert.create();
    dialog.show();
  }

  public void onLocationChange(Location location) {
    LogEvent.Log(TAG, "onLocationChange() invoked");
    mLastLocation = location;
    if(marker != null && mMap != null) {
      LogEvent.Log(TAG, "Updating marker position");
      marker.setPosition(new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude()));
      if(isLocationCenterEnabled) {
        LatLng latlng = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latlng, 16f));
      }
    } else {
      LogEvent.Log(TAG, "Marker position not updated");
    }
  }

  @Override
  public void onOpaqueAreaClicked(String TAG) {
    String alertType = null;
    if(TAG.equals("criminal")) {
      alertType = Alert.CRIMINAL;
    } else if(TAG.equals("pulled_over")) {
      alertType = Alert.PULLED_OVER;
    } else if(TAG.equals("police_interaction")) {
      alertType = Alert.POLICE_INTERACTION;
    } else if(TAG.equals("pre_authorisation")) {
      alertType = Alert.PRE_AUTHORISATION;
    } else if(TAG.equals("police_arrest")) {
      alertType = Alert.POLICE_ARREST;
    } else if(TAG.equals("danger")) {
      alertType = Alert.DANGER;
    } else if(TAG.equals("medical")) {
      alertType = Alert.MEDICAL;
    } else if(TAG.equals("being_bullied")) {
      alertType = Alert.BULLIED;
    } else if(TAG.equals("general")) {
      alertType = Alert.GENERAL;
    } else if(TAG.equals("photo")) {
      // Initiate Photo Alert
      if(imagePicker == null) {
        imagePicker = new ImagePicker(getActivity(), this, true);
      }
      imagePicker.displayAddImageChooserDialog();
    } else if(TAG.equals("video")) {
      alertType = Alert.VIDEO;
    } else if(TAG.equals("fire")) {
      alertType = Alert.FIRE;
    } else if(TAG.equals("broken_car")) {
      alertType = Alert.BROKEN_CAR;
    } else if(TAG.equals("hijack")) {
      alertType = Alert.HIJACK;
    } else if(TAG.equals("physical_abuse")) {
      alertType = Alert.PHYSICAL_ABUSE;
    } else if(TAG.equals("trapped")) {
      alertType = Alert.TRAPPED;
    } else if(TAG.equals("car_accident")) {
      alertType = Alert.CAR_ACCIDENT;
    } else if(TAG.equals("natural_disaster")) {
      alertType = Alert.NATURAL_DISASTER;
    }
    if(alertType != null) {
      Intent intentAlertIssuing = new Intent(getActivity(), AlertIssuingActivity.class);
      intentAlertIssuing.putExtra("alertType", alertType);
      startActivity(intentAlertIssuing);
    }
  }

  private void checkPermissionAndDialEmergencyNumber() {
    dialNumber();
  }

  @Override
  public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                         @NonNull int[] grantResults) {
    if(requestCode == PERMISSION_CALL_PHONE) {
      if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
        dialNumber();
      } else {
        getActivity().getApplicationContext();
        sendToast("App does not have permission to make a call");
      }
    } else if(requestCode == ImagePicker.PERMISSION_CAMERA) {
      if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
        if(imagePicker == null) {
          imagePicker = new ImagePicker(getActivity(), this, true);
        }
        imagePicker.dispatchTakePictureIntent();
      } else {
        getActivity().getApplicationContext();
        sendToast("App does not have permission to capture image");
      }
    } else {
      super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }
  }

  private void dialNumber() {
    Intent callIntent = new Intent(Intent.ACTION_DIAL);
    callIntent.setData(Uri.parse("tel:112"));
    startActivity(callIntent);
  }

  @Override
  public void onClick(View v) {
    int id = v.getId();
    switch(id) {
      case R.id.fab_patrol_mode:
        togglePatrolMode();
        break;
      case R.id.fab_location_center:
      case R.id.img_location_center:
        toggleLocationCenter();
        break;
    }
  }

  @Override
  public void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);
    LogEvent.Log(TAG, "requestCode: " + requestCode + " resultCode: " + resultCode);
    int SOCIAL_SHARING_REQUEST_CODE = 12321;
    if(resultCode == getActivity().RESULT_OK && requestCode == SOCIAL_SHARING_REQUEST_CODE) {
      // Display video recording alert
      LogEvent.Log(TAG, "Shared: Display video recording alert");
      DialogModules4IssuingAlert.showVideoStreamingAlert(getActivity(), null);
    } else if(resultCode == getActivity().RESULT_CANCELED &&
                requestCode == SOCIAL_SHARING_REQUEST_CODE) {
      // Display video recording alert
      LogEvent.Log(TAG, "Cancelled: Display video recording alert");
      DialogModules4IssuingAlert.showVideoStreamingAlert(getActivity(), null);
    } else if(resultCode == getActivity().RESULT_OK &&
                requestCode == ImagePicker.REQUEST_IMAGE_CAPTURE) {
      imagePicker.saveCameraImage("photo_alert.png");
      Intent intentAlertIssuing = new Intent(getActivity(), AlertIssuingActivity.class);
      intentAlertIssuing.putExtra("alertType", Alert.PHOTO);
      startActivity(intentAlertIssuing);
    } else if(resultCode == getActivity().RESULT_OK &&
                requestCode == ImagePicker.REQUEST_LOAD_IMAGE) {
      if(data != null && data.getData() != null) {
        imagePicker.saveGalleryImage(data);
        Intent intentAlertIssuing = new Intent(getActivity(), AlertIssuingActivity.class);
        intentAlertIssuing.putExtra("alertType", Alert.PHOTO);
        startActivity(intentAlertIssuing);
      } else {
        LogEvent.Log(TAG, "image data is null");
      }
    }
  }

  private void initSlices(View view) {
    fabPatrolMode = (FloatingActionButton) view.findViewById(R.id.fab_patrol_mode);
    fabLocationCenter = (FloatingActionButton) view.findViewById(R.id.fab_location_center);
    imgLocationCenter = view.findViewById(R.id.img_location_center);

    ((PolygonImageView) view.findViewById(R.id.img_medical)).setOnOpaqueAreaClickListener(this);
    ((PolygonImageView) view.findViewById(R.id.img_criminal)).setOnOpaqueAreaClickListener(this);
    ((PolygonImageView) view.findViewById(R.id.img_pulled_over)).setOnOpaqueAreaClickListener(
      this);
    ((PolygonImageView) view.findViewById(
      R.id.img_police_interaction)).setOnOpaqueAreaClickListener(this);
    ((PolygonImageView) view.findViewById(R.id.img_police_arrest)).setOnOpaqueAreaClickListener(
      this);
    ((PolygonImageView) view.findViewById(R.id.img_panic_button)).setOnOpaqueAreaClickListener(
      this);
    ((PolygonImageView) view.findViewById(R.id.img_danger)).setOnOpaqueAreaClickListener(this);
    ((PolygonImageView) view.findViewById(R.id.img_broken_car)).setOnOpaqueAreaClickListener(
      this);
    ((PolygonImageView) view.findViewById(R.id.img_being_bullied)).setOnOpaqueAreaClickListener(
      this);
    ((PolygonImageView) view.findViewById(R.id.img_general)).setOnOpaqueAreaClickListener(this);
    ((PolygonImageView) view.findViewById(R.id.img_photo)).setOnOpaqueAreaClickListener(this);
    ((PolygonImageView) view.findViewById(R.id.img_video)).setOnOpaqueAreaClickListener(this);
    ((PolygonImageView) view.findViewById(R.id.img_fire)).setOnOpaqueAreaClickListener(this);

    imgLocationCenter.setOnClickListener(this);
    if(ParseUser.getCurrentUser().get("PatrolMode") != null) {
      patrolMode = (int) ParseUser.getCurrentUser().get("PatrolMode");
      LogEvent.Log(TAG, "patrolMode: " + patrolMode);
    } else {
      ParseUser.getCurrentUser().put("PatrolMode", 1);
      ParseUser.getCurrentUser().saveInBackground();
      LogEvent.Log(TAG, "patrolMode is null");
    }
    if(patrolMode == 1) {
      fabPatrolMode.setBackgroundTintList(ColorStateList.valueOf(COLOR_ACCENT));
      fabPatrolMode.setImageResource(R.drawable.fab_patrol_mode_enabled);
    } else {
      fabPatrolMode.setBackgroundTintList(ColorStateList.valueOf(COLOR_GRAY_CCC));
      fabPatrolMode.setImageResource(R.drawable.fab_patrol_mode_disabled);
    }
    isLocationCenterEnabled = Singleton.INSTANCE
                                .getAppPrefs().getBoolean("LocationCenter", true);
    if(isLocationCenterEnabled) {
      fabLocationCenter.setBackgroundTintList(ColorStateList.valueOf(COLOR_PRIMARY));
      fabLocationCenter.setImageResource(R.drawable.fab_loc_center);
      imgLocationCenter.setBackgroundResource(R.drawable.bg_location_center_enabled);
      imgLocationCenter.setImageResource(R.drawable.fab_loc_center);
    } else {
      fabLocationCenter.setBackgroundTintList(ColorStateList.valueOf(COLOR_GRAY_CCC));
      fabLocationCenter.setImageResource(R.drawable.fab_loc_center);
      imgLocationCenter.setBackgroundResource(R.drawable.bg_location_center_disabled);
      imgLocationCenter.setImageResource(R.drawable.fab_loc_center);
    }
  }

  public void setMapType(ViewType viewType) {
    if(mMap == null) {
      sendToast(R.string.please_wait);
      return;
    }
    switch(viewType) {
      case STANDARD:
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        break;
      case SATELLITE:
        mMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
        break;
      case HYBRID:
        mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
        break;
    }
  }

  private void setUpMap() {
    mMap.getUiSettings().setAllGesturesEnabled(true);
    mMap.getUiSettings().setCompassEnabled(true);
    mMap.getUiSettings().setMyLocationButtonEnabled(false);
    mMap.getUiSettings().setRotateGesturesEnabled(true);
    mMap.getUiSettings().setScrollGesturesEnabled(true);
    mMap.getUiSettings().setTiltGesturesEnabled(true);
    mMap.getUiSettings().setZoomControlsEnabled(false);
    mMap.getUiSettings().setZoomGesturesEnabled(true);
  }

  @Override
  public void onMapReady(GoogleMap googleMap) {
    mMap = googleMap;
    setUpMap();
    if(mLastLocation != null) {
      setMarkerAtCurrentLocation();
    } else {
      setMarkerAtOldLocation();
    }
    if(AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES) {
      googleMap.setMapStyle(
        MapStyleOptions.loadRawResourceStyle(getActivity(), R.raw.map_style_aubergine));
    } else {
      mMap.setMapStyle(
        MapStyleOptions.loadRawResourceStyle(getActivity(), R.raw.map_style_standard));
    }
    //retrieveNearByPlaces();
    retrieveMapObjectives();
    retrieveAmenities();
  }

  private void retrieveAmenities() {
    LogEvent.Log(TAG, "retrieveAmenities() invoked");
    LatLngBounds bounds = mMap.getProjection().getVisibleRegion().latLngBounds;
    LatLng northeast = bounds.northeast;
    LatLng southwest = bounds.southwest;
    CharSequence text = "ne:" + northeast + " sw:" + southwest;
    getActivity();
    sendToast(text);
    new ApiCallOverpass(getActivity(), mMap, bounds).execute();
  }

  private void retrieveNearByPlaces() {
    // Initialize Places.
    Places.initialize(getActivity().getApplicationContext(), getString(R.string.google_api_key));
    // Create a new Places client instance.
    PlacesClient placesClient = Places.createClient(getActivity());
    // Use fields to define the data types to return.
    List<Place.Field> placeFields = Arrays.asList(Place.Field.NAME);
    // Use the builder to create a FindCurrentPlaceRequest.
    FindCurrentPlaceRequest request = FindCurrentPlaceRequest.builder(placeFields).build();
    // Call findCurrentPlace and handle the response (first check that the user has granted permission).
    if(ContextCompat.checkSelfPermission(getActivity(),
      android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
      Task<FindCurrentPlaceResponse> placeResponse = placesClient.findCurrentPlace(request);
      placeResponse.addOnCompleteListener(new OnCompleteListener<FindCurrentPlaceResponse>() {
        @Override
        public void onComplete(@NonNull Task<FindCurrentPlaceResponse> task) {
          if(task.isSuccessful()) {
            FindCurrentPlaceResponse response = task.getResult();
            for(PlaceLikelihood placeLikelihood : response.getPlaceLikelihoods()) {
              LogEvent.Log(TAG, String.format("Place '%s' has likelihood: %f",
                placeLikelihood.getPlace().getName(), placeLikelihood.getLikelihood()));
            }
          } else {
            Exception exception = task.getException();
            if(exception instanceof ApiException) {
              ApiException apiException = (ApiException) exception;
              LogEvent.Log(TAG, "Place not found: " + apiException.getStatusCode());
            }
          }
        }
      });
    } else {
      // A local method to request required permissions;
      // See https://developer.android.com/training/permissions/requesting
      //getLocationPermission();
      getActivity();
      sendToast("Permission not available");
    }
  }

  private void retrieveMapObjectives() {
    LogEvent.Log(TAG, "retrieveMapObjectives() invoked");
    ParseQuery<ParseObject> mapObjectiveQuery = ParseQuery.getQuery("MapObjective");
    //mapObjectiveQuery.whereNotEqualTo("createdBy", ParseUser.getCurrentUser());
    mapObjectiveQuery.include("createdBy");
    ParseGeoPoint currentLocation =
      new ParseGeoPoint(Singleton.INSTANCE.getLatitude(), Singleton.INSTANCE.getLongitude());
    String metric = Singleton.INSTANCE.getAppPrefs().getString("metric", "kms");
    int searchRadius = Singleton.INSTANCE.getAppPrefs().getInt("SearchRadius", 100);
    if(metric.equals("kms")) {
      mapObjectiveQuery.whereWithinKilometers("geoTag", currentLocation, searchRadius);
    } else {
      mapObjectiveQuery.whereWithinMiles("geoTag", currentLocation, searchRadius);
    }
    mapObjectiveQuery.findInBackground(new FindCallback<ParseObject>() {
      @Override
      public void done(List<ParseObject> objects, ParseException e) {
        if(e == null) {
          LogEvent.Log(TAG, "Total map objectives: " + objects.size());
          for(int i = 0; i < objects.size(); i++) {
            ParseObject mapObjective = objects.get(i);
            LogEvent.Log(TAG, "name: " + mapObjective.getString("name"));
            int pin = 0;
            if(mapObjective.getInt("category") == 1) { // Pharmacy
              pin = R.drawable.pin_pharmacy;
            } else if(mapObjective.getInt("category") == 2) { // Hospital
              pin = R.drawable.pin_hospital;
            } else { // Police
              pin = R.drawable.pin_police;
            }
            Marker m = mMap.addMarker(new MarkerOptions().position(
              new LatLng(mapObjective.getParseGeoPoint("geoTag").getLatitude(),
                mapObjective.getParseGeoPoint("geoTag").getLongitude())).title(
              mapObjective.getString("name")).icon(BitmapDescriptorFactory.fromResource(pin)));
            m.setTag(mapObjective);
            //m.showInfoWindow();
          }
        } else {
          LogEvent.Log(TAG, "Map Objective not found with exception: " + e.getMessage());
        }
      }
    });
  }

  private void togglePatrolMode() {
    // Toggle Patrol mode
    if(ParseUser.getCurrentUser().get("PatrolMode") != null) {
      patrolMode = (int) ParseUser.getCurrentUser().get("PatrolMode");
    }
    if(patrolMode == 0) {
      patrolMode = 1;
      fabPatrolMode.setBackgroundTintList(ColorStateList.valueOf(COLOR_ACCENT));
      fabPatrolMode.setImageResource(R.drawable.fab_patrol_mode_enabled);
      ParseUser.getCurrentUser().put("PatrolMode", 1);
      ParseUser.getCurrentUser().saveInBackground(new SaveCallback() {
        @Override
        public void done(ParseException e) {
          if(e == null) {
            // Turn on location updates and disable the Location Update slider button
            // cbLocationUpdate.setEnabled(false);
            // Turn on location updates
            SharedPreferences.Editor editor = prefs.edit();
            editor.putBoolean("LocationUpdate", true);
            editor.commit();
            //locationFragment.startLocationUpdates();
            if(MainActivity.weakRef.get() != null && !MainActivity.weakRef.get().isFinishing()) {
              getActivity().getApplicationContext();
              sendToast("Patrol mode enabled");
            }
          } else {
            patrolMode = 0;
            fabPatrolMode.setBackgroundTintList(ColorStateList.valueOf(COLOR_GRAY_CCC));
            fabPatrolMode.setImageResource(R.drawable.fab_patrol_mode_disabled);
            if(MainActivity.weakRef.get() != null && !MainActivity.weakRef.get().isFinishing()) {
              getActivity().getApplicationContext();
              sendToast("Patrol mode disabled");
            }
          }
        }
      });
    } else {
      patrolMode = 0;
      fabPatrolMode.setBackgroundTintList(ColorStateList.valueOf(COLOR_GRAY_CCC));
      fabPatrolMode.setImageResource(R.drawable.fab_patrol_mode_disabled);
      ParseUser.getCurrentUser().put("PatrolMode", 0);
      ParseUser.getCurrentUser().saveInBackground(new SaveCallback() {
        @Override
        public void done(ParseException e) {
          if(e == null) {
            // Turn off location updates and enable the Location Update button
            // cbLocationUpdate.setEnabled(true);
            if(MainActivity.weakRef.get() != null && !MainActivity.weakRef.get().isFinishing()) {
              getActivity().getApplicationContext();
              sendToast("Patrol mode disabled");
            }
          } else {
            patrolMode = 1;
            fabPatrolMode.setBackgroundTintList(ColorStateList.valueOf(COLOR_ACCENT));
            fabPatrolMode.setImageResource(R.drawable.fab_patrol_mode_enabled);
            // Turn on location updates
            SharedPreferences.Editor editor = prefs.edit();
            editor.putBoolean("LocationUpdate", true);
            editor.commit();
            //locationFragment.startLocationUpdates();
            if(MainActivity.weakRef.get() != null && !MainActivity.weakRef.get().isFinishing()) {
              getActivity().getApplicationContext();
              sendToast("Patrol mode enabled");
            }
          }
        }
      });
    }
  }

  private void toggleLocationCenter() {
    if(marker != null) {
      if(mLastLocation == null) {
        marker.setPosition(
          new LatLng(Singleton.INSTANCE.getLatitude(), Singleton.INSTANCE.getLongitude()));
      } else {
        marker.setPosition(new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude()));
      }
    }
    if(isLocationCenterEnabled) {
      isLocationCenterEnabled = false;
      fabLocationCenter.setBackgroundTintList(ColorStateList.valueOf(COLOR_GRAY_CCC));
      fabLocationCenter.setImageResource(R.drawable.fab_loc_center);
      imgLocationCenter.setBackgroundResource(R.drawable.bg_location_center_disabled);
      imgLocationCenter.setImageResource(R.drawable.fab_loc_center);
      sendToast(R.string.location_center_disabled);
    } else {
      isLocationCenterEnabled = true;
      fabLocationCenter.setBackgroundTintList(ColorStateList.valueOf(COLOR_PRIMARY));
      fabLocationCenter.setImageResource(R.drawable.fab_loc_center);
      imgLocationCenter.setBackgroundResource(R.drawable.bg_location_center_enabled);
      imgLocationCenter.setImageResource(R.drawable.fab_loc_center);
      sendToast(R.string.location_center_enabled);
      setUserPinToCenter();
    }
  }

  private AlertDialog showLocationCenterDialog() {
    AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
    alert.setTitle(R.string.dialog_title_location_alert);
    if(isLocationCenterEnabled) {
      alert.setMessage("Map will update when your current location changes");
    } else {
      alert.setMessage("Map will not update when your current location changes");
    }
    alert.setPositiveButton(R.string.dialog_btn_ok, new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int which) {
      }
    });
    AlertDialog dialog = alert.create();
    return dialog;
  }

  private void checkIfThereArePendingRequests() {
  }

  private void animateRadialMenu() {
    setUserPinToCenter();
    setRadialOuterAnimator.start();
    setRadialInner4Animator.start();
  }

  private void showPanicSliceAlertDialog() {
    AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
    alert.setMessage(R.string.dialog_message_did_you_purchase_panic_button);
    LayoutInflater inflater =
      (LayoutInflater) getActivity().getSystemService(Service.LAYOUT_INFLATER_SERVICE);
    View view = inflater.inflate(R.layout.dialog_panic_alert_popup, null);
    final CheckBox cbDontShowAgain = (CheckBox) view.findViewById(R.id.cb_dont_show_again);
    final TextView txtTriggerPanicAlert =
      (TextView) view.findViewById(R.id.txt_btn_trigger_panic_alert);
    final TextView txtManageBluetoothDevice =
      (TextView) view.findViewById(R.id.txt_btn_manage_bluetooth_device);
    final TextView txtAdvanceSettings = (TextView) view.findViewById(R.id.txt_btn_advance_settings);
    final TextView txtClose = (TextView) view.findViewById(R.id.txt_btn_close);
    final SharedPreferences.Editor editor = prefs.edit();
    cbDontShowAgain.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
      @Override
      public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        editor.putBoolean("HidePanicAlertPopup", isChecked).commit();
      }
    });
    alert.setView(view);
    final AlertDialog dialog = alert.create();
    dialog.show();
    txtTriggerPanicAlert.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        Intent intentIssuePanicAlert = new Intent(getActivity(), IssuePanicAlertActivity.class);
        intentIssuePanicAlert.putExtra(IssuePanicAlertActivity.EXTRAS_ALERT_TYPE, "Panic");
        startActivity(intentIssuePanicAlert);
        //DialogModules4IssuingAlert.sendPanicAlert(getActivity(), "PANIC");
      }
    });
    txtAdvanceSettings.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        Intent intentAdvanceSettings = new Intent(getActivity(), AdvanceSettingsActivity.class);
        startActivity(intentAdvanceSettings);
      }
    });
    txtClose.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        dialog.dismiss();
      }
    });
  }

  private void streamVideo() {
    boolean streamVideoToFBPage = prefs.getBoolean("StreamVideoToFacebookPage", false);
    boolean streamVideoToMyFBWall = prefs.getBoolean("StreamVideoToMyFacebookWall", false);
    boolean streamVideoToYTChannel = prefs.getBoolean("StreamVideoToYouTubeChannel", false);
    boolean streamVideoToMyYTChannel = prefs.getBoolean("StreamVideoToMyYouTubeChannel", false);
    if(streamVideoToFBPage || streamVideoToMyFBWall || streamVideoToYTChannel ||
         streamVideoToMyYTChannel) {
      if(Singleton.INSTANCE.getFriends() != null && Singleton.INSTANCE.getFriends().size() == 0) {
        // Initiate Video Recording
        DialogModules4IssuingAlert.streamVideoWithoutFriends(getActivity());
      } else if(Singleton.INSTANCE.getFriends() == null) {
        ParseUser user = ParseUser.getCurrentUser();
        ParseRelation relation = user.getRelation("friends");
        ParseQuery query = relation.getQuery();
        MainActivity.setSpinnerVisible(true);
        query.findInBackground(new FindCallback<ParseObject>() {
          @Override
          public void done(List list, ParseException e) {
            if(e == null) {
              ArrayList<User> friendList = new ArrayList<>();
              if(list != null) {
                for(int i = 0; i < list.size(); i++) {
                  ParseUser user = (ParseUser) list.get(i);
                  String email = null;
                  if(user.getEmail() != null && !user.getEmail().isEmpty()) {
                    email = user.getEmail();
                  } else {
                    email = user.getUsername();
                  }
                  friendList.add(
                    new User(email, (String) user.get("firstName"), (String) user.get("lastName"),
                      user));
                }
              }
              Singleton.INSTANCE.setFriends((ArrayList<User>) friendList.clone());
              if(friendList.size() == 0) {
                // Initiate Video Recording
                DialogModules4IssuingAlert.streamVideoWithoutFriends(getActivity());
              } else {
                Intent intentAlertIssuing = new Intent(getActivity(), AlertIssuingActivity.class);
                intentAlertIssuing.putExtra("alertType", Alert.VIDEO);
                startActivity(intentAlertIssuing);
              }
            } else {
              MainActivity.setSpinnerVisible(false);
              sendToast(e);
            }
          }
        });
      } else {
        Intent intentAlertIssuing = new Intent(getActivity(), AlertIssuingActivity.class);
        intentAlertIssuing.putExtra("alertType", Alert.VIDEO);
        startActivity(intentAlertIssuing);
      }
    } else {
      Intent intentAlertIssuing = new Intent(getActivity(), AlertIssuingActivity.class);
      intentAlertIssuing.putExtra("alertType", Alert.VIDEO);
      startActivity(intentAlertIssuing);
    }
  }

  private void setMarkerAtCurrentLocation() {
    if(getActivity() == null) {
      return;
    }
    // Retrieve reverse geo coded address
    String title = ParseUser.getCurrentUser().get("firstName") + ", " +
                     getString(R.string.tap_here_to_send_an_alert);
    if(Modules.isWeakReferenceValid()) {
      title = ParseUser.getCurrentUser().get("firstName") + ", " +
                getString(R.string.tap_here_to_send_an_alert);
    } else {
      title = ParseUser.getCurrentUser().getString("firstName");
    }
    // Add a marker at current location and move the camera
    LatLng latlng = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());
    if(marker == null) {
      marker = mMap.addMarker(new MarkerOptions().position(latlng).title(title));
    } else {
      marker.setPosition(new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude()));
    }
    mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latlng, 16f));
    //mMap.animateCamera(CameraUpdateFactory.zoomTo(16f), 500, null);
    Bitmap bmp = BitmapFactory.decodeResource(getResources(), R.drawable.logo);
    bmp = Bitmap.createScaledBitmap(bmp, GRAVATAR_SIZE, GRAVATAR_SIZE, false);
    marker.setIcon(BitmapDescriptorFactory.fromBitmap(bmp));
    marker.setInfoWindowAnchor(0.5f, 0.01f);
    marker.setAnchor(0.5f, 0.5f);
    marker.showInfoWindow();
    mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
      @Override
      public boolean onMarkerClick(Marker marker) {
        if(marker.equals(TabMapFragment.this.marker)) {
          animateRadialMenu();
          rlRadialMenu.setVisibility(View.VISIBLE);
        } else if(marker.getTag() instanceof OSMObjective) {
          rlRadialMenu.setVisibility(View.GONE);
          OSMObjective osmObjective = (OSMObjective) marker.getTag();
          Intent intentOSMObjective = new Intent(getActivity(), OSMObjectiveActivity.class);
          intentOSMObjective.putExtra("osmObjective", osmObjective);
          startActivity(intentOSMObjective);
          return false;
        } else {
          rlRadialMenu.setVisibility(View.GONE);
          ParseObject mapObjective = (ParseObject) marker.getTag();
          Intent intentMapObjective = new Intent(getActivity(), MapObjectiveActivity.class);
          intentMapObjective.putExtra("categoryId", mapObjective.getInt("category"));
          intentMapObjective.putExtra("name", mapObjective.getString("name"));
          intentMapObjective.putExtra("url", mapObjective.getString("url"));
          intentMapObjective.putExtra("description", mapObjective.getString("description"));
          intentMapObjective.putExtra("fullAddress", mapObjective.getString("fullAddress"));
          intentMapObjective.putExtra("hours", mapObjective.getString("hours"));
          intentMapObjective.putExtra("phone", mapObjective.getString("phone"));
          if(mapObjective.getParseFile("image") != null) {
            intentMapObjective.putExtra("imageUrl", mapObjective.getParseFile("image").getUrl());
          }
          startActivity(intentMapObjective);
          return false;
        }
        return true;
      }
    });
    mMap.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
      @Override
      public void onCameraIdle() {
        LogEvent.Log(TAG, "onCameraIdle() invoked");
      }
    });
    mMap.setOnCameraMoveListener(new GoogleMap.OnCameraMoveListener() {
      @Override
      public void onCameraMove() {
        rlRadialMenu.setVisibility(View.GONE);
        marker.showInfoWindow();
      }
    });
    mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
      @Override
      public void onMapClick(LatLng latLng) {
        rlRadialMenu.setVisibility(View.GONE);
        marker.showInfoWindow();
      }
    });
    Singleton.INSTANCE.setImageOnMap(marker);
  }

  private void setMarkerAtOldLocation() {
    if(getActivity() == null || !isAdded()) {
      return;
    }
    String title = ParseUser.getCurrentUser().get("firstName") + ", " +
                     getString(R.string.tap_here_to_send_an_alert);
    // Add a marker at current location and move the camera
    if(Singleton.INSTANCE.getLatitude() == 0) {
      Singleton.INSTANCE.setLatitude(38.8935128);
      Singleton.INSTANCE.setLongitude(-77.1546602);
    }
    LatLng latlng = new LatLng(Singleton.INSTANCE.getLatitude(), Singleton.INSTANCE.getLongitude());
    if(marker == null) {
      marker = mMap.addMarker(new MarkerOptions().position(latlng).title(title));
    }
    mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latlng, 16f));
    Bitmap bmp = BitmapFactory.decodeResource(getResources(), R.drawable.logo);
    bmp = Bitmap.createScaledBitmap(bmp, GRAVATAR_SIZE, GRAVATAR_SIZE, false);
    marker.setIcon(BitmapDescriptorFactory.fromBitmap(bmp));
    marker.setInfoWindowAnchor(0.5f, 0.01f);
    marker.setAnchor(0.5f, 0.5f);
    marker.showInfoWindow();
    mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
      @Override
      public boolean onMarkerClick(Marker marker) {
        if(marker.equals(TabMapFragment.this.marker)) {
          animateRadialMenu();
          rlRadialMenu.setVisibility(View.VISIBLE);
        } else if(marker.getTag() instanceof OSMObjective) {
          rlRadialMenu.setVisibility(View.GONE);
          OSMObjective osmObjective = (OSMObjective) marker.getTag();
          Intent intentOSMObjective = new Intent(getActivity(), OSMObjectiveActivity.class);
          intentOSMObjective.putExtra("osmObjective", osmObjective);
          startActivity(intentOSMObjective);
          return false;
        } else {
          rlRadialMenu.setVisibility(View.GONE);
          ParseObject mapObjective = (ParseObject) marker.getTag();
          Intent intentMapObjective = new Intent(getActivity(), MapObjectiveActivity.class);
          intentMapObjective.putExtra("categoryId", mapObjective.getInt("category"));
          intentMapObjective.putExtra("name", mapObjective.getString("name"));
          intentMapObjective.putExtra("url", mapObjective.getString("url"));
          intentMapObjective.putExtra("description", mapObjective.getString("description"));
          intentMapObjective.putExtra("fullAddress", mapObjective.getString("fullAddress"));
          intentMapObjective.putExtra("hours", mapObjective.getString("hours"));
          intentMapObjective.putExtra("phone", mapObjective.getString("phone"));
          if(mapObjective.getParseFile("image") != null) {
            intentMapObjective.putExtra("imageUrl", mapObjective.getParseFile("image").getUrl());
          }
          startActivity(intentMapObjective);
          return false;
        }
        return true;
      }
    });
    mMap.setOnCameraMoveListener(new GoogleMap.OnCameraMoveListener() {
      @Override
      public void onCameraMove() {
        rlRadialMenu.setVisibility(View.GONE);
        marker.showInfoWindow();
      }
    });
    mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
      @Override
      public void onMapClick(LatLng latLng) {
        rlRadialMenu.setVisibility(View.GONE);
        marker.showInfoWindow();
      }
    });
    Singleton.INSTANCE.setImageOnMap(marker);
  }

  private void setUserPinToCenter() {
    if(mMap != null) {
      if(mLastLocation != null) {
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
          new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude()), 16f));
      } else {
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
          new LatLng(Singleton.INSTANCE.getLatitude(), Singleton.INSTANCE.getLongitude()), 16f));
      }
    }
  }

  public enum ViewType {
    STANDARD, SATELLITE, HYBRID
  }

  private static class ApiCallOverpass extends AsyncTask<String, Void, Boolean> {
    private final String OVERPASS_BASE_URL = "https://www.overpass-api.de/api/interpreter";
    private final String PARAM_DATA = "?data=";
    private final String STATEMENT_OUT = "[out:json];";
    private final String STATEMENT_NODE_AMENITY_PHARMACY = "node[amenity=pharmacy]";
    private final String STATEMENT_NODE_AMENITY_HOSPITAL = "node[amenity=hospital]";
    private final String STATEMENT_NODE_AMENITY_POLICE = "node[amenity=police]";
    private final String BBOX =
      "(43.46669501043081,-5.708215989569187,43.588927989569186,-5.605835010430813);";
    private final String BBOX2 = "(28.53,77.26,28.66,77.47);";
    private final String STATEMENT_OUT_META = "out meta;";
    private final String BBOX3 =
      "(node[amenity=hospital](28.53,77.26,28.66,77.47);node[amenity=pharmacy](28.53,77.26,28.66,77.47););";
    private final String BBOX4 = "()";
    private GoogleMap mMap;
    private Activity context;
    private String URL;
    private String VALUE;

    public ApiCallOverpass(final Activity context, GoogleMap mMap, LatLngBounds bounds) {
      this.mMap = mMap;
      this.context = context;
      LatLng northeast = bounds.northeast;
      LatLng southwest = bounds.southwest;
      DecimalFormat df = new DecimalFormat("#.##");
      df.setRoundingMode(RoundingMode.CEILING);
      //String bbox = "(28.53,77.26,28.66,77.47);";
      //String bbox = "(" + southwest.latitude + "," + southwest.longitude + "," + northeast.latitude + "," + northeast.longitude + ");";
      String bbox = "(around:10000," + Singleton.INSTANCE.getLatitude() + "," +
                      Singleton.INSTANCE.getLongitude() + ");";
      VALUE = STATEMENT_OUT
                //+ "(" + df.format(southwest.longitude) + "," + df.format(southwest.latitude) + "," + df.format(northeast.longitude) + "," + df.format(northeast.latitude) + ");"
                //+ "(" + southwest.latitude + "," + southwest.longitude + "," + northeast.latitude + "," + northeast.longitude + ");"
                + "(" + STATEMENT_NODE_AMENITY_PHARMACY + bbox + STATEMENT_NODE_AMENITY_HOSPITAL +
                bbox +
                STATEMENT_NODE_AMENITY_POLICE + bbox + ");" + STATEMENT_OUT_META;
      try {
        URL = OVERPASS_BASE_URL + PARAM_DATA + URLEncoder.encode(VALUE, "UTF-8");
      } catch(UnsupportedEncodingException e) {
        e.printStackTrace();
      }
    }

    @Override
    protected Boolean doInBackground(String... params) {
      okhttp3.Request request = new okhttp3.Request.Builder().url(URL).addHeader("cache-control",
        "no-cache").get().build();
      try {
        okhttp3.OkHttpClient client = new okhttp3.OkHttpClient();
        okhttp3.Response response = client.newCall(request).execute();
        if(response != null) {
          LogEvent.Log(TAG, "response code: " + String.valueOf(response.code()));
          if(response.code() == HttpsURLConnection.HTTP_OK) {
            String responseJSON = response.body().string();
            JSONObject objResult = new JSONObject(responseJSON);
            JSONArray resultsArray = objResult.getJSONArray("elements");
            for(int i = 0; i < resultsArray.length(); i++) {
              JSONObject obj = resultsArray.getJSONObject(i);
              int id = obj.getInt("id");
              String type = obj.getString("type");
              final double lat = obj.getDouble("lat");
              final double lng = obj.getDouble("lon");
              JSONObject objTags = obj.getJSONObject("tags");
              String name = null;
              String opening_hours = null;
              String amenity = null;
              String tags = "";
              String website = null;
              Iterator<String> keys = objTags.keys();
              while(keys.hasNext()) {
                String key = keys.next();
                if(key.equals("name")) {
                  name = objTags.getString(key);
                } else if(key.equals("opening_hours")) {
                  opening_hours = objTags.getString(key);
                } else if(key.equals("amenity")) {
                  amenity = objTags.getString(key);
                } else if(key.equals("website")) {
                  website = objTags.getString(key);
                } else {
                  tags += key + ": " + objTags.get(key) + "\n";
                }
              }
              final OSMObjective osmObjective =
                new OSMObjective(name, amenity, opening_hours, website, lat, lng, tags);
              final String finalAmenity = amenity;
              final String finalName = name;
              context.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                  int pin = 0;
                  if(finalAmenity.equals("pharmacy")) { // Pharmacy
                    pin = R.drawable.pin_osm_pharmacy;
                  } else if(finalAmenity.equals("hospital")) { // Hospital
                    pin = R.drawable.pin_osm_hospital;
                  } else { // Police
                    pin = R.drawable.pin_osm_police;
                  }
                  Marker m = mMap.addMarker(
                    new MarkerOptions().position(new LatLng(lat, lng)).title(finalName).icon(
                      BitmapDescriptorFactory.fromResource(pin)));
                  m.setTag(osmObjective);
                }
              });
            }
            return true;
          } else {
            LogEvent.Log(TAG, "Error");
            return false;
          }
        }
        return false;
      } catch(IOException e) {
        LogEvent.Log(TAG, "IOException: " + e.getMessage());
        return null;
      } catch(JSONException e) {
        e.printStackTrace();
        return null;
      }
    }
  }

  private static class ApiCall extends AsyncTask<String, Void, Boolean> {
    private Context context;

    public ApiCall(final Context context) {
      this.context = context;
    }

    @Override
    protected Boolean doInBackground(String... params) {
      try {
        String urlString = params[0] + "&key=" + context.getString(R.string.google_api_key);
        LogEvent.Log(TAG, "urlString: " + urlString);
        URL url = new URL(urlString);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setReadTimeout(context.getResources().getInteger(R.integer.maximum_timeout_to_server));
        conn.setConnectTimeout(
          context.getResources().getInteger(R.integer.maximum_timeout_to_server));
        conn.setRequestMethod("POST");
        conn.setDoInput(true);
        conn.setDoOutput(true);
        int responseCode = conn.getResponseCode();
        LogEvent.Log(TAG, "responseCode : " + responseCode);
        String response = "";
        if(responseCode == HttpsURLConnection.HTTP_OK) {
          LogEvent.Log(TAG, "HTTP_OK");
          String line;
          BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
          while((line = br.readLine()) != null) {
            response += line;
          }
        } else {
          LogEvent.Log(TAG, "HTTP_NOT_OK");
          response = "";
        }
        JSONObject objResult = new JSONObject(response);
        JSONArray resultsArray = objResult.getJSONArray("results");
        for(int i = 0; i < resultsArray.length(); i++) {
          String placeId = resultsArray.getJSONObject(i).getString("place_id");
          String name = resultsArray.getJSONObject(i).getString("name");
          String vicinity = resultsArray.getJSONObject(i).getString("vicinity");
          JSONObject objLocation =
            resultsArray.getJSONObject(i).getJSONObject("geometry").getJSONObject("location");
          double lat = objLocation.getDouble("lat");
          double lng = objLocation.getDouble("lng");
          LogEvent.Log(TAG, "name: " + name);
          LogEvent.Log(TAG, "vicinity: " + vicinity);
          LogEvent.Log(TAG, "lat: " + lat);
          LogEvent.Log(TAG, "lng: " + lng);
        }
        return true;
      } catch(Exception e) {
        return false;
      }
    }
  }

  class AddressResultReceiver extends ResultReceiver {
    public AddressResultReceiver(Handler handler) {
      super(handler);
    }

    @Override
    protected void onReceiveResult(int resultCode, Bundle resultData) {
      LogEvent.Log(TAG, "onReceiveResult() invoked");
      // Display the city
      if(resultCode == Constants.SUCCESS_RESULT) {
        String city = resultData.getString(Constants.RESULT_DATA_CITY);
        if(Singleton.INSTANCE != null) {
          SharedPreferences.Editor editor = Singleton.INSTANCE.getAppPrefs().edit();
          editor.putString("city", city);
          editor.commit();
          if(ParseUser.getCurrentUser() != null) {
            ParseUser.getCurrentUser().put("city", city);
            ParseUser.getCurrentUser().saveInBackground();
          }
        }
      } else {
        LogEvent.Log(TAG, "onReceiveResult() invoked");
      }
    }
  }
}
