package cell411.methods;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;

import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseRelation;
import com.parse.ParseUser;
import com.parse.callback.CountCallback;
import com.parse.callback.GetCallback;
import com.parse.callback.SaveCallback;
import com.safearx.cell411.R;

import java.util.ArrayList;
import java.util.List;

import cell411.Singleton;
import cell411.activity.GalleryActivity;
import cell411.utils.LogEvent;

/**
 * Created by Sachin on 01-06-2016.
 */
public class PrivilegeModules {
  private static final String TAG = "PrivilegeModules";

  public static void checkAndUpdatePrivilege(final boolean isAlertCheckRequired) {
    final String privilege = (String) ParseUser.getCurrentUser().get("privilege");
    if(privilege == null || privilege.equals("FIRST") || privilege.equals("")) {
      // check if user has added at least two friends and has issued an alert to private cell
      // if the above condition is met then he/she can issue alerts globally
      ParseUser user = ParseUser.getCurrentUser();
      ParseRelation relFriends = user.getRelation("friends");
      ParseQuery query4Friends = relFriends.getQuery();
      query4Friends.countInBackground(new CountCallback() {
        @Override
        public void done(int count, ParseException e) {
          if(e == null) {
            // The count request succeeded. Log the count
            LogEvent.Log(TAG, "Total Friends: " + count);
            if(count >= 2) {
              if(!isAlertCheckRequired) {
                ParseUser parseUser = ParseUser.getCurrentUser();
                parseUser.put("privilege", "SECOND");
                parseUser.saveInBackground();
                return;
              }
              ParseQuery<ParseObject> parseQuery4SelfCell411Alerts =
                ParseQuery.getQuery("Cell411Alert");
              parseQuery4SelfCell411Alerts.whereEqualTo("issuedBy", ParseUser.getCurrentUser());
              parseQuery4SelfCell411Alerts.whereDoesNotExist("cellName");
              parseQuery4SelfCell411Alerts.whereNotEqualTo("isGlobal", 1);
              parseQuery4SelfCell411Alerts.whereDoesNotExist("entryFor");
              parseQuery4SelfCell411Alerts.whereDoesNotExist("to");
              parseQuery4SelfCell411Alerts.whereDoesNotExist("cellId");
              parseQuery4SelfCell411Alerts.whereExists("alertType");
              parseQuery4SelfCell411Alerts.whereExists("targetMembers");
              ParseQuery<ParseObject> parseQuery4ForwardedCell411Alerts =
                ParseQuery.getQuery("Cell411Alert");
              parseQuery4ForwardedCell411Alerts.whereEqualTo("forwardedBy",
                ParseUser.getCurrentUser());
              parseQuery4ForwardedCell411Alerts.whereExists("forwardedToMembers");
              parseQuery4ForwardedCell411Alerts.whereExists("forwardedAlert");
              parseQuery4ForwardedCell411Alerts.whereNotEqualTo("isGlobal", 1);
              parseQuery4ForwardedCell411Alerts.whereDoesNotExist("cellId");
              parseQuery4ForwardedCell411Alerts.whereDoesNotExist("cellName");
              List<ParseQuery<ParseObject>> queries = new ArrayList<>();
              queries.add(parseQuery4SelfCell411Alerts);
              queries.add(parseQuery4ForwardedCell411Alerts);
              ParseQuery<ParseObject> mainQuery = ParseQuery.or(queries);
              mainQuery.countInBackground(new CountCallback() {
                @Override
                public void done(int count, ParseException e) {
                  if(e == null) {
                    // The count request succeeded. Log the count
                    LogEvent.Log(TAG, "Total Alerts: " + count);
                    if(count >= 1) {
                      ParseUser parseUser = ParseUser.getCurrentUser();
                      parseUser.put("privilege", "SECOND");
                      parseUser.saveInBackground();
                    } else if(privilege == null) {
                      ParseUser parseUser = ParseUser.getCurrentUser();
                      parseUser.put("privilege", "FIRST");
                      parseUser.saveInBackground();
                      return;
                    }
                  } else {
                    // The request failed
                    return;
                  }
                }
              });
            } else if(privilege == null) {
              ParseUser parseUser = ParseUser.getCurrentUser();
              parseUser.put("privilege", "FIRST");
              parseUser.saveInBackground();
              return;
            }
          } else {
            // The request failed
            return;
          }
        }
      });
    }
  }

  public static void checkPrivilege() {
    ParseUser.getCurrentUser().fetchInBackground(new GetCallback<ParseObject>() {
      @Override
      public void done(ParseObject parseObject, ParseException e) {
        if(e == null) {
          final String privilege = (String) ParseUser.getCurrentUser().get("privilege");
          //if (privilege != null)
          LogEvent.Log(TAG, "privilege: " + privilege);
          //else
          //LogEvent.Log(TAG, "privilege is null");
          if(privilege == null || privilege.equals("FIRST")) {
            // check if user has added at least two friends and has issued an alert to private cell
            // if the above condition is met then he/she can issue alerts globally
            ParseUser user = ParseUser.getCurrentUser();
            ParseRelation relFriends = user.getRelation("friends");
            ParseQuery query4Friends = relFriends.getQuery();
            query4Friends.countInBackground(new CountCallback() {
              @Override
              public void done(int count, ParseException e) {
                if(e == null) {
                  // The count request succeeded. Log the count
                  LogEvent.Log(TAG, "Total Friends Check: " + count);
                  if(count >= 2) {
                    ParseQuery<ParseObject> parseQuery4SelfCell411Alerts =
                      ParseQuery.getQuery("Cell411Alert");
                    parseQuery4SelfCell411Alerts.whereEqualTo("issuedBy",
                      ParseUser.getCurrentUser());
                    parseQuery4SelfCell411Alerts.whereDoesNotExist("cellName");
                    parseQuery4SelfCell411Alerts.whereNotEqualTo("isGlobal", 1);
                    parseQuery4SelfCell411Alerts.whereDoesNotExist("entryFor");
                    parseQuery4SelfCell411Alerts.whereDoesNotExist("to");
                    parseQuery4SelfCell411Alerts.whereDoesNotExist("cellId");
                    parseQuery4SelfCell411Alerts.whereExists("alertType");
                    parseQuery4SelfCell411Alerts.whereExists("targetMembers");
                    ParseQuery<ParseObject> parseQuery4ForwardedCell411Alerts =
                      ParseQuery.getQuery("Cell411Alert");
                    parseQuery4ForwardedCell411Alerts.whereEqualTo("forwardedBy",
                      ParseUser.getCurrentUser());
                    parseQuery4ForwardedCell411Alerts.whereExists("forwardedToMembers");
                    parseQuery4ForwardedCell411Alerts.whereExists("forwardedAlert");
                    parseQuery4ForwardedCell411Alerts.whereNotEqualTo("isGlobal", 1);
                    parseQuery4ForwardedCell411Alerts.whereDoesNotExist("cellId");
                    parseQuery4ForwardedCell411Alerts.whereDoesNotExist("cellName");
                    List<ParseQuery<ParseObject>> queries = new ArrayList<>();
                    queries.add(parseQuery4SelfCell411Alerts);
                    queries.add(parseQuery4ForwardedCell411Alerts);
                    ParseQuery<ParseObject> mainQuery = ParseQuery.or(queries);
                    mainQuery.countInBackground(new CountCallback() {
                      @Override
                      public void done(int count, ParseException e) {
                        if(e == null) {
                          // The count request succeeded. Log the count
                          LogEvent.Log(TAG, "Total Alerts Check: " + count);
                          if(count >= 1) {
                            ParseUser parseUser = ParseUser.getCurrentUser();
                            parseUser.put("privilege", "SECOND");
                            parseUser.saveInBackground();
                          } else if(privilege == null) {
                            ParseUser parseUser = ParseUser.getCurrentUser();
                            parseUser.put("privilege", "FIRST");
                            parseUser.saveInBackground();
                            return;
                          }
                        } else {
                          // The request failed
                          return;
                        }
                      }
                    });
                  } else if(privilege == null) {
                    ParseUser parseUser = ParseUser.getCurrentUser();
                    parseUser.put("privilege", "FIRST");
                    parseUser.saveInBackground();
                    return;
                  }
                } else {
                  // The request failed
                  return;
                }
              }
            });
          } else if(privilege.equals("BANNED")) {
            // user is permanently banned
            logoutUser();
          } else if(privilege.contains("SUSPENDED")) {
            // blocked user temporarily
            logoutUser();
          }
        } else {
          LogEvent.Log(TAG, e.toString());
          if(e.getCode() == ParseException.INVALID_SESSION_TOKEN) {
            LogEvent.Log(TAG, "INVALID_SESSION_TOKEN");
            if(Modules.isWeakReferenceValid()) {
              Dialogs.showSessionExpiredAlertDialog();
            }
          } else if(e.getCode() == ParseException.OBJECT_NOT_FOUND) {
            LogEvent.Log(TAG, "OBJECT_NOT_FOUND");
            if(Modules.isWeakReferenceValid()) {
              Dialogs.showSessionExpiredAlertDialog();
            }
          } else if(e.getCode() == ParseException.SESSION_MISSING) {
            LogEvent.Log(TAG, "SESSION_MISSING");
            if(Modules.isWeakReferenceValid()) {
              Dialogs.showSessionExpiredAlertDialog();
            }
          } else {
            // The exception might have came up due to network error
          }
        }
      }
    });
  }

  public static void showUnfulfilledPrivilegeAlert(final Activity context) {
    AlertDialog.Builder alert = new AlertDialog.Builder(context);
    alert.setMessage(context.getString(R.string.dialog_msg_unfulfilled_privilege));
    alert.setPositiveButton(R.string.dialog_btn_ok, new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int which) {
        dialog.dismiss();
      }
    });
    alert.create().show();
  }

  public static void logoutUser() {
    LogEvent.Log(TAG, "logging out");
    // First de-associate the current user from the Installation object so to prevent notifications when logged out
    ParseInstallation installation = ParseInstallation.getCurrentInstallation();
    installation.remove("user");
    installation.saveInBackground(new SaveCallback() {
      @Override
      public void done(ParseException e) {
        if(e == null) {
          //ParseUser.logOut();
          Singleton.INSTANCE.clearData();
          Singleton.INSTANCE.getAppPrefs().edit().putBoolean("isLoggedIn", false).commit();
          Intent intent =
            new Intent(Singleton.INSTANCE.getCurrentActivity(), GalleryActivity.class);
          intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
          Singleton.INSTANCE.getCurrentActivity().startActivity(intent);
        } else {
          if(e.getCode() == ParseException.INVALID_SESSION_TOKEN) {
            LogEvent.Log(TAG, "INVALID_SESSION_TOKEN");
            Modules.logoutUserHavingInvalidSession();
          } else if(e.getCode() == ParseException.OBJECT_NOT_FOUND) {
            LogEvent.Log(TAG, "OBJECT_NOT_FOUND");
            Modules.logoutUserHavingInvalidSession();
          } else {
            if(Modules.isWeakReferenceValid()) {
              Singleton.INSTANCE.getCurrentActivity().getApplicationContext();
              Singleton.sendToast(e);
            }
          }
        }
      }
    });
  }
}
