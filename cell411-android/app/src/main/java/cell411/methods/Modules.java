package cell411.methods;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.safearx.cell411.R;

import java.lang.ref.WeakReference;

import cell411.Singleton;
import cell411.activity.AboutActivity;
import cell411.activity.AdvanceSettingsActivity;
import cell411.activity.AlertActivity;
import cell411.activity.AlertDetailActivity2;
import cell411.activity.AlertIssuingActivity;
import cell411.activity.BroadcastMessageActivity;
import cell411.activity.ChangePasswordActivity;
import cell411.activity.ChatActivity;
import cell411.activity.CreateOrEditPublicCellActivity;
import cell411.activity.CustomScannerActivity;
import cell411.activity.ExploreCellActivity;
import cell411.activity.GalleryActivity;
import cell411.activity.ImageScreenActivity;
import cell411.activity.IssuePanicAlertActivity;
import cell411.activity.KnowYourRightsActivity;
import cell411.activity.MainActivity;
import cell411.activity.MapObjectiveActivity;
import cell411.activity.NewChatActivity;
import cell411.activity.PickCustomLocationActivity;
import cell411.activity.ProfileEditActivity;
import cell411.activity.ProfileImageActivity;
import cell411.activity.ProfileViewActivity;
import cell411.activity.PublicCellMembersActivity;
import cell411.activity.ReviewsActivity;
import cell411.activity.SelectContactsActivity;
import cell411.activity.SettingsActivity;
import cell411.activity.SpammedUsersActivity;
import cell411.activity.UserActivity;
import cell411.activity.UserConsentActivity;
import cell411.activity.VideoSettingsActivity;
import cell411.constants.Alert;
import cell411.models.Cell411Alert;
import cell411.utils.LogEvent;

/**
 * Created by Sachin on 31-05-2016.
 */
public class Modules {
  private static final String TAG = "Modules";
  public static boolean enabled = false;

  public static boolean isWeakReferenceValid() {
    if(Singleton.INSTANCE == null) {
      LogEvent.Log(TAG, "Singleton.INSTANCE is null");
      return false;
    } else if(Singleton.INSTANCE.getCurrentActivity() == null) {
      LogEvent.Log(TAG, "Singleton.INSTANCE.getCurrentActivity() is null");
      return false;
    }
    if(!enabled) {
      return true;
    }
    if(Singleton.INSTANCE.getCurrentActivity() instanceof MainActivity) {
      LogEvent.Log(TAG, "Weak Reference of MainActivity");
      if(MainActivity.weakRef.get() != null && !MainActivity.weakRef.get().isFinishing()) {
        LogEvent.Log(TAG, "Valid Reference");
        return true;
      }
    } else if(Singleton.INSTANCE.getCurrentActivity() instanceof SettingsActivity) {
      if(SettingsActivity.weakRef.get() != null && !SettingsActivity.weakRef.get().isFinishing()) {
        LogEvent.Log(TAG, "Valid Reference");
        return true;
      }
    } else if(Singleton.INSTANCE.getCurrentActivity() instanceof VideoSettingsActivity) {
      if(VideoSettingsActivity.weakRef.get() != null &&
           !VideoSettingsActivity.weakRef.get().isFinishing()) {
        LogEvent.Log(TAG, "Valid Reference");
        return true;
      }
    } else if(Singleton.INSTANCE.getCurrentActivity() instanceof KnowYourRightsActivity) {
      if(KnowYourRightsActivity.weakRef.get() != null &&
           !KnowYourRightsActivity.weakRef.get().isFinishing()) {
        LogEvent.Log(TAG, "Valid Reference");
        return true;
      }
    } else if(Singleton.INSTANCE.getCurrentActivity() instanceof ChangePasswordActivity) {
      if(ChangePasswordActivity.weakRef.get() != null &&
           !ChangePasswordActivity.weakRef.get().isFinishing()) {
        LogEvent.Log(TAG, "Valid Reference");
        return true;
      }
    } else if(Singleton.INSTANCE.getCurrentActivity() instanceof ProfileViewActivity) {
      if(ProfileViewActivity.weakRef.get() != null &&
           !ProfileViewActivity.weakRef.get().isFinishing()) {
        LogEvent.Log(TAG, "Valid Reference");
        return true;
      }
    } else if(Singleton.INSTANCE.getCurrentActivity() instanceof ProfileEditActivity) {
      if(ProfileEditActivity.weakRef.get() != null &&
           !ProfileEditActivity.weakRef.get().isFinishing()) {
        LogEvent.Log(TAG, "Valid Reference");
        return true;
      }
    } else if(Singleton.INSTANCE
                .getCurrentActivity() instanceof CreateOrEditPublicCellActivity) {
      if(CreateOrEditPublicCellActivity.weakRef.get() != null &&
           !CreateOrEditPublicCellActivity.weakRef.get().isFinishing()) {
        LogEvent.Log(TAG, "Valid Reference");
        return true;
      }
    } else if(Singleton.INSTANCE.getCurrentActivity() instanceof AlertDetailActivity2) {
      if(AlertDetailActivity2.weakRef.get() != null &&
           !AlertDetailActivity2.weakRef.get().isFinishing()) {
        LogEvent.Log(TAG, "Valid Reference");
        return true;
      }
    } else if(Singleton.INSTANCE.getCurrentActivity() instanceof ImageScreenActivity) {
      if(ImageScreenActivity.weakRef.get() != null &&
           !ImageScreenActivity.weakRef.get().isFinishing()) {
        LogEvent.Log(TAG, "Valid Reference");
        return true;
      }
    } else if(Singleton.INSTANCE.getCurrentActivity() instanceof AlertActivity) {
      if(AlertActivity.weakRef.get() != null && !AlertActivity.weakRef.get().isFinishing()) {
        LogEvent.Log(TAG, "Valid Reference");
        return true;
      }
    } else if(Singleton.INSTANCE.getCurrentActivity() instanceof UserActivity) {
      if(UserActivity.weakRef.get() != null && !UserActivity.weakRef.get().isFinishing()) {
        LogEvent.Log(TAG, "Valid Reference");
        return true;
      }
    } else if(Singleton.INSTANCE.getCurrentActivity() instanceof AdvanceSettingsActivity) {
      if(AdvanceSettingsActivity.weakRef.get() != null &&
           !AdvanceSettingsActivity.weakRef.get().isFinishing()) {
        LogEvent.Log(TAG, "Valid Reference");
        return true;
      }
    } else if(Singleton.INSTANCE.getCurrentActivity() instanceof IssuePanicAlertActivity) {
      if(IssuePanicAlertActivity.weakRef.get() != null &&
           !IssuePanicAlertActivity.weakRef.get().isFinishing()) {
        LogEvent.Log(TAG, "Valid Reference");
        return true;
      }
    } else if(Singleton.INSTANCE.getCurrentActivity() instanceof PickCustomLocationActivity) {
      if(PickCustomLocationActivity.weakRef.get() != null &&
           !PickCustomLocationActivity.weakRef.get().isFinishing()) {
        LogEvent.Log(TAG, "Valid Reference");
        return true;
      }
    } else if(Singleton.INSTANCE.getCurrentActivity() instanceof CustomScannerActivity) {
      if(CustomScannerActivity.weakRef.get() != null &&
           !CustomScannerActivity.weakRef.get().isFinishing()) {
        LogEvent.Log(TAG, "Valid Reference");
        return true;
      }
    } else if(Singleton.INSTANCE.getCurrentActivity() instanceof PublicCellMembersActivity) {
      if(PublicCellMembersActivity.weakRef.get() != null &&
           !PublicCellMembersActivity.weakRef.get().isFinishing()) {
        LogEvent.Log(TAG, "Valid Reference");
        return true;
      }
    } else if(Singleton.INSTANCE.getCurrentActivity() instanceof SpammedUsersActivity) {
      if(SpammedUsersActivity.weakRef.get() != null &&
           !SpammedUsersActivity.weakRef.get().isFinishing()) {
        LogEvent.Log(TAG, "Valid Reference");
        return true;
      }
    } else if(Singleton.INSTANCE.getCurrentActivity() instanceof ProfileImageActivity) {
      if(ProfileImageActivity.weakRef.get() != null &&
           !ProfileImageActivity.weakRef.get().isFinishing()) {
        LogEvent.Log(TAG, "Valid Reference");
        return true;
      }
    } else if(Singleton.INSTANCE.getCurrentActivity() instanceof ReviewsActivity) {
      if(ReviewsActivity.weakRef.get() != null && !ReviewsActivity.weakRef.get().isFinishing()) {
        LogEvent.Log(TAG, "Valid Reference");
        return true;
      }
    } else if(Singleton.INSTANCE.getCurrentActivity() instanceof ChatActivity) {
      if(ChatActivity.weakRef.get() != null && !ChatActivity.weakRef.get().isFinishing()) {
        LogEvent.Log(TAG, "Valid Reference");
        return true;
      }
    } else if(Singleton.INSTANCE.getCurrentActivity() instanceof NewChatActivity) {
      if(NewChatActivity.weakRef.get() != null && !NewChatActivity.weakRef.get().isFinishing()) {
        LogEvent.Log(TAG, "Valid Reference");
        return true;
      }
    } else if(Singleton.INSTANCE.getCurrentActivity() instanceof BroadcastMessageActivity) {
      if(BroadcastMessageActivity.weakRef.get() != null &&
           !BroadcastMessageActivity.weakRef.get().isFinishing()) {
        LogEvent.Log(TAG, "Valid Reference");
        return true;
      }
//    } else if(Singleton.INSTANCE.getCurrentActivity() instanceof UploadContactsActivity) {
//      if(UploadContactsActivity.weakRef.get() != null &&
//           !UploadContactsActivity.weakRef.get().isFinishing()) {
//        LogEvent.Log(TAG, "Valid Reference");
//        return true;
//      }
    } else if(Singleton.INSTANCE.getCurrentActivity() instanceof ExploreCellActivity) {
      if(ExploreCellActivity.weakRef.get() != null &&
           !ExploreCellActivity.weakRef.get().isFinishing()) {
        LogEvent.Log(TAG, "Valid Reference");
        return true;
      }
    } else if(Singleton.INSTANCE.getCurrentActivity() instanceof SelectContactsActivity) {
      if(SelectContactsActivity.weakRef.get() != null &&
           !SelectContactsActivity.weakRef.get().isFinishing()) {
        LogEvent.Log(TAG, "Valid Reference");
        return true;
      }
    } else if(Singleton.INSTANCE.getCurrentActivity() instanceof AlertIssuingActivity) {
      if(AlertIssuingActivity.weakRef.get() != null &&
           !AlertIssuingActivity.weakRef.get().isFinishing()) {
        LogEvent.Log(TAG, "Valid Reference");
        return true;
      }
    } else if(Singleton.INSTANCE.getCurrentActivity() instanceof UserConsentActivity) {
      if(UserConsentActivity.weakRef.get() != null &&
           !UserConsentActivity.weakRef.get().isFinishing()) {
        LogEvent.Log(TAG, "Valid Reference");
        return true;
      }
    } else if(Singleton.INSTANCE.getCurrentActivity() instanceof AboutActivity) {
      if(AboutActivity.weakRef.get() != null && !AboutActivity.weakRef.get().isFinishing()) {
        LogEvent.Log(TAG, "Valid Reference");
        return true;
      }
    } else if(Singleton.INSTANCE.getCurrentActivity() instanceof MapObjectiveActivity) {
      if(MapObjectiveActivity.weakRef.get() != null &&
           !MapObjectiveActivity.weakRef.get().isFinishing()) {
        LogEvent.Log(TAG, "Valid Reference");
        return true;
      }
    }
    LogEvent.Log(TAG, "Invalid Reference");
    return false;
  }

  public static void check4NewAlertsAndPrivilege(final Activity context) {
    Modules4NewAlerts.checkParseForNewAlerts(context);
    PrivilegeModules.checkPrivilege();
  }

  public static Cell411Alert.AlertType convertStringToEnum(String alertType) {
    if(alertType.equals(Alert.BROKEN_CAR)) {
      return Cell411Alert.AlertType.BROKEN_CAR;
    } else if(alertType.equals(Alert.BULLIED)) {
      return Cell411Alert.AlertType.BULLIED;
    } else if(alertType.equals(Alert.CRIMINAL)) {
      return Cell411Alert.AlertType.CRIMINAL;
    } else if(alertType.equals(Alert.DANGER)) {
      return Cell411Alert.AlertType.DANGER;
    } else if(alertType.equals(Alert.FIRE)) {
      return Cell411Alert.AlertType.FIRE;
    } else if(alertType.equals(Alert.GENERAL)) {
      return Cell411Alert.AlertType.GENERAL;
    } else if(alertType.equals(Alert.MEDICAL)) {
      return Cell411Alert.AlertType.MEDICAL;
    } else if(alertType.equals(Alert.PHOTO)) {
      return Cell411Alert.AlertType.PHOTO;
    } else if(alertType.equals(Alert.POLICE_ARREST)) {
      return Cell411Alert.AlertType.POLICE_ARREST;
    } else if(alertType.equals(Alert.POLICE_INTERACTION)) {
      return Cell411Alert.AlertType.POLICE_INTERACTION;
    } else if(alertType.equals(Alert.PULLED_OVER)) {
      return Cell411Alert.AlertType.PULLED_OVER;
    } else if(alertType.equals(Alert.VIDEO)) {
      return Cell411Alert.AlertType.VIDEO;
    } else if(alertType.equals(Alert.HIJACK)) {
      return Cell411Alert.AlertType.HIJACK;
    } else if(alertType.equals(Alert.PANIC)) {
      return Cell411Alert.AlertType.PANIC;
    } else if(alertType.equals(Alert.FALLEN)) {
      return Cell411Alert.AlertType.FALLEN;
    } else if(alertType.equals(Alert.PHYSICAL_ABUSE)) {
      return Cell411Alert.AlertType.PHYSICAL_ABUSE;
    } else if(alertType.equals(Alert.TRAPPED)) {
      return Cell411Alert.AlertType.TRAPPED;
    } else if(alertType.equals(Alert.CAR_ACCIDENT)) {
      return Cell411Alert.AlertType.CAR_ACCIDENT;
    } else if(alertType.equals(Alert.NATURAL_DISASTER)) {
      return Cell411Alert.AlertType.NATURAL_DISASTER;
    } else if(alertType.equals(Alert.PRE_AUTHORISATION)) {
      return Cell411Alert.AlertType.PRE_AUTHORISATION;
    } else {
      return Cell411Alert.AlertType.UN_RECOGNIZED;
    }
  }

  public static String convertEnumToString(Context context, Cell411Alert.AlertType alertType) {
    switch(alertType) {
      case BROKEN_CAR:
        return context.getString(R.string.alert_broken_car);
      case BULLIED:
        return context.getString(R.string.alert_bullied);
      case CRIMINAL:
        return context.getString(R.string.alert_criminal);
      case DANGER:
        return context.getString(R.string.alert_danger);
      case FIRE:
        return context.getString(R.string.alert_fire);
      case GENERAL:
        return context.getString(R.string.alert_general);
      case MEDICAL:
        return context.getString(R.string.alert_medical);
      case PHOTO:
        return context.getString(R.string.alert_photo);
      case POLICE_ARREST:
        return context.getString(R.string.alert_police_arrest);
      case POLICE_INTERACTION:
        return context.getString(R.string.alert_police_interaction);
      case PULLED_OVER:
        return context.getString(R.string.alert_pulled_over);
      case VIDEO:
        return context.getString(R.string.alert_video);
      case HIJACK:
        return context.getString(R.string.alert_hijack);
      case PANIC:
        return context.getString(R.string.alert_panic);
      case FALLEN:
        return context.getString(R.string.alert_fallen);
      case PHYSICAL_ABUSE:
        return context.getString(R.string.alert_physical_abuse);
      case TRAPPED:
        return context.getString(R.string.alert_trapped);
      case CAR_ACCIDENT:
        return context.getString(R.string.alert_car_accident);
      case NATURAL_DISASTER:
        return context.getString(R.string.alert_natural_disaster);
      case PRE_AUTHORISATION:
        return context.getString(R.string.alert_pre_authorisation);
      default:
        return context.getString(R.string.alert_un_recognized);
    }
  }

  /**
   * This method is no more used since the new UI 2.0 was released
   *
   * @param message
   * @param name
   * @param alertRegarding
   * @param dispatchMode
   * @return
   */
  public static String getMessage4Alert(String message, String name, String alertRegarding,
                                        int dispatchMode) {
    if(dispatchMode == 1) {
      if(alertRegarding.equalsIgnoreCase("Vehicle Pulled")) {
        return message + name + " dispatched an alert for being pulled over by police";
      } else if(alertRegarding.equalsIgnoreCase("Arrested")) {
        return message + name + " dispatched an alert for observing someone being arrested";
      } else if(alertRegarding.equalsIgnoreCase("Medical")) {
        return message + name + " dispatched an alert for medical attention";
      } else if(alertRegarding.equalsIgnoreCase("Vehicle Broken")) {
        return message + name + " dispatched an alert for broken down car";
      } else if(alertRegarding.equalsIgnoreCase("Crime")) {
        return message + name + " dispatched an alert for observing criminal activity";
      } else if(alertRegarding.equalsIgnoreCase("Fire")) {
        return message + name + " dispatched an alert for observing a fire";
      } else if(alertRegarding.equalsIgnoreCase("Danger")) {
        return message + name + " dispatched an alert for being in danger";
      } else if(alertRegarding.equalsIgnoreCase("Bullied")) {
        return message + name + " dispatched an alert for being bullied";
      } else if(alertRegarding.equalsIgnoreCase("General")) {
        return message + name + " dispatched this alert";
      } else if(alertRegarding.equalsIgnoreCase("Hijack")) {
        return message + name + " dispatched an alert for being hijacked";
      } else if(alertRegarding.equalsIgnoreCase("Panic")) {
        return message + name + " dispatched an alert in panic";
      } else if(alertRegarding.equalsIgnoreCase("Fallen")) {
        return message + name + " dispatched an alert after falling";
      } else {
        return message + name + " dispatched an alert for cop blocking";
      }
    } else {
      if(alertRegarding.equalsIgnoreCase("Vehicle Pulled")) {
        return message + name + " is being pulled over by police";
      } else if(alertRegarding.equalsIgnoreCase("Arrested")) {
        return message + name + " is observing someone being arrested";
      } else if(alertRegarding.equalsIgnoreCase("Medical")) {
        return message + name + " needs medical attention";
      } else if(alertRegarding.equalsIgnoreCase("Vehicle Broken")) {
        return message + name + "'s car broken down";
      } else if(alertRegarding.equalsIgnoreCase("Crime")) {
        return message + name + " is observing criminal activity";
      } else if(alertRegarding.equalsIgnoreCase("Fire")) {
        return message + name + " is observing a fire";
      } else if(alertRegarding.equalsIgnoreCase("Danger")) {
        return message + name + " is in danger";
      } else if(alertRegarding.equalsIgnoreCase("Bullied")) {
        return message + name + " is being bullied";
      } else if(alertRegarding.equalsIgnoreCase("General")) {
        return message + name + " issued this alert";
      } else if(alertRegarding.equalsIgnoreCase("Hijack")) {
        return message + name + " is being hijacked";
      } else if(alertRegarding.equalsIgnoreCase("Panic")) {
        return message + name + " is in panic";
      } else if(alertRegarding.equalsIgnoreCase("Fallen")) {
        return message + name + " has fallen";
      } else {
        return message + name + " is cop blocking";
      }
    }
  }

  public static void checkErrorCode(ParseException e, WeakReference<Activity> weakRef,
                                    Activity activity) {
    if(e.getCode() == ParseException.INVALID_SESSION_TOKEN) {
      LogEvent.Log(TAG, "INVALID_SESSION_TOKEN");
    } else if(e.getCode() == ParseException.OBJECT_NOT_FOUND) {
      LogEvent.Log(TAG, "OBJECT_NOT_FOUND");
    } else {
      if(weakRef.get() != null && !weakRef.get().isFinishing()) {
        activity.getApplicationContext();
        Singleton.sendToast(e);
      }
    }
  }

  public static void logoutUserHavingInvalidSession() {
    LogEvent.Log(TAG, "showSessionExpiredAlertDialog() invoked");
    Singleton.INSTANCE.clearData();
    Singleton.INSTANCE.getAppPrefs().edit().putBoolean("isLoggedIn", false).apply();
    // De-associate the current user from the Installation object so to prevent notifications when logged out
    ParseInstallation installation = ParseInstallation.getCurrentInstallation();
    installation.remove("user");
    installation.saveEventually();
    gotoLogin();
  }

  public static void gotoLogin() {
    LogEvent.Log(TAG, "gotoLogin() invoked");
    Intent intent = new Intent(Singleton.INSTANCE.getCurrentActivity(), GalleryActivity.class);
    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
    Singleton.INSTANCE.getCurrentActivity().startActivity(intent);
  }
}
