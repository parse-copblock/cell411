package cell411.methods;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Service;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParsePush;
import com.parse.ParseQuery;
import com.parse.ParseRelation;
import com.parse.ParseUser;
import com.parse.callback.FindCallback;
import com.parse.callback.FunctionCallback;
import com.parse.callback.SaveCallback;
import com.safearx.cell411.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import cell411.Singleton;
import cell411.models.FriendRequest;
import cell411.models.User;
import cell411.utils.LogEvent;

/**
 * Created by Sachin on 01-06-2016.
 */
public class AddFriendModules {
  private static final String TAG = "AddFriendModules";
  private static ArrayList<User> friendList;

  public static void addFriend(final Activity context, final String email,
                               final OnSendFriendRequestListener onSendFriendRequestListener) {
    if(email == null || email.isEmpty()) {
      context.getApplicationContext();
      Singleton.sendToast(R.string.please_enter_email);
      onSendFriendRequestListener.onRequestFailed();
    } else if(email.equals(ParseUser.getCurrentUser().getUsername()) ||
                (ParseUser.getCurrentUser().getEmail() != null &&
                   email.equals(ParseUser.getCurrentUser().getEmail()))) {
      context.getApplicationContext();
      Singleton.sendToast(R.string.you_cannot_add_yourself_as_friend);
      onSendFriendRequestListener.onRequestFailed();
    } else {
      if(Singleton.INSTANCE.getFriends() != null) {
        friendList = (ArrayList<User>) Singleton.INSTANCE.getFriends().clone();
      }
      if(friendList != null) {
        if(idFriendAlreadyAdded(email)) {
          context.getApplicationContext();
          Singleton.sendToast(R.string.this_friend_already_added);
          onSendFriendRequestListener.onRequestFailed();
          return;
        } else {
          // send invite then
          sendInviteIfNotSpammed(context, email, onSendFriendRequestListener);
        }
      } else {
        ParseUser user = ParseUser.getCurrentUser();
        ParseRelation relation = user.getRelation("friends");
        ParseQuery query = relation.getQuery();
        query.findInBackground(new FindCallback<ParseObject>() {
          @Override
          public void done(List list, ParseException e) {
            if(e == null) {
              friendList = new ArrayList<>();
              if(list != null) {
                for(int i = 0; i < list.size(); i++) {
                  ParseUser user = (ParseUser) list.get(i);
                  String email = null;
                  if(user.getEmail() != null && !user.getEmail().isEmpty()) {
                    email = (String) user.getEmail();
                  } else {
                    email = user.getUsername();
                  }
                  friendList.add(
                    new User(email, (String) user.get("firstName"), (String) user.get("lastName"),
                      user));
                }
              }
              Singleton.INSTANCE.setFriends((ArrayList<User>) friendList.clone());
              if(idFriendAlreadyAdded(email)) {
                if(Modules.isWeakReferenceValid()) {
                  context.getApplicationContext();
                  Singleton.sendToast(R.string.this_friend_already_added);
                  onSendFriendRequestListener.onRequestFailed();
                }
                return;
              } else {
                // send invite then
                sendInviteIfNotSpammed(context, email, onSendFriendRequestListener);
              }
            } else {
              if(Modules.isWeakReferenceValid()) {
                context.getApplicationContext();
                Singleton.sendToast(e);
                onSendFriendRequestListener.onRequestFailed();
              }
            }
          }
        });
      }
    }
  }

  public static void addFriend(final Activity context, final String email) {
    if(email == null || email.isEmpty()) {
      context.getApplicationContext();
      Singleton.sendToast(R.string.please_enter_email);
    } else if(email.equals(ParseUser.getCurrentUser().getUsername()) ||
                (ParseUser.getCurrentUser().getEmail() != null &&
                   email.equals(ParseUser.getCurrentUser().getEmail()))) {
      context.getApplicationContext();
      Singleton.sendToast(R.string.you_cannot_add_yourself_as_friend);
    } else {
      if(Singleton.INSTANCE.getFriends() != null) {
        friendList = (ArrayList<User>) Singleton.INSTANCE.getFriends().clone();
      }
      if(friendList != null) {
        if(idFriendAlreadyAdded(email)) {
          context.getApplicationContext();
          Singleton.sendToast(R.string.this_friend_already_added);
          return;
        } else {
          // send invite then
          sendInviteIfNotSpammed(context, email);
        }
      } else {
        ParseUser user = ParseUser.getCurrentUser();
        ParseRelation relation = user.getRelation("friends");
        ParseQuery query = relation.getQuery();
        query.findInBackground(new FindCallback<ParseObject>() {
          @Override
          public void done(List list, ParseException e) {
            if(e == null) {
              friendList = new ArrayList<>();
              if(list != null) {
                for(int i = 0; i < list.size(); i++) {
                  ParseUser user = (ParseUser) list.get(i);
                  String email = null;
                  if(user.getEmail() != null && !user.getEmail().isEmpty()) {
                    email = (String) user.getEmail();
                  } else {
                    email = user.getUsername();
                  }
                  friendList.add(
                    new User(email, (String) user.get("firstName"), (String) user.get("lastName"),
                      user));
                }
              }
              Singleton.INSTANCE.setFriends((ArrayList<User>) friendList.clone());
              if(idFriendAlreadyAdded(email)) {
                if(Modules.isWeakReferenceValid()) {
                  context.getApplicationContext();
                  Singleton.sendToast(R.string.this_friend_already_added);
                }
                return;
              } else {
                // send invite then
                sendInviteIfNotSpammed(context, email);
              }
            } else {
              if(Modules.isWeakReferenceValid()) {
                context.getApplicationContext();
                Singleton.sendToast(e);
              }
            }
          }
        });
      }
    }
  }

  private static boolean idFriendAlreadyAdded(String email) {
    for(int i = 0; i < friendList.size(); i++) {
      if(friendList.get(i).user != null) {
        if(friendList.get(i).user.getUsername().equals(email) ||
             (friendList.get(i).user.getEmail() != null &&
                friendList.get(i).user.getEmail().equals(email))) {
          return true;
        }
      }
    }
    return false;
  }

  private static void sendInviteIfNotSpammed(final Activity context, final String email,
                                             final OnSendFriendRequestListener onSendFriendRequestListener) {
    ParseQuery<ParseUser> query = ParseUser.getQuery();
    query.whereEqualTo("username", email);
    ParseQuery<ParseUser> query2 = ParseUser.getQuery();
    query2.whereEqualTo("email", email);
    // Club all the queries into one master query
    List<ParseQuery<ParseUser>> queries = new ArrayList<>();
    queries.add(query);
    queries.add(query2);
    ParseQuery<ParseUser> mainQuery = ParseQuery.or(queries);
    mainQuery.findInBackground(new FindCallback<ParseUser>() {
      public void done(List<ParseUser> usersList, ParseException e) {
        //pbListFriends.setVisibility(View.GONE);
        if(e == null) {
          // The query was successful.
          if(usersList.size() == 0) {
            if(Modules.isWeakReferenceValid()) {
              // User with the specified email does not exist, so an invite can be sent.
              showSendInviteDialog(Singleton.INSTANCE.getCurrentActivity(), email);
            }
            return;
          }
          // Send Friend Request here
          final ParseUser user = usersList.get(0);
          ParseRelation relation = ParseUser.getCurrentUser().getRelation("spammedBy");
          ParseQuery parseQuery = relation.getQuery();
          parseQuery.findInBackground(new FindCallback<ParseUser>() {
            @Override
            public void done(List<ParseUser> list, ParseException e) {
              if(e == null) {
                if(list != null) {
                  boolean userHasSpammedCurrentUser = false;
                  for(int i = 0; i < list.size(); i++) {
                    if(((ParseUser) list.get(i)).getObjectId().equals(user.getObjectId())) {
                      userHasSpammedCurrentUser = true;
                      break;
                    }
                  }
                  if(userHasSpammedCurrentUser) {
                    onSendFriendRequestListener.onRequestFailed();
                    if(Modules.isWeakReferenceValid()) {
                      Dialogs.showAlertDialog(Singleton.INSTANCE.getCurrentActivity(),
                        context.getString(R.string.dialog_message_cannot_send_friend_request));
                    }
                  } else {
                    // Before sending request, check if the user has not already sent request which is pending
                    ParseQuery<ParseObject> query = ParseQuery.getQuery("Cell411Alert");
                    query.whereEqualTo("issuedBy", ParseUser.getCurrentUser());
                    query.whereEqualTo("to", user.getUsername());
                    query.whereEqualTo("status", "PENDING");
                    query.whereEqualTo("entryFor", "FR");
                    final String name = ParseUser.getCurrentUser().get("firstName") + " " +
                                          ParseUser.getCurrentUser().get("lastName");
                    query.findInBackground(new FindCallback<ParseObject>() {
                      @Override
                      public void done(final List<ParseObject> parseObjects, ParseException e) {
                        if(e == null) {
                          if(parseObjects != null &&
                               parseObjects.size() > 0) { // if true, the request already exists
                            // An approval request has already been sent which is pending,
                            // so display an alert to the current user
                            //showAlertDialog("A friend invite was already sent to " + email + " for approval");
                            // Send notification for friend request here
                            ParseQuery pushQuery = ParseInstallation.getQuery();
                            pushQuery.whereEqualTo("user", user);
                            ParsePush push = new ParsePush();
                            push.setQuery(pushQuery);
                            JSONObject data = new JSONObject();
                            try {
                              data.put("alert",
                                context.getString(R.string.notif_friend_request, name,
                                  context.getString(R.string.app_name)));
                              data.put("userId", ParseUser.getCurrentUser().getObjectId());
                              data.put("sound", "default");
                              data.put("friendRequestObjectId", parseObjects.get(0).getObjectId());
                              data.put("name", name);
                              data.put("alertType", "FRIEND_REQUEST");
                              data.put("badge", "Increment");
                            } catch(JSONException e1) {
                              e1.printStackTrace();
                            }
                            push.setData(data);
                            push.sendInBackground();
                            onSendFriendRequestListener.onRequestSent();
                            if(Modules.isWeakReferenceValid()) {
                              Dialogs.showAlertDialog(Singleton.INSTANCE.getCurrentActivity(),
                                context.getString(R.string.dialog_message_invitation_sent,
                                  email));
                            }
                          } else {
                            final ParseObject friendRequestObject = new ParseObject("Cell411Alert");
                            friendRequestObject.put("issuedBy", ParseUser.getCurrentUser());
                            friendRequestObject.put("issuerFirstName", name);
                            friendRequestObject.put("to", user.getUsername());
                            friendRequestObject.put("status", "PENDING");
                            friendRequestObject.put("entryFor", "FR");
                            friendRequestObject.saveInBackground(new SaveCallback() {
                              @Override
                              public void done(ParseException e) {
                                if(e == null) {
                                  // Send notification for friend request here
                                  ParseQuery pushQuery = ParseInstallation.getQuery();
                                  pushQuery.whereEqualTo("user", user);
                                  ParsePush push = new ParsePush();
                                  push.setQuery(pushQuery);
                                  JSONObject data = new JSONObject();
                                  try {
                                    data.put("alert",
                                      context.getString(R.string.notif_friend_request, name,
                                        context.getString(R.string.app_name)));
                                    data.put("userId", ParseUser.getCurrentUser().getObjectId());
                                    data.put("sound", "default");
                                    data.put("friendRequestObjectId",
                                      friendRequestObject.getObjectId());
                                    data.put("name", name);
                                    data.put("alertType", "FRIEND_REQUEST");
                                    data.put("badge", "Increment");
                                  } catch(JSONException e1) {
                                    e1.printStackTrace();
                                  }
                                  push.setData(data);
                                  push.sendInBackground();
                                  onSendFriendRequestListener.onRequestSent();
                                  if(Modules.isWeakReferenceValid()) {
                                    Dialogs
                                      .showAlertDialog(Singleton.INSTANCE.getCurrentActivity(),
                                        context.getString(R.string.dialog_message_invitation_sent,
                                          email));
                                  }
                                } else {
                                  if(Modules.isWeakReferenceValid()) {
                                    context.getApplicationContext();
                                    Singleton.sendToast(e);
                                    onSendFriendRequestListener.onRequestFailed();
                                  }
                                }
                              }
                            });
                          }
                        } else {
                          if(Modules.isWeakReferenceValid()) {
                            context.getApplicationContext();
                            Singleton.sendToast(e);
                            onSendFriendRequestListener.onRequestFailed();
                          }
                        }
                      }
                    });
                  }
                } else {
                  // Seems like the list is empty
                  LogEvent.Log(TAG, "Spam users list is null");
                  onSendFriendRequestListener.onRequestFailed();
                }
              } else {
                if(Modules.isWeakReferenceValid()) {
                  context.getApplicationContext();
                  Singleton.sendToast(e);
                  onSendFriendRequestListener.onRequestFailed();
                }
              }
            }
          });
          // Before sending request, check if the user has not already sent request which is pending
          ParseQuery<ParseObject> query = ParseQuery.getQuery("Cell411Alert");
          query.whereEqualTo("issuedBy", ParseUser.getCurrentUser());
          query.whereEqualTo("to", user.getUsername());
          query.whereEqualTo("status", "PENDING");
          query.whereEqualTo("entryFor", "FR");
          query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(final List<ParseObject> parseObjects, ParseException e) {
            }
          });
        } else {
          if(Modules.isWeakReferenceValid()) {
            context.getApplicationContext();
            Singleton.sendToast(e);
            onSendFriendRequestListener.onRequestFailed();
          }
        }
      }
    });
  }

  private static void sendInviteIfNotSpammed(final Activity context, final String email) {
    ParseQuery<ParseUser> query = ParseUser.getQuery();
    query.whereEqualTo("username", email);
    ParseQuery<ParseUser> query2 = ParseUser.getQuery();
    query2.whereEqualTo("email", email);
    // Club all the queries into one master query
    List<ParseQuery<ParseUser>> queries = new ArrayList<>();
    queries.add(query);
    queries.add(query2);
    ParseQuery<ParseUser> mainQuery = ParseQuery.or(queries);
    mainQuery.findInBackground(new FindCallback<ParseUser>() {
      public void done(List<ParseUser> usersList, ParseException e) {
        //pbListFriends.setVisibility(View.GONE);
        if(e == null) {
          // The query was successful.
          if(usersList.size() == 0) {
            if(Modules.isWeakReferenceValid()) {
              // User with the specified email does not exist, so an invite can be sent.
              showSendInviteDialog(Singleton.INSTANCE.getCurrentActivity(), email);
            }
            return;
          }
          // Send Friend Request here
          final ParseUser user = usersList.get(0);
          // Before sending request, check if the user has not already sent request which is pending
          ParseQuery<ParseObject> query = ParseQuery.getQuery("Cell411Alert");
          query.whereEqualTo("issuedBy", ParseUser.getCurrentUser());
          query.whereEqualTo("to", user.getUsername());
          query.whereEqualTo("status", "PENDING");
          query.whereEqualTo("entryFor", "FR");
          query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> parseObjects, ParseException e) {
              if(e == null) {
                //if (parseObjects != null && parseObjects.size() > 0) {
                // An approval request has already been sent which is pending,
                // so display an alert to the current user
                //showAlertDialog("A friend invite was already sent to " + email + " for approval");
                //} else {
                ParseRelation relation = ParseUser.getCurrentUser().getRelation("spammedBy");
                ParseQuery parseQuery = relation.getQuery();
                parseQuery.findInBackground(new FindCallback<ParseObject>() {
                  @Override
                  public void done(List list, ParseException e) {
                    if(e == null) {
                      if(list != null) {
                        boolean userHasSpammedCurrentUser = false;
                        for(int i = 0; i < list.size(); i++) {
                          if(((ParseUser) list.get(i)).getObjectId().equals(user.getObjectId())) {
                            userHasSpammedCurrentUser = true;
                            break;
                          }
                        }
                        if(userHasSpammedCurrentUser) {
                          if(Modules.isWeakReferenceValid()) {
                            Dialogs.showAlertDialog(Singleton.INSTANCE.getCurrentActivity(),
                              context.getString(
                                R.string.dialog_message_cannot_send_friend_request));
                          }
                        } else {
                          final String name = ParseUser.getCurrentUser().get("firstName") + " " +
                                                ParseUser.getCurrentUser().get("lastName");
                          final ParseObject friendRequestObject = new ParseObject("Cell411Alert");
                          friendRequestObject.put("issuedBy", ParseUser.getCurrentUser());
                          friendRequestObject.put("issuerFirstName", name);
                          friendRequestObject.put("to", user.getUsername());
                          friendRequestObject.put("status", "PENDING");
                          friendRequestObject.put("entryFor", "FR");
                          friendRequestObject.saveInBackground(new SaveCallback() {
                            @Override
                            public void done(ParseException e) {
                              if(e == null) {
                                // Send notification for friend request here
                                ParseQuery pushQuery = ParseInstallation.getQuery();
                                pushQuery.whereEqualTo("user", user);
                                ParsePush push = new ParsePush();
                                push.setQuery(pushQuery);
                                JSONObject data = new JSONObject();
                                try {
                                  data.put("alert",
                                    context.getString(R.string.notif_friend_request, name,
                                      context.getString(R.string.app_name)));
                                  data.put("userId", ParseUser.getCurrentUser().getObjectId());
                                  data.put("sound", "default");
                                  data.put("friendRequestObjectId",
                                    friendRequestObject.getObjectId());
                                  data.put("name", name);
                                  data.put("alertType", "FRIEND_REQUEST");
                                  data.put("badge", "Increment");
                                } catch(JSONException e1) {
                                  e1.printStackTrace();
                                }
                                push.setData(data);
                                push.sendInBackground();
                                if(Modules.isWeakReferenceValid()) {
                                  Dialogs
                                    .showAlertDialog(Singleton.INSTANCE.getCurrentActivity(),
                                      context.getString(R.string.dialog_message_invitation_sent,
                                        email));
                                }
                              } else {
                                if(Modules.isWeakReferenceValid()) {
                                  context.getApplicationContext();
                                  Singleton.sendToast(e);
                                }
                              }
                            }
                          });
                        }
                      } else {
                        // Seems like the list is empty
                        LogEvent.Log("SettingsActivity", "Spam users list is null");
                      }
                    } else {
                      if(Modules.isWeakReferenceValid()) {
                        context.getApplicationContext();
                        Singleton.sendToast(e);
                      }
                    }
                  }
                });
                //}
              } else {
                if(Modules.isWeakReferenceValid()) {
                  context.getApplicationContext();
                  Singleton.sendToast(e);
                }
              }
            }
          });
        } else {
          if(Modules.isWeakReferenceValid()) {
            context.getApplicationContext();
            Singleton.sendToast(e);
          }
        }
      }
    });
  }

  private static void showSendInviteDialog(final Activity context, final String email) {
    AlertDialog.Builder alert = new AlertDialog.Builder(context);
    alert.setMessage(context.getString(R.string.dialog_msg_send_email_invite, email,
      context.getString(R.string.app_name)));
    String senderEmail = null;
    LayoutInflater inflater =
      (LayoutInflater) context.getSystemService(Service.LAYOUT_INFLATER_SERVICE);
    View view = inflater.inflate(R.layout.dialog_email, null);
    final ProgressBar pb = (ProgressBar) view.findViewById(R.id.pb);
    final EditText etEmail = (EditText) view.findViewById(R.id.et_email);
    if(ParseUser.getCurrentUser().getUsername().contains("@") ||
         (ParseUser.getCurrentUser().getEmail() != null &&
            !ParseUser.getCurrentUser().getEmail().toString().isEmpty())) {
      senderEmail = ParseUser.getCurrentUser().getUsername();
    } else if(ParseUser.getCurrentUser().getEmail() != null &&
                !ParseUser.getCurrentUser().getEmail().toString().isEmpty()) {
      senderEmail = ParseUser.getCurrentUser().getEmail();
    } else {
      // Sender email is required
      alert.setView(view);
    }
    alert.setNegativeButton(R.string.dialog_btn_cancel, new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialogInterface, int i) {
      }
    });
    alert.setPositiveButton(R.string.dialog_btn_ok, new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialogInterface, int i) {
      }
    });
    final AlertDialog dialog = alert.create();
    dialog.show();
    final String finalSenderEmail = senderEmail;
    dialog.getButton(android.app.AlertDialog.BUTTON_POSITIVE).setOnClickListener(
      new View.OnClickListener() {
        @Override
        public void onClick(View v) {
          if(finalSenderEmail != null) {
            sendInvite(context, email, finalSenderEmail);
            dialog.dismiss();
          } else if(pb.getVisibility() != View.VISIBLE) {
            final String senderEmail = etEmail.getText().toString().trim();
            if(senderEmail.isEmpty()) {
              context.getApplicationContext();
              Singleton.sendToast(R.string.validation_email);
              return;
            } else if(!email.contains("@")) {
              context.getApplicationContext();
              Singleton.sendToast(R.string.validation_email_invalid);
              return;
            }
            pb.setVisibility(View.VISIBLE);
            ParseQuery<ParseUser> userParseQuery = ParseUser.getQuery();
            userParseQuery.whereEqualTo("username", email);
            ParseQuery<ParseUser> userParseQuery2 = ParseUser.getQuery();
            userParseQuery2.whereEqualTo("email", email);
            // Club all the queries into one master query
            List<ParseQuery<ParseUser>> queries = new ArrayList<>();
            queries.add(userParseQuery);
            queries.add(userParseQuery2);
            ParseQuery<ParseUser> mainQuery = ParseQuery.or(queries);
            mainQuery.findInBackground(new FindCallback<ParseUser>() {
              @Override
              public void done(List<ParseUser> list, ParseException e) {
                if(e == null) {
                  if(list == null || list.size() == 0) {
                    ParseUser parseUserCurrent = ParseUser.getCurrentUser();
                    parseUserCurrent.setEmail(senderEmail.toLowerCase().trim());
                    parseUserCurrent.saveInBackground(new SaveCallback() {
                      @Override
                      public void done(ParseException e) {
                        pb.setVisibility(View.GONE);
                        if(Modules.isWeakReferenceValid()) {
                          if(e == null) {
                            context.getApplicationContext();
                            Singleton.sendToast(R.string.email_updated_successfully);
                            sendInvite(context, email, senderEmail);
                          } else {
                            context.getApplicationContext();
                            Singleton.sendToast(e);
                          }
                        }
                        dialog.dismiss();
                      }
                    });
                  } else {
                    pb.setVisibility(View.GONE);
                    // display popup
                    showEmailAlreadyRegisteredAlert(context, email);
                  }
                } else {
                  pb.setVisibility(View.GONE);
                  if(Modules.isWeakReferenceValid()) {
                    context.getApplicationContext();
                    Singleton.sendToast(e);
                  }
                }
              }
            });
          }
        }
      });
  }

  private static void showEmailAlreadyRegisteredAlert(final Activity context, String email) {
    AlertDialog.Builder alert = new AlertDialog.Builder(context);
    alert.setMessage(email + " " + context.getString(R.string.email_already_registered));
    alert.setPositiveButton(R.string.dialog_btn_ok, new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int which) {
        dialog.dismiss();
      }
    });
    alert.create().show();
  }

  private static void sendInvite(final Activity context, final String email,
                                 final String senderEmail) {
    final String name = ParseUser.getCurrentUser().get("firstName") + " " +
                          ParseUser.getCurrentUser().get("lastName");
    ParseQuery<ParseObject> query = ParseQuery.getQuery("Cell411Alert");
    query.whereEqualTo("issuedBy", ParseUser.getCurrentUser());
    query.whereEqualTo("to", email);
    query.whereEqualTo("status", "PENDING");
    query.whereEqualTo("entryFor", "FI");
    query.findInBackground(new FindCallback<ParseObject>() {
      @Override
      public void done(List<ParseObject> list, ParseException e) {
        if(list != null && list.size() > 0) {
          // An entry in Cell411Alert has already been created which is pending,
          // so only send an email to the user
          // send email invitation here
          HashMap<String, Object> params = new HashMap<>();
          params.put("email", email);
          params.put("name", name);
          params.put("senderEmail", senderEmail);
          params.put("clientFirmId", 1);
          params.put("isLive", context.getResources().getBoolean(R.bool.enable_publish));
          params.put("languageCode", context.getString(R.string.language_code));
          params.put("platform", "android");
          ParseCloud.callFunctionInBackground("sendInvite", params, new FunctionCallback<String>() {
            public void done(String result, ParseException e) {
              if(Modules.isWeakReferenceValid()) {
                if(e == null) {
                  // Email sent successfully
                  context.getApplicationContext();
                  Singleton.sendToast(R.string.invitation_sent_successfully);
                } else {
                  context.getApplicationContext();
                  Singleton.sendToast(e);
                }
              }
            }
          });
        } else {
          // make an entry in Cell411Alert table and send email invitation here
          final ParseObject inviteObject = new ParseObject("Cell411Alert");
          inviteObject.put("issuedBy", ParseUser.getCurrentUser());
          inviteObject.put("issuerFirstName", name);
          inviteObject.put("to", email);
          inviteObject.put("status", "PENDING");
          inviteObject.put("entryFor", "FI");
          inviteObject.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
              if(e == null) {
                // send email invitation here
                HashMap<String, Object> params = new HashMap<>();
                params.put("email", email);
                params.put("name", name);
                params.put("senderEmail", senderEmail);
                params.put("clientFirmId", 1);
                params.put("isLive", context.getResources().getBoolean(R.bool.enable_publish));
                params.put("languageCode", context.getString(R.string.language_code));
                params.put("platform", "android");
                ParseCloud.callFunctionInBackground("sendInvite", params,
                  new FunctionCallback<String>() {
                    public void done(String result, ParseException e) {
                      if(Modules.isWeakReferenceValid()) {
                        if(e == null) {
                          // Email sent successfully
                          context.getApplicationContext();
                          Singleton.sendToast(R.string.invitation_sent_successfully);
                        } else {
                          context.getApplicationContext();
                          Singleton.sendToast(e);
                        }
                      }
                    }
                  });
              } else {
                if(Modules.isWeakReferenceValid()) {
                  context.getApplicationContext();
                  Singleton.sendToast(e);
                }
              }
            }
          });
        }
      }
    });
  }

  public static void approveFriendRequest(final Activity context, final FriendRequest friendRequest,
                                          final OnApproveFriendRequestListener onApproveFriendRequestListener) {
    ParseQuery<ParseUser> userParseQuery = ParseUser.getQuery();
    userParseQuery.whereEqualTo("objectId", friendRequest.userId);
    userParseQuery.findInBackground(new FindCallback<ParseUser>() {
      @Override
      public void done(final List<ParseUser> parseUsers, ParseException e) {
        if(e == null) {
          if(parseUsers != null && parseUsers.size() > 0) {
            final ParseObject taskObject = new ParseObject("Task");
            taskObject.put("userId", friendRequest.userId);
            taskObject.put("assigneeUserId", ParseUser.getCurrentUser().getObjectId());
            taskObject.put("task", "FRIEND_ADD");
            taskObject.put("status", "PENDING");
            final ParseUser user = parseUsers.get(0);
            taskObject.saveInBackground(new SaveCallback() {
              @Override
              public void done(ParseException e) {
                if(e == null) {
                  // Make approved entry in the FriendInviteOrRequest table
                  ParseQuery<ParseObject> query = ParseQuery.getQuery("Cell411Alert");
                  query.whereEqualTo("objectId", friendRequest.friendRequestObjectId);
                  query.findInBackground(new FindCallback<ParseObject>() {
                    @Override
                    public void done(List<ParseObject> parseObjects, ParseException e) {
                      if(e == null) {
                        if(parseObjects != null && parseObjects.size() > 0) {
                          ParseObject friendRequestObject = parseObjects.get(0);
                          friendRequestObject.put("status", "APPROVED");
                          friendRequestObject.saveInBackground(new SaveCallback() {
                            @Override
                            public void done(ParseException e) {
                              if(e == null) {
                                // Send notification for friend approved here
                                ParseQuery pushQuery = ParseInstallation.getQuery();
                                pushQuery.whereEqualTo("user", user);
                                ParsePush push = new ParsePush();
                                push.setQuery(pushQuery);
                                String name = ParseUser.getCurrentUser().
                                                                          get("firstName") + " " +
                                                ParseUser.getCurrentUser().get("lastName");
                                JSONObject data = new JSONObject();
                                try {
                                  data.put("alert",
                                    context.getString(R.string.notif_friend_approved, name));
                                  data.put("userId", ParseUser.
                                                                getCurrentUser().getObjectId());
                                  data.put("sound", "");
                                  data.put("name", name);
                                  data.put("taskId", taskObject.getObjectId());
                                  data.put("alertType", "FRIEND_APPROVED");
                                  data.put("badge", "Increment");
                                } catch(JSONException e1) {
                                  e1.printStackTrace();
                                }
                                push.setData(data);
                                push.sendInBackground();
                                if(onApproveFriendRequestListener != null) {
                                  onApproveFriendRequestListener.onRequestApproved();
                                }
                                // Check if the current user already has this user in their friend list
                                ParseRelation relation =
                                  ParseUser.getCurrentUser().getRelation("friends");
                                ParseQuery<ParseUser> parseQuery = relation.getQuery();
                                parseQuery.whereEqualTo("objectId", user.getObjectId());
                                parseQuery.findInBackground(new FindCallback<ParseUser>() {
                                  @Override
                                  public void done(List<ParseUser> parseUsers, ParseException e) {
                                    if(e == null) {
                                      if(parseUsers != null && parseUsers.size() > 0) {
                                        boolean userAlreadyFriend = false;
                                        for(int i = 0; i < parseUsers.size(); i++) {
                                          if(parseUsers.get(i).getObjectId().equals(
                                            user.getObjectId())) {
                                            LogEvent.Log(TAG, "The user is already friend, " +
                                                                "so no need to display add back");
                                            userAlreadyFriend = true;
                                            break;
                                          }
                                        }
                                        if(!userAlreadyFriend && Modules.isWeakReferenceValid()) {
                                          // Display add back dialog
                                          DialogModules4ReceivedAlerts.showFriendAddBackDialog(
                                            Singleton.INSTANCE.getCurrentActivity(),
                                            friendRequest, user);
                                        }
                                      } else {
                                        if(Modules.isWeakReferenceValid()) {
                                          // Display add back dialog
                                          DialogModules4ReceivedAlerts.showFriendAddBackDialog(
                                            Singleton.INSTANCE.getCurrentActivity(),
                                            friendRequest, user);
                                        }
                                      }
                                    } else {
                                    }
                                  }
                                });
                              } else {
                                if(Modules.isWeakReferenceValid()) {
                                  if(onApproveFriendRequestListener != null) {
                                    onApproveFriendRequestListener.onRequestFailed();
                                  }
                                  context.getApplicationContext();
                                  Singleton.sendToast(e);
                                }
                              }
                            }
                          });
                        }
                      } else {
                        if(Modules.isWeakReferenceValid()) {
                          if(onApproveFriendRequestListener != null) {
                            onApproveFriendRequestListener.onRequestFailed();
                          }
                          context.getApplicationContext();
                          Singleton.sendToast(e);
                        }
                      }
                    }
                  });
                } else {
                  if(Modules.isWeakReferenceValid()) {
                    if(onApproveFriendRequestListener != null) {
                      onApproveFriendRequestListener.onRequestFailed();
                    }
                    context.getApplicationContext();
                    Singleton.sendToast(e);
                  }
                }
              }
            });
          } else {
            LogEvent.Log(TAG, "User not found");
            if(Modules.isWeakReferenceValid()) {
              if(onApproveFriendRequestListener != null) {
                onApproveFriendRequestListener.onRequestFailed();
              }
              context.getApplicationContext();
              Singleton.sendToast("User not found");
            }
          }
        } else {
          if(Modules.isWeakReferenceValid()) {
            if(onApproveFriendRequestListener != null) {
              onApproveFriendRequestListener.onRequestFailed();
            }
            context.getApplicationContext();
            Singleton.sendToast(e);
          }
        }
      }
    });
  }

  public static void rejectFriendRequest(final Activity context, final FriendRequest friendRequest,
                                         final OnRejectFriendRequestListener onRejectFriendRequestListener) {
    ParseQuery<ParseObject> query = ParseQuery.getQuery("Cell411Alert");
    query.whereEqualTo("objectId", friendRequest.friendRequestObjectId);
    query.findInBackground(new FindCallback<ParseObject>() {
      @Override
      public void done(List<ParseObject> parseObjects, ParseException e) {
        if(e == null) {
          if(parseObjects != null && parseObjects.size() > 0) {
            ParseObject friendRequestObject = parseObjects.get(0);
            friendRequestObject.put("status", "DENIED");
            friendRequestObject.saveInBackground();
            if(onRejectFriendRequestListener != null) {
              onRejectFriendRequestListener.onRequestRejected();
            }
          }
        } else {
          if(Modules.isWeakReferenceValid()) {
            if(onRejectFriendRequestListener != null) {
              onRejectFriendRequestListener.onRequestFailed();
            }
            context.getApplicationContext();
            Singleton.sendToast(e);
          }
        }
      }
    });
  }

  public interface OnSendFriendRequestListener {
    void onRequestSent();

    void onRequestFailed();
  }

  public interface OnApproveFriendRequestListener {
    void onRequestApproved();

    void onRequestFailed();
  }

  public interface OnRejectFriendRequestListener {
    void onRequestRejected();

    void onRequestFailed();
  }
}
