package cell411.methods;

import static cell411.Singleton.getAlertNotificationTrail;
import static cell411.Singleton.sendToast;
import static cell411.Singleton.setCell411AlertObject;
import static cell411.Singleton.setIsGlobal;
import static cell411.Singleton.setPush;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Service;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseGeoPoint;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParsePush;
import com.parse.ParseQuery;
import com.parse.ParseRelation;
import com.parse.ParseUser;
import com.parse.callback.CountCallback;
import com.parse.callback.FindCallback;
import com.parse.callback.FunctionCallback;
import com.parse.callback.GetCallback;
import com.parse.callback.SaveCallback;
import com.safearx.cell411.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

import cell411.Singleton;
import cell411.activity.MainActivity;
import cell411.activity.SelectContactsActivity;
import cell411.activity.VideoStreamingActivity;
import cell411.adapters.CellListAdapter;
import cell411.adapters.PublicCellListAdapter;
import cell411.constants.Alert;
import cell411.constants.Constants;
import cell411.constants.LMA;
import cell411.models.Cell;
import cell411.models.Cell411Alert;
import cell411.models.NewPrivateCell;
import cell411.models.PrivateCell;
import cell411.models.PublicCell;
import cell411.models.ReceivedCell411Alert;
import cell411.services.FetchAddressIntentService;
import cell411.utils.StorageOperations;
import cell411.utils.LogEvent;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class DialogModules4IssuingAlert {
  public static final String TAG = "DialogModules4IssuingAlert";
  private static ArrayList<NewPrivateCell> cellList;
  private static NewPrivateCell selectedCell;
  private static Cell selectedPublicCell;
  private static AlertDialog dialog;
  private static ArrayList<Cell> joinedPublicCells;

  public static void showSendAlertDialog(final Activity context, final String alertMessage,
                                         final String title, final boolean isAlertForwarding,
                                         final ReceivedCell411Alert receivedCell411Alert,
                                         final ParseUser parseUser) {
    AlertDialog.Builder alert = new AlertDialog.Builder(context);
    alert.setTitle(alertMessage);
    LayoutInflater inflater =
      (LayoutInflater) context.getSystemService(Service.LAYOUT_INFLATER_SERVICE);
    View view = inflater.inflate(R.layout.layout_cell, null);
    final ListView listView = view.findViewById(R.id.list_cells);
    alert.setView(view);
    alert.setNegativeButton(R.string.dialog_btn_cancel, new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int arg1) {
        dialog.dismiss();
      }
    });
    alert.setPositiveButton(R.string.dialog_btn_ok, new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int which) {
        if(isAlertForwarding) {
          forwardAlert(context, title, receivedCell411Alert, parseUser);
        } else if(selectedCell != null) {
          if(title.equals("Video")) {
            if(selectedCell.parseObject == null && selectedCell.name.equals("GLOBAL ALERT")) {
              streamVideoGlobally(context);
            } else {
              streamVideo(context, selectedCell);
            }
          } else if(selectedCell.parseObject == null && selectedCell.name.equals("GLOBAL ALERT")) {
            showGlobalAlertAddNoteDialog(context, title);
          } else {
            showAddNoteDialog(context, selectedCell, title, false);
          }
        }
      }
    });
    final AlertDialog dialog = alert.create();
    cellList = null;
    if(Singleton.INSTANCE.getNewPrivateCells() != null) {
      cellList = (ArrayList<NewPrivateCell>) Singleton.INSTANCE.getNewPrivateCells().clone();
    }
    if(cellList == null) {
      ParseQuery<ParseObject> cellQuery = ParseQuery.getQuery("Cell");
      cellQuery.whereEqualTo("createdBy", ParseUser.getCurrentUser());
      cellQuery.whereNotEqualTo("type", 5);
      cellQuery.include("members");
      cellQuery.include("nauMembers");
      cellQuery.findInBackground(new FindCallback<ParseObject>() {
        @Override
        public void done(List<ParseObject> parseObjects, ParseException e) {
          cellList = new ArrayList<>();
          if(e == null) {
            if(parseObjects != null) {
              for(int i = 0; i < parseObjects.size(); i++) {
                ParseObject cell = parseObjects.get(i);
                NewPrivateCell newPrivateCell =
                  new NewPrivateCell((String) cell.get("name"), cell, cell.getInt("type"),
                    cell.getInt("totalMembers"));
                if(cell.getList("members") != null) {
                  LogEvent.Log("Cell",
                    "There are some members in " + cell.get("name") + " newPrivateCell");
                  List<ParseUser> members = cell.getList("members");
                  newPrivateCell.members.addAll(members);
                } else {
                  LogEvent.Log("Cell",
                    "There are no members in " + cell.get("name") + " newPrivateCell");
                }
                cellList.add(newPrivateCell);
              }
            }
          } else {
            if(Modules.isWeakReferenceValid()) {
              context.getApplicationContext();
              sendToast(e);
            }
          }
          Singleton.INSTANCE.setNewPrivateCells((ArrayList<NewPrivateCell>) cellList.clone());
          createCellsList(context, listView, alertMessage, title, isAlertForwarding, dialog);
        }
      });
    } else {
      createCellsList(context, listView, alertMessage, title, isAlertForwarding, dialog);
    }
    dialog.show();
  }

  private static void createCellsList(final Activity context, final ListView listView,
                                      final String alertMessage, final String title,
                                      final boolean isAlertForwarding, final AlertDialog dialog) {
    ArrayList<NewPrivateCell> cells = new ArrayList<>();
    for(int i = 0; i < cellList.size(); i++) {
      if((cellList.get(i).members != null && cellList.get(i).members.size() > 0) ||
           (cellList.get(i).nauMembers != null && cellList.get(i).nauMembers.length() > 0)) {
        cellList.get(i).selected = false;
        cells.add(cellList.get(i));
      }
    }
    cells.add(0, new NewPrivateCell("ALL FRIENDS", null, null, null));
    cells.get(0).selected = true;
    selectedCell = cells.get(0);
    boolean isPatrolModeFeatureEnabled =
      context.getResources().getBoolean(R.bool.is_patrol_mode_feature_enabled);
    if(isPatrolModeFeatureEnabled) {
      cells.add(0, new NewPrivateCell("GLOBAL ALERT", null, null, null));
      cells.get(0).selected = false;
    }
    if(!title.equals("Video") && !isAlertForwarding) {
      cells.add(0, new NewPrivateCell("PUBLIC CELLS", null, null, null));
      cells.get(0).selected = false;
    }
    final CellListAdapter adapterCell =
      new CellListAdapter(context, R.layout.cell_list_item, cells);
    listView.setAdapter(adapterCell);
    listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
      @Override
      public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
        if(adapterCell.cellsList.get(position).name.equals("PUBLIC CELLS") && !isAlertForwarding) {
          showSendAlertToPublicCellDialog(context, alertMessage, title);
          dialog.dismiss();
        } /*else if (adapterCell.cellsList.get(position).name.equals("NON APP USERS")
                        && !isAlertForwarding) {
                    showSendAlertToNonAppUserDialog(context, alertMessage, title);
                    dialog.dismiss();
                }*/ else if(adapterCell.cellsList.get(position).name.equals("SMS/Email") &&
                                        !isAlertForwarding) {
          Intent intentSelectContact = new Intent(context, SelectContactsActivity.class);
          intentSelectContact.putExtra("title", title);
          context.startActivity(intentSelectContact);
          dialog.dismiss();
        } else {
          for(int i = 0; i < adapterCell.cellsList.size(); i++) {
            adapterCell.cellsList.get(i).selected = false;
          }
          adapterCell.cellsList.get(position).selected = true;
          selectedCell = adapterCell.cellsList.get(position);
          adapterCell.notifyDataSetChanged();
        }
      }
    });
  }

  public static void showSendAlertToPublicCellDialog(final Activity context, String alertMessage,
                                                     final String title) {
    AlertDialog.Builder alert = new AlertDialog.Builder(context);
    alert.setTitle(alertMessage);
    LayoutInflater inflater =
      (LayoutInflater) context.getSystemService(Service.LAYOUT_INFLATER_SERVICE);
    View view = inflater.inflate(R.layout.layout_cell, null);
    final ListView listView = (ListView) view.findViewById(R.id.list_cells);
    final CheckBox cbIncludeSecurityGuards =
      (CheckBox) view.findViewById(R.id.cb_include_security_guards);
    cbIncludeSecurityGuards.setText(context.getString(R.string.checkbox_include_call_center,
      context.getString(R.string.app_name)));
    cbIncludeSecurityGuards.setEnabled(false);
    cbIncludeSecurityGuards.setVisibility(View.GONE);
    ParseQuery<ParseObject> cellQuery = ParseQuery.getQuery("PublicCell");
    cellQuery.whereGreaterThan("totalMembers", 1);
    cellQuery.whereEqualTo("createdBy", ParseUser.getCurrentUser());
    ParseQuery<ParseObject> cellQuery2 = ParseQuery.getQuery("PublicCell");
    cellQuery2.whereNotEqualTo("createdBy", ParseUser.getCurrentUser());
    cellQuery2.whereEqualTo("members", ParseUser.getCurrentUser());
    List<ParseQuery<ParseObject>> queries = new ArrayList<>();
    queries.add(cellQuery);
    queries.add(cellQuery2);
    ParseQuery<ParseObject> mainQuery = ParseQuery.or(queries);
    mainQuery.findInBackground(new FindCallback<ParseObject>() {
      @Override
      public void done(List<ParseObject> parseObjects, ParseException e) {
        joinedPublicCells = new ArrayList<>();
        if(e == null) {
          if(parseObjects != null) {
            for(int i = 0; i < parseObjects.size(); i++) {
              ParseObject cell = parseObjects.get(i);
              Cell cl = new Cell((String) cell.get("name"), cell);
              cl.isPublicCell = true;
              joinedPublicCells.add(cl);
            }
            if(joinedPublicCells.size() > 0) {
              joinedPublicCells.get(0).selected = true;
              selectedPublicCell = joinedPublicCells.get(0);
            }
          }
        } else {
          if(Modules.isWeakReferenceValid()) {
            context.getApplicationContext();
            sendToast(e);
          }
        }
        final PublicCellListAdapter adapterCell =
          new PublicCellListAdapter(context, R.layout.cell_list_item, joinedPublicCells);
        listView.setAdapter(adapterCell);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
          @Override
          public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
            for(int i = 0; i < adapterCell.cellsList.size(); i++) {
              adapterCell.cellsList.get(i).selected = false;
            }
            adapterCell.cellsList.get(position).selected = true;
            selectedPublicCell = adapterCell.cellsList.get(position);
            adapterCell.notifyDataSetChanged();
          }
        });
      }
    });
    alert.setView(view);
    alert.setNegativeButton(R.string.dialog_btn_cancel, new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int arg1) {
        dialog.dismiss();
      }
    });
    alert.setPositiveButton(R.string.dialog_btn_ok, new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int which) {
        if(selectedPublicCell != null) {
          showAddNoteDialog(context, null, title, true);
        }
      }
    });
    AlertDialog dialog = alert.create();
    dialog.show();
  }

  public static void streamVideoGlobally(final Activity context) {
    final int patrolModeRadius = Singleton.INSTANCE.getAppPrefs().getInt("PatrolModeRadius", 50);
    final ArrayList<ParseUser> parseUsersList = new ArrayList<>();
    final String name = ParseUser.getCurrentUser().get("firstName") + " " +
                          ParseUser.getCurrentUser().get("lastName");
    ParseGeoPoint userLocation =
      new ParseGeoPoint(Singleton.INSTANCE.getLatitude(), Singleton.INSTANCE.getLongitude());
    ParseQuery<ParseUser> query = ParseUser.getQuery();
    query.whereEqualTo("PatrolMode", 1);
    query.whereNotEqualTo("objectId", ParseUser.getCurrentUser().getObjectId());
    query.whereWithinMiles("location", userLocation, patrolModeRadius);
    ParseUser user = ParseUser.getCurrentUser();
    ParseRelation relation = user.getRelation("spammedBy");
    ParseQuery parseQuery = relation.getQuery();
    query.whereDoesNotMatchKeyInQuery("objectId", "objectId", parseQuery);
    query.findInBackground(new FindCallback<ParseUser>() {
      @Override
      public void done(List<ParseUser> parseUsers, ParseException e) {
        if(e == null) {
          if(parseUsers != null && parseUsers.size() > 0) {
            for(int i = 0; i < parseUsers.size(); i++) {
              ParseUser user = parseUsers.get(i);
              parseUsersList.add(user);
            }
            final ParseObject cell411AlertObject = new ParseObject("Cell411Alert");
            cell411AlertObject.put("status", "PROC_VID");
            cell411AlertObject.saveInBackground(new SaveCallback() {
              @Override
              public void done(ParseException e) {
                if(e == null) {
                  //                                    // Enable Everything is OK slice
                  //                                    cell411AlertObject4Ok = cell411AlertObject;
                  //                                    imgOk.setVisibility(View.VISIBLE);
                  ParseQuery pushQuery = ParseInstallation.getQuery();
                  pushQuery.whereContainedIn("user", parseUsersList);
                  pushQuery.setLimit(1000);
                  ParsePush push = new ParsePush();
                  push.setQuery(pushQuery);
                  JSONObject data = new JSONObject();
                  try {
                    data.put("alert", context.getString(R.string.notif_streaming_live_video, name));
                    data.put("alertRegarding", "Video");
                    data.put("userId", ParseUser.getCurrentUser().getObjectId());
                    data.put("sound", "default");
                    data.put("cell411AlertId", cell411AlertObject.getObjectId());
                    data.put("lat", Singleton.INSTANCE.getLatitude());
                    data.put("lon", Singleton.INSTANCE.getLongitude());
                    data.put("additionalNote", "");
                    data.put("createdAt", cell411AlertObject.getCreatedAt().getTime());
                    data.put("firstName", name);
                    data.put("isGlobal", 1);
                    data.put("alertType", "VIDEO");
                    data.put("badge", "Increment");
                  } catch(JSONException e1) {
                    e1.printStackTrace();
                  }
                  push.setData(data);
                  //push.sendInBackground();
                  if(parseUsersList.size() > 1) {
                    if(Modules.isWeakReferenceValid()) {
                      Singleton.sendToast("Video link sent to " + parseUsersList.size() + " users");
                    }
                  } else {
                    if(Modules.isWeakReferenceValid()) {
                      Singleton.sendToast("Video link sent to 1 user");
                    }
                  }

                  setIsGlobal(true);
                  setPush(push);
                  setCell411AlertObject(cell411AlertObject);
                  boolean streamVideoToFBPage =
                    Singleton.INSTANCE.getAppPrefs().getBoolean("StreamVideoToFacebookPage",
                      false);
                  boolean streamVideoToMyFBWall =
                    Singleton.INSTANCE.getAppPrefs().getBoolean("StreamVideoToMyFacebookWall",
                      false);
                  boolean streamVideoToYTChannel =
                    Singleton.INSTANCE.getAppPrefs().getBoolean("StreamVideoToYouTubeChannel",
                      false);
                  boolean streamVideoToMyYTChannel =
                    Singleton.INSTANCE.getAppPrefs().getBoolean("StreamVideoToMyYouTubeChannel",
                      false);
                  if(streamVideoToFBPage || streamVideoToMyFBWall || streamVideoToYTChannel ||
                       streamVideoToMyYTChannel) {
                    MainActivity.setSpinnerVisible(true);
                    // Initialize the receiver and start the service for reverse geo coded address
                    CityAndCountryReceiver mResultReceiver =
                      new CityAndCountryReceiver(new Handler(), context,
                        cell411AlertObject.getCreatedAt().getTime());
                    Intent intent = new Intent(context, FetchAddressIntentService.class);
                    intent.putExtra(Constants.RECEIVER, mResultReceiver);
                    intent.putExtra(Constants.LOCATION_DATA_LAT, Singleton.INSTANCE.getLatitude());
                    intent.putExtra(Constants.LOCATION_DATA_LNG, Singleton.INSTANCE.getLongitude());
                    context.startService(intent);
                  } else {
                    Intent intent = new Intent(context, VideoStreamingActivity.class);
                    context.startActivity(intent);
                  }
                } else {
                  if(Modules.isWeakReferenceValid()) {
                    context.getApplicationContext();
                    sendToast(e);
                  }
                }
              }
            });
          } else {
            if(Modules.isWeakReferenceValid()) {
              String metric = Singleton.INSTANCE.getAppPrefs().getString("metric", "kms");
              if(metric.equals("kms")) {
                if(Modules.isWeakReferenceValid()) {
                  context.getApplicationContext();
                  sendToast(
                    context.getString(R.string.toast_msg_no_one_found_having_patrol_mode_enabled,
                      patrolModeRadius, context.getString(R.string.kilometers)));
                }
              } else {
                if(Modules.isWeakReferenceValid()) {
                  context.getApplicationContext();
                  sendToast(
                    context.getString(R.string.toast_msg_no_one_found_having_patrol_mode_enabled,
                      patrolModeRadius, context.getString(R.string.miles)));
                }
              }
            }
          }
        } else {
          if(Modules.isWeakReferenceValid()) {
            context.getApplicationContext();
            sendToast(e);
          }
        }
      }
    });
  }

  public static void streamVideo(final Activity context, final NewPrivateCell selectedCell) {
    final ArrayList<ParseUser> parseUsersList = new ArrayList<>();
    final String name = ParseUser.getCurrentUser().get("firstName") + " " +
                          ParseUser.getCurrentUser().get("lastName");
    if(selectedCell.name.equals("ALL FRIENDS")) {
      // Send alert to all friends
      if(Singleton.INSTANCE.getFriends() != null) {
        for(int i = 0; i < Singleton.INSTANCE.getFriends().size(); i++) {
          parseUsersList.add(Singleton.INSTANCE.getFriends().get(i).user);
        }
        ParseRelation relation = ParseUser.getCurrentUser().getRelation("spammedBy");
        ParseQuery parseQuery = relation.getQuery();
        parseQuery.findInBackground(new FindCallback<ParseObject>() {
          @Override
          public void done(List list, ParseException e) {
            if(e == null) {
              if(list != null && list.size() > 0) {
                for(int i = 0; i < parseUsersList.size(); i++) {
                  for(int j = 0; j < list.size(); j++) {
                    if(parseUsersList.get(i).getObjectId().equals(
                      ((ParseUser) list.get(j)).getObjectId())) {
                      parseUsersList.remove(i);
                      --i;
                      break;
                    }
                  }
                }
              }
              if(parseUsersList.size() > 0) {
                final ParseObject cell411AlertObject = new ParseObject("Cell411Alert");
                cell411AlertObject.put("status", "PROC_VID");
                cell411AlertObject.saveInBackground(new SaveCallback() {
                  @Override
                  public void done(ParseException e) {
                    if(e == null) {
                      //                                            // Enable Everything is OK slice
                      //                                            cell411AlertObject4Ok = cell411AlertObject;
                      //                                            imgOk.setVisibility(View.VISIBLE);
                      ParseQuery pushQuery = ParseInstallation.getQuery();
                      pushQuery.whereContainedIn("user", parseUsersList);
                      pushQuery.setLimit(1000);
                      ParsePush push = new ParsePush();
                      push.setQuery(pushQuery);
                      JSONObject data = new JSONObject();
                      try {
                        data.put("alert",
                          context.getString(R.string.notif_streaming_live_video, name));
                        data.put("alertRegarding", "Video");
                        data.put("userId", ParseUser.getCurrentUser().getObjectId());
                        data.put("sound", "default");
                        data.put("cell411AlertId", cell411AlertObject.getObjectId());
                        data.put("lat", Singleton.INSTANCE.getLatitude());
                        data.put("lon", Singleton.INSTANCE.getLongitude());
                        data.put("additionalNote", "");
                        data.put("createdAt", cell411AlertObject.getCreatedAt().getTime());
                        data.put("firstName", name);
                        data.put("alertType", "VIDEO");
                        data.put("badge", "Increment");
                      } catch(JSONException e1) {
                        e1.printStackTrace();
                      }
                      push.setData(data);
                      //push.sendInBackground();
                      Singleton.sendToast("Video link sent to " + selectedCell.name);

                      setIsGlobal(false);
                      setPush(push);
                      setCell411AlertObject(cell411AlertObject);
                      boolean streamVideoToFBPage =
                        Singleton.INSTANCE.getAppPrefs().getBoolean("StreamVideoToFacebookPage",
                          false);
                      boolean streamVideoToMyFBWall =
                        Singleton.INSTANCE.getAppPrefs().getBoolean("StreamVideoToMyFacebookWall",
                          false);
                      boolean streamVideoToYTChannel =
                        Singleton.INSTANCE.getAppPrefs().getBoolean("StreamVideoToYouTubeChannel",
                          false);
                      boolean streamVideoToMyYTChannel =
                        Singleton.INSTANCE.getAppPrefs().getBoolean(
                          "StreamVideoToMyYouTubeChannel", false);
                      if(streamVideoToFBPage || streamVideoToMyFBWall || streamVideoToYTChannel ||
                           streamVideoToMyYTChannel) {
                        MainActivity.setSpinnerVisible(true);
                        // Initialize the receiver and start the service for reverse geo coded address
                        CityAndCountryReceiver mResultReceiver =
                          new CityAndCountryReceiver(new Handler(), context,
                            cell411AlertObject.getCreatedAt().getTime());
                        Intent intent = new Intent(context, FetchAddressIntentService.class);
                        intent.putExtra(Constants.RECEIVER, mResultReceiver);
                        intent.putExtra(Constants.LOCATION_DATA_LAT,
                          Singleton.INSTANCE.getLatitude());
                        intent.putExtra(Constants.LOCATION_DATA_LNG,
                          Singleton.INSTANCE.getLongitude());
                        context.startService(intent);
                      } else {
                        Intent intent = new Intent(context, VideoStreamingActivity.class);
                        context.startActivity(intent);
                      }
                    } else {
                      if(Modules.isWeakReferenceValid()) {
                        context.getApplicationContext();
                        sendToast(e);
                      }
                    }
                  }
                });
              } else {
                if(Modules.isWeakReferenceValid()) {
                  context.getApplicationContext();
                  sendToast(R.string.no_members_in_the_selected_cell);
                }
              }
            } else {
              if(Modules.isWeakReferenceValid()) {
                context.getApplicationContext();
                sendToast(e);
              }
            }
          }
        });
      } else {
        ParseUser user = ParseUser.getCurrentUser();
        ParseRelation relFriends = user.getRelation("friends");
        ParseQuery query4Friends = relFriends.getQuery();
        ParseRelation relation = user.getRelation("spammedBy");
        ParseQuery parseQuery = relation.getQuery();
        query4Friends.whereDoesNotMatchKeyInQuery("objectId", "objectId", parseQuery);
        query4Friends.findInBackground(new FindCallback<ParseObject>() {
          @Override
          public void done(List list, ParseException e) {
            if(e == null) {
              if(list != null && list.size() > 0) {
                for(int i = 0; i < list.size(); i++) {
                  ParseUser user = (ParseUser) list.get(i);
                  parseUsersList.add(user);
                }
              }
              if(parseUsersList.size() > 0) {
                final ParseObject cell411AlertObject = new ParseObject("Cell411Alert");
                cell411AlertObject.put("status", "PROC_VID");
                cell411AlertObject.saveInBackground(new SaveCallback() {
                  @Override
                  public void done(ParseException e) {
                    if(e == null) {
                      //                                        // Enable Everything is OK slice
                      //                                        cell411AlertObject4Ok = cell411AlertObject;
                      //                                        imgOk.setVisibility(View.VISIBLE);
                      ParseQuery pushQuery = ParseInstallation.getQuery();
                      pushQuery.whereContainedIn("user", parseUsersList);
                      pushQuery.setLimit(1000);
                      ParsePush push = new ParsePush();
                      push.setQuery(pushQuery);
                      JSONObject data = new JSONObject();
                      try {
                        data.put("alert",
                          context.getString(R.string.notif_streaming_live_video, name));
                        data.put("alertRegarding", "Video");
                        data.put("userId", ParseUser.getCurrentUser().getObjectId());
                        data.put("sound", "default");
                        data.put("cell411AlertId", cell411AlertObject.getObjectId());
                        data.put("lat", Singleton.INSTANCE.getLatitude());
                        data.put("lon", Singleton.INSTANCE.getLongitude());
                        data.put("additionalNote", "");
                        data.put("createdAt", cell411AlertObject.getCreatedAt().getTime());
                        data.put("firstName", name);
                        data.put("alertType", "VIDEO");
                        data.put("badge", "Increment");
                      } catch(JSONException e1) {
                        e1.printStackTrace();
                      }
                      push.setData(data);
                      //push.sendInBackground();
                      Singleton.sendToast("Video link sent to " + selectedCell.name);

                      setIsGlobal(false);
                      setPush(push);
                      setCell411AlertObject(cell411AlertObject);
                      boolean streamVideoToFBPage =
                        Singleton.INSTANCE.getAppPrefs().getBoolean("StreamVideoToFacebookPage",
                          false);
                      boolean streamVideoToMyFBWall =
                        Singleton.INSTANCE.getAppPrefs().getBoolean("StreamVideoToMyFacebookWall",
                          false);
                      boolean streamVideoToYTChannel =
                        Singleton.INSTANCE.getAppPrefs().getBoolean("StreamVideoToYouTubeChannel",
                          false);
                      boolean streamVideoToMyYTChannel =
                        Singleton.INSTANCE.getAppPrefs().getBoolean(
                          "StreamVideoToMyYouTubeChannel", false);
                      if(streamVideoToFBPage || streamVideoToMyFBWall || streamVideoToYTChannel ||
                           streamVideoToMyYTChannel) {
                        MainActivity.setSpinnerVisible(true);
                        // Initialize the receiver and start the service for reverse geo coded address
                        CityAndCountryReceiver mResultReceiver =
                          new CityAndCountryReceiver(new Handler(), context,
                            cell411AlertObject.getCreatedAt().getTime());
                        Intent intent = new Intent(context, FetchAddressIntentService.class);
                        intent.putExtra(Constants.RECEIVER, mResultReceiver);
                        intent.putExtra(Constants.LOCATION_DATA_LAT,
                          Singleton.INSTANCE.getLatitude());
                        intent.putExtra(Constants.LOCATION_DATA_LNG,
                          Singleton.INSTANCE.getLongitude());
                        context.startService(intent);
                      } else {
                        Intent intent = new Intent(context, VideoStreamingActivity.class);
                        context.startActivity(intent);
                      }
                    } else {
                      if(Modules.isWeakReferenceValid()) {
                        context.getApplicationContext();
                        sendToast(e);
                      }
                    }
                  }
                });
              } else {
                if(Modules.isWeakReferenceValid()) {
                  context.getApplicationContext();
                  sendToast(R.string.no_members_in_the_selected_cell);
                }
              }
            }
          }
        });
      }
    } else if(selectedCell.isCallCenter) {
      // Send alert to Security Guards
      final ParseObject cell411AlertObject = new ParseObject("Cell411Alert");
      cell411AlertObject.put("status", "PROC_VID");
      cell411AlertObject.saveInBackground(new SaveCallback() {
        @Override
        public void done(ParseException e) {
          if(e == null) {

            setIsGlobal(false);
            Singleton.sendToast("Video link sent to " + selectedCell.name);
            setPush(null);
            setCell411AlertObject(cell411AlertObject);
            boolean streamVideoToFBPage =
              Singleton.INSTANCE.getAppPrefs().getBoolean("StreamVideoToFacebookPage", false);
            boolean streamVideoToMyFBWall =
              Singleton.INSTANCE
                .getAppPrefs().getBoolean("StreamVideoToMyFacebookWall", false);
            boolean streamVideoToYTChannel =
              Singleton.INSTANCE
                .getAppPrefs().getBoolean("StreamVideoToYouTubeChannel", false);
            boolean streamVideoToMyYTChannel =
              Singleton.INSTANCE
                .getAppPrefs().getBoolean("StreamVideoToMyYouTubeChannel", false);
            if(streamVideoToFBPage || streamVideoToMyFBWall || streamVideoToYTChannel ||
                 streamVideoToMyYTChannel) {
              MainActivity.setSpinnerVisible(true);
              // Initialize the receiver and start the service for reverse geo coded address
              CityAndCountryReceiver mResultReceiver =
                new CityAndCountryReceiver(new Handler(), context,
                  cell411AlertObject.getCreatedAt().getTime());
              Intent intent = new Intent(context, FetchAddressIntentService.class);
              intent.putExtra(Constants.RECEIVER, mResultReceiver);
              intent.putExtra(Constants.LOCATION_DATA_LAT, Singleton.INSTANCE.getLatitude());
              intent.putExtra(Constants.LOCATION_DATA_LNG, Singleton.INSTANCE.getLongitude());
              context.startService(intent);
            } else {
              Intent intent = new Intent(context, VideoStreamingActivity.class);
              context.startActivity(intent);
            }
          } else {
            if(Modules.isWeakReferenceValid()) {
              context.getApplicationContext();
              sendToast(e);
            }
          }
        }
      });
    } else {
      // Send alert to selected cells
      parseUsersList.addAll(selectedCell.members);
      ParseRelation relation = ParseUser.getCurrentUser().getRelation("spammedBy");
      ParseQuery parseQuery = relation.getQuery();
      parseQuery.findInBackground(new FindCallback<ParseObject>() {
        @Override
        public void done(List list, ParseException e) {
          if(e == null) {
            if(list != null && list.size() > 0) {
              for(int i = 0; i < parseUsersList.size(); i++) {
                for(int j = 0; j < list.size(); j++) {
                  if(parseUsersList.get(i).getObjectId().equals(
                    ((ParseUser) list.get(j)).getObjectId())) {
                    parseUsersList.remove(i);
                    --i;
                    break;
                  }
                }
              }
            }
            if(parseUsersList.size() > 0) {
              final ParseObject cell411AlertObject = new ParseObject("Cell411Alert");
              cell411AlertObject.put("status", "PROC_VID");
              cell411AlertObject.saveInBackground(new SaveCallback() {
                @Override
                public void done(ParseException e) {
                  if(e == null) {
                    //                                        // Enable Everything is OK slice
                    //                                        cell411AlertObject4Ok = cell411AlertObject;
                    //                                        imgOk.setVisibility(View.VISIBLE);
                    ParseQuery pushQuery = ParseInstallation.getQuery();
                    pushQuery.whereContainedIn("user", parseUsersList);
                    pushQuery.setLimit(1000);
                    ParsePush push = new ParsePush();
                    push.setQuery(pushQuery);
                    JSONObject data = new JSONObject();
                    try {
                      data.put("alert",
                        context.getString(R.string.notif_streaming_live_video, name));
                      data.put("alertRegarding", "Video");
                      data.put("userId", ParseUser.getCurrentUser().getObjectId());
                      data.put("sound", "default");
                      data.put("cell411AlertId", cell411AlertObject.getObjectId());
                      data.put("additionalNote", "");
                      data.put("createdAt", cell411AlertObject.getCreatedAt().getTime());
                      data.put("firstName", name);
                      data.put("lat", Singleton.INSTANCE.getLatitude());
                      data.put("lon", Singleton.INSTANCE.getLongitude());
                      data.put("alertType", "VIDEO");
                      data.put("badge", "Increment");
                    } catch(JSONException e1) {
                      e1.printStackTrace();
                    }
                    push.setData(data);
                    //push.sendInBackground();

                    setIsGlobal(false);
                    Singleton.sendToast("Video link sent to " + selectedCell.name);
                    setPush(push);
                    setCell411AlertObject(cell411AlertObject);
                    boolean streamVideoToFBPage =
                      Singleton.INSTANCE.getAppPrefs().getBoolean("StreamVideoToFacebookPage",
                        false);
                    boolean streamVideoToMyFBWall =
                      Singleton.INSTANCE.getAppPrefs().getBoolean("StreamVideoToMyFacebookWall",
                        false);
                    boolean streamVideoToYTChannel =
                      Singleton.INSTANCE.getAppPrefs().getBoolean("StreamVideoToYouTubeChannel",
                        false);
                    boolean streamVideoToMyYTChannel =
                      Singleton.INSTANCE.getAppPrefs().getBoolean("StreamVideoToMyYouTubeChannel",
                        false);
                    if(streamVideoToFBPage || streamVideoToMyFBWall || streamVideoToYTChannel ||
                         streamVideoToMyYTChannel) {
                      MainActivity.setSpinnerVisible(true);
                      // Initialize the receiver and start the service for reverse geo coded address
                      CityAndCountryReceiver mResultReceiver =
                        new CityAndCountryReceiver(new Handler(), context,
                          cell411AlertObject.getCreatedAt().getTime());
                      Intent intent = new Intent(context, FetchAddressIntentService.class);
                      intent.putExtra(Constants.RECEIVER, mResultReceiver);
                      intent.putExtra(Constants.LOCATION_DATA_LAT,
                        Singleton.INSTANCE.getLatitude());
                      intent.putExtra(Constants.LOCATION_DATA_LNG,
                        Singleton.INSTANCE.getLongitude());
                      context.startService(intent);
                    } else {
                      Intent intent = new Intent(context, VideoStreamingActivity.class);
                      context.startActivity(intent);
                    }
                  } else {
                    if(Modules.isWeakReferenceValid()) {
                      context.getApplicationContext();
                      sendToast(e);
                    }
                  }
                }
              });
            } else {
              if(Modules.isWeakReferenceValid()) {
                context.getApplicationContext();
                sendToast(R.string.no_members_in_the_selected_cell);
              }
            }
          } else {
            if(Modules.isWeakReferenceValid()) {
              context.getApplicationContext();
              sendToast(e);
            }
          }
        }
      });
    }
  }

  public static void streamVideoWithoutFriends(final Activity context) {
    final ParseObject cell411AlertObject = new ParseObject("Cell411Alert");
    cell411AlertObject.put("status", "PROC_VID");
    cell411AlertObject.saveInBackground(new SaveCallback() {
      @Override
      public void done(ParseException e) {
        if(e == null) {

          setIsGlobal(false);
          Singleton.sendToast(null);
          setPush(null);
          setCell411AlertObject(cell411AlertObject);
          MainActivity.setSpinnerVisible(true);
          // Initialize the receiver and start the service for reverse geo coded address
          CityAndCountryReceiver mResultReceiver =
            new CityAndCountryReceiver(new Handler(), context,
              cell411AlertObject.getCreatedAt().getTime());
          Intent intent = new Intent(context, FetchAddressIntentService.class);
          intent.putExtra(Constants.RECEIVER, mResultReceiver);
          intent.putExtra(Constants.LOCATION_DATA_LAT, Singleton.INSTANCE.getLatitude());
          intent.putExtra(Constants.LOCATION_DATA_LNG, Singleton.INSTANCE.getLongitude());
          context.startService(intent);
        } else {
          MainActivity.setSpinnerVisible(false);
          if(Modules.isWeakReferenceValid()) {
            context.getApplicationContext();
            sendToast(e);
          }
        }
      }
    });
  }

  public static void showVideoStreamingAlert(final Activity context,
                                             final NewPrivateCell selectedCell) {
    AlertDialog.Builder alert = new AlertDialog.Builder(context);
    if(selectedCell == null) {
      alert.setMessage(R.string.dialog_msg_video_streaming_varient1);
    } else if(selectedCell.name.equals("ALL FRIENDS")) {
      alert.setMessage(R.string.dialog_msg_video_streaming_varient2);
    } else if(selectedCell.isCallCenter) {
      alert.setMessage(R.string.dialog_msg_video_streaming_varient3);
    } else {
      alert.setMessage(
        context.getString(R.string.dialog_msg_video_streaming_varient4, selectedCell.name));
    }
    LayoutInflater inflater =
      (LayoutInflater) context.getSystemService(Service.LAYOUT_INFLATER_SERVICE);
    View view = inflater.inflate(R.layout.layout_video_streaming, null);
    final TextView txtBtnStreamVideo = (TextView) view.findViewById(R.id.txt_btn_stream_video);
    final TextView txtBtnNo = (TextView) view.findViewById(R.id.txt_btn_no);
    txtBtnStreamVideo.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        if(selectedCell == null) {
          streamVideoGlobally(context);
        } else {
          streamVideo(context, selectedCell);
        }
        dialog.dismiss();
      }
    });
    alert.setView(view);
    alert.setNegativeButton(R.string.dialog_btn_close, new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int which) {
      }
    });
    dialog = alert.create();
    dialog.show();
  }

  public static void showGlobalAlertAddNoteDialog(final Activity context, final String title) {
    byte[] b = new byte[0];
    ParseFile file = null;
    if(title.equals("Photo")) {
      String path = context.getExternalFilesDir(null).getAbsolutePath() + "/Photo/photo_alert.png";
      Bitmap bmp = BitmapFactory.decodeFile(path);
      ByteArrayOutputStream baos = new ByteArrayOutputStream();
      bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
      b = baos.toByteArray();
      file = new ParseFile("photo_alert.png", b);
    }
    final boolean isDispatchModeEnabled =
      Singleton.INSTANCE.getAppPrefs().getBoolean("DispatchMode", false);
    AlertDialog.Builder alert = new AlertDialog.Builder(context);
    alert.setTitle(R.string.dialog_title_send_global_alert);
    final int patrolModeRadius;
    final String metric = Singleton.INSTANCE.getAppPrefs().getString("metric", "kms");
    if(metric.equals("kms")) {
      patrolModeRadius = Singleton.INSTANCE.getAppPrefs().getInt("PatrolModeRadius", 80);
      alert.setMessage(context.getString(R.string.dialog_msg_sending_global_alert,
        context.getString(R.string.app_name), patrolModeRadius,
        context.getString(R.string.kilometers)));
    } else {
      patrolModeRadius = Singleton.INSTANCE.getAppPrefs().getInt("PatrolModeRadius", 50);
      alert.setMessage(context.getString(R.string.dialog_msg_sending_global_alert,
        context.getString(R.string.app_name), patrolModeRadius,
        context.getString(R.string.miles)));
    }
    LayoutInflater inflater =
      (LayoutInflater) context.getSystemService(Service.LAYOUT_INFLATER_SERVICE);
    View view = inflater.inflate(R.layout.layout_add_text, null);
    final EditText etAdditionalText = (EditText) view.findViewById(R.id.et_additional_text);
    if(title.equals("General")) {
      etAdditionalText.setHint(R.string.enter_alert_description);
    }
    alert.setView(view);
    alert.setNegativeButton(R.string.dialog_btn_cancel, new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int arg1) {
      }
    });
    alert.setPositiveButton(R.string.dialog_btn_continue, new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int arg1) {
      }
    });
    final AlertDialog dialog = alert.create();
    dialog.show();
    final ParseFile finalFile = file;
    final byte[] finalB = b;
    dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        final String additionalText = etAdditionalText.getText().toString().trim();
        if(additionalText.isEmpty() && title.equals("General")) {
          if(Modules.isWeakReferenceValid()) {
            context.getApplicationContext();
            sendToast(R.string.validation_please_enter_description);
          }
          return;
        }
        ParseUser.getCurrentUser().fetchInBackground(new GetCallback<ParseObject>() {
          @Override
          public void done(ParseObject parseObject, ParseException e) {
            if(e == null) {
              String privilege = (String) ParseUser.getCurrentUser().get("privilege");
              if(privilege == null || privilege.equals("")) {
                // check if user has added at least two friends and has issued an alert
                // to private cell
                // if the above condition is met then he/she can issue alerts globally
                ParseUser user = ParseUser.getCurrentUser();
                ParseRelation relFriends = user.getRelation("friends");
                ParseQuery query4Friends = relFriends.getQuery();
                query4Friends.countInBackground(new CountCallback() {
                  @Override
                  public void done(int count, ParseException e) {
                    if(e == null) {
                      // The count request succeeded. Log the count
                      LogEvent.Log(TAG, "Total Friends Check: " + count);
                      if(count >= 2) {
                        ParseQuery<ParseObject> parseQuery4SelfCell411Alerts =
                          ParseQuery.getQuery("Cell411Alert");
                        parseQuery4SelfCell411Alerts.whereEqualTo("issuedBy",
                          ParseUser.getCurrentUser());
                        parseQuery4SelfCell411Alerts.whereDoesNotExist("cellName");
                        parseQuery4SelfCell411Alerts.whereNotEqualTo("isGlobal", 1);
                        parseQuery4SelfCell411Alerts.whereDoesNotExist("entryFor");
                        parseQuery4SelfCell411Alerts.whereDoesNotExist("to");
                        parseQuery4SelfCell411Alerts.whereDoesNotExist("cellId");
                        parseQuery4SelfCell411Alerts.whereExists("alertType");
                        parseQuery4SelfCell411Alerts.whereExists("targetMembers");
                        ParseQuery<ParseObject> parseQuery4ForwardedCell411Alerts =
                          ParseQuery.getQuery("Cell411Alert");
                        parseQuery4ForwardedCell411Alerts.whereEqualTo("forwardedBy",
                          ParseUser.getCurrentUser());
                        parseQuery4ForwardedCell411Alerts.whereExists("forwardedToMembers");
                        parseQuery4ForwardedCell411Alerts.whereExists("forwardedAlert");
                        parseQuery4ForwardedCell411Alerts.whereNotEqualTo("isGlobal", 1);
                        parseQuery4ForwardedCell411Alerts.whereDoesNotExist("cellId");
                        parseQuery4ForwardedCell411Alerts.whereDoesNotExist("cellName");
                        List<ParseQuery<ParseObject>> queries = new ArrayList<>();
                        queries.add(parseQuery4SelfCell411Alerts);
                        queries.add(parseQuery4ForwardedCell411Alerts);
                        ParseQuery<ParseObject> mainQuery = ParseQuery.or(queries);
                        mainQuery.countInBackground(new CountCallback() {
                          @Override
                          public void done(int count, ParseException e) {
                            if(e == null) {
                              // The count request succeeded. Log the count
                              LogEvent.Log(TAG, "Total Alerts Check: " + count);
                              if(count >= 1) {
                                ParseUser parseUser = ParseUser.getCurrentUser();
                                parseUser.put("privilege", "SECOND");
                                parseUser.saveInBackground();
                                sendAlertV2(context, title, finalB, selectedCell, additionalText,
                                  null, true, false, false);
                              } else {
                                ParseUser parseUser = ParseUser.getCurrentUser();
                                parseUser.put("privilege", "FIRST");
                                parseUser.saveInBackground();
                                PrivilegeModules.showUnfulfilledPrivilegeAlert(context);
                              }
                            } else {
                              // The request failed
                              if(Modules.isWeakReferenceValid()) {
                                context.getApplicationContext();
                                sendToast(R.string.network_error);
                              }
                            }
                          }
                        });
                      } else {
                        ParseUser parseUser = ParseUser.getCurrentUser();
                        parseUser.put("privilege", "FIRST");
                        parseUser.saveInBackground();
                        PrivilegeModules.showUnfulfilledPrivilegeAlert(context);
                      }
                    } else {
                      // The request failed
                      if(Modules.isWeakReferenceValid()) {
                        context.getApplicationContext();
                        sendToast(R.string.network_error);
                      }
                    }
                  }
                });
              } else if(privilege.equals("FIRST")) {
                PrivilegeModules.showUnfulfilledPrivilegeAlert(context);
              } else if(privilege.equals("BANNED")) {
                // user is permanently banned
                PrivilegeModules.logoutUser();
              } else if(privilege.contains("SUSPENDED")) {
                // blocked user temporarily
                PrivilegeModules.logoutUser();
              } else if(privilege.equals("SECOND")) {
                sendAlertV2(context, title, finalB, selectedCell, additionalText, null, true, false,
                  false);
              } else {
                // Some new privilege is added
                sendAlertV2(context, title, finalB, selectedCell, additionalText, null, true, false,
                  false);
              }
            } else {
              // The request failed
              if(Modules.isWeakReferenceValid()) {
                context.getApplicationContext();
                sendToast(R.string.network_error);
              }
            }
          }
        });
        dialog.dismiss();
      }
    });
  }

  public static void showAddNoteDialog(final Activity context, final NewPrivateCell selectedCell,
                                       final String title, final boolean isPublicCell) {
    ParseFile file = null;
    byte[] b = new byte[0];
    if(title.equals("Photo")) {
      String path = context.getExternalFilesDir(null).getAbsolutePath() + "/Photo/photo_alert.png";
      Bitmap bmp = BitmapFactory.decodeFile(path);
      ByteArrayOutputStream baos = new ByteArrayOutputStream();
      bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
      b = baos.toByteArray();
      file = new ParseFile("photo_alert.png", b);
    }
    AlertDialog.Builder alert = new AlertDialog.Builder(context);
    if(title.equals("General")) {
      alert.setTitle(R.string.add_a_note);
    } else {
      alert.setTitle(R.string.add_a_note2);
    }
    LayoutInflater inflater =
      (LayoutInflater) context.getSystemService(Service.LAYOUT_INFLATER_SERVICE);
    View view = inflater.inflate(R.layout.layout_add_text, null);
    final EditText etAdditionalText = (EditText) view.findViewById(R.id.et_additional_text);
    if(title.equals("General")) {
      etAdditionalText.setHint(R.string.enter_alert_description);
    }
    alert.setView(view);
    alert.setNegativeButton(R.string.dialog_btn_cancel, new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int arg1) {
        dialog.dismiss();
      }
    });
    alert.setPositiveButton(R.string.dialog_btn_continue, new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int which) {
      }
    });
    final AlertDialog dialog = alert.create();
    dialog.show();
    final ParseFile finalFile = file;
    final byte[] finalB = b;
    dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        final String additionalText = etAdditionalText.getText().toString().trim();
        if(additionalText.isEmpty() && title.equals("General")) {
          if(Modules.isWeakReferenceValid()) {
            context.getApplicationContext();
            sendToast(R.string.validation_please_enter_description);
          }
          return;
        }
        LogEvent.Log(TAG, "title: " + title);
        if(isPublicCell) {
          sendAlertV2(context, title, finalB, selectedCell, additionalText, null, false, true,
            false);
          dialog.dismiss();
          return;
        }
        if(selectedCell.isCallCenter) {
          sendAlertToSecurityGuards(context, title, additionalText, finalFile);
          dialog.dismiss();
          return;
        }
        sendAlertV2(context, title, finalB, selectedCell, additionalText, null, false, false,
          false);
        dialog.dismiss();
      }
    });
  }

  private static void sendAlertV2(final Activity context, final String title, final byte[] finalB,
                                  final NewPrivateCell selectedCell, final String additionalText,
                                  final ReceivedCell411Alert receivedCell411Alert,
                                  final boolean isGlobal, final boolean isPublicCell,
                                  final boolean isForwarding) {
    final boolean isDispatchModeEnabled =
      Singleton.INSTANCE.getAppPrefs().getBoolean("DispatchMode", false);
    final String name = ParseUser.getCurrentUser().get("firstName") + " " +
                          ParseUser.getCurrentUser().get("lastName");
    JSONObject objAlert = new JSONObject();
    try {
      objAlert.put("title", title);
      if(isDispatchModeEnabled && !isForwarding) { // An alert cannot be dispatched when forwarding
        objAlert.put("isDispatched", true);
        objAlert.put("lat", Singleton.INSTANCE.getCustomLocation().latitude);
        objAlert.put("lng", Singleton.INSTANCE.getCustomLocation().longitude);
      } else {
        objAlert.put("lat", Singleton.INSTANCE.getLatitude());
        objAlert.put("lng", Singleton.INSTANCE.getLongitude());
      }
      objAlert.put("additionalNote", additionalText);
      JSONObject objAudience = new JSONObject();
      if(isGlobal) { // GLOBAL ALERT
        int patrolModeRadius;
        String metric = Singleton.INSTANCE.getAppPrefs().getString("metric", "kms");
        if(metric.equals("kms")) {
          patrolModeRadius = Singleton.INSTANCE.getAppPrefs().getInt("PatrolModeRadius", 80);
        } else {
          patrolModeRadius = Singleton.INSTANCE.getAppPrefs().getInt("PatrolModeRadius", 50);
        }
        objAlert.put("metric", metric);
        objAlert.put("radius", patrolModeRadius);
        objAudience.put("Global", true);
        if(isForwarding) {
          objAlert.put("type", "NEEDY_FORWARDED");
          String issuerName =
            receivedCell411Alert.issuerFirstName + " " + receivedCell411Alert.issuerLastName;
          objAlert.put("msg", context.getString(R.string.alert_msg_global_forwarded, issuerName,
            Modules.convertEnumToString(context, Modules.convertStringToEnum(title)), name));
        } else {
          if(title.equals("Photo")) {
            objAlert.put("type", "PHOTO");
          } else {
            objAlert.put("type", "NEEDY");
          }
          objAlert.put("msg", context.getString(R.string.alert_msg_global, name,
            Modules.convertEnumToString(context, Modules.convertStringToEnum(title))));
        }
      } else if(isPublicCell) { // PUBLIC CELL
        JSONArray jsonArrayAudience = new JSONArray();
        jsonArrayAudience.put(selectedPublicCell.cell.getObjectId());
        objAudience.put("PublicCells", jsonArrayAudience);
        if(title.equals("Photo")) {
          objAlert.put("type", "PHOTO_CELL");
        } else {
          objAlert.put("type", "NEEDY_CELL");
        }
        objAlert.put("msg",
          name + ", " + context.getString(R.string.a_member_of) + " " + selectedPublicCell.name +
            " " + context.getString(R.string.issued_a) + " " +
            Modules.convertEnumToString(context, Modules.convertStringToEnum(title)) + " " +
            getAlertNotificationTrail());
      } else if(selectedCell.parseObject == null) { // ALL FRIENDS
        objAudience.put("AllFriends", true);
        if(isForwarding) {
          objAlert.put("type", "NEEDY_FORWARDED");
          String issuerName =
            receivedCell411Alert.issuerFirstName + " " + receivedCell411Alert.issuerLastName;
          objAlert.put("msg", issuerName + " " + context.getString(R.string.issued_a) + " " +
                                Modules.convertEnumToString(context,
                                  Modules.convertStringToEnum(title)) + " " +
                                getAlertNotificationTrail() + ", " + " " +
                                context.getString(R.string.forwarded_by) + " " + name);
        } else {
          if(title.equals("Photo")) {
            objAlert.put("type", "PHOTO");
          } else {
            objAlert.put("type", "NEEDY");
          }
          objAlert.put("msg", name + " " + context.getString(R.string.issued_a) + " " +
                                Modules.convertEnumToString(context,
                                  Modules.convertStringToEnum(title)) + " " +
                                getAlertNotificationTrail());
        }
      } else { // PRIVATE CELL
        JSONArray jsonArrayAudience = new JSONArray();
        jsonArrayAudience.put(selectedCell.parseObject.getObjectId());
        objAudience.put("PrivateCells", jsonArrayAudience);
        if(isForwarding) {
          objAlert.put("type", "NEEDY_FORWARDED");
          String issuerName =
            receivedCell411Alert.issuerFirstName + " " + receivedCell411Alert.issuerLastName;
          objAlert.put("msg", issuerName + " " + context.getString(R.string.issued_a) + " " +
                                Modules.convertEnumToString(context,
                                  Modules.convertStringToEnum(title)) + " " +
                                getAlertNotificationTrail() + ", " + " " +
                                context.getString(R.string.forwarded_by) + " " + name);
        } else {
          if(title.equals("Photo")) {
            objAlert.put("type", "PHOTO");
          } else {
            objAlert.put("type", "NEEDY");
          }
          objAlert.put("msg", name + " " + context.getString(R.string.issued_a) + " " +
                                Modules.convertEnumToString(context,
                                  Modules.convertStringToEnum(title)) + " " +
                                getAlertNotificationTrail());
        }
      }
      objAlert.put("audience", objAudience);
      if(isForwarding) {
        objAlert.put("forwardedAlertId", receivedCell411Alert.cell411AlertId);
      }
    } catch(JSONException e) {
      e.printStackTrace();
    }
    // send alert
    HashMap<String, Object> params = new HashMap<>();
    params.put("alert", objAlert.toString());
    params.put("clientFirmId", 1);
    params.put("isLive", context.getResources().getInteger(R.integer.is_live));
    params.put("languageCode", context.getString(R.string.language_code));
    params.put("platform", "android");
    if(title.equals("Photo")) {
      params.put("imageBytes", finalB);
    }
    ParseCloud.callFunctionInBackground("sendAlertV2", params, new FunctionCallback<String>() {
      public void done(String result, ParseException e) {
        if(e == null) {
          LogEvent.Log(TAG, "result: " + result);
          if(!isPublicCell && isGlobal) {
            // Check the privilege and update it, if required
            PrivilegeModules.checkAndUpdatePrivilege(false);
          }
          finalizeAlert(context, title, result, selectedCell, additionalText, receivedCell411Alert,
            isGlobal, isPublicCell, isForwarding);
          JSONObject objResult = null;
          int targetMembersCount = 0;
          int targetNAUMembersCount = 0;
          try {
            objResult = new JSONObject(result);
            targetMembersCount = objResult.getInt("targetMembersCount");
            targetNAUMembersCount = objResult.getInt("targetNAUMembersCount");
          } catch(JSONException jsonException) {
            e.printStackTrace();
          }
          int totalUsers = targetMembersCount + targetNAUMembersCount;
          if(Modules.isWeakReferenceValid()) {
            if(totalUsers >= 1) {
              if(totalUsers > 1) {
                context.getApplicationContext();
                sendToast(context.getString(R.string.toast_alert_sent, totalUsers));
              } else {
                context.getApplicationContext();
                sendToast(R.string.toast_alert_sent_to_1_user);
              }
            } else {
              if(isGlobal) {
                int patrolModeRadius;
                String metric = Singleton.INSTANCE.getAppPrefs().getString("metric", "kms");
                if(metric.equals("kms")) {
                  patrolModeRadius =
                    Singleton.INSTANCE.getAppPrefs().getInt("PatrolModeRadius", 80);
                } else {
                  patrolModeRadius =
                    Singleton.INSTANCE.getAppPrefs().getInt("PatrolModeRadius", 50);
                }
                if(metric.equals("kms")) {
                  context.getApplicationContext();
                  sendToast(
                    context.getString(R.string.toast_msg_no_one_found_having_patrol_mode_enabled,
                      patrolModeRadius, context.getString(R.string.kilometers)));
                } else {
                  context.getApplicationContext();
                  sendToast(
                    context.getString(R.string.toast_msg_no_one_found_having_patrol_mode_enabled,
                      patrolModeRadius, context.getString(R.string.miles)));
                }
              } else {
                context.getApplicationContext();
                sendToast(R.string.no_members_in_the_selected_cell);
              }
            }
          }
        } else {
          if(Modules.isWeakReferenceValid()) {
            context.getApplicationContext();
            sendToast(e);
          }
        }
      }
    });
  }

  private static void finalizeAlert(final Activity context, final String title, final String result,
                                    final NewPrivateCell selectedCell, final String additionalText,
                                    final ReceivedCell411Alert receivedCell411Alert,
                                    final boolean isGlobal, final boolean isPublicCell,
                                    final boolean isForwarding) {
    final boolean isDispatchModeEnabled =
      Singleton.INSTANCE.getAppPrefs().getBoolean("DispatchMode", false);
    final String name = ParseUser.getCurrentUser().get("firstName") + " " +
                          ParseUser.getCurrentUser().get("lastName");
    JSONObject objResult = null;
    String cell411AlertId = null;
    long createdAt = 0;
    String photoUrl = null;
    try {
      objResult = new JSONObject(result);
      cell411AlertId = objResult.getString("cell411AlertId");
      createdAt = objResult.getLong("createdAt");
      if(objResult.has("photoUrl")) {
        photoUrl = objResult.getString("photoUrl");
      }
    } catch(JSONException e) {
      e.printStackTrace();
    }
    if(isForwarding) {
      return;
    }
    if(title.equals("Medical")) {
      String message = "";
      ParseUser user = ParseUser.getCurrentUser();
      String bloodType = (String) user.get("bloodType");
      String allergies = (String) user.get("allergies");
      String otherMedicalConditions = (String) user.get("otherMedicalConditions");
      if(bloodType != null && !bloodType.isEmpty()) {
        message +=
          "\n" + context.getString(R.string.medical_info_blood_type) + " " + bloodType + "\n";
      }
      if(allergies != null && !allergies.isEmpty()) {
        message +=
          "\n" + context.getString(R.string.medical_info_allergies) + " " + allergies + "\n";
      }
      if(otherMedicalConditions != null && !otherMedicalConditions.isEmpty()) {
        message += "\n" + context.getString(R.string.medical_info_other_medical_conditions) + " " +
                     otherMedicalConditions + "\n";
      }
      if(!message.isEmpty()) {
        showMedicalDetailsDialog(context, message);
      }
    }
    if(Singleton.INSTANCE.getAppPrefs().getBoolean("PublishAlertToFacebook", false)) {
      int dispatchMode = 0;
      if(isDispatchModeEnabled && !title.equals("Photo")) {
        dispatchMode = 1;
      }
      int isGlob = 0;
      if(isGlobal) {
        isGlob = 1;
      }
    }
    if(!title.equals("Photo")) {
      // Alert for streaming video
      boolean isLiveStreamingEnabled =
        context.getResources().getBoolean(R.bool.is_live_streaming_enabled);
      if(isLiveStreamingEnabled) {
        showVideoStreamingAlert(context, selectedCell);
      }
    }
  }

  /**
   * This method is called only when issuing alert to a private cell
   *
   * @param context
   * @param selectedCell
   * @param title
   * @param parseUsersList
   * @param additionalText
   * @param finalFile
   */
  private static void sendAlert(final Activity context, final NewPrivateCell selectedCell,
                                final String title, final ArrayList<ParseUser> parseUsersList,
                                final String additionalText, final ParseFile finalFile) {
    final boolean isDispatchModeEnabled =
      Singleton.INSTANCE.getAppPrefs().getBoolean("DispatchMode", false);
    final String name = ParseUser.getCurrentUser().get("firstName") + " " +
                          ParseUser.getCurrentUser().get("lastName");
    // Check if the feature is enabled in the build configuration
    if(parseUsersList.size() > 0 || (selectedCell != null && selectedCell.nauMembers != null &&
                                       selectedCell.nauMembers.length() > 0)) {
      final ParseObject cell411AlertObject = new ParseObject("Cell411Alert");
      cell411AlertObject.put("issuedBy", ParseUser.getCurrentUser());
      cell411AlertObject.put("issuerFirstName", name);
      cell411AlertObject.put("issuerId", ParseUser.getCurrentUser().getObjectId());
      cell411AlertObject.put("alertType", title);
      cell411AlertObject.put("additionalNote", additionalText);
      cell411AlertObject.put("targetMembers", parseUsersList);
      if(title.equals("Photo")) {
        cell411AlertObject.put("photo", finalFile);
      }
      if(isDispatchModeEnabled && !title.equals("Photo")) {
        cell411AlertObject.put("location",
          new ParseGeoPoint(Singleton.INSTANCE.getCustomLocation().latitude,
            Singleton.INSTANCE.getCustomLocation().longitude));
        cell411AlertObject.put("dispatchMode", 1);
      } else {
        cell411AlertObject.put("location",
          new ParseGeoPoint(Singleton.INSTANCE.getLatitude(), Singleton.INSTANCE.getLongitude()));
      }
      cell411AlertObject.saveInBackground(new SaveCallback() {
        @Override
        public void done(ParseException e) {
          if(e == null) {
            if(!title.equals(
              "Photo")) { // In case of photo alert, app will not enable the All OK Slice
              // Enable Everything is OK slice
              //cell411AlertObject4Ok = cell411AlertObject;
              //imgOk.setVisibility(View.VISIBLE);
            }
            // Check the privilege and update it, if required
            PrivilegeModules.checkAndUpdatePrivilege(false);
            ParseQuery pushQuery = ParseInstallation.getQuery();
            pushQuery.whereContainedIn("user", parseUsersList);
            pushQuery.setLimit(1000);
            ParsePush push = new ParsePush();
            push.setQuery(pushQuery);
            JSONObject data = new JSONObject();
            try {
              if(title.equals("Photo")) {
                data.put("alert", context.getString(R.string.notif_photo_alert, name));
              } else {
                data.put("alert", context.getString(R.string.notif_emergency_alert, name));
              }
              data.put("alertRegarding", title);
              data.put("userId", ParseUser.getCurrentUser().getObjectId());
              data.put("sound", "default");
              data.put("cell411AlertId", cell411AlertObject.getObjectId());
              if(isDispatchModeEnabled && !title.equals("Photo")) {
                data.put("lat", Singleton.INSTANCE.getCustomLocation().latitude);
                data.put("lon", Singleton.INSTANCE.getCustomLocation().longitude);
                data.put("dispatchMode", 1);
              } else {
                data.put("lat", Singleton.INSTANCE.getLatitude());
                data.put("lon", Singleton.INSTANCE.getLongitude());
              }
              data.put("additionalNote", additionalText);
              data.put("createdAt", cell411AlertObject.getCreatedAt().getTime());
              data.put("firstName", name);
              if(title.equals("Photo")) {
                data.put("alertType", "PHOTO");
              } else {
                data.put("alertType", "NEEDY");
              }
              data.put("badge", "Increment");
            } catch(JSONException e1) {
              e1.printStackTrace();
            }
            push.setData(data);
            push.sendInBackground();
            JSONObject obj = new JSONObject();
            try {
              obj.put(LMA.PARAM_USER_ID, ParseUser.getCurrentUser().getObjectId());
              obj.put(LMA.PARAM_ALERT_TYPE, title);
              if(additionalText == null) {
                obj.put(LMA.PARAM_NOTE, "");
              } else {
                obj.put(LMA.PARAM_NOTE, additionalText);
              }
              if(title.equals("Photo")) {
                obj.put(LMA.PARAM_IMAGE_URL, finalFile.getUrl());
              }
              obj.put(LMA.PARAM_GEO_LOCATION,
                Singleton.INSTANCE.getLatitude() + "," + Singleton.INSTANCE.getLongitude());
            } catch(JSONException e1) {
              e1.printStackTrace();
            }
            if(title.equals("Medical")) {
              String message = "";
              ParseUser user = ParseUser.getCurrentUser();
              String bloodType = (String) user.get("bloodType");
              String allergies = (String) user.get("allergies");
              String otherMedicalConditions = (String) user.get("otherMedicalConditions");
              if(bloodType != null && !bloodType.isEmpty()) {
                message +=
                  "\n" + context.getString(R.string.medical_info_blood_type) + " " + bloodType +
                    "\n";
              }
              if(allergies != null && !allergies.isEmpty()) {
                message +=
                  "\n" + context.getString(R.string.medical_info_allergies) + " " + allergies +
                    "\n";
              }
              if(otherMedicalConditions != null && !otherMedicalConditions.isEmpty()) {
                message +=
                  "\n" + context.getString(R.string.medical_info_other_medical_conditions) + " " +
                    otherMedicalConditions + "\n";
              }
              if(!message.isEmpty()) {
                showMedicalDetailsDialog(context, message);
              }
            }
            if(Modules.isWeakReferenceValid()) {
              context.getApplicationContext();
              sendToast(context.getString(R.string.toast_alert_sent) + " " + selectedCell.name);
            }
            if(!title.equals("Photo")) {
              // Alert for streaming video
              boolean isLiveStreamingEnabled =
                context.getResources().getBoolean(R.bool.is_live_streaming_enabled);
              if(isLiveStreamingEnabled) {
                showVideoStreamingAlert(context, selectedCell);
              }
            }
          } else {
            if(Modules.isWeakReferenceValid()) {
              context.getApplicationContext();
              sendToast(e);
            }
          }
        }
      });
    } else {
      sendToast(R.string.no_members_in_the_selected_cell);
    }
  }

  private static void sendAlertToPublicCell(final Activity context, final String title,
                                            final String additionalText, final byte[] finalB,
                                            final ParseFile finalFile) {
    final boolean isDispatchModeEnabled =
      Singleton.INSTANCE.getAppPrefs().getBoolean("DispatchMode", false);
    final String name = ParseUser.getCurrentUser().get("firstName") + " " +
                          ParseUser.getCurrentUser().get("lastName");
    // send alert here
    HashMap<String, Object> params = new HashMap<>();
    params.put("name", name);
    params.put("issuerId", ParseUser.getCurrentUser().getObjectId());
    params.put("alertType", title);
    params.put("additionalNote", additionalText);
    params.put("cellObjectId", selectedPublicCell.cell.getObjectId());
    params.put("cellName", selectedPublicCell.name);
    if(title.equals("Photo")) {
      params.put("isPhotoAlert", true);
      params.put("imageBytes", finalB);
    } else {
      params.put("isPhotoAlert", false);
    }
    if(isDispatchModeEnabled && !title.equals("Photo")) {
      params.put("dispatchMode", 1);
      params.put("lat", Singleton.INSTANCE.getCustomLocation().latitude);
      params.put("lng", Singleton.INSTANCE.getCustomLocation().longitude);
    } else {
      params.put("dispatchMode", 2);
      params.put("lat", Singleton.INSTANCE.getLatitude());
      params.put("lng", Singleton.INSTANCE.getLongitude());
    }
    params.put("clientFirmId", 1);
    params.put("isLive", context.getResources().getBoolean(R.bool.enable_publish));
    params.put("languageCode", context.getString(R.string.language_code));
    params.put("platform", "android");
    final JSONObject objLMA = new JSONObject();
    try {
      objLMA.put(LMA.PARAM_USER_ID, ParseUser.getCurrentUser().getObjectId());
      objLMA.put(LMA.PARAM_ALERT_TYPE, title);
      if(additionalText == null) {
        objLMA.put(LMA.PARAM_NOTE, "");
      } else {
        objLMA.put(LMA.PARAM_NOTE, additionalText);
      }
      objLMA.put(LMA.PARAM_GEO_LOCATION,
        Singleton.INSTANCE.getLatitude() + "," + Singleton.INSTANCE.getLongitude());
    } catch(JSONException e1) {
      e1.printStackTrace();
    }
    ParseCloud.callFunctionInBackground("sendAlert", params, new FunctionCallback<String>() {
      public void done(String result, ParseException e) {
        if(e == null) {
          LogEvent.Log(TAG, "result: " + result);
          try {
            JSONObject obj = new JSONObject(result);
            String cell411AlertId = obj.getString("cell411AlertId");
            long createdAt = obj.getLong("createdAt");
            // Check if the feature is enabled in the build configuration
            String photoUrl = null;
            if(obj.has("photoUrl")) {
              photoUrl = obj.getString("photoUrl");
            }
            // Make an API call to LMA server for the issued alert
            if(Singleton.INSTANCE.getAppPrefs().getBoolean("PublishAlertToFacebook", false)) {
              int dispatchMode = 0;
              if(isDispatchModeEnabled && !title.equals("Photo")) {
                dispatchMode = 1;
              }
            }
          } catch(JSONException e1) {
            e1.printStackTrace();
          }
          // Alert sent successfully
          if(Modules.isWeakReferenceValid()) {
            context.getApplicationContext();
            sendToast(R.string.alert_sent_successfully);
          }
        } else {
          if(Modules.isWeakReferenceValid()) {
            context.getApplicationContext();
            sendToast(e);
          }
        }
      }
    });
  }

  private static void forwardAlert(final Activity context, final String title,
                                   final ReceivedCell411Alert receivedCell411Alert,
                                   final ParseUser parseUser) {
    // Check if the feature is enabled in the build configuration
    ParseQuery<ParseObject> cell411Query = ParseQuery.getQuery("Cell411Alert");
    cell411Query.whereEqualTo("objectId", receivedCell411Alert.cell411AlertId);
    cell411Query.getFirstInBackground(new GetCallback<ParseObject>() {
      @Override
      public void done(final ParseObject parseObjectCell411Alert, ParseException e) {
        if(e == null) {
          //LogEvent.Log(TAG, "showForwardAlertDialog issuerName2: " + issuerName);
          if(selectedCell.parseObject == null && selectedCell.name.equals("GLOBAL ALERT")) {
            ParseUser.getCurrentUser().fetchInBackground(new GetCallback<ParseObject>() {
              @Override
              public void done(ParseObject parseObject, ParseException e) {
                if(e == null) {
                  String privilege = (String) ParseUser.getCurrentUser().get("privilege");
                  if(privilege == null || privilege.equals("")) {
                    // check if user has added at least two friends and has issued an alert to newPrivateCell
                    // if the above condition is met then he/she can issue alerts globally
                    ParseUser user = ParseUser.getCurrentUser();
                    ParseRelation relFriends = user.getRelation("friends");
                    ParseQuery query4Friends = relFriends.getQuery();
                    query4Friends.countInBackground(new CountCallback() {
                      @Override
                      public void done(int count, ParseException e) {
                        if(e == null) {
                          // The count request succeeded. Log the count
                          LogEvent.Log(TAG, "Total Friends: " + count);
                          if(count >= 2) {
                            ParseQuery<ParseObject> parseQuery4SelfCell411Alerts =
                              ParseQuery.getQuery("Cell411Alert");
                            parseQuery4SelfCell411Alerts.whereEqualTo("issuedBy",
                              ParseUser.getCurrentUser());
                            parseQuery4SelfCell411Alerts.whereDoesNotExist("cellName");
                            parseQuery4SelfCell411Alerts.whereNotEqualTo("isGlobal", 1);
                            parseQuery4SelfCell411Alerts.whereDoesNotExist("entryFor");
                            parseQuery4SelfCell411Alerts.whereDoesNotExist("to");
                            parseQuery4SelfCell411Alerts.whereDoesNotExist("cellId");
                            parseQuery4SelfCell411Alerts.whereExists("alertType");
                            parseQuery4SelfCell411Alerts.whereExists("targetMembers");
                            ParseQuery<ParseObject> parseQuery4ForwardedCell411Alerts =
                              ParseQuery.getQuery("Cell411Alert");
                            parseQuery4ForwardedCell411Alerts.whereEqualTo("forwardedBy",
                              ParseUser.getCurrentUser());
                            parseQuery4ForwardedCell411Alerts.whereExists("forwardedToMembers");
                            parseQuery4ForwardedCell411Alerts.whereExists("forwardedAlert");
                            parseQuery4ForwardedCell411Alerts.whereNotEqualTo("isGlobal", 1);
                            parseQuery4ForwardedCell411Alerts.whereDoesNotExist("cellId");
                            parseQuery4ForwardedCell411Alerts.whereDoesNotExist("cellName");
                            List<ParseQuery<ParseObject>> queries = new ArrayList<>();
                            queries.add(parseQuery4SelfCell411Alerts);
                            queries.add(parseQuery4ForwardedCell411Alerts);
                            ParseQuery<ParseObject> mainQuery = ParseQuery.or(queries);
                            mainQuery.countInBackground(new CountCallback() {
                              @Override
                              public void done(int count, ParseException e) {
                                if(e == null) {
                                  // The count request succeeded. Log the count
                                  LogEvent.Log(TAG, "Total Alerts: " + count);
                                  if(count >= 1) {
                                    ParseUser parseUser = ParseUser.getCurrentUser();
                                    parseUser.put("privilege", "SECOND");
                                    parseUser.saveInBackground();
                                    // Forward alert globally
                                    sendAlertV2(context, title, null, selectedCell,
                                      receivedCell411Alert._additionalNote, receivedCell411Alert,
                                      true, false, true);
                                    // OLD CODE
                                    //sendForwardAlertGlobally(context, parseUser, receivedCell411Alert, parseObjectCell411Alert);
                                  } else {
                                    ParseUser parseUser = ParseUser.getCurrentUser();
                                    parseUser.put("privilege", "FIRST");
                                    parseUser.saveInBackground();
                                    PrivilegeModules.showUnfulfilledPrivilegeAlert(context);
                                  }
                                } else {
                                  // The request failed
                                  if(Modules.isWeakReferenceValid()) {
                                    context.getApplicationContext();
                                    sendToast(R.string.network_error);
                                  }
                                }
                              }
                            });
                          } else {
                            ParseUser parseUser = ParseUser.getCurrentUser();
                            parseUser.put("privilege", "FIRST");
                            parseUser.saveInBackground();
                            PrivilegeModules.showUnfulfilledPrivilegeAlert(context);
                          }
                        } else {
                          // The request failed
                          if(Modules.isWeakReferenceValid()) {
                            context.getApplicationContext();
                            sendToast(R.string.network_error);
                          }
                        }
                      }
                    });
                  } else if(privilege.equals("FIRST")) {
                    PrivilegeModules.showUnfulfilledPrivilegeAlert(context);
                  } else if(privilege.equals("BANNED")) {
                    // user is permanently banned
                    PrivilegeModules.logoutUser();
                  } else if(privilege.contains("SUSPENDED")) {
                    // blocked user temporarily
                    PrivilegeModules.logoutUser();
                  } /*else if (privilege.startsWith("SUSPENDED")) {
                                        // blocked for a predefined period of time
                                    }*/ else if(privilege.equals("SHADOW_BANNED")) {
                    // don't tell user he/she is blocked, but the alerts they
                    // issue should not go out
                  } else if(privilege.equals("SECOND")) {
                    // Forward alert globally
                    sendAlertV2(context, title, null, selectedCell,
                      receivedCell411Alert._additionalNote, receivedCell411Alert, true, false,
                      true);
                    // OLD CODE
                    //sendForwardAlertGlobally(context, parseUser, receivedCell411Alert, parseObjectCell411Alert);
                  } else {
                    // Some new privilege is added
                    // Forward alert globally
                    sendAlertV2(context, title, null, selectedCell,
                      receivedCell411Alert._additionalNote, receivedCell411Alert, true, false,
                      true);
                    // OLD CODE
                    //sendForwardAlertGlobally(context, parseUser, receivedCell411Alert, parseObjectCell411Alert);
                  }
                } else {
                  // The request failed
                  if(Modules.isWeakReferenceValid()) {
                    context.getApplicationContext();
                    sendToast(R.string.network_error);
                  }
                }
              }
            });
          } else {
            if(selectedCell.parseObject == null) {
              // Send alert to all friends
              final ArrayList<ParseUser> parseUsersList = new ArrayList<>();
              if(Singleton.INSTANCE.getFriends() != null) {
                for(int i = 0; i < Singleton.INSTANCE.getFriends().size(); i++) {
                  parseUsersList.add(Singleton.INSTANCE.getFriends().get(i).user);
                }
                ParseRelation relation = ParseUser.getCurrentUser().getRelation("spammedBy");
                final ParseQuery parseQuery = relation.getQuery();
                parseQuery.findInBackground(new FindCallback<ParseObject>() {
                  @Override
                  public void done(List list, ParseException e) {
                    if(e == null) {
                      if(list != null && list.size() > 0) {
                        for(int i = 0; i < parseUsersList.size(); i++) {
                          for(int j = 0; j < list.size(); j++) {
                            if(parseUsersList.get(i).getObjectId().equals(
                              ((ParseUser) list.get(j)).getObjectId())) {
                              parseUsersList.remove(i);
                              --i;
                              break;
                            }
                          }
                        }
                      }
                      sendAlertV2(context, title, null, selectedCell,
                        receivedCell411Alert._additionalNote, receivedCell411Alert, false, false,
                        true);
                      // OLD CODE
                      //sendForwardedAlert(context, parseUser, receivedCell411Alert,
                      //        parseUsersList, parseObjectCell411Alert, 0);
                    } else {
                      if(Modules.isWeakReferenceValid()) {
                        context.getApplicationContext();
                        sendToast(e);
                      }
                    }
                  }
                });
              } else {
                ParseUser user = ParseUser.getCurrentUser();
                ParseRelation relFriends = user.getRelation("friends");
                ParseQuery query4Friends = relFriends.getQuery();
                ParseRelation relation = user.getRelation("spammedBy");
                ParseQuery parseQuery = relation.getQuery();
                query4Friends.whereDoesNotMatchKeyInQuery("objectId", "objectId", parseQuery);
                query4Friends.findInBackground(new FindCallback<ParseObject>() {
                  @Override
                  public void done(List list, ParseException e) {
                    if(e == null) {
                      if(list != null && list.size() > 0) {
                        for(int i = 0; i < list.size(); i++) {
                          ParseUser user = (ParseUser) list.get(i);
                          parseUsersList.add(user);
                        }
                      }
                      sendAlertV2(context, title, null, selectedCell,
                        receivedCell411Alert._additionalNote, receivedCell411Alert, false, false,
                        true);
                      // OLD CODE
                      //sendForwardedAlert(context, parseUser, receivedCell411Alert,
                      //        parseUsersList, parseObjectCell411Alert, 0);
                    } else {
                      if(Modules.isWeakReferenceValid()) {
                        context.getApplicationContext();
                        sendToast(e);
                      }
                    }
                  }
                });
              }
            } else {
              // Send alert to selected cells
              final ArrayList<ParseUser> parseUsersList = new ArrayList<>();
              parseUsersList.addAll(selectedCell.members);
              ParseRelation relation = ParseUser.getCurrentUser().getRelation("spammedBy");
              ParseQuery parseQuery = relation.getQuery();
              parseQuery.findInBackground(new FindCallback<ParseObject>() {
                @Override
                public void done(List list, ParseException e) {
                  if(e == null) {
                    if(list != null && list.size() > 0) {
                      for(int i = 0; i < parseUsersList.size(); i++) {
                        for(int j = 0; j < list.size(); j++) {
                          if(parseUsersList.get(i).getObjectId().equals(
                            ((ParseUser) list.get(j)).getObjectId())) {
                            parseUsersList.remove(i);
                            --i;
                            break;
                          }
                        }
                      }
                    }
                    sendAlertV2(context, title, null, selectedCell,
                      receivedCell411Alert._additionalNote, receivedCell411Alert, false, false,
                      true);
                  } else {
                    if(Modules.isWeakReferenceValid()) {
                      context.getApplicationContext();
                      sendToast(e);
                    }
                  }
                }
              });
            }
          }
        } else {
          if(Modules.isWeakReferenceValid()) {
            context.getApplicationContext();
            sendToast(e);
          }
        }
      }
    });
  }

  private static void sendForwardedAlert(final Activity context, final ParseUser parseUser,
                                         final ReceivedCell411Alert receivedCell411Alert,
                                         final ArrayList<ParseUser> parseUsersList,
                                         final ParseObject parseObjectCell411Alert,
                                         final int isGlobal) {
    if(parseUsersList.size() > 0) {
      final String issuerName =
        receivedCell411Alert.issuerFirstName + " " + receivedCell411Alert.issuerLastName;
      final String forwardedBy = ParseUser.getCurrentUser().get("firstName") + " " +
                                   ParseUser.getCurrentUser().get("lastName");
      final ParseObject cell411AlertObject = new ParseObject("Cell411Alert");
      cell411AlertObject.put("forwardedBy", ParseUser.getCurrentUser());
      cell411AlertObject.put("forwardedAlert", parseObjectCell411Alert);
      cell411AlertObject.put("issuedBy", parseUser);
      cell411AlertObject.put("issuerFirstName", issuerName);
      cell411AlertObject.put("issuerId", parseUser.getObjectId());
      cell411AlertObject.put("alertType", convertEnumToAlertKey(receivedCell411Alert.alertType));
      cell411AlertObject.put("additionalNote", receivedCell411Alert._additionalNote);
      cell411AlertObject.put("dispatchMode", receivedCell411Alert._dispatchMode);
      cell411AlertObject.put("forwardedToMembers", parseUsersList);
      cell411AlertObject.put("location",
        new ParseGeoPoint(receivedCell411Alert.lat, receivedCell411Alert.lon));
      if(isGlobal == 1) {
        cell411AlertObject.put("isGlobal", 1);
      }
      cell411AlertObject.saveInBackground(new SaveCallback() {
        @Override
        public void done(ParseException e) {
          if(e == null) {
            // Check if the feature is enabled in the build configuration
            // Check the privilege and update it, if required
            PrivilegeModules.checkAndUpdatePrivilege(false);
            ParseQuery pushQuery = ParseInstallation.getQuery();
            pushQuery.whereContainedIn("user", parseUsersList);
            pushQuery.setLimit(1000);
            ParsePush push = new ParsePush();
            push.setQuery(pushQuery);
            JSONObject data = new JSONObject();
            try {
              data.put("alert",
                context.getString(R.string.notif_forwarded_an_emergency_alert, forwardedBy));
              data.put("alertRegarding", convertEnumToAlertKey(receivedCell411Alert.alertType));
              data.put("userId", parseUser.getObjectId());
              data.put("sound", "default");
              data.put("cell411AlertId", receivedCell411Alert.cell411AlertId);
              data.put("lat", receivedCell411Alert.lat);
              data.put("lon", receivedCell411Alert.lon);
              data.put("additionalNote", receivedCell411Alert._additionalNote);
              data.put("createdAt", receivedCell411Alert.createdAt);
              data.put("forwardedBy", forwardedBy);
              data.put("firstName", issuerName);
              data.put("alertType", "NEEDY_FORWARDED");
              data.put("dispatchMode", receivedCell411Alert._dispatchMode);
              if(isGlobal == 1) {
                data.put("isGlobal", 1);
              }
              data.put("badge", "Increment");
            } catch(JSONException e1) {
              e1.printStackTrace();
            }
            push.setData(data);
            push.sendInBackground();
            if(receivedCell411Alert.alertType == Cell411Alert.AlertType.MEDICAL) {
              String message = "";
              ParseUser user = ParseUser.getCurrentUser();
              String bloodType = (String) user.get("bloodType");
              String allergies = (String) user.get("allergies");
              String otherMedicalConditions = (String) user.get("otherMedicalConditions");
              if(bloodType != null && !bloodType.isEmpty()) {
                message +=
                  "\n" + context.getString(R.string.medical_info_blood_type) + " " + bloodType +
                    "\n";
              }
              if(allergies != null && !allergies.isEmpty()) {
                message +=
                  "\n" + context.getString(R.string.medical_info_allergies) + " " + allergies +
                    "\n";
              }
              if(otherMedicalConditions != null && !otherMedicalConditions.isEmpty()) {
                message +=
                  "\n" + context.getString(R.string.medical_info_other_medical_conditions) + " " +
                    otherMedicalConditions + "\n";
              }
              if(!message.isEmpty()) {
                showMedicalDetailsDialog(context, message);
              }
            }
            if(Modules.isWeakReferenceValid()) {
              context.getApplicationContext();
              sendToast(context.getString(R.string.toast_alert_sent) + " " + selectedCell.name);
            }
          } else {
            if(Modules.isWeakReferenceValid()) {
              context.getApplicationContext();
              sendToast(e);
            }
          }
        }
      });
    } else {
      sendToast(R.string.no_members_in_the_selected_cell);
    }
  }

  public static void sendPanicAlert(final Context context, final String title) {
    // Check if the feature is enabled in the build configuration
    SharedPreferences prefs = Singleton.INSTANCE.getAppPrefs();
    boolean panicAlertToAllFriends = prefs.getBoolean("PanicAlertToAllFriends", true);
    boolean panicAlertToNearBy = prefs.getBoolean("PanicAlertToNearBy", true);
    boolean panicAlertToPrivateCells = prefs.getBoolean("PanicAlertToPrivateCells", false);
    boolean panicAlertToNAUCells = prefs.getBoolean("PanicAlertToNAUCells", false);
    boolean panicAlertToPublicCells = prefs.getBoolean("PanicAlertToPublicCells", false);
    final String additionalText = prefs.getString("AdditionalNote", "");
    final String name = ParseUser.getCurrentUser().get("firstName") + " " +
                          ParseUser.getCurrentUser().get("lastName");
    if(panicAlertToAllFriends) {
      final ArrayList<ParseUser> parseUsersList = new ArrayList<>();
      // Send alert to all friends
      if(Singleton.INSTANCE.getFriends() != null) {
        for(int i = 0; i < Singleton.INSTANCE.getFriends().size(); i++) {
          parseUsersList.add(Singleton.INSTANCE.getFriends().get(i).user);
        }
        ParseRelation relation = ParseUser.getCurrentUser().getRelation("spammedBy");
        ParseQuery parseQuery = relation.getQuery();
        parseQuery.findInBackground(new FindCallback<ParseObject>() {
          @Override
          public void done(List list, ParseException e) {
            if(e == null) {
              if(list != null && list.size() > 0) {
                for(int i = 0; i < parseUsersList.size(); i++) {
                  for(int j = 0; j < list.size(); j++) {
                    if(parseUsersList.get(i).getObjectId().equals(
                      ((ParseUser) list.get(j)).getObjectId())) {
                      parseUsersList.remove(i);
                      --i;
                      break;
                    }
                  }
                }
              }
              if(parseUsersList.size() > 0) {
                final ParseObject cell411AlertObject = new ParseObject("Cell411Alert");
                cell411AlertObject.put("issuedBy", ParseUser.getCurrentUser());
                cell411AlertObject.put("issuerFirstName", name);
                cell411AlertObject.put("issuerId", ParseUser.getCurrentUser().getObjectId());
                cell411AlertObject.put("alertType", title);
                cell411AlertObject.put("additionalNote", additionalText);
                cell411AlertObject.put("targetMembers", parseUsersList);
                cell411AlertObject.put("location",
                  new ParseGeoPoint(Singleton.INSTANCE.getLatitude(),
                    Singleton.INSTANCE.getLongitude()));
                cell411AlertObject.saveInBackground(new SaveCallback() {
                  @Override
                  public void done(ParseException e) {
                    if(e == null) {
                      // Check the privilege and update it, if required
                      PrivilegeModules.checkAndUpdatePrivilege(false);
                      ParseQuery pushQuery = ParseInstallation.getQuery();
                      pushQuery.whereContainedIn("user", parseUsersList);
                      pushQuery.setLimit(1000);
                      ParsePush push = new ParsePush();
                      push.setQuery(pushQuery);
                      JSONObject data = new JSONObject();
                      try {
                        if(title.equals("Panic")) {
                          data.put("alert",
                            context.getString(R.string.notif_issued_panic_alert, name));
                        } else {
                          data.put("alert",
                            context.getString(R.string.notif_issued_fallen_alert, name));
                        }
                        data.put("alertRegarding", title);
                        data.put("userId", ParseUser.getCurrentUser().getObjectId());
                        data.put("sound", "default");
                        data.put("cell411AlertId", cell411AlertObject.getObjectId());
                        data.put("lat", Singleton.INSTANCE.getLatitude());
                        data.put("lon", Singleton.INSTANCE.getLongitude());
                        data.put("additionalNote", additionalText);
                        data.put("createdAt", cell411AlertObject.getCreatedAt().getTime());
                        data.put("firstName", name);
                        data.put("alertType", "NEEDY");
                        data.put("badge", "Increment");
                      } catch(JSONException e1) {
                        e1.printStackTrace();
                      }
                      push.setData(data);
                      push.sendInBackground();
                      if(Modules.isWeakReferenceValid()) {
                        context.getApplicationContext();
                        sendToast(context.getString(R.string.toast_alert_sent_to_friends,
                          parseUsersList.size()));
                      }
                    } else {
                      if(Modules.isWeakReferenceValid()) {
                        context.getApplicationContext();
                        sendToast(e);
                      }
                    }
                  }
                });
              }
            } else {
              if(Modules.isWeakReferenceValid()) {
                context.getApplicationContext();
                sendToast(e);
              }
            }
          }
        });
      } else {
        ParseUser user = ParseUser.getCurrentUser();
        ParseRelation relFriends = user.getRelation("friends");
        ParseQuery query4Friends = relFriends.getQuery();
        ParseRelation relation = user.getRelation("spammedBy");
        ParseQuery parseQuery = relation.getQuery();
        query4Friends.whereDoesNotMatchKeyInQuery("objectId", "objectId", parseQuery);
        query4Friends.findInBackground(new FindCallback<ParseObject>() {
          @Override
          public void done(List list, ParseException e) {
            if(e == null) {
              if(list != null && list.size() > 0) {
                for(int i = 0; i < list.size(); i++) {
                  ParseUser user = (ParseUser) list.get(i);
                  parseUsersList.add(user);
                }
              }
              if(parseUsersList.size() > 0) {
                final ParseObject cell411AlertObject = new ParseObject("Cell411Alert");
                cell411AlertObject.put("issuedBy", ParseUser.getCurrentUser());
                cell411AlertObject.put("issuerFirstName", name);
                cell411AlertObject.put("issuerId", ParseUser.getCurrentUser().getObjectId());
                cell411AlertObject.put("alertType", title);
                cell411AlertObject.put("additionalNote", additionalText);
                cell411AlertObject.put("targetMembers", parseUsersList);
                cell411AlertObject.put("location",
                  new ParseGeoPoint(Singleton.INSTANCE.getLatitude(),
                    Singleton.INSTANCE.getLongitude()));
                cell411AlertObject.saveInBackground(new SaveCallback() {
                  @Override
                  public void done(ParseException e) {
                    if(e == null) {
                      // Check the privilege and update it, if required
                      PrivilegeModules.checkAndUpdatePrivilege(false);
                      ParseQuery pushQuery = ParseInstallation.getQuery();
                      pushQuery.whereContainedIn("user", parseUsersList);
                      pushQuery.setLimit(1000);
                      ParsePush push = new ParsePush();
                      push.setQuery(pushQuery);
                      JSONObject data = new JSONObject();
                      try {
                        if(title.equals("Panic")) {
                          data.put("alert",
                            context.getString(R.string.notif_issued_panic_alert, name));
                        } else {
                          data.put("alert",
                            context.getString(R.string.notif_issued_fallen_alert, name));
                        }
                        data.put("alertRegarding", title);
                        data.put("userId", ParseUser.getCurrentUser().getObjectId());
                        data.put("sound", "default");
                        data.put("cell411AlertId", cell411AlertObject.getObjectId());
                        data.put("lat", Singleton.INSTANCE.getLatitude());
                        data.put("lon", Singleton.INSTANCE.getLongitude());
                        data.put("additionalNote", additionalText);
                        data.put("createdAt", cell411AlertObject.getCreatedAt().getTime());
                        data.put("firstName", name);
                        data.put("alertType", "NEEDY");
                        data.put("badge", "Increment");
                      } catch(JSONException e1) {
                        e1.printStackTrace();
                      }
                      push.setData(data);
                      push.sendInBackground();
                      if(Modules.isWeakReferenceValid()) {
                        context.getApplicationContext();
                        sendToast(context.getString(R.string.toast_alert_sent_to_friends,
                          parseUsersList.size()));
                      }
                    } else {
                      if(Modules.isWeakReferenceValid()) {
                        context.getApplicationContext();
                        sendToast(e);
                      }
                    }
                  }
                });
              }
            } else {
              if(Modules.isWeakReferenceValid()) {
                context.getApplicationContext();
                sendToast(e);
              }
            }
          }
        });
      }
    } else if(panicAlertToPrivateCells) {
      final ArrayList<ParseUser> parseUsersList = new ArrayList<>();
      final JSONArray nauMemberJSONArray = new JSONArray();
      ArrayList<PrivateCell> privateCells = StorageOperations.
                                                               getPrivateCellsForPanicAlert(
                                                                 Singleton.INSTANCE);
      if(privateCells != null && privateCells.size() > 0) {
        ArrayList<String> objectIdList = new ArrayList<>();
        for(int i = 0; i < privateCells.size(); i++) {
          String objectId = privateCells.get(i).getObjectId();
          objectIdList.add(objectId);
        }
        ParseQuery<ParseObject> query = new ParseQuery<>("Cell");
        query.whereContainedIn("objectId", objectIdList);
        query.include("members");
        query.include("nauMembers");
        query.findInBackground(new FindCallback<ParseObject>() {
          @Override
          public void done(List<ParseObject> list, ParseException e) {
            if(e == null) {
              if(list != null && list.size() > 0) {
                for(int i = 0; i < list.size(); i++) {
                  ParseObject cell = list.get(i);
                  List<ParseUser> members = cell.getList("members");
                  parseUsersList.addAll(members);
                  if(cell.getJSONArray("nauMembers") != null &&
                       cell.getJSONArray("nauMembers").length() > 0) {
                    for(int j = 0; j < cell.getJSONArray("nauMembers").length(); j++) {
                      try {
                        nauMemberJSONArray.put(cell.getJSONArray("nauMembers").get(j));
                      } catch(JSONException e1) {
                        e1.printStackTrace();
                      }
                    }
                    LogEvent.Log(TAG, "result: " + cell.getString("name") + " has nauMembers: " +
                                        cell.getJSONArray("nauMembers").length());
                    LogEvent.Log(TAG,
                      "result: nauMemberJSONArray.length(): " + nauMemberJSONArray.length());
                  } else {
                    LogEvent.Log(TAG, "result: " + cell.getString("name") + " has no nauMembers");
                  }
                }
                ParseRelation relation = ParseUser.getCurrentUser().getRelation("spammedBy");
                ParseQuery parseQuery = relation.getQuery();
                parseQuery.findInBackground(new FindCallback<ParseObject>() {
                  @Override
                  public void done(List list, ParseException e) {
                    if(e == null) {
                      if(list != null && list.size() > 0) {
                        for(int i = 0; i < parseUsersList.size(); i++) {
                          for(int j = 0; j < list.size(); j++) {
                            if(parseUsersList.get(i).getObjectId().equals(
                              ((ParseUser) list.get(j)).getObjectId())) {
                              parseUsersList.remove(i);
                              --i;
                              break;
                            }
                          }
                        }
                      }
                      if(parseUsersList.size() > 0) {
                        final ParseObject cell411AlertObject = new ParseObject("Cell411Alert");
                        cell411AlertObject.put("issuedBy", ParseUser.getCurrentUser());
                        cell411AlertObject.put("issuerFirstName", name);
                        cell411AlertObject.put("issuerId",
                          ParseUser.getCurrentUser().getObjectId());
                        cell411AlertObject.put("alertType", title);
                        cell411AlertObject.put("additionalNote", additionalText);
                        cell411AlertObject.put("targetMembers", parseUsersList);
                        cell411AlertObject.put("targetNAUMembers", nauMemberJSONArray);
                        cell411AlertObject.put("location",
                          new ParseGeoPoint(Singleton.INSTANCE.getLatitude(),
                            Singleton.INSTANCE.getLongitude()));
                        cell411AlertObject.saveInBackground(new SaveCallback() {
                          @Override
                          public void done(ParseException e) {
                            if(e == null) {
                              // Check the privilege and update it, if required
                              PrivilegeModules.checkAndUpdatePrivilege(false);
                              ParseQuery pushQuery = ParseInstallation.getQuery();
                              pushQuery.whereContainedIn("user", parseUsersList);
                              pushQuery.setLimit(1000);
                              ParsePush push = new ParsePush();
                              push.setQuery(pushQuery);
                              JSONObject data = new JSONObject();
                              try {
                                if(title.equals("Panic")) {
                                  data.put("alert",
                                    context.getString(R.string.notif_issued_panic_alert, name));
                                } else {
                                  data.put("alert",
                                    context.getString(R.string.notif_issued_fallen_alert, name));
                                }
                                data.put("alertRegarding", title);
                                data.put("userId", ParseUser.getCurrentUser().getObjectId());
                                data.put("sound", "default");
                                data.put("cell411AlertId", cell411AlertObject.getObjectId());
                                data.put("lat", Singleton.INSTANCE.getLatitude());
                                data.put("lon", Singleton.INSTANCE.getLongitude());
                                data.put("additionalNote", additionalText);
                                data.put("createdAt", cell411AlertObject.getCreatedAt().getTime());
                                data.put("firstName", name);
                                data.put("alertType", "NEEDY");
                                data.put("badge", "Increment");
                              } catch(JSONException e1) {
                                e1.printStackTrace();
                              }
                              push.setData(data);
                              push.sendInBackground();
                              // send sms/email alert if NAU is enabled
                              LogEvent.Log(TAG, "result: cloud function not invoked");
                              if(Modules.isWeakReferenceValid()) {
                                context.getApplicationContext();
                                sendToast(
                                  context.getString(R.string.toast_alert_sent_to_private_cell,
                                    parseUsersList.size()));
                              }
                            } else {
                              if(Modules.isWeakReferenceValid()) {
                                context.getApplicationContext();
                                sendToast(e);
                              }
                            }
                          }
                        });
                      }
                    } else {
                      if(Modules.isWeakReferenceValid()) {
                        context.getApplicationContext();
                        sendToast(e);
                      }
                    }
                  }
                });
              }
            } else {
              if(Modules.isWeakReferenceValid()) {
                context.getApplicationContext();
                sendToast(e);
              }
            }
          }
        });
      }
    }
    if(panicAlertToNearBy) {
      final ArrayList<ParseUser> parseUsersList = new ArrayList<>();
      // Send alert globally
      final String metric = Singleton.INSTANCE.getAppPrefs().getString("metric", "kms");
      ParseGeoPoint userLocation =
        new ParseGeoPoint(Singleton.INSTANCE.getLatitude(), Singleton.INSTANCE.getLongitude());
      final int patrolModeRadius;
      ParseQuery<ParseUser> query = ParseUser.getQuery();
      query.whereEqualTo("PatrolMode", 1);
      query.whereNotEqualTo("objectId", ParseUser.getCurrentUser().getObjectId());
      if(metric.equals("kms")) {
        patrolModeRadius = Singleton.INSTANCE.getAppPrefs().getInt("PatrolModeRadius", 80);
        query.whereWithinKilometers("location", userLocation, patrolModeRadius);
      } else {
        patrolModeRadius = Singleton.INSTANCE.getAppPrefs().getInt("PatrolModeRadius", 50);
        query.whereWithinMiles("location", userLocation, patrolModeRadius);
      }
      ParseUser user = ParseUser.getCurrentUser();
      ParseRelation relation = user.getRelation("spammedBy");
      ParseQuery parseQuery = relation.getQuery();
      query.whereDoesNotMatchKeyInQuery("objectId", "objectId", parseQuery);
      query.findInBackground(new FindCallback<ParseUser>() {
        @Override
        public void done(List<ParseUser> parseUsers, ParseException e) {
          if(e == null) {
            if(parseUsers != null && parseUsers.size() > 0) {
              for(int i = 0; i < parseUsers.size(); i++) {
                ParseUser user = parseUsers.get(i);
                parseUsersList.add(user);
              }
              final ParseObject cell411AlertObject = new ParseObject("Cell411Alert");
              cell411AlertObject.put("issuedBy", ParseUser.getCurrentUser());
              cell411AlertObject.put("issuerFirstName", name);
              cell411AlertObject.put("issuerId", ParseUser.getCurrentUser().getObjectId());
              cell411AlertObject.put("alertType", title);
              cell411AlertObject.put("additionalNote", additionalText);
              cell411AlertObject.put("targetMembers", parseUsersList);
              cell411AlertObject.put("isGlobal", 1);
              cell411AlertObject.put("location", new ParseGeoPoint(Singleton.INSTANCE.getLatitude(),
                Singleton.INSTANCE.getLongitude()));
              cell411AlertObject.saveInBackground(new SaveCallback() {
                @Override
                public void done(ParseException e) {
                  if(e == null) {
                    ParseQuery pushQuery = ParseInstallation.getQuery();
                    pushQuery.whereContainedIn("user", parseUsersList);
                    pushQuery.setLimit(1000);
                    ParsePush push = new ParsePush();
                    push.setQuery(pushQuery);
                    JSONObject data = new JSONObject();
                    try {
                      if(title.equals("Panic")) {
                        data.put("alert",
                          context.getString(R.string.notif_issued_panic_alert, name));
                      } else {
                        data.put("alert",
                          context.getString(R.string.notif_issued_fallen_alert, name));
                      }
                      data.put("alertRegarding", title);
                      data.put("userId", ParseUser.getCurrentUser().getObjectId());
                      data.put("sound", "default");
                      data.put("cell411AlertId", cell411AlertObject.getObjectId());
                      data.put("lat", Singleton.INSTANCE.getLatitude());
                      data.put("lon", Singleton.INSTANCE.getLongitude());
                      data.put("additionalNote", additionalText);
                      data.put("createdAt", cell411AlertObject.getCreatedAt().getTime());
                      data.put("firstName", name);
                      data.put("isGlobal", 1);
                      data.put("alertType", "NEEDY");
                      data.put("badge", "Increment");
                    } catch(JSONException e1) {
                      e1.printStackTrace();
                    }
                    push.setData(data);
                    push.sendInBackground();
                    if(Modules.isWeakReferenceValid()) {
                      context.getApplicationContext();
                      sendToast(context.getString(R.string.toast_alert_sent_to_near_by,
                        parseUsersList.size()));
                    }
                  } else {
                    if(Modules.isWeakReferenceValid()) {
                      context.getApplicationContext();
                      sendToast(e);
                    }
                  }
                }
              });
            } else {
              if(Modules.isWeakReferenceValid()) {
                if(metric.equals("kms")) {
                  context.getApplicationContext();
                  sendToast(
                    context.getString(R.string.toast_msg_no_one_found_having_patrol_mode_enabled,
                      patrolModeRadius, context.getString(R.string.kilometers)));
                } else {
                  context.getApplicationContext();
                  sendToast(
                    context.getString(R.string.toast_msg_no_one_found_having_patrol_mode_enabled,
                      patrolModeRadius, context.getString(R.string.miles)));
                }
              }
            }
          } else {
            if(Modules.isWeakReferenceValid()) {
              context.getApplicationContext();
              sendToast(e);
            }
          }
        }
      });
    }
    if(panicAlertToPublicCells) {
      final ArrayList<PublicCell> publicCells = StorageOperations.
                                                                   getPublicCellsForPanicAlert(
                                                                     Singleton.INSTANCE);
      if(publicCells != null && publicCells.size() > 0) {
        for(int i = 0; i < publicCells.size(); i++) {
          String objectId = publicCells.get(i).getObjectId();
          String cellName = publicCells.get(i).getName();
          // send alert here
          HashMap<String, Object> params = new HashMap<>();
          params.put("name", name);
          params.put("issuerId", ParseUser.getCurrentUser().getObjectId());
          params.put("alertType", title);
          params.put("additionalNote", additionalText);
          params.put("cellObjectId", objectId);
          params.put("cellName", cellName);
          params.put("isPhotoAlert", false);
          params.put("dispatchMode", 2);
          params.put("lat", Singleton.INSTANCE.getLatitude());
          params.put("lng", Singleton.INSTANCE.getLongitude());
          params.put("clientFirmId", 1);
          params.put("isLive", context.getResources().getBoolean(R.bool.enable_publish));
          params.put("languageCode", context.getString(R.string.language_code));
          params.put("platform", "android");
          final int finalI = i;
          ParseCloud.callFunctionInBackground("sendAlert", params, new FunctionCallback<String>() {
            public void done(String result, ParseException e) {
              if(e == null) {
                LogEvent.Log(TAG, "result: " + result);
                try {
                  JSONObject obj = new JSONObject(result);
                  String cell411AlertId = obj.getString("cell411AlertId");
                  long createdAt = obj.getLong("createdAt");
                  if(Modules.isWeakReferenceValid()) {
                    context.getApplicationContext();
                    sendToast(context.getString(R.string.alert_sent_to_public_cell,
                      publicCells.get(finalI).getName()));
                  }
                } catch(JSONException e1) {
                  e1.printStackTrace();
                }
              } else {
                if(Modules.isWeakReferenceValid()) {
                  context.getApplicationContext();
                  sendToast(e);
                }
              }
            }
          });
        }
      }
    }
    JSONObject obj = new JSONObject();
    try {
      obj.put(LMA.PARAM_USER_ID, ParseUser.getCurrentUser().getObjectId());
      obj.put(LMA.PARAM_ALERT_TYPE, title);
      if(additionalText == null) {
        obj.put(LMA.PARAM_NOTE, "");
      } else {
        obj.put(LMA.PARAM_NOTE, additionalText);
      }
      obj.put(LMA.PARAM_GEO_LOCATION,
        Singleton.INSTANCE.getLatitude() + "," + Singleton.INSTANCE.getLongitude());
    } catch(JSONException e1) {
      e1.printStackTrace();
    }
  }

  public static void showMedicalDetailsDialog(final Activity context, String message) {
    AlertDialog.Builder alert = new AlertDialog.Builder(context);
    alert.setTitle(R.string.dialog_title_medical_details);
    alert.setMessage(message);
    alert.setPositiveButton(R.string.dialog_btn_done, new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialogInterface, int i) {
      }
    });
    AlertDialog dialog = alert.create();
    dialog.show();
  }

  private static String convertEnumToAlertKey(Cell411Alert.AlertType alertType) {
    switch(alertType) {
      case BROKEN_CAR:
        return Alert.BROKEN_CAR;
      case BULLIED:
        return Alert.BULLIED;
      case CRIMINAL:
        return Alert.CRIMINAL;
      case DANGER:
        return Alert.DANGER;
      case FIRE:
        return Alert.FIRE;
      case GENERAL:
        return Alert.GENERAL;
      case MEDICAL:
        return Alert.MEDICAL;
      case PHOTO:
        return Alert.PHOTO;
      case POLICE_ARREST:
        return Alert.POLICE_ARREST;
      case POLICE_INTERACTION:
        return Alert.POLICE_INTERACTION;
      case PULLED_OVER:
        return Alert.PULLED_OVER;
      case VIDEO:
        return Alert.VIDEO;
      case FALLEN:
        return Alert.FALLEN;
      case PANIC:
        return Alert.PANIC;
      case HIJACK:
        return Alert.HIJACK;
      case PHYSICAL_ABUSE:
        return Alert.PHYSICAL_ABUSE;
      case TRAPPED:
        return Alert.TRAPPED;
      case CAR_ACCIDENT:
        return Alert.CAR_ACCIDENT;
      case NATURAL_DISASTER:
        return Alert.NATURAL_DISASTER;
      case PRE_AUTHORISATION:
        return Alert.PRE_AUTHORISATION;
      default:
        return Alert.UNRECOGNIZED;
    }
  }

  private static void sendAlertToSecurityGuards(final Activity context, final String title,
                                                final String additionalText,
                                                final ParseFile finalFile) {
    LogEvent.Log(TAG, "sendAlertToSecurityGuards() invoked");
    final boolean isDispatchModeEnabled =
      Singleton.INSTANCE.getAppPrefs().getBoolean("DispatchMode", false);
    final String name = ParseUser.getCurrentUser().get("firstName") + " " +
                          ParseUser.getCurrentUser().get("lastName");
    final ParseObject cell411AlertObject = new ParseObject("Cell411Alert");
    cell411AlertObject.put("issuedBy", ParseUser.getCurrentUser());
    cell411AlertObject.put("issuerFirstName", name);
    cell411AlertObject.put("issuerId", ParseUser.getCurrentUser().getObjectId());
    cell411AlertObject.put("alertType", title);
    cell411AlertObject.put("additionalNote", additionalText);
    if(title.equals("Photo")) {
      cell411AlertObject.put("photo", finalFile);
    }
    if(isDispatchModeEnabled && !title.equals("Photo")) {
      cell411AlertObject.put("location",
        new ParseGeoPoint(Singleton.INSTANCE.getCustomLocation().latitude,
          Singleton.INSTANCE.getCustomLocation().longitude));
      cell411AlertObject.put("dispatchMode", 1);
    } else {
      cell411AlertObject.put("location",
        new ParseGeoPoint(Singleton.INSTANCE.getLatitude(), Singleton.INSTANCE.getLongitude()));
    }
    cell411AlertObject.saveInBackground(new SaveCallback() {
      @Override
      public void done(ParseException e) {
        if(e == null) {
          // Make an API call to LMA server for the issued alert
          if(title.equals("Medical")) {
            String message = "";
            ParseUser user = ParseUser.getCurrentUser();
            String bloodType = (String) user.get("bloodType");
            String allergies = (String) user.get("allergies");
            String otherMedicalConditions = (String) user.get("otherMedicalConditions");
            if(bloodType != null && !bloodType.isEmpty()) {
              message +=
                "\n" + context.getString(R.string.medical_info_blood_type) + " " + bloodType +
                  "\n";
            }
            if(allergies != null && !allergies.isEmpty()) {
              message +=
                "\n" + context.getString(R.string.medical_info_allergies) + " " + allergies +
                  "\n";
            }
            if(otherMedicalConditions != null && !otherMedicalConditions.isEmpty()) {
              message +=
                "\n" + context.getString(R.string.medical_info_other_medical_conditions) + " " +
                  otherMedicalConditions + "\n";
            }
            if(!message.isEmpty()) {
              showMedicalDetailsDialog(context, message);
            }
          }
          if(!title.equals("Photo")) {
            // Alert for streaming video
            boolean isLiveStreamingEnabled =
              context.getResources().getBoolean(R.bool.is_live_streaming_enabled);
            if(isLiveStreamingEnabled) {
              showVideoStreamingAlert(context, null);
            }
          }
        } else {
          if(Modules.isWeakReferenceValid()) {
            context.getApplicationContext();
            sendToast(e);
          }
        }
      }
    });
  }

  private static class CityAndCountryReceiver extends ResultReceiver {
    private final Activity context;
    private final long createdAt;

    public CityAndCountryReceiver(Handler handler, final Activity context, final long createdAt) {
      super(handler);
      this.context = context;
      this.createdAt = createdAt;
    }

    @Override
    protected void onReceiveResult(int resultCode, Bundle resultData) {
      String city = null;
      String country = null;
      if(resultCode == Constants.SUCCESS_RESULT) {
        city = resultData.getString(Constants.RESULT_DATA_CITY);
        country = resultData.getString(Constants.RESULT_DATA_COUNTRY);
      }
      new GoLiveApiCall(context, createdAt, city, country).execute();
    }
  }

  private static class GoLiveApiCall extends AsyncTask<String, Void, Boolean> {
    private final Activity context;

    public GoLiveApiCall(final Activity context, final long createdAt, String city,
                         String country) {
      this.context = context;
    }

    @Override
    protected Boolean doInBackground(String... params) {
      return Boolean.TRUE;
    }

    @Override
    protected void onPostExecute(Boolean result) {
      super.onPostExecute(result);
      MainActivity.setSpinnerVisible(false);
      if(Modules.isWeakReferenceValid()) {
        if(result == null) {
          LogEvent.Log(TAG, "result is null");
        } else if(!result) {
          LogEvent.Log(TAG, "result is false");
        } else {
          LogEvent.Log(TAG, "result is true");
        }
        Intent intent = new Intent(context, VideoStreamingActivity.class);
        context.startActivity(intent);
      }
    }
  }

  private static class ApiCall extends AsyncTask<String, Void, Boolean> {
    private Activity context;
    private JSONObject obj;

    private ApiCall(final Activity context, JSONObject obj) {
      this.context = context;
      this.obj = obj;
    }

    @Override
    protected Boolean doInBackground(String... params) {
      LogEvent.Log(TAG, "URL: " + params[0]);
      LogEvent.Log(TAG, "param: " + obj.toString());
      okhttp3.MediaType mediaType = okhttp3.MediaType.parse("application/json; charset=utf-8");
      okhttp3.RequestBody requestBody = okhttp3.RequestBody.create(mediaType, obj.toString());
      okhttp3.Request request =
        new okhttp3.Request.Builder().url(params[0]).addHeader(LMA.PARAM_KEY,
          LMA.KEY_VALUE_ALERT).addHeader("content-type", "multipart/form-data").addHeader(
          "cache-control", "no-cache").post(requestBody).build();
      try {
        okhttp3.OkHttpClient client = new okhttp3.OkHttpClient();
        okhttp3.Response response = client.newCall(request).execute();
        if(response != null) {
          LogEvent.Log(TAG, "response code: " + String.valueOf(response.code()));
          if(response.code() == HttpsURLConnection.HTTP_OK) {
            String responseJSON = response.body().string();
            LogEvent.Log(TAG, "response JSON: " + responseJSON);
            return true;
          } else {
            LogEvent.Log(TAG, "Error");
            return false;
          }
        }
        return null;
      } catch(IOException e) {
        LogEvent.Log(TAG, "IOException: " + e.getMessage());
        return null;
      }
    }
  }

  private static class ApiCall2 extends AsyncTask<String, Void, Boolean> {
    private Activity context;
    private JSONObject obj;
    private boolean isPhotoAlert = false;
    private File sourceImageFile;
    private okhttp3.OkHttpClient client;
    private okhttp3.MediaType mediaType =
      okhttp3.MediaType.parse("application/json; charset=utf-8");

    private ApiCall2(final Activity context, JSONObject obj) {
      this.context = context;
      this.obj = obj;
    }

    private ApiCall2(final Activity context, JSONObject obj, boolean isPhotoAlert,
                     File sourceImageFile) {
      this.context = context;
      this.obj = obj;
      this.isPhotoAlert = isPhotoAlert;
      this.sourceImageFile = sourceImageFile;
    }

    @Override
    protected void onPreExecute() {
      super.onPreExecute();
      client = new okhttp3.OkHttpClient();
    }

    @Override
    protected Boolean doInBackground(String... params) {
      LogEvent.Log(TAG, "URL: " + params[0]);
      LogEvent.Log(TAG, "param: " + obj.toString());
      okhttp3.RequestBody requestBody = null;
      LogEvent.Log(TAG, "isPhotoAlert: " + isPhotoAlert);
      if(isPhotoAlert) {
        LogEvent.Log(TAG, "sourceImageFile: " + sourceImageFile.getAbsolutePath());
        if(sourceImageFile.exists()) {
          LogEvent.Log(TAG, "File exists");
        } else {
          LogEvent.Log(TAG, "File does not exists");
        }
        String path =
          context.getExternalFilesDir(null).getAbsolutePath() + "/Photo/photo_alert.png";
        Bitmap bmp = BitmapFactory.decodeFile(path);
        StorageOperations.saveImage(context, "lma.jpeg", bmp);
        File file =
          new File(context.getExternalFilesDir(null).getAbsolutePath() + "/Photo/lma.jpeg");
        OkHttpClient clientNew = new OkHttpClient();
        MediaType mediaTypeNew =
          MediaType.parse("multipart/form-data; boundary=---011000010111000001101001");
        RequestBody bodyNew = RequestBody.create(mediaTypeNew,
          "-----011000010111000001101001\r\nContent-Disposition: form-data; name=\"User_ID\"\r\n\r\nUxVyqnATHJ\r\n-----011000010111000001101001\r\nContent-Disposition: form-data; name=\"Alert_Type\"\r\n\r\nPhoto\r\n-----011000010111000001101001\r\nContent-Disposition: form-data; name=\"Geo_location\"\r\n\r\n28.628454,77.376945\r\n-----011000010111000001101001\r\nContent-Disposition: form-data; name=\"File\"; filename=\"" +
            file.getName() +
            "\"\r\nContent-Type: false\r\n\r\n\r\n-----011000010111000001101001--");
        Request requestNew =
          new Request.Builder().url("http://api.affinityhealth.co.za/api/ier/photoAlert").post(
            bodyNew).addHeader("content-type",
            "multipart/form-data; boundary=---011000010111000001101001").addHeader(
            "cache-control", "no-cache")
            //.addHeader("postman-token", "ac506db1-771c-4c2c-84d7-696ece0a0ced")
            .build();
        try {
          Response responseNew = clientNew.newCall(requestNew).execute();
          if(responseNew != null) {
            LogEvent.Log(TAG, "response code: " + String.valueOf(responseNew.code()));
            if(responseNew.code() == HttpsURLConnection.HTTP_OK) {
              String responseJSON = responseNew.body().string();
              LogEvent.Log(TAG, "response JSON: " + responseJSON);
              return true;
            } else {
              LogEvent.Log(TAG, "Error");
              return false;
            }
          }
          return null;
        } catch(IOException e) {
          LogEvent.Log(TAG, "IOException: " + e.getMessage());
          return null;
        }
      } else {
        requestBody = okhttp3.RequestBody.create(mediaType, obj.toString());
        okhttp3.Request request =
          new okhttp3.Request.Builder().url(params[0]).addHeader(LMA.PARAM_KEY,
            LMA.KEY_VALUE_ALERT).addHeader("content-type", "multipart/form-data").addHeader(
            "cache-control", "no-cache").post(requestBody).build();
        try {
          okhttp3.Response response = client.newCall(request).execute();
          if(response != null) {
            LogEvent.Log(TAG, "response code: " + String.valueOf(response.code()));
            if(response.code() == HttpsURLConnection.HTTP_OK) {
              String responseJSON = response.body().string();
              LogEvent.Log(TAG, "response JSON: " + responseJSON);
              return true;
            } else {
              LogEvent.Log(TAG, "Error");
              return false;
            }
          }
          return null;
        } catch(IOException e) {
          LogEvent.Log(TAG, "IOException: " + e.getMessage());
          return null;
        }
      }
    }

    @Override
    protected void onPostExecute(Boolean result) {
      super.onPostExecute(result);
      if(result == null) {
      } else if(!result) {
      } else {
      }
    }
  }

}
