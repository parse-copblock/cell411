package cell411.methods;

import android.widget.EditText;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Locale;

import cell411.SingletonConfig;
import cell411.models.CountryInfo;
import cell411.utils.CountryCodes;
import cell411.utils.LogEvent;

import static cell411.Singleton.getResId;
import static cell411.SingletonConfig.COUNTRY_CODE_MOLDOVA;
import static cell411.SingletonConfig.COUNTRY_CODE_ROMANIA;
import static cell411.SingletonConfig.COUNTRY_CODE_SOUTH_AFRICA;
import static cell411.SingletonConfig.DIALING_CODE_MOLDOVA;
import static cell411.SingletonConfig.DIALING_CODE_ROMANIA;

/**
 * Created by Sachin on 14-07-2017.
 */
public class UtilityMethods {
  public static String getUserCountryCode(String mobileNumber, ArrayList<CountryInfo> list) {
    if(mobileNumber != null && !mobileNumber.isEmpty()) {
      // if number matches the character of the country code
      mobileNumber = mobileNumber.replaceAll("[\\D]+", "");
      String countryCodeBasedOnLocale = Locale.getDefault().getCountry();
      String dialingCodeBasedOnLocale = null;
      for(int i = 0; i < list.size(); i++) {
        if(countryCodeBasedOnLocale.equalsIgnoreCase(list.get(i).shortCode)) {
          dialingCodeBasedOnLocale = list.get(i).dialingCode;
          break;
        }
      }
      // check if the country code is included in the phone number field
      if(dialingCodeBasedOnLocale != null && mobileNumber.startsWith(dialingCodeBasedOnLocale)) {
        // country code is included in the phone field, so it should be split into
        // separate country code and phone number
        return dialingCodeBasedOnLocale;
      } else {
        for(int i = 0; i < list.size(); i++) {
          // check if the country code is included in the phone number field
          if(mobileNumber.startsWith(list.get(i).dialingCode)) {
            // country code is included in the phone field, so it should be split into
            // separate country code and phone number
            return list.get(i).dialingCode;
          }
        }
      }
    }
    return null;
  }

  public static void setPhoneAndCountryCode(String mobileNumber, EditText etMobileNumber,
                                            Spinner spCountryCode, ArrayList<CountryInfo> list) {
    if(mobileNumber != null && !mobileNumber.isEmpty()) {
      // if number matches the character of the country code
      //mobileNumber = mobileNumber.replaceAll("[-\\[\\]^/,'*:.!><~@+#$%=?|\"\\\\()]+", "").replaceAll(" ", "");
      mobileNumber = mobileNumber.replaceAll("[\\D]+", "");
      String countryCodeBasedOnLocale = Locale.getDefault().getCountry();
      String dialingCodeBasedOnLocale = null;
      int index = 0;
      for(int i = 0; i < list.size(); i++) {
        if(countryCodeBasedOnLocale.equalsIgnoreCase(list.get(i).shortCode)) {
          index = i;
          dialingCodeBasedOnLocale = list.get(i).dialingCode;
          break;
        }
      }
      // check if the country code is included in the phone number field
      if(dialingCodeBasedOnLocale != null && mobileNumber.startsWith(dialingCodeBasedOnLocale)) {
        // country code is included in the phone field, so it should be split into
        // separate country code and phone number
        int countryCodeLength = dialingCodeBasedOnLocale.length();
        mobileNumber = mobileNumber.substring(countryCodeLength, mobileNumber.length());
        spCountryCode.setSelection(index);
      } else {
        for(int i = 0; i < list.size(); i++) {
          // check if the country code is included in the phone number field
          if(mobileNumber.startsWith(list.get(i).dialingCode)) {
            // country code is included in the phone field, so it should be split into
            // separate country code and phone number
            int countryCodeLength = list.get(i).dialingCode.length();
            mobileNumber = mobileNumber.substring(countryCodeLength, mobileNumber.length());
            spCountryCode.setSelection(i);
            break;
          }
        }
      }
      etMobileNumber.setText(mobileNumber);
    }
  }

  public static int getDefaultCountryCodeIndex(ArrayList<CountryInfo> list) {
    String country = Locale.getDefault().getCountry();
    int index = 228;
    for(int i = 0; i < list.size(); i++) {
      if(country.equalsIgnoreCase(list.get(i).shortCode)) {
        index = i;
        break;
      }
    }
    return index;
  }

  public static void initializeCountryCodeList(ArrayList<CountryInfo> list, int clientFirmId,
                                               boolean isLive) {
    if(clientFirmId == 3 && isLive) { // RO 112 will have only two countries
      Locale locRomania = new Locale("", COUNTRY_CODE_ROMANIA);
      list.add(new CountryInfo(locRomania.getDisplayCountry(), DIALING_CODE_ROMANIA,
        COUNTRY_CODE_ROMANIA,
        getResId(COUNTRY_CODE_ROMANIA.toLowerCase())));
      Locale locMoldova = new Locale("", COUNTRY_CODE_MOLDOVA);
      list.add(new CountryInfo(locMoldova.getDisplayCountry(), DIALING_CODE_MOLDOVA,
        COUNTRY_CODE_MOLDOVA,
        getResId(COUNTRY_CODE_MOLDOVA.toLowerCase())));
    } else {
      for(int i = 0; i < CountryCodes.countryNameCodesArray.length; i++) {
        Locale loc = new Locale("", CountryCodes.countryNameCodesArray[i]);
        list.add(new CountryInfo(loc.getDisplayCountry(),
          String.valueOf(CountryCodes.countryCodesArray[i]),
          CountryCodes.countryNameCodesArray[i],
          getResId(CountryCodes.countryNameCodesArray[i].toLowerCase())));
      }
    }
    Collections.sort(list, new Comparator<CountryInfo>() {
      @Override
      public int compare(CountryInfo lhs, CountryInfo rhs) {
        return lhs.name.compareToIgnoreCase(rhs.name);
      }
    });
    for(int i = 0; i < list.size(); i++) {
      if(list.get(i).shortCode.equals(COUNTRY_CODE_SOUTH_AFRICA)) {
        SingletonConfig.COUNTRY_CODE_INDEX_IER = i;
        LogEvent.Log("UtilityMethods", "COUNTRY_CODE_INDEX_IER: " + i);
      } else if(list.get(i).shortCode.equals(COUNTRY_CODE_ROMANIA)) {
        SingletonConfig.COUNTRY_CODE_INDEX_RO112 = i;
        LogEvent.Log("UtilityMethods", "COUNTRY_CODE_INDEX_RO112: " + i);
      }
    }
  }
}
