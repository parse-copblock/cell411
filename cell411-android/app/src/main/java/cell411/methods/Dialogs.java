package cell411.methods;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;

import com.parse.ParseInstallation;
import com.safearx.cell411.R;

import cell411.Singleton;
import cell411.utils.LogEvent;

/**
 * Created by Sachin on 27-03-2016.
 */
public class Dialogs {
  private static final String TAG = "Dialogs";

  public static void showAlertDialog(final Activity context, String message) {
    AlertDialog.Builder alert = new AlertDialog.Builder(context);
    alert.setMessage(message);
    alert.setPositiveButton(R.string.dialog_btn_ok, new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialogInterface, int i) {
      }
    });
    AlertDialog dialog = alert.create();
    dialog.show();
  }

  public static void showAlertWithTitleDialog(final Activity context, String title,
                                              String message) {
    AlertDialog.Builder alert = new AlertDialog.Builder(context);
    alert.setTitle(title);
    alert.setMessage(message);
    alert.setPositiveButton(R.string.dialog_btn_ok, new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int which) {
      }
    });
    final AlertDialog dialog = alert.create();
    dialog.show();
  }

  public static void showSessionExpiredAlertDialog() {
    LogEvent.Log(TAG, "showSessionExpiredAlertDialog() invoked");
    Singleton.INSTANCE.clearData();
    Singleton.INSTANCE.getAppPrefs().edit().putBoolean("isLoggedIn", false).commit();
    // De-associate the current user from the Installation object so to prevent notifications when logged out
    ParseInstallation installation = ParseInstallation.getCurrentInstallation();
    installation.remove("user");
    installation.saveEventually();
    AlertDialog.Builder alert =
      new AlertDialog.Builder(Singleton.INSTANCE.getCurrentActivity());
    alert.setTitle("Session Expired");
    alert.setMessage("We are sorry, you have been logged out, please login again.");
    alert.setOnCancelListener(new DialogInterface.OnCancelListener() {
      @Override
      public void onCancel(DialogInterface dialogInterface) {
        Modules.gotoLogin();
      }
    });
    alert.setPositiveButton(R.string.dialog_btn_ok, new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int which) {
        Modules.gotoLogin();
      }
    });
    final AlertDialog dialog = alert.create();
    dialog.show();
  }
}
/*
[DONE] ==============NewMemberJoinedActivity============= APP
    EN: name + " has just joined " + app_name + ", would you like to add them as friend?"
    RO:
    name + " s-a alăturat aplicației " + app_name + ", ai dori să îl adaugi ca prieten?"
    Now... in Romanian pronouns are gendered, so if it's a girl, this would become:
    name + " s-a alăturat aplicației " + app_name + ", ai dori să o adaugi ca prietenă?"
    in this context, only if we use it as plural (would you like to be added to friends):
    name + " s-a alăturat aplicației " + app_name + ", ai dori să fie adăugat la prieteni?"
[DONE] ==============Public_cell_model============= PORTAL
    EN: "Your Public Cell \"" . $cell_name . "\" has been verified and approved."
    RO: "Celula ta publică \"" . $cell_name . "\" a fost validată și aprobată."
    EN: "Your Public Cell \"" . $cell_name . "'s\" application for review was not approved."
    RO: "Cererea de examinare a Celulei tale Publice \"" . $cell_name . "\" nu a fost aprobată."
    EN: "A new public cell called " . $_POST['cellName'] . " has just been created in your area."
    RO: "O nouă celulă publică numită " . $_POST['cellName'] . " tocmai a fost creată în zona ta."
[DONE] ==============afterDelete Trigger on PublicCell============= CLOUD CODE
    EN: Cell <public cell name> was deleted by its owner and will no longer be able to send messages to its members.
    RO: Celula <public cell name> a fost ștearsă de către proprietar și ca urmare nu va mai putea trimite mesaje membrilor săi.
[DONE] ==============userFunctions============= CLOUD CODE
    EN: request.params.name + " invited you to Ro 112"
    RO: request.params.name + " te-a invitat pe RO 112"
    EN: "Verification Request for cell " + request.params.cellName
    RO: "Cerere de verificare pentru Celula " + request.params.cellName
[DONE] ==============TabChatsFragment============= APP
    EN: "Are you sure you want to delete this chat?"
    RO:
    EN: "You: " + chatRoom.msg
    RO:
    EN: "You: Image"
    RO:
    EN: chatRoom.senderFirstName + ": Image"
    RO:
    EN: "You: Location"
    RO:
    EN: chatRoom.senderFirstName + ": Location"
    RO:
    EN: Load more messages
    RO:
 */