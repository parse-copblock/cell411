package cell411.methods;

import android.app.Activity;
import android.content.Intent;

import com.parse.ParseException;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseRelation;
import com.parse.ParseUser;
import com.parse.callback.FindCallback;
import com.parse.callback.SaveCallback;
import com.safearx.cell411.R;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import cell411.Singleton;
import cell411.activity.AlertActivity;
import cell411.fragments.FriendsFragment;
import cell411.models.AlertFromHelper;
import cell411.models.AlertFromNeedy;
import cell411.models.AlertFromRejector;
import cell411.models.Cell411Alert;
import cell411.models.CellRequest;
import cell411.models.FriendRequest;
import cell411.models.NewPublicCell;
import cell411.models.ReceivedCell411Alert;
import cell411.models.User;
import cell411.models.Video;
import cell411.utils.LogEvent;
import cell411.utils.StorageOperations;

/**
 * Created by Sachin on 01-06-2016.
 */
public class Modules4NewAlerts {
  private static final String TAG = "Modules4NewAlerts";
  private static final long TIME_TO_LIVE = 7200000; // Two Hours

  public static void checkParseForNewAlerts(final Activity context) {
    displayCustomAlertsIfSaved(context);
    displayCell411AlertsIfSaved(context);
    displayNewCellJoinAlertsIfSaved(context);
    checkPendingTasks(context);
    displayFRandFIAlerts(context);
  }

  public static void displayCell411AlertsIfSaved(final Activity context) {
    // Display alert from Needy
    ArrayList<AlertFromNeedy> alertsFromNeedy = StorageOperations.getAlertsFromNeedy(context);
    if(alertsFromNeedy != null) {
      for(int i = 0; i < alertsFromNeedy.size(); i++) {
        AlertFromNeedy alert = alertsFromNeedy.get(i);
        double timeElapsed = System.currentTimeMillis() - alert.createdAt;
        if(timeElapsed <= TIME_TO_LIVE) {
          Cell411Alert.AlertType alertTypeEnum;
          if(alert.alertId != -1) {
            alertTypeEnum = Cell411Alert.AlertType.values()[alert.alertId];
          } else {
            alertTypeEnum = Modules.convertStringToEnum(alert.alertRegarding);
          }
          String[] nameArr = alert.firstName.split(" ");
          ReceivedCell411Alert receivedCell411Alert =
            new ReceivedCell411Alert(nameArr[0], nameArr[1], alert.userId, alert.lat, alert.lon,
              alert.createdAt, alert.message, alert.cell411AlertId, alert._additionalNote,
              alert._dispatchMode, alert._isGlobal, null, alert.cellName, alert.cellId,
              alertTypeEnum, null, alert.fullAddress, alert.city, alert.country);
          Intent alertIntent = new Intent(context, AlertActivity.class);
          alertIntent.putExtra("receivedCell411Alert", receivedCell411Alert);
          context.startActivity(alertIntent);
        } else {
          LogEvent.Log(TAG, "An alert from " + alert.firstName + " is expired");
        }
      }
    }
    alertsFromNeedy = new ArrayList<>();
    StorageOperations.storeAlertsFromNeedy(context, alertsFromNeedy);
    // Display alerts from helper
    ArrayList<AlertFromHelper> alertsFromHelper = StorageOperations.getAlertsFromHelper(context);
    if(alertsFromHelper != null) {
      for(int i = 0; i < alertsFromHelper.size(); i++) {
        AlertFromHelper alert = alertsFromHelper.get(i);
        double timeElapsed = System.currentTimeMillis() - alert.createdAt;
        String additionalNoteId = alert.additionalNoteId;
        if(timeElapsed <= TIME_TO_LIVE) {
          Dialogs.showAlertDialog(context, alert.message);
          ParseQuery<ParseObject> parseQuery = new ParseQuery<>("AdditionalNote");
          parseQuery.whereEqualTo("objectId", additionalNoteId);
          parseQuery.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> parseObjects, ParseException e) {
              if(e == null) {
                if(parseObjects != null && parseObjects.size() > 0) {
                  parseObjects.get(0).put("seen", 1);
                  parseObjects.get(0).saveInBackground();
                }
              }
            }
          });
        } else {
          LogEvent.Log(TAG, "An alert from " + alert.name + " is expired");
        }
      }
    }
    alertsFromHelper = new ArrayList<>();
    StorageOperations.storeAlertsFromHelper(context, alertsFromHelper);
    // Display alerts from rejector
    ArrayList<AlertFromRejector> alertsFromRejector =
      StorageOperations.getAlertsFromRejector(context);
    if(alertsFromRejector != null) {
      for(int i = 0; i < alertsFromRejector.size(); i++) {
        AlertFromRejector alert = alertsFromRejector.get(i);
        double timeElapsed = System.currentTimeMillis() - alert.createdAt;
        String additionalNoteId = alert.additionalNoteId;
        if(timeElapsed <= TIME_TO_LIVE) {
          Dialogs.showAlertDialog(context, alert.message);
          ParseQuery<ParseObject> parseQuery = new ParseQuery<>("AdditionalNote");
          parseQuery.whereEqualTo("objectId", additionalNoteId);
          parseQuery.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> parseObjects, ParseException e) {
              if(e == null) {
                if(parseObjects != null && parseObjects.size() > 0) {
                  parseObjects.get(0).put("seen", 1);
                  parseObjects.get(0).saveInBackground();
                }
              }
            }
          });
        } else {
          LogEvent.Log(TAG, "An alert from " + alert.name + " is expired");
        }
      }
    }
    alertsFromRejector = new ArrayList<>();
    StorageOperations.storeAlertsFromRejector(context, alertsFromRejector);
    // Display video alerts
    //        ArrayList<Video> videoAlerts = StorageOperations.getVideoAlerts(this);
    //        if (videoAlerts != null) {
    //            for (int i = 0; i < videoAlerts.size(); i++) {
    //                Video video = videoAlerts.get(i);
    //                showVideoStreamDialog(video);
    //            }
    //        }
    //        videoAlerts = new ArrayList<>();
    //        StorageOperations.storeVideoAlerts(this, videoAlerts);
  }

  public static void displayNewCellJoinAlertsIfSaved(final Activity context) {
    // Display new cell join alerts
    ArrayList<NewPublicCell> newPublicCellArrayList = StorageOperations.getNewPublicCells(context);
    if(newPublicCellArrayList != null) {
      for(int i = 0; i < newPublicCellArrayList.size(); i++) {
        NewPublicCell newPublicCell = newPublicCellArrayList.get(i);
        if(Modules.isWeakReferenceValid()) {
          DialogModules4ReceivedAlerts
            .showCellJoinDialog(Singleton.INSTANCE.getCurrentActivity(),
              newPublicCell);
        }
      }
    }
    newPublicCellArrayList = new ArrayList<>();
    StorageOperations.storeNewPublicCells(context, newPublicCellArrayList);
  }

  public static void displayCustomAlertsIfSaved(final Activity context) {
    // Display custom alerts
    ArrayList<String> otherAlerts = StorageOperations.getOtherAlerts(context);
    if(otherAlerts != null) {
      for(int i = 0; i < otherAlerts.size(); i++) {
        Dialogs.showAlertDialog(context, otherAlerts.get(i));
      }
    }
    otherAlerts = new ArrayList<>();
    StorageOperations.storeOtherAlerts(context, otherAlerts);
  }

  public static void displayFRandFIAlerts(final Activity context) {
    ParseUser user = ParseUser.getCurrentUser();
    Date dateBefore2Hours = new Date();
    dateBefore2Hours.setTime(System.currentTimeMillis() - TIME_TO_LIVE);
    Date dateBefore24Hours = new Date();
    // 2 hours TIME-TO-LIVE duration
    dateBefore24Hours.setTime(System.currentTimeMillis() - (1000 * 60 * 60 * 24));
    // Cell 411 alert issued by others having target as current user
    // and which current user hasn't performed any action yet
    ParseQuery<ParseObject> parseQuery4ReceivedAlerts = ParseQuery.getQuery("Cell411Alert");
    parseQuery4ReceivedAlerts.whereEqualTo("targetMembers", ParseUser.getCurrentUser());
    parseQuery4ReceivedAlerts.whereNotEqualTo("initiatedBy", ParseUser.getCurrentUser());
    parseQuery4ReceivedAlerts.whereNotEqualTo("rejectedBy", ParseUser.getCurrentUser());
    parseQuery4ReceivedAlerts.whereGreaterThanOrEqualTo("createdAt", dateBefore2Hours);
    // Cell 411 Photo alerts issued by cell member
    ParseQuery<ParseObject> parseQuery4CellPhotoAlerts = ParseQuery.getQuery("Cell411Alert");
    parseQuery4CellPhotoAlerts.whereEqualTo("alertType", "Photo");
    parseQuery4CellPhotoAlerts.whereEqualTo("cellMembers", ParseUser.getCurrentUser());
    parseQuery4CellPhotoAlerts.whereNotEqualTo("initiatedBy", ParseUser.getCurrentUser());
    parseQuery4CellPhotoAlerts.whereNotEqualTo("rejectedBy", ParseUser.getCurrentUser());
    parseQuery4CellPhotoAlerts.whereGreaterThanOrEqualTo("createdAt", dateBefore2Hours);
    // Exclude the alerts from the user who have spammed the current user
    // This is for the scenario as below:
    // Suppose you have spammed me, so you are in my spammedBy relation
    // Now if my app is open then if you will send me a needy alert then the alert will be
    // displayed but i will not be able to do anything on tap of Help or can't help,
    // neither i can make any entry on parse
    // Now whenever we fetch the needy alert from parse it will always show me those alerts
    // within TIME-TO-LIVE time frame as I had neither make any entry on "InitiateBy" nor
    // "RejectedBy" relation
    ParseRelation relation4SpammedByUsers = user.getRelation("spammedBy");
    ParseQuery parseQuery4SpammedByUsers = relation4SpammedByUsers.getQuery();
    parseQuery4ReceivedAlerts.whereDoesNotMatchQuery("issuedBy", parseQuery4SpammedByUsers);
    // Alerts issued by the current users for Everything is OK now Alert
    ParseQuery<ParseObject> parseQuery4IssuedAlerts = ParseQuery.getQuery("Cell411Alert");
    parseQuery4IssuedAlerts.whereEqualTo("issuerId", ParseUser.getCurrentUser().getObjectId());
    parseQuery4IssuedAlerts.whereGreaterThanOrEqualTo("createdAt", dateBefore24Hours); // 24 hours
    // Friend Request or Friend Invites received from users
    ArrayList<String> entryForList = new ArrayList<>();
    entryForList.add("FR");
    entryForList.add("FI");
    entryForList.add("CR");
    ParseQuery<ParseObject> parseQuery4FriendRequests = ParseQuery.getQuery("Cell411Alert");
    parseQuery4FriendRequests.whereEqualTo("to", ParseUser.getCurrentUser().getUsername());
    parseQuery4FriendRequests.whereNotEqualTo("seenBy", ParseUser.getCurrentUser());
    parseQuery4FriendRequests.whereContainedIn("entryFor", entryForList);
    parseQuery4FriendRequests.whereEqualTo("status", "PENDING");
    // this query will be utilized only if the current user has phone number verified
    ParseQuery<ParseObject> parseQuery4FriendRequestsUsingPhone =
      ParseQuery.getQuery("Cell411Alert");
    parseQuery4FriendRequestsUsingPhone.whereEqualTo("to",
      ParseUser.getCurrentUser().getString("mobileNumber"));
    parseQuery4FriendRequestsUsingPhone.whereNotEqualTo("seenBy", ParseUser.getCurrentUser());
    parseQuery4FriendRequestsUsingPhone.whereContainedIn("entryFor", entryForList);
    parseQuery4FriendRequestsUsingPhone.whereEqualTo("status", "PENDING");
    // this query will be utilized only if the user is a facebook user and email
    ParseQuery<ParseObject> parseQuery4FriendRequests2 = ParseQuery.getQuery("Cell411Alert");
    parseQuery4FriendRequests2.whereEqualTo("to", ParseUser.getCurrentUser().getEmail());
    parseQuery4FriendRequests.whereNotEqualTo("seenBy", ParseUser.getCurrentUser());
    parseQuery4FriendRequests2.whereContainedIn("entryFor", entryForList);
    parseQuery4FriendRequests2.whereEqualTo("status", "PENDING");
    // Club all the queries into one master query
    List<ParseQuery<ParseObject>> queries = new ArrayList<>();
    queries.add(parseQuery4ReceivedAlerts);
    queries.add(parseQuery4IssuedAlerts);
    queries.add(parseQuery4FriendRequests);
    queries.add(parseQuery4CellPhotoAlerts);
    if(ParseUser.getCurrentUser().getEmail() != null &&
         !ParseUser.getCurrentUser().getUsername().equals(ParseUser.getCurrentUser().getEmail())) {
      queries.add(parseQuery4FriendRequests2);
    }
    //       if (ParseUser.getCurrentUser().getString("mobileNumber") != null &&
    //           ParseUser.getCurrentUser().getBoolean("phoneVerified")) {
    //         queries.add(parseQuery4FriendRequestsUsingPhone);
    //       }
    ParseQuery<ParseObject> mainQuery = ParseQuery.or(queries);
    // Exclude Spam users alerts
    ParseRelation relation4SpamUsers = user.getRelation("spamUsers");
    ParseQuery parseQuery4SpamUsers = relation4SpamUsers.getQuery();
    //mainQuery.whereDoesNotMatchQuery("issuedBy", parseQuery4SpamUsers);
    // Required to check if the alert is issued by the current user
    mainQuery.include("issuedBy");
    // Sort the result as the most recent first
    mainQuery.orderByDescending("createdAt");
    mainQuery.findInBackground(new FindCallback<ParseObject>() {
      @Override
      public void done(List<ParseObject> parseObjects, ParseException e) {
        if(e == null) {
          LogEvent.Log(TAG, "Total Alerts: " + parseObjects.size());
          boolean firstIssuedAlertFound = false;
          boolean enableOkSlice = false;
          for(int i = 0; i < parseObjects.size(); i++) {
            String cell411AlertId = parseObjects.get(i).getObjectId();
            String additionalNote = (String) parseObjects.get(i).get("additionalNote");
            long createdAt = parseObjects.get(i).getCreatedAt().getTime();
            ParseUser issuedBy = (ParseUser) parseObjects.get(i).get("issuedBy");
            String issuerFirstName = (String) parseObjects.get(i).get("issuerFirstName");
            String issuerId = (String) parseObjects.get(i).get("issuerId");
            if(issuedBy == null) {
              continue;
            }
            ParseGeoPoint location = null;
            if(parseObjects.get(i).get("location") != null) {
              location = (ParseGeoPoint) parseObjects.get(i).get("location");
            }
            String entryFor = null;
            if(parseObjects.get(i).get("entryFor") != null) {
              entryFor = (String) parseObjects.get(i).get("entryFor");
            }
            ParseUser forwardedBy = null;
            if(parseObjects.get(i).get("forwardedBy") != null) {
              forwardedBy = (ParseUser) parseObjects.get(i).get("forwardedBy");
            }
            String cellName = null;
            if(parseObjects.get(i).get("cellName") != null) {
              cellName = (String) parseObjects.get(i).get("cellName");
            }
            String cellId = null;
            if(parseObjects.get(i).get("cellId") != null) {
              cellId = (String) parseObjects.get(i).get("cellId");
            }
            int isGlobal = 0;
            if(parseObjects.get(i).get("isGlobal") != null) {
              isGlobal = (int) parseObjects.get(i).get("isGlobal");
            }
            String status = null;
            if(parseObjects.get(i).get("status") != null) {
              status = (String) parseObjects.get(i).get("status");
            }
            String to = null;
            if(parseObjects.get(i).get("to") != null) {
              to = (String) parseObjects.get(i).get("to");
            }
            if(issuedBy.getObjectId().equals(ParseUser.getCurrentUser().getObjectId())) {
              // the alert is issued by the current user and it may be a Cell 411 Alert or a FR/FI
              if(entryFor != null && !entryFor.isEmpty()) {
                // the alert is FR/FI alert issued by the current user
              } else {
                // the alert is a Cell 411 alert issued by the current user
                                /*if (!firstIssuedAlertFound && !alertType.equals("Video") && !alertType.equals("Photo")
                                        && (forwardedBy == null)
                                        && (cellName == null || cellName.isEmpty())) {
                                    firstIssuedAlertFound = true;
                                    if (status == null || !status.equals("OK")) {
                                        enableOkSlice = true;
                                        cell411AlertObject4Ok = parseObjects.get(i);
                                    }
                                    LogEvent.Log(TAG, "alertType: " + alertType);
                                    LogEvent.Log(TAG, "additionalNote: " + additionalNote);
                                    LogEvent.Log(TAG, "issuerId: " + issuerId);
                                    LogEvent.Log(TAG, "issuerFirstName: " + issuerFirstName);
                                    LogEvent.Log(TAG, "cell411AlertId: " + cell411AlertId);
                                    LogEvent.Log(TAG, "status: " + status);
                                    LogEvent.Log(TAG, "createdAt: " + parseObjects.get(i).getCreatedAt());
                                }*/
              }
            } else {
              // the alert is having current user as target member or in to attribute and it may be a Cell 411 alert or a FR/FI
              if(entryFor != null && !entryFor.isEmpty()) {
                // the alert is FR/FI alert received by the current user
                if(entryFor.equals("FR") || entryFor.equals("FI")) {
                  LogEvent.Log(TAG, "Friend Request or Friend Invite");
                  //String message = issuerFirstName + " wants to add you as a friend in the " + context.getString(R.string.app_name) + " network. If approved, this user will be able to send you emergency alerts.";
                  FriendRequest friendRequest =
                    new FriendRequest(issuerFirstName, issuedBy.getObjectId(), cell411AlertId);
                  if(Modules.isWeakReferenceValid()) {
                    DialogModules4ReceivedAlerts.showFriendRequestDialog(
                      Singleton.INSTANCE.getCurrentActivity(), friendRequest);
                  }
                } else {
                  LogEvent.Log(TAG, "Cell Request");
                  String message =
                    context.getString(R.string.notif_cell_join_request, issuerFirstName,
                      cellName);
                  CellRequest cellRequest =
                    new CellRequest(issuerFirstName, issuedBy.getObjectId(), message,
                      cell411AlertId, cellName, cellId);
                  if(Modules.isWeakReferenceValid()) {
                    DialogModules4ReceivedAlerts.showCellRequestDialog(
                      Singleton.INSTANCE.getCurrentActivity(), cellRequest);
                  }
                }
              } else {
                // the alert is a Cell 411 alert received by the current user
                Cell411Alert.AlertType alertTypeEnum;
                if(parseObjects.get(i).get("alertId") != null) {
                  int alertId = parseObjects.get(i).getInt("alertId");
                  alertTypeEnum = Cell411Alert.AlertType.values()[alertId];
                } else {
                  String alertType = (String) parseObjects.get(i).get("alertType");
                  alertTypeEnum = Modules.convertStringToEnum(alertType);
                }
                if(alertTypeEnum == Cell411Alert.AlertType.VIDEO) {
                  // This is a video streaming alert
                  boolean live = true;
                  if(status.equals("VOD")) {
                    live = false;
                  }
                  Video video =
                    new Video(issuerId, issuerFirstName, createdAt, live, cell411AlertId,
                      isGlobal);
                  if(Modules.isWeakReferenceValid()) {
                    DialogModules4ReceivedAlerts.showVideoStreamDialog(
                      Singleton.INSTANCE.getCurrentActivity(), video);
                  }
                } else if(alertTypeEnum == Cell411Alert.AlertType.PHOTO) {
                  // This is a photo alert
                  Video video =
                    new Video(issuerId, issuerFirstName, createdAt, true, cell411AlertId,
                      isGlobal);
                  if(Modules.isWeakReferenceValid()) {
                    DialogModules4ReceivedAlerts.showPhotoAlertDialog(
                      Singleton.INSTANCE.getCurrentActivity(), video, cellName);
                  }
                } else {
                  // This is a Cell 411 alert
                }
              }
            }
          }
                    /*if (enableOkSlice)
                        imgOk.setVisibility(View.VISIBLE);
                    else
                        imgOk.setVisibility(View.GONE);*/
        } else {
        }
      }
    });
  }

  public static void checkPendingTasks(final Activity context) {
    ParseQuery<ParseObject> parseQuery = ParseQuery.getQuery("Task");
    parseQuery.whereEqualTo("userId", ParseUser.getCurrentUser().getObjectId());
    parseQuery.whereEqualTo("status", "PENDING");
    parseQuery.findInBackground(new FindCallback<ParseObject>() {
      @Override
      public void done(final List<ParseObject> parseObjects, ParseException e) {
        if(e == null) {
          if(parseObjects != null && parseObjects.size() > 0) {
            LogEvent.Log(TAG, parseObjects.size() + " Pending Tasks");
            for(int i = 0; i < parseObjects.size(); i++) {
              final ParseObject taskObject = parseObjects.get(i);
              String assigneeUserId = (String) taskObject.get("assigneeUserId");
              String task = (String) taskObject.get("task");
              if(task.equals("FRIEND_ADD")) {
                LogEvent.Log(TAG, "Task: FRIEND_ADD");
                ParseQuery<ParseUser> userQuery = ParseUser.getQuery();
                userQuery.whereEqualTo("objectId", assigneeUserId);
                userQuery.findInBackground(new FindCallback<ParseUser>() {
                  @Override
                  public void done(final List<ParseUser> parseUsers, ParseException e) {
                    if(e == null) {
                      if(parseUsers != null && parseUsers.size() > 0) {
                        ParseRelation<ParseUser> relation =
                          ParseUser.getCurrentUser().getRelation("friends");
                        relation.add(parseUsers.get(0));
                        parseUsers.get(0).saveInBackground(new SaveCallback() {
                          @Override
                          public void done(ParseException e) {
                            if(e == null) {
                              // Friend added successfully
                              taskObject.deleteInBackground();
                              // Check the privilege and update it, if required
                              PrivilegeModules.checkAndUpdatePrivilege(true);
                              String email = null;
                              if(parseUsers.get(0).getEmail() != null &&
                                   !parseUsers.get(0).getEmail().isEmpty()) {
                                email = parseUsers.get(0).getEmail();
                              } else {
                                email = parseUsers.get(0).getUsername();
                              }
                              User friend =
                                new User(email, (String) parseUsers.get(0).get("firstName"),
                                  (String) parseUsers.get(0).get("lastName"),
                                  parseUsers.get(0));
                              Singleton.INSTANCE.addFriendToList(friend);
                              // update the UI here
                              FriendsFragment.refreshFriendsList();
                              String name = parseUsers.get(0).get("firstName") + " " +
                                              parseUsers.get(0).get("lastName");
                              String message =
                                context.getString(R.string.notif_friend_approved, name);
                              if(Modules.isWeakReferenceValid()) {
                                DialogModules4ReceivedAlerts.showFriendApprovedDialog(
                                  Singleton.INSTANCE.getCurrentActivity(), message, friend);
                              }
                            } else {
                              LogEvent.Log(TAG, "Unable to add friend");
                            }
                          }
                        });
                      }
                    } else {
                      LogEvent.Log(TAG, "Unable to retrieve task assignee");
                    }
                  }
                });
              } else if(task.equals("SPAM_ADD")) {
                LogEvent.Log(TAG, "Task: SPAM_ADD");
                ParseQuery<ParseUser> userQuery = ParseUser.getQuery();
                userQuery.whereEqualTo("objectId", assigneeUserId);
                userQuery.findInBackground(new FindCallback<ParseUser>() {
                  @Override
                  public void done(List<ParseUser> parseUsers, ParseException e) {
                    if(e == null) {
                      if(parseUsers != null && parseUsers.size() > 0) {
                        ParseRelation<ParseUser> relation =
                          ParseUser.getCurrentUser().getRelation("spammedBy");
                        relation.add(parseUsers.get(0));
                        parseUsers.get(0).saveInBackground(new SaveCallback() {
                          @Override
                          public void done(ParseException e) {
                            if(e == null) {
                              // spammedBy entry done successfully
                              taskObject.deleteInBackground();
                            } else {
                              LogEvent.Log(TAG, "Unable to save spammedBy entry");
                            }
                          }
                        });
                      }
                    } else {
                      LogEvent.Log(TAG, "Unable to retrieve task assignee");
                    }
                  }
                });
              } else if(task.equals("SPAM_REMOVE")) {
                LogEvent.Log(TAG, "Task: SPAM_REMOVE");
                ParseQuery<ParseUser> userQuery = ParseUser.getQuery();
                userQuery.whereEqualTo("objectId", assigneeUserId);
                userQuery.findInBackground(new FindCallback<ParseUser>() {
                  @Override
                  public void done(List<ParseUser> parseUsers, ParseException e) {
                    if(e == null) {
                      if(parseUsers != null && parseUsers.size() > 0) {
                        ParseRelation<ParseUser> relation =
                          ParseUser.getCurrentUser().getRelation("spammedBy");
                        relation.remove(parseUsers.get(0));
                        parseUsers.get(0).saveInBackground(new SaveCallback() {
                          @Override
                          public void done(ParseException e) {
                            if(e == null) {
                              // spammedBy entry done successfully
                              taskObject.deleteInBackground();
                            } else {
                              LogEvent.Log(TAG, "Unable to save spammedBy entry");
                            }
                          }
                        });
                      }
                    } else {
                      LogEvent.Log(TAG, "Unable to retrieve task assignee");
                    }
                  }
                });
              }
            }
          }
        } else {
          LogEvent.Log(TAG, "Unable to retrieve pending tasks");
        }
      }
    });
  }
}
