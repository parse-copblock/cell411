package cell411.methods;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Service;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.ListView;

import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParsePush;
import com.parse.ParseQuery;
import com.parse.ParseRelation;
import com.parse.ParseUser;
import com.parse.callback.FindCallback;
import com.parse.callback.GetCallback;
import com.parse.callback.SaveCallback;
import com.safearx.cell411.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cell411.Singleton;
import cell411.activity.ImageScreenActivity;
import cell411.adapters.CellListAdapter;
import cell411.fragments.FriendsFragment;
import cell411.models.CellRequest;
import cell411.models.FriendRequest;
import cell411.models.NewPrivateCell;
import cell411.models.NewPublicCell;
import cell411.models.User;
import cell411.models.Video;
import cell411.utils.LogEvent;

/**
 * Created by Sachin on 01-06-2016.
 */
public class DialogModules4ReceivedAlerts {
  private static final String TAG = "DialogModules4ReceivedAlerts";

  public static void showFriendApprovedDialog(final Activity context, String message,
                                              final User user) {
    AlertDialog.Builder alert =
      new AlertDialog.Builder(Singleton.INSTANCE.getCurrentActivity());
    alert.setMessage(message);
    alert.setOnCancelListener(new DialogInterface.OnCancelListener() {
      @Override
      public void onCancel(DialogInterface dialog) {
      }
    });
    alert.setPositiveButton(R.string.dialog_btn_ok, new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialogInterface, int i) {
        showAddFriendToCellDialog(context, user);
      }
    });
    AlertDialog dialog = alert.create();
    dialog.show();
  }

  public static void showAddFriendToCellDialog(final Activity context, final User user) {
    AlertDialog.Builder alert =
      new AlertDialog.Builder(Singleton.INSTANCE.getCurrentActivity());
    alert.setTitle(context.getString(R.string.dialog_msg_add_friend_to_cell, user.firstName));
    LayoutInflater inflater =
      (LayoutInflater) context.getSystemService(Service.LAYOUT_INFLATER_SERVICE);
    View view = inflater.inflate(R.layout.layout_cell, null);
    final ListView listView = (ListView) view.findViewById(R.id.list_cells);
    final CheckBox cbIncludeSecurityGuards =
      (CheckBox) view.findViewById(R.id.cb_include_security_guards);
    cbIncludeSecurityGuards.setVisibility(View.GONE);
    final ArrayList<NewPrivateCell>[] cellList = new ArrayList[1];
    final NewPrivateCell[] selectedCell = new NewPrivateCell[1];
    if(Singleton.INSTANCE.getCells() != null) {
      cellList[0] =
        (ArrayList<NewPrivateCell>) Singleton.INSTANCE.getNewPrivateCells().clone();
    }
    if(cellList[0] == null) {
      ParseQuery<ParseObject> cellQuery = ParseQuery.getQuery("Cell");
      cellQuery.whereEqualTo("createdBy", ParseUser.getCurrentUser());
      cellQuery.whereNotEqualTo("type", 5);
      cellQuery.include("members");
      cellQuery.findInBackground(new FindCallback<ParseObject>() {
        @Override
        public void done(List<ParseObject> parseObjects, ParseException e) {
          cellList[0] = new ArrayList<>();
          if(e == null) {
            if(parseObjects != null) {
              for(int i = 0; i < parseObjects.size(); i++) {
                ParseObject cell = parseObjects.get(i);
                NewPrivateCell newPrivateCell =
                  new NewPrivateCell((String) cell.get("name"), cell, cell.getInt("type"),
                    cell.getInt("totalMembers"));
                if(cell.getList("members") != null) {
                  LogEvent.Log("Cell",
                    "There are some members in " + cell.get("name") + " newPrivateCell");
                  List<ParseUser> members = cell.getList("members");
                  newPrivateCell.members.addAll(members);
                } else {
                  LogEvent.Log("Cell",
                    "There are no members in " + cell.get("name") + " newPrivateCell");
                }
                cellList[0].add(newPrivateCell);
              }
            }
          } else {
            if(Modules.isWeakReferenceValid()) {
              context.getApplicationContext();
              Singleton.sendToast(e);
            }
          }
          Singleton.INSTANCE
            .setNewPrivateCells((ArrayList<NewPrivateCell>) cellList[0].clone());
          ArrayList<NewPrivateCell> cells = new ArrayList<>();
          for(int i = 0; i < cellList[0].size(); i++) {
            cells.add(cellList[0].get(i));
          }
          if(cells.size() > 0) {
            cells.get(0).selected = true;
            selectedCell[0] = cells.get(0);
          }
          final CellListAdapter adapterCell =
            new CellListAdapter(context, R.layout.cell_list_item, cells);
          listView.setAdapter(adapterCell);
          listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
              for(int i = 0; i < adapterCell.cellsList.size(); i++) {
                adapterCell.cellsList.get(i).selected = false;
              }
              adapterCell.cellsList.get(position).selected = true;
              selectedCell[0] = adapterCell.cellsList.get(position);
              adapterCell.notifyDataSetChanged();
            }
          });
        }
      });
    } else {
      ArrayList<NewPrivateCell> cells = new ArrayList<>();
      for(int i = 0; i < cellList[0].size(); i++) {
        cellList[0].get(i).selected = false;
        cells.add(cellList[0].get(i));
      }
      if(cells.size() > 0) { // put to prevent crash (still don't know why it was crashing
        // with IndexOutOfBound Exception)
        cells.get(0).selected = true;
        selectedCell[0] = cells.get(0);
      }
      final CellListAdapter adapterCell =
        new CellListAdapter(context, R.layout.cell_list_item, cells);
      listView.setAdapter(adapterCell);
      listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
          for(int i = 0; i < adapterCell.cellsList.size(); i++) {
            adapterCell.cellsList.get(i).selected = false;
          }
          adapterCell.cellsList.get(position).selected = true;
          selectedCell[0] = adapterCell.cellsList.get(position);
          adapterCell.notifyDataSetChanged();
        }
      });
    }
    alert.setView(view);
    alert.setNegativeButton(R.string.dialog_btn_not_now, new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int arg1) {
        dialog.dismiss();
      }
    });
    alert.setPositiveButton(R.string.dialog_btn_ok, new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int which) {
        if(selectedCell[0] == null) {
          return;
        }
        ArrayList<ParseUser> members = selectedCell[0].members;
        if(members == null) {
          members = new ArrayList<ParseUser>();
        }
        members.add(user.user);
        selectedCell[0].parseObject.put("members", members);
        final ArrayList<ParseUser> finalMembers = members;
        selectedCell[0].parseObject.saveInBackground(new SaveCallback() {
          @Override
          public void done(ParseException e) {
            if(e == null) {
              selectedCell[0].members = finalMembers;
            } else {
              if(Modules.isWeakReferenceValid()) {
                context.getApplicationContext();
                Singleton.sendToast(e);
              }
            }
          }
        });
      }
    });
    AlertDialog dialog = alert.create();
    dialog.show();
  }

  public static void showFriendRequestDialog(final Activity context,
                                             final FriendRequest friendRequest) {
    AlertDialog.Builder alert =
      new AlertDialog.Builder(Singleton.INSTANCE.getCurrentActivity());
    String message = context.getString(R.string.dialog_msg_friend_request, friendRequest.name,
      context.getString(R.string.app_name));
    alert.setMessage(message);
    alert.setCancelable(false);
    alert.setNeutralButton(R.string.dialog_btn_later, new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialogInterface, int i) {
        ParseQuery<ParseObject> query = ParseQuery.getQuery("Cell411Alert");
        query.whereEqualTo("objectId", friendRequest.friendRequestObjectId);
        query.findInBackground(new FindCallback<ParseObject>() {
          @Override
          public void done(List<ParseObject> list, ParseException e) {
            if(e == null) {
              if(list != null && list.size() > 0) {
                ParseObject friendRequestObject = list.get(0);
                ParseRelation relation = friendRequestObject.getRelation("seenBy");
                relation.add(ParseUser.getCurrentUser());
                friendRequestObject.saveInBackground();
              }
            } else {
              if(Modules.isWeakReferenceValid()) {
                context.getApplicationContext();
                Singleton.sendToast(e);
              }
            }
          }
        });
      }
    });
    alert.setNegativeButton(R.string.dialog_btn_deny, new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialogInterface, int i) {
        AddFriendModules.rejectFriendRequest(context, friendRequest, null);
      }
    });
    alert.setPositiveButton(R.string.dialog_btn_approve, new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialogInterface, int i) {
        AddFriendModules.approveFriendRequest(context, friendRequest, null);
      }
    });
    AlertDialog dialog = alert.create();
    dialog.show();
  }

  public static void showCellRequestDialog(final Activity context, final CellRequest cellRequest) {
    AlertDialog.Builder alert =
      new AlertDialog.Builder(Singleton.INSTANCE.getCurrentActivity());
    alert.setMessage(cellRequest.message);
    alert.setCancelable(false);
    alert.setNegativeButton(R.string.dialog_btn_deny, new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialogInterface, int i) {
        ParseQuery<ParseUser> userParseQuery = ParseUser.getQuery();
        userParseQuery.whereEqualTo("objectId", cellRequest.userId);
        userParseQuery.getFirstInBackground(new GetCallback<ParseUser>() {
          @Override
          public void done(final ParseUser parseUser, ParseException e) {
            if(e == null) {
              ParseQuery<ParseObject> query = ParseQuery.getQuery("Cell411Alert");
              query.whereEqualTo("objectId", cellRequest.cellRequestObjectId);
              query.findInBackground(new FindCallback<ParseObject>() {
                @Override
                public void done(List<ParseObject> parseObjects, ParseException e) {
                  if(e == null) {
                    if(parseObjects != null && parseObjects.size() > 0) {
                      ParseObject cellRequestObject = parseObjects.get(0);
                      cellRequestObject.put("status", "DENIED");
                      cellRequestObject.saveInBackground(new SaveCallback() {
                        @Override
                        public void done(ParseException e) {
                          if(e == null) {
                            // Send notification for newPrivateCell approved here
                            ParseQuery pushQuery = ParseInstallation.getQuery();
                            pushQuery.whereEqualTo("user", parseUser);
                            ParsePush push = new ParsePush();
                            push.setQuery(pushQuery);
                            String name = ParseUser.getCurrentUser().get("firstName") + " " +
                                            ParseUser.getCurrentUser().get("lastName");
                            JSONObject data = new JSONObject();
                            try {
                              data.put("alert", context.getString(R.string.notif_cell_denied,
                                cellRequest.cellName));
                              data.put("userId", ParseUser.getCurrentUser().getObjectId());
                              data.put("sound", "");
                              data.put("name", name);
                              data.put("cellName", cellRequest.cellName);
                              data.put("alertType", "CELL_DENIED");
                              data.put("badge", "Increment");
                            } catch(JSONException e1) {
                              e1.printStackTrace();
                            }
                            push.setData(data);
                            push.sendInBackground();
                          } else {
                            if(Modules.isWeakReferenceValid()) {
                              context.getApplicationContext();
                              Singleton.sendToast(e);
                            }
                          }
                        }
                      });
                    }
                  } else {
                    if(Modules.isWeakReferenceValid()) {
                      context.getApplicationContext();
                      Singleton.sendToast(e);
                    }
                  }
                }
              });
            } else {
              if(Modules.isWeakReferenceValid()) {
                context.getApplicationContext();
                Singleton.sendToast(e);
              }
            }
          }
        });
      }
    });
    alert.setPositiveButton(R.string.dialog_btn_approve, new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialogInterface, int i) {
        ParseQuery<ParseObject> query = ParseQuery.getQuery("PublicCell");
        query.whereEqualTo("objectId", cellRequest.cellId);
        query.getFirstInBackground(new GetCallback<ParseObject>() {
          @Override
          public void done(final ParseObject publicCellObject, ParseException e) {
            if(e == null) {
              ParseQuery<ParseUser> userParseQuery = ParseUser.getQuery();
              userParseQuery.whereEqualTo("objectId", cellRequest.userId);
              userParseQuery.getFirstInBackground(new GetCallback<ParseUser>() {
                @Override
                public void done(final ParseUser parseUser, ParseException e) {
                  if(e == null) {
                    ParseRelation relation = publicCellObject.getRelation("members");
                    relation.add(parseUser);
                    publicCellObject.increment("totalMembers");
                    publicCellObject.saveInBackground(new SaveCallback() {
                      @Override
                      public void done(ParseException e) {
                        if(e == null) {
                          ParseQuery<ParseObject> query = ParseQuery.getQuery("Cell411Alert");
                          query.whereEqualTo("objectId", cellRequest.cellRequestObjectId);
                          query.findInBackground(new FindCallback<ParseObject>() {
                            @Override
                            public void done(List<ParseObject> parseObjects, ParseException e) {
                              if(e == null) {
                                if(parseObjects != null && parseObjects.size() > 0) {
                                  ParseObject cellRequestObject = parseObjects.get(0);
                                  cellRequestObject.put("status", "APPROVED");
                                  cellRequestObject.saveInBackground(new SaveCallback() {
                                    @Override
                                    public void done(ParseException e) {
                                      if(e == null) {
                                        // Send notification for newPrivateCell approved here
                                        ParseQuery pushQuery = ParseInstallation.getQuery();
                                        pushQuery.whereEqualTo("user", parseUser);
                                        ParsePush push = new ParsePush();
                                        push.setQuery(pushQuery);
                                        String name =
                                          ParseUser.getCurrentUser().get("firstName") + " " +
                                            ParseUser.getCurrentUser().get("lastName");
                                        JSONObject data = new JSONObject();
                                        try {
                                          data.put("alert",
                                            context.getString(R.string.notif_cell_approved,
                                              cellRequest.cellName));
                                          data.put("userId",
                                            ParseUser.getCurrentUser().getObjectId());
                                          data.put("sound", "");
                                          data.put("name", name);
                                          data.put("cellId", publicCellObject.getObjectId());
                                          data.put("cellName", cellRequest.cellName);
                                          data.put("alertType", "CELL_APPROVED");
                                          data.put("badge", "Increment");
                                        } catch(JSONException e1) {
                                          e1.printStackTrace();
                                        }
                                        push.setData(data);
                                        push.sendInBackground();
                                      } else {
                                        if(Modules.isWeakReferenceValid()) {
                                          context.getApplicationContext();
                                          Singleton.sendToast(e);
                                        }
                                      }
                                    }
                                  });
                                }
                              } else {
                                if(Modules.isWeakReferenceValid()) {
                                  context.getApplicationContext();
                                  Singleton.sendToast(e);
                                }
                              }
                            }
                          });
                        } else {
                          if(Modules.isWeakReferenceValid()) {
                            context.getApplicationContext();
                            Singleton.sendToast(e);
                          }
                        }
                      }
                    });
                  } else {
                    if(Modules.isWeakReferenceValid()) {
                      context.getApplicationContext();
                      Singleton.sendToast(e);
                    }
                  }
                }
              });
            } else {
              if(Modules.isWeakReferenceValid()) {
                context.getApplicationContext();
                Singleton.sendToast(e);
              }
            }
          }
        });
      }
    });
    AlertDialog dialog = alert.create();
    dialog.show();
  }

  public static void showCellJoinDialog(final Activity context, final NewPublicCell newPublicCell) {
    AlertDialog.Builder alert =
      new AlertDialog.Builder(Singleton.INSTANCE.getCurrentActivity());
    String message =
      context.getString(R.string.dialog_msg_new_public_cell_join, newPublicCell.cellName);
    alert.setMessage(message);
    alert.setCancelable(false);
    alert.setNegativeButton(R.string.dialog_btn_cancel, new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialogInterface, int i) {
      }
    });
    alert.setPositiveButton(R.string.dialog_btn_join_cell, new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialogInterface, int i) {
        final String name = ParseUser.getCurrentUser().get("firstName") + " " +
                              ParseUser.getCurrentUser().get("lastName");
        ParseQuery<ParseUser> userParseQuery = ParseUser.getQuery();
        userParseQuery.whereEqualTo("objectId", newPublicCell.userId);
        userParseQuery.getFirstInBackground(new GetCallback<ParseUser>() {
          @Override
          public void done(final ParseUser parseUserCellOwner, ParseException e) {
            if(parseUserCellOwner != null) {
              // Retrieve the new object from parse to check if
              // it still exists or the owner has deleted
              ParseQuery parseQuery = ParseQuery.getQuery("PublicCell");
              parseQuery.whereEqualTo("objectId", newPublicCell.cellId);
              parseQuery.getFirstInBackground(new GetCallback<ParseObject>() {
                @Override
                public void done(ParseObject parseObject, ParseException e) {
                  if(e == null) {
                    final ParseObject cellRequestObject = new ParseObject("Cell411Alert");
                    cellRequestObject.put("issuedBy", ParseUser.getCurrentUser());
                    cellRequestObject.put("issuerFirstName", name);
                    cellRequestObject.put("to", parseUserCellOwner.getUsername());
                    cellRequestObject.put("status", "PENDING");
                    cellRequestObject.put("entryFor", "CR");
                    cellRequestObject.put("cellId", newPublicCell.cellId);
                    cellRequestObject.put("cellName", newPublicCell.cellName);
                    cellRequestObject.saveInBackground(new SaveCallback() {
                      @Override
                      public void done(ParseException e) {
                        if(e == null) {
                          // Send notification for friend request here
                          ParseQuery pushQuery = ParseInstallation.getQuery();
                          pushQuery.whereEqualTo("user", parseUserCellOwner);
                          ParsePush push = new ParsePush();
                          push.setQuery(pushQuery);
                          JSONObject data = new JSONObject();
                          try {
                            data.put("alert", context.getString(R.string.notif_cell_request, name,
                              newPublicCell.cellName));
                            data.put("userId", ParseUser.getCurrentUser().getObjectId());
                            data.put("sound", "");
                            data.put("cellRequestObjectId", cellRequestObject.getObjectId());
                            data.put("name", name);
                            data.put("cellId", newPublicCell.cellId);
                            data.put("cellName", newPublicCell.cellName);
                            data.put("alertType", "CELL_REQUEST");
                            data.put("badge", "Increment");
                          } catch(JSONException e1) {
                            e1.printStackTrace();
                          }
                          push.setData(data);
                          push.sendInBackground();
                          if(Modules.isWeakReferenceValid()) {
                            context.getApplicationContext();
                            Singleton
                              .sendToast(context.getString(R.string.notif_cell_join_request_sent,
                                newPublicCell.cellName));
                          }
                        } else {
                          if(Modules.isWeakReferenceValid()) {
                            context.getApplicationContext();
                            Singleton.sendToast(e);
                          }
                        }
                      }
                    });
                  } else {
                    if(e.getCode() == 101) {
                      // Cell does not exists anymore,
                      // so show appropriate message to the user
                      // and then remove this newPrivateCell from list
                      if(Modules.isWeakReferenceValid()) {
                        context.getApplicationContext();
                        Singleton.sendToast(R.string.cell_no_longer_exists);
                      }
                    } else {
                      // Seems like the list is empty
                      if(Modules.isWeakReferenceValid()) {
                        context.getApplicationContext();
                        Singleton.sendToast(e);
                      }
                    }
                  }
                }
              });
            }
          }
        });
      }
    });
    AlertDialog dialog = alert.create();
    dialog.show();
  }

  public static void showFriendAddBackDialog(final Activity context, FriendRequest friendRequest,
                                             final ParseUser user) {
    String[] firstName = friendRequest.name.split(" ");
    AlertDialog.Builder alert =
      new AlertDialog.Builder(Singleton.INSTANCE.getCurrentActivity());
    alert.setTitle(context.getString(R.string.dialog_title_friend_add_back, firstName[0]));
    alert.setMessage(context.getString(R.string.dialog_msg_friend_add_back, friendRequest.name,
      context.getString(R.string.app_name)));
    alert.setNegativeButton(R.string.dialog_btn_no, new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialogInterface, int i) {
      }
    });
    alert.setPositiveButton(context.getString(R.string.dialog_btn_add) + " " + firstName[0],
      new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialogInterface, int i) {
          ParseUser currentUser = ParseUser.getCurrentUser();
          ParseRelation<ParseUser> relation = currentUser.getRelation("friends");
          relation.add(user);
          currentUser.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
              if(e == null) {
                String email = null;
                if(user.getEmail() != null && !user.getEmail().isEmpty()) {
                  email = user.getEmail();
                } else {
                  email = user.getUsername();
                }
                User friend =
                  new User(email, (String) user.get("firstName"), (String) user.get("lastName"),
                    user);
                Singleton.INSTANCE.addFriendToList(friend);
                // update the UI here
                FriendsFragment.refreshFriendsList();
                if(Modules.isWeakReferenceValid()) {
                  context.getApplicationContext();
                  Singleton.sendToast(R.string.friend_added_successfully);
                  showAddFriendToCellDialog(context, friend);
                }
              } else {
                if(Modules.isWeakReferenceValid()) {
                  context.getApplicationContext();
                  Singleton.sendToast(e);
                }
              }
            }
          });
        }
      });
    AlertDialog dialog = alert.create();
    dialog.show();
  }

  public static void showVideoStreamDialog(final Activity context, final Video video) {
    ParseQuery<ParseObject> query = ParseQuery.getQuery("Cell411Alert");
    query.whereEqualTo("objectId", video.cell411AlertId);
    query.findInBackground(new FindCallback<ParseObject>() {
      @Override
      public void done(List<ParseObject> parseObjects, ParseException e) {
        if(e == null) {
          final ParseObject cell411AlertObject = parseObjects.get(0);
          ParseRelation relation = cell411AlertObject.getRelation("seenBy");
          relation.add(ParseUser.getCurrentUser());
          cell411AlertObject.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
              if(e != null) {
                cell411AlertObject.saveEventually();
              }
            }
          });
        }
      }
    });
    AlertDialog.Builder alert =
      new AlertDialog.Builder(Singleton.INSTANCE.getCurrentActivity());
    alert.setTitle(R.string.dialog_title_video_streaming_alert);
    int patrolMode = 1;
    if(ParseUser.getCurrentUser().get("PatrolMode") != null) {
      patrolMode = (int) ParseUser.getCurrentUser().get("PatrolMode");
    }
    String message = "";
    if(video.isGlobal == 1) {
      if(patrolMode == 1) {
        message = context.getString(R.string.alert_needy_variant1) + " ";
      } else {
        message = context.getString(R.string.alert_needy_variant2) + " ";
      }
    }
    alert.setMessage(message + context.getString(R.string.dialog_msg_video_streaming, video.name));
    alert.setNegativeButton(R.string.dialog_btn_later, new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialogInterface, int i) {
        ParseQuery<ParseObject> query = ParseQuery.getQuery("Cell411Alert");
        query.whereEqualTo("objectId", video.cell411AlertId);
        query.findInBackground(new FindCallback<ParseObject>() {
          @Override
          public void done(List<ParseObject> parseObjects, ParseException e) {
            if(e == null) {
              ParseObject cell411AlertObject = parseObjects.get(0);
              ParseRelation relation = cell411AlertObject.getRelation("rejectedBy");
              relation.add(ParseUser.getCurrentUser());
              cell411AlertObject.saveInBackground();
            } else {
              if(Modules.isWeakReferenceValid()) {
                context.getApplicationContext();
                Singleton.sendToast(e);
              }
            }
          }
        });
      }
    });
    alert.setPositiveButton(R.string.dialog_btn_watch_now, new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialogInterface, int i) {
        ParseQuery<ParseObject> query = ParseQuery.getQuery("Cell411Alert");
        query.whereEqualTo("objectId", video.cell411AlertId);
        query.findInBackground(new FindCallback<ParseObject>() {
          @Override
          public void done(List<ParseObject> parseObjects, ParseException e) {
            if(e == null) {
              ParseObject cell411AlertObject = parseObjects.get(0);
              ParseRelation relation = cell411AlertObject.getRelation("initiatedBy");
              relation.add(ParseUser.getCurrentUser());
              cell411AlertObject.saveInBackground();
            } else {
              if(Modules.isWeakReferenceValid()) {
                context.getApplicationContext();
                Singleton.sendToast(e);
              }
            }
          }
        });
        try {
          Intent myIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(video.link));
          context.startActivity(myIntent);
        } catch(ActivityNotFoundException e) {
          if(Modules.isWeakReferenceValid()) {
            context.getApplicationContext();
            Singleton.sendToast(R.string.video_player_not_available);
          }
          e.printStackTrace();
        }
      }
    });
    AlertDialog dialog = alert.create();
    dialog.show();
  }

  public static void showPhotoAlertDialog(final Activity context, final Video video,
                                          final String cellName) {
    ParseQuery<ParseObject> query = ParseQuery.getQuery("Cell411Alert");
    query.whereEqualTo("objectId", video.cell411AlertId);
    query.findInBackground(new FindCallback<ParseObject>() {
      @Override
      public void done(List<ParseObject> parseObjects, ParseException e) {
        if(e == null) {
          final ParseObject cell411AlertObject = parseObjects.get(0);
          ParseRelation relation = cell411AlertObject.getRelation("seenBy");
          relation.add(ParseUser.getCurrentUser());
          cell411AlertObject.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
              if(e != null) {
                cell411AlertObject.saveEventually();
              }
            }
          });
        }
      }
    });
    AlertDialog.Builder alert =
      new AlertDialog.Builder(Singleton.INSTANCE.getCurrentActivity());
    alert.setTitle(R.string.dialog_title_photo_alert);
    int patrolMode = 1;
    if(ParseUser.getCurrentUser().get("PatrolMode") != null) {
      patrolMode = (int) ParseUser.getCurrentUser().get("PatrolMode");
    }
    String message = "";
    if(video.isGlobal == 1) {
      if(patrolMode == 1) {
        message = context.getString(R.string.alert_needy_variant1) + " ";
      } else {
        message = context.getString(R.string.alert_needy_variant2) + " ";
      }
    }
    if(cellName != null) {
      alert.setMessage(
        message + context.getString(R.string.dialog_msg_photo_alert_cell, video.name, cellName));
    } else {
      alert.setMessage(message + context.getString(R.string.dialog_msg_photo_alert, video.name));
    }
    alert.setNegativeButton(R.string.dialog_btn_later, new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialogInterface, int i) {
        ParseQuery<ParseObject> query = ParseQuery.getQuery("Cell411Alert");
        query.whereEqualTo("objectId", video.cell411AlertId);
        query.findInBackground(new FindCallback<ParseObject>() {
          @Override
          public void done(List<ParseObject> parseObjects, ParseException e) {
            if(e == null) {
              ParseObject cell411AlertObject = parseObjects.get(0);
              ParseRelation relation = cell411AlertObject.getRelation("rejectedBy");
              relation.add(ParseUser.getCurrentUser());
              cell411AlertObject.saveInBackground();
            } else {
              if(Modules.isWeakReferenceValid()) {
                context.getApplicationContext();
                Singleton.sendToast(e);
              }
            }
          }
        });
      }
    });
    alert.setPositiveButton(R.string.dialog_btn_view, new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialogInterface, int i) {
        ParseQuery<ParseObject> query = ParseQuery.getQuery("Cell411Alert");
        query.whereEqualTo("objectId", video.cell411AlertId);
        query.findInBackground(new FindCallback<ParseObject>() {
          @Override
          public void done(List<ParseObject> parseObjects, ParseException e) {
            if(e == null) {
              ParseObject cell411AlertObject = parseObjects.get(0);
              ParseRelation relation = cell411AlertObject.getRelation("initiatedBy");
              relation.add(ParseUser.getCurrentUser());
              cell411AlertObject.saveInBackground();
            } else {
              if(Modules.isWeakReferenceValid()) {
                context.getApplicationContext();
                Singleton.sendToast(e);
              }
            }
          }
        });
        Intent myIntent = new Intent(context, ImageScreenActivity.class);
        myIntent.putExtra("cell411AlertId", video.cell411AlertId);
        context.startActivity(myIntent);
      }
    });
    AlertDialog dialog = alert.create();
    dialog.show();
  }
}
