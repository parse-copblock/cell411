package cell411.models;

import com.parse.ParseUser;

import java.io.Serializable;

/**
 * Created by Sachin on 8/5/2015.
 */
public class Cell411Alert implements Serializable {
  private static final long serialVersionUID = 101010L;
  public String issuerFirstName;
  public String forwardedBy;
  public ParseUser user;
  public ParseUser forwarder;
  public String issuerEmail;
  public String message;
  public boolean isVideo;
  public boolean isPhoto;
  public boolean isFriendRequest;
  public boolean isCellRequest;
  public boolean isMedical;
  public String status;
  public long millis;
  public boolean selfAlert;
  public boolean isCustomAlert;
  public String cell411AlertId;
  public boolean isDownloading;
  public boolean isDownloaded;
  public int progress;
  public String additionalNote;
  public AlertType alertType;
  public double lat;
  public double lng;
  public String time;
  public String cellName;
  public String fullAddress;
  public String city;
  public String country;

  public Cell411Alert(String issuerFirstName, ParseUser user, String issuerEmail,
                      ParseUser forwarder, boolean isVideo, String status, long millis,
                      boolean selfAlert, boolean isPhoto, String cell411AlertId,
                      String additionalNote, double lat, double lng, boolean isFriendRequest,
                      boolean isCellRequest, boolean isMedical, String forwardedBy,
                      AlertType alertType, String time, String cellName, String fullAddress,
                      String city, String country) {
    this.issuerFirstName = issuerFirstName;
    this.user = user;
    this.issuerEmail = issuerEmail;
    this.forwarder = forwarder;
    this.isVideo = isVideo;
    this.status = status;
    this.millis = millis;
    this.selfAlert = selfAlert;
    this.isPhoto = isPhoto;
    this.cell411AlertId = cell411AlertId;
    this.additionalNote = additionalNote;
    this.lat = lat;
    this.lng = lng;
    this.isFriendRequest = isFriendRequest;
    this.isCellRequest = isCellRequest;
    this.isMedical = isMedical;
    this.forwardedBy = forwardedBy;
    this.alertType = alertType;
    this.time = time;
    this.cellName = cellName;
    this.fullAddress = fullAddress;
    this.city = city;
    this.country = country;
  }

  public static enum AlertType {
    UN_RECOGNIZED(0), BROKEN_CAR(1), BULLIED(2), CRIMINAL(3), GENERAL(4), PULLED_OVER(5), DANGER(6),
    VIDEO(7), PHOTO(8), FIRE(9), MEDICAL(10), POLICE_INTERACTION(11), POLICE_ARREST(12), HIJACK(13),
    PANIC(14), FALLEN(15), PHYSICAL_ABUSE(16), TRAPPED(17), CAR_ACCIDENT(18), NATURAL_DISASTER(19),
    PRE_AUTHORISATION(20);
    private int value;

    private AlertType(int value) {
      this.value = value;
    }

    public int getValue() {
      return value;
    }
  }
}