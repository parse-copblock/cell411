package cell411.models;

import java.io.Serializable;

import static cell411.SingletonConfig.WOWZA_BASE_URL_DVR;
import static cell411.SingletonConfig.WOWZA_BASE_URL_DVR_POSTFIX;

/**
 * Created by Sachin on 8/26/2015.
 */
public class Video implements Serializable {
  private static final long serialVersionUID = 20001L;
  public String link;
  public String userId;
  public String name;
  public String time;
  public long createdAt;
  public boolean live;
  public String cell411AlertId;
  public int isGlobal;

  public Video(String userId, String name, long createdAt, boolean live, String cell411AlertId,
               int isGlobal) {
        /*if (live)
            link = Singleton.WOWZA_BASE_URL_LIVE + userId + "_" + createdAt;
        else
            link = Singleton.WOWZA_BASE_URL_VOD + userId + "_" + createdAt;*/
    link = WOWZA_BASE_URL_DVR + userId + "_" + createdAt +
             WOWZA_BASE_URL_DVR_POSTFIX;
    this.userId = userId;
    this.name = name;
    this.live = live;
    this.createdAt = createdAt;
    this.cell411AlertId = cell411AlertId;
    this.isGlobal = isGlobal;
  }
}