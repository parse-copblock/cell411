package cell411.models;

/**
 * Created by Sachin on 08-02-2017.
 */
public class ChatRoom {
  public int id;
  public int entityTypeId;
  public EntityType entityType;
  public String entityObjectId;
  public String entityName;
  public long entityCreatedAt;
  public String senderId;
  public String senderFirstName;
  public String senderLastName;
  public String msg;
  public Long msgTime;
  public String msgType;
  public int msgCount;
  public boolean isReceived;
  public boolean isRemoved;
  public boolean isExpired;

  public ChatRoom(int id, int entityTypeId, String entityObjectId, String entityName,
                  String entityCreatedAt, String senderId, String senderFirstName,
                  String senderLastName, String msg, String msgTime, String msgType, int msgCount,
                  boolean isReceived, boolean isRemoved) {
    this.id = id;
    this.entityTypeId = entityTypeId;
    this.entityObjectId = entityObjectId;
    this.entityName = entityName;
    this.entityCreatedAt = Long.valueOf(entityCreatedAt);
    this.senderId = senderId;
    this.senderFirstName = senderFirstName;
    this.senderLastName = senderLastName;
    this.msg = msg;
    this.msgTime = Long.parseLong(msgTime);
    this.msgType = msgType;
    this.msgCount = msgCount;
    this.isReceived = isReceived;
    switch(entityTypeId) {
      case 0:
        entityType = EntityType.PUBLIC_CELL;
        break;
      case 1:
        entityType = EntityType.ALERT;
        break;
    }
  }

  public ChatRoom(EntityType entityType, String entityObjectId, String entityName,
                  String entityCreatedAt, String senderId, String senderFirstName,
                  String senderLastName, String msg, long msgTime, String msgType,
                  boolean isReceived) {
    this.entityType = entityType;
    this.entityObjectId = entityObjectId;
    this.entityName = entityName;
    if(entityCreatedAt == null) {
      entityCreatedAt = "100000";
    }
    this.entityCreatedAt = Long.valueOf(entityCreatedAt);
    this.senderId = senderId;
    this.senderFirstName = senderFirstName;
    this.senderLastName = senderLastName;
    this.msg = msg;
    this.msgTime = msgTime;
    this.msgType = msgType;
    this.isReceived = isReceived;
    switch(entityType) {
      case PUBLIC_CELL:
        this.entityTypeId = 0;
        break;
      case ALERT:
        this.entityTypeId = 1;
        break;
    }
  }

  public static enum EntityType {
    PUBLIC_CELL, PRIVATE_CELL, ALERT
  }
}