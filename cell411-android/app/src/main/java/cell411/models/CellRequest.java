package cell411.models;

import java.io.Serializable;

/**
 * Created by Sachin on 8/1/2015.
 */
public class CellRequest implements Serializable {
  private static final long serialVersionUID = 101010L;
  public String name;
  public String userId;
  public String message;
  public String cellRequestObjectId;
  public String cellName;
  public String cellId;

  public CellRequest(String name, String userId, String message, String cellRequestObjectId,
                     String cellName, String cellId) {
    this.name = name;
    this.userId = userId;
    this.message = message;
    this.cellRequestObjectId = cellRequestObjectId;
    this.cellName = cellName;
    this.cellId = cellId;
  }
}