package cell411.models;

import java.io.Serializable;

/**
 * Created by Sachin on 10-08-2016.
 */
public class PublicCell implements Serializable {
  private static final long serialVersionUID = 101010L;
  public String description;
  public String category;
  public int cellType;
  public double lat;
  public double lng;
  public String city;
  public String country;
  public String fullAddress;
  private String name;
  private String objectId;

  public PublicCell(String name, String objectId) {
    this.name = name;
    this.objectId = objectId;
  }

  public String getName() {
    return name;
  }

  public String getObjectId() {
    return objectId;
  }
}