package cell411.models;

import java.io.Serializable;

/**
 * Created by Sachin on 7/21/2015.
 */
public class AlertFromNeedy implements Serializable {
  private static final long serialVersionUID = 101010L;
  public String firstName;
  public String userId;
  public double lat;
  public double lon;
  public long createdAt;
  public String message;
  public String cell411AlertId;
  public String alertRegarding;
  public String _additionalNote;
  public int _dispatchMode;
  public int _isGlobal;
  public String forwardedBy;
  public String cellName;
  public String cellId;
  public int alertId;
  public String forwardingAlertId;
  public String fullAddress;
  public String city;
  public String country;

  public AlertFromNeedy(String firstName, String userId, double lat, double lon, long createdAt,
                        String message, String cell411AlertId, String alertRegarding,
                        String _additionalNote, int _dispatchMode, int _isGlobal,
                        String forwardedBy, String cellName, String cellId, int alertId,
                        String forwardingAlertId, String fullAddress, String city, String country) {
    this.firstName = firstName;
    this.userId = userId;
    this.lat = lat;
    this.lon = lon;
    this.createdAt = createdAt;
    this.message = message;
    this.cell411AlertId = cell411AlertId;
    this.alertRegarding = alertRegarding;
    this._additionalNote = _additionalNote;
    this._dispatchMode = _dispatchMode;
    this._isGlobal = _isGlobal;
    this.forwardedBy = forwardedBy;
    this.cellName = cellName;
    this.cellId = cellId;
    this.alertId = alertId;
    this.forwardingAlertId = forwardingAlertId;
    this.fullAddress = fullAddress;
    this.city = city;
    this.country = country;
  }
}