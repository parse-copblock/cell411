package cell411.models;

/**
 * Created by Sachin on 14-05-2018.
 */
public class CellCategory {
  private String text;
  private int cellType;

  public CellCategory(String text, int cellType) {
    this.text = text;
    this.cellType = cellType;
  }

  public String getText() {
    return text;
  }

  public int getCellType() {
    return cellType;
  }
}