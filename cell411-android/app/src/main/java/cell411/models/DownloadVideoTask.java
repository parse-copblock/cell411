package cell411.models;

import java.io.Serializable;

/**
 * Created by Sachin on 7/12/2015.
 */
public class DownloadVideoTask implements Serializable {
  private static final long serialVersionUID = 101010L;
  public String videoLink;
  public String cell411AlertId;

  public DownloadVideoTask(String videoLink, String cell411AlertId) {
    this.videoLink = videoLink;
    this.cell411AlertId = cell411AlertId;
  }
}