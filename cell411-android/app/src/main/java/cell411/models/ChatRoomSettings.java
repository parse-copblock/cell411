package cell411.models;

/**
 * Created by Sachin on 08-02-2017.
 */
public class ChatRoomSettings {
  public int id;
  public int entityTypeId;
  public ChatRoom.EntityType entityType;
  public String entityObjectId;
  public String entityName;
  public boolean muteEnabled;
  public boolean notificationDisabled;
  public String muteFor;
  public long muteTime;
  public long muteUntil;

  public ChatRoomSettings(ChatRoom.EntityType entityType, String entityObjectId, String entityName,
                          boolean muteEnabled, boolean notificationDisabled, String muteFor,
                          long muteTime, long muteUntil) {
    this.entityType = entityType;
    this.entityObjectId = entityObjectId;
    this.entityName = entityName;
    this.muteEnabled = muteEnabled;
    this.notificationDisabled = notificationDisabled;
    this.muteFor = muteFor;
    this.muteTime = muteTime;
    this.muteUntil = muteUntil;
    switch(entityType) {
      case PUBLIC_CELL:
        this.entityTypeId = 0;
        break;
      case ALERT:
        this.entityTypeId = 1;
        break;
    }
  }
}