package cell411.models;

import java.io.Serializable;

/**
 * Created by Sachin on 8/5/2015.
 */
public class Cell411AlertDetail implements Serializable {
  private static final long serialVersionUID = 101010L;
  public String issuerFirstName;
  public String forwardedBy;
  public String issuerEmail;
  public boolean isVideo;
  public boolean isPhoto;
  public boolean isMedical;
  public String status;
  public long millis;
  public boolean selfAlert;
  public boolean isCustomAlert;
  public String cell411AlertId;
  public boolean isDownloading;
  public boolean isDownloaded;
  public int progress;
  public String additionalNote;
  public double lat;
  public double lng;
  public String time;
  public Cell411Alert.AlertType alertType;
  public String cellName;
  public String fullAddress = null;
  public String city = null;
  public String country = null;
  public int isDeleted;

  public Cell411AlertDetail() {
  }
}