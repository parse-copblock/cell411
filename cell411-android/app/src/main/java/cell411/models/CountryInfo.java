package cell411.models;

import java.io.Serializable;

public class CountryInfo implements Serializable {
  private static final long serialVersionUID = 1L;
  public String name;
  public String dialingCode;
  public String shortCode;
  public int flagId;
  public boolean selected;

  public CountryInfo(String name, String dialingCode, String shortCode, int flagId) {
    this.name = name;
    this.dialingCode = dialingCode;
    this.shortCode = shortCode;
    this.flagId = flagId;
  }

  @Override
  public String toString() {
    return name.toString();
  }
}