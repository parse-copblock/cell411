package cell411.models;

import com.parse.ParseUser;

import java.io.Serializable;

/**
 * Created by Sachin on 7/12/2015.
 */
public class User implements Serializable {
  private static final long serialVersionUID = 20001L;
  public String email;
  public String firstName;
  public String lastName;
  public ParseUser user;
  public boolean selected;
  public boolean friendRemoveInProgress;
  public boolean isFriend;
  public boolean friendAddInProgress;
  public boolean isRequestSent;

  public User(String email, String firstName, String lastName, ParseUser user) {
    this.email = email;
    this.firstName = firstName;
    this.lastName = lastName;
    this.user = user;
  }
}