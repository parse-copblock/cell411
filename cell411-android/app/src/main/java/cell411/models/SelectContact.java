package cell411.models;

import java.io.Serializable;

/**
 * Created by Sachin on 7/12/2015.
 */
public class SelectContact implements Serializable {
  private static final long serialVersionUID = 101010L;
  public String contactId;
  public String displayName;
  public String email;
  public String phone;
  public boolean isChecked;

  public SelectContact(String contactId, String displayName) {
    this.contactId = contactId;
    this.displayName = displayName;
  }
}