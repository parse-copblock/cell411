package cell411.models;

import com.parse.ParseObject;
import com.parse.ParseUser;

import org.json.JSONArray;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Sachin on 03-11-2017.
 */
public class NewPrivateCell implements Serializable {
  private static final long serialVersionUID = 101010L;
  public String name;
  public ParseObject parseObject;
  public ArrayList<ParseUser> members;
  public JSONArray nauMembers;
  public Integer type;
  public Integer totalMembers;
  public boolean selected;
  public boolean isCallCenter;

  public NewPrivateCell(String name, ParseObject parseObject, Integer type, Integer totalMembers) {
    this.name = name;
    this.parseObject = parseObject;
    this.type = type;
    this.totalMembers = totalMembers;
    members = new ArrayList<>();
    nauMembers = new JSONArray();
  }
}