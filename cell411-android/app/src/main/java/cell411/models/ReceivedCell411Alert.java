package cell411.models;

import java.io.Serializable;

/**
 * Created by Sachin on 8/5/2015.
 */
public class ReceivedCell411Alert implements Serializable {
  private static final long serialVersionUID = 101010L;
  public Cell411Alert.AlertType alertType;
  public String issuerFirstName;
  public String issuerLastName;
  public String userId;
  public double lat;
  public double lon;
  public long createdAt;
  public String message;
  public String cell411AlertId;
  public String _additionalNote;
  public int _dispatchMode;
  public int _isGlobal;
  public String forwardedBy;
  public String cellName;
  public String cellId;
  public String forwardingAlertId;
  public String fullAddress;
  public String city;
  public String country;

  public ReceivedCell411Alert(String issuerFirstName, String issuerLastName, String userId,
                              double lat, double lon, long createdAt, String message,
                              String cell411AlertId, String _additionalNote, int _dispatchMode,
                              int _isGlobal, String forwardedBy, String cellName, String cellId,
                              Cell411Alert.AlertType alertType, String forwardingAlertId,
                              String fullAddress, String city, String country) {
    this.issuerFirstName = issuerFirstName;
    this.issuerLastName = issuerLastName;
    this.userId = userId;
    this.lat = lat;
    this.lon = lon;
    this.createdAt = createdAt;
    this.message = message;
    this.cell411AlertId = cell411AlertId;
    this._additionalNote = _additionalNote;
    this._dispatchMode = _dispatchMode;
    this._isGlobal = _isGlobal;
    this.forwardedBy = forwardedBy;
    this.cellName = cellName;
    this.cellId = cellId;
    this.alertType = alertType;
    this.forwardingAlertId = forwardingAlertId;
    this.fullAddress = fullAddress;
    this.city = city;
    this.country = country;
  }
}