package cell411.models;

import com.parse.ParseUser;

import java.io.Serializable;

/**
 * Created by Sachin on 7/12/2015.
 */
public class Contact implements Serializable {
  private static final long serialVersionUID = 101010L;
  public String contactId;
  public String displayName;
  public Status status;
  public String email;
  public String phone;
  public ParseUser user;
  public boolean inProcessing;

  public Contact(String contactId, String displayName) {
    this.contactId = contactId;
    this.displayName = displayName;
    this.status = Status.INITIALIZING;
  }

  public static enum Status {
    INITIALIZING, FRIENDS, USER_EXIST, USER_DOES_NOT_EXIST, FRIEND_REQUEST_PENDING,
    INVITATION_PENDING
  }
}