package cell411.models;

import java.io.Serializable;

/**
 * Created by Sachin on 8/1/2015.
 */
public class FriendApproved implements Serializable {
  private static final long serialVersionUID = 101010L;
  public String name;
  public String userId;
  public String message;

  public FriendApproved(String name, String userId, String message) {
    this.name = name;
    this.userId = userId;
    this.message = message;
  }
}