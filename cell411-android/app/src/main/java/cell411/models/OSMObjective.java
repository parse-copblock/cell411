package cell411.models;

import java.io.Serializable;

public class OSMObjective implements Serializable {
  private String name;
  private String amenity;
  private String openingHours;
  private String website;
  private double lat;
  private double lng;
  private String tags;

  public OSMObjective(String name, String amenity, String openingHours, String website, double lat,
                      double lng, String tags) {
    this.name = name;
    this.amenity = amenity;
    this.openingHours = openingHours;
    this.website = website;
    this.lat = lat;
    this.lng = lng;
    this.tags = tags;
  }

  public String getName() {
    return name;
  }

  public String getAmenity() {
    return amenity;
  }

  public String getOpeningHours() {
    return openingHours;
  }

  public String getWebsite() {
    return website;
  }

  public double getLat() {
    return lat;
  }

  public double getLng() {
    return lng;
  }

  public String getTags() {
    return tags;
  }
}