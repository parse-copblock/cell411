package cell411.models;

import com.parse.ParseObject;
import com.parse.ParseUser;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Sachin on 7/12/2015.
 */
public class Cell implements Serializable {
  private static final long serialVersionUID = 101010L;
  public String name;
  public ParseObject cell;
  public ArrayList<ParseUser> members;
  public boolean selected;
  public boolean isPublicCell;
  public boolean isCallCenter;
  public int totalMembers;
  public int isVerified;
  public int verificationStatus;
  public Status status;
  public String cell411AlertId;
  public boolean inProcessing;

  public Cell(String name, ParseObject cell) {
    this.name = name;
    this.cell = cell;
    members = new ArrayList<>();
    this.status = Status.INITIALIZING;
  }

  public static enum Status {
    INITIALIZING, JOINED, NOT_JOINED, DENIED, PENDING, UN_CHANGED, OWNER
  }
}