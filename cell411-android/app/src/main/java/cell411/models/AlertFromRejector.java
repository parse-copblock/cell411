package cell411.models;

import java.io.Serializable;

/**
 * Created by Sachin on 7/21/2015.
 */
public class AlertFromRejector implements Serializable {
  private static final long serialVersionUID = 101010L;
  public String name;
  public String userId;
  public long createdAt;
  public String message;
  public String additionalNoteId;

  public AlertFromRejector(String name, String userId, long createdAt, String message,
                           String additionalNoteId) {
    this.name = name;
    this.userId = userId;
    this.createdAt = createdAt;
    this.message = message;
    this.additionalNoteId = additionalNoteId;
  }
}