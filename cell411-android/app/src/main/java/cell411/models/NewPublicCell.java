package cell411.models;

import java.io.Serializable;

/**
 * Created by Sachin on 7/21/2015.
 */
public class NewPublicCell implements Serializable {
  private static final long serialVersionUID = 101010L;
  public String cellName;
  public String cellId;
  public String userId;

  public NewPublicCell(String cellName, String cellId, String userId) {
    this.cellName = cellName;
    this.cellId = cellId;
    this.userId = userId;
  }
}