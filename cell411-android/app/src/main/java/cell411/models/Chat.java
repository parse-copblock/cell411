package cell411.models;

import java.io.Serializable;

/**
 * Created by Sachin on 10/7/2015.
 */
public class Chat implements Serializable {
  private static final long serialVersionUID = 101010L;
  public String senderId;
  public String senderFirstName;
  public String senderLastName;
  public String msg;
  public String msgType;
  public long time;
  public String path;
  public String imageUrl;
  public boolean isReceived;
  public String entityType;
  public String entityName;
  public String entityObjectId;
  public String entityCreatedAt;
  public double lat;
  public double lng;
  public boolean isImageUploadedToServer;
  public boolean isImageDownloadedFromServer;
  public boolean isMessageSentToServer;
  public boolean notificationDisabled;
  public boolean muteEnabled;

  public Chat(String senderId, String senderFirstName, String senderLastName, String msg,
              String msgType, long time) {
    this.senderId = senderId;
    this.senderFirstName = senderFirstName;
    this.senderLastName = senderLastName;
    this.msg = msg;
    this.msgType = msgType;
    this.time = time;
  }
}