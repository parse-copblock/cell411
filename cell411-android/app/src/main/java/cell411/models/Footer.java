package cell411.models;

import java.io.Serializable;

/**
 * Created by Sachin on 7/12/2015.
 */
public class Footer implements Serializable {
  private static final long serialVersionUID = 20001L;
  public String info;
  public boolean spinnerVisibility;

  public Footer(String info, boolean spinnerVisibility) {
    this.info = info;
    this.spinnerVisibility = spinnerVisibility;
  }
}