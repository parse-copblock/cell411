package com.parse;

import com.google.firebase.iid.FirebaseInstanceId;

import cell411.utils.LogEvent;

public class ParseCheater {
  public static String TAG = "ParseCheater";
  public static void setup() {
    new Thread(new Runnable() {
      public void run() {
        doSetup();
      }
    }).start();
  }

  ;

  public static void doSetup() {
    LogEvent.Log(TAG,"doSetup");
    ParseInstallation installation = ParseInstallation.getCurrentInstallation();
    boolean needsSave = false;
    if(installation.get("user") == null) {
      needsSave = true;
      ParseUser user = ParseUser.getCurrentUser();
      if(user != null) {
        installation.put("user", user);
      }
    }
    if(installation.getDeviceToken() == null) {
      needsSave = true;
      FirebaseInstanceId instanceId = FirebaseInstanceId.getInstance();
      String token = instanceId.getToken();
      if(token != null) {
        installation.setDeviceToken(token);
      }
    }
    ;
    if(needsSave) {
      installation.saveInBackground();
    }
  }
}
