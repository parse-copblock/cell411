# cell411-android

## Purpose

This is the android client for cell411.

## Obtaining

```
# Use bash -- the Bourne Again Shell

git clone <repo>
cd cell411/keys
gpg -d < secrets.xml.asc > secrets.xml
cd ..
cd cell411-android
mv ../keys/secrets.xml $(dirname $(find -name config.xml))

```

## Building

Either run ./gradlew assemble or fire up android studio and open the cell411-android directory.

## Alternate configurations:

For security, we have to keep the credentials in secrets.xml secret.  If you
are not part of the production development team, you'll have to fire up your
own instance of the server, and point your client to that.

Use a url that will point to your parse server, and whatever appid and client
key you like, so long as they match.

