source ~/.parse/parse.env

curl -X GET \
  -H "X-Parse-Application-Id: $X_PARSE_APPLICATION_ID" \
  -H "X-Parse-Master-Key: $X_PARSE_MASTER_KEY" \
  -H "X-Parse-Rest-API-Key: $X_PARSE_REST_API_KEY" \
  http://localhost:1337/parse/serverInfo | json_pp


