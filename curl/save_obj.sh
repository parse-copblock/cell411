source ~/.parse/parse.env
curl -X POST \
  -H "X-Parse-Application-Id: $X_PARSE_APPLICATION_ID" \
  -H "X-Parse-Master-Key: $X_PARSE_MASTER_KEY" \
  -H "X-Parse-Rest-API-Key: $X_PARSE_REST_API_KEY" \
  -H "Content-Type: application/json" \
  -d '{"score":123,"playerName":"Sean Plott","cheatMode":false}' \
http://localhost:1337/parse/classes/GameScore

