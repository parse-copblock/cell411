// Example express application adding the parse-server module to expose Parse
// compatible API routes.

const password = require('parse-server').password;
const express = require('express');
const ParseServer = require('parse-server').ParseServer;
const path = require('path');
const args = process.argv || [];
const fs = require('fs');

const config = require(process.env.HOME+"/.parse/config.json");
if (!config.pubServerURL) {
  console.log('pubServerURL not specified.  bye!');
  process.exit(1);
};
if (!config.databaseURL) {
  console.log('databaseURL not specified, failing');
  process.exit(1);
};

const app = express();

// Serve static assets from the /public folder
app.use('/public', express.static(path.join(__dirname, '/')));

var ParseDashboard = require('parse-dashboard');
var options =
{
};
var dashboard_cfg= {
  "apps": [
    {
      "serverURL": config.pubServerURL,
      "appId": config.appId,
      "masterKey": config.masterKey,
      "appName": config.appName,
    },
  ],
  "users": [ {
    "user": config.dashUser,
    "pass": config.dashPass,
    "apps": [{"appId": config.appId}],
  } ],
};
var dashboard = new ParseDashboard(dashboard_cfg,options);

app.use('/dashboard', dashboard );

const mountPath = '/parse';
const server = new ParseServer(config);
const reqKeys = [ 
"httpVersionMajor",
"httpVersionMinor",
"httpVersion",
"rawHeaders",
"url",
"method",
"statusCode",
"baseUrl",
"originalUrl",
"params",
"query",
"body",
];
var api = function fuck(req,res) {
//  console.log("fuck(req,res)");
//  console.trace();
  var obj={};
  reqKeys.forEach(function(key){ obj[key]=req[key] });
  console.log(obj);
  server(req,res); 
};

app.use(mountPath, api);

app.get('/', function(req, res) {
  res.status(200).send('I dream of being a website.  Please star the parse-server repo on GitHub!');
});

const port = config.port;
const http = require('http');
const httpServer = http.createServer(app); 
httpServer.listen(port, '127.0.0.1', function() {
  console.log('parse-server-example running on port ' + port + '.');
});
ParseServer.createLiveQueryServer(httpServer);

module.exports = {
  app,
  config
};
