# run-parse

This directory contains a supervise directory for use with daemontools.

install daemontools with
```
apt-get install demontools

ln -sf $PWD /service

```

when svscanboot is running, the parse server will come up with cell411
installed.
