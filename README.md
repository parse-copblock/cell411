# Cell411

## Purpose

Cell411 is a system to allow users to crowd-source emergency response

This repo will contain the entire system, both client and server side.

## What's here

 * cell411-android - android client
 * cell411-serv - cell411 server, including parse.
 * dbtools - various tools for dealing with mongo
 * gitlab.md - original README.md from gitlab, with some usage info
 * keys - secrets required by production version, developer public keys,
   management scripts.
 * parse-client - files for using parse from node
 * README.md - this file
