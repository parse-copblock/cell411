const Parse = require('./node.js');
const config = require(process.env.HOME+'/.parse/config.json');
class MyUser extends Parse.User {
  constructor(attributes) {
    super(attributes);
    console.log({attributes: attributes});
  };
};
Parse.Object.registerSubclass('_User',MyUser);
async function doit() {

  Parse.serverURL = config.devServerURL;
  Parse.initialize(
    config.appId,
    config.javascriptKey,
    config.masterKey
  );
  Parse.Object.enableSingleInstance();
  Parse.Config.get().then(function(config){
    console.log({config: config});
  }, function(error){
    console.log({error: error});
  });
//  const Config = await Parse.Config.get({userMasterKey: true});
  var query = new Parse.Query('_User');
  query.include('friends');
  query.limit(30);
  var res = await query.find();
  console.log(res);
  console.log(query);
}

doit();


