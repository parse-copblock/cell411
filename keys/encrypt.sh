#!/bin/bash

cd $(dirname "$BASH_SOURCE")

if (($#)); then
  files=( "$@" )
else
  shopt -s nullglob
  files=( *.h *.xml *.jks )
fi

set -- "${files[@]}"
if (( !$# )); then
  echo >&2 no files
  exit 1
fi
set -- 
set -- "$@" dev@copblock.app
set -- "$@" jim@vsiwest.com
set -- "$@" la@duke.dev
set -- "$@" tibrewal.sachin.m@gmail.com
set -- "$@" milan@wiselysoft.com

for file in ${files[@]}; do
  file="${file%.asc}"

  if test -e $file.asc ; then
    if test -e $file.old.asc; then
      rm -f $file.asc
    else
      mv -f $file.asc $file.old.asc
    fi
  fi

  if gpg -sea  $(printf ' -r %s ' "$@" ) --output ${file}.asc ${file} ; then
      shred $file
      rm -f $file
  fi
done
